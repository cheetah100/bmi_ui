import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import { GRAVITY_PARAMETERS, GravityParameters, GravityApiUser } from '@bmi/gravity-services';

if (environment.production) {
  enableProdMode();
} else {
  localStorage.setItem('devCardCache', 'true');
}

platformBrowserDynamic([
  {
    provide: GRAVITY_PARAMETERS,
    useValue: <GravityParameters>{
      baseAppId: 'admin',
      baseUrl: '/gravity/spring',
      boardConfigs: {},
    }
  }, {provide: 'USER_PROVIDER', useValue: <Partial<GravityApiUser>>{
    id: 'idp-nprod-dev',
    teams: {'administrators': ''}
  }},
]).bootstrapModule(AppModule)
  .catch(err => console.error(err));
