import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

// Application
import { ModulesComponent } from './components/modules/modules.component';
import { BmiCorePageComponent } from './components/bmi-core-page/bmi-core-page.component';

import { ModuleResolver, ModulePageResolver } from '@bmi/core';
import { ModuleGuard } from './utils/services/module-guard.service';
import { DefaultModuleComponent } from './components/modules/default-module.component';


@NgModule({
  imports: [
    RouterModule.forRoot([
      { path: '', pathMatch: 'full', redirectTo: 'modules' },
      { path: 'modules', component: ModulesComponent },
      {
        path: ':moduleId',
        resolve: {
          module: ModuleResolver
        },
        children: [
          { path: '', pathMatch: 'full', canActivate: [ModuleGuard], component: DefaultModuleComponent },
          { path: 'page-list', component: DefaultModuleComponent },
          { path: 'pages/:pageId', component: BmiCorePageComponent },
        ],
      },
    ],
    {
      useHash: false,
      scrollPositionRestoration: 'enabled',
      paramsInheritanceStrategy: 'always',
      relativeLinkResolution: 'legacy'
    })
  ],
  providers: [
    ModuleResolver,
    ModulePageResolver
  ]
})
export class BmiAppRoutingModule { }