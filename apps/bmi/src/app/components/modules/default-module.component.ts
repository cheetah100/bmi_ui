import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap, map } from 'rxjs/operators';
import { ModuleService, Module, BmiRoute } from '@bmi/core';
import { watchParams, combineLatestObject } from '@bmi/utils';
import sortBy from 'lodash-es/sortBy';
/*
 * Very simple: just a header and module pages list
 * It is intended for admin users to pick a default route for the module home.
 * So really this page would not show up unless the module is not fully configured.
 */

@Component({
  template: `
      <ng-template let-info>
        <section class="default-module__header">
          <h1>{{ info.config.name }}</h1>
          <p *ngIf="info.config.description">{{info.config.description}}</p>
        </section>
        <section class="default-module__page-list">
          <h2>Module Pages</h2>
          <ul><li *ngFor="let page of info.pages">
            <a [bmiRouteLink]="page.route" class="btn--link">
              {{ page.title }}
            </a>
          </li></ul>
        </section>
      </ng-template>
  `,
  styles: [`
    .default-module__header, .default-module__page-list {
      margin: var(--gra-spacing);
    }

    ul {
      list-style-type: none;
      margin: 0;
      padding: 0;
    }

    li {
      line-height: 2em;
    }
  `],
})
export class DefaultModuleComponent {

  module = watchParams(this.route, 'moduleId').pipe(
    map(({moduleId}) => moduleId),
    switchMap(moduleId => this.moduleService.getModule(moduleId))
  );

  moduleInfo = combineLatestObject({
    config: this.module.pipe(switchMap(m => m.config)),
    pages: this.module.pipe(
      switchMap(mod => mod.pageList),
      map(pages => {
        return sortBy(pages, p => p.title)
          .map(p => ({
            title: p.title,
            route: <BmiRoute>{type: 'page', pageId: p.id, moduleId: p.applicationId}
          }))
      })
    )
  });

  constructor(
    private moduleService: ModuleService,
    private route: ActivatedRoute,
  ) { }

}
