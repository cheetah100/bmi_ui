import { Component, OnInit, OnDestroy, isDevMode } from '@angular/core';

import { combineLatest, Subscription } from 'rxjs';

import { AuthService } from '../../utils/services/auth.service';
import { HttpClient } from '@angular/common/http';


interface AppInfo {
  id: string;
  name: string;
  description?: string;
  linkHref?: string;
  sortOrder?: number;
  adminOnly?: boolean;
}


@Component({
  selector: 'app-modules',
  templateUrl: './modules.component.html',
  styleUrls: ['./modules.component.scss']
})
export class ModulesComponent implements OnInit, OnDestroy {
  apps: AppInfo[];
  loading = true;

  // These pseudo-modules are built-in functionality, but should show up on the
  // modules screen along with Gravity provided apps.
  builtinApps: AppInfo[] = [
    {
      id: 'bmi-admin',
      name: 'BMI Admin',
      description: 'Administration tools for the BMI platform.' + (isDevMode() ? ` (Psst! Devs: Heads Up! This link won't work from the Angular dev server)` : ''),
      linkHref: '/admin',
      sortOrder: 1000,
      adminOnly: true
    }
  ];

  private subscription: Subscription;

  constructor(
    private authService: AuthService,
    private http: HttpClient
  ) { }

  ngOnInit() {

    this.subscription = combineLatest([
      this.http.get('/gravity/spring/app/descriptions')
    ]).subscribe(responses => {
      console.log('app-modules responses', responses);
      const appConfigs: AppInfo[] = [];
      Object.keys(responses[0]).forEach(appId => {
        appConfigs.push(Object.assign({},
          responses[0][appId],
          {
            id: appId,
          }
        ));
      });

      // TODO: Currently gravity_admin is only "admin only" module and is located only in above builtinApps.
      // Need to add module to gravity db and write generic function to append admin apps to appConfigs
      this.authService.shouldShowAdminTools().subscribe(response => {
        if (response) {
          appConfigs.push(...this.builtinApps);
        }
      });
      this.apps = appConfigs;
      this.loading = false;
      console.log('done loading', appConfigs);
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
