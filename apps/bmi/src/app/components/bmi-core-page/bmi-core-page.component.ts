import { Component, OnInit, OnDestroy, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { EMPTY, Subscription, of, combineLatest } from 'rxjs';
import { catchError, debounceTime, map, switchMap, retry, tap } from 'rxjs/operators';

import { PageRepositoryService, PageConfig, Module, ModuleService, migratePage } from '@bmi/core';
import { ObjectMap, distinctUntilNotEqual } from '@bmi/utils';
import { AuthService } from '../../utils/services/auth.service';

@Component({
  selector: 'app-bmi-core-page',
  template: `
    <a *ngIf="(isAdmin | async) && pageConfig" class="btn btn--link page__admin-edit-link" [href]="editPageUrl" target="_blank">
      <ui-icon icon="fa4-edit"></ui-icon>&nbsp;Edit Page
    </a>
    <bmi-page [config]="pageConfig" [filters]="filters" (filtersChange)="onPageFiltersUpdated.emit($event)"></bmi-page>
  `,

  styles: [`
    :host {
      display: block;
      position: relative;
    }

    .page__admin-edit-link {
      position: absolute !important;
      right: 0;
      top: 5px;

      opacity: 0.5;
      transition: opacity 0.1s;
    }

    .page__admin-edit-link:hover {
      opacity: 1;
    }
  `]
})
export class BmiCorePageComponent implements OnInit, OnDestroy {

  pageConfig: PageConfig = null;
  isLoading = true;
  errorMessage: string;
  filters: ObjectMap<string> = {};

  private subscription = new Subscription();

  onPageFiltersUpdated = new EventEmitter<ObjectMap<string>>();

  isAdmin = this.authService.shouldShowAdminTools();

  constructor (
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private pageRepository: PageRepositoryService,
    private moduleService: ModuleService,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.subscription.add(this.activatedRoute.paramMap.pipe(
      map(params => ({moduleId: params.get('moduleId'), pageId: params.get('pageId')})),
      distinctUntilNotEqual(),
      switchMap(({moduleId, pageId}) => {
        console.log({moduleId, pageId});
        this.isLoading = true;
        this.errorMessage = null;
        return this.loadPage(moduleId, pageId)
      }),
      catchError(() => {
        this.errorMessage = 'Page Not Found';
        return of(<PageConfig>null);
      }),
    ).subscribe((page) => {
      this.pageConfig = page;
      this.isLoading = false;
    }))

    this.subscription.add(this.activatedRoute.paramMap.pipe(
      map(params => {
        const filters: ObjectMap<string> = {};
        params.keys.forEach(key => filters[key] = params.get(key));
        delete filters['pageId'];
        delete filters['moduleId'];
        return filters;
      }),
      distinctUntilNotEqual(),
    ).subscribe(filters => this.filters = filters));

    this.subscription.add(this.onPageFiltersUpdated.pipe(
      distinctUntilNotEqual(),
      debounceTime(100),
    ).subscribe(filters => {
      this.updateUrlFilters(filters);
    }));
  }


  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  updateUrlFilters(filter: ObjectMap<string>) {
    this.router.navigate([filter], {
      replaceUrl: true,
      relativeTo: this.activatedRoute,
    });
  }

  get editPageUrl() {
    return `/admin/module/${this.pageConfig.applicationId}/pages/${this.pageConfig.id}`;
  }

  private loadPage(moduleId: string, pageId: string) {
    return this.moduleService.getModule(moduleId).pipe(
      switchMap(module => module.getPage(pageId)),
      distinctUntilNotEqual(),
      map(page => migratePage(page))
    );
  }
}
