import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';

import { BmiCoreModule, BmiWidgetsModule, BmiCoreEditorModule } from '@bmi/core';
import { BmiChartsEditorModule, ChartDataSourceService } from '@bmi/legacy-charts';
import { SharedUiModule, ModalService } from '@bmi/ui';
import { GravityModule, GravityUiModule, GRAVITY_PARAMETERS, GravityParameters } from '@bmi/gravity-services';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ConfirmationModalService } from '@bmi/ui';

import { BmiAppRoutingModule } from './app.routes';
import { CommonModule } from '@angular/common';
import { ModulesComponent } from './components/modules/modules.component';
import { DefaultModuleComponent } from './components/modules/default-module.component';
import { BmiCorePageComponent } from './components/bmi-core-page/bmi-core-page.component';

@NgModule({
  declarations: [
    AppComponent,
    ModulesComponent,
    DefaultModuleComponent,
    BmiCorePageComponent,
  ],
  imports: [
    BmiAppRoutingModule,
    BrowserModule,
    CommonModule,
    BrowserAnimationsModule,
    BmiCoreModule,
    BmiCoreEditorModule,
    BmiWidgetsModule,
    SharedUiModule,
    BmiChartsEditorModule,
    GravityModule.forRoot({
      registerDataSources: true
    }),
    GravityUiModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule
  ],
  providers: [
    ConfirmationModalService,
    {
      provide: GRAVITY_PARAMETERS,
      useValue: <GravityParameters>{
        baseUrl: '/gravity/spring',
        boardConfigs: {}
      }
    },
    ModalService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
