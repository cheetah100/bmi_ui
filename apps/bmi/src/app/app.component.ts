import { Component, ViewContainerRef, OnInit } from '@angular/core';
import { ModalService } from '@bmi/ui';
import { Subscription, combineLatest } from 'rxjs';
import { tap } from 'rxjs/operators';

import { PageConfig, PageRepositoryService } from '@bmi/core';
import { GravityService, GravityConfigService } from '@bmi/gravity-services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(
    viewContainerRef: ViewContainerRef,
    modalService: ModalService
  ) {
    modalService.setViewContainer(viewContainerRef);
  }

}
