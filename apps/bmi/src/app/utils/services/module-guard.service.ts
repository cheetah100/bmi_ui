import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, ParamMap, Params, Router } from '@angular/router';
import { Observable, combineLatest } from 'rxjs';
import { map, switchMap, take } from 'rxjs/operators';
import { RoutesService, BmiRoute, ModuleService } from '@bmi/core';
import { GravityApiApplication } from '@bmi/gravity-services';

@Injectable({
  providedIn: 'root'
})
export class ModuleGuard implements CanActivate {

  BUILTIN_MODULE_HOMEPAGES = new Map<string, string>([
    ['spc', '/spc/it-financial-summary'],
    ['service_performance', '/service_performance/action-center'],
    ['planning', '/planning/portfolio-prioritization'],
    ['operations', '/operations/dashboard']
  ]);

  constructor(
    private routesService: RoutesService,
    private moduleService: ModuleService,
    private router: Router,
  ) { }

  /**
   * for /{moduleId} , looks to module config for a default route
   * Return UrlTree for that default route
   */
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ) {
    const moduleId = route.params.moduleId as string;

    if (!moduleId) {
      return false;
    }

    return this.moduleService.getModule(moduleId).pipe(
      switchMap(module => module.defaultRoute.pipe(
        switchMap(route => this.routesService.resolveRoute(route, {moduleId, binder: null}))
      )),
      take(1),  // Needed, as Angular expects us to emit a single value.
      map(defaultRoute => {
        if (defaultRoute) {
          const routerLink = this.routesService.getRouterLink(defaultRoute) as any[];
          return this.router.createUrlTree(routerLink);
        } else if (this.BUILTIN_MODULE_HOMEPAGES.has(moduleId)) {
          return this.router.parseUrl(this.BUILTIN_MODULE_HOMEPAGES.get(moduleId));
        } else {
          return true;
        }
      })
    );
  }
}
