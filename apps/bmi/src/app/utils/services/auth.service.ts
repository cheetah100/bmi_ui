import { Injectable, Inject } from '@angular/core';
import { PreloadData, PRELOAD_DATA_PROMISE_TOKEN } from '../../../preload';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(
    @Inject(PRELOAD_DATA_PROMISE_TOKEN)
    private preloadDataPromise: Promise<PreloadData>
  ) {}

  getCurrentUser(): Observable<GravityUser> {
    return from(this.preloadDataPromise).pipe(
      map(value => {
        return value.user;
      })
    );
  }

  /**
   * Should we show the current user admin tools?
   *
   * NOTE: this method is for usability, not security. We don't want to show
   * regular users features that aren't relevant to them, or will just lead to
   * errors.
   *
   * Real security can only exist on trusted systems, a.k.a. on the backend.
   */
  shouldShowAdminTools(): Observable<boolean> {
    return this.getCurrentUser().pipe(
      map(user => Object.keys(user.teams).includes('administrators'))
    );
  }
}

export interface GravityUser {
  id: string;
  email: string;
  fullName: string;
  firstname: string;
  surname: string;
  teams: { [team: string]: string };
}

/*
{
    "id": "bmi_nprod_dev",
    "metadata": null,
    "name": "bmi_nprod_dev",
    "email": "na",
    "firstname": "BMI NonProd",
    "surname": "Developer",
    "passwordhash": null,
    "key": null,
    "teams": {
        "administrators": "ADMIN"
    },
    "fullName": "BMI NonProd Developer"
}*/
