import { InjectionToken } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { environment } from './environments/environment';

export interface PreloadData {
    user: {
        email: string,
        firstname: string,
        fullName: string,
        id: string,
        surname: string,
        teams: any
    },
    /*app: {
        id: string,
        name: string,
        modules: any,
        visualisation: any,
        permissions: any
    },*/
    apps: { [id: string]: string },
    /*widgets: {
        id: string,
        metadata?: any,
        name?: string,
        applicationId?: string,
        description?: string,
        group?: string,
        phase?: string,
        title?: string,
        widgetType: string,
        fields?: any
    }[]*/
}

async function getPreloadData(): Promise<PreloadData> {
    const responses = await Promise.all([
        fetch('/gravity/spring/user/current', {
            method: 'GET',
        }).then(response => {
          if(response.ok && response.headers.get('content-type').startsWith('application/json')) {
            return response.json();
          } else {
            throw new HttpErrorResponse({status: response.status, statusText: response.statusText });
          }
        }),
        fetch('/gravity/spring/app', {
            method: 'GET',
        }).then(response => {
          if(response.ok && response.headers.get('content-type').startsWith('application/json')) {
            return response.json();
          } else {
            throw new HttpErrorResponse({status: response.status, statusText: response.statusText });
          }
        })
    ]);

    return {
        user: await responses[0],
        apps: await responses[1]
    };
}

const preload = getPreloadData();

export const PRELOAD_DATA_PROMISE_TOKEN = new InjectionToken('Preload Data Promise Token', { providedIn: 'root', factory: () => preload });
