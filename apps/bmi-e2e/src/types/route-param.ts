export interface RouteParam  {
  endPoint: string | RegExp;
  fixture: string;
  alias:string;
}
