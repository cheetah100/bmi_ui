// import { RouteParam } from "../types/route-param";
import { DataServer, DataServerOptions, RouteParam } from '@bmi/cypress/mocking/data-server';
import { getMock, postMock } from '@bmi/cypress/helper-functions';

describe('BasicChartWidget in page', () => {
  let dataServer: DataServer;

  beforeEach(() => {

    // set the clock: stubbed chart data is for a specific year
    const date = new Date(Date.UTC(2021, 0, 9))
    cy.clock(date.getTime());
    console.info('Testing date set at:', date);

    const mockedRoutes: RouteParam[] = [
      getMock('/gravity/spring/app/testing_module/visualisation',
        'page',
        'visualisation.json'
      ),
      getMock('/gravity/spring/app/testing_module'),
      postMock(
        '/gravity/spring/board/quarter/search?order=startDate&view=all',
        'quarters-cards',
        '{"conditions":{}}'
      ),
      getMock('/gravity/spring/board/quarter/cards?order=startDate&descending=true',
        'quarters-cards-desc'
      ),
      postMock('/gravity/spring/board/test_kpi_data/search?order=_id&view=all',
        'card-data-cards',
        '{"conditions":{}}'
      ),
      getMock('/gravity/spring/board/test_kpi_data',
        'card-data',
      ),
      getMock('/gravity/spring/board/test_data_category/cards/list?allcards=true', 'card')
    ];

    const options: DataServerOptions = {
      basePath: 'basic-chart-widget',
      genericRoutes: ['app', 'current-user'] //, 'descriptions', 'current-user', 'configs']
    };
    dataServer = new DataServer(mockedRoutes, options);
    dataServer.waitforRoutesToBeMocked().subscribe(routes => {
      dataServer.readFixture('configs').then(configs => {
        window.sessionStorage.setItem('gravityCache:/gravity/spring/board/configs', JSON.stringify(configs))
      });
    });
    cy.visit('/testing_module/pages/page-basic-chart;metric=tdc1');
  });

  it('should display a chart based on the supplied config and data', () => {
    cy.get('[data-cy=widget-wrapper-title]').contains('Random metric graph');

    // using classes: an anti-pattern, but we don't have the ability to add data
    // attributes easily to highcharts generated content

    // yaxis label
    cy
      .get('[data-cy=chart]')
      .contains('.highcharts-yaxis', 'Metric Value');

    // chart starts on correct quarter given the current year
    cy
      .get('[data-cy=chart]')
      .get('.highcharts-xaxis-labels')
      .contains('tspan', 'Q1 FY2021');

    // chart has the correct number of points given card data
    cy
      .get('[data-cy=chart]')
      .get('.highcharts-point')
      .then(points => expect(points.length).to.equal(9));
  });

});
