import { getMock } from "@bmi/cypress/helper-functions";
import { RouteParam } from "@bmi/cypress/mocking/data-server";

export const M2E_SHARED_ROUTES: RouteParam[] = [
  getMock('/gravity/spring/app', 'app'),
  getMock(
    '/gravity/spring/board/quarter/cards?order=startDate&descending=true',
    'quarter_cards'
  ),
  getMock('/gravity/spring/app/m2e_onboarding', 'm2e_onboarding_app'),
  getMock(
    '/gravity/spring/app/m2e_onboarding/visualisation',
    'app_visualisation'
  ),
  getMock('/gravity/spring/user/current', 'current_user'),
  getMock('/gravity/spring/board/quarter/cards', 'all_quarter_cards'),
  getMock(
    '/gravity/spring/board/m2e_objectives/cards/list?allcards=true',
    'allcards_m2e_objectives'
  ),
  getMock(
    '/gravity/spring/board/m2e_entity_types/cards/list?allcards=true',
    'allcards_m2e_entity_types'
  ),
  getMock('/gravity/spring/board/m2e_objectives', 'board_m2e_objectives'),
  getMock('/gravity/spring/board/m2e_metrics', 'board_m2e_metrics'),
  getMock(
    '/gravity/spring/board/m2e_sources/cards/list?allcards=true',
    'allcards_m2e_sources'
  ),
  getMock(
    '/gravity/spring/board/metric_units/cards/list?allcards=true',
    'allcards_metric_units'
  ),
  getMock(
    '/gravity/spring/board/staff/cards/list?allcards=true',
    'allcards_staff'
  ),
  getMock(
    '/gravity/spring/board/m2e_aggregation_method/cards/list?allcards=true',
    'allcards_m2e_aggregation_method'
  ),
  getMock(
    '/gravity/spring/board/m2e_aggregation_resolutions/cards/list?allcards=true',
    'allcards_m2e_aggregation_resolutions'
  ),
  getMock('/gravity/spring/board/m2e_metrics/cards/list?allcards=true',
      'cardlist_m2e_metrics'),
];