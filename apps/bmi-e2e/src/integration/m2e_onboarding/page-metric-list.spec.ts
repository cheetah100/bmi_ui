import { dataCy } from '@bmi/cypress/common-element-actions';
import { getMock, postMock } from '@bmi/cypress/helper-functions';
import { DataServer, DataServerOptions } from '@bmi/cypress/mocking/data-server';
import { M2E_SHARED_ROUTES } from '../shared-routes';

describe('Module: m2e_onboarding | Page: Metric List', () => {

  let originalSearchRowsResponse;
  let searchRowsResponse;
  const routes = [
    ...M2E_SHARED_ROUTES,
    getMock('/gravity/spring/board/m2e_entity_types', 'board_m2e_entity_types'),
    postMock(
      '/gravity/spring/board/m2e_metrics/search?order=_id&view=all',
      'search_m2e_metrics',
      '{"conditions":{"_0":{"fieldName":"metadata.phase","operation":"EQUALTO","conditionType":"PROPERTY","value":"active|planning"}}}',
      () => [...searchRowsResponse],
    ),
    postMock(
      '/gravity/spring/board/m2e_entity_types/cards/cardlist',
      'cardlist_m2e_entity_types',
      '["application"]'
    ),
    postMock(
      '/gravity/spring/board/m2e_objectives/cards/cardlist',
      'cardlist_m2e_objectives',
      '["M2000041", "M2000015", "M2000014"]'
    ),
    postMock(
      './gravity/spring/board/m2e_metrics/cards/cardlist',
      'cardlist_m2e_metrics',
      '["M2000071"]'
    ),
    postMock(
      '/gravity/spring/board/m2e_exceptiontypes/search?order=_id&descending=true&view=all',
      'search_m2e_exceptiontypes',
      '',
      () => []
    ),
    postMock(
      '/gravity/spring/board/m2e_metric_data/search?order=_id&view=all',
      'search_m2e_metric_data',
      '',
      () => []
    ),
    postMock(
      '/gravity/spring/board/m2e_metrics/cards/M2000071/move',
      'm2e_metric_data_move_M2000071',
      '{"phase": "planning"}',
      () => searchRowsResponse[0],
    )
  ];
  const columnCount = 5;
  const rowCount = 3;
  const metricNames = [
    'Percent employees with super powers',
    'Happiness threshold',
    'Fancy dress ranking'
  ];
  const newName = 'Percent employees with super hats';


  beforeEach(() => {
    const options: DataServerOptions = {
      basePath: 'm2e_boarding/metric-list',
    };
    const dataServer = new DataServer(routes, options);
    dataServer.readFixture('search_m2e_metrics').then(rows => {
      originalSearchRowsResponse = [...rows];
      searchRowsResponse = [...rows];
    })
    cy.visit('/m2e_onboarding/pages/p_7GXRQWDC');
    cy.wait('@search_m2e_metrics');
  });

  it('Show the metric list page and populate elements correctly', () => {

    /**
     * Some of these use html structure and/or classes, which isn't ideal.
     * I.e. the page title was created using a markdown widget for this page,
     * a generic widget that can be anywhere and contain any dynamic elements.
     */

    // module title
    cy.get(dataCy('app-header')).contains('M2E Onboarding');

    // page title
    cy.get('.markdown-content h1').contains('Metric List');

    // table columns
    cy.get('.head-cell').should(ths => {
      expect(ths.length).to.equal(columnCount);
      expect(ths[1]).to.contain('Metric Name');
      expect(ths[2]).to.contain('Phase');
      expect(ths[3]).to.contain('Entity Type');
      expect(ths[4]).to.contain('Objective');
    });

    // row data: using grid so need to divide cells by columns
    cy.get('.body-cell').should(tds => {
      expect(tds.length).to.equal(rowCount * columnCount);
      expect(tds[1 + columnCount * 0]).to.contain(metricNames[0]);
      expect(tds[1 + columnCount * 1]).to.contain(metricNames[1]);
      expect(tds[1 + columnCount * 2]).to.contain(metricNames[2]);
    });

    // sort by metric name
    cy.get('.head-cell').eq(1).click();
    cy.get('.body-cell').should(tds => {
      expect(tds.length).to.equal(rowCount * columnCount);
      expect(tds[1 + columnCount * 0]).to.contain(metricNames[2]);
      expect(tds[1 + columnCount * 1]).to.contain(metricNames[1]);
      expect(tds[1 + columnCount * 2]).to.contain(metricNames[0]);
    });

    // filter to one objective type and check rows
    searchRowsResponse = [{...originalSearchRowsResponse[2]}];
    cy.get('ui-single-select').first().click();
    cy.get('.select-options__option').then(options => {
      options[4].click();
      cy.get('.body-cell').should(tds => {
        expect(tds.length).to.eql(columnCount);
      });
    });

    // open metric detail page for first table row (content tested as part of Pilars page)
    cy.get('.button-toolbar__button').first().click();
    /** 
     * Cypress is incorrectly finding this element hidden. the {force: true} overrides that.
     * Seems to be because of the complexites around many ancestors, some with position: relative
     * and some with display: hidden. Or, could be trying to click while animating the dropdown 
     * open. */
    cy.get('ui-dropdown-item').eq(1).click({force: true});
    cy.get('.markdown-content h1').contains('Metric Details');

  });

  it('Support card edit actions and then update the table data correctly', () => {

    // confirm existing name field
    cy.get('.body-cell').should(tds => {
      expect(tds[1]).to.contain(metricNames[0]);
    });

    // open actions dropdown on first item
    cy.get('.button-toolbar__button').first().click();
    // click edit, open form
    cy.get('ui-dropdown-item').first().click();

    // change name of metric
    cy
      .get(dataCy('form-control:text-input:Name'))
      .type('{backspace}{backspace}{backspace}{backspace}{backspace}{backspace}hats');

    // save
    cy.intercept(
      'PUT',
      '/gravity/spring/board/m2e_metrics/cards/M2000071',
      (req) => {
        expect(req.body.name).to.eql(newName);
        req.reply(req.body);
      }
    );
    cy.get(dataCy('form-control:button')).click();
    searchRowsResponse = [...originalSearchRowsResponse];
    searchRowsResponse[0].fields.name = newName;
    searchRowsResponse[0].title = newName;

    // check updated name in the correct table row
    cy.get('.body-cell').should(tds => {
      expect(tds[1]).to.contain(newName);
    });

    // edit phase modal functionality

    // open actions dropdown on first item
    cy.get('.button-toolbar__button').first().click();
    // change phase: open form
    cy.get('ui-dropdown-item').eq(2).click();

    // select new phase
    cy.get('bmi-phase-edit-form .ui-select-toggler').click();
    cy.get('bmi-phase-edit-form .select-options__option').eq(3).click();

    // save
    searchRowsResponse[0].phase = 'planning';
    cy.get('bmi-phase-edit-form button.btn--primary').click();

    // check phase status in table
    cy.get('.body-cell').should(tds => {
      expect(tds[2]).to.contain('planning');
    });

  });

});
