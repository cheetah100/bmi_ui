import {
  DataServer,
  DataServerOptions,
  RouteParam,
} from '@bmi/cypress/mocking/data-server';
import { getMock, postMock } from '@bmi/cypress/helper-functions';
import { M2E_SHARED_ROUTES } from '../shared-routes';
import { dataCy } from '@bmi/cypress/common-element-actions';

const specFixturesFolder = 'm2e_onboarding';

const specFixturesFolderPath = (fileName: string): string => {
  return `${specFixturesFolder}/${fileName}`;
};

const newMetric = {
  id: 'M2000065',
  template: 'm2e_metrics',
  creator: 'bmi_nprod_dev',
  created: 1614647463211,
  modifiedby: null,
  modified: null,
  color: null,
  fields: {
    name: 'Metric Test',
    entity_type: null,
    description: null,
    source_system: null,
    unit: null,
    metric_owner: null,
    aggregation_method: null,
    aggregation_resolution: null,
    data_access_detail: null,
    objective: 'M2000014',
    source_data_board: null,
    source_date_field: null,
    source_entity_field: null,
    source_value_field: null,
    filter: null,
  },
  board: 'm2e_metrics',
  title: 'Test 4',
  lock: null,
  phase: 'planning',
  deleted: false,
  alerted: false,
};

describe('Test Pillars page', () => {
  let server: DataServer;
  let search_m2e_metrics_content = [];
  const routes: RouteParam[] = [
    ...M2E_SHARED_ROUTES,
    getMock('/gravity/spring/board/configs', 'configs'),    
    postMock(
      '/gravity/spring/board/m2e_objectives/search?order=pillar&view=all',
      'search_all_m2e_objectives',
      '{"conditions":{}}'
    ),
    postMock(
      '/gravity/spring/board/m2e_pillars/search?order=_id&view=all',
      'search_pillars',
      '{"conditions":{}}'
    ),
    getMock('/gravity/spring/board/m2e_pillars', 'board_m2e_pillars'),
    postMock(
      '/gravity/spring/board/staff/cards/cardlist',
      'cardlist_staff',
      '["stevboyl"]'
    ),
    getMock('/gravity/spring/board/staff', 'board_staff'),
    postMock(
      '/gravity/spring/board/m2e_metrics/search?order=_id&view=all',
      'search_m2e_metrics',
      '{"conditions":{"_0":{"fieldName":"objective","operation":"EQUALTO","conditionType":"PROPERTY","value":"M2000014"}}}',
      () => {
        console.log('responding to metric search');
        console.log('smmc: ', search_m2e_metrics_content);
        return search_m2e_metrics_content;
      }
    ),
    postMock(
      '/gravity/spring/board/m2e_objectives/search?order=_id&view=all',
      'search_m2e_objectives',
      '{"conditions":{"_0":{"fieldName":"pillar","operation":"EQUALTO","conditionType":"PROPERTY","value":"M2000002"}}}'
    ),
    postMock(
      '/gravity/spring/board/m2e_exceptiontypes/search?order=_id&view=all',
      'search_m2e_exceptiontypes',
      '{"conditions":{"_0":{"fieldName":"metric.objective","operation":"EQUALTO","conditionType":"PROPERTY","value":"M2000014"}}}'
    ),
    postMock(
      '/gravity/spring/board/m2e_entity_types/cards/cardlist',
      'cardlist_m2e_entity_types',
      '["application"]'
    ),
    postMock('/gravity/spring/board/m2e_objectives/cards/cardlist',
      'cardlist_m2e_objectives',
      '["M2000015"]'
    ),
    getMock('/gravity/spring/board/m2e_pillars/cards/list?allcards=true',
      'cardlist_m2e_pillars'),
    postMock(
      '/gravity/spring/board/metric_units/cards/cardlist',
      'cardlist_metric_units',
      '["count"]'
    )
  ];

  const newMetricFieldName = newMetric.fields.name;

  const options: DataServerOptions = {
    basePath: 'm2e_boarding/pillars',
  };
  beforeEach(() => {
    server = new DataServer(routes, options);
    cy.visit('/m2e_onboarding/pages/p_FF9HKXRX');
  });

  it('should open Pillars page', () => {
    cy.contains('Pillars');

    // Check if the first entry is selected
    server.readFixture('search_m2e_objectives').then((objectives) => {
      server.readFixture('search_m2e_metrics').then((m2eMetrics) => {
        search_m2e_metrics_content = m2eMetrics;

        cy.get('.tabs-grid.nav-left .tab__heading').should(
          'have.length',
          objectives.length
        );

        cy.get('bmi-core-card > bmi-core-widget-wrapper')
          .contains('Objectives')
          .get(' .markdown-content > h3')
          .contains(objectives[0].title);

        cy.get('[title="Ease of Access"]').click();

        cy.get('bmi-core-card > bmi-core-widget-wrapper')
          .contains('Objectives')
          .get(' .markdown-content > h3')
          .contains(objectives[1].title);

        // });

        // it.only('should edit an objective', () => {
        server.readFixture('search_m2e_objectives').then((objectives) => {
          cy.get(dataCy('form-control:button'))
            .contains('Edit Objective')
            .click();
          const newDescription = 'New Description';
          cy.intercept(
            'PUT',
            '/gravity/spring/board/m2e_objectives/cards/M2000015',
            (req) => {
              expect(req.body.description).to.eql(newDescription);
              req.reply(req.body);
            }
          );

          cy.get(dataCy('form-control:text-input:Description')).type(
            newDescription
          );

          cy.get(dataCy('form-control:button')).contains('Save').click();
        });
        // });
        // it('should add a metric', () => {
        cy.get(dataCy('form-control:button')).contains('Add Metric').click();

        cy.intercept(
          'POST',
          '/gravity/spring/board/m2e_metrics/cards',
          (req) => {
            expect(req.body.fields.name).to.eql(newMetricFieldName);
            search_m2e_metrics_content.push(newMetric);
            req.reply(newMetric);
          }
        );

        cy.get(dataCy('form-control:text-input:Name')).type(newMetricFieldName);

        cy.get(dataCy('form-control:button')).contains('Save').click();
      });

      it('should open metric details', () => {
        cy.get('[data-table-col-index="1"]')
          .contains(newMetricFieldName)
          .click();

        cy.get('.navigation-list--breadcrumbs')
          .children()
          .should('have.length', 3);
        cy.get('.navigation-list--breadcrumbs')
          .children()
          .last()
          .contains('Metric Details');
      });
    });
  });

  it('should edit a metric', () => {
    cy.contains('Pillars');
    search_m2e_metrics_content.push(newMetric);
    server.readFixture('search_m2e_objectives').then((objectives) => {
      console.log('title: ', objectives[0].title);

      cy.get('.tabs-grid.nav-left .tab__heading').should(
        'have.length',
        objectives.length
      );
      const numOfColumns = 6;
      cy.get('bmi-core-card > bmi-core-widget-wrapper')
        .contains('Objectives')
        .get(' .markdown-content > h3')
        .contains(objectives[0].title)
        .get('[data-table-row-index="0"]')
        .should('have.length', numOfColumns);

      cy.get('[data-table-row-index="0"][data-table-col-index="1"]')
        .contains('Metric Test')
        .click();

      const metricName = 'm2e_test_app_metric3';
      cy.get('.ui-select-toggler__caret').click();

      cy.get('.select-options__option-list').contains(metricName).click();

      const title =
        'Count of records within an app more than 6 in last 90 days';
      // cy.get('[data-table-row-index="0"][data-table-col-index="1"]')
      //   .contains(title)
    });
  });
});
