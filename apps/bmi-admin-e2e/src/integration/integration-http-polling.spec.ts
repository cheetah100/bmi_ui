import {
  DataServer,
  DataServerOptions,
  RouteParam,
} from '@bmi/cypress/mocking/data-server';
import { getMock, postMock } from '@bmi/cypress/helper-functions';
import { assertInputEmpty, checkInputValue, dataCy, enterInputValue, formFieldSelector, getUiSelect, pickUiOption, selectInput, uiSelectContains } from '@bmi/cypress/common-element-actions';
import { post } from 'cypress/types/jquery';


describe('Integration editor, HTTP Polling', () => {

  let dataServer: DataServer;
  let state = 0;
  let integrationsList;
  let postedIntegration;
  let editedIntegration;

  const currentList = () => {
    switch (state) {
      case 1:
      case 2:
        const ret = [
          ...integrationsList,
          currentIntegration(),
        ];
        return ret;
      default:
        return [...integrationsList];
    }
  }

  const currentIntegration = () => {
    if (state === 2) {
      return {...editedIntegration};
    }
    return {...postedIntegration};
  }

  const dsOptions: DataServerOptions = {
    basePath: 'integration/edit',
    genericRoutes: ['app', 'descriptions', 'current-user', 'configs'],
    allCardNames: [],
  }

  const routes: RouteParam[] = [
    getMock('/gravity/spring/cred', 'all-credentials'),
    getMock(
      '/gravity/spring/integration',
      'all-integrations',
      null,
      () => currentList(),
    ),
    getMock(
      '/gravity/spring/integration/e2e_http_polling_test',
      'e2e-test-int-http',
      null,
      () => currentIntegration(),
    ),
    {
      endPoint: '/gravity/spring/integration/e2e_http_polling_test',
      alias: 'e2e-test-int-http-edit',
      method: 'PUT',
    },
    {
      endPoint: 'c',
      alias: 'e2e-test-int-http-edit',
      method: 'DELETE',
      content: () => null
    },
    postMock(
      '/gravity/spring/integration',
      'e2e-test-int-http-post',
      null,
      () => currentIntegration(),
    ),
    getMock('/gravity/spring/int-http-edit', 'e2e-test-int-http-edit'),
    postMock(
      'http://pdx-col.eum-appdynamics.com/eumcollector/beacons/browser/v2/AD-AAB-ABA-DVD/adrum',
      'adrum',
      null,
      () => ({})
    ),
    getMock(
      '/gravity/spring/integration/plugins',
      'all-integration-plugins',
    ),
    getMock('/gravity/spring/system/automation/plugins/metadata', 'metadata'),
  ];

  beforeEach(() => {
    dataServer = new DataServer(routes, dsOptions);
    dataServer.readFixture('all-integrations').then(list => integrationsList = list);
    dataServer.readFixture('e2e-test-int-http').then(i => postedIntegration = i);
    dataServer.readFixture('e2e-test-int-http-edit').then(i => editedIntegration = i);
    cy.viewport(1000, 1232);
    cy.visit('/system/integrations/');
  });

  it('should allow creation of a new http polling integration with metricifier action, then viewing config, then editing, then deleting.', () => {

    // navigate to integration creation view
    cy.get(dataCy('integrations-create-button')).click({force: true});

    // save form without sufficient / valid config: should get 500 error notification
    // TODO: we need to add the ability to return different response codes to DataServer

    // check instructions are showing
    cy.get('bmi-help-box h4').contains('Integrations');
    cy.get('bmi-help-box h4').contains('Schedule');

    // name, id, type fields are available but empty
    assertInputEmpty('Name');
    assertInputEmpty('Integration ID');
    uiSelectContains(formFieldSelector('Integration Type'), '-');

    // actions list is empty
    cy.get('bmi-action-editor').should('not.exist');

    // select http polling integration
    pickUiOption(formFieldSelector('Integration Type'), 'Polling HTTP Call');

    // fill out form (resource editing tested elsewhere)
    const piConf = postedIntegration.config;
    enterInputValue('Name', postedIntegration.name);
    enterInputValue('Integration ID', postedIntegration.id);
    enterInputValue('URL to call', piConf.url);
    enterInputValue('Method Type', piConf.type);
    pickUiOption(formFieldSelector('Credential'), 'string');
    enterInputValue('Number of records for each call. Insert {limit} into url.', piConf.records);
    enterInputValue('Name of field that specifies Total Records. Insert {offset} into url.', piConf.total_field);
    enterInputValue('Location of Dataset', piConf.list);
    enterInputValue('Comma delimited set of fields for list data', piConf.fields);

    // set schedule
    cy.get('bmi-ui-cron-editor span.ui-icon').click();
    cy.get(`${formFieldSelector('Minute')} input[type=number]`).clear().type('20');
    cy.get(`${formFieldSelector('Hour')} input[type=number]`).clear().type('16');
    pickUiOption(formFieldSelector('Day of week'), 'Every day');

    // add metricifier plugin
    cy.get('button').contains('Add Action').click();
    cy.get('ui-dropdown-item').contains('Metric Generation').click({force: true});
    const metConf = postedIntegration.actions.metricifier_MV4V.config;

    // date and value fields should be inputs before a board context is selected
    selectInput('Date Field in Source Data');
    selectInput('Value Field in Source Data');

    // board context
    pickUiOption(formFieldSelector('Source of Data'), metConf.source_data);

    // date and value fields: should now be selects
    pickUiOption(formFieldSelector('Date Field in Source Data'), 'source data creation date');
    pickUiOption(formFieldSelector('Value Field in Source Data'), 'value');

    // fill out remainder of plugin
    pickUiOption(formFieldSelector('Target Destination Metric Data Board'), 'M2E Metrics');

    cy.get(formFieldSelector('Entity Field to group by in Source Data'))
      .contains('Add')
      .click()
      .click();
    getUiSelect(
      formFieldSelector('Entity Field to group by in Source Data')
    ).eq(0).click();
    cy.get(formFieldSelector('Entity Field to group by in Source Data'))
      .contains('source data creation date').click();
    getUiSelect(
      formFieldSelector('Entity Field to group by in Source Data')
    ).eq(1).click();
    cy.get(formFieldSelector('Entity Field to group by in Source Data'))
      .contains('source data status').click();

    cy.get(formFieldSelector('Filter')).contains('Add').click().click().click().click().click();
    const filterValues = '0#1w2a3t4?';
    for (let c = 0, l = filterValues.length; c < l; c++) {
      selectInput('Filter').eq(c).type(filterValues.charAt(c));
    }

    enterInputValue('Aggregation Method', metConf.aggregation_method);
    enterInputValue('Aggregation Resolution', metConf.aggregation_resolution);
    enterInputValue('Value Field in Source Data to Aggregate Total Value used for Percentage or Ratio', metConf.denominator_total_value_field);
    enterInputValue('Filter in Source Data to Aggregate Total Value used for Ratio', metConf.denominator_total_value_filter);

    // save
    cy.get(dataCy('btn-save')).click();
    // cancel save: form should be in same state as before
    cy.get('mat-dialog-container button').contains('Cancel').click();
    // really save
    cy.get(dataCy('btn-save')).click().then(() => state = 1);
    cy.get('mat-dialog-container button').contains('Save').click();

    // new integration should exist list. Click to edit
    cy.contains('ui-row-with-menu', 'E2E HTTP Polling Test').click();

    // correct integration config is showing
    cy.get('h1').contains(`Edit Integration: ${postedIntegration.name}`);

    // check integration config in edit mode
    checkInputValue('Name', postedIntegration.name);
    checkInputValue('Integration ID', postedIntegration.id);
    uiSelectContains(formFieldSelector('Integration Type'), 'Polling HTTP Call');
    checkInputValue('URL to call', piConf.url);
    checkInputValue('Method Type', piConf.type);
    uiSelectContains(formFieldSelector('Credential'), 'string');
    checkInputValue('Number of records for each call. Insert {limit} into url.', piConf.records);
    checkInputValue('Name of field that specifies Total Records. Insert {offset} into url.', piConf.total_field);
    checkInputValue('Location of Dataset', piConf.list);
    checkInputValue('Comma delimited set of fields for list data', piConf.fields);
    cy.get('bmi-ui-cron-editor .alert__message').contains('Schedule: every day, at 4:20pm.');

    // 1 action exists
    cy.get('bmi-action-editor').should('have.length', 1);

    // metricifier is stored and displayed correctly
    cy.get('.action-editor__header').contains('Metric Generation');
    uiSelectContains(formFieldSelector('Source of Data'), 'm2e_test_sourcedata_board');
    uiSelectContains(formFieldSelector('Target Destination Metric Data Board'), 'M2E Metrics');
    uiSelectContains(formFieldSelector('Date Field in Source Data'), 'source data creation date');
    uiSelectContains(formFieldSelector('Value Field in Source Data'), 'value');
    getUiSelect(formFieldSelector('Entity Field to group by in Source Data'))
      .should('have.length', 2);
    getUiSelect(formFieldSelector('Entity Field to group by in Source Data'))
      .eq(0)
      .contains('source data creation date');
    getUiSelect(formFieldSelector('Entity Field to group by in Source Data'))
      .eq(1)
      .contains('source data status');
    // Filter map values
    for (let c = 0, l = filterValues.length; c < l; c++) {
      selectInput('Filter').eq(c).should('have.value', filterValues.charAt(c));
    }
    checkInputValue('Aggregation Method', metConf.aggregation_method);
    checkInputValue('Aggregation Resolution', metConf.aggregation_resolution);
    checkInputValue('Value Field in Source Data to Aggregate Total Value used for Percentage or Ratio', metConf.denominator_total_value_field);
    checkInputValue('Filter in Source Data to Aggregate Total Value used for Ratio', metConf.denominator_total_value_filter);

    // make edits to integration config
    enterInputValue('Name', editedIntegration.name);
    enterInputValue('Number of records for each call. Insert {limit} into url.', editedIntegration.config.records);

    // remove metricifier
    cy.get('ui-stacker-list button.stacker-box-button.delete').eq(0).click();
    // check deleted
    cy.get('bmi-action-editor').should('not.exist');

    // save edits, return to index page
    cy.get(dataCy('btn-save')).click().then(() => state = 2);
    cy.get('mat-dialog-container button').contains('Save').click();

    // check updated name in list
    cy.contains('ui-row-with-menu', editedIntegration.name);

    // delete integration
    cy.contains('ui-row-with-menu', editedIntegration.name)
      .find('button')
      .click({force: true});
    cy.get('ui-dropdown-item').contains('Delete Integration')
      .click({force: true})
      .then(() => state = 3);
    cy.get(dataCy('confirmation-modal-confirm-button')).click();

    // list is updated with removed integration
    cy.contains('ui-row-with-menu', editedIntegration.name)
      .should('not.exist');

  });

});
