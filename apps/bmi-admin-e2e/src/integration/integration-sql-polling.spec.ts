import {
  DataServer,
  DataServerOptions,
  RouteParam,
} from '@bmi/cypress/mocking/data-server';
import { getMock, postMock } from '@bmi/cypress/helper-functions';
import { assertInputEmpty, checkInputValue, dataCy, enterInputValue, formFieldSelector, getUiSelect, pickUiOption, selectInput, uiSelectContains } from '@bmi/cypress/common-element-actions';


describe('Integration editing, JDBC Polling', () => {

  let dataServer: DataServer;
  let state = 0;
  let integrationsList;
  let postedIntegration;
  let editedIntegration;

  const currentList = () => {
    switch (state) {
      case 1:
      case 2:
        const ret = [
          ...integrationsList,
          currentIntegration(),
        ];
        return ret;
      default:
        return [...integrationsList];
    }
  }

  const currentIntegration = () => {
    if (state === 2) {
      return {...editedIntegration};
    }
    return {...postedIntegration};
  }

  const dsOptions: DataServerOptions = {
    basePath: 'integration/edit',
    genericRoutes: ['app', 'descriptions', 'current-user', 'configs'],
    allCardNames: [],
  }

  const routes: RouteParam[] = [
    getMock('/gravity/spring/cred', 'all-credentials'),
    getMock(
      '/gravity/spring/integration',
      'all-integrations',
      null,
      () => currentList(),
    ),
    getMock(
      '/gravity/spring/integration/e2e_sql_polling_test',
      'e2e-test-int-sql',
      null,
      () => currentIntegration(),
    ),
    {
      endPoint: '/gravity/spring/integration/e2e_sql_polling_test',
      alias: 'e2e-test-int-sql-edit',
      method: 'PUT',
    },
    {
      endPoint: '/gravity/spring/integration/e2e_sql_polling_test',
      alias: 'e2e-test-int-sql-edit',
      method: 'DELETE',
      content: () => null
    },
    postMock(
      '/gravity/spring/integration',
      'e2e-test-int-sql',
      null,
      () => currentIntegration(),
    ),
    getMock('/gravity/spring/int-sql-edit', 'e2e-test-int-sql-edit'),
    postMock(
      'http://pdx-col.eum-appdynamics.com/eumcollector/beacons/browser/v2/AD-AAB-ABA-DVD/adrum',
      'adrum',
      null,
      () => ({})
    ),
    getMock(
      '/gravity/spring/integration/plugins',
      'all-integration-plugins',
    ),
    getMock('/gravity/spring/system/automation/plugins/metadata', 'metadata'),
    getMock('/gravity/spring/board/asg_metrics/resources', 'asg_metrics_resources'),
  ];

  beforeEach(() => {
    dataServer = new DataServer(routes, dsOptions);
    dataServer.readFixture('all-integrations').then(list => integrationsList = list);
    dataServer.readFixture('e2e-test-int-sql').then(i => postedIntegration = i);
    dataServer.readFixture('e2e-test-int-sql-edit').then(i => editedIntegration = i);
    cy.viewport(1000, 1232);
    cy.visit('/system/integrations/');
  });

  it('should allow creation of a new sql polling integration with metricifier action, then viewing config, then editing, then deleting.', () => {
    
    // navigate to integration creation view
    cy.get(dataCy('integrations-create-button')).click({force: true});

    // check instructions are showing
    cy.get('bmi-help-box h4').contains('Integrations');
    cy.get('bmi-help-box h4').contains('Schedule');
  
    // name, id, type fields are available but empty
    assertInputEmpty('Name');
    assertInputEmpty('Integration ID');
    uiSelectContains(formFieldSelector('Integration Type'), '-');

    // actions list is empty
    cy.get('bmi-action-editor').should('not.exist');

    // select sql polling integration
    pickUiOption(formFieldSelector('Integration Type'), 'Polling SQL Database');

    // fill out form
    enterInputValue('Name', postedIntegration.name);
    enterInputValue('Integration ID', postedIntegration.id);
    pickUiOption(formFieldSelector('Board containing Query Resource'), 'ASG Metric');
    pickUiOption(formFieldSelector('Query Resource '), postedIntegration.config.resource);
    pickUiOption(formFieldSelector('Credential'), 'string');

    // // add create card action
    cy.get('button').contains('Add Action').click();
    cy.get('ui-dropdown-item').contains('Create Card').click({force: true});

    // configure action
    pickUiOption(formFieldSelector('Target Board to create card on'), 'Test'); 
    const listLabel = formFieldSelector('Fields with map or collection of data to store.');
    cy.get(listLabel).contains('Add').click().click();
    getUiSelect(listLabel).eq(0).click();
    cy.get(listLabel).contains('Created').click();
    getUiSelect(listLabel).eq(1).click();
    cy.get(listLabel).contains('Trigger Date').click();

    const mapLabel = 'Fields mappings. Leave empty to store all.';
    const mapSelector = formFieldSelector(mapLabel);
    cy.get(mapSelector).contains('Add').click().click();
    selectInput(mapLabel).eq(0).type('title');
    selectInput(mapLabel).eq(1).type('start_date');
    getUiSelect(mapSelector).eq(0).click();
    cy.get(mapSelector).contains('Name').click();
    getUiSelect(mapSelector).eq(1).click();
    cy.get(mapSelector).contains('Trigger Date').click();    

    // save
    cy.get(dataCy('btn-save')).click().then(() => state = 1);
    cy.get('mat-dialog-container button').contains('Save').click();

    // new integration should exist list. Click to edit
    cy.contains('ui-row-with-menu', postedIntegration.name).click();

    // correct integration config is showing
    cy.get('h1').contains(`Edit Integration: ${postedIntegration.name}`);

    // check integration config in edit mode
    checkInputValue('Name', postedIntegration.name);
    checkInputValue('Integration ID', postedIntegration.id);
    uiSelectContains(formFieldSelector('Board containing Query Resource'), 'ASG Metric');
    uiSelectContains(formFieldSelector('Query Resource '), postedIntegration.config.resource);
    uiSelectContains(formFieldSelector('Credential'), 'string');

    // 1 action exists
    cy.get('bmi-action-editor').should('have.length', 1);

    // action is stored and displayed correctly
    uiSelectContains(formFieldSelector('Target Board to create card on'), 'Test');
    getUiSelect(listLabel).eq(0).contains('Created');
    getUiSelect(listLabel).eq(1).contains('Phase');
    getUiSelect(mapSelector).eq(0).contains('Name');
    selectInput(mapLabel).eq(0).should('have.value', 'title');
    getUiSelect(mapSelector).eq(1).contains('Trigger Date');
    selectInput(mapLabel).eq(1).should('have.value', 'start_date');


    // make edits to integration config
    enterInputValue('Name', editedIntegration.name);
    pickUiOption(formFieldSelector('Query Resource '), editedIntegration.config.resource);
    cy.get('body').click();
    cy.get(`${listLabel} button.delete`).eq(0).click();

    // save edits, return to index page
    cy.get(dataCy('btn-save')).click().then(() => state = 2);
    cy.get('mat-dialog-container button').contains('Save').click();

    // check updated name in list
    cy.contains('ui-row-with-menu', editedIntegration.name);
    
    // return to edit, check edits are persisted
    cy.contains('ui-row-with-menu', editedIntegration.name).click();
    cy.get('h1').contains(`Edit Integration: ${editedIntegration.name}`);
    checkInputValue('Name', editedIntegration.name);
    uiSelectContains(formFieldSelector('Query Resource '), editedIntegration.config.resource);
    getUiSelect(listLabel).should('have.length', 1);

    // back to index
    cy.get(dataCy('btn-cancel')).click();

    // delete integration
    cy.contains('ui-row-with-menu', editedIntegration.name)
      .find('button')
      .click({force: true});
    cy.get('ui-dropdown-item').contains('Delete Integration')
      .click({force: true})
      .then(() => state = 3);
    cy.get(dataCy('confirmation-modal-confirm-button')).click();

    // list is updated with removed integration
    cy.contains('ui-row-with-menu', editedIntegration.name)
      .should('not.exist');

  });

});
