import {
  DataServer,
  DataServerOptions,
  RouteParam,
} from '@bmi/cypress/mocking/data-server';
import { getMock, postMock } from '@bmi/cypress/helper-functions';
import { assertInputEmpty, checkInputValue, dataCy, enterInputValue, formFieldSelector } from '@bmi/cypress/common-element-actions';


describe('Credential edit tools', () => {

  let dataServer: DataServer;

  /**
   * This feels clumsy, but I haven't found a better way to return different response
   * data for the same GET request
   * 0: init
   * 1: created new credential
   * 2: edited credential
   * 3: deleted credential
   */
  let state = 0;

  let credentialsList;
  let postedCredential;
  let editedCredential;
  const currentList = () => {
    switch (state) {
      case 1:
      case 2:
        const ret = {
          ...credentialsList,
          [currentCredential().id]: currentCredential().name,
        };
        return ret;
      default:
        return {...credentialsList};
    }
  }
  const currentCredential = () => {
    if (state === 2) {
      return {...editedCredential};
    }
    return {...postedCredential};
  }

  const dsOptions: DataServerOptions = {
    basePath: 'integration/edit',
    genericRoutes: ['app', 'descriptions', 'current-user', 'configs'],
  }

  const routes: RouteParam[] = [
    getMock('/gravity/spring/integration', 'all-integrations'),
    getMock(
      '/gravity/spring/cred',
      'all-credentials',
      null,
      () => currentList(),
    ),
    getMock(
      '/gravity/spring/cred/e2e_test_cred',
      'e2e-test-credential',
      null,
      () => currentCredential(),
    ),
    {
      endPoint: '/gravity/spring/cred/e2e_test_cred',
      alias: 'e2e-test-credential-edit',
      method: 'PUT',
    },
    {
      endPoint: '/gravity/spring/cred/e2e_test_cred',
      alias: 'e2e-test-credential-edit',
      method: 'DELETE',
      content: () => null
    },
    postMock(
      '/gravity/spring/cred',
      'e2e-test-credential',
      null,
      () => currentCredential(),
    ),
    getMock('/gravity/spring/cred-edit', 'e2e-test-credential-edit'),
    postMock(
      'http://pdx-col.eum-appdynamics.com/eumcollector/beacons/browser/v2/AD-AAB-ABA-DVD/adrum',
      'adrum',
      null,
      () => ({})
    )
  ];

  beforeEach(() => {
    dataServer = new DataServer(routes, dsOptions);
    dataServer.readFixture('all-credentials').then(list => credentialsList = list);
    // dataServer.readFixture('e2e-test-credential').then(cred => {
    //   console.log(`e2e-test-credential done`)
    //   postedCredential = cred
    // });
    // dataServer.readFixture('e2e-test-credential-edit').then(cred => editedCredential = cred);
    cy.visit('/system/integrations/');
  });

  it.only('should allow creation of a new credential, then viewing config, then editing, then deleting.', () => {
    // Make sure the cred to be added does not exists
    cy
      .get('div h1')
      .contains('Integration Credentials')
      .get('ui-row-with-menu')
      .contains('E2E Test Credential')
      .should('have.length', 0)

    // navigate to credential creation view
    cy.get(dataCy('integrations-create-credential-button')).click({force: true});

    // all controls should be showing, with empty values
    // indentifier / secret checkboxes should be disabled and checked
    assertInputEmpty('Credential ID');
    assertInputEmpty('Credential Name');
    cy.get(`${formFieldSelector('Credential Description')} textarea`)
      .should('be.empty');
    assertInputEmpty('Credential Resource');
    assertInputEmpty('Credential Method');
    assertInputEmpty('Credential Request Method');
    cy.get(`${dataCy('credential-editor-identifier-checkbox')} span.ui-icon`)
      .should('have.class', 'disabled')
      .should('have.class', 'ui-icon-check-square');
    assertInputEmpty('Credential Identifier');
    cy.get(`${dataCy('credential-editor-secret-checkbox')} span.ui-icon`)
      .should('have.class', 'disabled')
      .should('have.class', 'ui-icon-check-square');
    assertInputEmpty('Credential Secret');

    // save button should be disabled until required fields are valid
    cy.get(dataCy('credential-editor-save-button')).should('be.disabled');

    // enter required values, with invalid id
    enterInputValue('Credential ID', 'e2e-test-cred');
    enterInputValue('Credential Name', 'E2E Test Credential');
    enterInputValue('Credential Resource', 'e2e-test-resource-file.js');
    enterInputValue('Credential Identifier', 'e2e-test-identifier-value');
    enterInputValue('Credential Secret', 'e2e-test-secret-value');

    // save button should remain disabled with invalid id
    cy.get(dataCy('credential-editor-save-button')).should('be.disabled');

    // change to valid id
    enterInputValue('Credential ID', 'e2e_test_cred');

    // save button should now be enabled
    cy.get(dataCy('credential-editor-save-button')).should('be.enabled');

    // enter remaining values
    cy.get(`${formFieldSelector('Credential Description')} textarea`)
      .type('Credential for e2e tests.');
    enterInputValue('Credential Method', 'basic');
    enterInputValue('Credential Request Method', 'GET');

    // save credential, navigate to list page
    cy.get(dataCy('credential-editor-save-button'))
      .should('be.enabled')
      .click()
      .then(() => state = 1);
    cy.get(dataCy('confirmation-modal-confirm-button')).click();

    // newly created credential should show in list. Select to edit again
    cy.contains('ui-row-with-menu', 'E2E Test Credential').click();

    // check all config is displayed correctly.
    // Checkboxes should be enabled, checked
    // hidden values should show security message
    checkInputValue('Credential ID', 'e2e_test_cred');
    checkInputValue('Credential Name', 'E2E Test Credential');
    cy.get(`${formFieldSelector('Credential Description')} textarea`)
      .should('have.value', 'Credential for e2e tests.');
    checkInputValue('Credential Resource', 'e2e-test-resource-file.js');
    checkInputValue('Credential Method', 'basic');
    checkInputValue('Credential Request Method', 'GET');
    cy.get(`${dataCy('credential-editor-identifier-checkbox')} span.ui-icon`)
      .should('not.have.class', 'disabled')
      .should('have.class', 'ui-icon-check-square');
    checkInputValue('Credential Identifier', '<removed for security purposes>');
    cy.get(`${dataCy('credential-editor-secret-checkbox')} span.ui-icon`)
      .should('not.have.class', 'disabled')
      .should('have.class', 'ui-icon-check-square');
    checkInputValue('Credential Secret', '<removed for security purposes>');

    // save button should be enabled
    cy.get(dataCy('credential-editor-save-button')).should('be.enabled');

    // make edits and save
    enterInputValue('Credential Name', 'E2E Test Credential (edited)');
    cy.get(`${formFieldSelector('Credential Description')} textarea`)
      .clear()
      .type('Credential for e2e tests.');
    cy.get(dataCy('credential-editor-save-button'))
      .should('be.enabled')
      .click()
      .then(() => state = 2);
    cy.get(dataCy('confirmation-modal-confirm-button')).click();

    // Edited title should show on index page
    cy.contains('ui-row-with-menu', 'E2E Test Credential (edited)');

    // Delete credential
    cy.contains('ui-row-with-menu', 'E2E Test Credential (edited)')
      .find('button')
      .click({force: true});

    // Delete Credential
    cy.contains('ui-dropdown-item', 'Delete Credential')
      .click({force: true})
      .then(() => state = 3);
    cy.get(dataCy('confirmation-modal-confirm-button')).click();

    // check removed from credentials list
    cy.get('ui-row-with-menu')
    cy.contains('ui-row-with-menu', 'E2E Test Credential (edited)')
      .should('not.exist');

  });

});
