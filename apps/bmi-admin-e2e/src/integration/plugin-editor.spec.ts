import { DataServer, DataServerOptions, RouteParam } from '@bmi/cypress/mocking/data-server';
import { getMock, postMock } from '@bmi/cypress/helper-functions';
import { PLUGIN_STARTER_RULE } from './rules-json';
import { cloneDeep } from 'lodash-es';
import { dataCy, getUiSelect, pickUiOption, uiSelectContains } from '@bmi/cypress/common-element-actions';


const boardName = 'Dick\'s Read Only Field Test';
const boardNameParam = 'dick_s_read_only_field_test';
const backend = '/gravity/spring';
const ruleUrl = 'e2e_plugin_testing_rule';

const saveRuleAndConfirm = () => {
  cy.get(dataCy('rule-builder-save-button')).should('be.enabled').click();

  // Rule danger message
  cy.get('gravity-rule-save-modal h2')
    .should('be.visible')
    .contains('Confirm save rule?');
  // Confirm and save
  cy.get('gravity-rule-save-modal button')
    .contains('Save Rule')
    .should('be.enabled')
    .click();

  // Navigated to board page after save
  cy.get('.board-summary__layout');
}

const updatedConfig = (config, partial = {}) => {
  const whole = cloneDeep(config);
  const part = cloneDeep(partial);
  return {
    ...whole,
    ...part
  }
}

const navigateToRule = () => cy.visit(`/board/${boardNameParam}/rules/${ruleUrl}`, {});


describe('Plugin Editor:', () => {

  let savedRule;
  const getSavedRule = () => savedRule;
  const mockedRoutes:RouteParam[] = [
    getMock(
      `${backend}/app`,
      'loadApp',
      'app.json'
    ),
    {
      endPoint: `${backend}/board/${boardNameParam}/rules/full`,
      alias: 'allRules',
      enabled: true
    },
    getMock(
      `${backend}/board/${boardNameParam}/resources`,
      'resources'
    ),
    getMock(
      `${backend}/user/current`,
      'current'
    ),
    {
      endPoint: `${backend}/board/configs`,
      fixture: 'configs.json',
      alias: 'configs',
      enabled: false
    },
    getMock(`${backend}/app/descriptions`, 'descriptions'),
    getMock(
      `${backend}/board/${boardNameParam}/rules/${ruleUrl}`,
      'pluginTestingRule',
      null,
      () => savedRule
    ),
    getMock(`${backend}/system/automation/plugins/metadata`, 'metadata'),
    postMock(
      `${backend}/board/${boardNameParam}/rules`,
      ruleUrl,
      JSON.stringify(savedRule),
      () => getSavedRule()
    ),
  ];

  let server:DataServer;

  beforeEach(() => {
    const options: DataServerOptions = {
      basePath: 'rule-editor',
      genericRoutes: ['app', 'descriptions', 'current-user', 'configs']
    };
    server = new DataServer(mockedRoutes, options);
    server.readFixture('configs').then(configs => {
      window.sessionStorage.setItem(
        `gravityCache:${backend}/board/configs`,
        JSON.stringify(configs)
      )
    })
    cy.viewport(1000, 1232);
    savedRule = updatedConfig(PLUGIN_STARTER_RULE);
    navigateToRule();
    cy.wait('@pluginTestingRule');
  });

  describe('Change Phase', () => {

    it('should create a change phase action, save, check, edit plugins, save, check again', () => {

      const moveConfig = (phase: string) => ({
        "id": "move_test",
        "type": "move",
        "order": 0,
        "config": {
          "method": "",
          "parameters": [],
          "resource": phase,
          "response": "",
          "properties": {}
        },
        "name": null,
        "description": null
      });

      // rule edit view is rendered
      cy.get('h1').contains('E2E Plugin Testing Rule');

      // Add action
      cy.contains('Add Action').click();
      cy.contains('Change Phase').should('be.visible').click();
      pickUiOption(dataCy('ui-form-field-Phase to move to'), 'Archive');

      // Save and navigate to board summary
      cy.get(dataCy('rule-builder-save-button')).should('be.enabled').then(() => {
        savedRule = updatedConfig(
          savedRule,
          {
            actions: {
              ...savedRule.actions,
              move_test: moveConfig('archive'),
            }
          },
        );
      });
      saveRuleAndConfirm();

      // return to the rule edit page
      navigateToRule();

      // check saved config for fields is diplayed correctly
      cy.get('bmi-action-editor-list bmi-plugin-form').should('have.length', 2);
      uiSelectContains(dataCy('ui-form-field-Phase to move to'), 'Archive');

      // Edit plugins: delete first plugin, edit phase, add another move plugin
      cy.get('bmi-action-editor-list .ui-icon-trash').eq(0).click();
      pickUiOption(dataCy('ui-form-field-Phase to move to'), 'Current');
      cy.contains('Add Action').click();
      cy.get('.ui-dropdown').contains('Change Phase').should('be.visible').click();
      getUiSelect(dataCy('ui-form-field-Phase to move to')).eq(1).click();
      cy.get(`${dataCy('ui-form-field-Phase to move to')}`).contains('STR').click();

      // save
      cy.get(dataCy('rule-builder-save-button')).should('be.enabled').then(() => {
        savedRule = updatedConfig(
          savedRule,
          {
            actions: {
              move_test: moveConfig('current'),
              move_2: moveConfig('str')
            }
          },
        );
      });
      saveRuleAndConfirm();

      // check saved
      navigateToRule();

      // check saved config for fields is diplayed correctly
      cy.get('bmi-action-editor-list bmi-plugin-form').should('have.length', 2);
      getUiSelect(dataCy('ui-form-field-Phase to move to')).eq(0).contains('Current');
      getUiSelect(dataCy('ui-form-field-Phase to move to')).eq(1).contains('STR');

    });

  });

  describe('Card Search', () => {

    it('should create a card search action, save, check', () => {
      // rule edit view is rendered
      cy.get('h1').contains('E2E Plugin Testing Rule');

      // Add action
      cy.contains('Add Action').click();
      cy.contains('Card Search').should('be.visible').click();
      pickUiOption(dataCy('ui-form-field-Target Board to Search'), boardName);
      pickUiOption(dataCy('ui-form-field-Field in Local Card to use for search'), 'DDay');
      cy.get(
        `${dataCy('ui-form-field-Field in the Target Board to search in')} input`
      ).type('Target field');
      cy.get(`${dataCy('ui-form-field-View to apply')} input`).type('Apply view');
      cy.get(`${dataCy('ui-form-field-Field name to order results by')} input`)
        .type('Order field');
      cy.get(`${dataCy('ui-form-field-Sort descending')} input`).click();

      // Save and navigate to board summary
      savedRule = updatedConfig(
        savedRule,
        {
          actions: {
            ...savedRule.actions,
            "cardsearch_test": {
              "id": "cardsearch_test",
              "type": "cardsearch",
              "order": 1,
              "config": {
                  "method": "",
                  "parameters": [],
                  "resource": "dick_s_read_only_field_test",
                  "response": "",
                  "properties": {},
                  "localfield": "dday",
                  "remotefield": "Target field",
                  "view": "Apply view",
                  "order": "Order field",
                  "descending": true
              },
              "name": null,
              "description": null
            }
          },
        },
      );
      saveRuleAndConfirm();

      // return to the rule edit page
      navigateToRule();

      // check saved config for fields is diplayed correctly
      cy.get('bmi-action-editor-list bmi-plugin-form').should('have.length', 2);
      uiSelectContains(dataCy('ui-form-field-Target Board to Search'), boardName);
      uiSelectContains(dataCy('ui-form-field-Field in Local Card to use for search'), 'DDay');
        cy.get(`${dataCy('ui-form-field-Field in the Target Board to search in')} input`)
          .should('have.value', 'Target field');
        cy.get(`${dataCy('ui-form-field-View to apply')} input`)
          .should('have.value', 'Apply view');
        cy.get(`${dataCy('ui-form-field-Field name to order results by')} input`)
          .should('have.value', 'Order field');
        cy.get(`${dataCy('ui-form-field-Sort descending')} input`).should('be.checked');

    });

  });

  describe('Create Card', () => {

    // Fields with map or collection of data to store.
    // Fields mappings. Leave empty to store all.

    it('should create a create card action, save, check', () => {
      // rule edit view is rendered
      cy.get('h1').contains('E2E Plugin Testing Rule');

      // Add action
      cy.contains('Add Action').click();
      cy.contains('Create Card').should('be.visible').click();
      pickUiOption(dataCy('ui-form-field-Target Board to create card on'), boardName);
      cy.get(dataCy('ui-form-field-Fields with map or collection of data to store.'))
        .contains('Add')
        .click()
        .click();
      getUiSelect(
        dataCy('ui-form-field-Fields with map or collection of data to store.')
      ).eq(0).click();
      cy.get(dataCy('ui-form-field-Fields with map or collection of data to store.'))
        .contains('DDay').click();
      getUiSelect(
        dataCy('ui-form-field-Fields with map or collection of data to store.')
      ).eq(1).click();
      cy.get(dataCy('ui-form-field-Fields with map or collection of data to store.'))
        .contains('Test ID')
        .scrollIntoView()
        .should('be.visible').click();
      cy.get(dataCy('ui-form-field-Fields mappings. Leave empty to store all.'))
        .contains('Add')
        .click();
      pickUiOption(dataCy('ui-form-field-Fields mappings. Leave empty to store all.'), 'Card ID');
      cy.get(
        `${dataCy('ui-form-field-Fields mappings. Leave empty to store all.')} .map-editor__grid > input`
      ).type('Icicles: Test icicles.')

      // Save and navigate to board summary
      savedRule = updatedConfig(
        savedRule,
        {
          actions: {
            ...savedRule.actions,
            "createcard_test": {
              "id": "createcard_test",
              "type": "createcard",
              "order": 1,
              "config": {
                "method": "",
                "parameters": [
                  "dday",
                  "test_id"
                ],
                "resource": "dick_s_read_only_field_test",
                "response": "",
                "properties": {
                  "id": "Icicles: Test icicles."
                }
              },
              "name": null,
              "description": null
            }
          },
        },
      );
      saveRuleAndConfirm();

      // return to the rule edit page
      navigateToRule();

      // check saved config for fields is diplayed correctly
      cy.get('bmi-action-editor-list bmi-plugin-form').should('have.length', 2);
      uiSelectContains(dataCy('ui-form-field-Target Board to create card on'), boardName);
      getUiSelect(
        dataCy('ui-form-field-Fields with map or collection of data to store.')
      ).eq(0).contains('DDay');
      getUiSelect(
        dataCy('ui-form-field-Fields with map or collection of data to store.')
      ).eq(1).contains('Test ID');
      uiSelectContains(dataCy('ui-form-field-Fields mappings. Leave empty to store all.'), 'Card ID');
      cy.get(
        `${dataCy('ui-form-field-Fields mappings. Leave empty to store all.')} .map-editor__grid > input`
      ).should('have.value', 'Icicles: Test icicles.');

    });


  });

  describe('Spawn New Card', () => {

    it('should create a spawn card action, save, check', () => {
      // rule edit view is rendered
      cy.get('h1').contains('E2E Plugin Testing Rule');

      // Add action
      cy.contains('Add Action').click();
      cy.get('.dropdown-menu__dropdown').scrollTo('bottom');
      cy.contains('Spawn new Card').should('be.visible').click();
      pickUiOption(dataCy('ui-form-field-Target Board'), boardName);
      pickUiOption(dataCy('ui-form-field-ID Field'), 'Test ID');
      pickUiOption(dataCy('ui-form-field-Parent board field'), 'Action Type');
      pickUiOption(dataCy('ui-form-field-Parent card field'), 'DDay');
      cy.get(dataCy('ui-form-field-Properties to include in new Card')).contains('Add').click();
      pickUiOption(dataCy('ui-form-field-Properties to include in new Card'), 'Card ID')
      cy.get(
        `${dataCy('ui-form-field-Properties to include in new Card')} .map-editor__grid > input`
      ).type('testy');
      cy.get(
        `${dataCy('ui-form-field-Field to set new cards id')} input`
      ).type('test');


      // Save and navigate to board summary
      savedRule = updatedConfig(
        savedRule,
        {
          actions: {
            ...savedRule.actions,
            "spawn_test": {
              "id": "spawn_test",
              "type": "spawn",
              "order": 1,
              "config": {
                "method": "",
                "parameters": [],
                "resource": boardNameParam,
                "response": "test",
                "properties": {
                  "id": "testy"
                },
                "id": "test_id",
                "parent_board_field": "action_type",
                "parent_card_field": "dday"
              }
            }
          },
        },
      );
      saveRuleAndConfirm();

      // return to the rule edit page
      navigateToRule();

      // check saved config for fields is diplayed correctly
      cy.get('bmi-action-editor-list bmi-plugin-form').should('have.length', 2);
      uiSelectContains(dataCy('ui-form-field-Target Board'), boardName);
      uiSelectContains(dataCy('ui-form-field-ID Field'), 'Test ID');
      uiSelectContains(dataCy('ui-form-field-Parent board field'), 'Action Type');
      uiSelectContains(dataCy('ui-form-field-Parent card field'), 'DDay');
      uiSelectContains(dataCy('ui-form-field-Properties to include in new Card'), 'Card ID');
      cy.get(`${dataCy('ui-form-field-Properties to include in new Card')}  .map-editor__grid > input`)
        .should('be.visible')
        .should('have.value', 'testy');
      cy.get(`${dataCy('ui-form-field-Field to set new cards id')} input`)
        .should('be.visible')
        .should('have.value', 'test');

    });

  });

  describe('Metric Generator', () => {

    it('should create a metricifier config, save, revisit and check display', () => {

      const filterFieldSelector = dataCy('ui-form-field-Filter');
      const addMapItem = (index: number, key: string, value: string): void => {
        cy.get(`${filterFieldSelector} bmi-ui-select-input`).eq(index).click();
        cy.get(`${filterFieldSelector} .dropdown-wrapper__input`).type(key);
        cy.get(`${filterFieldSelector} .dropdown-wrapper__input-icon`).click();
        cy.get(`${filterFieldSelector} ${dataCy('map-editor-value-input')}`).eq(index).type(value);
      }
      const checkMapItem = (index: number, key: string, value: string): void => {
        getUiSelect(filterFieldSelector).eq(index).contains(key);
        cy.get(`${filterFieldSelector} ${dataCy('map-editor-value-input')}`).eq(index)
          .should('have.value', value);
      }

      const metricFields = {
        'Source of Data': 'Dick\'s Read Only Field Test',
        'Target Destination Metric Data Board': 'Notes',
        'Date Field in Source Data': 'Test ID',
        'Entity Field to group by in Source Data': 'DDay',
        'Value Field in Source Data': 'Card ID'
      }
      // rule edit view is rendered
      cy.get('h1').contains('E2E Plugin Testing Rule');

      // remove existing action
      cy.get('bmi-action-editor-list .ui-icon-trash').eq(0).click();

      // Add action
      cy.contains('Add Action').click();
      cy.contains('Metric Generation').scrollIntoView().should('be.visible').click();

      pickUiOption(dataCy('ui-form-field-Source of Data'),metricFields['Source of Data']);
      pickUiOption(dataCy('ui-form-field-Target Destination Metric Data Board'),metricFields['Target Destination Metric Data Board']);
      pickUiOption(dataCy('ui-form-field-Date Field in Source Data'),metricFields['Date Field in Source Data']);
      pickUiOption(dataCy('ui-form-field-Entity Field to group by in Source Data'),metricFields['Entity Field to group by in Source Data']);
      pickUiOption(dataCy('ui-form-field-Value Field in Source Data'),metricFields['Value Field in Source Data']);

      cy.get(filterFieldSelector).contains('Add').click().click().click().click().click();
      addMapItem(0, '0', '#');
      addMapItem(1, '1', 'a');
      addMapItem(2, '2', 'b');
      addMapItem(3, '3', 'c');
      getUiSelect(filterFieldSelector).eq(4).click();
      cy.get(`${filterFieldSelector} .select-options__option`).contains('Phase').scrollIntoView().click();
      cy.get(`${filterFieldSelector} ${dataCy('map-editor-value-input')}`).eq(4).type('phase filter value');

      cy.get(
        `${dataCy('ui-form-field-Aggregation Method')} input`
      ).type('test method');
      cy.get(
        `${dataCy('ui-form-field-Aggregation Resolution')} input`
      ).type('test resolution');

      // Save and navigate to board summary
      cy.get(dataCy('rule-builder-save-button')).should('be.enabled').then(() => {
        savedRule = updatedConfig(
          savedRule,
          {
            actions: {
              "metricifier_test": {
                "id": "metricifier_test",
                "type": "metricifier",
                "order": 1,
                "config": {
                    "method": "",
                    "parameters": [],
                    "resource": "",
                    "response": "",
                    "properties": {},
                    "source_data": "dick_s_read_only_field_test",
                    "metric_data": "notes",
                    "date_field": "test_id",
                    "entity_field": "dday",
                    "value_field": "id",
                    "filter": {
                        "0": "#",
                        "1": "a",
                        "2": "b",
                        "3": "c",
                        "phase": "phase filter value"
                    },
                    "aggregation_method": "test method",
                    "aggregation_resolution": "test resolution"
                },
                "name": null,
                "description": null
              }
            },
          },
        );
      });
      saveRuleAndConfirm();

      // return to the rule edit page, for checking correctness
      navigateToRule();

      // check plugin display
      cy.get('bmi-action-editor-list bmi-plugin-form').should('have.length', 1);

      // check saved config
      uiSelectContains(dataCy('ui-form-field-Source of Data'),metricFields['Source of Data']);
      uiSelectContains(dataCy('ui-form-field-Target Destination Metric Data Board'),metricFields['Target Destination Metric Data Board']);
      uiSelectContains(dataCy('ui-form-field-Date Field in Source Data'),metricFields['Date Field in Source Data']);
      uiSelectContains(dataCy('ui-form-field-Entity Field to group by in Source Data'),metricFields['Entity Field to group by in Source Data']);
      uiSelectContains(dataCy('ui-form-field-Value Field in Source Data'),metricFields['Value Field in Source Data']);

      checkMapItem(0, '0', '#');
      checkMapItem(1, '1', 'a');
      checkMapItem(2, '2', 'b');
      checkMapItem(3, '3', 'c');
      checkMapItem(4, 'Phase', 'phase filter value');

      cy.get(
        `${dataCy('ui-form-field-Aggregation Method')} input`
      ).should('have.value', 'test method');
      cy.get(
        `${dataCy('ui-form-field-Aggregation Resolution')} input`
      ).should('have.value', 'test resolution');

    });

  });

});
