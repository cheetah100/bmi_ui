import { RouteParam, DataServer, DataServerOptions } from '@bmi/cypress/mocking/data-server';
import {
  checkCardView,
  postMock,
} from '@bmi/cypress/helper-functions';
// import { Formatters } from '@bmi/ui';
import sortBy from 'lodash-es/sortBy';
import { BoardConfig, Field } from '@bmi/gravity-services';
import { dataCy, pickUiOption } from '@bmi/cypress/common-element-actions';
// tslint:disable-next-line:nx-enforce-module-boundaries
// import { getTableCellFormatter } from '../../../bmi-admin/src/app/data-viewer/data-viewer.component';

// export function getTableCellFormatter(field: Field) {
//   switch(field.type) {
//     case 'DATE':
//       return Formatters.date;
//     case 'MAP':
//       return Formatters.map;
//     default:
//       return Formatters.defaultFormatter;
//   }
// }

export interface CheckTableContentOptions {
  actions?: boolean;
}

// const checkTableContent = (boardConfig: BoardConfig, rows: Array<object>, options: CheckTableContentOptions = {}) => {
//   const fields = sortBy(Object.values(boardConfig.fields), f => f.index, f => f.name);
//   const tableFields = [
//     ...fields.filter(f => !f.isMetadata),
//     ...fields.filter(f => f.isMetadata && f.id !== 'id')
//   ];
//
//   // only add id field for board config: view template might not have it
//   const idField = fields.find(f => f.id === 'id' && f.isMetadata);
//   if (idField) {
//     tableFields.unshift(idField);
//   }
//
//   console.log('tabelFields: ', tableFields.map(f => f.name));
//   const fieldLabels = tableFields.map(f => f.label);
//   if (options?.actions) {
//     fieldLabels.unshift('Actions');
//   }
//
//   // Check headings
//   cy.get('.head-cell').each((heading, index) => {
//     console.log(' => index: ', index);
//     console.log(' => heading: ', heading);
//     console.log(' => fieldLabels[index]: ', fieldLabels[index]);
//     cy.wrap(heading)
//       .should('contain.text', fieldLabels[index])
//   })
//
//   // Check data
//   rows.forEach( (row, cardsIndex) => {
//     cy.get(`[data-table-row-index="${cardsIndex}"]`)
//       .each((cell, cellIndex) => {
//         console.log(options?.actions ? 'actions' : 'no actions')
//         const fieldIndex = options?.actions ? cellIndex - 1 : cellIndex;
//         console.log(' => fieldIndex: ', fieldIndex);
//         if (!options?.actions || cellIndex > 0) {
//           const field = tableFields[fieldIndex];
//           let fieldValue = row[field.name]; // ?? cards[0].fields[field.name] ?? '';
//           const formatter = getTableCellFormatter(field)
//           fieldValue = formatter.formatValue(fieldValue);
//           cy.wrap(cell)
//             .should('contain.text', fieldValue);
//         }
//       })
//   })
// }


describe('Data Viewer', () => {
  const allCardNames = [
    'm2e_entity_types',
    'metric_units',
    'staff',
    'm2e_aggregation_method',
    'm2e_aggregation_resolutions',
    'm2e_objectives',
    'm2e_sources'
  ]
  const mockedRoutes: RouteParam[] = [
    postMock(
      '/gravity/spring/board/m2e_metrics/search?view=all',
      'search_m2e_metrics',
      '{conditions: {}}'
    ),
    postMock(
      '/gravity/spring/board/m2e_metrics/cards/cardlist',
      'card_m2e_metrics_M2000072',
      '["M2000072"]',
      () => {
        console.log(`returning metricCard`, metricCard)
        return [metricCard];
      }
    ),

  ];

  const boardName = 'm2e_metrics';
  let dataServer: DataServer;
  let boardConfig: BoardConfig;
  let metricCard;
  beforeEach(() => {
    const options: DataServerOptions = {
      basePath: 'data-viewer',
      genericRoutes: ['app', 'descriptions', 'current-user', 'configs'],
      allCardNames: allCardNames
    };
    dataServer = new DataServer(mockedRoutes, options);
    dataServer.readFixture('configs').then(configs => {
      boardConfig = new BoardConfig(configs[boardName]);
    });
    dataServer.readFixture('card_m2e_metrics_M2000072').then(cards => metricCard = cards[0])
    cy.visit('/board/m2e_metrics/data-viewer');
  });

  // it('should display the populated data viewer', () => {
  //
  //   // check we have the correct board config being used
  //   cy.get(dataCy('board-name')).contains(boardConfig.name);
  //
  //   dataServer.readFixture('search_m2e_metrics').then(cards => {
  //     cards = sortBy(cards, c => c.id);
  //
  //     checkTableContent(boardConfig, cards, {actions: true})
  //   })
  //
  // });

  it.only('should show the editor modal with the correct data', () => {
    console.log(`selecting first board`);
    expect('a').to.eql('a');

    // This is the only card mocked
    cy.get('[data-table-col-index=0][data-table-row-index=3] [title=Edit]').click();
    // return
    console.log(`about to check card view`)
    // Check the fields
    checkCardView(boardConfig, metricCard, dataServer);
    console.log(`cardview checked`)

    // check the list control exists, and that it displays the field value correctly
    cy.get(dataCy('form-control:list'))
      .find('input')
      .should('have.value', 'Only one')
  });

  it('should update the database after the card data has been updated', () => {
    // click 4th row
    cy.get('[data-table-col-index=0][data-table-row-index=3] [title=Edit]').click();
    // Wait until data is loaded
    cy.wait('@card_m2e_metrics_M2000072');

    const card = metricCard;
    const currentName = card.fields.name;
    const newName = `${currentName} (Updated)`;

    // change name
    cy.get(dataCy(`form-control:text-input:Name`))
      .type(`{selectall}${newName}`)
    // Change Entity Type
    const cardId = 'application', cardName = 'Application'
    pickUiOption(dataCy('ui-form-field-Entity Type'), cardName);

    // Mock POST request
    cy.intercept('PUT', '/gravity/spring/board/m2e_metrics/cards/M2000072', (req) => {
      // Check if the changed data is in the post message
      expect(req.body.name).to.eql(newName);
      expect(req.body.entity_type).to.eql(cardId);

      // Update the currect card
      metricCard.name = newName;
      metricCard.entity_type = cardId;
      req.reply(req.body)
    }).as('updateCard');

    // Save the data
    cy.get(dataCy('form-control:button')).contains('Save').click();
    // Wait for the process of the POST
    cy.wait('@updateCard')

    // This extra wait is needed to make it work
    cy.wait(200)
    // click on the 4th row
    cy.get('[data-table-col-index=0][data-table-row-index=3] [title=Edit]').click();

    //Verify the changed data has persisted
    checkCardView(boardConfig, metricCard, dataServer);
  })

  // it('should filter records using the entered criteria', () => {
  //   dataServer.readFixture('search_m2e_metrics').then(cards => {
  //     console.log(cards)
  //     const filter = 'employees';
  //     const filteredCards = cards.filter(card => {
  //       console.log(`includes: `, card.fields.name.includes(filter))
  //       return card.fields.name.includes(filter)
  //     })
  //
  //     cy.get(`${dataCy('board-data-searcher')} input`)
  //       .type(`{selectall}${filter}`);
  //
  //     cy.get('idp-data-table ui-button-toolbar').should('have.length', filteredCards.length);
  //
  //     checkTableContent(boardConfig, filteredCards, {actions: true})
  //   });
  // });
  /**
     * TODO: Test all the things.
     * - correct and complete data shown // done
     * - formatting for other field types // still to verify links
     * -
     * - view card // done
     * - edit card (both buttons), and check that table updates // only done for edit icon
     * - update phase, check table updates
     * - watch card, check card is watched
     * - delete card
     * - sorting
     * - filtering (search)
     * - add card
     * - pagination display and navigation
     */

});
