import { dataCy } from "@bmi/cypress/common-element-actions";
import { getMock, postMock } from "@bmi/cypress/helper-functions";
import { DataServer, DataServerOptions, RouteParam } from "@bmi/cypress/mocking/data-server";


/**
 * A cynical ploy to pick up some cheap test coverage
 *
 * These tests navigate to views that have the worst test coverage up to now
 * We're just checking that the views load without error and maybe clicking around to expose
 * as many components as possible. This is going to run at least some of the code that supports
 * these views - init, destroy, anything that happens by default during a cd cycle.
 *
 * So, not completely worthless - code is really running and some errors will be identified -
 * but the views touched here will definitely still need proper testing to cover all use cases
 */

describe('Review widgets page', () => {

  const mockedRoutes: RouteParam[] = [
    getMock('/gravity/spring/board/configs'),
    postMock(
      'http://pdx-col.eum-appdynamics.com/eumcollector/beacons/browser/v2/AD-AAB-ABA-DVD/adrum',
      'adrum',
      null,
      () => ({})
    ),
    getMock(
      '/gravity/spring/board/test/rules/full',
      'test-rules',
      null,
      '[]'
    ),
    getMock(
      '/gravity/spring/board/test/resources',
      'test-resources',
      null,
      '{}'
    ),
    getMock(
      '/gravity/spring/board/test',
      'board-test'
    ),
    {
      endPoint: '/gravity/spring/board/test',
      alias: 'board-test',
      method: 'PUT',
    },
    getMock('/gravity/spring/team'),
    getMock(
      '/gravity/spring/transform/',
      'transforms-list'
    ),
    getMock('/gravity/spring/transform/cost-trend-test'),
    postMock(
      '/gravity/spring/transform/execute',
      'cost-trend-test',
      null
    ),
    getMock(
      '/gravity/spring/transform/plugins',
      'transform-plugins'
    ),
    getMock(
      '/gravity/spring/board/cost_elements_raw',
      'cost_elements_raw',
      null,
      '{}'
    ),
  ];
  let dataServer: DataServer;

  beforeEach(() => {
    cy.viewport(1280, 1200);
    const options: DataServerOptions = {
      basePath: 'coverage-clicker',
      genericRoutes: ['app', 'descriptions', 'current-user'],
      allCardNames: []
    };
    dataServer = new DataServer(mockedRoutes, options);
    cy.visit('/');
  });

  it('should show the board home page and allow navigation to show related editor views', () => {
    // check home page
    cy.get('h1').contains('Modules');
    // navigate to board home
    cy.get('li.board-item').contains(/^Test$/).click({force: true});
    cy.get('h1').contains(/^Test$/);
    // edit board, check each tab
    cy.get('header a').contains('Edit Board').click({force: true});
    cy.wait('@board-test');
    cy.get('h1').contains(/^Test$/);
    cy.get('bmi-board-editor h2').contains('Edit Test Board');
    cy.get('bmi-board-editor li a').contains('General').click({force: true});
    cy.get('bmi-board-editor li a').contains('Phases').click({force: true});
    cy.get('bmi-board-editor li a').contains('Permissions').click({force: true});
    cy.get('.board-editor__save-button').click({force: true});
    cy.get(dataCy('confirmation-modal-confirm-button')).click({force: true});
    // back on board home
    cy.get('.board-summary__body p').contains('Test board for testing');
    // check view editor display
    cy.get('.btn--success.btn--large').contains('Add').click({force: true});
    cy.get('ui-dropdown-item').contains('View').click({force: true});
    cy.get('h1').contains('Create View');
    cy.get('.btn--secondary').contains('Add Field').click({force: true});
    cy.get('.btn--ghost').contains('Revert your changes').click({force: true});
    // back to board home
    cy.get('ui-breadcrumbs a').contains('test').click({force: true});
    cy.get('.board-summary__body p').contains('Test board for testing');
  });

  it.only('show navigate to the transforms home page, and show editor, config and preview for an existing transform', () => {
    // check home page
    // cy.wait('@app');
    // cy.wait(5000);
    cy.get('h1').contains('Modules');
    // navigate to transforms home
    cy.get('nav.sidebar li').contains('Transforms').click({force: true});
    cy.get('h1').contains('Transforms');
    // show config viewer and json
    cy.get('.object-list li').contains('cost-trend-test').click({force: true});
    cy.get('.toggle-json-button').click({force: true});
    // navigate to preview
    cy.get('.object-list li')
      .eq(2)
      .find('.row-action')
      .eq(1)
      .click({force: true});
    // back to transform home, view transform editor
    cy.get('ui-breadcrumbs a').contains('transform').click({force: true});
    cy.get('.object-list li')
      .eq(2)
      .find('.row-action')
      .eq(0)
      .click({force: true});
    cy.get('h1').contains('Edit Transform: Application Developer Allocation');
    cy.get('.btn--success').contains('Add Transformer').click({force: true});
    cy.get('ui-dropdown-item').contains('transpose').click({force: true});
    cy.get('.btn--ghost').contains('Cancel').click({force: true});
  });

});
