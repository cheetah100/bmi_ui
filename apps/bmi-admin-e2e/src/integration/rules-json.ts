const NEW_REGULAR_RULE = {
  "id": "e2e_test_regular_rule_ID",
  "name": "e2e test regular rule",
  "boardId": "dick_s_read_only_field_test",
  "ruleType": "COMPULSORY",
  "index": 66,
  "description": "Rule description content.",
  "automationConditions": {
    "property_test_id_M36BDPVQ": {
      "fieldName": "test_id",
      "value": "9001",
      "operation": "EQUALTO",
      "conditionType": "PROPERTY"
    },
    "phase_phase_D9Q93Q6V": {
      "fieldName": "phase",
      "value": "archive",
      "operation": "NOTEQUALTO",
      "conditionType": "PHASE"
    },
    "task_test_XHQ36C28": {
      "fieldName": "test",
      "value": "",
      "operation": "COMPLETE",
      "conditionType": "TASK"
    }
  },
  "taskConditions": {},
  "completionConditions": {},
  "actions": {
    "csvparser_CM36": {
      "id": "csvparser_CM36",
      "type": "csvparser",
      "order": 0,
      "config": {
        "method": "",
        "parameters": [],
        "resource": "test_id",
        "response": "",
        "properties": {}
      },
      "name": null,
      "description": null
    }
  },
  "schedule": ""
};

const NEW_SCHEDULED_RULE = {
  "id": "e2e_test_scheduled_rule",
  "name": "e2e test scheduled rule",
  "boardId": "dick_s_read_only_field_test",
  "ruleType": "SCHEDULED",
  "index": 67,
  "description": "Rule description here.",
  "automationConditions": {},
  "taskConditions": {},
  "completionConditions": {},
  "actions": {
    "spawn_DVR2": {
      "id": "spawn_DVR2",
      "type": "spawn",
      "order": 0,
      "config": {
        "method": "",
        "parameters": [],
        "resource": "dick_s_read_only_field_test",
        "response": "test",
        "properties": {
          "id": "testy"
        },
        "id": "test_id",
        "parent_board_field": "action_type",
        "parent_card_field": "dday"
      },
      "name": null,
      "description": null
    }
  },
  "schedule": "0 20 16 * * ?"
};

const NEW_TASK_RULE = {
  "id": "e2e_test_task_rule",
  "name": "e2e test task rule",
  "boardId": "dick_s_read_only_field_test",
  "ruleType": "TASK",
  "index": 0,
  "description": "Rule description here (task).",
  "automationConditions": {
    "property_test_id_896CVDVF": {
      "fieldName": "test_id",
      "value": "9001",
      "operation": "EQUALTO",
      "conditionType": "PROPERTY"
    }
  },
  "taskConditions": {
    "phase_phase_D6VPRF4P": {
      "fieldName": "phase",
      "value": "archive",
      "operation": "NOTEQUALTO",
      "conditionType": "PHASE"
    }
  },
  "completionConditions": {
    "task_test_9C23V8D2": {
      "fieldName": "test",
      "value": "",
      "operation": "COMPLETE",
      "conditionType": "TASK"
    }
  },
  "actions": {
    "csvparser_PHJT": {
      "id": "csvparser_PHJT",
      "type": "csvparser",
      "order": 0,
      "config": {
        "method": "",
        "parameters": [],
        "resource": "test_id",
        "response": "",
        "properties": {}
      },
      "name": null,
      "description": null
    }
  },
  "schedule": ""
};

const NEW_VALIDATION_RULE = {
  "id": "e2e_test_validation_rule",
    "name": "e2e test validation rule",
    "boardId": "dick_s_read_only_field_test",
    "ruleType": "VALIDATION",
    "index": 0,
    "description": "Rule description here (validation).",
    "automationConditions": {
      "task_test_36BDQTHK": {
      "fieldName": "test",
      "value": "",
      "operation": "COMPLETE",
      "conditionType": "TASK"
    }
  },
  "taskConditions": {},
  "completionConditions": {},
  "actions": {
    "csvparser_BWRX": {
      "id": "csvparser_BWRX",
      "type": "csvparser",
      "order": 0,
      "config": {
        "method": "",
        "parameters": [],
        "resource": "test_id",
        "response": "",
        "properties": {}
      },
      "name": null,
      "description": null
    }
  },
  "schedule": ""
};

const PLUGIN_STARTER_RULE = {
  "id": "e2e_plugin_testing_rule",
  "name": "E2E Plugin Testing Rule",
  "boardId": "dick_s_read_only_field_test",
  "ruleType": "SCHEDULED",
  "description": "Base rule for plugin e2e testing.",
  "automationConditions": {},
  "taskConditions": {},
  "completionConditions": {},
  "actions": {
    "csvparser_BWRX": {
      "id": "csvparser_BWRX",
      "type": "csvparser",
      "order": 0,
      "config": {
        "method": "",
        "parameters": [],
        "resource": "test_id",
        "response": "",
        "properties": {}
      },
      "name": null,
      "description": null
    }
  },
  "schedule": "0 20 16 * * ?"
};

export {
  NEW_REGULAR_RULE,
  NEW_SCHEDULED_RULE,
  NEW_TASK_RULE,
  NEW_VALIDATION_RULE,
  PLUGIN_STARTER_RULE
};
