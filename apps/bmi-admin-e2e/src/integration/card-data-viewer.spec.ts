import { dataCy } from '@bmi/cypress/common-element-actions';
import { DataServer, RouteParam } from '@bmi/cypress/mocking/data-server';


describe('Card Data Viewer Test', () => {
  const cardId = 'DI000001';
  const cardList = [];
  cardList.push(cardId);

  const useFullProperties = [
    'id',
    'board',
    'phase',
    'title',
    'lock',
    'alerted'
  ]
  let cardData;
  let alerts;
  let tasks;
  let server:DataServer;
  const configRoute: RouteParam =  {
    endPoint: '/gravity/spring/board/configs',
    fixture: 'configs.json',
    alias: 'configs',
  }
  const appRoutes:RouteParam[] = [
    {
      endPoint: '/gravity/spring/app/descriptions',
      // fixture: 'descriptions.json',
      alias: 'descriptions',
    },
    {
      endPoint: '/gravity/spring/app',
      // fixture: 'app.json',
      alias: 'loadApp',
    },
    {
      endPoint: '/gravity/spring/user/current',
      // fixture: 'current.json',
      alias: 'current',
    },
    {
      endPoint: 'http://pdx-col.eum-appdynamics.com/eumcollector/beacons/browser/v2/AD-AAB-ABA-DVD/adrum',
      method: 'POST',
      alias: 'adrum',
      enabled: false,
      content: '{}'
    }
  ]
  const mockedRoutes:RouteParam[] = [
    {
      endPoint: `/gravity/spring/board/dick_s_read_only_field_test/cards/${cardId}/tasks`,
      alias: 'tasks'
    },
    {
      endPoint: `/gravity/spring/board/dick_s_read_only_field_test/cards/cardlist`,
      alias: 'cardList',
      method: 'POST',
      payLoad: JSON.stringify(cardList)
    },
    {
      endPoint: `/gravity/spring/board/dick_s_read_only_field_test/cards/${cardId}/alerts`,
      // gravity/spring/board/dick_s_read_only_field_test/cards/DI000001/alerts
      alias: 'alerts'
    }
  ]

  before(() => {
    const allRoutes: RouteParam[] = [...appRoutes, ...mockedRoutes, configRoute];
    server = new DataServer(allRoutes);
    server.readFixture('cardList').then(cardList => {
      cardData = cardList[0];
      console.log(' => cardData: ', cardData);
    })
    server.readFixture('alerts').then(alertData => alerts = alertData)
    server.readFixture('tasks').then(taskData => tasks = taskData)
  });


  it('should display card data', () => {
    cy.visit(`/board/dick_s_read_only_field_test/data-viewer/${cardId}`)
    // server.waitForRoutes();
    cy.wait(`@cardList`);

    // Check if all useful properties are displayed
    useFullProperties.forEach(prop => {
      let value = cardData[prop];
      if ( typeof value === 'boolean') {
        value = value ? "true" : "false";
      }
      cy.get('[data-cy=card-details-useful-props]')
        .contains('.ui-property-list__key', prop)
        .siblings()
        .contains(value)
    })
    //Check alerts
    if (alerts.length === 0) {
      cy.get(dataCy('card-details-no-alerts'))
    }

    // Check tasks
    if (tasks.length === 0) {
      cy.get(dataCy('card-details-no-tasks'))
    }

    cy.get(dataCy('button-toolbar-edit-button')).click()

    cy.get(dataCy('form-control:text-input:name'))
      .should('have.value', cardData.fields.name)
      // .should('have.length', 1)
    cy.get(dataCy('form-control:numeric-input') + '[disabled]')
      .should('have.value', cardData.fields.test_id)
      .should('be.disabled')

    const newName = 'fred'

    cy.intercept('PUT', '/gravity/spring/board/dick_s_read_only_field_test/cards/DI000001', (req) => {
      console.log(req.body)
      expect(req.body.name).to.eql(newName)
      req.reply(req.body)
    })

    cy.get(dataCy('form-control:text-input:name'))
      .clear()
      .type(newName);

    cy.get(dataCy('form-control:button'))
      .contains('Save')
      .click()
  });
})
