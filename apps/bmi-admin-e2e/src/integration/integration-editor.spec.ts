import {
  DataServer,
  DataServerOptions,
  RouteParam,
} from '@bmi/cypress/mocking/data-server';
import { getMock } from '@bmi/cypress/helper-functions';
import { cloneDeep } from 'lodash-es';
import { EOL } from 'os';
import {
  checkFormFieldInput,
  dataCy,
  formFieldSelector,
  pickUiOption,
  typeFormFieldInput
} from '@bmi/cypress/common-element-actions';
import { resolve } from '@angular/compiler-cli/src/ngtsc/file_system';


const updatedIntegration = {
  id: 'dick_test',
  name: 'dick test',
  config: {
    url: null,
    records: null,
    total_field: null,
    type: null,
    board: 'asg_metrics',
    resource: 'set_status',
    credential: null,
    list: null,
    fields: null,
    schedule: null,
  },
  connector: 'polling_http',
  actions: {},
};
const checkBoardList = (dataServer: DataServer) => {
  dataServer.readFixture('configs').then((configs) => {
    const numofBoards = Object.keys(configs).length;
    cy.get('.select-options__option').should('have.length', numofBoards);
  });
};

const selectFirstResource = (
  dataServer: DataServer,
  metric: string,
  resourceSelector: string
) => {
  dataServer.readFixture(`${metric}-resources`).then((resourcesObject) => {
    console.log(resourcesObject)
    const resourceList = Object.keys(resourcesObject);
    const resourceName = resourceList[0];

    // select a resource
    cy.get(resourceSelector).click();
    cy.get('.select-options__option').should(
      'have.length',
      resourceList.length
    );
    cy.get('.select-options__option').contains(resourceName).click();
    cy.get('.ui-select-toggler__content').contains(resourceName);
  });
};

describe('Test Resource Selector Widget', () => {
  const integrationId = 'dick_test';
  let dataServer: DataServer = null;
  let pluginsResponse;
  let asgResources;
  let resourceContent;
  const newResourceName = 'set_title';
  let newResource;

  const baseDropdowmSelector = 'form-control:dropdown-select';
  const baseUiFormFieldSelector = 'ui-form-field';
  const newMetrics = 'asg-metrics';

  const pluginFormBoardSelector = dataCy(
    `${baseUiFormFieldSelector}-Board containing Request Body Resource`
  );
  const integrationTypeSelector = dataCy(
    `${baseDropdowmSelector}-Integration Type`
  );
  const resourceSelector = dataCy(
    `${baseUiFormFieldSelector}-Request Body Resource`
  );
  const resourceSelectorWidgetBoardSelector = dataCy(
    `${baseUiFormFieldSelector}-Board to select resources from`
  );
  const standardRoutes: RouteParam[] = [
    getMock('/gravity/spring/app', 'app'),
    getMock('/gravity/spring/app/descriptions', 'descriptions'),
    getMock('/gravity/spring/user/current', 'current-user'),
    getMock('/gravity/spring/board/configs', 'configs'),
  ];

  const testSpecificRoutes: RouteParam[] = [
    getMock('/gravity/spring/integration/dick_test', 'integration-test'),
    getMock('/gravity/spring/integration', 'all-integrations'),
    getMock(
      '/gravity/spring/integration/plugins',
      'all-integration-plugins',
      null,
      () => {
        return pluginsResponse;
      }
    ),
    getMock(
      '/gravity/spring/board/asgs/resources',
      'all-asg-resources',
      null,
      () => {
        return asgResources;
      }
    ),
    getMock('/gravity/spring/cred', 'all-credentials'),
    getMock(
      '/gravity/spring/board/asg_metrics/resources',
      'asg-metrics-resources'
    ),
    getMock(
      '/gravity/spring/board/asgs/resources/get_leader',
      'resource_get_leader',
      'resource_get_leader.txt',
      () => {
        return resourceContent;
      }
    ),
    getMock(
      '/gravity/spring/board/asgs/resources/get_leader/details',
      'resource_get_leader_details'
    ),
    getMock(
      '/gravity/spring/board/asgs/resources/set_title',
      'set_title',
      'set_title.txt',
    ),
    getMock('/gravity/spring/board/asgs/resources/set_title/details',
      'resource_set_title_details'
    )
  ];

  const dataServerOptions: DataServerOptions = {
    basePath: 'integration/edit',
  };

  beforeEach(() => {
    newResource = "const name = 'Fred';";
    const routes: RouteParam[] = [...standardRoutes, ...testSpecificRoutes];
    dataServer = new DataServer(routes, dataServerOptions);

    dataServer.readFixture('all-integration-plugins').then((plugins) => {
      pluginsResponse = cloneDeep(plugins);
    });
    // dataServer.readFixture('all-asg-resources').then((resources) => {
    //   asgResources = cloneDeep(resources);
    // });

  });

  it.only(`should create new integration`, () => {
    cy.visit(`/system/integrations/`);
    cy.get(dataCy('integrations-create-button')).click();
    // cy.get(`${dataCy(`${formFieldSelector('Name') input`).type(integrationId);
    typeFormFieldInput('Name', integrationId, {delay: 100});
    cy.wait(1000)
    // checkFormFieldInput('Integration ID', 'dick_test1')
    pickUiOption(formFieldSelector('Integration Type'), 'Polling HTTP Call');
    typeFormFieldInput('URL to call', 'https://api.watercare.co.nz/lake/lakelevel/lhd');
    cy.get(formFieldSelector('Schedule (Cron Expression)')).click();
    typeFormFieldInput('Hour', '10');
  });

  it('should open an existing integration with a polling http connector', () => {
    console.log(`open existing integration`)
    cy.visit(`/system/integrations/${integrationId}/edit`);

    // Make sure the edit page is showing for requested integration
    dataServer.readFixture('integration-test').then((integration) => {
      // Check name input field
      cy.get(`${dataCy('ui-form-field-Name')} input`).should(
        'have.value',
        integration.name
      );

      dataServer.readFixture('all-integration-plugins').then((plugins) => {
        // Check plugin name
        const plugin = plugins[integration.connector];
        expect(plugin).to.be.ok;
        cy.get('ui-form-field[label="Integration Type"]').contains(plugin.name);

        //check selected board
        dataServer.readFixture('configs').then((configs) => {
          const boardName = configs[integration.config.board].name;
          cy.get(pluginFormBoardSelector).contains(boardName);
        });

        //check if correct resource is shown
        cy.get(resourceSelector).contains(integration.config.resource);
      });
    });
  });

  it('should update the board and resource of an existing integration', () => {
    cy.visit(`/system/integrations/${integrationId}/edit`);
    // Check the effect of selecting another board
    cy.get(pluginFormBoardSelector).click();
    checkBoardList(dataServer);

    cy.get('.select-options__option').contains('ASG Metric').click();
    // check if no resource is selected
    cy.get('.ui-select-toggler__content').contains('-');
    selectFirstResource(dataServer, newMetrics, resourceSelector);

    cy.get(dataCy('btn-save')).click();

    const original = {
      board: '"asgs"',
      resource: '"get_leader"',
    };
    const modified = {
      board: `"asg_metrics"`,
      resource: '"set_status"',
    };
    // check if the popup is displayed
    cy.get('h1').contains('Save Integration?');

    cy.get('.diff-viewer__headings').contains('Original');
    cy.get('.diff-viewer__headings').contains('Modified');

    cy.get('.editor.original .view-line span')
      .contains('"resource"')
      .contains(original.resource);

    cy.get('.editor.original .view-line span').contains('"board"');

    cy.get('.editor.modified .view-line span')
      .contains('"resource"')
      .contains(modified.resource);

    cy.get('.editor.modified .view-line span')
      .contains('"board"')
      .contains(modified.board);

    cy.intercept('PUT', '/gravity/spring/integration', (req) => {
      expect(req.body).to.eql(updatedIntegration);
      req.reply(req.body);
    });

    cy.get('.mat-dialog-container button').contains('Save').click();
  });

  it('should create a new resource', () => {
    cy.visit(`/system/integrations/${integrationId}/edit`);

    //Open Editor modal to create new resource
    cy.get('.resource-selector__create-button').click();

    cy.get('mat-dialog-container h1').contains('Resource Editor');

    cy.get(dataCy('ui-form-field-Resource ID') + ' input').type(
      newResourceName
    );

    cy.get('ngx-monaco-editor textarea.inputarea').focus().type(newResource);

    cy.get('.mat-dialog-container button').contains('Save').click();

    cy.get('.mat-dialog-content h2').contains(`Create ${newResourceName}`);

    cy.get('.mat-dialog-content p').contains(
      /This will create the resource on the .* board\./
    );

    cy.intercept(
      'POST',
      `/gravity/spring/board/asgs/resources/${newResourceName}/text`,
      (req) => {
        expect(req.body).to.eql(newResource);
        asgResources[newResourceName] = newResourceName;
        req.reply(req.body);
      }
    );
    cy.get('mat-dialog-actions button').contains('Create resource').click();

    cy.get(resourceSelector).click();
    cy.get('.select-options__option').should('have.length', 2);
  });

  it('should show the board selector', () => {
    cy.visit(`/system/integrations/${integrationId}/edit`);
    // change the plugin definition to force the Resource Selector widget
    // to show the Board Selector control
    const pollingHttoConnctorFields = cloneDeep(
      pluginsResponse.polling_http.fields
    );
    const updatedFieldList = pollingHttoConnctorFields.filter(
      (field) => field.type !== 'BOARD'
    );
    pluginsResponse.polling_http.fields = updatedFieldList;

    // check if the plugin board selector does not exist
    cy.get(pluginFormBoardSelector).should('have.length', 0);

    // check if the Resource Selector Widget board selector does wxist
    cy.get(resourceSelectorWidgetBoardSelector).should('have.length', 1);

    // Open board selector
    cy.get(resourceSelectorWidgetBoardSelector).click();
    checkBoardList(dataServer);

    cy.get('.select-options__option').contains('ASG Metric').click();
    selectFirstResource(dataServer, 'asg-metrics', resourceSelector);
  });

  it('should edit an existing resource', () => {
    cy.visit(`/system/integrations/${integrationId}/edit`);
    dataServer.readFixture('resource_get_leader').then((content) => {
      // Make sure only linefeeds are in the resource text, otherwise the
      // expected data from the POST is incorrect.
      resourceContent = content.replace(/\r\n/g, '\n');
      const resourceName = 'get_leader';
      cy.get(dataCy('resource-selector__edit-button')).click();

      cy.get(dataCy('ui-form-field-Resource ID') + ' input').should(
        'have.value',
        resourceName
      );

      const newLine = "const newLine:string = 'NEW LINE';";

      cy.get('ngx-monaco-editor textarea.inputarea')
        .focus()
        .type('{ctrl+end}{enter}{enter}')
        .type(newLine);


      cy.intercept(
        'POST',
        `/gravity/spring/board/asgs/resources/${resourceName}/text`,
        (req) => {
          console.log(EOL.length)
          resourceContent = resourceContent + `${EOL}${EOL}${newLine}`;
          console.log(`rc`, resourceContent.length)
          console.log(`rc`, `'${resourceContent}'`)
          console.log(`body`, `'${req.body}'`)
          console.log(`body`, req.body.length)
          expect(req.body).to.eql(resourceContent);
          req.reply(req.body);
        }
      ).as('post_resource');

      cy.get('.board-resource-coponent__buttons').contains('Save').click();

      cy.get('mat-dialog-actions button').contains('Save resource').click();

      // Make sure the modal is no longer showing
      cy.get(dataCy('ui-form-field-Resource ID') + ' input').should(
        'have.length',
        0
      );
    });
  });

  it('should delete an existing resource', () => {

    // Add new resource to list of resources
    asgResources[newResourceName] = newResourceName;
    dataServer.readFixture('resource_get_leader').then((content) => {
      // Make sure only linefeeds are in the resource text, otherwise the
      // expected data from the POST is incorrect.
      resourceContent = content.replace(/\r\n/g, '\n');
      return resourceContent;
    });
    const numOfResources = Object.keys(asgResources).length;


    cy.visit(`/system/integrations/${integrationId}/edit`);

    //make sure the get_leader resource is selected
    cy.get(resourceSelector).click();
    cy.get('.select-options__option').should(
      'have.length',
      numOfResources
    );
    cy.get('.select-options__option').contains(newResourceName).click();
    cy.get('.ui-select-toggler__content').contains(newResourceName);
    cy.get(resourceSelector).contains(newResourceName);

    // Click to show delete confirmation modal
    cy.get(dataCy('resource-selector__delete-button')).click();

    cy.intercept('DELETE', '/gravity/spring/board/asgs/resources/set_title',
      req => {
        expect(req.body).to.eql('');
        req.reply(req.body);
      }).as('delete_set_title')

    cy.get('h2').contains('Confirm deletion of set_title resource');

    cy.get('button').contains('Delete Resource').click().then( () => delete asgResources[newResourceName])
    // Update mocked data to reflect the resource has been deleted

    cy.get(resourceSelector).click();
    cy.get('.select-options__option').should(
      'have.length',
      numOfResources-1
    );

    // click cancel button to check if the correct message is displayed
    cy.get(dataCy('btn-cancel')).click();

    cy.get('mat-dialog-content p').contains('No resource has been currently selected, Please select one!!')

  });
});
