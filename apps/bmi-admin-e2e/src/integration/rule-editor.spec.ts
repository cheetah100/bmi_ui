import { DataServer, DataServerOptions, RouteParam } from '@bmi/cypress/mocking/data-server';
import { getMock, postMock } from '@bmi/cypress/helper-functions';
import { NEW_REGULAR_RULE, NEW_SCHEDULED_RULE, NEW_TASK_RULE, NEW_VALIDATION_RULE } from './rules-json';
import { dataCy, getUiSelect, pickUiOption } from '@bmi/cypress/common-element-actions';


describe('Rule Editor', () => {

  let rulesData = [];
  const boardName = 'Dick\'s Read Only Field Test';
  const ruleName = 'test';
  const mockedRoutes:RouteParam[] = [
    {
      endPoint: '/gravity/spring/app',
      fixture: 'app.json',
      alias: 'loadApp'
    },
    {
      endPoint: '/gravity/spring/fake_endpoint',
      alias: 'allRules',
      enabled: false
    },
    {
      endPoint: '/gravity/spring/board/dick_s_read_only_field_test/resources',
      alias: 'resources'
    },

    {
      endPoint: '/gravity/spring/user/current',
      alias: 'current'
    },
    {
      endPoint: '/gravity/spring/board/configs',
      fixture: 'configs.json',
      alias: 'configs',
      enabled: false
    },
    {
      endPoint: '/gravity/spring/board/dick_s_read_only_field_test/rules/e2e_test_regular_rule_ID',
      fixture: null,
      alias: 'newRegularRule',
      content: JSON.stringify(NEW_REGULAR_RULE)
    },
    {
      endPoint: '/gravity/spring/board/dick_s_read_only_field_test/rules/e2e_test_scheduled_rule',
      fixture: null,
      alias: 'newScheduledRule',
      content: JSON.stringify(NEW_SCHEDULED_RULE)
    },
    {
      endPoint: '/gravity/spring/board/dick_s_read_only_field_test/rules/e2e_test_task_rule',
      fixture: null,
      alias: 'newTaskRule',
      content: JSON.stringify(NEW_TASK_RULE)
    },
    {
      endPoint: '/gravity/spring/board/dick_s_read_only_field_test/rules/e2e_test_validation_rule',
      fixture: null,
      alias: 'newValidationRule',
      content: JSON.stringify(NEW_VALIDATION_RULE)
    },
    getMock(
      '/gravity/spring/board/dick_s_read_only_field_test/rules/full',
      'rulesData',
      null,
      () => rulesData
    ),
    getMock('/gravity/spring/app/descriptions', 'descriptions'),
    getMock('/gravity/spring/system/automation/plugins/metadata', 'metadata'),
    postMock(
      '/gravity/spring/board/dick_s_read_only_field_test/rules',
      'post_regular_rule',
      JSON.stringify(NEW_REGULAR_RULE),
      () => NEW_REGULAR_RULE
    ),
    postMock(
      '/gravity/spring/board/dick_s_read_only_field_test/rules',
      'post_scheduled_rule',
      JSON.stringify(NEW_SCHEDULED_RULE),
      () => NEW_SCHEDULED_RULE
    ),
    postMock(
      '/gravity/spring/board/dick_s_read_only_field_test/rules',
      'post_task_rule',
      JSON.stringify(NEW_TASK_RULE),
      () => NEW_TASK_RULE
    ),
    postMock(
      '/gravity/spring/board/dick_s_read_only_field_test/rules',
      'post_validation_rule',
      JSON.stringify(NEW_VALIDATION_RULE),
      () => NEW_VALIDATION_RULE
    ),
    postMock(
      'http://pdx-col.eum-appdynamics.com/eumcollector/beacons/browser/v2/AD-AAB-ABA-DVD/adrum',
      'adrum',
      null,
      () => ({})
    )
  ];
  let server:DataServer;

  beforeEach( () => {
    const options: DataServerOptions = {
      basePath: 'rule-editor',
      genericRoutes: ['app', 'descriptions', 'current-user', 'configs']
    };
    server = new DataServer(mockedRoutes, options);
    server.readFixture('configs').then(configs => {
      window.sessionStorage.setItem(
        'gravityCache:/gravity/spring/board/configs',
        JSON.stringify(configs)
      )
    })
    cy.viewport(1000, 1232);
    server.readFixture('allRules').then(data => rulesData = data);
    cy.visit("/board/dick_s_read_only_field_test", {})
  });

  describe('should create, and then check, a new ...', () => {

    it('regular rule', () => {

      // Start at board summary page, select add rule
      cy.contains('Add').click();
      cy.contains('Rule').click({force: true});

      // Help content for regular rules is showing
      cy.get(dataCy('rule-editor-help')).find('h3').contains('Regular Rules');

      // Rule type is regular
      getUiSelect(dataCy('rule-type')).contains('Regular');

      // name, id, desc are empty
      cy.get(dataCy('input-rule-name')).should('have.value', '');
      cy.get(dataCy('input-rule-id')).should('have.value', '');
      cy.get(dataCy('textarea-rule-description')).should('have.value', '');

      // No conditions or actions are defined.
      cy.get(`${dataCy('rule-trigger-conditions')} ui-stacker-box`).should('not.exist');
      cy.get('bmi-action-editor-list ui-stacker-box').should('not.exist');

      // No conditions warning is showing
      cy.get('ui-alert-box').contains('Rule has no conditions!');

      // Validation: save button is disabled
      cy.get(dataCy('rule-builder-save-button')).should('be.disabled');

      // Fill out rule form
      cy.get(dataCy('input-rule-name')).type('e2e test regular rule');
      cy.get(dataCy('textarea-rule-description')).type('Rule description content.');

      // Validation: save button should be enabled now
      cy.get(dataCy('rule-builder-save-button')).should('be.enabled');

      // check generated id, and then change it
      cy.get(dataCy('input-rule-id')).should('have.value', 'e2e_test_regular_rule');
      cy.get(dataCy('input-rule-id')).type('_ID');

      // Add conditions
      // property
      cy.contains('Add condition').click();
      cy.contains('Property (regular)').should('be.visible').click();
      cy.get('gravity-condition-list-row-property [label=Field] .ui-icon-edit')
        .click();
      cy.get('gravity-condition-list-row-property [label=Field]')
        .contains('test_id')
        .click();
      getUiSelect('gravity-condition-list-row-property [label=Value]').click();
      cy.get('gravity-condition-list-row-property [label=Value] input')
        .type('9001');
      cy.get('gravity-condition-list-row-property [label=Value]')
        .contains('Add')
        .click();
      // phase
      cy.contains('Add condition').click();
      cy.contains('Phase Condition').should('be.visible').click();
      cy.get('gravity-condition-list-row-phase [label=Operation] select')
        .select('Not equal to');
      pickUiOption('gravity-condition-list-row-phase [label=Value]', 'Archive');
      // task status
      cy.contains('Add condition').click();
      cy.contains('Task Status Condition').should('be.visible').click();
      pickUiOption('gravity-condition-list-row-task [label=Value]', 'test');

      // Add action
      cy.contains('Add Action').click();
      cy.contains('CSV Parser').should('be.visible').click();
      pickUiOption(dataCy('ui-form-field-Field containing CSV content'), 'Test ID');

      // Save
      rulesData.push(NEW_REGULAR_RULE);
      cy.get(dataCy('rule-builder-save-button')).should('be.enabled').click();

      // Rule danger confirmation required
      cy.get('gravity-rule-save-modal h2')
        .should('be.visible')
        .contains('Confirm save rule?');
      // Save disabled until confirmation
      cy.get('gravity-rule-save-modal button')
        .contains('Save Rule')
        .should('be.disabled');
      // Confirm and save
      cy.get('gravity-rule-save-modal ui-checkbox').click();
      cy.get('gravity-rule-save-modal button')
        .contains('Save Rule')
        .should('be.enabled')
        .click();

      // Navigates to board page
      cy.get('.board-summary__layout');

      // Check that rule is added to the list - is there a better way to make sure
      // this alias has been accessed for the 4th time?
      cy.wait(['@rulesData', '@rulesData', '@rulesData', '@rulesData']);
      cy.get(dataCy('compulsory-rule')).its('length').should('eq', 2);
      cy.get(dataCy('compulsory-rule')).contains('e2e test regular rule');

      // View rule in editor.
      cy.get(dataCy('compulsory-rule'))
        .contains('e2e test regular rule')
        .click({force: true});

      // Check name, id, desc have persisted correctly
      cy.get(dataCy('input-rule-name')).should('have.value', NEW_REGULAR_RULE.name);
      cy.get(dataCy('input-rule-id')).should('have.value', NEW_REGULAR_RULE.id);
      cy.get(dataCy('textarea-rule-description')).should('have.value', NEW_REGULAR_RULE.description);

      // Rule type is still regular
      getUiSelect(dataCy('rule-type')).contains('Regular');

      // Correct conditions and actions were saved and displayed correctly
      cy.get('gravity-condition-list ui-stacker-box').should('have.length', 3);
      cy.get('gravity-condition-list-row-property [label=Field] .path-segments')
        .should('have.length', 1);
      cy.get('gravity-condition-list-row-property [label=Field] .path-segments')
        .contains('test_id');
      cy.get('gravity-condition-list-row-property [label=Operation] select')
        .should('have.value', 'EQUALTO');
      getUiSelect('gravity-condition-list-row-property [label=Value]').contains('9001');
      cy.get('gravity-condition-list-row-phase [label=Operation] select')
        .should('have.value', 'NOTEQUALTO');
      getUiSelect('gravity-condition-list-row-phase [label=Value]').contains('archive');
      cy.get('gravity-condition-list-row-task [label=Operation] select')
        .should('have.value', 'COMPLETE');
      getUiSelect('gravity-condition-list-row-task [label=Value]').contains('test');
      cy.get('bmi-action-editor-list ui-stacker-box').should('have.length', 1);
      getUiSelect(dataCy('ui-form-field-Field containing CSV content')).contains('Test ID');

    });

    it('scheduled rule', () => {
      // Start at board summary page, select add rule
      cy.contains('Add').click();
      cy.contains('Rule').click({force: true});

      // select regular rule type
      pickUiOption(dataCy('rule-type'), 'Scheduled');

      // No help for scheduled rules yet
      cy.get(dataCy('rule-editor-help')).find('h3').should('not.exist');

      // name, id, desc, are empty
      cy.get(dataCy('input-rule-name')).should('have.value', '');
      cy.get(dataCy('input-rule-id')).should('have.value', '');
      cy.get(dataCy('textarea-rule-description')).should('have.value', '');

      // Schedule not defined
      cy.get('bmi-ui-cron-editor .alert__message').contains('No schedule');

      // No actions are defined.
      cy.get('bmi-action-editor-list ui-stacker-box').should('not.exist');

      // Validation: save button is disabled
      cy.get(dataCy('rule-builder-save-button')).should('be.disabled');

      // Fill out rule form
      cy.get(dataCy('input-rule-name')).type('e2e test scheduled rule');
      cy.get(dataCy('textarea-rule-description')).type('Rule description here.');

      // Validation: save button should be enabled now
      cy.get(dataCy('rule-builder-save-button')).should('be.enabled');

      // check generated id
      cy.get(dataCy('input-rule-id')).should('have.value', 'e2e_test_scheduled_rule');

      // set schedule
      cy.get('bmi-ui-cron-editor ui-checkbox').click();
      cy.get(`${dataCy('ui-form-field-Minute')} [type=number]`).should('exist').type('20');
      cy.get(`${dataCy('ui-form-field-Hour')} [type=number]`).should('exist').type('16');
      cy.get(`${dataCy('ui-form-field-Day of week')} ui-select`).click();
      cy.get(`${dataCy('ui-form-field-Day of week')} .select-options__option`)
        .contains('Every day')
        .click();

      // Add action
      cy.contains('Add Action').click();
      cy.get('.dropdown-menu__dropdown').scrollTo('bottom');
      cy.contains('Spawn new Card').should('be.visible').click();
      pickUiOption(dataCy('ui-form-field-Target Board'), boardName);
      pickUiOption(dataCy('ui-form-field-ID Field'), 'Test ID');
      pickUiOption(dataCy('ui-form-field-Parent board field'), 'Action Type');
      pickUiOption(dataCy('ui-form-field-Parent card field'), 'DDay');
      cy.get(dataCy('ui-form-field-Properties to include in new Card')).contains('Add').click();
      pickUiOption(dataCy('ui-form-field-Properties to include in new Card'), 'Card ID')
      cy.get(
        `${dataCy('ui-form-field-Properties to include in new Card')} .map-editor__grid > input`
      ).type('testy');
      cy.get(
        `${dataCy('ui-form-field-Field to set new cards id')} input`
      ).type('test');

      // Save
      rulesData.push(NEW_SCHEDULED_RULE);
      cy.get(dataCy('rule-builder-save-button')).should('be.enabled').click();

      // Rule danger message
      cy.get('gravity-rule-save-modal h2')
        .should('be.visible')
        .contains('Confirm save rule?');
      // Confirm and save
      cy.get('gravity-rule-save-modal button')
        .contains('Save Rule')
        .should('be.enabled')
        .click();

      // Navigates to board page
      cy.get('.board-summary__layout');

      // Check that rule is added to the list
      cy.wait(['@rulesData', '@rulesData']);
      cy.get(dataCy('scheduled-rule')).its('length').should('eq', 2);
      cy.get(dataCy('scheduled-rule')).contains('e2e test scheduled rule');

      // View rule in editor.
      cy.get(dataCy('scheduled-rule'))
        .contains('e2e test scheduled rule')
        .click({force: true});

      // Check name, desc, and id have persisted correctly
      cy.get(dataCy('input-rule-name')).should('have.value', NEW_SCHEDULED_RULE.name);
      cy.get(dataCy('input-rule-id')).should('have.value', NEW_SCHEDULED_RULE.id);
      cy.get(dataCy('textarea-rule-description')).should('have.value', NEW_SCHEDULED_RULE.description);

      // Rule type is still validation
      getUiSelect(dataCy('rule-type')).contains('Scheduled');

      // Action is displayed correctly
      cy.get('bmi-action-editor-list bmi-plugin-form').should('have.length', 1);
      getUiSelect(dataCy('form-control:dropdown-select-Target Board'))
        .contains(boardName);
      getUiSelect(dataCy('ui-form-field-ID Field'))
        .contains('Test ID');
      getUiSelect(dataCy('ui-form-field-Parent board field'))
        .contains('Action Type');
      getUiSelect(dataCy('ui-form-field-Parent card field'))
        .contains('DDay');
      getUiSelect(dataCy('ui-form-field-Properties to include in new Card'))
        .should('be.visible')
        .contains('Card ID');
      cy.get(`${dataCy('ui-form-field-Properties to include in new Card')}  .map-editor__grid > input`)
        .should('be.visible')
        .should('have.value', 'testy');
      cy.get(`${dataCy('ui-form-field-Field to set new cards id')} input`)
        .should('be.visible')
        .should('have.value', 'test');

    });

    it('task rule', () => {
      // Start at board summary page, select add rule
      cy.contains('Add').click();
      cy.contains('Rule').click({force: true});

      // select regular rule type
      pickUiOption(dataCy('rule-type'), 'Task');

      // Help content for regular rules is showing
      cy.get(dataCy('rule-editor-help')).find('h3').contains('Task Rules');

      // name, id, desc, are empty
      cy.get(dataCy('input-rule-name')).should('have.value', '');
      cy.get(dataCy('input-rule-id')).should('have.value', '');
      cy.get(dataCy('textarea-rule-description')).should('have.value', '');

      // No conditions or actions are defined.
      cy.get(`${dataCy('rule-trigger-conditions')} ui-stacker-box`).should('not.exist');
      cy.get(`${dataCy('rule-start-conditions')} ui-stacker-box`).should('not.exist');
      cy.get(`${dataCy('rule-completion-conditions')} ui-stacker-box`).should('not.exist');
      cy.get('bmi-action-editor-list ui-stacker-box').should('not.exist');

      // Validation: save button is disabled
      cy.get(dataCy('rule-builder-save-button')).should('be.disabled');

      // Fill out rule form
      cy.get(dataCy('input-rule-name')).focus().type('e2e test task rule');
      cy.get(dataCy('textarea-rule-description')).type('Rule description here (task).');

      // Validation: save button should be enabled now
      cy.get(dataCy('rule-builder-save-button')).should('be.enabled');

      // check generated id
      cy.get(dataCy('input-rule-id')).should('have.value', 'e2e_test_task_rule');

      // add conditions
      cy.get(dataCy('rule-trigger-conditions')).contains('Add condition').click();
      cy.contains('Property (regular)').should('be.visible').click();
      cy.get('gravity-condition-list-row-property [label=Field] .ui-icon-edit')
        .click();
      cy.get('gravity-condition-list-row-property [label=Field]')
        .contains('test_id')
        .click();
      getUiSelect('gravity-condition-list-row-property [label=Value]').click();
      cy.get('gravity-condition-list-row-property [label=Value] input')
        .type('9001');
      cy.get('gravity-condition-list-row-property [label=Value]')
        .contains('Add')
        .click();

      cy.get(dataCy('rule-start-conditions')).contains('Add condition').click();
      cy.contains('Phase Condition').should('be.visible').click();
      cy.get('gravity-condition-list-row-phase [label=Operation] select')
        .select('Not equal to');
      pickUiOption('gravity-condition-list-row-phase [label=Value]', 'Archive');

      cy.get(dataCy('rule-completion-conditions')).contains('Add condition').click();
      cy.contains('Task Status Condition').should('be.visible').click();
      pickUiOption('gravity-condition-list-row-task [label=Value]', 'test');

      // Add action
      cy.contains('Add Action').click();
      cy.contains('CSV Parser').should('be.visible').click();
      pickUiOption(dataCy('ui-form-field-Field containing CSV content'), 'Test ID');

      // Save
      rulesData.push(NEW_TASK_RULE);
      cy.get(dataCy('rule-builder-save-button')).should('be.enabled').click();

      // Rule danger message
      cy.get('gravity-rule-save-modal h2')
        .should('be.visible')
        .contains('Confirm save rule?');
      // Confirm and save
      cy.get('gravity-rule-save-modal button')
        .contains('Save Rule')
        .should('be.enabled')
        .click();

      // Navigates to board page
      cy.get('.board-summary__layout');

      // Check that rule is added to the list
      cy.wait(['@rulesData', '@rulesData', '@rulesData', '@rulesData']);
      cy.get(dataCy('task-rule')).its('length').should('eq', 2);
      cy.get(dataCy('task-rule')).contains('e2e test task rule');

      // View rule in editor.
      cy.get(dataCy('task-rule'))
        .contains('e2e test task rule')
        .click({force: true});

      // Check name, desc, and id have persisted correctly
      cy.get(dataCy('input-rule-name')).should('have.value', NEW_TASK_RULE.name);
      cy.get(dataCy('input-rule-id')).should('have.value', NEW_TASK_RULE.id);
      cy.get(dataCy('textarea-rule-description')).should('have.value', NEW_TASK_RULE.description);

      // Rule type is still scheduled
      getUiSelect(dataCy('rule-type')).contains('Task');

      // Correct conditions and actions were saved and displayed correctly
      cy.get(`${dataCy('rule-trigger-conditions')} ui-stacker-box`).should('have.length', 1);
      cy.get('gravity-condition-list-row-property [label=Field] .path-segments')
        .should('have.length', 1);
      cy.get('gravity-condition-list-row-property [label=Field] .path-segments')
        .contains('test_id');
      cy.get('gravity-condition-list-row-property [label=Operation] select')
        .should('have.value', 'EQUALTO');
      getUiSelect('gravity-condition-list-row-property [label=Value]').contains('9001');
      cy.get('gravity-condition-list-row-phase [label=Operation] select')
        .should('have.value', 'NOTEQUALTO');
      cy.get(`${dataCy('rule-start-conditions')} ui-stacker-box`).should('have.length', 1);
      getUiSelect('gravity-condition-list-row-phase [label=Value]').contains('archive');
      cy.get(`${dataCy('rule-completion-conditions')} ui-stacker-box`).should('have.length', 1);
      cy.get('gravity-condition-list-row-task [label=Operation] select')
        .should('have.value', 'COMPLETE');
      getUiSelect('gravity-condition-list-row-task [label=Value]').contains('test');
      cy.get('bmi-action-editor-list ui-stacker-box').should('have.length', 1);
      getUiSelect(dataCy('ui-form-field-Field containing CSV content')).contains('Test ID');

    });

    it('validation rule', () => {

      // Start at board summary page, select add rule
      cy.contains('Add').click();
      cy.contains('Rule').click({force: true});

      // select regular rule type
      pickUiOption(dataCy('rule-type'), 'Validation');

      // Help content for regular rules is showing
      cy.get(dataCy('rule-editor-help')).find('h3').contains('Validation Rules');

      // name, id, desc, are empty
      cy.get(dataCy('input-rule-name')).should('have.value', '');
      cy.get(dataCy('input-rule-id')).should('have.value', '');
      cy.get(dataCy('textarea-rule-description')).should('have.value', '');

      // No conditions or actions are defined.
      cy.get(`${dataCy('rule-trigger-conditions')} ui-stacker-box`).should('not.exist');
      cy.get('bmi-action-editor-list ui-stacker-box').should('not.exist');

      // Validation: save button is disabled
      cy.get(dataCy('rule-builder-save-button')).should('be.disabled');

      // Fill out rule form
      cy.get(dataCy('input-rule-name')).focus().type('e2e test validation rule');
      cy.get(dataCy('textarea-rule-description')).type('Rule description here (validation).');

      // Validation: save button should be enabled now
      cy.get(dataCy('rule-builder-save-button')).should('be.enabled');

      // check generated id
      cy.get(dataCy('input-rule-id')).should('have.value', 'e2e_test_validation_rule');

      // add conditions
      cy.get(dataCy('rule-trigger-conditions')).contains('Add condition').click();
      cy.contains('Task Status Condition').should('be.visible').click();
      pickUiOption('gravity-condition-list-row-task [label=Value]', 'test');

      // Add action
      cy.contains('Add Action').click();
      cy.contains('CSV Parser').should('be.visible').click();
      pickUiOption(dataCy('ui-form-field-Field containing CSV content'), 'Test ID');

      // Save
      rulesData.push(NEW_VALIDATION_RULE);
      cy.get(dataCy('rule-builder-save-button')).should('be.enabled').click();

      // Rule danger message
      cy.get('gravity-rule-save-modal h2')
        .should('be.visible')
        .contains('Confirm save rule?');
      // Confirm and save
      cy.get('gravity-rule-save-modal button')
        .contains('Save Rule')
        .should('be.enabled')
        .click();

      // Navigates to board page
      cy.get('.board-summary__layout');

      // Check that rule is added to the list
      cy.wait(['@rulesData', '@rulesData', '@rulesData', '@rulesData']);
      cy.get(dataCy('validation-rule')).its('length').should('eq', 1);
      cy.get(dataCy('validation-rule')).contains('e2e test validation rule');

      // View rule in editor.
      cy.get(dataCy('validation-rule'))
        .contains('e2e test validation rule')
        .click({force: true});

      // Check name, desc, and id have persisted correctly
      cy.get(dataCy('input-rule-name')).should('have.value', NEW_VALIDATION_RULE.name);
      cy.get(dataCy('input-rule-id')).should('have.value', NEW_VALIDATION_RULE.id);
      cy.get(dataCy('textarea-rule-description')).should('have.value', NEW_VALIDATION_RULE.description);

      // Rule type is still scheduled
      getUiSelect(dataCy('rule-type')).contains('Validation');

      // check 1 condition
      cy.get(`${dataCy('rule-trigger-conditions')} ui-stacker-box`).should('have.length', 1);

      // check 1 action
      cy.get('bmi-action-editor-list ui-stacker-box').should('have.length', 1);

    });

  });

  it('should display board details', () => {

    cy.get("h1").should('contain', boardName);
    cy.get(dataCy('compulsory-rule')).contains(ruleName)
  });


});
