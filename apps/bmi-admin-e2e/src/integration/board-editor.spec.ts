
import { RuleType } from '@bmi/gravity-services';
import { DataServer, DataServerOptions, RouteParam } from '@bmi/cypress/mocking/data-server';
import { getMock, postMock } from '@bmi/cypress/helper-functions';
const { _ } = Cypress
import { dataCy } from '@bmi/cypress/common-element-actions';

const RuleTypes = {
  validation: 'VALIDATION',
  compulsory: 'COMPULSORY',
  task: 'TASK',
  scheduled: 'SCHEDULED',
};

const isObject = (object: any): boolean => {
  return !!object && typeof object === 'object' && !!Object.keys(object).length;
}

const getChildren = ((object: object, recursive = false) => {
  let children;
  // debugger
  const childProperties = _.pickBy(object, isObject);
  children = Object.keys(childProperties)
  if (recursive) {
    for (const [key, val] of Object.entries(childProperties)) {
      const descendants = getChildren(val, true);
      children = [...children, ...descendants];
      console.log('children: ', children)
    }
  }
  return children;
})

describe('Test Board Editor', () => {
  const boardTitle = "Dick's Read Only Field Test";
  const boardName = 'dick_s_read_only_field_test';
  const gravityCacheStorageKey = 'gravityCache:/gravity/spring/board/configs';

  // Test data
  let testBoard;
  let testData;
  let configs;

  // Route Parameters
  const mockedRoutes: RouteParam[] = [
    getMock(
      '/gravity/spring/board/dick_s_read_only_field_test/rules/full',
      'allRules'
    ),
    getMock(
      '/gravity/spring/board/dick_s_read_only_field_test/resources',
    ),
    postMock(
      '/gravity/spring/board/dick_s_read_only_field_test/search?view=all',
      'cards',
      '{"conditions": {}}'
    )
  ];

  // const dataViewerRoutes: RouteParam[] = [
  //   {
  //     endPoint: '/gravity/spring/board/dick_s_read_only_field_test/search?view=all',
  //     alias:'cards',
  //     method: 'POST',
  //     payLoad: JSON.stringify({"conditions": {}}),
  //     enabled: true
  //   }
  // ]

  // selectors used throughout the tests
  const selectors = {
    boardName: dataCy('board-name'),
    boardType: dataCy('board-type'),
    boardClassification: dataCy('board-classification'),
    fieldLabel: dataCy('field-label'),
    permissionEntry: dataCy('permission-entry'),
    relatedBoardName: dataCy('related-board-name')
  };

  beforeEach(() => {
    // Prepare  test data
    const routes = [
      ...mockedRoutes
    ]
    const options: DataServerOptions = {
      basePath: 'board-editor',
      genericRoutes: ['app', 'descriptions', 'current-user', 'configs']
    };
    console.log(`routes: `, routes)

    let dataServer = new DataServer(routes, options);

    dataServer.waitforRoutesToBeMocked().subscribe(routes => {
      console.log('routes mocked')
      dataServer.readFixture('configs').then(response => {
        configs = response;
        console.log(typeof configs)
        console.log(`templates `, Object.keys(configs))
        testBoard = configs[boardName];
        testData = {
          boardTitle: '',
          fields: Object.values(testBoard.templates[boardName].fields),
          boardType: testBoard.boardType,
          boardClassification: '-',
          permissions: testBoard.permissions,
          rules: {}
        }
        if ( !window.sessionStorage.getItem(gravityCacheStorageKey)) {
          window.sessionStorage.setItem(gravityCacheStorageKey, JSON.stringify(configs));
        }
      })
      dataServer.readFixture('allRules').then(rules => {
        console.log(rules)
        testData.rules = rules;
      });
      dataServer.readFixture('cards').then(cards => {
        testData.cards = cards;
      })
    })
    // Open board editor
    // cy.wait(3000)
    cy.visit('/board/dick_s_read_only_field_test', {});
  });



  it('should show the details', () => {
    // cy.wait(3000)
    // return
    cy.get(selectors.boardName).should('contain', boardTitle);

    // fields
    cy.get(selectors.fieldLabel).should('have.length', testData.fields.length);

    // Rules
    // Check different type of rules
    const rulesByType = (type: RuleType) =>
      testData.rules.filter((rule) => rule.ruleType === type);
    cy.get(dataCy('task-rule')).should('have.length', 1)
    Object.entries(RuleTypes).map(([key, value]) => {
      const numOfRules = rulesByType(value as RuleType).length;
      console.log(value, ' => numOfRules: ', numOfRules)
      if (numOfRules === 0 ) {
        cy.get(dataCy(`${key}-rule`)).should('not.exist');
      } else {
        cy.get(dataCy(`${key}-rule`)).should(
          'have.length',
          numOfRules
        );
      }
    });

    // Details
    cy.get(selectors.boardType).contains(testData.boardType);
    cy.get(selectors.boardClassification).contains(
      testData.boardClassification
    );

    // Permissions
    const principals = Object.keys(testData.permissions);
    cy.get(selectors.permissionEntry)
      .should('have.length', principals.length)
      .each((element, index) => {
        cy.wrap(element)
          .should('contain', principals[index])
          .should('contain', testData.permissions[principals[index]]);
      });
    // Properties

    // Links

    // Related Boards
    // @ts-ignore
    const relatedFields = testData.fields.filter(field => field.referenceField === true);
    cy.get(selectors.relatedBoardName).should('have.length', relatedFields.length)
  });

  it('should show the board properties', () => {
    const selectors = {
      childPathHeading: dataCy('child-path-heading'),
      topLevelProperty: dataCy('root-level-property')
    }
    // Check top level properties
    const topLevelProperties = _.pickBy(testBoard, _.negate(isObject));
    const topLevelKeys = Object.keys(topLevelProperties).sort();

    cy.get(selectors.topLevelProperty)
      .should('have.length', Object.keys(topLevelProperties).length)
      .each((element, index) => {
        const actualValue = topLevelProperties[topLevelKeys[index]]
        const nullValues = [null, {}]
        const expectedValue = nullValues.find( val => val === actualValue) ? actualValue : '';
        cy.wrap(element)
          .should('contain', topLevelKeys[index])
          .should('contain',  expectedValue)
      })
  });

  it('should show board data', () => {
    const selectors = {
      boardDataSearcher: dataCy('board-data-searcher'),
      tabHeadingData: dataCy('tab-heading-data-viewer'),
      cardRow: '[data-table-col-index="0"]'
    }
    // dataViewerRoutes[0].enabled = true;
    // const server = new DataServer(dataViewerRoutes)
    cy.get(selectors.tabHeadingData).click();
    // server.waitForRoutes()
    // check if search component exists
    cy.get(selectors.boardDataSearcher).should('exist');

    const numOfCards = testData.cards.length;
    cy.get(selectors.cardRow).should('have.length', numOfCards)
    // cy.contains('DI000001').click()
    // board/dick_s_rea d_only_field_test/search?view=all
  });
});
