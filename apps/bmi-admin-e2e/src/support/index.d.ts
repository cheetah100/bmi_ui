declare namespace Cypress {
  interface Chainable {
    /**
     * Custom command to select DOM element by data-cy attribute.
     * @example cy.dataCy('greeting')
     */
    loadFixtures(routes: RouteParam[] | RouteParam): Chainable<any>;
    readFixture(name: string);
    loginTest(email: string, password: string): Chainable<Element>
  }
}
