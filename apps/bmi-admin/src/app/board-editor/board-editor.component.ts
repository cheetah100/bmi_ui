import { Subscription } from 'rxjs';
import {
  Component,
  OnDestroy,
  OnInit,
  Input,
  SimpleChanges,
  OnChanges
} from '@angular/core';
import {
  ConfirmationModalService,
  TypedFormGroup,
  TypedFormControl,
  IdpValidators
} from '@bmi/ui';

import dedent from 'ts-dedent';
import { BoardDetailsService } from './board-details.service';
import {
  GravityApiBoard,
  BoardConfigBuilder,
  Phases,
  Fields,
  Permissions,
  GravityApiTemplate
} from '@bmi/gravity-services';
import { guessPlural, guessSingular, probablyValidGravityId } from '@bmi/utils';
import {
  connectNameToIdFieldHelper,
  connectNameToControlFieldHelper
} from '../utils/connect-name-to-id-field-helper';

const NEW_BOARD: Partial<GravityApiBoard> = {
  boardType: 'OBJECT',
  permissions: {
    administators: 'ADMIN',
    readonly: 'READ'
  },
  phases: {
    current: {
      id: 'current',
      name: 'Current',
      description: 'Default current phase for this object',
      color: null,
      invisible: false,
      index: 1
    },
    archive: {
      id: 'archive',
      name: 'Archived',
      description: 'Default archive phase for this object',
      color: null,
      invisible: true,
      index: 2
    }
  },
  fields: {},
  templates: {}
};

const listItemsHaveRequiredFields = (values, requiredFields: string[]) => {
  if (!values) {
    return true;
  }
  return Object.values(values).every(value =>
    requiredFields.every(
      fieldName => !!value[fieldName] && !!value[fieldName].trim()
    )
  );
};

@Component({
  selector: 'bmi-board-editor',
  templateUrl: './board-editor.component.html',
  styleUrls: ['./board-editor.component.scss']
})
export class BoardEditorComponent implements OnDestroy, OnInit, OnChanges {
  @Input() board: GravityApiBoard;

  readonly form = new TypedFormGroup<Partial<GravityApiBoard>>({
    id: new TypedFormControl<string>(null, {
      validators: [
        IdpValidators.required,
        IdpValidators.satisfies(
          probablyValidGravityId,
          'Must be a valid Gravity ID (letters and numbers, underscores, no spaces or other punctuation)'
        )
      ]
    }),
    name: new TypedFormControl<string>(null, {
      validators: [IdpValidators.required, IdpValidators.mustBeNonBlank]
    }),
    boardType: new TypedFormControl<string>(),
    description: new TypedFormControl<string>(),
    history: new TypedFormControl<boolean>(),
    classification: new TypedFormControl<string>(),
    permissions: new TypedFormControl<Permissions>({}),
    phases: new TypedFormControl<Phases>(null, {
      validators: [
        IdpValidators.satisfies(
          (value: Phases) => listItemsHaveRequiredFields(value, ['id', 'name']),
          'Phases must have valid ID and Name (title) fields.'
        )
      ]
    }),
    fields: new TypedFormControl<Fields>(null, {
      validators: [
        IdpValidators.satisfies(
          (value: Fields) =>
            listItemsHaveRequiredFields(value, ['id', 'label']),
          'Fields must have valid ID and Label (title) fields.'
        )
      ]
    })
  });

  readonly singularControl = new TypedFormControl<string>();
  readonly pluralControl = new TypedFormControl<string>();

  readonly classificationOptions = [
    { text: 'Public', value: 'CLASSIFICATION_PUBLIC' },
    { text: 'Confidential', value: 'CLASSIFICATION_CONFIDENTIAL' },
    { text: 'Highly Confidential', value: 'CLASSIFICATION_HIGHLY_CONFIDENTIAL' },
    { text: 'Restricted', value: 'CLASSIFICATION_RESTRICTED' }
  ];

  readonly boardTypeOptions = [
    { text: 'Data Object', value: 'OBJECT' },
    { text: 'Optionlist', value: 'OPTIONLIST' }
  ];

  // start on fields tab
  public activeTab = 0;
  public isNewBoard = false;

  private subscription = new Subscription();

  creating = true;
  confirmationModalAction: string;
  saving = false;

  public showImpacted = false;
  public hasBoardLinks = false;
  public isOptionlist = false;
  public savedPhases: string[];

  constructor(
    private service: BoardDetailsService,
    private confirmationModalService: ConfirmationModalService
  ) {}

  isActive(tab: number) {
    return tab === this.activeTab;
  }
  activateTab(tab: number) {
    this.activeTab = tab;
  }

  /**
   * Gets the validity status of the form.
   *
   * If this is true, it should be safe to call save() on this component.
   */
  public get isValid(): boolean {
    return this.form && this.form.valid;
  }

  get template() {
    if (this.board && this.board.templates) {
      return this.board.templates[this.board.id];
    }
    return null;
  }

  get hasUnsavedChanges() {
    return this.form && this.form.dirty && this.form.touched;
  }

  ngOnInit(): void {
    this.subscription
      .add(
        connectNameToIdFieldHelper(
          this.form.controls.name,
          this.form.controls.id
        )
      )
      .add(
        connectNameToControlFieldHelper<string>(
          this.form.controls.name,
          this.singularControl,
          guessSingular
        )
      )
      .add(
        connectNameToControlFieldHelper<string>(
          this.form.controls.name,
          this.pluralControl,
          guessPlural
        )
      );
  }

  ngOnChanges(changes: SimpleChanges) {
    // TODO: fix the initialization.
    this.isNewBoard = !this.board;
    this.form.reset(this.board || NEW_BOARD);

    if (this.isNewBoard) {
      // This is a new board, so we can enable ID editing, and we want to start
      // on the "General" tab.
      this.form.get('id').enable();
      this.activeTab = 0;
    } else {
      this.form.get('id').disable();
      this.activeTab = 1;
      const template = this.template;
      if (template && template.ui) {
        this.singularControl.patchValue(template.ui.singular);
        this.pluralControl.patchValue(template.ui.plural);
      }
    }

    const name = this.form.value.name;
    if (name) {
      if (!this.singularControl.value) {
        this.singularControl.patchValue(guessSingular(name));
      }
      if (!this.pluralControl.value) {
        this.pluralControl.patchValue(guessPlural(name));
      }
    }

    this.form.controls.fields.patchValue(this.gatherFields(), {emitEvent: false});
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  gatherFields(): Fields {
    const template = this.template;
    if (template) {
      return template.fields;
    }
    return {};
  }

  private updatePluralsFromTemplate() {

  }

  get fields() {
    return this.form.controls.fields;
  }

  pageTitle(): string {
    return this.board ? 'Edit ' + this.board.name + ' Board' : 'Create Board';
  }


  save(): GravityApiBoard {
    const originalConfig = this.board || (NEW_BOARD as GravityApiBoard);
    const value = this.form.getRawValue() as GravityApiBoard;
    const existingTemplate =
      originalConfig.templates[originalConfig.id] || ({} as GravityApiTemplate);
    const existingTemplateUi = existingTemplate.ui || {};

    // The BoardConfigBuilder lets us update our existing board config, or
    // create a new one. This will handle a lot of the grunt work, while
    // creating templates, groups etc. for the fields.
    //
    // By using the existing config as a starting point this will attempt to
    // preserve as much information as possible if the UI doesn't support it,
    // rather than stomping over settings it doesn't know about.
    const builder = new BoardConfigBuilder(originalConfig)
      .id(value.id || this.service.generateGravityId(value.name))
      .name(value.name)
      .permissions(value.permissions)
      .assign({
        boardType: value.boardType,
        description: value.description,
        classification: value.classification,
        history: value.history,
        orderField: value.orderField
      });

    builder.defaultTemplate(t =>
      t.assign({
        cardTitle: existingTemplate.cardTitle || null,
        cardTitleFormat: existingTemplate.cardTitleFormat || null,
        prefix: existingTemplate.prefix
      })
      .ui({
        businessContext: existingTemplateUi.businessContext,
        singular: this.singularControl.value,
        plural: this.pluralControl.value,
        color: existingTemplateUi.color,
        icon: existingTemplateUi.icon
      })
    );

    Object.values(value.phases).forEach(phase =>
      builder.addPhase(
        phase.id || this.service.generateGravityId(phase.name),
        p => p
          .name(phase.name)
          .index(phase.index)
          .invisible(phase.invisible)
          .assign({
            invisible: existingTemplateUi.invisible,
            color: phase.color,
            description: phase.description
          })
      )
    );

    Object.values(value.fields).forEach(field => {
      builder.addField(
        field.id || this.service.generateGravityId(field.name),
        f => f
            .name(field.name)
            .label(field.label)
            .type(field.type as any)
            .control('INPUT')
            .optionlist(field.optionlist)
            .required(field.required)
            .index(field.index)
            .assign({
              description: field.description,
              immutable: field.immutable,
              requiredPhase: field.requiredPhase,
              optionlistfilter: field.optionlistfilter,
              length: field.length
            })
      );
    });

    // If we're modifying an existing config, we should now get rid of any
    // fields or phases that aren't in the current form value, as this would
    // suggest they have been deleted by the user.
    const fieldsOnForm = Object.values(value.fields)
      .map(f => f.id)
      .filter(f => !!f);
    const phasesOnForm = Object.values(value.phases)
      .map(p => p.id)
      .filter(p => !!p);
    const originalPhases = this.board ? Object.keys(this.board.phases) : [];

    // We won't rely on `this.board.fields` to exist/be correct at the moment,
    // so instead scan through all the templates -> groups and collect up the
    // field IDs
    const originalFields: string[] = [];
    if (this.board) {
      for (const template of Object.values(this.board.templates)) {
        for (const group of Object.values(template.groups)) {
          originalFields.push(...Object.keys(group.fields));
        }
      }
    }

    originalPhases
      .filter(phase => !phasesOnForm.includes(phase))
      .forEach(phase => builder.deletePhase(phase));

    originalFields
      .filter(field => !fieldsOnForm.includes(field))
      .forEach(field => builder.deleteField(field));

    return builder.finish();
  }

  showDeleteConfirmation(event) {
    const phaseObject = event.item.getRawValue();
    const phaseName = phaseObject.name as string;
    if (this.savedPhases) {
      if (this.savedPhases.includes(phaseObject.id)) {
        /** Checks whether the phase to be deleted exists in gravity */
        this.service
          .getCardCountByPhase(this.board.id, phaseObject.id)
          .subscribe(response => {
            const cardCount = response.cards as number;
            if (response.cards > 0) {
              /* Show a warning message to the admin that the board currently has
               * more than 1 card in the phase to be deleted.
               *
               * TODO: Show a dropdown of target phases to the admin which the
               * cards to be moved to.
               */
              this.confirmationModalService
                .show({
                  content: dedent`
                ## Cannot remove ${phaseName} ##

                There are currently ${cardCount} cards in the ${phaseName} phase
                on this board. You must move these cards to a different phase
                before this phase can be removed.

                This isn't really supported from the Gravity Admin UI at the
                moment so you'll probably need to talk to a dev about this
                (sorry).
              `,

                  buttonYesText: 'Um. Ok'
                })
                .subscribe(() => {
                  // This URL doesn't exist in Gravity admin so um, yeah. Whatever for now I guess.
                  // this.router.navigate(['table/' + this.board.id]);
                });
            } else {
              this.confirmationModalService
                .show({
                  content: dedent`
                ## Confirm removal of ${phaseName} ##

                There are no cards in this phase, so it can be removed. Are you
                sure you want to remove the ${phaseName} phase?
              `,
                  buttonYesText: 'Remove Phase'
                })
                .subscribe(() => {
                  const phasesValue = this.form.value.phases;
                  delete phasesValue[phaseObject.id];
                  this.form.get('phases').patchValue(phasesValue);
                });
            }
          });
      } else {
        this.confirmationModalService
          .show({
            content: dedent`
            ## Confirm removal of ${phaseName} ##

            Are you sure you want to remove this phase?
          `,
            buttonYesText: 'Remove Phase'
          })
          .subscribe(() => {
            // this.phases.removeAt(
            //   this.phases
            //     .getRawValue()
            //     .findIndex(phase => phase.id === phaseObject.id)
            // );
          });
      }
    }
  }
}
