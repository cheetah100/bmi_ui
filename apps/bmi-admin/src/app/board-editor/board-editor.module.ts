import { NgModule } from '@angular/core';
import { BoardEditorComponent } from './board-editor.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedUiModule } from '@bmi/ui';
import { GravityModule, GravityUiModule } from '@bmi/gravity-services';
import { FieldEditorListComponent } from './field-editor-list.component';
import { PhaseEditorListComponent } from './phase-editor-list.component';
import { FieldEditorComponent } from './field-editor.component';
import { PhaseEditorComponent } from './phase-editor.component';

@NgModule({
  declarations: [
    BoardEditorComponent,
    FieldEditorListComponent,
    PhaseEditorListComponent,
    FieldEditorComponent,
    PhaseEditorComponent
  ],

  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedUiModule,
    GravityModule,
    GravityUiModule
  ],

  exports: [BoardEditorComponent]
})
export class BoardEditorModule {}
