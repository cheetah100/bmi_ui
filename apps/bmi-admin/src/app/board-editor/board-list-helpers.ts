import { TemplatedFormArray } from '@bmi/ui';

const orderIndices = (
  formArray: TemplatedFormArray<unknown>,
  emitEvent: boolean = false
) => {
  formArray.controls.forEach((control, i: number) => {
    control.patchValue(Object.assign({}, control.value, { index: i + 1 }), {
      emitEvent
    });
  });
};

interface IdObject {
  id?: string;
}

const arrayToIdKeyedObject = <T extends IdObject>(
  arrayValue: T[]
): { [id: string]: T } => {
  const obj: { [id: string]: T } = {};
  arrayValue.forEach((value: T) => {
    if (value && value.id) {
      obj[value.id] = value;
    }
  });
  return obj;
};

export { orderIndices, arrayToIdKeyedObject };
