import {
  Component,
  forwardRef,
  ElementRef,
  ViewChild,
  OnInit,
  OnDestroy
} from '@angular/core';
import {
  provideAsControlValueAccessor,
  BaseControlValueAccessor,
  TypedFormGroup,
  TypedFormControl,
  SimpleCloseDialogService,
  IdpValidators
} from '@bmi/ui';
import { Phase, PhaseViewModel } from '@bmi/gravity-services';
import { Subscription, timer } from 'rxjs';
import { connectNameToIdFieldHelper } from '../utils/connect-name-to-id-field-helper';
import { filter } from 'rxjs/operators';
import { probablyValidGravityId } from '@bmi/utils';

export const PHASE_DEFAULT_VALUES = {
  id: null,
  name: null,
  index: null,
  isNew: true
};

@Component({
  selector: 'bmi-phase-editor',
  templateUrl: './phase-editor.component.html',
  styleUrls: ['./phase-editor.component.scss'],
  providers: [
    provideAsControlValueAccessor(forwardRef(() => PhaseEditorComponent))
  ]
})
export class PhaseEditorComponent extends BaseControlValueAccessor<PhaseViewModel>
  implements OnInit, OnDestroy {

  editingTitle = false;
  private originalPhase: PhaseViewModel = null;
  readonly form = new TypedFormGroup<Partial<PhaseViewModel>>({
    id: new TypedFormControl<string>(null, {
      validators: [
        IdpValidators.required,
        IdpValidators.satisfies(
          probablyValidGravityId,
          'Must be a valid Gravity ID (letters and numbers, underscores, no spaces or other punctuation)'
        )
      ]
    }),
    name: new TypedFormControl<string>(null, {
      validators: [IdpValidators.required, IdpValidators.mustBeNonBlank]
    }),
    index: new TypedFormControl(null),
    invisible: new TypedFormControl(false)
  });
  private subscription = new Subscription();

  get hasValidName() {
    return this.form.controls.name.valid;
  }

  ngOnInit() {
    connectNameToIdFieldHelper(this.form.controls.name, this.form.controls.id);
    this.subscription.add(
      this.form.valueChanges
        .pipe(filter(() => !this.isWriteValueInProgress))
        .subscribe(() =>
          this.onChanged(this.mergeWithOriginal(this.form.getRawValue()))
        )
    );
    this.form.controls.index.disable();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  mergeWithOriginal(value: Partial<PhaseViewModel>): PhaseViewModel {
    return Object.assign({}, this.originalPhase, value);
  }

  updateComponentValue(phase: PhaseViewModel) {
    this.originalPhase = phase || PHASE_DEFAULT_VALUES;
    this.form.patchValue(this.originalPhase, {
      emitEvent: false
    });
    if (this.originalPhase.id && !this.originalPhase.isNew) {
      this.form.get('id').disable();
    }

    if (!this.originalPhase.name) {
      this.editingTitle = true;
    }
  }
}
