import dedent from 'ts-dedent';

export const BOARD_EDITOR_HELP = dedent `
  # Board Editor #

  ## Foundational Properties ##

  ### Singular and Plural ###

  If left blank, IDP will guess spellings for these using Title. This may be
  incorrect in some cases.

  ### Card Title Format ###

  Existing field names in the format string will be substituted with the card's value for that field.

  ### Card title ###
  A list of fields required for the card title format substitutions. (Not useful to see, but required to pass QA testing).

  ### Prefix ###
  The first part of the card id. This will be auto-generated if left empty.


  ## Phases ##

  Define the phases for this object. Phases default to having a current
  and archive. The first "Visible" phase listed will be the Default Phase
  for the board i.e. any card added will be added to the default phase.

  ### Invisible Phases ###

  If a phase has been marked as invisible, any cards in it will not be shown to
  users, as if they have been deleted from the system. By default, the archive
  phase is invisible.


  ## Fields ##

  These are the 'columns' on the cards. Fields can contain regular data such as
  text, numbers or booleans, or can reference other cards in Gravity by
  specifying the related *Optionlist* board.

  **TODO: add more help for configuring fields.**
`;