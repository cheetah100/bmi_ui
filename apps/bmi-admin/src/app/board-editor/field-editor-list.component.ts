import { Component, OnInit, forwardRef, HostListener } from '@angular/core';
import {
  TemplatedFormArray,
  TypedFormControl,
  BaseControlValueAccessor,
  provideAsControlValueAccessor,
  SimpleCloseDialogService
} from '@bmi/ui';
import { Field, Fields } from '@bmi/gravity-services';
import { filter } from 'rxjs/operators';
import { orderIndices, arrayToIdKeyedObject } from './board-list-helpers';
import sortBy from 'lodash-es/sortBy';

import { FIELD_DEFAULT_VALUES } from './field-editor.component';

@Component({
  selector: 'bmi-field-editor-list',
  providers: [
    provideAsControlValueAccessor(forwardRef(() => FieldEditorListComponent))
  ],
  template: `
    <ui-stacker-list
      [items]="formArray"
      (orderChanged)="orderIndices(true)"
      (itemDeleted)="orderIndices(true)"
    >
      <ng-template let-fieldControl>
        <bmi-field-editor [formControl]="fieldControl"></bmi-field-editor>
      </ng-template>
    </ui-stacker-list>

    <button type="button" class="btn btn--success" (click)="addField()">
      <span class="icon-plus"></span>
      Add Field
    </button>
  `
})
export class FieldEditorListComponent extends BaseControlValueAccessor<Fields>
  implements OnInit {
  readonly formArray = new TemplatedFormArray<Field>(
    () => new TypedFormControl(null)
  );

  constructor(private dialogService: SimpleCloseDialogService) {
    super();
  }

  ngOnInit() {
    this.formArray.valueChanges
      .pipe(filter(() => !this.isWriteValueInProgress))
      .subscribe(value => this.onChanged(arrayToIdKeyedObject(value)));
  }

  orderIndices(emit: boolean = false) {
    orderIndices(this.formArray, emit);
  }

  updateComponentValue(fields: Fields) {
    this.formArray.patchValue(sortBy(Object.values(fields || {}), 'index'), {
      emitEvent: false
    });
    this.orderIndices();
  }

  addField() {
    this.formArray.pushValue({...FIELD_DEFAULT_VALUES}, { emitEvent: false });
    this.orderIndices(true);
  }

  @HostListener('keyup', ['$event']) onKeyUp(e: KeyboardEvent) {
    if (e.key === 'Enter' || e.key === 'Escape') {
      this.dialogService.triggerClose();
    }
  }
}
