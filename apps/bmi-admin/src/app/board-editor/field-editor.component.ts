import {
  Component,
  forwardRef,
  OnInit,
  HostListener,
  ViewChild,
  ElementRef,
  OnDestroy
} from '@angular/core';
import {
  provideAsControlValueAccessor,
  BaseControlValueAccessor,
  TypedFormGroup,
  TypedFormControl,
  IdpValidators
} from '@bmi/ui';
import { Field, FieldViewModel } from '@bmi/gravity-services';
import { filter, tap } from 'rxjs/operators';
import { connectNameToIdFieldHelper } from '../utils/connect-name-to-id-field-helper';
import { BoardDetailsService } from './board-details.service';
import { Option, probablyValidGravityId } from '@bmi/utils';
import { timer, Subscription } from 'rxjs';
import sortBy from 'lodash-es/sortBy';

export const FIELD_DEFAULT_VALUES: FieldViewModel = {
  id: null,
  label: null,
  name: null,
  type: 'STRING',
  immutable: false,
  isNew: true
};


const OPTIONLIST_FIELD_TYPES = ['STRING', 'LIST'];

function isOptionlistCompatibleFieldType(fieldType: string) {
  return OPTIONLIST_FIELD_TYPES.includes(fieldType);
}

@Component({
  selector: 'bmi-field-editor',
  providers: [
    provideAsControlValueAccessor(forwardRef(() => FieldEditorComponent))
  ],
  templateUrl: './field-editor.component.html',
  styleUrls: ['./field-editor.component.scss']
})
export class FieldEditorComponent extends BaseControlValueAccessor<FieldViewModel>
  implements OnInit, OnDestroy {
  boardOptions: Option[] = [];
  readonly typeOptions = [
    { text: 'String', value: 'STRING' },
    { text: 'List', value: 'LIST' },
    { text: 'Map', value: 'MAP' },
    { text: 'Number', value: 'NUMBER' },
    { text: 'Boolean', value: 'BOOLEAN' },
    { text: 'Date', value: 'DATE' }
  ];

  readonly optionlistTypeOptions = this.typeOptions.filter(t => isOptionlistCompatibleFieldType(t.value));
  editingTitle = false;
  private originalField: FieldViewModel = null;
  readonly form = new TypedFormGroup<Partial<FieldViewModel>>({
    id: new TypedFormControl<string>(null, {
      validators: [
        IdpValidators.required,
        IdpValidators.satisfies(
          probablyValidGravityId,
          'Must be a valid Gravity ID (letters and numbers, underscores, no spaces or other punctuation)'
        )
      ]
    }),
    label: new TypedFormControl<string>(null, {
      validators: [IdpValidators.required, IdpValidators.mustBeNonBlank]
    }),
    type: new TypedFormControl<string>('STRING', {
      validators: IdpValidators.required
    }),
    required: new TypedFormControl(false),
    optionlist: new TypedFormControl(null),
    index: new TypedFormControl(null),
    indexed: new TypedFormControl(false),
    description: new TypedFormControl(null),
    immutable: new TypedFormControl(false),
  });
  private subscription = new Subscription();

  get hasValidTitle() {
    return this.form.controls.label.valid;
  }

  constructor(
    private boardDetailsService: BoardDetailsService,
  ) {
    super();
  }

  ngOnInit() {
    this.boardOptions = [
      { text: 'None', value: null },
      ...sortBy(this.boardDetailsService.boardOptions(), 'text')
    ];

    this.subscription.add(connectNameToIdFieldHelper(this.form.controls.label, this.form.controls.id));
    this.subscription.add(
      this.form.valueChanges
        .pipe(filter(() => !this.isWriteValueInProgress))
        .subscribe(value => {
          if (!!value.optionlist && !isOptionlistCompatibleFieldType(value.type)) {
            this.form.controls.type.patchValue('STRING');
          }
          this.onChanged(this.mergeWithOriginal(this.form.getRawValue()));
        })
    );
    this.form.controls.index.disable();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  mergeWithOriginal(value: Partial<FieldViewModel>): FieldViewModel {
    return Object.assign({}, this.originalField, value);
  }

  get shouldOpenAdvancedByDefault(): boolean {
    const value = this.form.value;
    return value && (value.immutable || value.indexed);
  }

  onIsEditingUpdated(value: boolean) {
    this.editingTitle = value;
  }

  updateComponentValue(field: FieldViewModel) {
    this.originalField = field || FIELD_DEFAULT_VALUES;
    this.form.patchValue(this.originalField, {
      emitEvent: false
    });
    if (!!this.originalField.id && this.originalField.isNew !== true) {
      this.form.get('id').disable();
    }

    // If we think this is a new field, then we will make its title
    // editable. Doing this 'synchronously' has issues though because it will open, but
    // for some reason
    if (this.originalField.isNew && !this.originalField.label) {
      this.editingTitle = true;
    }
  }
}
