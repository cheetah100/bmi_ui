import { AbstractControl } from '@angular/forms';

export function ValidateUnique(control: AbstractControl) {
  if (control['controls'] && control['controls'][0] && control['controls'][0]['controls']) {
    const arraylength = control['controls'].length;
    const array = [];
    for (let i = 0; i < arraylength; i++) {
      array.push(control['controls'][i]['controls']['index'].value);
    }
    if (!checkIfArrayIsUnique(array)) {
      return { duplicateIndices: true };
    }
    return null;
  }
}

function checkIfArrayIsUnique(arr) {
  const myArray = arr.sort();
  for (let i = 0; i < myArray.length; i++) {
    if (myArray.indexOf(myArray[i]) !== myArray.lastIndexOf(myArray[i])) {
      return false;
    }
  }
  return true;
}
