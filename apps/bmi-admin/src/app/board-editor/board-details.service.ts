import { Injectable } from '@angular/core';
import {
  Gravity,
  GravityApiBoard,
  GravityApiPhase,
  GravityService,
  GravityUrlService,
  Permissions
} from '@bmi/gravity-services';

//import { MatrixService } from '@admin/permissions/matrix.service';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap, switchMap, map } from 'rxjs/operators';

//import { NotifierService } from 'angular-notifier';

import { BoardConfig } from '@bmi/gravity-services';
import { Option } from '@bmi/ui';


// TODO: the contents of this service need to be moved into @bmi/gravity-services
//       so that the host app has more control over this proccess.
//
//       The overall process of updating a board should be made simple enough to
//       do in a single network request, however that might not be possible.


export interface Phase extends GravityApiPhase {
  id: string;
  name: string;
  description: string;
  color: string;
  invisible: boolean;
  index: number;
  default: boolean;
  transitions: string[];
  path?: string;
}

@Injectable()
export class BoardDetailsService {

  constructor(
    private gravity: Gravity,
    private gravityService: GravityService,
    //private matrixService: MatrixService,
    private httpClient: HttpClient,
    //private notifierService: NotifierService,
  ) {}

  notify(type: string, message: string) {
    // TODO
    console.log(type, message);
  }

  // true if other boards have optionlist fields pointing to this board
  getHasBoardLinks(boardId: string): boolean {
    return this.gravity.configService.getBoardConfigs()
      .some(config => Object.values(config.fields).some(field => field.optionlist && field.optionlist === boardId));
  }

  boardOptions(): Option[] {
    return this.gravity.configService.getBoardConfigs()
      .map(config => ({
        text: config.name,
        value: config.id,
      }));
  }

  loadBoard(boardId: string, refresh: boolean): Observable<GravityApiBoard> {
    if (refresh) {
      return this.httpClient.get<GravityApiBoard>(this.gravity.url.board(boardId).baseUrl).pipe(
        // This done to update the cache when the board config is loaded
        // A side effect is that the cache is reloaded when running in another tab
        tap(boardConfig => this.gravity.configService.registerBoard(boardConfig))
      );
    } else {
      return of(this.gravity.configService.getBoardConfig(boardId).config);
    }
  }

  savePermissions(boardId: string, permissions: Permissions): Observable<Permissions> {
    return this.httpClient.post<Permissions>(this.gravity.url.board(boardId).url('/permissions' + '?replace=true'), permissions);

  }

  generateGravityId(name: string): string {
    return name.toLowerCase().trim().replace(/\W+/g, '_');
  }

  savePhase(board: GravityApiBoard, phase: Phase): Observable<Phase> {
    let request: Observable<Phase>;
    if (phase.id) {
      delete phase.path;
      request = this.saveExistingPhase(board.id, phase);
    } else {
      phase.id = this.generateGravityId(phase.name);
      request = this.saveNewPhase(board.id, phase);
    }
    return request.pipe(
      catchError((err: HttpErrorResponse) => {
        this.notify('error', `Unable to save phase ${phase.name}.`);
        return of(null);
      }),
    );
  }

  saveNewPhase(boardId: string, phase: Phase): Observable<Phase> {
    return this.httpClient.post<Phase>(this.gravity.url.board(boardId).phases(), phase);
  }

  saveExistingPhase(boardId: string, phase: Phase): Observable<Phase> {
    return this.httpClient.put<Phase>(this.gravity.url.board(boardId).phase(phase.id), phase);
  }

  deletePhase(boardId: string, phase: Phase): Observable<void> {
    return this.httpClient.delete(this.gravity.url.board(boardId).phase(phase.id)).pipe(
      catchError((err: HttpErrorResponse) => {
        this.notify('error', `Unable to delete phase ${phase.name}.`);
        return of(null);
      })
    );
  }

  getCardCountByPhase(boardId: string, phaseId: string): Observable<any> {
    return this.httpClient.get(this.gravity.url.board(boardId).phase(phaseId));
  }

  saveBoard(board: GravityApiBoard, isNew: boolean): Observable<BoardConfig> {
    let boardSource = !isNew ?
        this.httpClient.put<GravityApiBoard>(this.gravity.url.board(board.id).baseUrl, board) :
        this.httpClient.post<GravityApiBoard>(this.gravity.url.url('/board'), board);

    return boardSource.pipe(
      catchError((err: HttpErrorResponse) => {
        this.notify('error', `Unable to save board ${board.name}.`);
        return of(null);
      }),
      switchMap(() => this.httpClient.get<GravityApiBoard>(this.gravity.url.board(board.id).baseUrl)),
      map(boardConfig => this.gravity.configService.registerBoard(boardConfig, true)),
      tap(() => {
        // This is a hack to force the board configs to refresh
        return this.gravityService.invalidateBoardConfigs()
      })
    );
  }

  saveNewTemplate(boardId: string, template: any): Observable<any> {
    return this.httpClient.post(this.gravity.url.board(boardId).templates(), template);
  }

  saveExistingTemplate(boardId: string, template: any): Observable<any> {
    return this.httpClient.put(this.gravity.url.board(boardId).template(boardId), template);
  }

  /**
   * Reload the config for a board.
   */
  reloadBoardConfig(boardId: string): Observable<BoardConfig> {
    return this.gravity.boards.getBoardConfig(boardId).pipe(
      map(config => {
        const boardConfig = this.gravity.configService.registerBoard(config, true);
        if (boardConfig) {
          this.gravity.cache.removeAll(boardConfig.id);
        }
        return boardConfig;
      })
    );
  }

}
