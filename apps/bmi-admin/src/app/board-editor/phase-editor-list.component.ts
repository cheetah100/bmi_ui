import { Component, OnInit, forwardRef, HostListener } from '@angular/core';
import { Phase, Phases } from '@bmi/gravity-services';
import {
  TemplatedFormArray,
  TypedFormControl,
  BaseControlValueAccessor,
  provideAsControlValueAccessor,
  SimpleCloseDialogService
} from '@bmi/ui';
import { filter } from 'rxjs/operators';
import { orderIndices, arrayToIdKeyedObject } from './board-list-helpers';
import sortBy from 'lodash-es/sortBy';
import { PHASE_DEFAULT_VALUES } from './phase-editor.component';

@Component({
  selector: 'bmi-phase-editor-list',
  providers: [
    provideAsControlValueAccessor(forwardRef(() => PhaseEditorListComponent))
  ],
  template: `
    <ui-stacker-list
      [items]="formArray"
      (orderChanged)="orderIndices(true)"
      (itemDeleted)="orderIndices(true)"
    >
      <ng-template let-phaseControl>
        <bmi-phase-editor [formControl]="phaseControl"></bmi-phase-editor>
      </ng-template>
    </ui-stacker-list>

    <button type="button" class="btn btn--success" (click)="addPhase()">
      <span class="icon-plus"></span>
      Add Phase
    </button>
  `
})
export class PhaseEditorListComponent extends BaseControlValueAccessor<Phases>
  implements OnInit {
  readonly formArray = new TemplatedFormArray<Phase>(
    () => new TypedFormControl(null)
  );

  constructor(private dialogService: SimpleCloseDialogService) {
    super();
  }

  ngOnInit() {
    this.formArray.valueChanges
      .pipe(filter(() => !this.isWriteValueInProgress))
      .subscribe(value => this.onChanged(arrayToIdKeyedObject(value)));
  }

  updateComponentValue(phases: Phases) {
    this.formArray.patchValue(sortBy(Object.values(phases || {}), 'index'), {
      emitEvent: false
    });
    this.orderIndices();
  }

  orderIndices(emit: boolean = false) {
    orderIndices(this.formArray, emit);
  }

  addPhase() {
    this.formArray.pushValue({ ...PHASE_DEFAULT_VALUES }, { emitEvent: false });
    this.orderIndices(true);
  }

  @HostListener('keyup', ['$event']) onKeyUp(e: KeyboardEvent) {
    if (e.key === 'Enter' || e.key === 'Escape') {
      this.dialogService.triggerClose();
    }
  }
}
