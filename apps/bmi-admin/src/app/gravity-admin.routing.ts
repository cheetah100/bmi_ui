import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BoardHomeComponent } from './pages/board-home/board-home.component';
import { BoardShellComponent } from './pages/board-home/board-shell.component';
import { BoardDataViewerShimComponent } from './pages/board-data-viewer-shim/board-data-viewer-shim.component';
import { BoardEditorShimComponent } from './pages/board-editor-shim/board-editor-shim.component';
import { CardDataViewerShimComponent } from './pages/board-data-viewer-shim/card-data-viewer-shim.component';
import { TransformHomeComponent } from './pages/transform-home/transform-home.component';
import { TransformPreviewShimComponent } from './pages/transform-home/transform-preview-shim.component';
import { BoardSummaryComponent } from './pages/board-home/board-summary.component';
import { BoardResourcePageComponent } from './pages/board-resource-page/board-resource-page.component';
import { SystemPageComponent } from './pages/system-page/system-page.component';

import { RuleBuilderShimComponent } from './pages/rule-builder-shim/rule-builder-shim.component';
import { ViewSummaryComponent } from './pages/view-home/view-summary.component';
import { ViewEditorShimComponent } from './pages/view-home/view-editor-shim.component';
import { FilterEditorShimComponent } from './pages/filter-editor-shim/filter-editor-shim.component';
import { WidgetEditorShimComponent } from './pages/widget-editor-shim/widget-editor-shim.component';
import { ModuleHubPageComponent } from './pages/module-hub/module-hub-page.component';
import { WidgetHomeComponent } from './pages/widget-home/widget-home.component';
import { MenuPageComponent } from './pages/menu-page/menu-page.component';
import { ModuleInfoEditorComponent } from './pages/module-info-editor/module-info-editor.component';
import { ModuleInfoResolver } from './pages/module-info-editor/module-info.resolver';

import { PageDesignerPageComponent } from './pages/page-designer/page-designer-page.component';
import { RootHomeComponent } from './pages/root-home/root-home.component';

import { HasUnsavedChangesGuard } from './has-unsaved-changes.guard';
import { IntegrationEditorPageComponent } from './pages/integration-editor-page/integration-editor-page.component';
import { RuleDebuggerComponent } from './rule-builder/rule-debugger/rule-debugger.component';
import { IntegrationsHomeComponent } from './pages/integrations-home/integrations-home.component';
import { TransformEditorPageComponent } from './pages/transform-editor/transform-editor-page.component';
import { CredentialEditorPageComponent } from './pages/credential-editor-page/credential-editor-page.component';

@NgModule({
  imports: [
    RouterModule.forRoot([
      {
        path: '',
        component: RootHomeComponent
      },
      {
        path: 'module/add',
        component: ModuleInfoEditorComponent
      },
      {
        path: 'module/edit/:moduleId',
        component: ModuleInfoEditorComponent,
        resolve: {
          moduleData: ModuleInfoResolver
        }
      },
      {
        path: 'board',
        component: BoardHomeComponent
      },
      {
        path: 'board/create',
        component: BoardEditorShimComponent,
        canDeactivate: [HasUnsavedChangesGuard]
      },
      {
        path: 'board/:boardId',
        component: BoardShellComponent,
        children: [
          {
            path: '',
            component: BoardSummaryComponent,
          },
          {
            path: 'edit',
            component: BoardEditorShimComponent,
            canDeactivate: [HasUnsavedChangesGuard]
          },
          {
            path: 'data-viewer',
            component: BoardDataViewerShimComponent
          },
          {
            path: 'data-viewer/:cardId',
            component: CardDataViewerShimComponent
          },
          {
            path: 'add-rule',
            component: RuleBuilderShimComponent,
          },
          {
            path: 'rules/:ruleId',
            component: RuleBuilderShimComponent
          },
          {
            path: 'view/create',
            component: ViewEditorShimComponent,
          },
          {
            path: 'view/:viewId',
            component: ViewSummaryComponent,
          },
          {
            path: 'view/:viewId/edit',
            component: ViewEditorShimComponent,
          },
          {
            path: 'view/:viewId/preview',
            component: BoardDataViewerShimComponent,
          },
          {
            path: 'filter/:filterId',
            component: FilterEditorShimComponent
          },
          {
            path: 'filter',
            component: FilterEditorShimComponent
          },
          {
            path: 'resources/:resourceId',
            component: BoardResourcePageComponent,
            canDeactivate: [HasUnsavedChangesGuard],
          },
          {
            path: 'add-resource',
            component: BoardResourcePageComponent,
            canDeactivate: [HasUnsavedChangesGuard],
          }
        ]
      },
      {
        path: 'transform',
        component: TransformHomeComponent,
      },
      {
        path: 'transform/create',
        component: TransformEditorPageComponent,
      },
      {
        path: 'transform/:transformId/edit',
        component: TransformEditorPageComponent,
      },
      {
        path: 'transform/:transformId/preview',
        component: TransformPreviewShimComponent,
      },
      {
        path: 'system',
        component: SystemPageComponent,
      },
      {
        path: 'system/integrations',
        component: IntegrationsHomeComponent,
      },
      {
        path: 'system/integrations/:integrationId/edit',
        component: IntegrationEditorPageComponent,
        canDeactivate: [HasUnsavedChangesGuard],
      },
      {
        path: 'system/add-integration',
        component: IntegrationEditorPageComponent,
      },
      {
        path: 'system/credentials/:credentialId/edit',
        component: CredentialEditorPageComponent,
      },
      {
        path: 'system/add-credential',
        component: CredentialEditorPageComponent,
      },
      {
        path: 'module/:moduleId/widget',
        component: WidgetHomeComponent,
      },
      {
        path: 'module/:moduleId/widget/:widgetId',
        component: WidgetEditorShimComponent
      },
      {
        path: 'module/:moduleId/add-widget',
        component: WidgetEditorShimComponent
      },
      {
        path: 'module/:moduleId/menu',
        component: MenuPageComponent,
      },
      {
        path: 'module/:moduleId',
        component: ModuleHubPageComponent,
      },
      {
        path: 'module/:moduleId/pages/:pageId',
        component: PageDesignerPageComponent,
        canDeactivate: [HasUnsavedChangesGuard]
      },
      {
        path: 'rule-debugger-demo/:boardId/:cardId',
        component: RuleDebuggerComponent,
      }
    ], {
    // This makes 'shell' routes much more convenient because shared
    // parameters will automatically propagate down from the parent.
    //
    // It's our app, we get to make these decisions; none of the modules
    // should rely on this behaviour, or touch the router at all!
    paramsInheritanceStrategy: 'always',
    relativeLinkResolution: 'legacy'
})
  ],

  providers: [
    HasUnsavedChangesGuard
  ]
})
export class GravityAdminRoutingModule { }
