import { Component, Input, Output, EventEmitter } from '@angular/core';
import { BmiLink } from '@bmi/core';
import { Option } from '@bmi/ui';
import { CUI_ICONS } from './cui-icon-strings';

@Component({
  selector: 'gravity-admin-menu-sub-item-control',
  template: `
<section class="menu-item-card sub-item" [class.error]="hasError">
  <button type="button" class="btn btn--circle btn--ghost btn--small">
    <span class="icon-close" (click)="closed()"></span>
  </button>
  <h2>Edit menu link: {{ itemConfig.title }}</h2>
  <ui-form-field label="title" [class.error]="!itemConfig.title" required>
    <input [(ngModel)]="itemConfig.title">
  </ui-form-field>
  <ui-form-field label="route" [class.error]="!itemConfig.route" required>
    <bmi-core-route-selector [(ngModel)]="itemConfig.route" [moduleId]="moduleId"></bmi-core-route-selector>
  </ui-form-field>
  <ui-form-field label="icon">
    <ui-icon-select
      [displayFormat]="iconNameFormat"
      [icons]="iconClassList"
      [(ngModel)]="itemConfig.icon">
    </ui-icon-select>
  </ui-form-field>
</section>
  `,
  styleUrls: ['./menu-item-controls.scss'],
})
export class MenuSubItemControlComponent {

  @Input() itemConfig: BmiLink;
  @Input() pages: Option[];
  @Input() moduleId: string;
  @Output() close = new EventEmitter();

  iconClassList: string[] = CUI_ICONS.map((icon: string) => this.getIconClass(icon));

  get hasError(): boolean {
    return (!this.itemConfig.route || !this.itemConfig.title);
  }

  closed() {
    this.close.emit();
  }

  getIconClass(iconId: string): string {
    return `icon-${iconId}`;
  }

  iconNameFormat(iconId: string): string {
    return iconId
      .replace(/(icon)?-/g, ' ')
      .replace(/(?:^|\s)\w/g, (match: string) => match.toUpperCase());
  }

}
