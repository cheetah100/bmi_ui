import {
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  QueryList,
  SimpleChanges,
  ViewChildren,
} from '@angular/core';
import { MenuGroup, MenuItem, isBmiLink } from './menu-item';
import { ListsDragDropHelper } from './lists-drag-drop-utils';
import { Subscription, fromEvent } from 'rxjs';
import { throttleTime, tap } from 'rxjs/operators';
import { Option } from '@bmi/ui';
import cloneDeep from 'lodash-es/cloneDeep';
import { BmiRoute, BmiLink, BmiBuiltinRoute, BmiPageRoute } from '@bmi/core';
import { GravityApiMenuUi } from '../pages/menu-page/menu.service';

@Component({
  selector: 'gravity-admin-menu-editor',
  templateUrl: './menu-editor.component.html',
  styleUrls: ['./menu-editor.component.scss'],
})
export class MenuEditorComponent extends ListsDragDropHelper<MenuItem> implements OnChanges, OnInit, OnDestroy {

  @Input() moduleId: string;
  @Input() menuConfig: GravityApiMenuUi = { data: [], defaultRoute: '' };
  @Input() offset: number = null;
  @Input() pages: Option[] = null;
  @Output() cancel = new EventEmitter();
  @Output() save = new EventEmitter();
  @ViewChildren('box') menuBoxes: QueryList<ElementRef>;
  @ViewChildren('sub') subBoxes: QueryList<ElementRef>;

  dragBox: ElementRef = null;
  flatMenu: MenuItem[];
  boxIndex = 0;
  selectedItem: MenuItem;
  menuItems: MenuItem[];
  defaultRoute: BmiRoute;
  private subscriptions: Subscription[];

  ngOnInit() {
    this.subscriptions = [
      // update drag loc for smooth dragging on mousemove.
      // but only calculate the target drop area at intervals for performance.
      fromEvent(document, 'mousemove').pipe(
        tap((event: MouseEvent) => this.updateDragY(event)),
        throttleTime(100),
      ).subscribe((event: MouseEvent) => this.mouseMoveEvent(event)),
      // need to refresh box locations if scrolling while dragging
      fromEvent(document, 'scroll').pipe(
        throttleTime(100),
      ).subscribe((event: MouseEvent) => {
        if (!this.dragging) {
          return;
        }
        if (this.containerIndex === -1) {
          this.refreshBoxes();
        } else {
          this.refreshBoxes(
            this.getItemViews(this.menuItems[this.containerIndex]),
            this.subBoxes.filter((box: ElementRef) => {
              const groupId = box.nativeElement.dataset.group;
              return (!!groupId && parseInt(groupId, 10) === this.containerIndex);
            })
          );
        }
      }),
    ];
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.menuConfig) {
      this.selectItem(null);
      this.menuItems = cloneDeep(this.menuConfig.data);
      this.defaultRoute = cloneDeep(this.menuConfig.defaultRoute);
    }
  }

  ngOnDestroy() {
    if (this.subscriptions) {
      this.subscriptions.forEach(
        (sub: Subscription) => sub.unsubscribe()
      );
    }
  }

  protected reorderedItems(start: number, end: number): void {
    let itemList: MenuItem[];
    if (this.containerIndex === -1) {
      itemList = this.menuItems;
    } else {
      itemList = this.getItemViews(this.menuItems[this.containerIndex]);
    }
    itemList.splice(end, 0, itemList.splice(start, 1)[0]);
  }

  getItemViews(menuItem: MenuItem): BmiLink[] {
    if (!isBmiLink(menuItem)) {
      const item = menuItem as MenuGroup;
      return item.views;
    }
    return [];
  }

  initSubDrag(group: number, view: number, event: MouseEvent) {
    const menuGroup: MenuGroup = this.menuItems[group] as MenuGroup;
    this.selectItem(menuGroup.views[view]);
    this.refreshBoxes(menuGroup.views, this.subBoxes.filter((box: ElementRef) => {
      const groupId = box.nativeElement.dataset.group;
      return (!!groupId && parseInt(groupId, 10) === group);
    }));
    this.startDrag(event, view, group);
  }

  convertToGroup(link: BmiLink): MenuGroup {
    return {
      title: link.title,
      icon: link.icon,
      views: [],
    };
  }
  convertToLink(group: MenuGroup): BmiLink {
    return {
      title: group.title,
      icon: group.icon,
      route: null,
    };
  }

  addGroup(): void {
    const group: BmiLink = {
      title: '(Untitled)',
      icon: 'icon-square-o',
      route: null,
    };
    this.menuItems.push(group);
    this.selectItem(group);
  }

  addView(n: number): void {
    const item: MenuItem = this.menuItems[n];
    let group;
    if (isBmiLink(item)) {
      group = this.convertToGroup(item as BmiLink);
      this.menuItems[n] = group;
    } else {
      group = item as MenuGroup;
    }
    const view: BmiLink = {
      title: '(Untitled)',
      icon: 'icon-square-o',
      route: null,
    };
    group.views.push(view);
    this.selectItem(view);
  }

  deleteGroup(groupIndex: number, event: MouseEvent): void {
    event.stopPropagation();
    this.selectItem(null);
    this.menuItems.splice(groupIndex, 1);
  }

  deleteView(groupIndex: number, viewIndex: number, event: MouseEvent): void {
    event.stopPropagation();
    const group = this.menuItems[groupIndex] as MenuGroup;
    this.selectItem(null);
    group.views.splice(viewIndex, 1);
    if (!group.views.length) {
      this.menuItems[groupIndex] = this.convertToLink(group);
    }
  }

  refreshBoxes(
    items: MenuItem[] = this.menuItems,
    boxes = this.menuBoxes.toArray(),
  ): void {
    this.updateBoxes(items, boxes.filter(Boolean));
  }

  initDrag(index: number, event: MouseEvent) {
    this.selectItem(this.menuItems[index]);
    this.refreshBoxes();
    this.startDrag(event, index);
  }

  selectItem(item: MenuItem, event: MouseEvent = null): void {
    if (event) {
      event.stopPropagation();
    }
    this.selectedItem = item;
  }

  @HostListener('document:mouseup', ['$event'])
  mouseUp(event: MouseEvent) {
    this.mouseUpEvent(event);
  }

  cancelButton() {
    this.cancel.emit();
  }

  validationFail(): boolean {
    return this.menuItems.some((item: MenuItem) => this.itemIncomplete(item));
  }

  itemIncomplete(item: MenuItem): boolean {
    const isLink = isBmiLink(item);
    if (isLink) {
      const link = item as BmiLink;
      return this.linkItemIncomplete(link);
    } else {
      const group = item as MenuGroup;
      const views = this.getItemViews(group);
      return (
        !group.icon
        || !group.title
        || !views
        || !views.length
        || views.some(
          (bmiLink: BmiLink) => this.linkItemIncomplete(bmiLink)
        )
      );
    }
  }

  linkItemIncomplete(link: BmiLink): boolean {
    return (!link.title || !link.route);
  }

  itemHasViews(item: MenuItem): boolean {
    const views: BmiLink[] = this.getItemViews(item);
    return (!!views && !!views.length);
  }

  saveButton() {
    this.save.emit({
      data: this.menuItems,
      defaultRoute: this.defaultRoute,
    });
  }

}
