import { ElementRef, Input, Directive } from '@angular/core';

interface Box<T> {
  data: T;
  rect: ClientRect;
  boxes?: Box<T>[];
}

@Directive()
export abstract class ListsDragDropHelper<T> {

  // Handling nested lists
  // drag container: all sub-items move too
  // drag sub-item: can be dropped into position in same or other list

  @Input() offset = null;

  private boxes: Box<T>[] = null;
  private dragIndex = -1;
  private dragOffset = 0;
  private dragY = 0;
  containerIndex = -1;
  placeholderHeight: number;
  dragTarget: number;

  constructor() { }

  protected startDrag(
    event: MouseEvent,
    dragIndex: number,
    subItem = -1,
  ) {
    this.dragIndex = dragIndex;
    if (this.dragIndex >= 0) {
      this.containerIndex = subItem;
      const dragBox: Box<T> = this.boxes[this.dragIndex];
      this.dragOffset = dragBox.rect.top - event.pageY - this.offset - 10;
      this.placeholderHeight = dragBox.rect.height + 2;
      this.updateDragY(event);
      this.mouseMoveEvent(event);
    }
  }

  protected abstract reorderedItems(start: number, end: number): void;

  updateBoxes(
    items: T[],
    elements: ElementRef[],
  ) {
    this.boxes = [];
    for (let i = 0, il = items.length; i < il; i++) {
      const item: T = items[i];
      this.boxes.push(this.createBox(item, elements[i]));
    }
  }

  createBox(item: T, element: ElementRef): Box<T> {
    return {
      data: item,
      rect: element.nativeElement.getBoundingClientRect(),
    };
  }

  get dragging(): number {
    return this.dragIndex;
  }

  protected get dragLocation(): number {
    return this.dragY + this.dragOffset;
  }

  protected mouseUpEvent(event: MouseEvent): void {
    if (this.dragIndex >= 0) {
      this.containerIndex = -1;
      this.dragIndex = -1;
      this.dragY = 0;
    }
  }

  protected updateDragY(event: MouseEvent): void {
    if (this.dragIndex >= 0) {
      this.dragY = event.pageY;
    }
  }

  protected mouseMoveEvent(event: MouseEvent): void {
    if (this.dragIndex >= 0) {
      this.dragTarget = this.getClosestIndex(this.dragY);
      this.reorderedItems(this.dragIndex, this.dragTarget);
      this.dragIndex = this.dragTarget;
    }
  }

  protected getClosestIndex(pageY: number): number {
    const y = pageY - window.scrollY;
    return this.boxes.reduce(
      (a: number, c: Box<T>, i) => (y > c.rect.top) ? i : a, 0
    );
  }

}
