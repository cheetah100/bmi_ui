import { NgModule } from '@angular/core';
import { SharedUiModule } from '@bmi/ui';
import { MenuEditorComponent } from './menu-editor.component';
import { CommonModule } from '@angular/common';
import { MenuItemControlComponent } from './menu-item-control.component';
import { MenuSubItemControlComponent } from './menu-sub-item-control.component';
import { FormsModule } from '@angular/forms';
import { BmiCoreEditorModule, BmiCoreModule } from '@bmi/core';

@NgModule({
  imports: [
    SharedUiModule,
    CommonModule,
    FormsModule,
    BmiCoreEditorModule,
    BmiCoreModule
  ],
  declarations: [
    MenuEditorComponent,
    MenuItemControlComponent,
    MenuSubItemControlComponent
  ],
  exports: [MenuEditorComponent]
})
export class MenuEditorModule {}
