import { Component, Input, Output, EventEmitter } from '@angular/core';
import { MenuItem, isBmiLink, MenuGroup } from './menu-item';
import { Option } from '@bmi/ui';
import { BmiLink } from '@bmi/core';
import { CUI_ICONS } from './cui-icon-strings';

@Component({
  selector: 'gravity-admin-menu-item-control',
  template: `
<section class="menu-item-card" [class.error]="hasError">
  <button type="button" class="btn btn--circle btn--ghost btn--small">
    <span class="icon-close" (click)="closed()"></span>
  </button>
  <h2>Edit menu group: {{ itemConfig.title }}</h2>
  <ui-form-field label="title" [class.error]="!itemConfig.title" required>
    <input [(ngModel)]="itemConfig.title">
  </ui-form-field>
  <ui-form-field label="route" *ngIf="isLink" [class.error]="!itemConfig.route" required>
    <input [(ngModel)]="itemConfig.route">
  </ui-form-field>
  <ui-form-field label="icon" [class.error]="!itemConfig.icon" required>
    <ui-icon-select
      [displayFormat]="iconNameFormat"
      [icons]="iconClassList"
      [(ngModel)]="itemConfig.icon">
    </ui-icon-select>
  </ui-form-field>
</section>
  `,
  styleUrls: ['./menu-item-controls.scss'],
})
export class MenuItemControlComponent {

  @Input() itemConfig: MenuItem;
  @Input() pages: Option[];
  @Output() close = new EventEmitter();

  iconClassList: string[] = CUI_ICONS.map((icon: string) => this.getIconClass(icon));

  get hasError(): boolean {
    return (
      !this.itemConfig.title
      || !this.itemConfig.icon
      || !this.hasData
    );
  }

  get isLink(): boolean {
    return isBmiLink(this.itemConfig);
  }

  get hasData(): boolean {
    if (this.isLink) {
      const link = this.itemConfig as BmiLink;
      return !!link.route;
    }
    const group = this.itemConfig as MenuGroup;
    return (!!group.views && !!group.views.length);
  }

  getIconClass(iconId: string): string {
    return `icon-${iconId}`;
  }

  iconNameFormat(iconId: string): string {
    return iconId
      .replace(/(icon)?-/g, ' ')
      .replace(/(?:^|\s)\w/g, (match: string) => match.toUpperCase());
  }

  closed() {
    this.close.emit();
  }

}
