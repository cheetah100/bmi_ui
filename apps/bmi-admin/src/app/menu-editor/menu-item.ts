import { BmiLink } from '@bmi/core';

export interface MenuGroup {
  icon: string;
  title: string;
  views: BmiLink[];
}

export type MenuItem = MenuGroup | BmiLink;

export function isBmiLink(menuItem: MenuItem) {
  return !menuItem.hasOwnProperty('views');
}
