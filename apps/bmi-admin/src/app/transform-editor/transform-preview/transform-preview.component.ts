import {
  Component,
  Input,
  ChangeDetectionStrategy,
  SimpleChanges,
  OnChanges,
  ChangeDetectorRef, OnInit
} from '@angular/core';
import { TableModel, Option } from '@bmi/ui';
import {
  TransformService,
} from '../transform.service';
import { Observable, ReplaySubject } from 'rxjs';
import {
  TransformDetail,
  TransformConfig,
  Transform, TransformData
} from '@bmi/gravity-services';
import { map, tap } from 'rxjs/operators';
import { ObjectMap } from '@bmi/utils';


@Component({
  selector: 'idp-transform-preview',
  templateUrl: './transform-preview.component.html',
  styleUrls: ['./transform-preview.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TransformPreviewComponent implements OnChanges {

  /**
   * config overrides transformDetail. Merged config + lab form values.
   */
  @Input() transformDetail: TransformDetail;
  @Input() config: TransformConfig;
  @Input() show: boolean;

  tableData: TableModel;
  rawData: TransformData;
  type: string;
  ready: boolean;
  hasError: boolean;
  hasData: boolean;
  noDataMessage = 'No transform data to display';
  errorMessage: string;

  constructor(
    private transformService: TransformService,
    private changeDetectorRef: ChangeDetectorRef,
  ) { }

  get title(): string {
    if (!!this.config) {
      return this.config.name;
    }
    return this.transformDetail.title;
  }
  private _selectedSubject:ReplaySubject<ObjectMap<object>> = new ReplaySubject();

  private _seriesSubject:ReplaySubject<string[]> = new ReplaySubject();
  // private _transFormDataSubject:ReplaySubject<any> = new ReplaySubject();

  get series(): Observable<Option[]>{
    return this._seriesSubject.asObservable().pipe(
      map(series => series.sort().map(p => <Option>{value: p, text: p}))
    );
  }


  get selectedItems(): Observable<ObjectMap<object>> {
    return this._selectedSubject.asObservable().pipe(
      tap(res => console.log('selectedItems => res: ', res))
    );
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (
      !this.transformDetail
      || !changes.transformDetail.previousValue
      || this.transformDetail.id !== changes.transformDetail.previousValue.id
    ) {
      this.tableData = null;
      this.ready = false;
      this.changeDetectorRef.markForCheck();
      if (this.config) {
        const transform = Transform.fromJson(this.config);
        const firstTransformer = transform.transforms.length > 0 ? transform.transforms[0] : null;
        this.type = firstTransformer.transformer;
        if (firstTransformer) {
            this.getTableModel();
        }
      }

    }
  }

  getTableModel(): void {
    let tableModelObservable: Observable<TableModel|TransformData>
    if (this.config) {
      tableModelObservable = this.transformService.previewConfigAsTable(this.config);
    }

    if (tableModelObservable) {
      tableModelObservable.subscribe((tableModel: TableModel|TransformData) => {
        this.tableData = null;
        this.rawData = null;
        const tableData: TableModel | TransformData = tableModel;
        if (tableData && (tableData instanceof TableModel)) {
          this.ready = true;
          this.tableData = tableData
          this.changeDetectorRef.markForCheck();
          this.tableData.update();
        } else {
          this.ready = true;
          this.changeDetectorRef.markForCheck();
          this.rawData = tableData as TransformData;
        }
      },(error) => {
          this.ready = true;
          this.changeDetectorRef.markForCheck();
          this.hasError = true;
          this.errorMessage = `Unable to load transform '${this.config.id}' for preview`;
      });
    }
  }
}
