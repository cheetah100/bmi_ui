import { mapObjectValues, ObjectMap } from '@bmi/utils';
import { Action, GravityApiAction, Transform, TransformConfig } from '@bmi/gravity-services';
import { cloneDeep } from 'lodash-es';

// export type TransformType = 'pivot' | 'transpose';

// export interface TransformerConfigSource {
//   id: string;
//   config: TransformerConfig;
// }

// export interface TransformerConfig {
//   id?: string;
//   configuration: ObjectMap<string | string[]>;
//   order: number;
//   transformer: string;
// }

export interface PivotData {
  data: number[][];
  datasources: string[];
  xAxis: string[];
  yAxis: string[];
}

export interface TransposeDataObject {
  [key: string]: string | number | string[] | ObjectMap<string>;
}

export interface TransposeData {
  columns: string[];
  data: TransposeDataObject[];
}
