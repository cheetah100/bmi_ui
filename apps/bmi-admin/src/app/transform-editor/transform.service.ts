import { Injectable } from '@angular/core';
import { Observable, forkJoin, of } from 'rxjs';
import { map, mergeMap, tap } from 'rxjs/operators';
import {
  Gravity,
  GravityTransformService,
  PivotData,
  TransformConfig,
  TransformDetail,
  TransformType,
  TransformerConfig,
  TransposeData,
  TransposeDataObject,
  TransformData, Transform, DataseriesData
} from '@bmi/gravity-services';
import { TableModel, TableColumn, SimpleTableModel } from '@bmi/ui';
import { Option } from '@bmi/utils';

export interface RawData {
  data: any;
}

@Injectable()
export class TransformService {
  private cachedConfigs = null;

  constructor(
    private gravity: Gravity,
    private gravityTransformService: GravityTransformService
  ) {}

  get(id: string): Observable<TransformConfig> {
    return this.gravityTransformService.get(id);
  }

  create(transformConfig: TransformConfig): Observable<TransformConfig> {
    return this.gravityTransformService.create(transformConfig);
  }

  update(id: string, transformConfig: TransformConfig): Observable<TransformConfig> {
    return this.gravityTransformService.update(id, transformConfig);
  }

  delete(id: string): Observable<any> {
    return this.gravityTransformService.delete(id);
  }

  getTransformTableModel(transformData: TransformData, config: TransformConfig): TableModel {
    const transform = Transform.fromJson(config);
    const firstTransformer = transform.transforms.length > 0 ? transform.transforms[0] : null;
    if (firstTransformer) {
      switch (firstTransformer.transformer) {
        case 'pivot':
          return this.getPivotTableModel(transformData as PivotData);
        case 'transpose':
          return this.getTransposeTableModel(transformData as TransposeData);
        case 'dataseries':
          return this.getDataseriesTableModel(transformData as DataseriesData);
        default:
          return null;
      }
    }
  }

  previewConfigAsTable(config: TransformConfig): Observable<TableModel | TransformData> {
    return this.gravityTransformService
      .configPreview(config)
      .pipe(
        map((transformData: TransformData) => {
            const tableModel = this.getTransformTableModel(transformData, config);
            return tableModel || transformData;
          }
        )
      );
  }

  getBoards(): Observable<Option[]> {
    return this.gravity.boards.getBoards().pipe(
      map(boards => {
        const boardList = [];
        Object.keys(boards).forEach(board => {
          boardList.push({
            text: boards[board],
            value: board,
          });
        });
        return boardList;
      })
    );
  }

  getBoardFilters(board): Observable<Option[]> {
    return this.gravity.boards.getBoardFilters(board).pipe(
      map(filters => {
        const filterList = [];
        Object.keys(filters).forEach(filter => {
          filterList.push({
            text: filters[filter],
            value: filter,
          });
        });
        return filterList;
      })
    );
  }

  getTransformsOptions(type: TransformType): Observable<Option[]> {
    return this.gravityTransformService.getTransformsByType(type).pipe(
      map((transforms: TransformDetail[]) =>
        transforms.map((transform: TransformDetail) => ({
          text: transform.title,
          value: transform.id,
        }))
      )
    );
  }

  getPivotTableModel(data: PivotData): TableModel {
    if (
      data.data.length > 0 &&
      data.yAxis.length === data.data.length &&
      data.xAxis.length === data.data[0].length
    ) {
      const tableColumns: TableColumn[] = [
        new TableColumn('', false, null),
        ...data.xAxis.map(column => new TableColumn(column, false, null)),
      ];
      const tableModel: TableModel = new SimpleTableModel(tableColumns);
      data.yAxis.forEach((row: string, index: number) => {
        tableModel.addRow([row, ...data.data[index]]);
      });
      return tableModel;
    }
    return new SimpleTableModel([new TableColumn(null, false, null)]);
  }

  getTransposeTableModel(data: TransposeData): TableModel {
    if (data.columns) {
      const tableColumns: TableColumn[] = data.columns.map(
        column => new TableColumn(column, false, null)
      );
      const tableModel: TableModel = new SimpleTableModel(tableColumns);
      data.data.forEach((row: TransposeDataObject) => {
        const tableRow = data.columns.map((column: string) => (row[column] ? row[column] : ''));
        tableModel.addRow(tableRow);
      });
      return tableModel;
    }
  }

  getDataseriesTableModel(data: DataseriesData): TableModel {
    const categories = this.categoriseData(data);
    const tableColumns: TableColumn[] = [];
    tableColumns.push(new TableColumn('name', false, null))
    tableColumns.push(new TableColumn('kpi', false, null))
    categories.forEach(cat => tableColumns.push(new TableColumn(cat, false, null)))
    const tableModel: TableModel = new SimpleTableModel(tableColumns);
    Object.keys(data).sort().forEach((key) => {
      const dataObject = data[key];
      let tableRow = [
        key,
        dataObject.fields.kpi || ''
      ];
      // Set the values for the given data object
      const columns = categories.map(cat => {
        const categorySpecified = dataObject.items.find(item => item.x.toString() === cat);
        return categorySpecified ? categorySpecified.y.toString() : '';
      })
      tableRow = tableRow.concat(columns);
      tableModel.addRow(tableRow);
    })
    return tableModel
  }

  private categoriseData(dataSeries: DataseriesData): Array<string> {
    const categories:Set<string> = new Set()


    Object.entries(dataSeries).forEach( ([key, dataObject]) => {
      dataObject.items.forEach(item => categories.add(item.x.toString()))
    })

    const orderedCategories = [...(categories)].sort();
    return orderedCategories;
  }

  getTransformsList(): Observable<TransformDetail[]> {
    return this.gravityTransformService.getAll()
  }

  getBoardTransformsByType(board: string, type: string): Observable<Option[]> {
    return this.getAllEditableConfigs().pipe(
      map(configs =>
        configs
          .filter(config => {
            if (config.type !== type) {
              return false;
            }
            let boards: string[] = [];
            // sorry, lots of null checks: transform config schema may change
            if (
              config &&
              config.transforms &&
              Object.values(config.transforms).some(
                (transform: TransformerConfig) =>
                  transform && transform.configuration && !!transform.configuration.board
              )
            ) {
              boards = Object.values(config.transforms).map((transform: TransformerConfig) =>
                transform && transform.configuration ? transform.configuration.board : null
              ) as string[];
            }
            return boards.includes(board);
          })
          .map(config => ({
            text: config.name,
            value: config.id,
          }))
      )
    );
  }

  getAllEditableConfigs(): Observable<TransformConfig[]> {
    if (this.cachedConfigs) {
      return of(this.cachedConfigs);
    } else {
      return this.getTransformsList().pipe(
        map(transforms =>
          transforms.map(transform => this.gravityTransformService.get(transform.id))
        ),
        mergeMap((configObservables: Observable<TransformConfig>[]) => forkJoin(configObservables)),
        tap(configs => (this.cachedConfigs = configs))
      );
    }
  }
}
