import { Component, forwardRef, OnDestroy, OnInit } from '@angular/core';
import {
  BaseControlValueAccessor,
  IdpValidators,
  provideAsControlValueAccessor, provideAsValidator,
  TypedFormControl,
  TypedFormGroup
} from '@bmi/ui';
import { Permissions, Transform } from '@bmi/gravity-services';
import { probablyValidGravityId } from '@bmi/utils';
import { defer, ReplaySubject, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { cloneDeep } from 'lodash-es';
import { connectNameToIdFieldHelper } from '../utils/connect-name-to-id-field-helper';
import { TransformService } from './transform.service';

@Component({
  selector: 'bmi-transform-editor',
  templateUrl: './transform-editor-component.html',
  styleUrls: ['./transform-editor-component.scss'],
  providers: [
    provideAsControlValueAccessor(forwardRef(() => TransformEditorComponent)),
  ]
})
export class TransformEditorComponent extends BaseControlValueAccessor<Transform>
  implements OnInit, OnDestroy {

  readonly form = new TypedFormGroup<Transform>({

    id: new TypedFormControl('', {
      validators: [
        IdpValidators.required,
        IdpValidators.satisfies(
          probablyValidGravityId,
          'Must be a valid Gravity ID (letters and numbers, underscores, no spaces or other punctuation)'
        )
      ],
      // asyncValidators: [
      //   IdpValidators.asyncMustNotContainValue(() => this.existingIntegrations, 'An integration with this ID already exists')
      // ]
    }),
    isNew: new TypedFormControl(false),
    description: new TypedFormControl(''),
    name: new TypedFormControl('', [
      IdpValidators.required,
      IdpValidators.mustBeNonBlank(),
    ]),
    transforms: new TypedFormControl([]),
    ui: new TypedFormControl(null),
    type: new TypedFormControl(null),
    metadata: new TypedFormControl({}),
    active: new TypedFormControl(true),
    publish: new TypedFormControl(false),
    uniqueid: new TypedFormControl(''),
    version: new TypedFormControl(''),
    permissions: new TypedFormControl<Permissions>({}),
  });

  private onFormUpdated = new ReplaySubject<Transform>(1);

  private subscription = new Subscription();

  readonly existingTransforms = defer(() =>
    this.transformService.getTransformsList()).pipe(
    map(transforms => transforms.map(i => i.id))
  );

  constructor(
    private transformService: TransformService,
  ) {
    super();
  }

  ngOnInit() {
    this.form.valueChanges.subscribe(value => {
      this.onChanged(this.form.getRawValue())
    });

    this.subscription.add(connectNameToIdFieldHelper(
      this.form.controls.name,
      this.form.controls.id,
      this.existingTransforms
    ));
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.subscription.unsubscribe();
  }

  get idControl() {
    return this.form.controls.id;
  }

  updateComponentValue(transform: Transform) {
    const updatedTransform = cloneDeep(transform);
    updatedTransform.transforms = transform.transforms;
    this.form.patchValue(transform, {emitEvent: false});
    this.onFormUpdated.next(transform);

    if (transform.isNew) {
      this.idControl.enable();
    } else {
      this.idControl.disable();
    }
  }

}
