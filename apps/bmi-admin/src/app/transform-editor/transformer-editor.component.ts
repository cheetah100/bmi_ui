import { Component, forwardRef, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { BaseControlValueAccessor, provideAsControlValueAccessor, TypedFormControl, TypedFormGroup } from '@bmi/ui';
import {
  Action,
  GravityApiAction,
  PluginMetadataService,
  TransformConfig, TransformerConfig
} from '@bmi/gravity-services';
import { BehaviorSubject } from 'rxjs';
import pick from 'lodash-es/pick';
import { ObjectMap } from '@bmi/utils';
import { filter, switchMap, tap } from 'rxjs/operators';
import omit from 'lodash-es/omit';
// import { TransformerConfig } from './transform.types';

@Component({
  selector: 'bmi-transformer-editor',
  templateUrl: './transformer-editor.component.html',
  // styleUrls: ['./action-editor.component.scss'],
  providers: [
    provideAsControlValueAccessor(forwardRef(() => TransformerEditorComponent))
  ]
})
export class TransformerEditorComponent
  extends BaseControlValueAccessor<TransformerConfig>
  implements OnInit, OnChanges {

  @Input() transformerType: string;
  @Input() boardId: string = null;

  originalTransformer: TransformerConfig;

  readonly form = new TypedFormGroup({
    // name: new TypedFormControl<string>(),
    // description: new TypedFormControl<string>(),
    configuration: new TypedFormControl<ObjectMap<any>>(null)
  });

  private transformerTypeSubject = new BehaviorSubject<string>(undefined);
  readonly pluginMetadata = this.transformerTypeSubject.pipe(
    tap(transformerType => console.log(' => transformerType: ', transformerType)),
    switchMap(transformerType => this.pluginService.get('transformer', transformerType))
  );

  get type() {
    return this.transformerTypeSubject.value;
  }

  constructor(
    private pluginService: PluginMetadataService,
  ) {
    super();
  }

  ngOnInit() {
    this.form.valueChanges
      .pipe(filter(() => !this.isWriteValueInProgress))
      .subscribe(formValue => {
        this.onChanged({
          ...formValueToTransformer(formValue.configuration, this.originalTransformer),
          // name: formValue.name,
          // description: formValue.description,
        })
      });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.transformerType) {
      this.transformerTypeSubject.next(this.transformerType);
    }
  }

  updateComponentValue(transformer: TransformerConfig) {
    console.log('updateComponentValue => transformer: ', transformer);
    this.originalTransformer = transformer;
    this.transformerTypeSubject.next(transformer ? transformer.transformer : undefined);

    this.form.reset({
      configuration: transformer ? transformerToFormValue(transformer) : {}
    }, {emitEvent: false});
  }
}

function transformerToFormValue(transformer: TransformerConfig) {
  return transformer.configuration
}

function formValueToTransformer(
  formValue: ObjectMap<any>,
  originalTransformer: TransformerConfig
): TransformerConfig {
  console.log('formValueToTransformer => formatValue: ', formValue)
  const retval = Object.assign(TransformerConfig.clone(originalTransformer), {
    configuration: {
      ...formValue,
    }
  });
  return retval;
}
