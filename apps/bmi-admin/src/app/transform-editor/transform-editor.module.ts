import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { TransformService } from './transform.service';
import { BrowserModule } from '@angular/platform-browser';
import { GravityModule, GravityUiModule } from '@bmi/gravity-services';
import { SharedUiModule } from '@bmi/ui';
import { ReactiveFormsModule } from '@angular/forms';
import { TransformPreviewComponent } from './transform-preview/transform-preview.component';
import { BmiCoreModule } from '@bmi/core';
import { RulesAdminModule } from '../rule-builder/rules-admin.module';
import { TransformerEditorListComponent } from './transformer-editor-list.component';
import { TransformerEditorComponent } from './transformer-editor.component';
import { TransformEditorComponent } from './transform-editor-component';

@NgModule({
  entryComponents: [

  ],
  declarations: [
    TransformPreviewComponent,
    TransformerEditorComponent,
    TransformerEditorListComponent,
    TransformEditorComponent
  ],
  imports: [
    BrowserModule,
    GravityModule,
    GravityUiModule,
    SharedUiModule,
    ReactiveFormsModule,
    BmiCoreModule,
    RulesAdminModule
  ],
  providers: [TransformService],
  exports: [
    TransformPreviewComponent,
    TransformEditorComponent
  ]
})
export class TransformEditorModule {}
