export const AGGREGATE_OPTIONS = [{
  text: 'Average',
  value: 'average'
}, {
  text: 'Count',
  value: 'count'
}, {
  text: 'Min',
  value: 'min'
}, {
  text: 'Max',
  value: 'max'
}, {
  text: 'Sum',
  value: 'sum'
}];
