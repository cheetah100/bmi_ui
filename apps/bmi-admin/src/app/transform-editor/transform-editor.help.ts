import dedent from 'ts-dedent';

export const PIVOT_TRANSFORM_EDITOR_HELP = dedent`
  # Pivot Transform Editor #
  ## Transform Metadata ##
  ### Select Aggregation Type ###
  Selects the operator for the calculation
  ## Data Calculator
  ### Select X Axis
  Selects the board's field that populates the X Axis
  ### Select Y Axis
  Selects the board's field that populates the Y Axis
  ### Select Total
  Selects the board's field who value would be aggregated on
  Should be a numeric field for Aggregation Type SUM
`;