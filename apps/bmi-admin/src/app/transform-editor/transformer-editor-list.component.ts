import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { BaseControlValueAccessor, provideAsControlValueAccessor, TemplatedFormArray, TypedFormControl } from '@bmi/ui';
import { Action, PluginMetadata, PluginMetadataService, Transform, TransformerConfig } from '@bmi/gravity-services';
import { filter, map, shareReplay } from 'rxjs/operators';
import sortBy from 'lodash-es/sortBy';
import { uniqueId } from '@bmi/utils';
// import { TransformerConfig } from './transform.types';

@Component({
  selector: 'bmi-transformer-editor-list',
  templateUrl: './transformer-editor-list.component.html',
  providers: [
    provideAsControlValueAccessor(forwardRef(() => TransformerEditorListComponent))
  ]
})
export class TransformerEditorListComponent
  extends BaseControlValueAccessor<TransformerConfig[]>
  implements OnInit {
  @Input() transformId: string = null;

  readonly formArray = new TemplatedFormArray<TransformerConfig>(
    () => new TypedFormControl(null)
  );

  readonly transformerTypes = this.pluginMetadataService.list('transformer').pipe(
    map(plugins => sortBy(plugins, p => p.name)),
    shareReplay(1)
  );

  constructor(private pluginMetadataService: PluginMetadataService) {
    super();
  }

  ngOnInit() {
    this.formArray.valueChanges
      .pipe(filter(() => !this.isWriteValueInProgress))
      .subscribe(value => this.onChanged(value));
  }

  updateComponentValue(transformerConfigs: TransformerConfig[]) {
    this.formArray.patchValue(Object.values(transformerConfigs) || [], { emitEvent: false });
  }

  addTransformer(pluginType: PluginMetadata) {
    console.log('addTransformer => pluginType: ', pluginType);
    this.formArray.pushValue(
      TransformerConfig.create(pluginType.id)
    );
  }
}
