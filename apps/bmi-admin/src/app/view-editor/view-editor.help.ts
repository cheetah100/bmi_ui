import dedent from 'ts-dedent';

export const VIEW_EDITOR_HELP = dedent`
  # View Editor #

  ------------------------------------------------------------------------------
  ## Core Properties ##

  ------------------------------------------------------------------------------
  ### ID ###
  Will be generated using the Name field (this might change, manual entry could be required)
  ## Name ##
  User-facing title
  ### Type ###
  Used by the app to select the correct component or to filter views
  ### UI ###
  Ignored by gravity. Custom app-required fields can go here

  ------------------------------------------------------------------------------
  ## Fields ##

  ------------------------------------------------------------------------------
  ### Label ###
  User-facing title
  ### Field Path ###
  Data source for field. Required, also used to generate field ID
  ### UI ###
  Ignored by gravity. Custom app-required fields can go here

  ------------------------------------------------------------------------------

`;