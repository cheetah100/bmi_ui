import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import {
  View,
  ViewField,
} from '@bmi/gravity-services';
import { cloneDeep } from 'lodash-es';
import { DiffEditorModel } from 'ngx-monaco-editor';
import {
  FormGroup,
  AbstractControl,
  FormArray,
  FormControl,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { distinctUntilChanged, debounce, debounceTime } from 'rxjs/operators';
import { Subscription, Observable } from 'rxjs';
import { ObjectMap } from '@bmi/utils';
import { VIEW_EDITOR_HELP } from './view-editor.help'

const mustBeValidJson = (): ValidatorFn => {
  return (control: AbstractControl): ValidationErrors => {
    try {
      JSON.parse(control.value);
      return null;
    } catch {
      return { error: 'Parsing error' };
    }
  };
};

@Component({
  selector: 'gravity-view-editor',
  templateUrl: './view-editor.component.html',
  styleUrls: ['./view-editor.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ViewEditorComponent implements OnInit, OnChanges, OnDestroy {

  @Input() originalView: View;
  // we're just outputting the modified view.
  // shim component in app should handle saving
  @Output() viewModified = new EventEmitter<{ view: View, valid: boolean }>();
  @Output() helpContent: EventEmitter<string> = new EventEmitter<string>();

  modifiedView: View;
  loading = true;
  options = {
    theme: 'vs-dark',
    formatOnType: true,
    // Only allow view to be edited on form. For initial simplicity.
    // Later, will allow code edits and update the View model
    // Would need to check for errors / api compatability
    // And feedback errors to user somehow
    readOnly: true,
  };
  form: FormGroup;
  subscriptions: Subscription[] = [];
  modifiedModel: DiffEditorModel;
  originalModel: DiffEditorModel;

  get modifiedFields(): FormArray {
    return this.form.get('fields') as FormArray;
  }

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
  ) { }

  ngOnInit() {
    this.helpContent.emit(VIEW_EDITOR_HELP);
  }

  ngOnChanges() {
    if (this.originalView) {
      this.resetView();
    }
  }

  ngOnDestroy() {
    this.unsubscribe();
  }

  unsubscribe() {
    this.subscriptions.forEach((sub: Subscription) => sub.unsubscribe());
    this.subscriptions = [];
  }

  setModel(view: View): DiffEditorModel {
    return {
      code: this.formattedJson(view),
      language: 'json',
    };
  }

  setOriginalModel(): void {
    this.originalModel = this.setModel(this.originalView);
  }

  /**
   * After a config modification, updating this model will refresh the diff editor
   */
  setModifiedModel(): void {
    this.modifiedModel = this.setModel(this.modifiedView);
  }

  update() {
    this.changeDetectorRef.markForCheck();
  }

  createFieldFormField(
    options: { value: string, disabled?: boolean, validators?},
    field: ViewField
  ) {
    const control: FormControl = new FormControl(
      field[options.value],
      options.validators || [],
    );
    if (options.disabled) {
      control.disable();
    }
    this.subscriptions.push(
      this.reduceEvents(control.valueChanges).subscribe(() => {
        this.updateViewFields();
      }),
    );
    return control;
  }

  createFieldFormGroup(field: ViewField): FormGroup {
    const form: FormGroup = new FormGroup({});
    [
      { value: 'fieldPath', validators: [Validators.required], },
      { value: 'id', disabled: true },
      { value: 'label', },
    ].forEach((options: { value: string, disabled?: boolean }) => {
      const control = this.createFieldFormField(options, field);
      form.addControl(options.value, control);
    });
    form.addControl('ui', new FormControl({ value: JSON.stringify(field.ui, null, 1), disabled: true }));
    return form;
  }

  createViewFormGroup(view: View): FormGroup {
    this.unsubscribe();
    const form: FormGroup = new FormGroup({});
    [
      { value: 'boardId', disabled: true, },
      { value: 'id', disabled: true, },
      { value: 'name', validators: [Validators.required] },
      { value: 'type' },
    ].forEach(options => {
      const key = options.value;
      const control: FormControl = new FormControl(
        view[options.value],
        options.validators || [],
      );
      if (options.disabled) {
        control.disable();
      }
      this.subscriptions.push(
        this.reduceEvents(control.valueChanges)
          .subscribe(() => this.updateView(view, control, key)),
      );
      form.addControl(key, control);
    });

    const uiControl: FormControl = new FormControl(
      { value: JSON.stringify(view.ui, null, 1), disabled: false },
      [mustBeValidJson()]
    );
    this.reduceEvents(uiControl.valueChanges).subscribe(() => {
      this.updateView(view, uiControl, 'ui');
    });
    form.addControl('ui', uiControl);

    const fields: FormArray = new FormArray(view.fields.map(
      (field: ViewField) => this. createFieldFormGroup(field)
    ));
    form.addControl('fields', fields);
    return form;
  }

  viewModifiedActions(): void {
    this.viewModified.emit({ view: this.modifiedView, valid: this.form.valid });
    this.setModifiedModel();
  }

  updateView(view: View, control: FormControl, key: string): void {
    if (key === 'ui') {
      if (control.valid) {
        view[key] = JSON.parse(control.value);
      }
    } else {
      view[key] = control.value;
    }
    this.viewModifiedActions();
  }

  updateViewFields(): void {
    this.modifiedView.fields = this.modifiedFields.controls
      .map((fieldFormGroup: FormGroup) => {
        const fieldValue: ObjectMap<any> = fieldFormGroup.getRawValue();
        const viewField: ViewField = this.modifiedView.addField(
          fieldValue.id,
          fieldValue.fieldPath,
        );
        viewField.label = fieldValue.label;
        viewField.ui = JSON.parse(fieldValue.ui);
        return viewField;
      });
    this.viewModifiedActions();
  }

  resetView(): void {
    this.modifiedView = cloneDeep(this.originalView);
    this.form = this.createViewFormGroup(this.modifiedView);
    this.setOriginalModel();
    this.setModifiedModel();
    // do changes. Mainly for updating diff editor without requiring blur event
    this.reduceEvents(this.form.valueChanges, 800).subscribe(() => this.update());
    this.loading = false;
    this.viewModifiedActions();
    this.update();
  }

  formattedJson(view: View): string {
    return JSON.stringify(view.toJSON(), null, 1);
  }

  reduceEvents(observable: Observable<any>, interval: number = 500): Observable<any> {
    return observable.pipe(
      debounceTime(interval),
      distinctUntilChanged(),
    );
  }

  toggleFieldDeets(stackerSection: HTMLElement) {
    if (this.isExpanded(stackerSection)) {
      stackerSection.classList.remove('open');
    } else {
      stackerSection.classList.add('open');
    }
  }

  isExpanded(stackerSection: HTMLElement) {
    return stackerSection.classList.contains('open');
  }

  addField() {
    this.modifiedFields.push(this.createFieldFormGroup(new ViewField('', '')));
    this.updateViewFields();
  }

}
