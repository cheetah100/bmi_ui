import { NgModule } from '@angular/core';
import { ViewEditorComponent } from './view-editor.component';
import { MonacoEditorModule } from 'ngx-monaco-editor';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedUiModule } from '@bmi/ui';
import { GravityUiModule } from '@bmi/gravity-services';
import { BmiCoreModule } from '@bmi/core';

@NgModule({
  declarations: [ViewEditorComponent],
  imports: [
    MonacoEditorModule,
    BrowserModule,
    ReactiveFormsModule,
    SharedUiModule,
    GravityUiModule,
    BmiCoreModule
  ],
  exports: [ViewEditorComponent]
})
export class ViewEditorModule {}
