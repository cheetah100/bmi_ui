import { Injectable } from "@angular/core";
import { GravityService, GravityApiApplication } from '@bmi/gravity-services';
import { ModuleService } from '@bmi/core';
import { ObjectMap } from '@bmi/utils';
import { tap } from 'rxjs/operators';
import { of, Observable, BehaviorSubject } from 'rxjs';
import { ParamMap, Router } from '@angular/router';

@Injectable()
export class AppService {

  constructor(
    private gravityService: GravityService,
    private router: Router,
    private moduleService: ModuleService
  ) { }

  private appNameCache: ObjectMap<string> = null;
  private appConfigCache: ObjectMap<GravityApiApplication> = {};
  private currentApp: string = null;
  private appIdSubject: BehaviorSubject<string> = new BehaviorSubject(null);

  getGravityApps(): Observable<ObjectMap<string>> {
    if (!this.appNameCache) {
      return this.gravityService.getApps().pipe(
        tap((apps: ObjectMap<string>) => {
          if (!this.currentApp && Object.keys(apps) && Object.keys(apps).length) {
            this.currentApp = Object.keys(apps)[0];
            this.appIdSubject.next(this.currentApp);
          }
          this.appNameCache = apps;
        }),
      );
    }
    return of(this.appNameCache);
  }

  getAppConfig(app: string, refresh: boolean = false): Observable<GravityApiApplication> {
    if (refresh) {
      this.moduleService.invalidateModuleConfig(app);
    }
    return this.moduleService.getModuleConfig(app);
  }

  /**
   * Emit new app value to subscribers.
   * Also navigate to new url if /app/{appName}/ is present.
   * Component does not reload so this seems a simple solution for now
   */
  setCurrentApp(app: string, navigateToHub = true): void {
    if (app) {
      this.currentApp = app;
      this.appIdSubject.next(app);

      if (navigateToHub) {
        this.router.navigate(['/module', app]);
      }
    }
  }

  /**
   * Get the app from url param.
   * Used for initial load, or for manual route changes
   */
  captureUrlParam(paramMap: ParamMap): void {
    const appId: string = paramMap.get('moduleId');
    if (appId && appId !== this.currentApp) {
      this.setCurrentApp(appId, false);
    }
  }

  get appName(): string {
    return this.appNameCache[this.currentApp];
  }

  get appId(): Observable<string> {
    return this.appIdSubject.asObservable();
  }

}
