import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ConfirmationModalService } from '@bmi/ui';
import { dedent } from 'ts-dedent';


export interface CanCheckUnsavedChanges {
  readonly hasUnsavedChanges?: boolean;
  readonly hasUnsavedMessage?: string;
}

@Injectable()
export class HasUnsavedChangesGuard implements CanDeactivate<CanCheckUnsavedChanges> {
  constructor (
    private confirmationService: ConfirmationModalService
  ) {}

  canDeactivate(component: CanCheckUnsavedChanges): boolean | Observable<boolean> {
    if (!component.hasUnsavedChanges) {
      return true;
    } else {
      const defaultMessage = `
          ## Discard Changes?

          If you leave this page, any unsaved changes will be lost.
        `;
      const message = component?.hasUnsavedMessage || defaultMessage;
      return this.confirmationService.show({
        content: dedent(message),
        buttonYesText: 'Discard Changes',
        buttonCancelText: 'Keep Editing',
        emitCancelEvent: true
      }).pipe(
        map(result => result.action === 'yes')
      );
    }
  }
}
