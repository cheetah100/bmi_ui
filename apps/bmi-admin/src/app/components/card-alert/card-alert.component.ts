import {
  Component, EventEmitter,
  Input,
  OnChanges,
  OnInit, Output,
  SimpleChanges
} from '@angular/core';
import { GravityApiCardEvent, GravityCardAlertService } from '@bmi/gravity-services';
import groupBy from 'lodash-es/groupBy';
import orderBy from 'lodash-es/orderBy';
import { forkJoin, observable, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import {
  ConfirmationModalService,
  DataTableAction,
  Formatters,
  InMemoryTableController,
  TableColumn,
  TableModel,
  TableRow
} from '@bmi/ui';
import { ColumnDefinition } from '../card-alert-summary/card-alert-summary.component';
import dedent from 'ts-dedent';

const DEFAULT_COLUMN_WIDTH = '1fr';
const COLUMN_TYPES = ['data', 'actions'];

@Component({
  selector: 'bmi-card-alert, card-alert',
  templateUrl: './card-alert.component.html',
  styleUrls: ['./card-alert.component.scss']
})
export class CardAlertComponent implements OnInit, OnChanges {
  @Input() board: string = null;
  @Input() alerts: GravityApiCardEvent[] = null;

  @Output() alertsModified = new EventEmitter();

  public summary: BoardAlerts = null;
  public tableModel: TableModel = null; //Map<string, TableModel> = new Map<string, TableModel>(null);
  private tableController: InMemoryTableController = null;
  private columnMap: Map<string, ColumnDefinition>;

  alertSummary: AlertSummary = null;

  constructor(
    private confirmationService: ConfirmationModalService,
    private cardAlertService: GravityCardAlertService,
  ) {}

  ngOnChanges(changes: SimpleChanges) {
    if (changes.alerts || changes.board) {
      this.alertSummary = summarizeAlerts(this.board, this.alerts);
      this.initTable()
    }
  }

  ngOnInit() {
  }

  onTableAction(action: DataTableAction) {
    const boardId = action.row.getDataId('boardId');
    const groupTitle = action.row.getDataId('groupTitle');
    const summaryGroupIndex = this.alertSummary.groups.findIndex(group => group.title === groupTitle);
    const summaryGroup = this.alertSummary.groups[summaryGroupIndex];

    return this.confirmationService.show({
      content: dedent`
            ## Dismiss Alerts for group ${groupTitle}?

            Dismiss alerts?
          `,
      buttonYesText: 'Dismiss alerts',
      buttonCancelText: 'No, leave alerts'
    }).pipe(
      map(() => summaryGroup.alerts ),
      switchMap((alerts) => {
        if (alerts.length > 0 ) {
          const alertsAsString:string[] = alerts.map(alert => alert.uuid);
          return forkJoin(this.cardAlertService.dismissCardAlerts(boardId, alertsAsString));
        } else {
          return of(null);
        }
      })

    ).subscribe(result => {
      this.alertsModified.emit(this.board);
    });
  }

  private initTable() {
    const columns: TableColumn[] = this.createColumns();

    // Create table models
    this.tableModel = new TableModel(columns);
    this.tableController = new InMemoryTableController(this.tableModel);

    this.tableModel.updateRequested
      .pipe(switchMap(() => this.tableController.refresh()))
      .subscribe();

    // Add rows
    for (const group of this.alertSummary.groups) {
      // Only use the specified fields
      const fields = [];
      this.columnMap.forEach((column, key) => {
        switch (column.type || 'data') {
          case 'data':
            fields.push(group[key]);
            break;
          case 'actions':
            fields.push({
              type: 'actions',
              buttons: [
                { title: 'Delete', icon: 'trash', action: 'delete-alerts' }
              ]
            });
            break;
        }
      });
      this.tableController.addRow(
        new TableRow(this.tableModel, fields, undefined, {
          boardId: this.alertSummary.board,
          groupTitle: group.title
        })
      );
    }

    // Set TableModel properties
    this.tableModel.pagination.enabled = true;
    this.tableModel.pagination.rowsPerPage = 20;
    this.tableModel.update();
  }

  private createColumns(): TableColumn[] {
    this.columnMap = new Map<string, ColumnDefinition>([
      [
        'count',
        {
          label: 'Count',
          width: '1fr'
        }
      ],
      [
        'title',
        {
          label: 'Detail',
          width: '3fr'
        }
      ],
      [
        'lastRaisedAt',
        {
          label: 'Last Occurred',
          width: '1fr',
          formatter: Formatters.msecTimestamp
        }
      ],
      [
        'actions',
        {
          label: 'Actions',
          width: '1fr',
          type: 'actions'
        }
      ]
    ]);

    // Create TableColumns
    const columns: TableColumn[] = [];
    this.columnMap.forEach((colDef, name) => {
      if (colDef.type && !COLUMN_TYPES.includes(colDef.type)) {
        throw new Error(`Unknown Column Type: ${colDef.type}`);
      }
      const column = new TableColumn(
        colDef.label,
        false,
        colDef.width ? colDef.width : DEFAULT_COLUMN_WIDTH
      );
      if (colDef.formatter) column.formatter = colDef.formatter;
      columns.push(column);
    });

    return columns;
  }
}

export interface BoardAlerts {
  board: string,
  alerts: GravityApiCardEvent[]
}

export interface AlertSummary {
  board: string;
  alerts: GravityApiCardEvent[];
  groups: AlertSummaryGroup[];
}

export interface AlertSummaryGroup {
  title: string;
  lastRaisedAt: number;
  count: number;
  alerts: GravityApiCardEvent[];
}

function summarizeAlerts(
  board: string,
  gravityAlerts: GravityApiCardEvent[]
): AlertSummary {
  const groupedByMessage = groupBy(gravityAlerts, alert => [alert.detail]);
  return {
    board,
    alerts: gravityAlerts,
    groups: orderBy(
      Object.entries(groupedByMessage).map(([message, alerts]) => {
        return {
          title: message,
          count: alerts.length,
          alerts: alerts,
          lastRaisedAt: Math.max(...alerts.map(a => a.occuredTime))
        };
      }),
      group => [group.count],
      ['desc']
    )
  };
}
