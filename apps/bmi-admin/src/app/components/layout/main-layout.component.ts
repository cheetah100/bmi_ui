import { Component, OnInit, OnDestroy, isDevMode } from '@angular/core';
import { ComponentPortal } from '@angular/cdk/portal';
import { ReplaySubject, Observable, Subscription, Subject } from 'rxjs';
import { Breadcrumb, SimpleCloseDialogService } from '@bmi/ui';
import { ActivatedRouteSnapshot, UrlSegment, Router, Route, RouterEvent, NavigationEnd, ActivatedRoute } from '@angular/router';
import { ObjectMap } from '@bmi/utils';
import { AppService } from '../../app.service';
import { MenuItem } from '../../menu-editor/menu-item';
import { HelpService } from '../help/help.service';
import { HelpSidebarComponent } from '../help/help-sidebar.component';
import { map } from 'rxjs/operators';
import { BmiLink, CurrentUserService } from '@bmi/core';
import { AdminAppNavService } from '../../admin-app-nav.service';
import { SidebarHostContext } from '../admin-sidebar/sidebar-host-context';
import { FocusPanelComponent } from '../focus-panel/focus-panel.component';
import { FocusPanelService } from '../focus-panel/focus-panel.service';
import { SsoLogoutService } from '@bmi/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gravity-admin-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss'],
  providers: [
    SidebarHostContext
  ]
})
export class MainLayoutComponent implements OnInit, OnDestroy {

  private breadcrumbsSubject = new ReplaySubject<Breadcrumb[]>();
  private subscription = new Subscription();
  public currentApp: string = null;
  public selectedBoard: string = null;
  public isTransformContext = false;
  public menuOpen = false;
  sidebarOpen = false;
  isHelpEnabled = false;
  headerTitle = 'Gravity Workflow Automation';
  contextTabs: MenuItem[];
  modules: { id: string, title: string }[] = null;

  appHasSidebar = true;
  menuContent: MenuItem[] = [];
  private closeSubject = new Subject<null>();
  closeEvents: Observable<null> = this.closeSubject.asObservable();

  public get breadcrumbs(): Observable<Breadcrumb[]> {
    return this.breadcrumbsSubject.asObservable();
  }

  userInfo = this.currentUser.currentUserInfo;

  constructor(
    private helpService: HelpService,
    private router: Router,
    private appService: AppService,
    private activatedRoute: ActivatedRoute,
    private dialogService: SimpleCloseDialogService,
    private appNav: AdminAppNavService,
    private currentUser: CurrentUserService,
    private sidebarHost: SidebarHostContext,
    private focusPanelService: FocusPanelService,
    private ssoLogoutService: SsoLogoutService
  ) { }

  ngOnInit() {
    this.subscription.add(this.router.events.subscribe((event: RouterEvent) => {
      if (event instanceof NavigationEnd) {
        this.navigationEvent();
      }
    }));
    this.navigationEvent();
    this.appService.getGravityApps().subscribe((modules: ObjectMap<string>) => {
      this.modules = Object.entries(modules).map(([id, title]: string[]) => ({ id, title }));
      this.menuContent = this.getMenuContent(this.modules);
    });
    this.dialogService.onClose.subscribe(() => this.closeSubject.next(null));

    this.subscription.add(
      this.sidebarHost.addSidebar({
        id: 'focus',
        title: 'Toolbox',
        content: new ComponentPortal(FocusPanelComponent),
      })
    );

    this.subscription.add(
      this.sidebarHost.addSidebar({
        id: 'help',
        title: 'Help',
        content: new ComponentPortal(HelpSidebarComponent),
      })
    );

    this.subscription.add(this.focusPanelService.widgetAdded.subscribe(widgets => {
      this.sidebarOpen = true;
      this.sidebarHost.activatePanel('focus');
    }));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getMenuContent(modules: { id: string, title: string }[]): MenuItem[] {
    const moduleItems = modules.map((module: { id: string, title: string }) => ({
      title: module.title,
      route: this.appNav.moduleHub(module.id).commands,
    }));
    const menuItems: MenuItem[] = [
      { title: 'Home', icon: 'icon-home', route: ['/'], },
      { title: 'Modules', icon: 'icon-grid-view', views: moduleItems, },
      { title: 'Boards', icon: 'icon-devices', route: ['/board'], },
      { title: 'Transforms', icon: 'icon-poll', route: ['/transform'], },
      { title: 'Integrations', icon: 'icon-computer-queue', route: this.appNav.integrationHome.commands},
      { title: 'System', 'icon': 'icon-cog', route: this.appNav.systemStatus.commands },
    ];
    if (isDevMode()) {
      menuItems.push({ title: 'Colors', icon: 'icon-draw', route: ['color-reference'] });
    }
    return menuItems;
  }

  navigationEvent(): void {
    if (this.activatedRoute.snapshot.firstChild) {
      this.selectedBoard = this.activatedRoute.snapshot.firstChild.params.boardId || null;
      this.isTransformContext = !!this.activatedRoute.snapshot.firstChild.params.transformId;
      this.currentApp = this.activatedRoute.snapshot.firstChild.params.moduleId || null;
      this.appService.captureUrlParam(this.activatedRoute.snapshot.children[0].paramMap);
      this.contextTabs = this.getContextMenuItems();
      this.createBreadcrumbs();
    }
  }

  /**
   * Auto generating breadcrumbs.
   */
  createBreadcrumbs(): void {
    const breadcrumbs: Breadcrumb[] = [{ title: 'Home', uri: '/', active: false, },];
    let path: string[] = [];
    const addBreadcrumb = ((ss: ActivatedRouteSnapshot) => {
      ss.url.forEach((u: UrlSegment) => {
        const segment: string = u.path;
        path = [...path, segment];
        const active = !this.isRoute(path, ss.params);
        breadcrumbs.push({ title: segment, uri: [...path], active, });
      });
      ss.children.forEach((child: ActivatedRouteSnapshot) => {
        addBreadcrumb(child);
      });
    });
    addBreadcrumb(this.router.routerState.snapshot.root);
    breadcrumbs[breadcrumbs.length - 1].active = true;
    this.breadcrumbsSubject.next(breadcrumbs);
  }

  writeParams(path: string, params: ObjectMap<string>): string[] {
    return path.split('/').map((segment: string) => {
      const key: string = segment.replace(':', '');
      return params[key] || segment;
    });
  }

  /**
   * We need to know whether a segment in breadcrumbs corresponds to an actual view
   * Checking against route setup for this.
   */
  isRoute(path, params: ObjectMap<string>): boolean {
    let matchingRoute: Route;
    const findRoute = ((routes: Route[]): void => {
      if (!!matchingRoute) {
        return;
      }
      routes.forEach((route: Route) => {
        const paramsPath: string[] = this.writeParams(route.path, params);
        if (paramsPath.join() === path.join()) {
          matchingRoute = route;
        }
        findRoute(route.children || []);
      });
    });
    findRoute(this.router.config);
    return !!matchingRoute;
  }

  get helpHtml() {
    return this.helpService.currentHelpHtml;
  }

  get isHelpAvailable() {
    return this.helpHtml.pipe(map(h => !!h));
  }

  hideHelp() {
    this.isHelpEnabled = false;
  }

  toggleHelp() {
    this.isHelpEnabled = !this.isHelpEnabled;
  }

  toggleSidebar() {
    this.sidebarOpen = !this.sidebarOpen;
  }

   getContextMenuItems(): BmiLink[] {
    if (!!this.selectedBoard) {
      return [
        { title: 'Board Summary', route: ['/board', this.selectedBoard], icon: 'icon-features' },
        { title: 'View Data', route: ['/board', this.selectedBoard, 'data-viewer'], icon: 'icon-tables' },
        { title: 'Edit Board', route: ['/board', this.selectedBoard, 'edit'], icon: 'icon-configurations' },
      ]
    } else if (this.isTransformContext) {
      return [];
    } else if (!!this.currentApp) {
      return [
        { title: 'Module Hub', route: ['/module', this.currentApp], icon: 'icon-features' },
        { title: 'Edit Module', route: ['/module', 'edit', this.currentApp], icon: 'icon-configurations' },
        { title: 'Edit Menu', route: ['/module', this.currentApp, 'menu'], icon: 'icon-configurations' },
        { title: 'Widgets', route: ['/module', this.currentApp, 'widget'], icon: 'icon-apps' }
      ];
    } else {
      return [];
    }
  }

  ssoLogout() {
    this.ssoLogoutService.ssoLogout();
  }

}
