import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { TypedFormControl, ModalContext } from '@bmi/ui';
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
import { Credential, CredentialService } from '@bmi/gravity-services';
import { switchMap, take } from 'rxjs/operators';

@Component({
  template: `
    <div style="height: 80%; overflow: scroll"  #scrolledContents>
        <ui-observable-loader [source]="credentialObservable | async">
        <ng-template let-credential>
            <h1>{{ credential.isNew ? 'Create Credential' : 'Edit Credential: ' + credential.name }}</h1>
            <credential-editor [formControl]="formControl" (close)="closed()" (save)="saved($event)"></credential-editor>
        </ng-template>
        </ui-observable-loader>
    </div>
  `,
  styleUrls: ['./credential-modal.component.scss']
})
export class CredentialModalComponent implements OnInit, OnDestroy {
    originalCredential: Credential = undefined;
    formControl = new TypedFormControl<Credential>(undefined);

  @Input() inputCredential: Credential = null;

  @Output() credentialSaved = new EventEmitter();

  private subscription = new Subscription();

  credentialObservable = new BehaviorSubject<Observable<Credential>>(undefined);
  

  constructor(
    private credentialService: CredentialService,
    private modalContext: ModalContext
  ) {}

  ngOnInit() {
    const { id, isNew } = this.modalContext.data;
    if (!isNew) {
        this.credentialObservable.next(this.credentialService.get(id, { invalidateNow: true }));
    } else {
      this.credentialObservable.next(of(this.modalContext.data));
    }

    this.subscription.add(this.credentialObservable.pipe(
        switchMap(obs => obs || of(<Credential>undefined))
      ).subscribe(credentialToEdit => {
        this.formControl.setValue(credentialToEdit, {emitEvent: false});
        this.originalCredential = credentialToEdit;
      }));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  closed() {
    this.modalContext.complete(
      {
        action: 'closed',
        data: this.credentialObservable.value
      });
  }

  saved(credential: Credential) {
    this.modalContext.complete(
      {
        action: 'saved',
        data: credential
      });
  }
}
