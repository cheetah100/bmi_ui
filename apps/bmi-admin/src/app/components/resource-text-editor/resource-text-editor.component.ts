import { Component, Input, forwardRef, OnInit } from '@angular/core';
import { AsyncValidatorFn } from '@angular/forms';
import { Observable, isObservable, of, BehaviorSubject, combineLatest, OperatorFunction } from 'rxjs';
import { catchError, distinctUntilChanged, filter, map, switchMap, shareReplay, take } from 'rxjs/operators';

import { TypedFormGroup, TypedFormControl, provideAsControlValueAccessor, BaseControlValueAccessor, Option, IdpValidators } from '@bmi/ui';
import { BoardResource, BoardResourceSummary, GravityResourceService } from '@bmi/gravity-services';
import keyBy from 'lodash-es/keyBy';
import isEqual from 'lodash-es/isEqual';
import capitalize from 'lodash-es/capitalize';

const SUPPORTED_LANGUAGES = [
  'plaintext',
  'javascript',
  'typescript',
  'python',
  'html',
  'css',
  'json',
  'lua',
  'r',
  'markdown',
  'handlebars',
  'xml',
  'yaml',
  'graphql',
];

const RESOURCE_TYPE_TO_MONACO_MAP: {[key: string]: string} = {
  ...keyBy(SUPPORTED_LANGUAGES, lang => lang),
  'velocity': 'html',
  'text': 'plaintext'
};

const DEFAULT_EDITOR_OPTIONS = {
  theme: 'vs-dark',
  minimap: {enabled: false},
  language: 'javascript',
  automaticLayout: true,
};


function asyncMustNotContainValue<T = string>(valueFn: () => (T[] | Observable<T[]>), message = 'This value already exists'): AsyncValidatorFn {
  const source = () => {
    const values_or_observable = valueFn();
    return Array.isArray(values_or_observable) ? of(values_or_observable) : values_or_observable;
  };

  return control => source().pipe(
    map(values => values.includes(control.value)),
    map(isFound => !isFound ? null : {mustNotContainValue: message}),
    take(1)
  );
}

function ofEmptyArray<T>() {
  return of(new Array<T>());
}

function onErrorEmitEmptyArray<T>(): OperatorFunction<T[], T[]> {
  return catchError(() => ofEmptyArray<T>());
}

type ProjectFunction<T, R> = (value: T, index: number) => Observable<R>;

function switchMapIf<T, R>(predicate: (value: T, index: number) => boolean, projectFn: ProjectFunction<T, R>, elseFn: ProjectFunction<T,R>): OperatorFunction<T, R> {
  return switchMap((value, index) => predicate(value, index) ? projectFn(value, index) : elseFn(value, index));
}

function switchMapIfTruthy<T, R>(projectFn: ProjectFunction<T, R>, elseFn: ProjectFunction<T, R>) {
  return switchMapIf(x => !!x, projectFn, elseFn);
}


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gravity-resource-text-editor',
  templateUrl: './resource-text-editor.component.html',
  styleUrls: ['./resource-text-editor.component.scss'],
  providers: [provideAsControlValueAccessor(forwardRef(() => ResourceTextEditorComponent))]
})
export class ResourceTextEditorComponent extends BaseControlValueAccessor<BoardResource> implements OnInit{
  readonly form = new TypedFormGroup<BoardResource>({
    id: new TypedFormControl(null, {
      validators: [IdpValidators.required, IdpValidators.mustBeNonBlank(), IdpValidators.pattern(/^\w[\w]+$/)],
      asyncValidators: asyncMustNotContainValue(() => this.resourcesInCurrentBoard)
    }),
    name: new TypedFormControl(null),
    type: new TypedFormControl('javascript'),
    resource: new TypedFormControl(null, {
      validators: [IdpValidators.required, IdpValidators.mustBeNonBlank()]
    }),
    isNew: new TypedFormControl(true),
    boardId: new TypedFormControl(null),
  });

  private originalResource: BoardResource;

  readonly editorOptions = new BehaviorSubject<any>(DEFAULT_EDITOR_OPTIONS);
  readonly monacoEditor = new BehaviorSubject<any>(null);
  readonly currentBoard = new BehaviorSubject<string>(null);

  readonly languageOptions = Object.keys(RESOURCE_TYPE_TO_MONACO_MAP)
    .map(lang => <Option>{value: lang, text: capitalize(lang)});

  resourcesInCurrentBoard = this.currentBoard.pipe(
    switchMapIfTruthy(boardId => this.resourceService.list(boardId), () => ofEmptyArray<BoardResourceSummary>()),
    map(resources => resources.map(r => r.id)),
    shareReplay(1),
  );

  get editorHasInitialized() {
    return !!this.monacoEditor.value;
  }

  get isNew() {
    return this.form.value.isNew;
  }

  get isValid() {
    return this.form.valid;
  }

  constructor(
    private resourceService: GravityResourceService
  ) {
    super();
  }

  ngOnInit() {
    combineLatest(
      this.monacoEditor.pipe(filter(editor => !!editor)),
      this.editorOptions,
    ).subscribe(([editor, options]) => editor.updateOptions(options));

    this.form.valueChanges.pipe(
      filter(() => !this.isWriteValueInProgress)
    ).subscribe(resource => this.onChanged({
      // Merge the original resource object with the updated form values.
      // Disabled fields won't be included in the form value, so they will take
      // on their original values here (e.g. id)
      ...this.originalResource || {},
      ...resource
    }));

    this.form.valueChanges.pipe(
      map(value => value.type),
      distinctUntilChanged(),
    ).subscribe(() => this.updateEditorLanguage());
  }

  updateComponentValue(boardResource: BoardResource) {
    this.originalResource = boardResource;
    this.form.reset(boardResource || {}, {emitEvent: false});
    this.updateEditorLanguage();
    this.updateIsIdEnabled();
    this.currentBoard.next((boardResource && boardResource.boardId) ? boardResource.boardId : null);
  }

  private updateEditorLanguage() {
    const resourceLanguage = this.form.value.type || '';
    const mappedLanguage = RESOURCE_TYPE_TO_MONACO_MAP[resourceLanguage.toLowerCase()] || null;
    this.editorOptions.next({
      ...this.editorOptions.value,
      language: mappedLanguage
    });
  }

  private updateIsIdEnabled() {
    if (this.isNew) {
      this.form.controls.id.enable();
    } else {
      this.form.controls.id.disable();
    }
  }

  onMonacoInitialized(monacoEditor) {
    this.monacoEditor.next(monacoEditor);
  }
}
