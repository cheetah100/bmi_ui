import { Component, forwardRef, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { AsyncValidatorFn, FormGroup, ValidatorFn } from '@angular/forms';
import { Observable, of, defer, ReplaySubject, Subscription } from 'rxjs';
import { map, switchMap, shareReplay, take } from 'rxjs/operators';

import { TypedFormGroup, TypedFormControl, provideAsControlValueAccessor, BaseControlValueAccessor, IdpValidators, ConfirmationModalService } from '@bmi/ui';
import { Credential, CredentialService, Permissions } from '@bmi/gravity-services';
import { connectNameToIdFieldHelper } from '../../utils/connect-name-to-id-field-helper';
import dedent from 'ts-dedent';
import { escapeMarkdown } from '@bmi/utils';

function asyncMustNotContainValue<T = string>(valueFn: () => (T[] | Observable<T[]>), message = 'This value already exists'): AsyncValidatorFn {
  const source = () => {
    const values_or_observable = valueFn();
    return Array.isArray(values_or_observable) ? of(values_or_observable) : values_or_observable;
  };

  return control => source().pipe(
    map(values => values.includes(control.value)),
    map(isFound => !isFound ? null : {mustNotContainValue: message}),
    take(1)
  );
}

const CredentialValidator: ValidatorFn = (fg: FormGroup) => {
  const updateIdentifier: boolean = fg.get('updateIdentifier').value;
  const updateSecret: boolean = fg.get('updateSecret').value;
  const isNew: boolean = fg.get('isNew').value;

  if (updateIdentifier && !fg.get("identifier").valid) {
    return { formError: true };
  }

  if (updateSecret && !fg.get("secret").valid) {
    return { formError: true };
  }

  if (isNew && !fg.get("id").valid) {
    return { formError: true };
  }

  if (fg.get("name").valid &&
    fg.get("resource").valid
    ) {
        return null;
  }
  else {
    return { formError: true };
  }
};

@Component({
  selector: 'credential-editor',
  templateUrl: './credential-editor.component.html',
  styleUrls: ['./credential-editor.component.scss'],
  providers: [provideAsControlValueAccessor(forwardRef(() => CredentialEditorComponent))]
})
export class CredentialEditorComponent extends BaseControlValueAccessor<Credential> implements OnInit, OnDestroy {
    @Output() close  = new EventEmitter();
    @Output() save = new EventEmitter();

    readonly form = new TypedFormGroup<Credential>({
        id: new TypedFormControl('', {
            validators: [IdpValidators.required, IdpValidators.mustBeNonBlank(), IdpValidators.pattern(/^\w[\w]+$/)],
            asyncValidators: asyncMustNotContainValue(() => this.existingCredentials, 'A credential with this ID already exists')
        }),
        name: new TypedFormControl('', {
            validators: [IdpValidators.required, IdpValidators.mustBeNonBlank()]
        }),
        description: new TypedFormControl(''),
        resource: new TypedFormControl('', {
          validators: [IdpValidators.required, IdpValidators.mustBeNonBlank()]
        }),
        method: new TypedFormControl(''),
        requestMethod: new TypedFormControl(''),
        updateIdentifier: new TypedFormControl<boolean>(true),
        identifier: new TypedFormControl('', {
            validators: [IdpValidators.required, IdpValidators.mustBeNonBlank()]
        }),
        updateSecret: new TypedFormControl<boolean>(true),
        secret: new TypedFormControl('', {
            validators: [IdpValidators.required, IdpValidators.mustBeNonBlank()]
        }),
        isNew: new TypedFormControl(false),
        metadata: new TypedFormControl({}),
        responseFields: new TypedFormControl({}),
        permissions: new TypedFormControl<Permissions>({}),
    }, CredentialValidator);

  private onFormUpdated = new ReplaySubject<Credential>(1);

  private subscription = new Subscription();

  private originalCredential: Credential;

  readonly existingCredentials = defer(() => this.credentialService.list()).pipe(
    map(creds => Object.keys(creds)));

  get isNew() {
    return this.form.value.isNew;
  }

  get isValid() {
    return this.form.valid;
  }

  constructor(
    private credentialService: CredentialService,
    private confirmationService: ConfirmationModalService,
  ) {
    super();
  }

  ngOnInit() {
    this.form.valueChanges.subscribe(() => this.onChanged(this.form.getRawValue()));
    this.form.controls.updateIdentifier.valueChanges.subscribe(value => this.updateCredentialConditionalValue("identifier", value));
    this.form.controls.updateSecret.valueChanges.subscribe(value => this.updateCredentialConditionalValue("secret", value));
    this.subscription.add(connectNameToIdFieldHelper(
        this.form.controls.name,
        this.form.controls.id,
        this.existingCredentials
      ));
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.subscription.unsubscribe();
  }

  updateComponentValue(credential: Credential) {
    this.form.patchValue(credential, {emitEvent: false});
    this.onFormUpdated.next(credential);
    if (credential.isNew) {
        this.form.controls.id.enable();
      } else {
        this.form.controls.id.disable();
      }
  }

  getOrCreateCredential(credentialId: string | null): Observable<Credential> {
    if (credentialId) {
      return this.credentialService.get(credentialId, {invalidateNow: true}).pipe(shareReplay(1), take(1));
    } else {
      return of(<Credential>{
        id: '',
        isNew: true,
        metadata: null,
        name: '',
        description: '',
        resource: '',
        method: '',
        requestMethod: '',
        identifier: '',
        secret: '',
        responseFields: {},
        permissions: {}
      });
    }
  }

  saved() {
    this.save.emit(this.originalCredential)
  }

  closed() {
    this.close.emit();
  }

  updateCredentialConditionalValue(controlField: string, isChecked: boolean) {
    if (isChecked) {
      this.form.controls[controlField].setValue("");
      this.form.controls[controlField].enable();
    }
    else {
      this.form.controls[controlField].setValue(null);
      this.form.controls[controlField].disable();
    }
  }

  private setInitialCredential(credential: Credential) {
    this.originalCredential = credential;
    if (!credential.isNew) {
        credential.isNew = false;
    }
    if (!credential.updateIdentifier) {
      credential.updateIdentifier = true;
    }
    if (!credential.updateSecret) {
      credential.updateSecret = true;
    }
    this.form.setValue(credential);
  }

  saveCredential() {
    const cred = this.form.getRawValue();//this.form.value;
    if (!this.isValid) {
      throw new Error('Not a valid credential');
    }

    const verb = cred.isNew ? 'Create' : 'Save';

    this.subscription.add(this.confirmationService.show({
        content: dedent`
          ## ${verb} ${escapeMarkdown(cred.id)}?

          This will ${verb.toLowerCase()} the credential.
        `,
        buttonYesText: `${verb} credential`,
        buttonCancelText: 'Cancel',
        emitCancelEvent: false,
      }).pipe(
      switchMap(() => this.credentialService.save(cred))
    ).subscribe({
      next: savedCred => {
        this.setInitialCredential(savedCred);
        this.saved();
      },

      error: err => {
        this.showErrorMessage(dedent`
          ## Error saving credential

          There was a problem when saving the error credential:


          ${escapeMarkdown(err.message)}
        `);
      }
    }));
  }

  private showErrorMessage(content: string) {
    this.subscription.add(this.confirmationService.show({
      content: content,
    }).subscribe())
  }
}