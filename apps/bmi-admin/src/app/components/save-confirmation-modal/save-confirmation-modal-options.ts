export interface SaveConfirmationModalOptions {
  title: string;
  body: string;

  showDiff?: boolean;
  original: string;
  modified: string;
}
