import { Component } from '@angular/core';
import { ModalContext } from '@bmi/ui';
import { SaveConfirmationModalOptions } from './save-confirmation-modal-options';

@Component({
  template: `
    <p>{{options.body}}</p>

    <div *ngIf="options.showDiff && hasModifications">
      <hr>
      <h2>Comparison of changes</h2>

      <p>
        This allows you to audit the changes to the JSON that will be updated
        in Gravity. If you see significant changes here that you didn't intend
        to make, there's possibly a bug in the admin UI, and saving could lead
        to corruption.
      </p>

      <bmi-admin-diff-viewer
        *ngIf="options.showDiff"
        [original]="options.original"
        [modified]="options.modified">
      </bmi-admin-diff-viewer>
    </div>
  `,

  styles: [`
    bmi-admin-diff-viewer {
      height: 400px;
    }
  `]
})
export class SaveConfirmationModalComponent {
  constructor(private modalContext: ModalContext) {}

  get options() {
    return this.modalContext.data as SaveConfirmationModalOptions;
  }

  get hasModifications() {
    return this.options.showDiff && this.options.original !== this.options.modified;
  }
}
