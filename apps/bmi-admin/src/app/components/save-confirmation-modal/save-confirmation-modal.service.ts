import { Injectable } from '@angular/core';
import { filter } from 'rxjs/operators';
import { ModalService } from '@bmi/ui';
import { SaveConfirmationModalComponent } from './save-confirmation-modal.component';
import { SaveConfirmationModalOptions } from './save-confirmation-modal-options';


@Injectable({providedIn: 'root'})
export class SaveConfirmationModalService {
  constructor(private modalService: ModalService) {}

  show(options: SaveConfirmationModalOptions) {
    return this.modalService.open({
      title: options.title || 'Save?',
      content: SaveConfirmationModalComponent,
      data: options,
      size: options.showDiff ? 'large' : 'small',
      actions: [
        {
          action: 'save',
          buttonStyle: 'primary',
          buttonText: 'Save',
        },
        {
          action: 'cancel',
          buttonStyle: 'ghost',
          buttonText: 'Cancel',
        }
      ]
    }).pipe(
      filter(result => result.action !== 'cancel')
    );
  }
}



export function jsonifyForDiff(object: unknown): string {
  return JSON.stringify(object, undefined, 2);
}
