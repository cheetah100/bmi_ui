import { Component } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { WidgetConfig, GridLayoutUtils, PageDataContext, FilterService } from '@bmi/core';
import { getPreference, setPreference } from '@bmi/utils';
import { FocusPanelService } from './focus-panel.service';


@Component({
  template: `
    <h1>Toolbox</h1>
    <button type="button" class="btn btn--link btn--small" (click)="clearAll()">Clear all</button>

    <bmi-widget-host [config]="gridConfig | async"></bmi-widget-host>

    <bmi-add-widget-dropdown
        type="admin"
        [alwaysShowModal]="true"
        [showSpecialOptions]="false"
        (widgetCreated)="addWidget($event)">
      <button type=button class="btn btn--link">
        <ui-icon icon="cui-plus" [fixedWidth]="true"></ui-icon> Add Tool
      </button>
    </bmi-add-widget-dropdown>
  `,

  styles: [`
    bmi-add-widget-dropdown {
      display: inline-block;
    }
  `],
  providers: [
    FilterService,
    PageDataContext,
  ]
})
export class FocusPanelComponent {
  constructor(
    private focusPanelService: FocusPanelService
  ) {}

  gridConfig = this.focusPanelService.widgets.pipe(
    map(widgets => GridLayoutUtils.createStackedGrid(widgets))
  );

  addWidget(widget: WidgetConfig) {
    this.focusPanelService.addWidget(widget);
  }

  clearAll() {
    this.focusPanelService.clearAll();
  }
}
