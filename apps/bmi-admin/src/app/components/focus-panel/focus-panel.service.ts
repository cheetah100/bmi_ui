import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject, Observable, defer } from 'rxjs';
import { share } from 'rxjs/operators';
import { WidgetConfig } from '@bmi/core';
import { getPreference, setPreference } from '@bmi/utils';


interface FocusPanelPrefs {
  widgets?: WidgetConfig[];
}


const PREFS_KEY = 'focusPanel';


@Injectable({providedIn: 'root'})
export class FocusPanelService {
  constructor() {
    this.loadWidgets();
  }

  private widgetsSubject = new BehaviorSubject<WidgetConfig[]>([]);
  public readonly widgets = this.widgetsSubject.asObservable();

  private widgetAddedSubject = new Subject<WidgetConfig>();
  public readonly widgetAdded = this.widgetAddedSubject.asObservable();

  addWidget(widget: WidgetConfig) {
    this.updateWidgets([...this.widgetsSubject.value, widget]);
    this.widgetAddedSubject.next(widget);
  }

  clearAll() {
    this.updateWidgets([]);
  }

  private updateWidgets(widgets: WidgetConfig[]) {
    this.widgetsSubject.next(widgets);
    this.saveWidgets();
  }

  private loadWidgets() {
    const focusPanelPrefs = getPreference(PREFS_KEY) as FocusPanelPrefs;
    if (focusPanelPrefs && focusPanelPrefs.widgets) {
      this.widgetsSubject.next(focusPanelPrefs.widgets);
    }
  }

  private saveWidgets() {
    setPreference(PREFS_KEY, 'widgets', this.widgetsSubject.value);
  }


  public addCardWatcher(board: string, card: string) {
    this.addWidget({
      widgetType: 'admin:card-watcher',
      fields: {
        board: board,
        card: card,
      }
    })
  }
}
