import { GravityApiCardTask, GravityApiCardTaskAction } from '@bmi/gravity-services';

export interface PerformTaskActionEvent {
  task: GravityApiCardTask;
  action: GravityApiCardTaskAction;
}
