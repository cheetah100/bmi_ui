import { Component, Input, Output, EventEmitter } from '@angular/core';
import { GravityApiCardTask, GravityApiCardTaskAction } from '@bmi/gravity-services';
import { describeTimestampFriendly } from '@bmi/utils';

import { PerformTaskActionEvent } from './perform-task-action-event';

@Component({
  selector: 'bmi-admin-card-task-item',
  templateUrl: './card-task-item.component.html',
  styleUrls: ['./card-task-item.component.scss'],
})
export class CardTaskItemComponent {
  @Input() task: GravityApiCardTask;
  @Output() performAction = new EventEmitter<PerformTaskActionEvent>();

  get isComplete() {
    return !!this.task.complete;
  }

  get assignedUser() {
    return this.task.user;
  }

  get completionDateText() {
    return describeTimestampFriendly(this.task.occuredTime);
  }

  completeTask() {
    this.performAction.next({
      task: this.task,
      action: {taskAction: 'COMPLETE'}
    });
  }

  revertTask() {
    this.performAction.next({
      task: this.task,
      action: {taskAction: 'REVERT'}
    });
  }
}
