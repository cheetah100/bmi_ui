import { Component, Input } from '@angular/core';
import { GravityApiCardTask, CardTaskService } from '@bmi/gravity-services';
import { PerformTaskActionEvent } from './perform-task-action-event';


@Component({
  selector: 'bmi-admin-card-task-list',
  template: `
    <ui-list-group class="ui-list-group--tight">
      <bmi-admin-card-task-item
          uiListGroupItem
          *ngFor="let task of tasks; trackBy: trackByTaskUUID"
          [task]="task"
          (performAction)="performTaskAction($event)">
      </bmi-admin-card-task-item>
    </ui-list-group>
  `
})
export class CardTaskListComponent {
  constructor(
    private taskService: CardTaskService
  ) {}

  @Input() tasks: GravityApiCardTask[];

  performTaskAction(event: PerformTaskActionEvent) {
    const {board, card, taskid} = event.task;
    this.taskService.performAction(board, card, taskid, event.action).subscribe();
  }

  trackByTaskUUID(index: number, task: GravityApiCardTask) {
    return task && task.uuid;
  }
}
