import { Component, Input, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { CardEditorModalService } from '@bmi/core';
import { FocusPanelService } from '../focus-panel/focus-panel.service';


@Component({
  selector: 'bmi-admin-card-toolbar',
  templateUrl: './card-toolbar.component.html'
})
export class CardToolbarComponent {
  constructor(
    private cardEditorModal: CardEditorModalService,
    private focusPanelService: FocusPanelService
  ) {}

  @Input() boardId: string;
  @Input() cardId: string;
  @Input() showPinToFocusPanel = true;
  @Input() small = false;

  private subscription = new Subscription();

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  get buttonClasses() {
    return this.small ? 'btn--small': ''
  }

  pinToFocusPanel() {
    // this.focusPanelService.addCardWatcher(this.boardId, this.cardId);
  }

  editCard() {
    this.subscription.add(this.cardEditorModal.editCard(this.boardId, this.cardId).subscribe());
  }
}
