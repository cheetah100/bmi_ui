import { Component } from '@angular/core';
import { Observable, Subscription, combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { GravityConfigService } from '@bmi/gravity-services';
import { ModuleService } from '@bmi/core';
import { FuzzySearcher, flattenLatest } from '@bmi/utils';


interface SearchData {
  name: string;
  type: string;
  link: any[];
}


@Component({
  selector: 'bmi-admin-global-search',
  templateUrl: './global-search.component.html',
  styleUrls: ['./global-search.component.scss'],
})
export class GlobalSearchComponent {
  constructor(
    private gravityConfigService: GravityConfigService,
    private moduleService: ModuleService,
  ) {}

  private subscription = new Subscription();
  private searcher = new FuzzySearcher<SearchData>();
  private searchResults: SearchData[];
  private searchQuery = '';

  public isOpen = false;

  get hasResults() {
    return !!this.searchResults?.length;
  }

  ngOnInit() {
    this.subscription.add(
      flattenLatest([
        this.getBoardSearchData(),
        this.getModuleSearchData(),
      ]).subscribe(results => {
        this.searcher.items = results;
      })
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private getBoardSearchData(): Observable<SearchData[]> {
    return this.gravityConfigService.watchAllConfigs().pipe(
      map(configs => configs.map(c => <SearchData>{
        type: 'board',
        name: c.name,
        link: ['/board', c.id]
      }))
    );
  }

  private getModuleSearchData(): Observable<SearchData[]> {
    return this.moduleService.getModuleSummaries().pipe(
      map(modules => modules.map(m => <SearchData>{
        type: 'module',
        name: m.name,
        link: ['/module', m.id]
      }))
    );
  }

  applySearch(query: string) {
    this.searchQuery = query;
    this.searchResults = this.searcher.search(query, 'name');
    this.considerWhetherToOpen();
  }

  considerWhetherToOpen() {
    this.isOpen = !!this.searchQuery;
  }

  completeSearch() {
    this.isOpen = false;
    this.searchQuery = '';
  }

  getIcon(result: SearchData) {
    switch (result.type) {
      case 'board':
        return 'cui-devices';
      case 'module':
        return 'cui-grid-view';
      default:
        return null;
    }
  }
}
