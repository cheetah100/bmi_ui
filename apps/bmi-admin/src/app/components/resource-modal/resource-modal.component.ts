import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { ModalContext } from '@bmi/ui';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { BoardResource, BoardResourceSummary, GravityResourceService } from '@bmi/gravity-services';
import { take } from 'rxjs/operators';

@Component({
  template: `
    <div style="height: 80%; overflow: scroll"  #scrolledContents>
      <gravity-board-resource
        [boardResource]="boardResource | async"
        (close)="closed()"
        (save)="saved($event)"
        ></gravity-board-resource>
    </div>
  `,
  styleUrls: ['./resource-modal.component.scss']
})
export class ResourceModalComponent implements OnInit, OnDestroy {
  @Input() resourceSummary: BoardResourceSummary = null;

  @Output() resourceSaved = new EventEmitter();

  private subscription = new Subscription();

  boardResourceSubject = new BehaviorSubject(null);

  boardResource:Observable<BoardResource> = this.boardResourceSubject.asObservable();

  constructor(
    private resourceService: GravityResourceService,
    private modalContext: ModalContext
  ) {}

  ngOnInit() {
    const { id, boardId, isNew } = this.modalContext.data;
    if (!isNew) {
      this.resourceService.get(boardId, id, { invalidateNow: true }).pipe(
        take(1)
      ).subscribe(resource => this.boardResourceSubject.next(resource));
    } else {
      this.boardResourceSubject.next(this.modalContext.data);
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  closed() {
    this.modalContext.complete(
      {
        action: 'closed',
        data: this.boardResourceSubject.value
      });
  }

  saved(resource) {
    this.modalContext.complete(
      {
        action: 'saved',
        data: resource
      });
  }
}
