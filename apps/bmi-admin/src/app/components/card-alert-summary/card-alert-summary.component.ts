import {
  Component,
  OnChanges,
  OnInit,
  SimpleChanges
} from '@angular/core';
import { map, tap } from 'rxjs/operators';
import {
  GravityApiCardEvent, GravityCardAlertService,
  SystemStatusService
} from '@bmi/gravity-services';
import { ValueFormatter } from '@bmi/ui';
import { distinctUntilNotEqual, ObjectMap } from '@bmi/utils';
import { orderBy } from 'lodash-es';

export interface ColumnDefinition {
  label: string;
  width?: string;
  formatter?: ValueFormatter;
  type?: string;
}

@Component({
  selector: 'bmi-card-alert-summary, card-alert-summary',
  templateUrl: './card-alert-summary.component.html',
  styleUrls: ['./card-alert-summary.component.scss']
})
export class CardAlertSummaryComponent implements OnInit, OnChanges {
  public alertSummaries = []
  public sectionState: ObjectMap<boolean> = {}

  isLoading = true;

  cardAlertDetails = this.systemStatusService.getDetails().pipe(
    tap(() => this.isLoading = true),
    map(details => details.alertDetails),
    distinctUntilNotEqual(),
    map(alertDetails => Object.entries(alertDetails).map(([board, alerts]) => {
      return {board, alerts}
    })),
    map(alertSummaries => orderBy(alertSummaries, s => s.alerts.length, ['desc']))
  );

  constructor(
    private systemStatusService: SystemStatusService,
    private cardAlertService: GravityCardAlertService,
  ) {}

  ngOnChanges(changes: SimpleChanges) {}

  ngOnInit() {
    this.cardAlertDetails.subscribe(alertSummaries => {
      this.alertSummaries = alertSummaries;
      this.isLoading = false;
    });
  }

  /**
   * Refresh alerts
   */
  refreshBoard() {
    console.log('refreshBoard => msg:', 'calling it!');
    if (this.isLoading === false) {
      this.systemStatusService.refreshDetails();
      this.isLoading = true;
    }
  }

  updateOpenSection(board, state) {
    this.sectionState[board] = state;
  }
}

