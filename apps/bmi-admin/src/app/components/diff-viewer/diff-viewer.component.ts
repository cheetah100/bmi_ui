import { Component, Input, OnChanges } from '@angular/core';
import { ReplaySubject } from 'rxjs';

import { DiffEditorModel } from 'ngx-monaco-editor';

@Component({
  selector: 'bmi-admin-diff-viewer',
  template: `
    <div class="diff-viewer__headings">
      <h3>Original</h3>
      <h3>Modified</h3>
    </div>
    <ngx-monaco-diff-editor
      [options]="options"
      [originalModel]="originalModel"
      [modifiedModel]="modifiedModel"
      (onInit)="onMonacoInitialized()">
    </ngx-monaco-diff-editor>
  `,

  styles: [`
    ngx-monaco-diff-editor {
      height: inherit;
    }

    .diff-viewer__headings {
      display: flex;
    }

    .diff-viewer__headings h3 {
      flex: 1;
    }
  `]
})
export class DiffViewerComponent implements OnChanges {
  @Input() original: string;
  @Input() modified: string;
  @Input() language = 'json';

  originalModel: DiffEditorModel;
  modifiedModel: DiffEditorModel;

  isEditorReady = false;

  options = {
    theme: 'vs-dark',
    formatOnType: true,
    readOnly: true,
  };

  ngOnChanges() {
    this.originalModel = this.makeDiffModel(this.original);
    this.modifiedModel = this.makeDiffModel(this.modified);
  }

  makeDiffModel(value: string): DiffEditorModel {
    return {
      code: value,
      language: this.language,
    };
  }

  onMonacoInitialized() {
    this.isEditorReady = true;
  }
}
