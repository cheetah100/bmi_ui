import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output, SimpleChanges,
  ViewChild
} from '@angular/core';
import { BoardResource, GravityResourceService } from '@bmi/gravity-services';
import { ConfirmationModalService, IdpValidators, ModalResult, Option, TypedFormControl } from '@bmi/ui';
import { ResourceTextEditorComponent } from '../resource-text-editor/resource-text-editor.component';
import { Observable, of, Subscription, throwError } from 'rxjs';
import lodashIsEqual from 'lodash-es/isEqual';
import { map, shareReplay, switchMap, take } from 'rxjs/operators';
import dedent from 'ts-dedent';
import { escapeMarkdown } from '@bmi/utils';
import { OptionlistService } from '@bmi/core';

function isValidBoardResource(boardResource: BoardResource): boolean {
  return boardResource && !!boardResource.id && !!boardResource.boardId;
}

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gravity-board-resource',
  templateUrl: './board-resource.component.html',
  styleUrls: ['./board-resource.component.scss'],
})
export class BoardResourceComponent implements OnInit, OnDestroy, OnChanges {
  @Input() boardResource: BoardResource = null;

  @Output() close  = new EventEmitter();
  @Output() save = new EventEmitter();

  @ViewChild(ResourceTextEditorComponent, { static: true }) resourceEditor: ResourceTextEditorComponent;

  resourceControl = new TypedFormControl<BoardResource>(null,
    IdpValidators.satisfies(isValidBoardResource,
    'Board resource is not valid'));
  originalResource: BoardResource = null;

  private subscription = new Subscription();

  get resource() {
    return this.resourceControl.value;
  }

  get hasUnsavedChanges() {
    return this.originalResource && !lodashIsEqual(this.originalResource, this.resource);
  }

  get canSave() {
    return this.hasUnsavedChanges && isValidBoardResource(this.resource) && this.isResourceEditorValid;
  }

  get isResourceEditorValid() {
    return this.resourceEditor && this.resourceEditor.isValid;
  }

  constructor(
    private resourceService: GravityResourceService,
    private confirmationService: ConfirmationModalService,
  ) {}

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.boardResource) {
      this.setInitialResource(this.boardResource)
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  saveIt():ModalResult {
    this.saveResource();
    return null;
  }

  closed() {
    this.close.emit();
  }

  saved() {
    this.save.emit(this.originalResource)
  }

  private setInitialResource(boardResource: BoardResource) {
    this.originalResource = boardResource;
    this.resourceControl.setValue(boardResource);
  }

  saveResource() {
    const boardResource = this.resourceControl.value;
    if (!isValidBoardResource(boardResource)) {
      throw new Error('Not a valid resource');
    }

    const verb = boardResource.isNew ? 'Create' : 'Save';
    const board = boardResource.boardId;

    this.subscription.add(of(boardResource).pipe(
      switchMap(resource => this.canResourceBeSafelyCreated(resource)),
      switchMap(resource => this.confirmationService.show({
        content: dedent`
          ## ${verb} ${escapeMarkdown(resource.id)}?

          This will ${verb.toLowerCase()} the resource on the ${escapeMarkdown(board)} board.
        `,
        buttonYesText: `${verb} resource`,
        buttonCancelText: 'Cancel',
        emitCancelEvent: false,
      })),
      switchMap(() => this.resourceService.save(boardResource.boardId, boardResource))
    ).subscribe({
      next: savedResource => {
        this.setInitialResource(savedResource);
        this.saved();
      },

      error: err => {
        this.showErrorMessage(dedent`
          ## Error saving resource

          There was a problem when saving the error resource:


          ${escapeMarkdown(err.message)}
        `);
      }
    }));
  }

  private canResourceBeSafelyCreated(resource: BoardResource): Observable<BoardResource> {
    if (!isValidBoardResource(resource)) {
      return throwError(new Error('Board Resource is not valid. This is a bug.'))
    } else {
      return this.resourceService.list(resource.boardId).pipe(
        take(1),
        map(resources => {
          const resourceDoesNotExist = !resources.some(r => r.id === resource.id);
          if (!resource.isNew && resourceDoesNotExist) {
            throw new Error('Cannot update resource if it does not exist in Gravity');
          } else if (resource.isNew && !resourceDoesNotExist) {
            throw new Error('Cannot create resource as it already exists');
          } else {
            return resource;
          }
        })
      );
    }
  }

  private showErrorMessage(content: string) {
    this.subscription.add(this.confirmationService.show({
      content: content,
    }).subscribe())
  }

}
