import { Portal } from '@angular/cdk/portal';


/**
 *
 */
export interface SidebarPanel {
  id: string;
  title: string;
  content: Portal<any>
}
