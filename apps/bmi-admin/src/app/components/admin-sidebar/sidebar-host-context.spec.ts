import { Subscription } from 'rxjs';
import { SidebarHostContext } from './sidebar-host-context';
import { SidebarPanel } from './sidebar-panel';
import { ObservableWatcher } from '@bmi/utils';


describe('SidebarHostContext', () => {
  let ctx: SidebarHostContext;
  let panelsWatch: ObservableWatcher<SidebarPanel[]>;
  let activeWatch: ObservableWatcher<SidebarPanel>;
  beforeEach(() => {
    ctx = new SidebarHostContext();
    panelsWatch = new ObservableWatcher(ctx.panels);
    activeWatch = new ObservableWatcher(ctx.activePanel);
  });

  it('should initially have an empty array of sidebars', () =>
    expect(ctx.currentPanels).toEqual([]));

  it('should initially have an undefined active panel', () =>
    expect(ctx.currentActivePanel).toBeUndefined());


  describe('Calling addSidebar() with no existing panels', () => {
    let panel: SidebarPanel;
    let subscription: Subscription;

    beforeEach(() => {
      panel = <SidebarPanel>{
        id: 'test-panel',
        title: 'Test Panel',
        content: null
      };

      subscription = ctx.addSidebar(panel);
    })

    it('should return a value', () =>
      expect(subscription).toBeDefined());

    it('should return a valid subscription', () =>
      expect(typeof subscription.unsubscribe === 'function'));

    it('should add the panel to the list of current panels', () =>
      expect(ctx.currentPanels).toContain(panel));

    it('should cause panels to emit', () =>
      expect(panelsWatch.latestValue).toContain(panel));

    it('should make it the active panel', () =>
      expect(ctx.currentActivePanel).toBe(panel));


    describe('When removing the panel', () => {
      beforeEach(() => subscription.unsubscribe());

      it('should remove the panel', () =>
        expect(ctx.currentPanels).not.toContain(panel));

      it('should emit a new panels array with the panel removed', () =>
        expect(panelsWatch.values).toEqual([[], [panel], []]));

      it('should clear the active panel, as there are no panels left', () =>
        expect(ctx.currentActivePanel).toBeUndefined());
    });

    describe('After another panel', () => {
      let secondPanel: SidebarPanel;
      let secondSub: Subscription;
      beforeEach(() => {
        secondPanel = {
          id: 'panel2',
          title: 'Second Panel',
          content: null
        };

        secondSub = ctx.addSidebar(secondPanel);
      });

      it('should add the second panel to the collection', () =>
        expect(ctx.currentPanels).toContain(secondPanel));

      it('should still contain the first panel', () =>
        expect(ctx.currentPanels).toContain(panel));

      it('should not change the active panel, as the first panel still exists', () =>
        expect(ctx.currentActivePanel).toBe(panel));

      describe('When unsubbing the this second panel', () => {
        beforeEach(() => secondSub.unsubscribe());

        it('should remove the second panel when unsubcribing it', () =>
          expect(ctx.currentPanels).not.toContain(secondPanel));

        it('should leave the first panel', () =>
          expect(ctx.currentPanels).toContain(panel));
      });
    });
  });

  describe('Realistic multi panel scenario', () => {
    const focusPanel: SidebarPanel = {id: 'focus-panel', title: 'Focus Panel', content: null};
    const helpPanel: SidebarPanel = {id: 'help', title: 'Help', content: null};

    let focusSub: Subscription;
    let helpSub: Subscription;

    beforeEach(() => {
      focusSub = ctx.addSidebar(focusPanel);
      helpSub = ctx.addSidebar(helpPanel);
    });

    it('should activate the first panel by default', () =>
      expect(ctx.currentActivePanel).toBe(focusPanel));

    describe('Switching to an existing panel by object-ref', () =>{
      beforeEach(() => ctx.activatePanel(helpPanel));

      it('should switch to panel', () =>
        expect(ctx.currentActivePanel).toBe(helpPanel));

      it('should emit an event when the panel is switched', () =>
        expect(activeWatch.latestValue).toBe(helpPanel));
    });

    it('should switch panels by ID', () => {
      ctx.activatePanel('help');
      expect(ctx.currentActivePanel).toBe(helpPanel);
    });

    describe('Removing the activated panel', () => {
      let watch: ObservableWatcher<SidebarPanel>;
      beforeEach(() => {
        watch = new ObservableWatcher(ctx.activePanel);
        ctx.activatePanel(helpPanel);
        ctx.removeSidebar(helpPanel);
      });

      it('should switch back to the first panel', () =>
        expect(ctx.currentActivePanel).toBe(focusPanel));

      it('should not emit excessive active changed events', () =>
        expect(watch.values).toEqual([focusPanel, helpPanel, focusPanel]));

      it('should remove the panel from the list', () =>
        expect(ctx.currentPanels).not.toContain(helpPanel));
    });

    describe('Removing all panels', () => {
      beforeEach(() => {
        focusSub.unsubscribe();
        helpSub.unsubscribe();
      });

      it('should leave the active panel as undefined', () =>
        expect(ctx.currentActivePanel).toBeUndefined());

      it('should have no active panels', () =>
        expect(ctx.currentPanels).toEqual([]));

      it('should have switched the active panel as they were removed', () =>
        expect(activeWatch.values).toEqual([undefined, focusPanel, helpPanel, undefined]));
    });

  });
});
