import { Component } from '@angular/core';
import { SidebarPanel } from './sidebar-panel';
import { SidebarHostContext } from './sidebar-host-context';

@Component({
  selector: 'bmi-admin-sidebar',
  templateUrl: './admin-sidebar.component.html',
  styleUrls: ['./admin-sidebar.component.scss']
})
export class AdminSidebarComponent {
  constructor(
    private sidebarHost: SidebarHostContext
  ) {}

  panels = this.sidebarHost.panels;
  activePanel = this.sidebarHost.activePanel;

  activatePanel(panel: SidebarPanel) {
    this.sidebarHost.activatePanel(panel);
  }

}
