import { Injectable } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';
import { SidebarPanel } from './sidebar-panel';
import isNil from 'lodash-es/isNil';


@Injectable()
export class SidebarHostContext {
  private panelsSubject = new BehaviorSubject<SidebarPanel[]>([]);
  private activePanelSubject = new BehaviorSubject<SidebarPanel>(undefined);

  public readonly panels = this.panelsSubject.asObservable();
  public readonly activePanel = this.activePanelSubject.asObservable();

  get currentActivePanel() {
    return this.activePanelSubject.value;
  }

  get currentPanels() {
    return this.panelsSubject.value as ReadonlyArray<SidebarPanel>;
  }

  addSidebar(panel: SidebarPanel): Subscription {
    this.panelsSubject.next([...this.currentPanels, panel]);
    this.updateActivePanel();
    return new Subscription(() => this.removeSidebar(panel));
  }

  removeSidebar(panel: SidebarPanel) {
    this.panelsSubject.next(this.currentPanels.filter(p => p !== panel));
    this.updateActivePanel();
  }

  /**
   *
   */
  activatePanel(panel_or_id: string | SidebarPanel) {
    const panel = this.resolvePanel(panel_or_id);
    if ((isNil(panel_or_id) && this.currentActivePanel) || panel !== this.currentActivePanel) {
      this.activePanelSubject.next(panel);
    }
  }

  /**
   * Looks up the sidebar panel by object ref or ID, returning undefined if not
   * found.
   *
   * If there are multiple instances of the same ID, it will pick the first.
   * If the panel does not exist -- either due to no ID match, or if the panel
   * object isn't in the list, then it will return undefined.
   */
  private resolvePanel(panel_or_id: string | SidebarPanel): SidebarPanel | undefined {
    if (typeof panel_or_id === 'string') {
      return this.currentPanels.find(p => p.id === panel_or_id)
    } else {
      return this.currentPanels.includes(panel_or_id) ? panel_or_id : undefined;
    }
  }

  private updateActivePanel() {
    if (!this.currentActivePanel || !this.currentPanels.includes(this.currentActivePanel)) {
      this.activatePanel(this.currentPanels[0]);
    }
  }
}
