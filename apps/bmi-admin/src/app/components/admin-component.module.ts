import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PortalModule } from '@angular/cdk/portal';

import { SharedUiModule } from '@bmi/ui';
import { BmiCoreModule, BmiCoreEditorModule } from '@bmi/core';
import { MonacoEditorModule } from 'ngx-monaco-editor';

import { HelpSidebarComponent } from './help/help-sidebar.component';

import { MainLayoutComponent } from './layout/main-layout.component';
import { ResourceTextEditorComponent } from './resource-text-editor/resource-text-editor.component';
import { CredentialEditorComponent } from './credential-editor/credential-editor.component'
import { DiffViewerComponent } from './diff-viewer/diff-viewer.component';
import { SaveConfirmationModalComponent } from './save-confirmation-modal/save-confirmation-modal.component';
import { AdminSidebarComponent } from './admin-sidebar/admin-sidebar.component';

import { CardAlertSummaryComponent } from './card-alert-summary/card-alert-summary.component';
import { CardAlertComponent } from './card-alert/card-alert.component';

import { ResourceModalComponent } from './resource-modal/resource-modal.component';
import { CredentialModalComponent } from './credential-modal/credential-modal.component';
import { BoardResourceComponent } from './board-resource/board-resource.component';
import { FocusPanelComponent } from './focus-panel/focus-panel.component';

import { CardToolbarComponent } from './card-toolbar/card-toolbar.component';

import { CardTaskItemComponent } from './card-tasks/card-task-item.component'
import { CardTaskListComponent } from './card-tasks/card-task-list.component';

import { GlobalSearchComponent } from './global-search/global-search.component';

@NgModule({
  imports: [
    CommonModule,
    SharedUiModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    PortalModule,
    BmiCoreModule,
    BmiCoreEditorModule,
    MonacoEditorModule,
  ],

  declarations: [
    HelpSidebarComponent,
    MainLayoutComponent,
    CredentialEditorComponent,
    ResourceTextEditorComponent,
    DiffViewerComponent,
    SaveConfirmationModalComponent,
    AdminSidebarComponent,
    CardAlertSummaryComponent,
    CardAlertComponent,
    CredentialModalComponent,
    ResourceModalComponent,
    BoardResourceComponent,
    FocusPanelComponent,
    CardToolbarComponent,
    CardTaskItemComponent,
    CardTaskListComponent,
    GlobalSearchComponent,
  ],

  entryComponents: [
    SaveConfirmationModalComponent,
    ResourceModalComponent,
    HelpSidebarComponent,
    FocusPanelComponent,
  ],

  exports: [
    HelpSidebarComponent,
    MainLayoutComponent,
    CredentialEditorComponent,
    ResourceTextEditorComponent,
    DiffViewerComponent,
    SaveConfirmationModalComponent,
    AdminSidebarComponent,
    CardAlertSummaryComponent,
    CardAlertComponent,
    CredentialModalComponent,
    ResourceModalComponent,
    BoardResourceComponent,
    FocusPanelComponent,
    CardToolbarComponent,
    CardTaskItemComponent,
    CardTaskListComponent,
    GlobalSearchComponent,
  ]
})
export class AdminComponentsModule {}
