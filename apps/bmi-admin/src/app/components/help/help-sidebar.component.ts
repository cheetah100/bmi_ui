import { Component, Input, Output, EventEmitter } from '@angular/core';
import { SafeHtml } from '@angular/platform-browser';
import { HelpService } from './help.service';

@Component({
  selector: 'gravity-help-sidebar',
  templateUrl: './help-sidebar.component.html',
  styleUrls: ['./help-sidebar.component.scss']
})
export class HelpSidebarComponent {
  constructor (
    private helpService: HelpService
  ) {}

  helpHtml = this.helpService.currentHelpHtml;
}
