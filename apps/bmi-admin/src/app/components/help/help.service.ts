import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { SafeHtml, DomSanitizer } from '@angular/platform-browser';
import { distinctUntilChanged, map, publishBehavior, refCount } from 'rxjs/operators';

import * as showdown from 'showdown';

/**
 * A service for rendering Markdown help pages.
 *
 * This service should be injected into components within the gravity admin app
 * which can call display() to set or clear the help text.
 *
 * A top-level component can be responsible for rendering the Markdown document
 * in a sidebar panel, without requiring individual components to host their own
 * help UI.
 */
@Injectable({providedIn: 'root'})
export class HelpService {
  private markdownConverter = new showdown.Converter({
    openLinksInNewWindow: true,
    tables: true,
    literalMidWordUnderscores: true,
  });

  private currentMarkdownSubject = new BehaviorSubject<string>(null);
  private renderedMarkdown = this.currentMarkdownSubject.pipe(
    distinctUntilChanged(),
    map(text => {
      if (text) {
        // We're going to trust the Markdown content that's being fed into the
        // help service, as this should only ever come from inside the app.
        //
        // Displaying user-entered Markdown with this service would pose a
        // serious XSS vulnerability!
        const html = this.markdownConverter.makeHtml(text);
        return this.domSanitizer.bypassSecurityTrustHtml(html);
      } else {
        return null;
      }
    }),
    publishBehavior(null),
    refCount()
  );

  constructor(private domSanitizer: DomSanitizer) {}

  /**
   * Gets the current rendered help text as HTML.
   */
  get currentHelpHtml(): Observable<SafeHtml> {
    return this.renderedMarkdown;
  }

  /**
   * Sets the helptext to display in the sidebar of the app.
   *
   * Setting this value to null means there is no help text to display, and the
   * sidebar will probably disappear.
   *
   * @param helpMarkdown the help text, formatted as Markdown
   */
  public display(helpMarkdown: string | null) {
    this.currentMarkdownSubject.next(helpMarkdown);
  }
}
