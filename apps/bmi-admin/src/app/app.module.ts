import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PortalModule } from '@angular/cdk/portal';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';


import {
  SharedUiModule,
  ModalService,
  ConfirmationModalService,
  ConfirmationModalComponent,
  CloseDialogService,
  SimpleCloseDialogService,
  CLOSE_DIALOG_SERVICE_TOKEN
} from '@bmi/ui';

import { BmiChartsModule, BmiChartsEditorModule } from '@bmi/legacy-charts';

import {
  BmiCoreModule,
  BmiCoreEditorModule,
  BmiWidgetsModule,
  BMI_PAGE_ROUTE_GENERATOR,
  BmiResolvedPageRoute
} from '@bmi/core';

//import { ChartSelectorComponent } from '@bmi/legacy-charts/lib/chart-editor/chart-selector/chart-selector.component';

import { GravityModule, GravityUiModule } from '@bmi/gravity-services';

import { AppComponent } from './app.component';
import { GravityAdminRoutingModule } from './gravity-admin.routing';
import { AdminComponentsModule } from './components/admin-component.module';

import { BoardHomeComponent } from './pages/board-home/board-home.component';
import { BoardShellComponent } from './pages/board-home/board-shell.component';
import { TestModalComponent } from './pages/board-home/test-modal.component';
import { BoardEditorShimComponent } from './pages/board-editor-shim/board-editor-shim.component';

import { BoardEditorModule } from './board-editor/board-editor.module';
import { BoardDetailsService } from './board-editor/board-details.service';

import { DataViewerModule } from './data-viewer/data-viewer.module';
import { DataViewerRoutesService } from './data-viewer/data-viewer-routes.service';
import { BoardDataViewerShimComponent } from './pages/board-data-viewer-shim/board-data-viewer-shim.component';
import { CardDataViewerShimComponent } from './pages/board-data-viewer-shim/card-data-viewer-shim.component';
import { GravityAdminDataViewerRoutesService } from './pages/board-data-viewer-shim/gravity-admin-data-viewer-routes.service';
import { BoardResourcePageComponent } from './pages/board-resource-page/board-resource-page.component';

import { TransformEditorModule } from './transform-editor/transform-editor.module';
import { TransformHomeComponent } from './pages/transform-home/transform-home.component';
import { TransformPreviewShimComponent } from './pages/transform-home/transform-preview-shim.component';
import { BoardSummaryComponent } from './pages/board-home/board-summary.component';
import { BoardInfoService } from './pages/board-home/board-info.service';

import { FilterEditorModule } from './filter-editor/filter-editor.module';

import { RulesAdminModule } from './rule-builder/rules-admin.module';
import { RuleBuilderShimComponent } from './pages/rule-builder-shim/rule-builder-shim.component';

import { SystemPageComponent } from './pages/system-page/system-page.component';

import { MonacoEditorModule, NgxMonacoEditorConfig } from 'ngx-monaco-editor';
import { ViewSummaryComponent } from './pages/view-home/view-summary.component';
import { ViewEditorShimComponent } from './pages/view-home/view-editor-shim.component';
import { ViewEditorModule } from './view-editor/view-editor.module';
import { FilterEditorShimComponent } from './pages/filter-editor-shim/filter-editor-shim.component';
import { WidgetEditorShimComponent } from './pages/widget-editor-shim/widget-editor-shim.component';
import { AppService } from './app.service';
import { ModuleHubPageComponent } from './pages/module-hub/module-hub-page.component';
import { WidgetHomeComponent } from './pages/widget-home/widget-home.component';
import { MenuEditorModule } from './menu-editor/menu-editor.module';
import { MenuPageComponent } from './pages/menu-page/menu-page.component';
import {
  ModuleInfoEditorComponent,
  ModuleInfoResolver
} from './pages/module-info-editor';

import { PageDesignerPageComponent } from './pages/page-designer/page-designer-page.component';

import { RootHomeComponent } from './pages/root-home/root-home.component';

import { IntegrationsHomeComponent } from './pages/integrations-home/integrations-home.component';
import { IntegrationEditorPageComponent } from './pages/integration-editor-page/integration-editor-page.component';
import { CredentialEditorPageComponent } from './pages/credential-editor-page/credential-editor-page.component';

import { BmiAdminWidgetsModule } from './widgets/bmi-admin-widgets.module';
import { TransformEditorPageComponent } from './pages/transform-editor/transform-editor-page.component';

const monacoConfig: NgxMonacoEditorConfig = {
  baseUrl: `./assets`
};

@NgModule({
  declarations: [
    AppComponent,
    BoardShellComponent,
    BoardHomeComponent,
    TestModalComponent,
    BoardEditorShimComponent,
    BoardDataViewerShimComponent,
    CardDataViewerShimComponent,
    TransformHomeComponent,
    TransformPreviewShimComponent,
    BoardSummaryComponent,
    RuleBuilderShimComponent,
    ViewSummaryComponent,
    ViewEditorShimComponent,
    FilterEditorShimComponent,
    WidgetEditorShimComponent,
    ModuleHubPageComponent,
    WidgetHomeComponent,
    MenuPageComponent,
    ModuleInfoEditorComponent,
    PageDesignerPageComponent,
    RootHomeComponent,
    BoardResourcePageComponent,
    SystemPageComponent,
    IntegrationsHomeComponent,
    IntegrationEditorPageComponent,
    CredentialEditorPageComponent,
    TransformEditorPageComponent
  ],

  imports: [
    CommonModule,
    BrowserModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    SharedUiModule,
    GravityModule.forRoot(),
    GravityUiModule,
    BoardEditorModule,
    DataViewerModule,
    TransformEditorModule,
    FilterEditorModule,
    RulesAdminModule,
    ViewEditorModule,
    BmiChartsModule.forRoot(),
    BmiChartsEditorModule,
    MonacoEditorModule.forRoot(monacoConfig),
    GravityAdminRoutingModule,
    MenuEditorModule,
    PortalModule,
    AdminComponentsModule,

    BmiCoreModule,
    BmiCoreEditorModule,
    BmiWidgetsModule,
    BmiAdminWidgetsModule,

  ],

  entryComponents: [
    ConfirmationModalComponent,
    TestModalComponent,

  ],

  bootstrap: [AppComponent],

  providers: [
    AppService,
    ModalService,
    ConfirmationModalService,
    SimpleCloseDialogService,
    {
      provide: CLOSE_DIALOG_SERVICE_TOKEN,
      useExisting: SimpleCloseDialogService
    },

    BoardDetailsService,
    BoardInfoService,
    ModuleInfoResolver,
    {
      provide: DataViewerRoutesService,
      useClass: GravityAdminDataViewerRoutesService
    },

    {
      provide: BMI_PAGE_ROUTE_GENERATOR,
      useValue: (route: BmiResolvedPageRoute) => ['/module', route.moduleId, 'pages', route.pageId, route.filters || {}]
    }
  ]
})
export class AppModule {}
