import { Injectable } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';


@Injectable({providedIn: 'root'})
export class AdminAppNavService {
  constructor(private router: Router) {}

  home = this.route(['/']);
  integrationHome = this.route(['/system/integrations']);
  transformHome = this.route(['/transform']);
  systemStatus = this.route(['/system']);

  private route(commands: any[]) {
    return new AppRoute(commands, this.router);
  }

  moduleHub(moduleId: string) {
    return this.route(['/module', moduleId]);
  }

  editExistingPage(moduleId: string, pageId: string) {
    return this.route(['/module', moduleId, 'pages', pageId]);
  }

  cardViewer(boardId: string, cardId: string) {
    return this.route(['/board', boardId, 'data-viewer', cardId]);
  }
}


class AppRoute {
  constructor (
    public readonly commands: any[],
    private router: Router
   ) {}

  navigate(extras?: NavigationExtras) {
    this.router.navigate(this.commands, extras);
  }

  get url() {
    return this.router.createUrlTree(this.commands).toString();
  }
}
