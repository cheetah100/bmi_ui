import { CardData } from '@bmi/gravity-services';


export interface CardRuleDebuggerState {
  boardId: string;
  cardId: string;

  // Timestamp for the state
  snapshotTime: number;

  rules: RuleExecutionState[];


  // Include a snapshot of the card right now. this could allow time-travel
  // debugging in the UI.
  card: CardData;

  // There could be a separate API for getting the events log, as each of these
  // is timestamped. Could be based off the card events API instead.
  // events: RuleDebuggerEvent[];
}


export interface RuleExecutionState {
  ruleId: string;
  explanation: string;   // Briefly, why is the rule in this state?22
  areConditionsMet: boolean;
  canRuleExecute: boolean;

  taskRuleState?: 'not-started' | 'active' | 'complete';

  conditionDetails: {
    automationConditions: { [conditionId: string]: boolean },
    //...
  }
}


export interface RuleDebuggerEvent {
  ruleId: string;
  actionId?: string;
  message: string;
  occurredAt: number;
  data?: any;
}
