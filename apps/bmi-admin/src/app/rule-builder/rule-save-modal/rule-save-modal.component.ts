import { Component, Input, EventEmitter, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Observable } from 'rxjs';

import { ModalComponent } from '@bmi/ui';

import { Rule } from '@bmi/gravity-services';
import { RuleValidationError, ValidationErrorSeverity } from '../rule-validator';


@Component({
  selector: 'gravity-rule-save-modal',
  templateUrl: './rule-save-modal.component.html',
  styleUrls: ['./rule-save-modal.component.scss']
})
export class RuleSaveModalComponent {
  rule: Rule;
  validationErrors: RuleValidationError[] = [];

  @ViewChild(ModalComponent, { static: true }) modal: ModalComponent;

  private dialogClosedEvent = new EventEmitter<boolean>();

  public isDangerousRule = false;
  public yesIKnowItIsDangerous = false;
  public canEditDangerousRules = false;

  constructor(private changeDetector: ChangeDetectorRef) {}


  get canSaveRule() {
    return !this.isDangerousRule || (this.canEditDangerousRules && this.yesIKnowItIsDangerous);
  }


  show(rule: Rule, validationErrors: RuleValidationError[], canEditDangerousRules = true): Observable<boolean> {
    this.rule = rule;
    this.validationErrors = validationErrors;
    this.yesIKnowItIsDangerous = false;
    this.isDangerousRule = validationErrors.length > 0 && validationErrors.some(e => e.severity !== 'INFO');
    this.canEditDangerousRules = canEditDangerousRules;

    this.changeDetector.markForCheck();

    this.modal.show();
    return this.dialogClosedEvent;
  }

  confirm() {
    if (this.canSaveRule) {
      this.modal.close();
      this.dialogClosedEvent.emit(true);
    }
  }

  close() {
    this.modal.close();
    this.dialogClosedEvent.emit(false);
  }

  getSeverityAlertBoxType(severity: ValidationErrorSeverity): string {
    switch (severity) {
      case 'CRITICAL':
        return 'error';
      case 'WARNING':
        return 'warning';
      default:
        return 'default';
    }
  }
}
