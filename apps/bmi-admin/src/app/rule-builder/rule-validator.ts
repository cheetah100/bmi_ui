import { Observable, of as observableOf, concat, empty, isObservable } from 'rxjs';
import { filter, map, toArray } from 'rxjs/operators';

import { Rule, Action } from '@bmi/gravity-services';
import { sortBy } from '@bmi/utils';


// This module defines a system for doing a 'preflight check' on rules before
// they get saved.
//
// This file defines multiple validator functions which will get run against a
// rule, and these validator functions can return or emit validation errors if
// they detect anything amiss.
//
// A rule validator can either return a RuleValidationError, or an
// Observable<RuleValidationError> which can emit multiple values, or nothing.
//
// If a rule validator doesn't return a result -- or if the value returned is
// falsy, then it will be treated as an empty sequence of errors.


export type ValidationErrorSeverity = 'INFO' | 'WARNING' | 'CRITICAL';
export type RuleHazard = 'loop' | 'noop' | 'heavy-load';

export interface RuleValidationError {
  /** How significant is this error? */
  severity: ValidationErrorSeverity;

  /**
   * Optionally lists 'hazards' that this rule presents.
   *
   * Currently, these will show up as badges in the list of error messages, but
   * a future change could aggregate these across all errors to display as a
   * summary.
   */
  hazards?: RuleHazard[];

  /** A short, one-line description of the error. This will be the title. */
  synopsis: string;

  /** A longer, Markdown formatted error message. */
  detailedMessage: string;
}

// This is the order that validation errors will be returned in,
const SEVERITY_ORDER: ValidationErrorSeverity[]  = [
  'CRITICAL',
  'WARNING',
  'INFO'
];


/**
 * Detect the amount of indentation and remove this from all of the lines.
 *
 * This makes it easier to use multi-line strings with Markdown text in them
 * without having to break the indention of this source code file.
 */
function trimIndent(text: string): string {
  const lines = text.split('\n');
  const firstNonBlank = lines.find(t => !!t.match(/\S/g));
  const initialWhitespaceMatch = firstNonBlank.match(/^\s*/g);
  if (initialWhitespaceMatch) {
    // Now remove this from the start of each string by building a regex from
    // the matched spaces. If you mix tabs and spaces then you are a bad person
    // and should set up your editor properly :D
    const indentationRegex = new RegExp(`^${initialWhitespaceMatch[0]}`);
    return lines.map(line => line.replace(indentationRegex, '')).join('\n');
  } else {
    return text;
  }
}


function wrapNonObservables(validationResult: RuleValidationError | Observable<RuleValidationError>): Observable<RuleValidationError> {
  if (!validationResult) {
    return empty();
  } else if (isObservable(validationResult)) {
    return validationResult;
  } else {
    return observableOf(validationResult);
  }
}


/**
 * Checks a rule for potential issues
 *
 * This performs more in-depth checks on a rule than are possible with simple
 * form validation, such as trying to detect dangerous conditions.
 *
 * This can return multiple errors of varying severity -- critical errors should
 * be addressed, but warnings and info messages are possible too.
 *
 * @param rule the rule to check for issuess
 * @returns an observable that will emit an array of issues. If there are no
 *     problems this will emit an empty array.
 */
export function checkRuleForErrors(rule: Rule): Observable<RuleValidationError[]> {
  return concat(...[
    // Add validators to this list. These can be synchronous, returning a
    // RuleValidationError (or a falsy value), or they can return an
    // Observable<RuleValidationError> if they need to do some async work, or
    // want to return multiple values.
    checkThatRuleHasActions(rule),
    warnIfCompulsoryRuleIsUnconditional(rule),
    checkThatAtLeastOneConditionIsPersisted(rule),
    warnIfMakingCompulsoryRule(rule),
    errorIfValidationRuleIsMutating(rule),
    warnAboutScriptActions(rule),
  ].map(r => wrapNonObservables(r))).pipe(
    // The wrapNonObservables call above should filter out null returns from
    // synchronous validators, but the async ones might still emit falsy
    // values.
    filter(r => !!r),
    toArray(),
    map(errors => sortBy(errors, v => SEVERITY_ORDER.findIndex(x => x === v.severity) || 0))
  );
}


// TODO: perhaps instead we could use the ActionType metadata to find
// validation-safe actions rather than duplicating this info here.
const BAD_ACTIONS_FOR_VALIDATION_RULES = [
  'persist'
];


function warnAboutScriptActions(rule: Rule): RuleValidationError {
  // I've lowered this to an info so that non-developers can still update rules
  // with script actions, but really this should be a warning for new/edited
  // scripts but we don't have a good way of knowing if it's new.
  if (rule.actions.some(a => a.type === 'script')) {
    return {
      severity: 'INFO',
      synopsis: 'Script actions are an advanced feature',
      detailedMessage: trimIndent(`
        JavaScript actions are a powerful feature but need to be treated with
        care. Any syntax or semantic errors in the script will not be picked up
        until the rule executes, so these can be hard to test.

        Please ensure that the script is valid Javascript and uses only ES3
        compatible features and library functions, as the Javascript Engine used
        by Gravity is quite primitive compared to Node or modern browsers.
      `)
    };
  }
}



function errorIfValidationRuleIsMutating(rule: Rule): RuleValidationError {
  const badActions = rule.actions.filter(a => BAD_ACTIONS_FOR_VALIDATION_RULES.includes(a.type));
  if (rule.ruleType === 'VALIDATION' && badActions.length > 0) {
    return {
      severity: 'CRITICAL',
      synopsis: 'Validation rule should not modify data!',
      detailedMessage: trimIndent(`
        Validation rules should only inspect values on the card, and should not
        have any side-effects, such as modifying data or triggering other
        automation rules to run.
      `)
    };
  }
}


function warnIfMakingCompulsoryRule(rule: Rule): RuleValidationError {
  if (rule.ruleType === 'COMPULSORY') {
    return {
      severity: 'WARNING',
      synopsis: 'Regular (a.k.a. Compulsory) rules are dangerous',
      detailedMessage: trimIndent(`
        Compulsory rules will run multiple times per card if the trigger
        conditions permit it, allowing for automation loops.

        Task rules are inherently safer, as they will only be run once per card.
        Please consider whether this rule could be implemented as a task rule
        instead.
      `)
    };
  }
}


function checkThatRuleHasActions(rule: Rule): RuleValidationError {
  if (rule.actions.length === 0 && rule.ruleType !== 'TASK') {
    return {
      severity: 'WARNING',
      synopsis: 'Rule has no actions',
      hazards: ['noop'],
      detailedMessage: trimIndent(`
        As this rule has no actions, it won't accomplish anything.
      `)
    };
  }
}


function warnIfCompulsoryRuleIsUnconditional(rule: Rule): RuleValidationError {
  if (rule.ruleType === 'COMPULSORY' && rule.automationConditions.length === 0) {
    return {
      severity: 'WARNING',
      hazards: ['heavy-load', 'loop'],
      synopsis: 'Rule does not have any trigger conditions.',
      detailedMessage: trimIndent(`
        As this rule doesn't have any trigger conditions, it will always be
        executed when a card is added or modified on this board.

        This may be intentional but rules without conditions have the potential
        to cause a lot of server load on a 'busy' board, or cause an infinite
        loop
      `)
    };
  }
}


function checkThatAtLeastOneConditionIsPersisted(rule: Rule): RuleValidationError {
  const persistActions = rule.actions.filter(a => a.type === 'persist');
  const propertyConditions = rule.automationConditions.filter(c => c.conditionType === 'PROPERTY');
  const hasOnlyPropertyConditions = rule.automationConditions.length > 0 && propertyConditions.length === rule.automationConditions.length;

  if (rule.ruleType === 'COMPULSORY' && hasOnlyPropertyConditions && persistActions.length > 0) {
    const persistedFields = new Set([].concat(...persistActions.map(a => a.config.parameters)));
    const conditionFields = propertyConditions.map(c => c.fieldName);

    if (!conditionFields.some(f => persistedFields.has(f))) {
      return {
        severity: 'CRITICAL',
        hazards: ['loop'],
        synopsis: 'Rule does not update any condition properties',
        detailedMessage: trimIndent(`
          The only trigger conditions for this rule check fields on the card,
          but none of these fields are saved in the rule actions. This will
          result in an infinite loop, as saving the card will cause the rules to
          be re-evaluated, and this rule will run again.

          Please ensure that executing the actions on this rule will result in
          the trigger conditions no longer being met.
        `)
      };
    }
  }
}
