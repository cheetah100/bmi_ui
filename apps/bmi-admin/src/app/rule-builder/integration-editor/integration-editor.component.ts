import { Component, forwardRef, OnDestroy, OnInit } from '@angular/core';
import { Observable, ReplaySubject, merge, defer, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { Integration, IntegrationsService, PluginMetadataService } from '@bmi/gravity-services';
import {
  Option,
  TypedFormGroup,
  TypedFormControl,
  BaseControlValueAccessor,
  provideAsControlValueAccessor,
  IdpValidators,
  TypedAbstractControl,
} from '@bmi/ui';

import { Omit, probablyValidGravityId, sanitizeGravityApiId, ensureIdUniqueIn } from '@bmi/utils';

import { connectNameToIdFieldHelper } from '../../utils/connect-name-to-id-field-helper';


function isValidId(id: string) {
  return id && id.match(/\w+/);
}


@Component({
  selector: 'bmi-integration-rule-editor',
  templateUrl: './integration-editor.component.html',
  styleUrls: ['./integration-editor.component.scss'],
  providers: [
    provideAsControlValueAccessor(forwardRef(() => IntegrationEditorComponent))
  ]
})
export class IntegrationEditorComponent extends BaseControlValueAccessor<Integration> implements OnInit, OnDestroy {
  readonly form = new TypedFormGroup<Integration>({
    id: new TypedFormControl('', {
      validators: [
        IdpValidators.required,
        IdpValidators.satisfies(
          probablyValidGravityId,
          'Must be a valid Gravity ID (letters and numbers, underscores, no spaces or other punctuation)'
        )
      ],
      asyncValidators: [
        IdpValidators.asyncMustNotContainValue(() => this.existingIntegrations, 'An integration with this ID already exists')
      ]
    }),
    isNew: new TypedFormControl(false),
    name: new TypedFormControl('', [
      IdpValidators.required,
      IdpValidators.mustBeNonBlank(),
    ]),
    connector: new TypedFormControl('', [IdpValidators.required, IdpValidators.mustBeNonBlank()]),
    config: new TypedFormControl({}),
    actions: new TypedFormControl([]),
  });

  private onFormUpdated = new ReplaySubject<Integration>(1);

  private subscription = new Subscription();

  readonly connectorOptions = defer(() => this.pluginMetadata.list('integration')).pipe(
    map(integrationPlugins => integrationPlugins.map(p => <Option>{value: p.id, text: p.name}))
  );

  readonly selectedConnector = merge(this.onFormUpdated, this.form.valueChanges).pipe(
    map(() => this.form.value.connector),
    distinctUntilChanged(),
  );

  readonly selectedConnectorPlugin = this.selectedConnector.pipe(
    switchMap(connectorType => this.pluginMetadata.get('integration', connectorType))
  );

  readonly existingIntegrations = defer(() => this.integrationService.list()).pipe(
    map(integrations => integrations.map(i => i.id))
  );

  constructor(
    private pluginMetadata: PluginMetadataService,
    private integrationService: IntegrationsService,
  ) {
    super();
  }

  ngOnInit() {
    this.form.valueChanges.subscribe(value => this.onChanged(this.form.getRawValue()));

    this.subscription.add(connectNameToIdFieldHelper(
      this.form.controls.name,
      this.form.controls.id,
      this.existingIntegrations
    ));
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.subscription.unsubscribe();
  }

  get idControl() {
    return this.form.controls.id;
  }

  updateComponentValue(integration: Integration) {
    this.form.patchValue(integration, {emitEvent: false});
    this.onFormUpdated.next(integration);

    if (integration.isNew) {
      this.idControl.enable();
    } else {
      this.idControl.disable();
    }
  }
}


