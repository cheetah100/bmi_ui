import { Component, Input, OnInit, forwardRef, OnChanges, Injector } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';
import { filter, switchMap, map, tap } from 'rxjs/operators';
import pick from 'lodash-es/pick';
import omit from 'lodash-es/omit';

import {
  Action,
  PluginMetadataService,
} from '@bmi/gravity-services';
import {
  TypedFormControl,
  TypedFormGroup,
  provideAsControlValueAccessor,
  BaseControlValueAccessor, ModalService
} from '@bmi/ui';
import { ObjectMap } from '@bmi/utils';


@Component({
  selector: 'bmi-action-editor',
  templateUrl: './action-editor.component.html',
  styleUrls: ['./action-editor.component.scss'],
  providers: [
    provideAsControlValueAccessor(forwardRef(() => ActionEditorComponent))
  ]
})
export class ActionEditorComponent
    extends BaseControlValueAccessor<Action>
    implements OnInit, OnChanges {

  @Input() actionType: string;
  @Input() boardId: string = null;
  selectedBoard: string;
  originalAction: Action;

  readonly form = new TypedFormGroup({
    name: new TypedFormControl<string>(),
    description: new TypedFormControl<string>(),
    actionConfig: new TypedFormControl<ObjectMap<any>>(null)
  });

  private actionTypeSubject = new BehaviorSubject<string>(undefined);
  readonly pluginMetadata = this.actionTypeSubject.pipe(
    switchMap(actionType => this.pluginService.get('action', actionType))
  );

  isEditingName = false;

  constructor(
    private pluginService: PluginMetadataService,
    private modalService: ModalService,
    private injector: Injector) {
    super();
  }

  ngOnInit() {
    this.form.valueChanges
      .pipe(filter(() => !this.isWriteValueInProgress))
      .subscribe(formValue => {
        this.onChanged({
          ...formValueToAction(formValue.actionConfig, this.originalAction),
          name: formValue.name,
          description: formValue.description,
        })
      });
  }

  ngOnChanges(changes) {
    if (changes.actionType) {
      this.actionTypeSubject.next(this.actionType);
    }
  }

  updateComponentValue(action: Action) {
    this.originalAction = action;
    this.actionTypeSubject.next(action ? action.type : undefined);

    this.form.reset({
      name: action ? action.name : null,
      description: action ? action.description : null,
      actionConfig: action ? action.config : {}
    }, {emitEvent: false});
  }

  get type() {
    return this.actionTypeSubject.value;
  }
}


function formValueToAction(
  formValue: ObjectMap<any>,
  originalAction: Action
): Action {
  return Object.assign(Action.clone(originalAction), {
    config: {
       ...formValue
    }
  })
}
