import { Component, Input, Output, EventEmitter } from '@angular/core';

import { RuleExecutionState } from '../rule-debugger-types';



@Component({
  selector: 'bmi-debugger-rule-state',
  template: `
    <h3>
      <ui-status-indicator [color]="statusColor"></ui-status-indicator>
      {{state.ruleId}}
    </h3>
    <small>{{state.explanation}}</small>

    <button
        *ngIf="isLatestState"
        type="button"
        class="btn btn--success btn--small"
        [disabled]="!state.canRuleExecute"
        (click)="executeRule.emit(state.ruleId)">
      Execute
    </button>
  `,
  styles: [`
    ui-status-indicator {
      display: inline-block;
      margin-right: 5px;
    }
  `]
})
export class RuleDebuggerStateComponent {
  @Input() state: RuleExecutionState;
  @Input() isLatestState = false;
  @Output() executeRule = new EventEmitter<string>();

  get statusColor() {
    return this.state.canRuleExecute ? 'green' : null;
  }
}
