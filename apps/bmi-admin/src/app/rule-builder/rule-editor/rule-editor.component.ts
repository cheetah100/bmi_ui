import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy, OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import {
  FormControl, FormGroup,
  Validators
} from '@angular/forms';
import {
  BoardConfig, Condition, Gravity,
  Rule,
  RulesService
} from '@bmi/gravity-services';
import {
  clearFormArray, ErrorModalService, getFormValue,
  IdpValidators,
  Option, TemplatedFormArray,
  TypedFormGroup
} from '@bmi/ui';
import {
  Observable,
  of as observableOf, Subscription
} from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  finalize,
  map, mapTo,
  mergeMap,
  take,
  tap
} from 'rxjs/operators';
import { connectNameToIdFieldHelper } from '../../utils/connect-name-to-id-field-helper';
import { RuleSaveModalComponent } from '../rule-save-modal/rule-save-modal.component';
import { checkRuleForErrors } from '../rule-validator';


function makeConditionFormGroup(condition: Condition) {
  return new FormGroup({
    // This is used to track the original ID of the condition to avoid churn
    // when saving
    key: new FormControl(condition.key || null),
    fieldName: new FormControl(condition.fieldName),
    operation: new FormControl(condition.operation),
    conditionType: new FormControl(condition.conditionType),
    value: new FormControl(condition.values)
  });
}

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gravity-rule-editor',
  templateUrl: './rule-editor.component.html',
  styleUrls: ['./rule-editor.component.scss']
})
export class RuleEditorComponent implements OnInit, OnChanges, OnDestroy {
  @Input() public rule: Rule;
  @Output() public ruleTypeChanged = new EventEmitter<string>();

  private boardConfig: BoardConfig;
  public boardId: string = null;

  private subscriptions: Subscription[] = [];

  // For assistance when saving, we keep the original rule that this was
  // build from as a way to try and preserve anything we might not know about.
  private baseRule: Rule = null;

  isLoading = false;
  isSaving = false;
  hasError = false;
  errorMessage = null;

  readonly form = new TypedFormGroup({
    id: new FormControl('', [
      Validators.required,
      IdpValidators.mustBeNonBlank()
    ]),
    name: new FormControl('', [
      Validators.required,
      IdpValidators.mustBeNonBlank()
    ]),
    description: new FormControl(),
    ruleType: new FormControl('COMPULSORY', [
      Validators.required,
      IdpValidators.mustBeNonBlank()
    ]),

    automationConditions: new TemplatedFormArray<Condition>(condition =>
      makeConditionFormGroup(condition)
    ),
    taskConditions: new TemplatedFormArray<Condition>(condition =>
      makeConditionFormGroup(condition)
    ),
    completionConditions: new TemplatedFormArray<Condition>(condition =>
      makeConditionFormGroup(condition)
    ),

    actions: new FormControl([]),

    schedule: new FormControl()
  });

  readonly ruleTypeOptions: Option[] = [
    { value: 'COMPULSORY', text: 'Regular' },
    { value: 'SCHEDULED', text: 'Scheduled' },
    { value: 'TASK', text: 'Task' },
    { value: 'VALIDATION', text: 'Validation' }
  ];

  @ViewChild('saveConfirmationModal', { static: true }) confirmSaveModal: RuleSaveModalComponent;

  get boardName() {
    if (!this.boardConfig) {
      return this.boardId;
    } else if (this.boardConfig.ui.singular) {
      return this.boardConfig.ui.singular;
    } else {
      return this.boardConfig.name;
    }
  }

  get isNewRule(): boolean {
    return !this.baseRule || !this.baseRule.id;
  }

  get ruleType(): string {
    return getFormValue(this.form, 'ruleType');
  }

  get ruleId(): string {
    return getFormValue(this.form, 'id');
  }

  get isTaskRule(): boolean {
    return this.ruleType === 'TASK';
  }

  get isScheduleRule(): boolean {
    return this.ruleType === 'SCHEDULED';
  }

  get ruleExplanationGraphicAssetFile() {
    if (this.ruleType) {
      return `assets/docs/diagrams/rules-diagram-${(this.ruleType).toLowerCase()}.png`;
    } else {
      return null;
    }
  }

  get automationConditions() {
    return this.form.get('automationConditions') as TemplatedFormArray<
      Condition
    >;
  }

  get taskConditions() {
    return this.form.get('taskConditions') as TemplatedFormArray<Condition>;
  }

  get completionConditions() {
    return this.form.get('completionConditions') as TemplatedFormArray<Condition>;
  }

  get isUnconditionalRule() {
    return (
      this.ruleType === 'COMPULSORY' && this.automationConditions.length === 0
    );
  }

  get canEditDangerousRules() {
    return true;
  }

  get isFormValid() {
    return this.form && !this.form.invalid;
  }

  get idControl() {
    return this.form.controls.id;
  }

  constructor(
    private gravity: Gravity,
    private rulesService: RulesService,
    private errorModal: ErrorModalService,
  ) {}


  ngOnInit() {
    this.subscriptions.push(
      this.form
        .get('ruleType')
        .valueChanges.pipe(distinctUntilChanged())
        .subscribe(ruleType => {
          if (this.isScheduleRule) {
            // Remove automation conditions
            clearFormArray(
              this.automationConditions
            )
            // this.baseRule.automationConditions = [];
            // this.form.patchValue(this.baseRule, {emitEvent: false})
          }
          this.ruleTypeChanged.emit(ruleType);

        })
    );

    this.subscriptions.push(
      connectNameToIdFieldHelper(this.form.controls.name, this.form.controls.id)
    );

    this.init(this.rule);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.rule) {
      this.init(this.rule);
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  init(rule: Rule) {
    if (!rule) {
      return;
    }

    this.boardId = rule.boardId;
    if (this.boardId && this.gravity.configService.boardExists(this.boardId)) {
      this.boardConfig = this.gravity.configService.getBoardConfig(
        this.boardId
      );
    } else {
      throw new Error(`Unknown board config: '${this.boardId}'`);
    }

    this.form.patchValue(rule, {emitEvent: false});
    this.isLoading = false;
    this.errorMessage = null;
    this.baseRule = rule;

    if (this.isNewRule) {
      this.idControl.enable();
    } else {
      this.idControl.disable();
    }

    this.form.markAsPristine();
  }

  save(): Observable<Rule> {
    return this.buildRule().pipe(
      mergeMap(rule => this.showSaveConfirmation(rule)),
      tap(() => (this.isSaving = true)),
      tap(rule => {
        console.log(rule);
      }),
      mergeMap(rule => this.rulesService.saveRule(rule).pipe(
        this.errorModal.catchError({retryable: true}),
        mapTo(rule)
      )),
      finalize(() => {
        this.isSaving = false;
        this.form.markAsPristine();
      })
    );
  }

  showSaveConfirmation(rule: Rule) {
    return checkRuleForErrors(rule).pipe(
      mergeMap(validationResults =>
        this.confirmSaveModal.show(
          rule,
          validationResults,
          this.canEditDangerousRules
        )
      ),
      take(1),
      filter(confirmation => confirmation === true),
      map(() => rule)
    );
  }

  /**
   * Builds a rule object based on the current values that are present on
   * the form.
   */
  buildRule(): Observable<Rule> {
    const formValue = this.form.getRawValue();

    const rule = this.baseRule.clone();
    if (this.isNewRule) {
      rule.id = formValue.id;
    }

    rule.name = formValue.name;
    rule.description = formValue.description;
    rule.ruleType = formValue.ruleType;

    rule.automationConditions = this.getConditions(this.automationConditions);
    rule.taskConditions = this.getConditions(this.taskConditions);
    rule.completionConditions = this.getConditions(this.completionConditions);

    rule.actions = formValue.actions;
    rule.schedule = formValue.schedule;

    return observableOf(rule);
  }

  getConditions(conditionsForm: TemplatedFormArray<Condition>): Condition[] {
    const value = conditionsForm.value as any[];
    return value.map(
      cond =>
        new Condition(
          cond['fieldName'],
          cond['value'],
          cond['operation'],
          cond['conditionType'],
          cond['key']
        )
    );
  }
}
