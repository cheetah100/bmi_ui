import dedent from 'ts-dedent';


export const RULE_EDITOR_HELP_OVERVIEW = dedent `
  # Rule Editor #

  Rules are a feature of Gravity which allow actions to be taken when certain
  conditions on a card are met. Common actions would be to update properties of
  a card, change the phase, or run a custom script.

  Rule conditions are checked when a card is *examined* by Gravity. This
  happens when a card is added, updated, moved etc.

  There are three types of rules supported by Gravity:

  **Regular** (a.k.a 'compulsory'): The trigger conditions for these rules are
  assessed every time the card is examined, and the actions will be executed every
  time the conditions are met.

  **Validation**: These are run when a card is added or updated. The actions for a
  validation rule can produce a \`result\` variable, which, if set will contain a
  validation error message.

  **Task**: Task rules will only run once per card. In addition to trigger
  conditions, they have *start conditions* and *completion conditions.*

  Rules can have multiple trigger conditions, which are joined with an AND
  operation; e.g. \`phase IS 'execution' AND development_status IS NOT
  'in-factory'\`. Some conditions support multiple values, and the condition will
  be met if any of these values match. For example \`phase IS ('assessment' OR
  'execution')\`.

  If there are no conditions, the check will always pass.
  `;


 const regularRuleHelp = assetPath => dedent `
  Regular rules are evaluated whenever a card is examined by Gravity, e.g. when
  the card is added, modified etc.

  The trigger condition is checked, and if they are met then the actions are
  executed in order.

  If there are no conditions, then the rule will always execute.

  <img src="${assetPath}/rule-flow-regular.svg" alt="Rule flow regular">

  Care must be taken to avoid creating automation loops when working with
  regular rules. In general, a rule should have trigger conditions which will be
  made false by the rule's actions, ensuring the rule won't run a second time.
`;


const taskRuleHelp = assetPath => dedent `
  Task rules will only run once per card. Cards track the state of the tasks
  that have run on them.

  Tasks have three states: *Inactive*, *Active*, and *Completed*, and
  correspondingly have three sets of conditions that control the progress
  thorugh these stages.

  <img src="${assetPath}/rule-flow-task.svg" alt="Rule flow task">

  ### Not Started ###

  A task isn't active on a card until Gravity examines the card, and the *Start
  Conditions* are met.

  ### Task Active ###

  One a task has started on a card, the *Trigger Conditions* will be checked,
  and once these are met, the Actions will be executed in sequence.

  After the task's actions have been performed, the *Completion Conditions* come
  into play. These determine when the task should be moved into the completed
  state.

  If no completion conditions are supplied, then the task will immediately
  complete.

  ### Completed ###

  Once a task is completed on a card it will not run again, but other rules can
  check if the task has been completed using a *Task Condition*. This is a way
  of orchestrating multiple business rules.

  --------------------------------------------------------------------------

  The *Start Conditions* and *Completion Conditions* are optional. If no
  conditions are specified, then the task will start immediately when the rule
  runs, or complete immediately after the actions have been performed.
`;


export function generateRuleHelp(ruleType: string, assetPath: string = 'assets/images'): string {
  const ruleFlowHelp = ruleType === 'TASK' ? taskRuleHelp(assetPath) : regularRuleHelp(assetPath)

  return dedent `
    ${RULE_EDITOR_HELP_OVERVIEW}

    ## Rule Flow ##

    ${ruleFlowHelp}
  `;
}
