import { Component, TemplateRef, ContentChild } from '@angular/core';

@Component({
  selector: 'bmi-help-form-section',
  template: `
    <section class="properties form-help-grid">
      <div class="form-help-grid__form">
        <ng-content></ng-content>
      </div>
      <div class="form-help-grid__help">
        <blockquote class="form-help-grid__help__panel blockquote--primary">
          <ng-container *ngTemplateOutlet="help"></ng-container>
        </blockquote>
      </div>
    </section>
  `,
  styles: [
    `
      .form-help-grid {
        display: grid;
        grid-gap: 40px;
        grid-template-columns: minmax(720px, 960px) minmax(360px, 1fr);
      }
      .form-help-grid__help__panel {
        max-width: 480px;
      }
      .form-help-grid__help__panel ::ng-deep {
        font-size: var(--gra-font-size-sm);
      }
    `
  ]
})
export class HelpFormSectionComponent {
  @ContentChild('help') help: TemplateRef<any>;
}
