import { Component, Input, OnInit, forwardRef } from '@angular/core';
import { filter, map, shareReplay } from 'rxjs/operators';
import sortBy from 'lodash-es/sortBy';
import {
  TypedFormGroup,
  TypedFormControl,
  TemplatedFormArray,
  BaseControlValueAccessor,
  provideAsControlValueAccessor,
  Option
} from '@bmi/ui';
import {
  Action,
  PluginMetadataService,
  PluginMetadata
} from '@bmi/gravity-services';

import { uniqueId } from '@bmi/utils';

@Component({
  selector: 'bmi-action-editor-list',
  templateUrl: './action-editor-list.component.html',
  providers: [
    provideAsControlValueAccessor(forwardRef(() => ActionEditorListComponent))
  ]
})
export class ActionEditorListComponent
  extends BaseControlValueAccessor<Action[]>
  implements OnInit {
  @Input() boardId: string = null;
  readonly formArray = new TemplatedFormArray<Action>(
    () => new TypedFormControl(null)
  );
  readonly actionTypes = this.pluginMetadataService.list('action').pipe(
    map(plugins => sortBy(plugins, p => p.name)),
    shareReplay(1)
  );

  constructor(private pluginMetadataService: PluginMetadataService) {
    super();
  }

  ngOnInit() {
    this.formArray.valueChanges
      .pipe(filter(() => !this.isWriteValueInProgress))
      .subscribe(value => this.onChanged(value));
  }

  updateComponentValue(actions: Action[]) {
    this.formArray.patchValue(actions || [], { emitEvent: false });
  }

  addAction(pluginType: PluginMetadata) {
    this.formArray.pushValue(
      Action.create(pluginType.id, uniqueId(pluginType.id, 4))
    );
  }
}
