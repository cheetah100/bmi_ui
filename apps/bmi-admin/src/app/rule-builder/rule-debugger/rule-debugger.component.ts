import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { combineLatest, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { CardCacheService, RulesService, Rule, CardData } from '@bmi/gravity-services';
import { watchParams } from '@bmi/utils';
import sortBy from 'lodash-es/sortBy';
import cloneDeep from 'lodash-es/cloneDeep';

import { CardRuleDebuggerState, RuleExecutionState, RuleDebuggerEvent } from '../rule-debugger-types';

import ms from 'ms';


@Component({
  selector: 'bmi-rule-debugger',
  template: `
    <div class="row">
      <div class="col-6">
        <ng-container *ngIf="currentState">
          <h1>Debugging {{currentState.boardId}}:{{currentState.cardId}}</h1>
          <small>Snapshot: {{currentState.snapshotTime | date:'HH:mm:ss.SSS (MMM d)'}}</small>

          <bmi-core-card *ngFor="let rule of currentState.rules">
            <bmi-debugger-rule-state
                [state]="rule"
                [isLatestState]="isLatestState"
                (executeRule)="executeRule(rule)">
            </bmi-debugger-rule-state>
          </bmi-core-card>

        </ng-container>
      </div>
      <div class="col-6">
        <h1>States</h1>
        <ui-list-group>
          <div uiListGroupItem [isActionable]="true" [isActive]="s === currentState" *ngFor="let s of allStates" (click)="selectState(s)">
            {{s.snapshotTime | date:'HH:mm:ss.SSS (MMM d)'}}
          </div>
        </ui-list-group>

        <ng-container *ngIf="currentState">
          <h1>Card at current state</h1>
          <gravity-config-viewer [config]="currentState.card" [viewJson]="true"></gravity-config-viewer>
        </ng-container>
      </div>
    </div>
  `,
  styles: [`
    bmi-core-card {
      display: block;
      margin-bottom: 10px;
    }

    bmi-core-card ::ng-deep .widget-card {
      height: unset;
    }

    ui-list-group {
      margin-bottom: 20px;
    }
  `]
})
export class RuleDebuggerComponent {
  @Input() currentState: CardRuleDebuggerState;
  @Input() allStates = [];

  get isLatestState() {
    return this.currentState === this.allStates[this.allStates.length - 1];
  }

  constructor(
    private activatedRoute: ActivatedRoute,
    private cardCacheService: CardCacheService,
    private ruleService: RulesService,
  ) {}

  ngOnInit() {
    watchParams(this.activatedRoute, 'boardId', 'cardId').pipe(
      switchMap(({boardId, cardId}) => combineLatest([
          of(boardId),
          this.ruleService.getAllRules(boardId),
          this.cardCacheService.get(boardId, cardId),
        ])
      ),
      map(([boardId, rules, card]) => this.fabricateDebugStates(boardId, rules, card))
    ).subscribe(states => {
      this.allStates = sortBy(states, s => s.snapshotTime);
      this.currentState = this.allStates[this.allStates.length - 1];
    })
  }

  executeRule(ruleState: RuleExecutionState) {
    alert(`This would run ${ruleState.ruleId}`);
  }

  selectState(state: CardRuleDebuggerState) {
    this.currentState = state;
  }

  fabricateDebugStates(boardId: string, rules: Rule[], card: CardData): CardRuleDebuggerState[] {
    const statesToGenerate = Math.ceil(Math.random() * 20) + 1;
    const states: CardRuleDebuggerState[] = [];

    let time = new Date().getTime() - ms('10 minutes');
    function advanceTime(millis: number) {
      time += millis;
      return time;
    }

    let currentCard = card;

    for (let i = 0; i < statesToGenerate; i++) {
      currentCard = modifyCard(currentCard, time);

      states.push({
        boardId,
        cardId: card.id,
        card: currentCard,
        snapshotTime: advanceTime(Math.random() * ms('30 seconds')),
        rules: rules.map(r => {
          const canExecute = Math.random() > 0.5;

          return <RuleExecutionState>{
            ruleId: r.id,
            explanation: canExecute ? 'Ready to Execute' : generateExcuse(),
            areConditionsMet: canExecute,
            canRuleExecute: canExecute,
          }
        })
      });
    }

    return states;
  }
}


function chooseFrom<T>(items: T[]): T {
  return items[Math.floor(Math.random() * items.length)];
}


function generateExcuse() {
  return chooseFrom([
    'Automation conditions not met',
    'Waiting on task XYZ',
    'Planets not in alignment',
    'Need additional monkeys'
  ]);
}


function modifyCard(card: CardData, time: number) {
  const cloned = cloneDeep(card);
  cloned.modified = time;
  for (let i = 0; i < 3; i++) {
    const fieldToModify = chooseFrom(Object.keys(cloned.fields));
    cloned.fields[fieldToModify] = chooseFrom([
      'Lorem ipsum dolor sit amet',
      'sed quia non numquam',
      Math.floor(Math.random() * 1000),
      Math.random() > 0.5,
    ]);
  }

  return cloned;
}
