import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { GravityModule, GravityUiModule } from '@bmi/gravity-services';
import { SharedUiModule } from '@bmi/ui';
import { BmiCoreModule } from '@bmi/core';

import { MonacoEditorModule } from 'ngx-monaco-editor';

import { DataViewerModule } from '../data-viewer/data-viewer.module';

import { RuleEditorComponent } from './rule-editor/rule-editor.component';
import { RuleSaveModalComponent } from './rule-save-modal/rule-save-modal.component';

import { ActionEditorComponent } from './action-editor/action-editor.component';
import { IntegrationEditorComponent } from './integration-editor/integration-editor.component';
import { ActionEditorListComponent } from './action-editor-list/action-editor-list.component';

import { RuleDebuggerStateComponent } from './rule-debugger-state/rule-debugger-state.component';
import { RuleDebuggerComponent } from './rule-debugger/rule-debugger.component';
import { HelpFormSectionComponent } from './rule-editor/help-form-section.component';
import { PluginFormComponent } from '../../../../../libs/core/src/lib/forms/plugin-form/plugin-form.component';

@NgModule({
  declarations: [
    RuleEditorComponent,
    ActionEditorComponent,
    RuleSaveModalComponent,
    IntegrationEditorComponent,
    ActionEditorListComponent,

    RuleDebuggerStateComponent,
    RuleDebuggerComponent,
    HelpFormSectionComponent
  ],

  exports: [
    RuleEditorComponent,
    RuleSaveModalComponent,
    IntegrationEditorComponent,
    PluginFormComponent
  ],

  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedUiModule,
    GravityModule,
    GravityUiModule,
    MonacoEditorModule,
    BmiCoreModule,
    DataViewerModule
  ]
})
export class RulesAdminModule {}
