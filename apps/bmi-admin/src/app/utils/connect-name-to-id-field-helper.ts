import { Observable, Subscription, of } from 'rxjs';
import {
  withLatestFrom,
  distinctUntilChanged,
  debounceTime,
  filter
} from 'rxjs/operators';
import { TypedAbstractControl } from '@bmi/ui';
import { sanitizeGravityApiId, ensureIdUniqueIn } from '@bmi/utils';

const userHasNotTouchedTheField = control =>
  control.untouched || !control.dirty || !control.value;

const nameChangeObservable = (nameControl, control) =>
  nameControl.valueChanges.pipe(
    distinctUntilChanged(),
    filter(name => control.enabled && userHasNotTouchedTheField(control)),
    debounceTime(100)
  );

/**
 * A form helper function to bind name to generate a sensible ID.
 *
 * This will listen for changes to the name control, and will then update the ID
 * field accordingly with a generated id, provided the id control is enabled,
 * and the user hasn't touched/dirtied the control or the ID is blank.
 *
 * Example:
 *
 *     connectNameToIdFieldHelper(
 *       this.form.controls.name,
 *       this.form.controls.id
 *     );
 *
 * The function returns a Subscription, as there is a debounce timer involved it
 * pays to attach this to component cleanup (although it's probably no big deal)
 *
 * You can optionally provide an Observable array of IDs that have already been
 * taken. It will not suggest one of these.
 */

export function connectNameToIdFieldHelper(
  nameControl: TypedAbstractControl<string>,
  idControl: TypedAbstractControl<string>,
  existingIds: Observable<string[]> = of([])
): Subscription {
  return nameChangeObservable(nameControl, idControl)
    .pipe(withLatestFrom(existingIds))
    .subscribe(([name, existingIdStrings]) => {
      const generatedId = ensureIdUniqueIn(
        sanitizeGravityApiId(name || ''),
        existingIdStrings
      );
      idControl.setValue(generatedId);
      idControl.markAsUntouched();
    });
}

/**
 * Same deal, but accepts a function for converting to any value for the
 * watching control
 * Input function should handle blacklisting existing values if required
 */
export function connectNameToControlFieldHelper<T>(
  nameControl: TypedAbstractControl<string>,
  control: TypedAbstractControl<T>,
  operation: (id: string) => T
): Subscription {
  return nameChangeObservable(nameControl, control).subscribe(
    (name: string) => {
      control.setValue(operation(name));
      control.markAsUntouched();
    }
  );
}
