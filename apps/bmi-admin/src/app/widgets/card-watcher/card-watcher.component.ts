import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription, timer, of, Subject, BehaviorSubject, merge } from 'rxjs';
import { catchError, filter, map, switchMap, tap } from 'rxjs/operators';
import { WidgetContext, WidgetConfig } from '@bmi/core';
import { BoardService, GravityCardAlertService, CardData, GravityApiCardEvent, CardDetails, CardDetailsService } from '@bmi/gravity-services';
import { combineLatestObject, distinctUntilNotEqual } from '@bmi/utils';
import { CardWatcherConfigFields } from './card-watcher-config';
import sortBy from 'lodash-es/sortBy';


@Component({
  templateUrl: './card-watcher.component.html',
  styleUrls: ['./card-watcher.component.scss']
})
export class CardWatcherComponent {
  constructor(
    private context: WidgetContext<WidgetConfig<CardWatcherConfigFields>>,
    private detailsService: CardDetailsService,
    private alertService: GravityCardAlertService,
  ) {}

  private subscription = new Subscription();

  selectedCard = this.context.widgetConfig.pipe(
    map(config => ({boardId: config.fields.board, cardId: config.fields.card})),
  );

  boardId: string;
  cardId: string;

  error: Error;

  details = new BehaviorSubject<CardDetails>(undefined);

  card = this.details.pipe(
    map(details => details && details.card)
  );

  cardState = this.card.pipe(
    map(card => (card ? {
      phase: card.phase,
      lock: card.lock,
      creator: card.creator,
      modifiedby: card.modifiedby,
    } : undefined))
  );

  isLoading = this.selectedCard.pipe(
    switchMap(({boardId, cardId}) => this.detailsService.isLoading(boardId, cardId))
  );

  get currentDetails() {
    return this.details.value;
  }

  ngOnInit() {
    this.subscription.add(this.selectedCard.pipe(
      tap(({boardId, cardId}) => {
        this.boardId = boardId;
        this.cardId = cardId;
      }),
      switchMap(({boardId, cardId}) => (this.detailsService.get(boardId, cardId)))
      // TODO: error handling here is still not good enough. Initially I had
      // this switchmap return a CardDetails | Error (by using a catchError to
      // make an error a regular value.) This worked okay, BUT it still cancels
      // the refresh, so this kind of behaviour is actually something that the
      // CacheEntry and ObservableMap need to support directly -- e.g. output
      // events rather than plain values. This is the same issue we have with
      // data sources (literally the same issue...) so a similar solution is
      // needed here. APIs need to make regular errors (like the card does not
      // exist) a regular, not exceptional occurrence
    ).subscribe(details_or_error => {
      if (details_or_error instanceof Error) {
        this.details.next(undefined);
        this.error = details_or_error;
      } else {
        this.details.next(details_or_error);
        this.error = null;
      }
    }));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  dismissAlerts(alerts: GravityApiCardEvent[]) {
    this.alertService.dismissCardAlerts(this.boardId, alerts.map(a => a.uuid)).subscribe(() => {
      this.triggerRefresh();
    });
  }

  private triggerRefresh() {
    this.detailsService.refresh(this.boardId, this.cardId);
  }
}

