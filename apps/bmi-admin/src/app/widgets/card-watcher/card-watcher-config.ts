export interface CardWatcherConfigFields {
  board: string;
  card: string;
}
