import { Component } from '@angular/core';
import { WidgetConfig, WidgetContext, OptionlistService } from '@bmi/core';
import { TypedFormGroup, TypedFormControl } from '@bmi/ui';
import { CardWatcherConfigFields } from './card-watcher-config';
import { debugObservableEvents } from '@bmi/utils';

@Component({
  template: `

    <ng-container [formGroup]="form">
      <ui-form-field label="Board">
        <ui-select formControlName="board" [optionGroups]="boards | async"></ui-select>
      </ui-form-field>

      <ui-form-field label="Card ID">
        <input type="text" formControlName="card">
      </ui-form-field>
    </ng-container>
  `
})
export class CardWatcherEditorComponent {
  constructor(
    private context: WidgetContext<WidgetConfig<CardWatcherConfigFields>>,
    private optionlistService: OptionlistService
  ){}

  form = new TypedFormGroup<CardWatcherConfigFields>({
    board: new TypedFormControl(),
    card: new TypedFormControl()
  });

  boards = this.optionlistService.getOptionGroups('gravity-system-BOARD');

  ngOnInit() {
    this.context.widgetConfig.subscribe(config =>
      this.form.patchValue(config.fields || {}, {emitEvent: false}));

    this.form.valueChanges.subscribe(fields => this.context.updateWidgetConfig(c => c.fields = fields));
  }
}
