import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { defineWidget, defineEditor, BmiCoreModule, BmiCoreEditorModule } from '@bmi/core';
import { SharedUiModule } from '@bmi/ui';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AdminComponentsModule } from '../components/admin-component.module';
import { ResourceSelectorWidgetComponent } from './resource-selector-widget/resource-selector-widget.component';
import { CardWatcherComponent } from './card-watcher/card-watcher.component';
import { CardWatcherEditorComponent } from './card-watcher/card-watcher-editor.component';

@NgModule({
  imports: [
    CommonModule,
    SharedUiModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    BmiCoreModule,
    BmiCoreEditorModule,
    AdminComponentsModule,
  ],

  declarations: [
    ResourceSelectorWidgetComponent,
    CardWatcherComponent,
    CardWatcherEditorComponent,
  ],

  entryComponents: [
    ResourceSelectorWidgetComponent,
    CardWatcherComponent,
    CardWatcherEditorComponent,
  ],

  providers: [
    defineWidget({ widgetType: 'resource-selector', component: ResourceSelectorWidgetComponent }),

    defineWidget({widgetType: 'admin:card-watcher', component: CardWatcherComponent}),
    defineEditor('admin:card-watcher', CardWatcherEditorComponent, {
      name: 'Card Watcher',
      type: 'admin',
      description: 'Provides a useful view of a card for debugging',
      groups: ['Debug'],
      widgetConfig: {
        widgetType: 'admin:card-watcher',
        fields: {board: null, card: null}
      }
    })

  ]
})
export class BmiAdminWidgetsModule { }
