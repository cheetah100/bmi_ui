import { Component, HostListener, Injector, OnDestroy, OnInit, Optional } from '@angular/core';
import { distinctUntilChanged, map, mapTo, shareReplay, switchMap, tap } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';

import { FormContext, FormField } from '@bmi/core';
import { WidgetConfig } from '@bmi/core';
import { WidgetContext } from '@bmi/core';
import { OptionlistService } from '@bmi/core';
import { ConfirmationModalService, ErrorModalService, ModalService, Option, OptionGroup } from '@bmi/ui';
import { PageDataContext } from '@bmi/core';
import { BoardResource, GravityResourceService } from '@bmi/gravity-services';
import { ResourceModalComponent } from '../../components/resource-modal/resource-modal.component';
import dedent from 'ts-dedent';
import { ResourceHandle } from '@bmi/gravity-services';

export interface ResourceSelectWidgetFields {
  fieldId: string;
}

@Component({
  templateUrl: './resource-selector-widget.component.html',
  styleUrls: ['./resource-selector-widget.component.scss']
})
export class ResourceSelectorWidgetComponent implements OnInit, OnDestroy {
  fieldId$ = this.widgetContext.widgetConfig.pipe(
    map(config => config.fields.fieldId),
    distinctUntilChanged()
  );

  private boardIdSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  field$ = this.fieldId$.pipe(
    switchMap(fieldId =>
      this.formContext
        ? this.formContext.get(fieldId)
        : of(<FormField>undefined)
    )
  );

  private fieldValue: Subscription = null;

  boardOptions = this.optionlistService.getAllBoards().pipe(
    map(boards => boards.map(b => <Option>{ value: b.id, text: b.name })),
    shareReplay(1),
  );

  boardControl = new FormControl(null);

  control$ = this.field$.pipe(
    map(field => (field ? field.control : new FormControl()))
  );

  label$ = this.field$.pipe(map(field => (field && field.label) || 'No label'));

  optionGroups$: Observable<OptionGroup[]>;

  // Selected resource id
  resourceId = null;

  showBoardSelector = false;

  private subscription: Subscription = new Subscription();
  private boardId: string = null;

  constructor(
    private widgetContext: WidgetContext<WidgetConfig<ResourceSelectWidgetFields>>,
    private confirmationModalService: ConfirmationModalService,
    private optionlistService: OptionlistService,
    private modalService: ModalService,
    private errorModal: ErrorModalService,
    private injector: Injector,
    private resourceService: GravityResourceService,
    @Optional() private formContext: FormContext,
    @Optional() private binder: PageDataContext,
  ) {
  }

  ngOnInit() {
    if (this.formContext) {
      this.subscription.add(
        this.fieldId$.pipe(
          switchMap(fieldId => this.formContext.getFieldValue(fieldId)),
        ).subscribe(resourceId => {
          this.resourceId = resourceId;
        })
      );
      this.subscription.add(
        this.formContext.value.subscribe(value => {
          this.boardIdSubject.next(value.board)
        })
      )
    }

    this.subscription.add(
      this.field$.subscribe(
        field => {
          if (field.options?.useBoardFromField) {
            // only setup subscription once
            if (this.fieldValue === null) {
              this.fieldValue = this.formContext.getFieldValue(field.options.useBoardFromField).subscribe(value => {
                this.boardIdSubject.next(value)
              })
              this.subscription.add(this.fieldValue);
            }
            this.showBoardSelector = false;
          } else {
            this.boardIdSubject.next(this.boardId)
            this.showBoardSelector = this.boardId === null;
          }

          this.getGroups();
        }
      )
    );
    this.subscription.add(
      this.boardControl.valueChanges.subscribe(boardId => {
        this.boardIdSubject.next(boardId)

        if (this.showBoardSelector) {
          this.boardControl.setValue(boardId);
        }
      })
    )
    this.subscription.add(
      this.boardIdSubject.asObservable().subscribe(boardId => {
        // Check if the select board has changed and it is not the first time
        if (this.boardId !== null && this.boardId !== boardId) {
          this.formContext.setFieldValue('resource', null);
        }
        this.boardId = boardId;

        this.getGroups();
      })
    )
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  createResource() {
    const boardResource: BoardResource = {
      id: '',
      isNew: true,
      name: null,
      resource: '',
      type: null,
      boardId: this.boardId
    };
    this.openResourceModal(boardResource).subscribe(result => {
      console.log('saveResources => result:', result);
      // Set the id so that the control displays the created resource
      this.subscription.add(
        this.control$.subscribe(fieldControl => {
          fieldControl.patchValue(result.data.id)
        })
      )
    });
  }

  editResource() {
    const boardResource: BoardResource = {
      id: this.resourceId,
      isNew: false,
      name: null,
      resource: '',
      type: 'javascript',
      boardId: this.boardId
    };
    this.openResourceModal(boardResource).subscribe(result => {
      return {}
    });
  }

  deleteResource() {
    // Show are you sure Modal
    return this.confirmationModalService.show({
      content: dedent`
          ## Confirm deletion of ${this.resourceId} resource ##

          Are you sure you want to delete this *${this.resourceId}* resource?
        `,
      buttonYesText: 'Delete Resource',
      // emitCancelEvent: false,
    }).pipe(
      switchMap(result => {
        const handle:ResourceHandle = {
          id: this.resourceId,
          boardId: this.boardId
        }
        return this.resourceService.delete(handle).pipe(
          this.errorModal.catchError({retryable: true}),
          tap(result => {
            this.getGroups();
            this.formContext.setFieldValue('resource', null);
            return result;
          }),
          mapTo(result)
        )
      })
    ).subscribe();
  }

  private openResourceModal(boardResource: BoardResource) {
    return this.modalService
      .open({
        title: 'Resource Editor',
        content: ResourceModalComponent,
        injector: this.injector,
        size: 'large',
        data: boardResource
      })

  }

  private getGroups() {
    if (!this.boardId) {
      this.optionGroups$ = of(null);
    } else if (this.formContext) {
      this.optionlistService.getOptionGroups('gravity-system-RESOURCE', this.boardId)
        .subscribe(groups => {
          this.optionGroups$ = of(groups)
        })
    }
  }
}
