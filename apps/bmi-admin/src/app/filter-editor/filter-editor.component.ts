import { Component, OnInit, OnChanges, Input, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
import { Filter, FilterOperations } from './filter.types';
import { PathPickerFilters } from '@bmi/gravity-services';

@Component({
  selector: 'lib-filter-editor',
  templateUrl: './filter-editor.component.html',
  styleUrls: ['./filter-editor.component.scss']
})
export class FilterEditorComponent implements OnInit, OnChanges {

  @Input() public filter: Filter;
  @Input() public boardId: string;
  @Output() public filterSaveEvent = new EventEmitter<Filter>();
  @Output() public filterDeleteEvent = new EventEmitter<Filter>();

  filterForm: FormGroup;
  filterOperations = FilterOperations;

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit() {
  }

  ngOnChanges() {
    this.loadForm();
  }

  get conditions() {
    return this.filterForm.get('conditions') as FormArray;
  }

  get _boardId() {
    return this.boardId || this.filterForm.get('boardId');
  }

  loadForm() {
      this.filterForm = this.fb.group({
        boardId: new FormControl(this._boardId, Validators.required),
        conditions: this.populateConditionArray(),
        id: new FormControl(this.filter ? this.filter.id : ''),
        name: new FormControl((this.filter ? this.filter.name : ''), Validators.required),
      });
  }

  populateConditionArray() {
    const formArray = this.fb.array([]);
    if (this.filter && this.filter.conditions) {
      Object.keys(this.filter.conditions).forEach(id => {
        const condition = this.filter.conditions[id];
        formArray.push(this.generateFilterConditionFormGroup(condition.fieldName, condition.operation, condition.value));
      });
    }
    return formArray;
  }

  addCondition() {
    this.conditions.push(this.generateFilterConditionFormGroup('', '', ''));
  }

  generateFilterConditionFormGroup(fieldName: string, operation: string, value: string) {
    return this.fb.group({
      fieldName: this.fb.control(fieldName, Validators.required),
      operation: this.fb.control(operation, Validators.required),
      value: this.fb.control(value, Validators.required)
    });
  }

  ppFilter() {
    return PathPickerFilters.pathLength(2);
  }

  reset() {
    this.filterForm.setControl('conditions', this.fb.array([]));
    this.filterForm.reset();
  }

  reload() {
    this.loadForm();
  }

  save() {
    this.filterSaveEvent.emit(this.transform());
  }

  /**
   * Transforms the filterForm.value into the accepted
   * type "Filter" format supported by Gravity.
   */
  transform() {
    const transformedConfig = Object.assign({}, this.filterForm.value);
    const conditions = transformedConfig.conditions || [];
    const conditionObj = {};
    conditions.forEach((condition, index) => {
      conditionObj[index] = condition;
    });
    transformedConfig.conditions = conditionObj;
    return transformedConfig;
  }

  delete() {
    this.filterDeleteEvent.emit(this.filterForm.value.id);
  }

}
