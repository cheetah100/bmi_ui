import { OPERATIONS } from '@bmi/gravity-services';

export interface Filter {
  access: null;
  boardId: string;
  conditions: {
    [key: string]: FilterCondition
  };
  excludedPhases: any;
  expression: any;
  id: string;
  metadata: any;
  name: string;
  owner: any;
  permissions: any;
  phase: any;
}

export interface FilterCondition {
  conditionType: any;
  fieldName: string;
  operation: string;
  path: any;
  value: string;
}

export const FilterOperations = [{
  text: 'Contains',
  value: 'CONTAINS'
}, {
  text: 'Not Contains',
  value: 'NOTCONTAINS'
}, {
  text: 'Equals To',
  value: 'EQUALTO'
}, {
  text: 'Not Equals To',
  value: 'NOTEQUALTO'
}, {
  text: 'Greater Than',
  value: 'GREATERTHAN'
}, {
  text: 'Less Than',
  value: 'LESSTHAN'
}, {
  text: 'Greater Than Equal To',
  value: 'GREATERTHANEQUALTO'
}, {
  text: 'Less Than Equal To',
  value: 'LESSTHANEQUALTO'
}, {
  text: 'Is Null',
  value: 'ISNULL'
}, {
  text: 'Not Null',
  value: 'NOTNULL'
}, {
  text: 'Before',
  value: 'BEFORE'
}, {
  text: 'After',
  value: 'AFTER'
}, ];
