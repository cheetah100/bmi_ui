import dedent from 'ts-dedent';

export const FILTER_EDITOR_HELP = dedent`
  # Filter Editor #

  The filter editor is used to view, create, update and delete board level filters. <br/>
  These filters are then applied on various board level APIs to filter down the responses based on the
  conditions that are configured. <br/>
  Rules and Visualizations like widgets, tables, roadmaps etc. often use these filters to show a more precise response.

  ### Conditions ###
  Every condition that is added gets **AND**ded with the previous condition/s

  **Field Name** <br/>
  Is a valid field that exists on the template of the filter board or its reference field's board <br/>
  TODO: The path picker should filter to only whitefields and references of the first level and whitefields of the second level

  **Operation** <br/>
  Denotes the operation to be applied between the 1. field selected and its 3. value <br/>

  **Value** <br/>
  Based on the Operations selected the values should be entered in the following format <br/>

    i.  Equal To / Not Equal To        : Pipe Delimited String <br/>
    ii. Contains / Not Contains        : Pipe Delimited String <br/>
    iii.Greater and Less than equal to : Number <br/>
    iv. Is Null / Not Null             : Value field will be disabled <br/>
    v.  Before / After                 : Date (MM/DD/YYYY) <br/>

  To Be Supported: Preview Filter on Data Table, Dropdown to select value
`;
