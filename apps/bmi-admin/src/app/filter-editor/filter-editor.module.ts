import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FilterEditorComponent } from './filter-editor.component';
import { SharedUiModule } from '@bmi/ui';
import { GravityUiModule } from '@bmi/gravity-services';
import { BmiCoreModule } from '@bmi/core';

@NgModule({
  declarations: [FilterEditorComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedUiModule,
    GravityUiModule,
    BmiCoreModule
  ],
  exports: [FilterEditorComponent]
})
export class FilterEditorModule {}
