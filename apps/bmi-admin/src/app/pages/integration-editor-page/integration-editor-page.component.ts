import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, BehaviorSubject, Subscription, of } from 'rxjs';
import { switchMap, shareReplay, take, tap } from 'rxjs/operators';
import { IntegrationsService, Integration } from '@bmi/gravity-services';
import { TypedFormControl, ErrorModalService } from '@bmi/ui';
import { watchParams, escapeMarkdown } from '@bmi/utils';
import dedent from 'ts-dedent';
import { SaveConfirmationModalService, jsonifyForDiff } from '../../components/save-confirmation-modal/save-confirmation-modal.service';

import { AdminAppNavService } from '../../admin-app-nav.service';
import { CanCheckUnsavedChanges } from '../../has-unsaved-changes.guard';


@Component({
  templateUrl: './integration-editor-page.component.html',
})
  export class IntegrationEditorPageComponent implements OnInit, OnDestroy, CanCheckUnsavedChanges {
  integrationObservable = new BehaviorSubject<Observable<Integration>>(undefined);

  originalIntegration: Integration = undefined;
  formControl = new TypedFormControl<Integration>(undefined);

  hasUnsavedChanges = false;
  hasUnsavedMessage = null;

  unSavedMessages = {
    resource: `
          ## Discard Changes?

          No resource has been currently selected, Please select one!!
      `,
  }

  private subscription = new Subscription();

  constructor(
    private activatedRoute: ActivatedRoute,
    private integrationsService: IntegrationsService,
    private errorModal: ErrorModalService,
    private saveConfirmationService: SaveConfirmationModalService,
    private appNav: AdminAppNavService
  ) {}

  ngOnInit() {
    watchParams(this.activatedRoute, 'integrationId').subscribe(({integrationId}) => {
      this.integrationObservable.next(this.getOrCreateIntegration(integrationId));
    });

    this.subscription.add(this.integrationObservable.pipe(
      switchMap(obs => obs || of(<Integration>undefined))
    ).subscribe(integrationToEdit => {
      this.formControl.setValue(integrationToEdit, {emitEvent: false});
      this.originalIntegration = integrationToEdit;
    }));

    this.subscription.add(
      this.formControl.valueChanges.subscribe(value => {
        console.log(`value`, value);
        console.log(`integration to edit: `, this.originalIntegration);
        if ( value.config.board !== '' && value.config.resource === null ) {
          this.hasUnsavedChanges = true;
          this.hasUnsavedMessage = this.unSavedMessages.resource;
        } else {
          this.hasUnsavedChanges = true;
          this.hasUnsavedMessage = null;
        }

      })
    )
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getOrCreateIntegration(integrationId: string | null): Observable<Integration> {
    if (integrationId) {
      return this.integrationsService.get(integrationId, {invalidateNow: true}).pipe(shareReplay(1), take(1));
    } else {
      return of(<Integration>{
        id: '',
        isNew: true,
        connector: null,
        config: {},
        actions: []
      });
    }
  }

  save() {
    const integration = this.formControl.value;
    const verb = integration.isNew ? 'Create' : 'Save';
    const integrationJson = Integration.toJSON(this.formControl.value);
    const originalJson = this.originalIntegration ? Integration.toJSON(this.originalIntegration) : {};

    this.subscription.add(this.saveConfirmationService.show({
      title: `${verb} Integration?`,
      body: `Are you sure you want to save the integration ${integration.id}?`,
      showDiff: !integration.isNew,
      original: jsonifyForDiff(originalJson),
      modified: jsonifyForDiff(integrationJson),
    }).pipe(
      switchMap(() => this.integrationsService.save(integration).pipe(
        tap(result => this.hasUnsavedChanges = false),
        this.errorModal.catchError({retryable: true}))
      )
    ).subscribe(() => this.appNav.integrationHome.navigate()));
  }

  cancel() {
    this.appNav.integrationHome.navigate();
  }
}
