import { Component, OnDestroy, OnInit } from '@angular/core';
import { BehaviorSubject, defer, iif, Observable, of, Subscription } from 'rxjs';
import { Integration, Transform } from '@bmi/gravity-services';
import { ConfirmationModalService, ErrorModalService, TypedFormControl } from '@bmi/ui';
import { watchParams } from '@bmi/utils';
import { map, shareReplay, switchMap, take, tap } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { TransformService } from '../../transform-editor/transform.service';
import { AdminAppNavService } from '../../admin-app-nav.service';
import {
  jsonifyForDiff,
  SaveConfirmationModalService
} from '../../components/save-confirmation-modal/save-confirmation-modal.service';
import { cloneDeep } from 'lodash-es';
import dedent from 'ts-dedent';

@Component({
  templateUrl: './transform-editor-page.component.html',
})
export class TransformEditorPageComponent implements OnInit, OnDestroy {
  transformObservable = new BehaviorSubject<Observable<Transform>>(undefined)

  originalTransform: Transform = undefined;
  formControl = new TypedFormControl<Transform>(undefined);
  private validSubject:BehaviorSubject<boolean> = new BehaviorSubject(false);

  get invalidForm() {
    return this.validSubject.asObservable().pipe(
      map(validStatus => !validStatus),
      tap(invalidStatus => console.log('invalidForm => invalidStatus: ', invalidStatus))
    )
  }

  private subscription = new Subscription();

  constructor(
    private activatedRoute: ActivatedRoute,
    private transformService: TransformService,
    private appNav: AdminAppNavService,
    private saveConfirmationService: SaveConfirmationModalService,
    private errorModal: ErrorModalService,
    private confirmationModalService: ConfirmationModalService
  ) {
  }

  ngOnInit() {
    watchParams(this.activatedRoute, 'transformId').subscribe(
      ({transformId}) => {
      this.transformObservable.next(this.getOrCreateTransform(transformId));
    });

    this.subscription.add(this.transformObservable.pipe(
      tap(obs => console.log(' => obs: ', obs)),
      switchMap(obs => obs || of(<Transform>undefined))
    ).subscribe(transformToEdit => {
      this.formControl.setValue(transformToEdit, {emitEvent: false});
      this.originalTransform = transformToEdit;
    }));

    this.formControl.statusChanges.subscribe(x => console.log('statusChange => x: ', x));
    this.formControl.valueChanges.subscribe(changes => {
      this.validate(changes);
    })
  }

  getOrCreateTransform(transformId: string | null): Observable<Transform> {
    if (transformId) {
      return this.transformService.get(transformId).pipe(
        shareReplay(1),
        take(1),
        switchMap(tr => of(Transform.fromJson(tr))),
        tap(transform => this.validate(transform)),
      )
    } else {
      return of(Transform.create())
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  cancel() {
    this.appNav.transformHome.navigate();
  }

  delete() {
    const transform = this.formControl.value;
    this.confirmationModalService.show({
      content: dedent`
        ## Delete Transform?

        Are you sure you want to delete the transform '${transform.name}'?
      `,
      buttonYesText: 'Delete transform',
    }).subscribe(() => this.deleteTransform());
  }

  deleteTransform() {
    const transform = this.formControl.value;
    this.transformService.delete(transform.id).subscribe(response => {
      // navigate
      this.cancel();
    }, error => {
      console.error(`Error in deleting Transform`);
    });
  }

  save() {
    console.log('save => about to save: ', 'about to save');
    const transform = this.formControl.value;
    const transformJson = Transform.toJson(transform);
    const verb = transform.isNew ? 'Create' : 'Save';
    const integrationId = transform.id;
    const integrationJson = Transform.toJson(this.formControl.value);
    const originalJson = this.originalTransform ? Transform.toJson(this.originalTransform) : {};

    this.subscription.add(this.saveConfirmationService.show({
      title: `${verb} Integration?`,
      body: `Are you sure you want to save the integration ${transform.id}?`,
      showDiff: !transform.isNew,
      original: jsonifyForDiff(originalJson),
      modified: jsonifyForDiff(integrationJson),
    }).pipe(
      switchMap( () =>
        iif(
        () => transform.isNew,
          defer(() => this.transformService.create(transformJson)),
          defer(() => this.transformService.update(integrationId, transformJson))
        ).pipe(
          this.errorModal.catchError({retryable: true})
        )
      )
    ).subscribe(() => this.appNav.transformHome.navigate()));
  }

  private validate(formData):void {
    // The transform should have
    // Name + id
    // At least 1 transformer
    let valid = !!formData.name || false;
    if (valid) {
      valid = formData.transforms.length > 0;
    }
    this.validSubject.next(valid);
  }

}

