import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Subscription, of, combineLatest } from 'rxjs';
import { distinctUntilChanged, map, switchMap, tap, shareReplay } from 'rxjs/operators';

import { GravityApiApplication, GravityConfigService } from '@bmi/gravity-services';
import { PageInfo, PageConfig, PageRepositoryService, WidgetTemplateService, ModuleService } from '@bmi/core';
import { ConfirmationModalService, ErrorModalService } from '@bmi/ui';
import { combineLatestObject, watchParams, escapeMarkdown } from '@bmi/utils';
import { AdminAppNavService } from '../../admin-app-nav.service';
import { ModuleInfoEditorService } from '../module-info-editor/module-info-editor.service';
import dedent from 'ts-dedent';
import sortBy from 'lodash-es/sortBy';

@Component({
  templateUrl: './module-hub-page.component.html',
  styleUrls: ['../transform-home/object-home.scss', './module-hub-page.component.scss'],
})
export class ModuleHubPageComponent implements OnInit, OnDestroy {
  search = '';


  private subscription = new Subscription();
  currentModuleId: string = undefined;

  readonly moduleId = watchParams(this.activatedRoute, 'moduleId').pipe(
    map(({moduleId}) => moduleId)
  );

  readonly module = this.moduleId.pipe(
    switchMap(moduleId => this.moduleService.getModule(moduleId)),
    shareReplay(1),
  );

  readonly moduleConfig = this.module.pipe(switchMap(m => m.config));
  readonly pageList = this.module.pipe(
    switchMap(m => m.pageList),
    map(pages => sortBy(pages, [p => p.title]))
  );
  readonly extraSettings = this.module.pipe(switchMap(m => m.extraSettings));

  readonly favoriteBoards = combineLatest([
    this.extraSettings.pipe(map(s => new Set(s.favoriteBoards || []))),
    this.configService.watchAllConfigs()
  ]).pipe(
    map(([favoriteBoardIds, allBoardConfigs]) => allBoardConfigs.filter(b => favoriteBoardIds.has(b.id))),
    map(favoriteBoards => sortBy(favoriteBoards, [b => b.name])),
    shareReplay(1)
  );

  constructor(
    private moduleService: ModuleService,
    private pageRepository: PageRepositoryService,
    private widgetTemplateService: WidgetTemplateService,
    private activatedRoute: ActivatedRoute,
    private appNav: AdminAppNavService,
    private confirmationModal: ConfirmationModalService,
    private errorModal: ErrorModalService,
    private configService: GravityConfigService,
  ) { }

  ngOnInit() {
    this.subscription.add(this.moduleId.subscribe(moduleId => {
      this.currentModuleId = moduleId;
      this.moduleService.refresh(moduleId);
    }));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }


  updateSearch(searchTerm: string) {
    this.search = searchTerm.toLocaleLowerCase().trim();
  }

  createPage() {
    const newPage: PageConfig = {
      id: null,
      applicationId: this.currentModuleId,
      permissions: null,
      type: 'page-v2',
      name: 'Untitled Page',
      ui: {
        rootWidget: this.widgetTemplateService.createTemplate('bmi-page-layout-template')
      }
    };

    this.pageRepository.createPage(this.currentModuleId, newPage).subscribe(savedPage => {
      this.appNav.editExistingPage(this.currentModuleId, savedPage.id).navigate();
    });
  }

  confirmDeletePage(page: PageInfo): void {
    this.confirmationModal.show({
      content: dedent`
          ## Delete page: ${page.title}?

          This action cannot be undone.
        `,
      buttonYesText: 'Delete page',
      buttonCancelText: 'Cancel'
    }).subscribe((res) => {
      this.pageRepository.deletePage(this.currentModuleId, page.id).pipe(
        this.errorModal.catchError({retryable: true})
      ).subscribe(() => {
        this.moduleService.refresh(this.currentModuleId);
      });
    });
  }


  confirmDeleteModule() {
    const moduleId = this.currentModuleId;
    this.confirmationModal.show({
      content: dedent`
        ## Delete Module?

        Are you sure you want to delete the module
        \`${escapeMarkdown(moduleId)}\`?

        This cannot be easily undone!
      `,
      buttonYesText: 'Delete module',
      buttonCancelText: 'Cancel'
    }).subscribe(() => {
      this.moduleService.deleteModule(moduleId).pipe(
        this.errorModal.catchError({retryable: true})
      ).subscribe(() => {
        this.appNav.home.navigate();
      });
    });
  }

  visitPage(page: PageInfo) {
    window.open(`/${page.applicationId}/pages/${page.id}`);
  }

  visitModule() {
    window.open(`/${this.currentModuleId}`);
  }

  trackPageById(index: number, page: PageInfo) {
    return page.id;
  }
}
