import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ValidatorFn, ValidationErrors } from '@angular/forms';

import { Observable, of, Subscription, throwError } from 'rxjs';
import { distinctUntilChanged, map, switchMap, take } from 'rxjs/operators';

import { TypedFormControl, IdpValidators, ConfirmationModalService } from '@bmi/ui';
import { escapeMarkdown } from '@bmi/utils';
import { BoardResource, GravityResourceService } from '@bmi/gravity-services';
import { combineLatestObject, ObjectMap, watchParams } from '@bmi/utils';
import { CanCheckUnsavedChanges } from '../../has-unsaved-changes.guard';
import dedent from 'ts-dedent';

import { ResourceTextEditorComponent } from '../../components/resource-text-editor/resource-text-editor.component';

import lodashIsEqual from 'lodash-es/isEqual';


function isValidBoardResource(boardResource: BoardResource): boolean {
  return boardResource && !!boardResource.id && !!boardResource.boardId;
}


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gravity-board-resource-page',
  templateUrl: './board-resource-page.component.html'
})
export class BoardResourcePageComponent implements OnInit, OnDestroy {
  pageTitle = '';
  resourceControl = new TypedFormControl<BoardResource>(null, IdpValidators.satisfies(isValidBoardResource, 'Board resource is not valid'));
  boardResource: BoardResource = null;

  @ViewChild(ResourceTextEditorComponent) resourceEditor: ResourceTextEditorComponent;

  private subscription = new Subscription();

  constructor(
    private activatedRoute: ActivatedRoute,
    private resourceService: GravityResourceService,
    private confirmationService: ConfirmationModalService,
    private router: Router
  ) {}

  ngOnInit() {
    watchParams(this.activatedRoute, 'boardId', 'resourceId').pipe(
      switchMap(({boardId, resourceId}) => {
        if (resourceId) {
          return this.resourceService.get(boardId, resourceId, {invalidateNow: true}).pipe(take(1));
        } else {
          return of(<BoardResource>{
            id: '',
            isNew: true,
            name: null,
            resource: '',
            type: 'javascript',
            boardId: boardId
          });
        }
      })
    ).subscribe(resource => {
      this.setInitialResource(resource);
      this.pageTitle = resource.isNew ? 'Create Resource' : resource.name || resource.id;
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  close() {
    this.router.navigate(['/board', this.boardResource.boardId]);
  }

  private setInitialResource(boardResource: BoardResource) {
    this.boardResource = boardResource;
    this.resourceControl.setValue(boardResource);
  }
}
