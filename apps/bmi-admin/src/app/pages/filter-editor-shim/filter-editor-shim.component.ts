import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Filter } from '../../filter-editor/filter.types';
import { FILTER_EDITOR_HELP } from '../../filter-editor/filter-editor.help'
import { GravityFilterService } from '@bmi/gravity-services';
import { HelpService } from '../../components/help/help.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-filter-editor-shim',
  templateUrl: './filter-editor-shim.component.html',
  styleUrls: ['./filter-editor-shim.component.css']
})
export class FilterEditorShimComponent implements OnInit, OnDestroy {

  constructor(
    private route: ActivatedRoute,
    private service: GravityFilterService,
    private help: HelpService,
    private router: Router
  ) { }

  private _filterId: string;
  private _boardId: string;
  private _filterConfig: Filter;

  set filterId(id: string) { this._filterId = id; }
  get filterId(): string { return this._filterId; }

  set boardId(id: string) { this._boardId = id; }
  get boardId(): string { return this._boardId; }

  set filterConfig(config: Filter) { this._filterConfig = config; }
  get filterConfig(): Filter { return this._filterConfig; }

  ngOnInit() {
    this.help.display(FILTER_EDITOR_HELP);
    this.route.params.subscribe(routes => {
      this.filterId = routes.filterId ? routes.filterId : undefined;
      this.boardId = routes.boardId ? routes.boardId : undefined;
      if (this.boardId && this.filterId) {
        this.service.getFilter(this.boardId, this.filterId).subscribe((filter: Filter) => {
          this.filterConfig = filter;
        });
      }
    });
  }

  save(updatedFilterConfig: Filter) {
    if (updatedFilterConfig.id) {
      this.service.updateFilter(this.boardId, this.filterId, updatedFilterConfig).subscribe(response => {
        this.router.navigate(['/board', this.boardId]);
      }, error => {
        console.error(error);
      });
    } else {
      delete updatedFilterConfig.id;
      this.service.addFilter(this.boardId, updatedFilterConfig).subscribe(response => {
      }, error => {
        console.error(error);
      });
    }
  }

  delete(filterId: string) {
    this.service.deleteFilter(this.boardId, filterId).subscribe(response => {
      this.router.navigate(['/board', this.boardId]);
    }, error => {
      console.error(error);
    });
  }

  ngOnDestroy() {
    this.help.display(null);
  }

}
