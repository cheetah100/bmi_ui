import { Component, OnInit, OnDestroy, Injector } from '@angular/core';
import { Subscription, of, combineLatest, NEVER, throwError } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { filter, map, distinctUntilChanged, shareReplay, switchMap, tap } from 'rxjs/operators';
import { ConfirmationModalService } from '@bmi/ui';
import {
  GravityConfigService,
  BoardConfig,
  Field,
  RuleSummary,
  GravityApiView,
  GravityApiFilter,
  BoardResourceSummary,
  GravityResourceService,
  SpreadsheetUploadModalService,
  UploadStatusModalService,
  RulesService, Rule
} from '@bmi/gravity-services';
import { Option, watchParams, sanitizeGravityApiId } from '@bmi/utils';
import sortBy from 'lodash-es/sortBy';
import capitalize from 'lodash-es/capitalize';

import { BoardInfoService } from './board-info.service';
import { BoardRelationshipService } from './board-relationship.service';
import dedent from 'ts-dedent';
import { MatDialog } from '@angular/material/dialog';

@Component({
  templateUrl: './board-summary.component.html',
  styleUrls: ['./board-summary.component.scss'],
})
export class BoardSummaryComponent implements OnInit, OnDestroy {
  subscription = new Subscription();
  boardConfig: BoardConfig;

  views: GravityApiView[];
  filters: GravityApiFilter[];

  transformOptions: Option[];
  ruleSummaries: RuleSummary[];

  compulsoryRules: RuleSummary[];
  taskRules: RuleSummary[];
  validationRules: RuleSummary[];
  scheduledRules: RuleSummary[];

  resources: BoardResourceSummary[];

  ready = false;

  boardId = watchParams(this.activatedRoute, 'boardId').pipe(
    map(({boardId}) => boardId),
    distinctUntilChanged(),
  );

  config = this.boardId.pipe(
    switchMap(boardId => this.gravityConfigService.watchBoardConfig(boardId))
  );

  fields = this.config.pipe(
    map(config => this.makeFieldInfo(config))
  );

  relatedBoards = this.boardId.pipe(
    switchMap(boardId => this.boardRelationshipsService.getRelated(boardId)),
    map(boards => sortBy(boards, [b => b.name]))
  );

  permissionEntries = this.config.pipe(
    map(config => Object.entries(config.permissions).map(([key, value]) => ({principal: key, permission: value})))
  );

  constructor(
    private activatedRoute: ActivatedRoute,
    private gravityConfigService: GravityConfigService,
    private boardInfoService: BoardInfoService,
    private resourceService: GravityResourceService,
    private confirmationService: ConfirmationModalService,
    private matDialogService: MatDialog,
    private injector: Injector,
    private spreadsheetUploadModalService: SpreadsheetUploadModalService,
    private uploadStatusModalService: UploadStatusModalService,
    private boardRelationshipsService: BoardRelationshipService,
    private rulesService: RulesService,
  ) { }

  ngOnInit() {
    this.subscription.add(
      this.boardId.pipe(
        tap(() => this.ready = false),
        switchMap(boardId => combineLatest([
          this.config,
          this.boardInfoService.watch(boardId)
        ]))
      ).subscribe(([boardConfig, boardData]) => {
        this.boardConfig = boardConfig;
        this.transformOptions = boardData.transforms;
        this.ruleSummaries = boardData.rules;
        this.resources = boardData.resources;

        // Split out the rules by type to display in separate boxes.
        this.compulsoryRules = this.ruleSummaries ? this.ruleSummaries.filter(r => r.ruleType === 'COMPULSORY') : null;
        this.taskRules = this.ruleSummaries ? this.ruleSummaries.filter(r => r.ruleType === 'TASK') : null;
        this.validationRules = this.ruleSummaries ? this.ruleSummaries.filter(r => r.ruleType === 'VALIDATION') : null;
        this.scheduledRules = this.ruleSummaries ? this.ruleSummaries.filter(r => r.ruleType === 'SCHEDULED') : null;
        this.views = Object.values(boardConfig.config.views);
        this.filters = Object.values(boardConfig.config.filters);

        this.ready = true;
      })
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  deleteResource(resource: BoardResourceSummary) {
    this.subscription.add(this.confirmationService.show({
      content: dedent`
        ## Delete resource?

        Are you sure you wish to delete the resource ${resource.id}?
      `,
      buttonYesText: 'Yes, delete the resource',
    }).pipe(
      switchMap(() => this.resourceService.delete(resource))
    ).subscribe());
  }

  deleteRule(ruleSummary: RuleSummary) {
    this.subscription.add(this.confirmationService.show({
      content: dedent`
        ## Delete Rule?

        Are you sure you wish to delete the rule ${ruleSummary.id} of type ${ruleSummary.ruleType}?
      `,
      buttonYesText: 'Yes, delete the rule',
    }).pipe(
      switchMap(() => this.rulesService.deleteRule(ruleSummary.boardId, ruleSummary.id))
    ).subscribe( () => this.boardInfoService.refreshBoard(ruleSummary.boardId)));
  }

  makeFieldInfo(config: BoardConfig) {
    const fields = Object.values(config.fields)
      .filter(f => !f.isMetadata)
      .map(field => ({
        ...field,
        hasUnexpectedId: field.id !== sanitizeGravityApiId(field.label),
        typeDescription: describeFieldType(field),
        descriptiveTerms: [
          field.required ? 'Required' : 'Optional',
          field.indexed ? 'Indexed': '',
          field.immutable ? 'Immutable' : ''
        ].filter(x => !!x).join(', ')
      }));

    return sortBy(fields, [f => f.index]);
  }

  showUploadStatusModal() {
    this.uploadStatusModalService.showPopup();
  }

  showBulkDataManagerModal(boardId) {
    this.spreadsheetUploadModalService.showPopup(boardId);
  }
}


function describeFieldType(field: Field): string {
  if (field.optionlist && field.type === 'LIST') {
    return `List of ${field.optionlist}`;
  } else if (field.optionlist) {
    return field.optionlist;
  } else {
    return capitalize(field.type);
  }
}
