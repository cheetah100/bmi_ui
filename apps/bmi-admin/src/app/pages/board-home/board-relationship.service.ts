import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { BoardConfig, GravityConfigService } from '@bmi/gravity-services';
import { createMapFrom } from '@bmi/utils';
import uniq from 'lodash-es/uniq';


@Injectable({providedIn: 'root'})
export class BoardRelationshipService {
  constructor(
    private configService: GravityConfigService
  ) {}

  getRelated(boardId: string): Observable<BoardRelationship[]> {
    return this.configService.watchAllConfigs().pipe(
      map(configs => {
        const boardsById = createMapFrom(configs, bc => bc.id);
        const board = boardsById.get(boardId);
        if (board) {
          return uniq([
            ...this.getReferencedBoards(board),
            ...this.getBoardsWhichReference(board, configs)
          ]).map(id => <BoardRelationship>{
            id,
            name: boardsById.has(id) ? boardsById.get(id).name : id
          });
        } else {
          return [];
        }
      })
    );
  }

  private getReferencedBoards(board: BoardConfig): string[] {
    return Object.values(board.fields)
      .filter(f => !!f.optionlist)
      .map(f => f.optionlist);
  }

  private getBoardsWhichReference(board: BoardConfig, boardConfigs: BoardConfig[]): string[] {
    const boardId = board.id;
    return boardConfigs
      .filter(bc => Object.values(bc.fields).some(f => f.optionlist === boardId))
      .map(bc => bc.id);
  }
}


interface BoardRelationship {
  id: string;
  name: string;
}
