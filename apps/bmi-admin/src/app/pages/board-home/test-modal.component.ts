import { Component, ViewChild } from '@angular/core';
import { ModalComponent } from '@bmi/ui';

@Component({
  template: `<ui-modal>Test Modal</ui-modal>`
})
export class TestModalComponent {
  @ViewChild(ModalComponent, { static: true }) private modal: ModalComponent;

  show() {
    this.modal.show();
  }
}
