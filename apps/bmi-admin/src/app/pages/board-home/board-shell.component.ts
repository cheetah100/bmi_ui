import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GravityConfigService } from '@bmi/gravity-services';
import { distinctUntilChanged, map, switchMap } from 'rxjs/operators';
import { of, Subscription } from 'rxjs';
import { SpreadsheetUploadModalService, UploadStatusModalService } from '@bmi/gravity-services';

import { BoardConfig } from '@bmi/gravity-services';

@Component({
  selector: 'gravity-board-shell',
  template: `
    <header class="board-header">
      <h1 data-cy="board-name">{{board.name}}</h1>

    </header>
    <router-outlet></router-outlet>
  `,
  styleUrls: ['./board-shell.component.scss']
})
export class BoardShellComponent implements OnInit, OnDestroy {
  board: BoardConfig = null;
  private subscriptions: Subscription[] = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private configService: GravityConfigService,
  ) { }

  ngOnInit() {
    this.subscriptions.push(
      this.activatedRoute.paramMap.pipe(
        map(params => params.get('boardId')),
        distinctUntilChanged(),
        switchMap(boardId => boardId ? this.configService.watchBoardConfig(boardId) : of<BoardConfig>(null))
      ).subscribe(board => {
        this.board = board;
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }
}
