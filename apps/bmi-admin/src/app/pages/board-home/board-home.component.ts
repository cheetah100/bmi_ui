import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { GravityConfigService, BoardConfig, Field, GravityUrlService } from '@bmi/gravity-services';
import { map, mergeMap, tap } from 'rxjs/operators';
import { sortBy } from '@bmi/utils';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'gravity-board-home',
  styleUrls: ['../transform-home/object-home.scss', './board-home.component.scss'],
  templateUrl: './board-home.component.html',
})
export class BoardHomeComponent {
  @Input() enableSidebar = true;

  search = '';
  selectedBoardId: string = null;
  refOutBoards: string[] = null;
  refInBoards: string[] = null;
  allBoardConfigs: Observable<BoardConfig[]> = this.gravityConfigService.watchAllConfigs()
    .pipe(
      map((configs: BoardConfig[]) => sortBy<BoardConfig>(
        configs,
        // order by most fields with optionlists.
        // was as confusing as you'd expect.
        // c => Object.values(c.config.fields).filter(field => !!field.optionlist).length,
        c => c.name.toLocaleLowerCase(),
        1,
      )),
      tap((configs: BoardConfig[]) => {
        if (!this.selectedBoardId && configs.length) {
          this.selectBoard(configs[0].id);
        }
      }),
    );

  constructor(
    private gravityConfigService: GravityConfigService,
    private router: Router
  ) { }

  get filteredBoards(): Observable<BoardConfig[]> {
    return this.allBoardConfigs.pipe(
      map((boardConfigs: BoardConfig[]) => boardConfigs.filter((boardConfig: BoardConfig) => (
        !this.search
        || boardConfig.name.toLocaleLowerCase().includes(this.search)
      )))
    );
  }

  updateSearch(search: string): void {
    this.search = search.toLocaleLowerCase().trim();
  }

  boardRowClicked(boardId: string) {
    if (this.enableSidebar) {
      this.selectBoard(boardId);
    } else {
      this.router.navigate(['/board', boardId]);
    }
  }

  selectBoard(boardId: string): void {
    if (!this.enableSidebar) {
      return;
    }

    this.selectedBoardId = boardId;
    this.selectedConfig.pipe(
      tap((boardConfig: BoardConfig) => this.refOutBoards = this.getOptionlistBoards(boardConfig)),
      mergeMap((boardConfig: BoardConfig) => this.getBoardReferences(boardConfig)),
    ).subscribe((ids: string[]) => this.refInBoards = ids);
  }

  deselectId(): void {
    this.selectedBoardId = null;
  }

  isSelected(boardConfig): boolean {
    return (this.selectedBoardId && boardConfig.id === this.selectedBoardId);
  }

  get selectedConfig(): Observable<BoardConfig> {
    return this.getBoardConfig(this.selectedBoardId);
  }

  getBoardConfig(id: string): Observable<BoardConfig> {
    if (!id) {
      return of(null);
    }
    return this.allBoardConfigs.pipe(
      map(
        (configs: BoardConfig[]) => configs.find(
          (config: BoardConfig) => config.id === id
        )
      ),
    );
  }

  /**
   * Boards referenced by optionlists in this board's fields
   */
  getOptionlistBoards(config: BoardConfig): string[] {
    const ids: string[] = Array.from(new Set<string>(
      Object.values(config.fields)
        .map((field: Field) => field.optionlist)
        .filter(Boolean)
    ));
    return (ids.length ? ids.sort() : null);
  }

  /**
   * Boards that reference the targetConfig with their field optionlists
   */
  getBoardReferences(targetConfig: BoardConfig): Observable<string[]> {
    return this.allBoardConfigs.pipe(
      map((boardConfigs: BoardConfig[]) => {
        return Array.from(new Set<string>(boardConfigs.filter(
          (config: BoardConfig) => Object.values(config.fields).some(
            (field: Field) => field.optionlist === targetConfig.id
          )
        ).map((config: BoardConfig) => config.id)));
      }),
      map((ids: string[]) => ids.length ? ids.sort() : null),
    );
  }

  isRefOut(boardConfig: BoardConfig): boolean {
    return this.isRef(boardConfig, this.refOutBoards);
  }

  isRefIn(boardConfig: BoardConfig): boolean {
    return this.isRef(boardConfig, this.refInBoards);
  }

  getBoardTooltip(board: BoardConfig): string {
    if (!this.selectedBoardId || this.isSelected(board)) {
      return board.name;
    } else {
      const isReferencedBySelectedBoard = this.isRefIn(board);
      const isOptionlistOnSelectedBoard = this.isRefOut(board);
      if (isReferencedBySelectedBoard && isOptionlistOnSelectedBoard) {
        return `${board.name}: references, and is referenced by ${this.selectedBoardId}`;
      } else if (isReferencedBySelectedBoard) {
        return `${board.name} has a reference to ${this.selectedBoardId}`;
      } else if (isOptionlistOnSelectedBoard) {
        return `${board.name} is an optionlist field on ${this.selectedBoardId}`;
      } else {
        return board.name;
      }
    }
  }

  isRef(boardConfig: BoardConfig, ids: string[]): boolean {
    if (!this.selectedBoardId || !ids) {
      return false;
    }
    return ids.includes(boardConfig.id);
  }

}
