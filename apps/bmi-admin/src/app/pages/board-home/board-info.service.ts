import { Injectable } from '@angular/core';
import { forkJoin, of, Observable, BehaviorSubject } from 'rxjs';
import { filter, map } from 'rxjs/operators';

import { Option } from '@bmi/ui';

import { RuleSummary, RulesService, GravityResourceService, BoardResourceSummary } from '@bmi/gravity-services';
import { TransformService } from '../../transform-editor/transform.service';

import { ObservableCache, combineLatestObject, ObservableMap } from '@bmi/utils';

@Injectable()
export class BoardInfoService {

  // private transforms = new ObservableCache<Option[]>({
  //   factory: boardId => forkJoin([
  //     this.transformService.getBoardTransformsByType(boardId, 'pivot'),
  //     this.transformService.getBoardTransformsByType(boardId, 'transpose')
  //   ]).pipe(
  //     map(([pivots, transforms]) => [...pivots, ...transforms])
  //   ),
  //   subjectFactory: () => new BehaviorSubject(null)
  // });

  private rules = new ObservableCache<RuleSummary[]>({
    factory: boardId => this.rulesService.getRuleSummaries(boardId),
    subjectFactory: () => new BehaviorSubject(null)
  });

  constructor(
    private rulesService: RulesService,
    // private transformService: TransformService,
    private resourceService: GravityResourceService,
  ) {}

  public watch(boardId: string): Observable<BoardData> {
    this.refreshBoard(boardId);
    return combineLatestObject({
      boardId: of(boardId),
      rules: this.rules.watch(boardId),
      transforms: of(<Option[]>[]), //this.transforms.watch(boardId),
      resources: this.resourceService.list(boardId, {refreshNow: true}),
    });
  }

  refreshBoard(boardId: string) {
    this.rules.refresh(boardId);
    // this.transforms.refresh(boardId);
  }
}

export interface BoardData {
  boardId: string;
  rules: RuleSummary[];
  transforms: Option[];
  resources: BoardResourceSummary[];
}
