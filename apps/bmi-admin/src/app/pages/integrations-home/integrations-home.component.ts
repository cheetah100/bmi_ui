import { Component } from '@angular/core';
import { IntegrationSummary, IntegrationsService, CredentialService, CredentialSummary  } from '@bmi/gravity-services';
import { ConfirmationModalService, ErrorModalService } from '@bmi/ui';
import { escapeMarkdown } from '@bmi/utils';
import { Observable, Subscription } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { orderBy } from 'lodash-es';
import dedent from 'ts-dedent';
import { Router } from '@angular/router';

@Component({
  templateUrl: './integrations-home.component.html',
})
export class IntegrationsHomeComponent {
  integrations: Observable<IntegrationSummary[]> = this.integrationService.list()
    .pipe(
    map((integrations: IntegrationSummary[]) => orderBy<IntegrationSummary>(
      integrations,
      i => i.name.toLocaleLowerCase(),
      ['asc']
    ))
  );
  integrationsSearch: string = '';
  credentials: Observable<CredentialSummary[]> = this.credentialService.list()
    .pipe(
      map((creds: CredentialSummary[]) => orderBy<CredentialSummary>(
        creds,
        c => c.name.toLocaleLowerCase(),
        ['asc']
      ))
    );
  credentialSearch: string = '';
  private subscription: Subscription = new Subscription();

  constructor(
    private integrationService: IntegrationsService,
    private credentialService: CredentialService,
    private confirmationService: ConfirmationModalService,
    private errorModal: ErrorModalService,
    private router: Router
  ) {}

  get filteredIntegrations(): Observable<IntegrationSummary[]> {
    return this.integrations.pipe(
      map((integrationSummaries: IntegrationSummary[]) => integrationSummaries.filter((integrationSummary: IntegrationSummary) => (
        !this.integrationsSearch
        || integrationSummary.name.toLocaleLowerCase().includes(this.integrationsSearch)
      )))
    );
  }

  get filteredCredentials(): Observable<CredentialSummary[]> {
    return this.credentials.pipe(
      map((creds: CredentialSummary[]) => creds.filter((cred: CredentialSummary) => (
        !this.credentialSearch
        || cred.name.toLocaleLowerCase().includes(this.credentialSearch)
      )))
    );
  }

  executeIntegration(integrationId: string) {
    this.subscription.add(this.confirmationService.show({
      content: dedent`
        ## Execute ${escapeMarkdown(integrationId)}?

        This will execute the integration.
      `,
      buttonYesText: `Yes`,
      buttonCancelText: 'Cancel',
      emitCancelEvent: false,
    }).pipe(
    switchMap(() => this.integrationService.execute(integrationId).pipe(
      this.errorModal.catchError({retryable: true})
    ))
   ).subscribe());
  }

  deleteIntegration(integrationId: string) {
    this.subscription.add(this.confirmationService.show({
      content: dedent`
        ## Delete ${escapeMarkdown(integrationId)}?

        This will delete the integration.
      `,
      buttonYesText: `Delete integration`,
      buttonCancelText: 'Cancel',
      emitCancelEvent: false,
    }).pipe(
    switchMap(() => this.integrationService.delete(integrationId).pipe(
      this.errorModal.catchError({retryable: true})
    ))
   ).subscribe());
  }

  deleteCredential(credId: string) {
    this.subscription.add(this.confirmationService.show({
        content: dedent`
          ## Delete ${escapeMarkdown(credId)}?

          This will delete the credential.
        `,
        buttonYesText: `Delete credential`,
        buttonCancelText: 'Cancel',
        emitCancelEvent: false,
      }).pipe(
      switchMap(() => this.credentialService.delete(credId).pipe(
        this.errorModal.catchError({retryable: true})
      ))
    ).subscribe());
  }

  updateIntegrationsSearch(search: string): void {
    this.integrationsSearch = search.toLocaleLowerCase().trim();
  }

  updateCredentialSearch(search: string): void {
    this.credentialSearch = search.toLocaleLowerCase().trim();
  }

  integrationClicked(integrationId: string) {
    this.router.navigate(['/system/integrations/', integrationId, 'edit']);
  }

  credentialClicked(credentialId: string) {
    this.router.navigate(['/system/credentials/', credentialId, 'edit']);
  }
}
