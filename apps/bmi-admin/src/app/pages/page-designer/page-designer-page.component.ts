import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';
import { PageConfig, PageRepositoryService, migratePage, ModuleService } from '@bmi/core';

import { ConfirmationModalService } from '@bmi/ui';
import { CanCheckUnsavedChanges } from '../../has-unsaved-changes.guard';

import dedent from 'ts-dedent';
import lodashIsEqual from 'lodash-es/isEqual';

@Component({
  selector: 'app-page-designer-page',
  templateUrl: './page-designer-page.component.html',
  styleUrls: ['./page-designer-page.component.scss']
})
export class PageDesignerPageComponent implements CanCheckUnsavedChanges {
  appId: string;

  // The original, un-edited, unmigrated config that came from the page repository
  originalConfig: PageConfig = null;

  // The page config we feed into the PageEditorComponent. If we change this
  // value the page editor will re-initialize, clearing the undo (e.g. we've
  // changed the page being edited). The page editor keeps its own state which
  // is a bit weird.
  pageConfig: PageConfig = null;

  // This is the latest edited page, corresponding to the current state of the
  // page editor. This is the version of the page that will be saved.
  updatedConfig: PageConfig = null;

  // This property is used by CanCheckUnsavedChanges to implement the automatic
  // "has unsaved changes" popup when you navigate away. This value is
  // controlled by the onPageUpdated method, which compares the updated config
  // against the original. It also determines whether the save button is enabled
  hasUnsavedChanges = false;

  constructor(
    private pageRepository: PageRepositoryService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private confirmationModal: ConfirmationModalService,
    private moduleService: ModuleService,
  ) { }

  ngOnInit() {
    this.activatedRoute.paramMap.pipe(
      map(params => ({ appId: params.get('moduleId'), pageId: params.get('pageId') })),
      switchMap(({ appId, pageId }) => this.pageRepository.getPage(appId, pageId).pipe(
        map(page => ({ appId, pageId, page }))
      ))
    ).subscribe(({ page, appId }) => {
      this.appId = appId;
      this.loadPage(page);
    });
  }

  loadPage(page: PageConfig) {
    this.originalConfig = page;

    // Apply any migrations to the page now, as a pseudo edit action, calling
    // the onPageUpdated handler so that save will be enabled if there are
    // changes to the page as a result of migration
    this.pageConfig = migratePage(page);
    this.onPageUpdated(this.pageConfig);
  }

  onPageUpdated(newConfig: PageConfig) {
    this.updatedConfig = newConfig;
    this.hasUnsavedChanges = this.pageConfig && !lodashIsEqual(newConfig, this.originalConfig);
  }

  get hasPageConfig() {
    return !!this.pageConfig;
  }

  saveClicked() {
    this.pageRepository.savePage(this.appId, this.updatedConfig).subscribe(savedConfig => {
      // Load the page. This will ensure that hasUnsavedChanges is updated etc.
      // While we could just set the flag to false then navigate away, doing it
      // this way is more robust if we want to add a save and keep editing
      // feature later.
      this.loadPage(savedConfig);
      this.moduleService.refresh(this.appId);
      this.returnToModuleHub();
    });
  }

  cancelClicked() {
    if (this.hasUnsavedChanges) {
      this.confirmationModal.show({
        content: dedent`
          ## Discard changes?

          If you proceed, any changes to this page will be lost.
        `,
        buttonYesText: 'Discard changes',
        buttonCancelText: 'Keep editing'
      }).subscribe(() => {
        this.loadPage(this.originalConfig);
        this.returnToModuleHub();
      });
    } else {
      this.returnToModuleHub();
    }
  }

  private returnToModuleHub() {
    this.router.navigate(['/module', this.appId]);
  }
}
