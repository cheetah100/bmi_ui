import { HttpErrorResponse } from '@angular/common/http';
import { Component, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GravityApiBoard } from '@bmi/gravity-services';
import { ConfirmationModalService } from '@bmi/ui';
import { empty, Observable, of, Subscription } from 'rxjs';
import {
  catchError,
  distinctUntilChanged,
  map,
  switchMap,
  tap
} from 'rxjs/operators';
import dedent from 'ts-dedent';
import { BoardDetailsService } from '../../board-editor/board-details.service';
import { BoardEditorComponent } from '../../board-editor/board-editor.component';
import { BOARD_EDITOR_HELP } from '../../board-editor/board-editor.help';
import { HelpService } from '../../components/help/help.service';
import { CanCheckUnsavedChanges } from '../../has-unsaved-changes.guard';

@Component({
  selector: 'gravity-board-editor-shim',
  template: `
    <ui-loading-container
      [loadingMessage]="loadingMessage"
      [isLoading]="isLoading"
      [errorMessage]="errorMessage"
      [hasError]="!!errorMessage"
    >
      <ng-template>
        <bmi-board-editor [board]="board"></bmi-board-editor>
        <hr />
        <button
          type="button"
          class="btn btn--primary board-editor__save-button"
          (click)="saveClicked()"
          [disabled]="!canSaveBoard"
        >
          <span class="icon-save"></span>
          Save Board
        </button>
        <button
          type="button"
          class="btn btn--ghost board-editor__cancel-button"
          (click)="cancelClicked()"
        >
          <span class="icon-close"></span>
          Cancel
        </button>
      </ng-template>
    </ui-loading-container>
  `,
  styles: [
    `
      ui-loading-container {
        min-height: 50vh;
      }

      gravity-board-editor {
        margin-bottom: 5rem;
      }
    `
  ]
})
export class BoardEditorShimComponent implements CanCheckUnsavedChanges {
  public board: GravityApiBoard = null;

  public get isLoading() {
    return !!this.loadingMessage;
  }
  public loadingMessage: string = null;
  public errorMessage: string = null;

  @ViewChild(BoardEditorComponent) boardEditor: BoardEditorComponent;
  private subscriptions: Subscription[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: BoardDetailsService,
    private confirmationService: ConfirmationModalService,
    private helpService: HelpService
  ) {}

  get canSaveBoard(): boolean {
    return this.boardEditor && this.boardEditor.isValid;
  }

  get hasUnsavedChanges() {
    return this.boardEditor && this.boardEditor.hasUnsavedChanges;
  }

  ngOnInit() {
    this.helpService.display(BOARD_EDITOR_HELP);

    this.subscriptions.push(
      this.route.paramMap
        .pipe(
          tap(() => {
            this.errorMessage = null;
          }),
          map(params => params.get('boardId')),
          distinctUntilChanged(),
          switchMap(boardId => this.loadBoard(boardId))
        )
        .subscribe(board => {
          this.board = board;
          this.loadingMessage = null;
          this.errorMessage = null;
        })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
    this.helpService.display(null);
  }

  loadBoard(boardId: string): Observable<GravityApiBoard> {
    this.loadingMessage = 'Loading board';
    if (!boardId) {
      return of(null);
    } else {
      return this.service.loadBoard(boardId, true).pipe(
        catchError((err: HttpErrorResponse) => {
          this.errorMessage = 'Error loading board: ' + err.message;
          return empty();
        })
      );
    }
  }

  cancelClicked() {
    // existing board home, or /board index if creating a new board
    const route: string[] = ['/board'];
    if (this.board && !!this.board.id) {
      route.push(this.board.id);
    }
    this.router.navigate(route);
  }

  saveClicked() {
    this.subscriptions.push(
      this.confirmationService
        .show({
          content: dedent`
        ## Are you sure you want to save the board? ##

        Removing or modifying fields can impact views and visualizations that
        use them. If you've renamed or removed fields, you will need to manually
        fix any configurations that depend on them.`
        })
        .pipe(
          tap(() => (this.loadingMessage = 'Saving board')),
          switchMap(() => {
            const isNewBoard = this.boardEditor.isNewBoard;
            const config = this.boardEditor.save();
            return (isNewBoard
              ? of(config)
              : this.combineWithLatestConfig(config)
            ).pipe(
              switchMap(preparedConfig =>
                this.service.saveBoard(preparedConfig, isNewBoard)
              )
            );
          }),
          catchError(error => {
            this.errorMessage = 'Error Saving Board: ' + error.message;
            return empty();
          })
        )
        .subscribe(boardConfig => {
          this.router.navigate(['/board', boardConfig.id]);
        })
    );
  }

  private combineWithLatestConfig(
    board: GravityApiBoard
  ): Observable<GravityApiBoard> {
    return this.service.loadBoard(board.id, true).pipe(
      catchError(() => of({ filters: {}, views: {} })),
      map(latestConfig => {
        return {
          ...board,

          // We don't want to accidentally update views or filters. These are
          // 'bycatch' in this API due to the way Gravity is currently
          // implemented.
          filters: latestConfig.filters || {},
          views: latestConfig.views || {}
        };
      })
    );
  }
}
