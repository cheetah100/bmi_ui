import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GravityUrlService, GravityApiApplication } from '@bmi/gravity-services';
import { HttpClient } from '@angular/common/http';

@Injectable({providedIn: 'root'})
export class ModuleInfoEditorService {

  constructor(
    private http: HttpClient,
    private urls: GravityUrlService
  ) { }

  getModule(moduleId: string): Observable<GravityApiApplication> {
    return this.http.get<GravityApiApplication>(this.urls.app(moduleId).baseUrl);
  }

  saveModule(moduleObject: GravityApiApplication, isNew: boolean) {
    if (!isNew) {
      return this.http.put<GravityApiApplication>(this.urls.url(`/app/${moduleObject.id}`), moduleObject);
    } else {
      return this.http.post<GravityApiApplication>(this.urls.url(`/app`), moduleObject);
    }
  }

  deleteModule(moduleId) {
    return this.http.delete(this.urls.url(`/app/${moduleId}`));
  }
}
