import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { empty, of, Observable, ReplaySubject } from 'rxjs';
import { map, switchMap, catchError, tap, distinctUntilChanged, flatMap } from 'rxjs/operators';
import dedent from 'ts-dedent';

import get from 'lodash-es/get';
import cloneDeep from 'lodash-es/cloneDeep';

import { GravityApiApplication, Permissions } from '@bmi/gravity-services';
import { ConfirmationModalService, TypedFormGroup, TypedFormControl, Option, ErrorModalService} from '@bmi/ui';
import { ModuleExtraSettings, OptionlistService, ModuleService} from '@bmi/core';
import { watchParams } from '@bmi/utils';

import { ModuleInfoEditorService } from './module-info-editor.service';
import { SaveConfirmationModalService } from '../../components/save-confirmation-modal/save-confirmation-modal.service';
import { AdminAppNavService } from '../../admin-app-nav.service';
import { connectNameToIdFieldHelper } from '../../utils/connect-name-to-id-field-helper';

const DEFAULT_MODULE: Partial<GravityApiApplication> = {
  modules: {
    menu: {
      id: 'menu',
      ui: {defaultRoute: '', data: []}
    },

    extraSettings: {
      id: 'extraSettings',
      ui: {}
    }
  }
}

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'app-module-info-editor',
  templateUrl: './module-info-editor.component.html',
  styleUrls: ['./module-info-editor.component.scss']
})
export class ModuleInfoEditorComponent implements OnInit {

  data: any;
  moduleForm: FormGroup;
  loading = false;
  isNew = false;

  moduleObservable = new ReplaySubject<Observable<GravityApiApplication>>(1);
  originalModule: GravityApiApplication = undefined;

  boardOptions = this.optionlistService.getAllBoards().pipe(
    map(boards => boards.map(b => <Option>{value: b.id, text: b.name}))
  );

  readonly form = new TypedFormGroup({
    app: new TypedFormGroup<Partial<GravityApiApplication>>({
      id: new TypedFormControl<string>(),
      name: new TypedFormControl<string>(),
      description: new TypedFormControl<string>(),
      permissions: new TypedFormControl<Permissions>()
    }),

    extraSettings: new TypedFormGroup<ModuleExtraSettings>({
      group: new TypedFormControl(),
      sortWeight: new TypedFormControl(),
      favoriteBoards: new TypedFormControl()
    })
  });


  get idControl() {
    return this.form.get('app.id') as TypedFormControl<string>;
  }

  get nameControl() {
    return this.form.get('app.name') as TypedFormControl<string>;
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private confirmationService: ConfirmationModalService,
    private moduleInfoEditorService: ModuleInfoEditorService,
    private saveConfirmation: SaveConfirmationModalService,
    private optionlistService: OptionlistService,
    private errorModalService: ErrorModalService,
    private appNav: AdminAppNavService,
    private moduleService: ModuleService
  ) { }

  ngOnInit() {
    connectNameToIdFieldHelper(this.nameControl, this.idControl);


    watchParams(this.route, 'moduleId').subscribe(({moduleId}) => {
      this.moduleObservable.next(this.getOrCreateModule(moduleId));
    });

    this.moduleObservable.pipe(
      switchMap(moduleObservable => moduleObservable)
    ).subscribe(moduleConfig => {
      this.originalModule = moduleConfig;
      this.isNew = !moduleConfig.id;
      const extraSettings = moduleConfig.modules['extraSettings'];
      if (!!moduleConfig) {
        this.form.reset({
          app: moduleConfig || {},
          extraSettings: extraSettings && extraSettings.ui || {}
        }, {emitEvent: false});
      } else {
        this.form.reset({}, {emitEvent: false});
      }

      if (this.isNew) {
        this.idControl.enable();
      } else {
        this.idControl.disable();
      }
    });
  }

  getOrCreateModule(moduleId: string): Observable<GravityApiApplication> {
    if (moduleId) {
      return this.moduleInfoEditorService.getModule(moduleId);
    } else {
      return of(DEFAULT_MODULE as GravityApiApplication);
    }
  }


  saveModule() {
    const formValue = this.form.getRawValue();
    const existingSource = !this.isNew ? this.moduleInfoEditorService.getModule(this.originalModule.id) : of(DEFAULT_MODULE as GravityApiApplication);

    existingSource.pipe(
      map(config => {
        const original = config;
        const updated = cloneDeep(config);
        Object.assign(updated, formValue.app);
        const extraSettings = get(updated, ['modules', 'extraSettings'], {id: 'extraSettings', ui: {}});
        extraSettings.ui = Object.assign(extraSettings.ui || {}, formValue.extraSettings);
        updated.modules['extraSettings'] = extraSettings;
        return {original, updated};
      }),
      switchMap(({original, updated}) => {
        return this.saveConfirmation.show({
          title: 'Save Module?',
          body: 'Are you sure you want to save this module?',
          showDiff: !this.isNew,
          original: JSON.stringify(original, undefined, 2),
          modified: JSON.stringify(updated, undefined, 2),
        }).pipe(map(() => updated));
      }),
      switchMap(updated => {
        return this.moduleInfoEditorService.saveModule(updated, this.isNew).pipe(
          this.errorModalService.catchError({retryable: true})
        )
      })
    ).subscribe(saved => {
      this.moduleService.invalidateModuleConfig(saved.id);
      this.appNav.moduleHub(saved.id).navigate();
    });
  }

  deleteModule() {
    this.confirmationService.show({
      content: dedent`
      ## Confirmation for Module Deletion

      Deleting the module is an irreversible action. On deletion,  all the associated data like pages and sub-modules would be lost.

      Are you sure you want to delete this module?
      `,
      buttonYesText: 'Delete Module',
      buttonCancelText: 'Cancel',
    }).pipe(
      tap(() => this.loading = true),
      flatMap(() => this.moduleInfoEditorService.deleteModule(this.moduleForm.value.id)),
      catchError(error => {
        this.loading = false;
        console.error(error);
        return empty();
      }),
    ).subscribe(() => {
      this.router.navigate(['/']);
    });
  }

}
