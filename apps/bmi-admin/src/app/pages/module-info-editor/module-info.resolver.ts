import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { GravityUrlService } from '@bmi/gravity-services';
import { ActivatedRouteSnapshot } from '@angular/router';

@Injectable()
export class ModuleInfoResolver implements Resolve<Observable<any>> {
  constructor(
    private http: HttpClient,
    private gravityUrlService: GravityUrlService
  ) {

  }
  resolve(route: ActivatedRouteSnapshot) {
    return this.http.get(this.gravityUrlService.url(`app/` + route.paramMap.get('moduleId')));
  }
}
