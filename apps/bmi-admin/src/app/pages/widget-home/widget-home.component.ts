import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { AppService } from '../../app.service';
import { GravityWidgetService, GravityApiWidget } from '@bmi/gravity-services';
import { mergeMap, filter, tap, catchError, map, take } from 'rxjs/operators';
import { ChartData, ChartDataSourceService } from '@bmi/legacy-charts';
import { EMPTY } from 'rxjs';

import sortBy from 'lodash-es/sortBy';

@Component({
  templateUrl: './widget-home.component.html',
  styleUrls: ['../transform-home/object-home.scss', './widget-home.component.scss'],
})
export class WidgetHomeComponent implements OnInit {
  search = '';
  widgets: GravityApiWidget[] = null;
  selectedWidget: GravityApiWidget = null;
  widgetTypes: string[];
  chartData: ChartData[] = null;
  moduleId = '';
  private editableCharts = new Set<string>([
    'bmi_chart'
  ]);

  constructor(
    private appService: AppService,
    private gravityWidgetService: GravityWidgetService,
    private chartDataSourceService: ChartDataSourceService,
  ) { }

  ngOnInit(): void {
    this.appService.appId.pipe(
      filter(Boolean),
      tap((id: string) => {
        this.moduleId = id;
        this.widgets = null;
      }),
      mergeMap((id: string) => this.gravityWidgetService.getAppWidgets(id)),
    ).subscribe((widgets: GravityApiWidget[]) => {
      const widgetTypeSet = new Set<string>(widgets.map(w => w.widgetType));
      this.widgets = sortBy(widgets, [w => w.widgetType, w => this.displayName(w)]);
      this.widgetTypes = Array.from(widgetTypeSet).sort();
    });
  }

  get filteredWidgets(): GravityApiWidget[] {
    return this.widgets.filter((widget: GravityApiWidget) => (
      !this.search
      || widget.title.toLocaleLowerCase().includes(this.search)
    ));
  }

  typeHasWidgets(widgetType: string): boolean {
    return !!this.filteredWidgetsByType(widgetType).length;
  }

  filteredWidgetsByType(widgetType: string): GravityApiWidget[] {
    return this.filteredWidgets.filter(
      (widget: GravityApiWidget) => widget.widgetType === widgetType
    );
  }

  canEditWidget(widget: GravityApiWidget = this.selectedWidget): boolean {
    return this.editableCharts.has(widget.widgetType);
  }

  updateSearch(search: string): void {
    this.search = search.toLocaleLowerCase().trim();
  }

  displayName(widget: GravityApiWidget): string {
    const title = widget.title.trim() || widget.name;

    if (title.length < 5) {
      return `${title} (id: ${widget.id})`;
    } else {
      return title;
    }
  }

  selectWidget(widget: GravityApiWidget): void {
    this.selectedWidget = widget;
    this.chartData = null;
    if (this.canEditWidget(widget)) {
      this.getChartData(widget);
    }
  }

  deselectWidget(): void {
    this.selectedWidget = null;
  }

  isSelected(widget: GravityApiWidget): boolean {
    return (this.selectedWidget && widget.id === this.selectedWidget.id);
  }

  getChartData(widget: GravityApiWidget): void {
    this.chartDataSourceService.getChartData(widget.fields.dataSources).pipe(
      catchError(error => {
        console.log(error);
        return EMPTY;
      }),
    ).subscribe(data => this.chartData = data);
  }
}
