import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { of, Observable, combineLatest } from 'rxjs';
import { catchError, map, shareReplay } from 'rxjs/operators';
import { distinctUntilNotEqual, combineLatestObject } from '@bmi/utils';
import { Integration, IntegrationSummary, IntegrationsService, SystemStatusService, GravityApiCardEvent, GravityApiSystemRevisionInfo } from '@bmi/gravity-services';
import orderBy from 'lodash-es/orderBy';
import groupBy from 'lodash-es/groupBy';
import mapValues from 'lodash-es/mapValues';

@Component({
  templateUrl: './system-page.component.html',
  styleUrls: ['./system-page.component.scss']
})
export class SystemPageComponent {
  systemStatus = this.systemStatusService.getStatus();

  softwareVersions = combineLatest([makeRevisionInfo(
      'Gravity',
      this.systemStatusService.getRevisionInfo(),
      'your-repository-web-ui-url'
    ),

    makeRevisionInfo(
      'BMI Frontend',
      this.httpClient.get<GravityApiSystemRevisionInfo>('/revision').pipe(shareReplay(1)),
      'your-repo-web-ui-url'
    )
  ]);

  constructor(
    private systemStatusService: SystemStatusService,
    private httpClient: HttpClient,
  ) {}
}


function makeRevisionInfo(name: string, versionInfo: Observable<GravityApiSystemRevisionInfo>, bitBucketBaseUrl?: string) {
  return versionInfo.pipe(
    catchError(() => of(<GravityApiSystemRevisionInfo>{})),
    map(version => {
      const hash = version['git.commit.id'];
      return {
        name,
        hash,
        versionText: hash ? hash.substring(0, 8) : 'Development',
        message: version[''],
        buildTime: new Date(version['git.build.time']).getTime(),
        scmUrl: (bitBucketBaseUrl && hash) ? bitBucketBaseUrl + version['git.commit.id'] : null
      };
    }),
  )
}
