import { Component, Input, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { MenuService, GravityApiMenuUi } from './menu.service';
import { Router, ActivatedRoute } from '@angular/router';
import { PageRepositoryService, PageInfo } from '@bmi/core';
import { AppService } from '../../app.service';
import { combineLatest, Subscription } from 'rxjs';
import { mergeMap, tap } from 'rxjs/operators';
import { Option } from '@bmi/utils';

@Component({
  template: `
<h1 #heading>Menu Editor</h1>
<ui-loading-container [isLoading]="!isLoading">
  <ng-template>
    <gravity-admin-menu-editor
      (cancel)="cancel()"
      (save)="saveMenu($event)"
      [offset]="editorY"
      [moduleId]="moduleId"
      [menuConfig]="menuConfig"
      [pages]="pageOptions">
    </gravity-admin-menu-editor>
  </ng-template>
</ui-loading-container>
  `,
  styles: [`
h1 {
  margin: 1rem 0;
}
  `],
})
export class MenuPageComponent implements OnInit, OnDestroy {

  /** Menu page, works as shim for menu editor module */

  @ViewChild('heading', { static: true }) heading: ElementRef;

  private menuConfig: GravityApiMenuUi;
  private subscription: Subscription;
  pageOptions: Option[];
  moduleId: string;

  get isLoading(): boolean {
    return !!this.menuConfig;
  }

  get editorY(): number {
    return this.heading.nativeElement.getBoundingClientRect().bottom;
  }

  constructor(
    private menuService: MenuService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private pageRepositoryService: PageRepositoryService,
    private appService: AppService,
  ) { }

  ngOnInit() {
    this.subscription = this.appService.appId.pipe(
      tap((moduleId: string) => this.moduleId = moduleId),
      mergeMap((moduleId: string) => combineLatest([
        this.menuService.menuConfig,
        this.pageRepositoryService.listPages(moduleId),
      ]))
    ).subscribe((result: [GravityApiMenuUi, PageInfo[]]) => {
      const [menuConfig, pages] = result;
      this.menuConfig = menuConfig;
      this.pageOptions = pages.map((page: PageInfo) => ({
        text: page.title,
        value: page.id,
      }));
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  cancel(): void {
    this.navigateToParent();
  }

  navigateToParent(): void {
    this.router.navigate(['../'], { relativeTo: this.activatedRoute });
  }

  saveMenu(menuConfig: GravityApiMenuUi): void {
    this.menuService.saveMenuConfig(menuConfig);
    this.navigateToParent();
  }

}
