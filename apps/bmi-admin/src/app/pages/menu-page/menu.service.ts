import { Injectable } from '@angular/core';
import { MenuItem } from '../../menu-editor/menu-item';
import { Observable } from 'rxjs';
import { AppService } from '../../app.service';
import { GravityApiApplication, GravityUrlService } from '@bmi/gravity-services';
import { mergeMap, map, filter } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { BmiRoute } from '@bmi/core';

export interface GravityApiMenuUi {
  data: MenuItem[];
  defaultRoute: BmiRoute;
}

@Injectable({
  providedIn: 'root',
})
export class MenuService {

  constructor(
    private appService: AppService,
    private gravityUrlService: GravityUrlService,
    private httpClient: HttpClient,
  ) { }

  get menuConfig(): Observable<GravityApiMenuUi> {
    return this.appService.appId.pipe(
      mergeMap((id: string) => this.appService.getAppConfig(id)),
      filter(Boolean),
      map((config: GravityApiApplication) => {
        if (config.modules && config.modules.menu && config.modules.menu.ui) {
          return {
            data: [],
            defaultRoute: '',
            ...config.modules.menu.ui
          };
        }
        return { data: [], defaultRoute: '' };
      }),
    );
  }

  /**
   * This api usage would be best moved to shared-gravity lib.
   * However, we are not yet decided whether menu will remain at /modules/menu
   * so I'm keeping here for now. If moved to /visualisation/menu, we
   * already have shared-gravity tools
   * using POST: PUT wasn't working
   */
  saveMenuConfig(config: GravityApiMenuUi) {
    this.appService.appId.subscribe((appId: string) => {
      const appUrl = this.gravityUrlService.appUrl(appId);
      const moduleUrl = `${appUrl}/modules`;
      const payload = {
        id: 'menu',
        ui: config,
      };
      this.httpClient.post(moduleUrl, payload).subscribe(() => {
        this.appService.getAppConfig(appId, true).subscribe(() => { });
      });
    });
  }

}
