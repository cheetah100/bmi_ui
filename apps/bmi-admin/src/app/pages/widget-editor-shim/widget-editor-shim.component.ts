import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { of, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, switchMap, tap } from 'rxjs/operators';

import { ConfirmationModalService, TypedFormGroup, TypedFormControl } from '@bmi/ui';
import { DataSourceService, GravityWidgetService, GravityApiWidget } from '@bmi/gravity-services';
import { ChartEditorService, ChartData, ChartConfig } from '@bmi/legacy-charts';

import lodashIsEqual from 'lodash-es/isEqual';

import dedent from 'ts-dedent';
import { ConfirmationModalComponent } from '@bmi/ui';


@Component({
  selector: 'app-widget-editor-shim',
  templateUrl: './widget-editor-shim.component.html',
  styleUrls: ['./widget-editor-shim.component.scss'],
  providers: [
    ChartEditorService
  ]
})
export class WidgetEditorShimComponent implements OnInit {
  @Input() widgetType = 'bmi_charts';

  @ViewChild(ConfirmationModalComponent) private confirmationModal: ConfirmationModalComponent;

  readonly form = new TypedFormGroup({
    title: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    chartConfig: new TypedFormControl(<ChartConfig>{}),
  });

  // To prevent overwriting any existing fields on the widget we will preserve
  // the original widget config, then just merge in the fields that have
  // changed.
  originalWidget: GravityApiWidget;

  previewConfig: ChartConfig;
  chartData: ChartData[];

  isLoading = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private widgetService: GravityWidgetService,
    private confirmationService: ConfirmationModalService,
    private chartEditorService: ChartEditorService
  ) { }

  ngOnInit() {
    this.route.paramMap.pipe(
      map(params => ({ appId: params.get('moduleId'), widgetId: params.get('widgetId') })),
      distinctUntilChanged((a, b) => lodashIsEqual(a, b)),
      switchMap(({ appId, widgetId }) => {
        this.chartEditorService.reset();
        this.isLoading = true;
        if (appId && widgetId) {
          return this.widgetService.getWidget(appId, widgetId);
        } else if (appId) {
          return of(this.createNewWidgetConfig(appId));
        } else {
          throw new Error('App ID must be provided to use this component');
        }
      }),
      tap(() => this.isLoading = false)
    ).subscribe(widgetConfig => {
      this.originalWidget = widgetConfig;
      this.form.reset();
      this.form.patchValue({
        title: widgetConfig.title,
        description: widgetConfig.description,
        chartConfig: widgetConfig.fields as ChartConfig
      });
    });

    this.chartEditorService.chartData.subscribe(chartData => this.chartData = chartData);

    this.form.valueChanges.pipe(
      debounceTime(500),
      filter(() => this.form.controls.chartConfig.valid),
      map(value => value.chartConfig),
      distinctUntilChanged((a, b) => lodashIsEqual(a, b))
    ).subscribe(value => {
      this.previewConfig = value;
    });
  }

  saveWidget() {
    const widget = this.mergeIntoWidgetConfig(this.form.getRawValue());
    const appId = widget.applicationId;
    const widgetId = widget.id;
    const verb = !widgetId ? 'create' : 'update';

    this.confirmationService.show({
      content: dedent`
        ### Confirm ${verb} widget

        Are you sure you want to ${verb} this widget?
      `
    }).pipe(
      switchMap(() => {
        if (widgetId) {
          return this.widgetService.putWidget(appId, widgetId, widget);
        } else {
          return this.widgetService.createWidget(appId, widget);
        }
      })
    ).subscribe(updatedWidget => {
      this.router.navigate(['/module', appId, 'widget']);
    });
  }

  mergeIntoWidgetConfig(value: { [key: string]: any }): GravityApiWidget {
    return Object.assign({}, this.originalWidget, {
      name: value.title,
      title: value.title,
      description: value.description,
      widgetType: 'bmi_chart',
      fields: Object.assign({}, this.originalWidget ? this.originalWidget.fields : {}, value.chartConfig)
    });
  }

  loadExistingWidget(appId: string, widgetId: string): Observable<GravityApiWidget> {
    return this.widgetService.getWidget(appId, widgetId);
  }

  createNewWidgetConfig(appId: string): GravityApiWidget {
    return {
      id: null,
      name: null,
      title: null,
      description: null,
      widgetType: 'bmi_chart',
      width: 4,
      height: 4,
      applicationId: appId,
      phase: 'unpublished',
      fields: {
        "chartType": "@bmi/legacy-charts:highcharts-basic-chart-v1",
        "chartSettings": {}
      }
    };
  }

  cancelWidget() {
    this.confirmationService.show({
      content: dedent`
      ## Confirmation for Discarding Changes

      Any additional unsaved changes made while designing the widget would be lost on confirming the cancellation.

      Are you sure you want to discard the changes?
      `,
      buttonYesText: 'Discard Changes',
      buttonCancelText: 'Keep Editing',
    }).subscribe(() => {
      this.router.navigateByUrl(`/module/${this.originalWidget.applicationId}/widget`);
    });
  }
}
