import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { GravityConfigService, BoardConfig } from '@bmi/gravity-services';

@Component({
  selector: 'gravity-view-summary',
  templateUrl: './view-summary.component.html',
  styleUrls: ['./view-summary.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ViewSummaryComponent implements OnInit {

  viewId;
  viewConfig;

  constructor(
    private activatedRoute: ActivatedRoute,
    private gravityConfigService: GravityConfigService,
  ) { }

  ngOnInit() {
    const snapshot: ActivatedRouteSnapshot = this.activatedRoute.snapshot;
    const boardId = snapshot.paramMap.get('boardId');
    this.viewId = snapshot.paramMap.get('viewId');
    // just using the view contained in board configs for now
    this.gravityConfigService.watchBoardConfig(boardId).subscribe(
      (boardConfig: BoardConfig) => this.viewConfig = boardConfig.views[this.viewId]
    );
  }

}
