import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Subscription, of, Observable } from 'rxjs';
import { distinctUntilChanged, mergeMap } from 'rxjs/operators';
import { HelpService } from '../../components/help/help.service';
import { GravityViewService, View, GravityApiView, Gravity, BoardConfig } from '@bmi/gravity-services';

@Component({
  templateUrl: './view-editor-shim.component.html',
  styles: [`
gravity-view-editor {
  margin-bottom: 1rem;
  display: block;
}
  `],
})
export class ViewEditorShimComponent implements OnInit, OnDestroy {

  private subscription: Subscription;
  loadedView: View;
  modifiedView: View;
  boardId: string;
  viewId: string;
  valid = true;

  constructor(
    private activatedRoute: ActivatedRoute,
    private helpService: HelpService,
    private gravityViewService: GravityViewService,
    private router: Router,
    private gravity: Gravity,
  ) { }

  ngOnInit() {
    this.subscription = this.activatedRoute.paramMap.pipe(
      distinctUntilChanged(),
      mergeMap((params: ParamMap) => {
        this.viewId = params.get('viewId');
        this.boardId = params.get('boardId');
        if (!!this.viewId) {
          return this.gravityViewService.getViewConfig(this.boardId, this.viewId);
        }
        return of(new View(this.boardId, '', '', 'view').toJSON());
      }),
    ).subscribe((viewConfig: GravityApiView) => {
      this.setViews(viewConfig);
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.showHelp();
  }

  showHelp(copy: string = null): void {
    this.helpService.display(copy);
  }

  setViews(viewConfig: GravityApiView): void {
    this.loadedView = View.fromJSON(viewConfig);
    this.modifiedView = View.fromJSON(viewConfig);
  }

  updateModifiedView(update: { view: View, valid: boolean }): void {
    this.valid = update.valid;
    this.modifiedView = update.view;
  }

  saveView(): void {
    this.gravityViewService.saveView(this.boardId, this.modifiedView.toJSON())
      .subscribe(() => {
        this.router.navigate(['board', this.boardId]);
        this.reloadBoardConfig();
      });
  }

  reloadBoardConfig(): void {
    this.gravity.boards.getBoardConfig(this.boardId).subscribe(
      config => this.gravity.configService.registerBoard(config, true)
    );
  }

}
