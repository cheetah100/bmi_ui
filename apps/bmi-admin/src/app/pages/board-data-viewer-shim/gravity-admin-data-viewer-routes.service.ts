import { DataViewerRoutesService } from '../../data-viewer/data-viewer-routes.service';
import { Injectable } from '@angular/core';

@Injectable()
export class GravityAdminDataViewerRoutesService extends DataViewerRoutesService {
  boardDataView(boardId: string) {
    return boardId ? ['/board', boardId, 'data-viewer'] : null;
  }

  cardDataView(boardId: string, cardId: string) {
    return boardId && cardId ? ['/board', boardId, 'data-viewer', cardId] : null;
  }
}
