import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params, ParamMap } from '@angular/router';

import { Subscription, Subject, timer, forkJoin } from 'rxjs';
import { map, debounce, distinctUntilChanged, mergeMap, tap } from 'rxjs/operators';

/**
 * ::ng-deep style temporary. Until data-table handles overflow.
 */

@Component({
  selector: 'gravity-board-data-viewer-shim',
  template: `
    <ui-search-input data-cy="board-data-searcher"
      placeholder="Find in cards"
      [selection]="true"
      (newSearch)="updateSearch($event)">
    </ui-search-input>
    <gravity-data-viewer
      *ngIf="boardId"
      [boardId]="boardId"
      [viewId]="viewId"
      [search]="search"></gravity-data-viewer>
  `,
  styles: [`
    :host ::ng-deep .data-head > div {
      min-width: 12rem;
    }
    ui-search-input {
      margin-bottom: 1rem;
      display: block;
    }
    gravity-data-viewer {
      max-height: calc(100vh - 182px);
      overflow: scroll;
      display: block;
    }
    gravity-data-viewer::-webkit-scrollbar {
      height: 9px;
      width: 10px;
    }
    :host ::ng-deep gravity-data-viewer .table-content {
      overflow-x: visible;
    }
  `]
})
export class BoardDataViewerShimComponent implements OnInit, OnDestroy {
  public boardId: string = null;
  public viewId: string = null;
  public search = '';
  private subscriptions: Subscription[];
  private searchStream: Subject<string> = new Subject();

  constructor(
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.subscriptions = [
      this.activatedRoute.paramMap.subscribe((paramMap: ParamMap) => {
        this.boardId = paramMap.get('boardId');
        this.viewId = paramMap.get('viewId');
      }),
      this.searchStream.asObservable().pipe(
        debounce(() => timer(200)),
        distinctUntilChanged(),
      ).subscribe((search: string) => {
        this.search = search;
      }),
    ];
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription: Subscription) => {
      if (subscription) {
        subscription.unsubscribe()
      }
    });
  }

  updateSearch(search: string): void {
    this.searchStream.next(search);
  }

}
