import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { of, Subscription, BehaviorSubject, Observable } from 'rxjs';
import { catchError, map, switchMap, shareReplay, tap } from 'rxjs/operators';

import { CardDetails, CardDetailsService } from '@bmi/gravity-services';
import { watchParams } from '@bmi/utils';

@Component({
  selector: 'gravity-card-data-viewer-shim',
  template: `
    <ui-observable-loader [source]="currentCardSource | async">
      <ng-template let-cardDetails>
        <gravity-card-viewer *ngIf="cardDetails" [cardDetails]="cardDetails"></gravity-card-viewer>
      </ng-template>
    </ui-observable-loader>
  `,

  styles: [`
    ui-loading-container {
      min-height: 50vh;
    }
  `]
})
export class CardDataViewerShimComponent implements OnInit, OnDestroy {
  constructor(
    private activatedRoute: ActivatedRoute,
    private cardDetailsService: CardDetailsService
  ) { }

  currentCardSource = new BehaviorSubject<Observable<CardDetails>>(undefined);
  private subscription = new Subscription();

  ngOnInit() {
    this.subscription.add(
      watchParams(this.activatedRoute, 'boardId', 'cardId').subscribe(({boardId, cardId}) => {
        this.currentCardSource.next(this.cardDetailsService.get(boardId, cardId, {throwErrors: true}));
      })
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
