import { Component } from '@angular/core';
import { Observable, defer, combineLatest, BehaviorSubject } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { GravityUrlService } from '@bmi/gravity-services';
import { HttpClient } from '@angular/common/http';
import { ObjectMap, Option, getPreference, setPreference } from '@bmi/utils';
import { Module, ModuleService } from '@bmi/core';
import { SimpleCloseDialogService } from '@bmi/ui';
import orderBy from 'lodash-es/orderBy';

export interface AppDescription {
  id?: string;
  name: string;
  description: string;
}

@Component({
  templateUrl: './root-home.component.html',
  styleUrls: ['./root-home.component.scss']
})
export class RootHomeComponent {
  selectedId: string = null;
  selectedModule: Observable<Module> = null;
  readonly preferenceId = 'module-selection';
  pageOptions: Option[] = [];
  selectedTitle: string = null;

  triggerFavoritesChanged = new BehaviorSubject<void>(undefined);

  appModules = combineLatest([
    defer(() => this.moduleService.getModuleSummaries()),
    this.triggerFavoritesChanged
  ]).pipe(
    map(([modules,]) => {
      const favoriteModules = this.getFavoriteModules();
      return modules.map(mod => ({
        ...mod,
        favorite: !!favoriteModules[mod.id]
      }));
    }),
    map(modules => orderBy(modules, [m => m.favorite, m => m.name], ['desc', 'asc'])),
    shareReplay(1)
  );

  constructor(
    private gravityUrlService: GravityUrlService,
    private httpClient: HttpClient,
    private closeDialogService: SimpleCloseDialogService,
    private moduleService: ModuleService
  ) {}

  /**
   * bypass the parent's routerLink action. But still close other dialogs.
   */
  stopPropagation(ev: Event) {
    ev.stopPropagation();
    this.closeDialogService.triggerClose();
  }

  visitModule(moduleId: string) {
    window.open(`/${moduleId}`);
  }

  setFavorite(moduleId: string, isFavorite: boolean) {
    setPreference('favoriteModules', moduleId, isFavorite);
    this.triggerFavoritesChanged.next();
  }

  private getFavoriteModules() {
    return getPreference('favoriteModules') as ObjectMap<string>;
  }
}
