import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { EMPTY, Observable, of, throwError, Subscription } from 'rxjs';
import { catchError, distinctUntilChanged, map, switchMap, tap } from 'rxjs/operators';

import { Rule, RulesService } from '@bmi/gravity-services';
import { RuleEditorComponent } from '../../rule-builder/rule-editor/rule-editor.component';
import { generateRuleHelp } from '../../rule-builder/rule-editor/rule-editor.help';

import { HelpService } from '../../components/help/help.service';

@Component({
  selector: 'gravity-admin-rule-builder-shim',
  template: `
    <ui-loading-container [isLoading]="!!loadingMessage" [loadingMessage]="loadingMessage" [hasError]="!!errorMessage" [errorMessage]="errorMessage">
      <ng-template>
        <h1>{{pageTitle}}</h1>
        <gravity-rule-editor *ngIf="rule" [rule]="rule" (ruleTypeChanged)="updateHelp($event)"></gravity-rule-editor>

        <button
          type="button"
          class="btn btn--primary"
          [disabled]="!isValid"
          (click)="saveClicked()"
          data-cy="rule-builder-save-button"
        >
          Save
        </button>
      </ng-template>
    </ui-loading-container>
  `,
  styles: [`
    ui-loading-container {
      min-height: 50vh;
    }
    body.gra :host ::ng-deep h2 {
      margin: .83em 0;
    }
  `]
})
export class RuleBuilderShimComponent implements OnInit {
  @ViewChild(RuleEditorComponent) ruleEditor: RuleEditorComponent;

  public rule: Rule;
  public loadingMessage: string = null;
  public errorMessage: string = null;

  private subscriptions: Subscription[] = [];

  get pageTitle() {
    return (this.rule && this.rule.id) ? this.rule.name : 'New Rule';
  }

  get isValid() {
    return this.ruleEditor && this.ruleEditor.isFormValid;
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private ruleService: RulesService,
    private helpService: HelpService,
  ) { }

  ngOnInit() {
    this.updateHelp('COMPULSORY');
    this.subscriptions.push(this.route.paramMap.pipe(
      map(p => [p.get('boardId'), p.get('ruleId')]),
      distinctUntilChanged(),
      tap(() => this.loadingMessage = 'Loading Rule'),
      switchMap(([boardId, ruleId]) => this.loadRule(boardId, ruleId)),
      catchError((error: Error) => {
        this.rule = null;
        this.errorMessage = error.message;
        return EMPTY;
      })
    ).subscribe(rule => {
      this.rule = rule;
      this.loadingMessage = null;
      this.errorMessage = null;
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
    this.helpService.display(null);
  }

  loadRule(boardId: string, ruleId: string | null): Observable<Rule> {
    if (!boardId) {
      return throwError(new Error('Cannot load rule: Board not specified'));
    } else if (!ruleId) {
      return of(new Rule('', '', boardId));
    } else {
      return this.ruleService.getRule(boardId, ruleId);
    }
  }

  updateHelp(ruleType: string) {
    this.helpService.display(generateRuleHelp(ruleType || 'COMPULSORY'))
  }

  saveClicked() {
    this.subscriptions.push(this.ruleEditor.save().subscribe(savedRule => {
      this.router.navigate(['/board', savedRule.boardId]);
    }));
  }
}
