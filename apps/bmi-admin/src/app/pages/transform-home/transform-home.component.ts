import { Component, OnInit } from '@angular/core';
import { TransformService } from '../../transform-editor/transform.service';
import { TransformDetail, TransformConfig } from '@bmi/gravity-services';
import { sortBy } from 'lodash-es';

@Component({
  templateUrl: './transform-home.component.html',
  styleUrls: ['./object-home.scss', '../widget-home/widget-home.component.scss'],
})
export class TransformHomeComponent implements OnInit {

  transforms: TransformDetail[];
  selectedTransform: TransformDetail;
  selectedConfig: TransformConfig = null;
  search = '';
  // readonly transformTypes = ['pivot', 'transpose',];

  constructor(
    private transformService: TransformService,
  ) { }

  get filteredTransforms(): TransformDetail[] {
    return this.transforms.filter((transform: TransformDetail) => {
      return (
        !this.search
        || transform.title.toLocaleLowerCase().includes(this.search)
        // || transform.type.toLocaleLowerCase().includes(this.search)
      );
    });
  }

  filteredTransformsByType(type: string): TransformDetail[] {
    return this.filteredTransforms.filter(
      (transform: TransformDetail) => transform.type === type
    );
  }

  typeHasTransforms(type: string): boolean {
    return !!this.filteredTransformsByType(type).length;
  }

  ngOnInit() {
    this.loadTransformList();
  }

  loadTransformList(): void {
    this.transformService.getTransformsList().subscribe(

      (transforms: TransformDetail[]) =>
        this.transforms = sortBy(transforms, (t) => t.title)
    );
  }

  selectTransform(transform: TransformDetail): void {
    this.selectedTransform = transform;
    this.selectedConfig = null;
    this.transformService.get(transform.id).subscribe(
      (config: TransformConfig) => this.selectedConfig = config
    );
  }

  isSelected(transform: TransformDetail): boolean {
    return (this.selectedTransform && transform.id === this.selectedTransform.id);
  }

  closePreview(): void {
    this.selectedTransform = null;
    this.selectedConfig = null;
  }

  updateSearch(search: string): void {
    this.search = search.toLocaleLowerCase().trim();
  }

}
