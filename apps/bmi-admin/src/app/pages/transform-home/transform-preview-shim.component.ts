import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from '@angular/router';
import { TransformService } from '../../transform-editor/transform.service';
import { TransformConfig } from '@bmi/gravity-services';

@Component({
  selector: 'gravity-transform-preview-shim',
  template: `
<idp-transform-preview [show]="true" [config]="config"></idp-transform-preview>
  `,
})
export class TransformPreviewShimComponent implements OnInit {

  config: TransformConfig;

  constructor(
    private activatedRoute: ActivatedRoute,
    private transformService: TransformService,
  ) { }

  ngOnInit() {
    const id: string = this.activatedRoute.snapshot.paramMap.get('transformId');
    this.transformService.get(id).subscribe((c: TransformConfig) => this.config = c);
  }

}
