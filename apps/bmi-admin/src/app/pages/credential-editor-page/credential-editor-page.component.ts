import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, BehaviorSubject, Subscription, of } from 'rxjs';
import { switchMap, shareReplay, take } from 'rxjs/operators';
import { CredentialService, Credential } from '@bmi/gravity-services';
import { TypedFormControl } from '@bmi/ui';
import { watchParams } from '@bmi/utils';

import { AdminAppNavService } from '../../admin-app-nav.service';


@Component({
  templateUrl: './credential-editor-page.component.html',
})
export class CredentialEditorPageComponent implements OnInit, OnDestroy {
  credentialObservable = new BehaviorSubject<Observable<Credential>>(undefined);

  originalCredential: Credential = undefined;
  formControl = new TypedFormControl<Credential>(undefined);

  private subscription = new Subscription();

  constructor(
    private activatedRoute: ActivatedRoute,
    private credentialService: CredentialService,
    private appNav: AdminAppNavService
  ) {}

  ngOnInit() {
    watchParams(this.activatedRoute, 'credentialId').subscribe(({credentialId}) => {
        this.credentialObservable.next(this.getOrCreateCredential(credentialId));
    });

    this.subscription.add(this.credentialObservable.pipe(
      switchMap(obs => obs || of(<Credential>undefined))
    ).subscribe(credentialToEdit => {
      this.formControl.setValue(credentialToEdit, {emitEvent: false});
      this.originalCredential = credentialToEdit;
    }));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getOrCreateCredential(credentialId: string | null): Observable<Credential> {
    if (credentialId) {
        return this.credentialService.get(credentialId, {invalidateNow: true}).pipe(shareReplay(1), take(1));
    } else {
      return of(<Credential>{
        id: '',
        isNew: true,
        metadata: null,
        name: '',
        description: '',
        resource: '',
        method: '',
        requestMethod: '',
        identifier: '',
        secret: '',
        responseFields: {},
        permissions: {}
      });
    }
  }

  redir() {
    this.appNav.integrationHome.navigate();
  }
}
