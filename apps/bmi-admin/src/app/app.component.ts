import { Component, OnInit, HostListener, ViewContainerRef } from '@angular/core';

import { Subscription, combineLatest } from 'rxjs';

import { GravityService, GravityConfigService } from '@bmi/gravity-services';
import { tap } from 'rxjs/operators';
import { ModalService, SimpleCloseDialogService } from '@bmi/ui';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss', './gra-global-overrides.scss'],
})
export class AppComponent implements OnInit {
  subscriptions: Subscription[] = [];

  isLoading = true;
  errorMessage = null;

  constructor(
    private gravityService: GravityService,
    private gravityConfigService: GravityConfigService,
    private closeDialogService: SimpleCloseDialogService,
    viewContainerRef: ViewContainerRef,
    modalService: ModalService
  ) {
    modalService.setViewContainer(viewContainerRef);
  }

  ngOnInit() {
    this.isLoading = true;
    this.subscriptions.push(
      combineLatest([
        this.doGravityBootstrap()
        // Add more loading tasks here, they should emit when finished, or raise
        // an error.
      ]).subscribe(() => {
        this.isLoading = false;
        this.errorMessage = null;
      }, error => {
        this.isLoading = false;
        this.errorMessage = `Error: ${error.message}`;
      })
    );
  }

  doGravityBootstrap() {
    return this.gravityService.getBoardConfigs().pipe(
      tap(configs => {
        console.log(`boardConfigs: `, configs.length)
        for (const config of Object.values(configs)) {
          try {
            this.gravityConfigService.registerBoard(config, true);
          } catch (error) {
            console.warn(`Register Board Failed: ${error.message}`);
          }
        }
      })
    );
  }

  @HostListener('document:click')
  strayClickCaught() {
    this.closeDialogService.triggerClose();
  }
}
