import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedUiModule } from '@bmi/ui';
import { GravityModule } from '@bmi/gravity-services'

import { CardViewerComponent } from './card-viewer/card-viewer.component';
import { DataViewerComponent } from './data-viewer.component';
import { DataViewerRoutesService } from './data-viewer-routes.service';
import { ConfigViewerComponent } from './config-viewer/config-viewer.component';
import { TransformViewerComponent } from './transform-viewer/transform-viewer.component';

import { AdminComponentsModule } from '../components/admin-component.module';

@NgModule({
  declarations: [
    CardViewerComponent,
    DataViewerComponent,
    ConfigViewerComponent,
    TransformViewerComponent,
  ],

  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    SharedUiModule,
    GravityModule,
    AdminComponentsModule,
  ],

  providers: [
    // This routes service helps components within this module get routing
    // commands within the application. This is a dummy implementation and it
    // should be overridden in the app module to provide proper routes.
    DataViewerRoutesService
  ],

  exports: [
    CardViewerComponent,
    DataViewerComponent,
    ConfigViewerComponent,
    TransformViewerComponent,
  ]
})
export class DataViewerModule { }
