import { Component, Input } from '@angular/core';
import { TransformConfig, TransformerConfig } from '@bmi/gravity-services';
import { ObjectMap } from '@bmi/utils';

@Component({
  selector: 'gravity-transform-viewer',
  templateUrl: './transform-viewer.component.html',
  styleUrls: ['../object-viewer.scss'],
})
export class TransformViewerComponent {

  @Input() transformConfig: TransformConfig;
  Object = Object;

  getTransformerDisplay(transformer: TransformerConfig): ObjectMap<number | string | string[]> {
    return {
      transformer: transformer.transformer,
      order: transformer.order,
      ...transformer.configuration,
    }
  }

  formatValue(key: string, value: any): Date | any {
    if ((key.includes('modified') || key === 'created')
      && typeof value === 'number') {
      return new Date(value);
    }
    return value;
  }

}
