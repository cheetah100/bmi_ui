import {
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
  Input,
  SimpleChanges,
  ComponentFactoryResolver,
  Injector
} from '@angular/core';

import { Subject, Observable, combineLatest, of, Subscription, BehaviorSubject, forkJoin, EMPTY } from 'rxjs';
import { distinctUntilChanged, map, flatMap, switchMap, tap} from 'rxjs/operators';

import {
  BoardConfig,
  GravityConfigService,
  GravityService,
  Field,
  FieldPath,
  BoardPathResolver,
  BoardFieldPath,
  GravityApiBoard,
  GravityApiTemplate,
  Card,
  CardCacheService,
  CardData,
  GravityCardAlertService,
  GravityApiCardEvent, BoardService
} from '@bmi/gravity-services';

import {
  TableColumn,
  TableModel,
  SimpleTableModel,
  Formatters,
  InMemoryTableController,
  DataTableAction,
  TableRow,
  ConfirmationModalService, ModalService, ButtonToolbarTableCell
} from '@bmi/ui';

import { escapeMarkdown } from '@bmi/utils';

import { DataViewerRoutesService } from './data-viewer-routes.service';
import { FocusPanelService } from '../components/focus-panel/focus-panel.service';

import dedent from 'ts-dedent';
import sortBy from 'lodash-es/sortBy';
import { CardEditorModalService } from '@bmi/core';
import { PhasePickerComponent } from '../../../../../libs/gravity-services/src/lib/ui/phase-picker/phase-picker.component';
import { AdminAppNavService } from '../admin-app-nav.service';


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gravity-data-viewer',
  templateUrl: './data-viewer.component.html',
  styleUrls: ['./data-viewer.component.scss']
})
export class DataViewerComponent implements OnInit, OnChanges, OnDestroy {

  @Input() boardId: string;
  @Input() viewId: string = null;
  @Input() search = '';

  public tableModel: TableModel = null;
  private tableController: InMemoryTableController;

  private boardSubject = new Subject<string>();
  private triggerRefresh = new BehaviorSubject<void>(null);
  private subscription = new Subscription();
  private boardConfig: BoardConfig ;

  public isLoading = true;

  get canEditCards() {
    return this.boardId && !this.viewId;
  }

  constructor(
    private gravityConfigService: GravityConfigService,
    private gravityService: GravityService,
    private cardCacheService: CardCacheService,
    private routesService: DataViewerRoutesService,
    private cardEditorModal: CardEditorModalService,
    private confirmationService: ConfirmationModalService,
    private cardAlertService: GravityCardAlertService,
    private focusPanelService: FocusPanelService,
    private modalService: ModalService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private injector: Injector,
    private boardService: BoardService,
    private appNav: AdminAppNavService,
  ) { }

  ngOnInit() {
    this.subscription.add(combineLatest([
      this.boardSubject.pipe(distinctUntilChanged()),
      this.triggerRefresh
    ]).pipe(
      tap(() => this.isLoading = true),
      switchMap(([boardId]) => {
        const templateObservable = (
          this.viewId
            ? this.gravityService.getViewTemplate(boardId, this.viewId)
            : this.gravityConfigService.watchBoardConfig(boardId)
        )
        const cardsObservable = this.cardCacheService.search({
          boardId: boardId,
          viewId: this.viewId || undefined,
        });
        return combineLatest(
          templateObservable,
          cardsObservable,
        );
      }),
    ).subscribe(([boardConfig, cards]) => {
      this.boardConfig = boardConfig as BoardConfig;
      this.initTable(boardConfig, cards);
    }));

    this.boardSubject.next(this.boardId);
  }

  initTable(boardConfig: BoardConfig | GravityApiTemplate, cards: CardData[]) {
    // We want to present the table with the real fields first, followed by the
    // metadata fields; BUT we want the id to be first.
    const fields = sortBy(Object.values(boardConfig.fields), f => f.index, f => f.name);
    const tableFields = [
      ...fields.filter(f => !f.isMetadata),
      ...fields.filter(f => f.isMetadata && f.id !== 'id')
    ];
    // only add id field for board config: view template might not have it
    const idField = fields.find(f => f.id === 'id' && f.isMetadata);
    if (idField) {
      tableFields.unshift(idField);
    }

    const hasActionsColumn = this.canEditCards;

    const columns = [];

    if (hasActionsColumn) {
      // Setting the width to 'max-content' will auto-size the column
      columns.push(new TableColumn('Actions', false, 'max-content'));
    }

    for (const field of tableFields) {
      const formatter = getTableCellFormatter(field);
      const column = new TableColumn(field.label || field.name, false, null, formatter);
      let columnBoardId = boardConfig.id;
      if (this.viewId) {
        const fieldPath = this.getBoardFieldPath(boardConfig.id, field.name);
        columnBoardId = fieldPath.boardId;
      }

      if (field.id === 'id') {
        column.formatter = Formatters.link(cardId => this.routesService.cardDataView(columnBoardId, cardId));
      } else if (field.optionlist) {
        column.formatter = Formatters.link(cardId => this.routesService.cardDataView(field.optionlist, cardId));
      } else if (
        this.viewId
        && columnBoardId !== boardConfig.id
        && ['_id', 'id'].includes(field.name.split('.').pop())
      ) {
        column.formatter = Formatters.link(cardId => this.routesService.cardDataView(columnBoardId, cardId));
      }

      columns.push(column);
    }

    const tableModel = new TableModel(columns);
    const tableController = new InMemoryTableController(tableModel);

    tableModel.onTableUpdate.pipe(
      switchMap(() => tableController.refresh())
    ).subscribe();

    for (const card of cards) {
      const row = tableFields.map(f => f.isMetadata ? card[f.id] : card.fields[f.id]);
      if (hasActionsColumn) {
        row.unshift(<ButtonToolbarTableCell>{
          type: 'button-toolbar',
          value: [
            {type: 'action', tooltip: 'Edit', icon: 'shared-ui-edit', invoke: () => this.editCard(card.board, card.id)},
            {
              type: 'menu',
              icon: 'cui-more',
              items: [
                {type: 'action', text: 'View Card', invoke: () => this.appNav.cardViewer(card.board, card.id).navigate() },
                {type: 'separator'},
                {type: 'action', text: 'Edit', icon: 'shared-ui-edit', invoke: () => this.editCard(card.board, card.id)},
                {type: 'action', text: 'Update Phase', icon: 'cui-animation', invoke: () => this.updatePhase(card.board, card.id)},
                {type: 'separator'},
                {type: 'action', text: 'Add card watch', icon: 'shared-ui-eye', invoke: () => this.addCardWatcher(card.board, card.id)},
                {type: 'separator'},
                {type: 'action', text: 'Delete Card', icon: 'cui-trash', invoke: () => this.deleteCard(card.board, card.id)},
              ]
            },
            card.alerted ? {type: 'action', tooltip: 'View Card Alerts', icon: 'warning', invoke: () => this.showCardAlerts(card.board, card.id)} : null,
          ].filter(x => !!x)
        });
      }

      tableController.addRow(new TableRow(tableModel, row, undefined));
    }

    tableModel.pagination.enabled = true;
    tableModel.pagination.rowsPerPage = 20;

    tableModel.sortByColumn(tableModel.columns[hasActionsColumn ? 1 : 0]);
    tableModel.update();

    this.tableModel = tableModel;
    this.tableController = tableController;
    this.isLoading = false;
  }

  getBoardFieldPath(rootBoardId: string, path: string): BoardFieldPath {
    const boardPathResolver = new BoardPathResolver(rootBoardId, this.gravityConfigService);
    return boardPathResolver.resolve(path.replace(/\._id$/, '.id'));
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.boardId) {
      this.boardSubject.next(this.boardId);
    }
    if (changes.search && !!this.tableModel) {
      this.tableModel.filters.search = this.search;
      this.tableController.refresh();
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  scrollToTop() {
    window.scroll(0, 0);
  }

  editCard(boardId: string, cardId: string) {
    this.subscription.add(this.cardEditorModal.editCard(boardId, cardId).subscribe((result) => {
      if (result.action === 'yes') {
        this.refreshBoard()
      }
    }));
  }

  deleteCard(boardId: string, cardId: string) {
    this.subscription.add(this.cardEditorModal.deleteCard(boardId, cardId).subscribe(result => {
      if (result.action === 'yes') {
        this.refreshBoard();
      }
    }));
  }

  addCardWatcher(boardId: string, cardId: string) {
    // this.focusPanelService.addCardWatcher(boardId, cardId);
  }

  createCard() {
    this.subscription.add(this.cardEditorModal.createCard(this.boardId)
      .subscribe((result) => {
        if (result.action === 'yes') {
          this.refreshBoard()
        }
      }));
  }

  refreshBoard() {
    this.cardCacheService.refreshBoard(this.boardId, true);
    this.triggerRefresh.next();
  }

  showCardAlerts(boardId: string, cardId: string) {
    this.cardAlertService.getAlertsForCard(boardId, cardId).pipe(
      switchMap(alerts => {
        const sortedAlerts = sortBy(alerts, a => a.occuredTime).reverse();

        return this.confirmationService.show({
          content: dedent`
            ## Card Alerts for ${cardId}

            ${sortedAlerts.map(a => summarizeAlert(a)).join('\n\n------\n\n')}

            Dismiss all alerts?
          `,
          buttonYesText: 'Dismiss alerts',
          buttonCancelText: 'No, leave alerts'
        }).pipe(map(() => alerts));
      }),
      switchMap(alertsToCancel => {
        if (alertsToCancel.length > 0) {
          return forkJoin(alertsToCancel.map(a => this.cardAlertService.dismissCardAlert(boardId, cardId, a.uuid)));
        } else {
          return of(null);
        }
      })
    ).subscribe(() => this.refreshBoard());
  }

  updatePhase(boardId: string, cardId: string): void {
    const modalData = {
      boardId,
      cardId
    }

    // Get current phase value
    this.modalService.open({
      title: 'Phase Picker',
      content: PhasePickerComponent,
      injector: this.injector,
      size: 'small',
      data: modalData
    }).subscribe(result => {
      if ( result?.action === 'save' && result?.data ) {
        this.savePhase(boardId, cardId, result.data)
      }
    })
  }

  private savePhase(boardId:string, cardId:string, newPhase:string) {
    this.boardService.fetchById(boardId, cardId).subscribe(
      card => {
        card.phase = newPhase;
        this.boardService.moveCard(boardId, cardId, newPhase).subscribe(
          () => this.refreshBoard()
        )
      }
    )
  }
}


function summarizeAlert(event: GravityApiCardEvent): string {
  console.log(event);
  const occurredAt = new Date(event.occuredTime).toLocaleString();
  return dedent`
    ### ${escapeMarkdown(event.user)} at ${escapeMarkdown(occurredAt)}

    <pre>${event.detail}</pre>
  `;
}

export function getTableCellFormatter(field: Field) {
  switch(field.type) {
    case 'DATE':
      return Formatters.date;
    case 'MAP':
      return Formatters.map;
    default:
      return Formatters.defaultFormatter;
  }
}
