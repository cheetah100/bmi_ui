import { Component, Input,  } from '@angular/core';
import { CardDetails, } from '@bmi/gravity-services';



@Component({
  selector: 'gravity-card-viewer',
  templateUrl: './card-viewer.component.html',
  styleUrls: ['./card-viewer.component.scss']
})
export class CardViewerComponent {
  @Input() public cardDetails: CardDetails;

  get card() {
    return this.cardDetails.card;
  }
}
