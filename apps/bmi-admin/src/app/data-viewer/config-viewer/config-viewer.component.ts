import { Component, Input, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { BoardConfig } from '@bmi/gravity-services';
import { ObjectMap } from '@bmi/utils';

@Component({
  selector: 'gravity-config-viewer',
  templateUrl: './config-viewer.component.html',
  styleUrls: ['../object-viewer.scss', './config-viewer.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfigViewerComponent {

  /**
   * Gives a default view, with expandable nested elements, for a json config.
   * Allows the user to switch to a json code block display
   */

  @Input() config: any;
  @Input() pageTitle: string = 'Config';

  toggles = new Map<string, boolean>();
  @Input() viewJson = false;

  toggleJson() {
    this.viewJson = !this.viewJson;
  }

  isObject(object: any): boolean {
    return !!object && typeof object === 'object' && !!Object.keys(object).length;
  }

  sortKeys(object): string[] {
    const keys: string[] = Object.keys(object);
    return [
      ...keys.filter((key: string) => !this.isObject(object[key])).sort(),
      ...keys.filter((key: string) => this.isObject(object[key])).sort(),
    ]
  }

  formatValue(key: string, value: any): string | number | Date {
    if ((key.includes('modified') || key === 'created')
      && typeof value === 'number') {
      return new Date(value);
    }
    if (!!value && typeof value === 'object') {
      return '';
    }
    return value;
  }

  toggleOpen(key: string): void {
    const value: boolean = this.toggles.get(key) || false;
    this.toggles.set(key, !value);
  }

  isOpen(key: string): boolean {
    return this.toggles.get(key) || false;
  }

  copyToClipboard() {
    const textArea = document.createElement('textarea');
    textArea.value = JSON.stringify(this.config, null, 2);
    document.body.append(textArea);
    textArea.select();
    document.execCommand('copy');
    textArea.remove();
  }
}
