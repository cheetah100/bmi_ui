import { Injectable } from '@angular/core';

export type RoutingCommand = string | any[] | null;

/**
 * This service provides routes for linking within the data viewer components.
 *
 * This implementation of the service only returns null. The application should
 * provide its own instance, extending or replacing this one which returns
 * application-specific routes.
 *
 * This separation means that the data viewer doesn't know about the
 * application's URL layout at all.
 *
 * If a method in this class returns null, it should be interpreted as no URL
 * available. Ideally the component shouldn't show a link here, but if it does
 * the behaviour will be harmless.
 *
 * Null or undefined values *within* a route command can cause problems when
 * displaying the routes. Implementations of the service should guard against
 * bad values and return an appropriate valid routing command.
 */
@Injectable()
export class DataViewerRoutesService {
  public boardDataView(boardId: string): RoutingCommand {
    return null;
  }

  public boardEditView(boardId: string): RoutingCommand {
    return null;
  }

  public cardDataView(boardId: string, cardId: string): RoutingCommand {
    return null;
  }
}
