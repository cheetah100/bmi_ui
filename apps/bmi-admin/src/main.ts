import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

import { GRAVITY_PARAMETERS, GravityParameters, GravityApiUser } from '@bmi/gravity-services';
import { BMI_APP_INIT_PARAMETERS, BmiAppInitParameters } from '@bmi/gravity-services';

if (environment.production) {
  enableProdMode();
} else {
  localStorage.setItem('devCardCache', 'true');
}


platformBrowserDynamic([
  {
    provide: GRAVITY_PARAMETERS,
    useValue: <GravityParameters>{
      baseAppId: 'admin', // TODO: what does this even mean here?
      baseUrl: '/gravity/spring',
      boardConfigs: {},
    }
  },
  {
    provide: 'USER_PROVIDER', useValue: <Partial<GravityApiUser>>{
      id: 'idp-nprod-dev',
      teams: {'administrators': ''}
    }
  },
  {
    provide: BMI_APP_INIT_PARAMETERS,
    useValue: <BmiAppInitParameters>{
      ...window['APP_CONFIG'] || {}
    }
  },
]).bootstrapModule(AppModule, {
  preserveWhitespaces: true
}).catch(err => console.error(err));
