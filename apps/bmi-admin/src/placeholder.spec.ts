describe('Test Placeholder', () => {
  // This shouldn't be necessary but for unknown reasons, Karma isn't respecting
  // the options to prevent the tests from failing for empty test suites, or
  // on errors. This causes the Jenkins build to fail, so I'm including this
  // test case just to make sure it passes.
  it('should prevent the tests from failing due to no test suite', () => {
    expect(true).toBeTruthy();
  });
})
