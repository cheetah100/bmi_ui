module.exports = {
  name: 'bmi-frontend-server',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/bmi-frontend-server'
};
