import * as express from 'express';
import * as cors from 'cors';
import * as fs from 'fs';
import * as helmet from 'helmet';
import * as path from 'path';
import * as httpProxy from 'http-proxy-middleware';
import * as args from 'commander';

import compression from 'compression';

import { PingClientAuthToken, loadSecretsConfig, makeGravityProxyConfig } from '@bmi/ping-auth-helpers';

import { hostAngularApp, AngularAppParameters } from './app/host-angular-app';

const app = express();

// Disable CORS altogether. The applications served here should only be making
// same-origin requests via a proxy, so there's no need for CORS on any of these
// endpoints.
app.use(cors({
  origin: false
}));

// Set the X-Frame-Options header to provide clickjacking protection
app.use(helmet.frameguard({
  action: "deny",
}));

app.use(compression());

args
  .option('--proxy-secrets <configfile>', 'Set up a Gravity proxy using the provided secrets file')
  .option('--gravity-url <url>', 'Set up a reverse proxy to this Gravity instance')
  .option('--oauth-token-url <url>', 'Overrides the SSO token URL, by default this will be cloudsso-test')
  .parse(process.argv);

// Search for the compiled BMI application. To simplify development, we also
// support loading the app from the parent directory (where it would be in the
// compiled Angular workspace)

const APP_SEARCH_DIRS = ['./dist', './dist/apps', '../../../dist/apps/'].map(dir => path.join(__dirname, dir));


function findAppDir(appName: string) {
  const searchDirectories = APP_SEARCH_DIRS
    .map(dir => path.join(dir, appName))

  const appDirectory = searchDirectories.find(dir => fs.existsSync(dir + '/index.html'));

  if (!appDirectory) {
    console.error(`App '${appName}' not found!`);
    console.error(`We looked in:\n\n ${searchDirectories.join(',\n ')}\n but could not find index.html`)
    process.exit(1);
    throw new Error(`App not found: ${appName}`);
  } else {
    return appDirectory;
  }
}


function mountApp(baseUrl: string, appName: string, config?: Partial<AngularAppParameters>) {
  app.use(baseUrl, hostAngularApp({
    baseUrl: baseUrl,
    appDirectory: findAppDir(appName),
    ...config || {}
  }));
}

// TODO Please specify
const lifecycle = "local"; // was process.env["LIFE_CYCLE_ENV_VAR"] || "local";
let revisionContent;
const revisionInfoFile = path.join(__dirname, 'revision.json');
if (fs.existsSync(revisionInfoFile)) {
  revisionContent = {
    lifecycle: lifecycle,
    ...JSON.parse(fs.readFileSync(revisionInfoFile).toString()),
  };
} else {
  console.log('No revision.json found');
  revisionContent = {
    lifecycle: lifecycle,
    "git.build.time": new Date().toISOString()
  };
}


// If enabled, the server can provide a reverse-proxy back to a Gravity
// instance. This can either by done by providing a Gravity URL, or a proxy
// secrets file. If a secrets file is used, client oauth will generate an auth
// token allowing the app to be used outside a CAE environment.
if (args.proxySecrets) {
  const config = loadSecretsConfig(args.proxySecrets);
  // TODO Please specify
  const tokenUrl = args.oauthTokenUrl || 'DEFAULT_TOOKEN_URL';
  const gravityUrl = args.gravityUrl || config.gravityUrl;

  if (!gravityUrl) {
    console.error('Must provide gravityUrl in secrets config or --gravity-url');
    process.exit(1);
  }

  console.warn('------- !! Using client auth for Gravity Proxy !! -------');
  console.warn('  DO NOT USE IN PRODUCTION! ');
  console.log(`Setting up Gravity Proxy on '/gravity' to ${gravityUrl} WITH CLIENT AUTH`);
  app.use('/gravity', httpProxy.createProxyMiddleware(
    makeGravityProxyConfig(gravityUrl, '/gravity', new PingClientAuthToken(tokenUrl, config))
  ));
} else if (args.gravityUrl) {
  const gravityUrl = args.gravityUrl;
  console.log(`Setting up Gravity Proxy on '/gravity' to ${gravityUrl}`);
  app.use('/gravity', httpProxy.createProxyMiddleware(makeGravityProxyConfig(gravityUrl, '/gravity')));
}

app.get('/revision', (req, res) => {
  res.header('Content-Type', 'application/json');
  res.send(JSON.stringify(revisionContent, undefined, 2));
});

mountApp('/admin', 'bmi-admin', {
  pageTitle: `BMI Admin ${lifecycle}`,
  appConfig: { gravityBaseUrl: '/gravity', lifecycle: lifecycle }
});

mountApp('/', 'legacy-spc', {
  pageTitle: `BMI ${lifecycle}`,
  appConfig: { gravityBaseUrl: '/gravity', lifecycle: lifecycle }
});



const port = 5000;
app.listen(port, () => console.log(`Node server BMI Frontend running on port ${port}`));
