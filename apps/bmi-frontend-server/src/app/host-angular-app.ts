import { Router, static as expressStatic} from 'express';
import cheerio from 'cheerio';


const fsp = require('fs').promises;


export interface AngularAppParameters {
  /**
   * The base URL where this app will be mounted. This gets injected into the
   * <base> element of the HTML.
   */
  baseUrl: string;

  /**
   * The directory on disk where the app assets are located. This should be the
   * directory with the index.html file.
   */
  appDirectory: string;

  /**
   * The initial value for the page's <title> element. This might be later
   * overriden by the app code.
   */
  pageTitle?: string;

  /**
   * An optional JSON serializable config that is included into the page as a
   * <script> element.
   */
  appConfig?: any;
}


export function hostAngularApp(conf: AngularAppParameters): Router {
  const router = Router();
  const baseUrl = !conf.baseUrl.endsWith('/') ? conf.baseUrl + '/' : conf.baseUrl;
  const appDirectory = conf.appDirectory;

  const templatedHtml = fsp.readFile(`${appDirectory}/index.html`)
    .then(file => {
      const indexFileHtml = file.toString();
      const $ = cheerio.load(indexFileHtml);

      $('base').attr('href', baseUrl);
      if (conf.pageTitle) {
        $('head title').html(conf.pageTitle);
      }

      const serializedConfig = JSON.stringify(conf.appConfig || {});

      $('head').append($(`
        <script>
          window.APP_CONFIG = JSON.parse('${serializedConfig}');
        </script>
      `));

      return $.html();
    })


  const serveAppHandler = (req, res) => {
    templatedHtml.then(indexPage => res.send(indexPage));
  };

  // We want to support modern pushState style of navigation, but this means that
  // the browser may request a page that doesn't actually exist. For this reason
  // we want to serve up the main index.html to any request that isn't explicitly
  // an asset file.
  //
  // We want control over the file that gets served up (e.g. to preload data) so
  // we first need to match the index page (to avoid it getting sent as an asset)
  router.get(['/', '/index.html'], serveAppHandler);

  // Also serve up any assets and scripts
  router.use(expressStatic(appDirectory, {
    maxAge: '28 days'
  }));

  // And for anything else, send the main page again.
  router.get('/*', serveAppHandler);

  return router;
}

