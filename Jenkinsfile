pipeline {
  agent any

  tools {
    jdk 'JDK1.8.0'
    nodejs 'node-v10.16.0'
  }


  environment {
    npm_config_cache = 'npm-cache'
    NPM_CONFIG_USERCONFIG = ".npmrc"
    CYPRESS_CACHE_FOLDER = '.cypress-cache'
    DISABLE_DEV_PROXY = 1
    GENERATE_CYPRESS_COVERAGE_REPORT = 1
  }


  stages {
    stage ('Pre-Build') {
      steps {
        notifyBuildStart()
      }
    }

    stage('Build') {
      agent {
        docker {
          image 'circleci/node:10-browsers'
          args '-v /apps/dftjenkins/local:/apps/dftjenkins/local'
          reuseNode true
        }
      }

      steps {
        // Clean any existing builds from the workspace
        sh 'rm -fR dist/'
        sh 'npm ci'

        sh 'npm run build-all:prod'

        // Prepare the bmi-frontend-server for deployment. The revision.json
        // file contains info about the current git version/branch so that
        // this can be exposed via the API
        sh 'node tools/make-revision-file.js dist/apps/bmi-frontend-server/revision.json'
        sh 'cd dist/apps/bmi-frontend-server && npm ci && cd ../../..'
      }
    }

    stage ('Push') {
      when { branch 'master' }
      steps {
        dockerBuild();

        // Authenticates with your remote Docker Repository, and pushes the value of "$DOCKER_PUSH_TAG",
        // which will exist if you used 'tagDocker' to tag your image, or set it manually. If you have done neither,
        // you can instead define your image using the 'image' parameter.
        // You can change the credentials used by using the 'authId' parameter.
        // The difference between this, and 'docker push $image', is that this handles 'docker login' for you.
        dockerPush()

        // Send Webex notification about docker push event status to the room defined in $SPARK_ROOM, using the
        // 'CoDE:ContainerHub' bot
        notifyDocker()
      }
    }

    stage ('QA/Deployment') {
      parallel {
        stage ('Static Security Scan') {
          steps {
            staticSecurityScan(sparkroomid: "$WEBEX_TEAMS_ROOM_ID", stackName: 'bmi-4e3f5ae10daa')
          }
        }

        stage ('Test/Sonar') {
          // Running with a custom agent here so that SonarQube has an up-to-date version of Node and
          // can actually run the Typescript rules.
          stages('Test and scan') {
            stage('Run tests') {
              agent {
                docker {
                  image 'circleci/node:10-browsers'
//                   image 'cypress/browsers:node14.16.0-chrome89-ff86'
                  args '-v /apps/dftjenkins/local:/apps/dftjenkins/local'
                  reuseNode true
                }
              }
              steps {
                // Added this comment to force another rebuild on the server
                // to see if this error still occurs
                // wrapper script does not seem to be touching the log
                sh 'npm run test'
              }
            }
//             stage("Run Cypress tests") {
//               agent {
//                 docker {
//                   image 'cypress/browsers:node14.16.0-chrome89-ff86'
//                   args '-v /apps/dftjenkins/local:/apps/dftjenkins/local'
//                   reuseNode true
//                 }
//               }
//               steps {
//                 sh 'mkdir -p $PWD/.config' // Make sure AppData directory, used by electron, exists
//                 sh 'XDG_CONFIG_HOME=$PWD/.config npx ng e2e bmi-admin-e2e --headless --browser chrome'
//                 sh 'sed  -i "s/SF:\\.\\./SF:apps/g" apps/bmi-admin-e2e/coverage/lcov.info'
//                 sh 'XDG_CONFIG_HOME=$PWD/.config npx ng e2e bmi-e2e --headless --browser chrome'
//                 sh 'sed  -i "s/SF:\\.\\./SF:apps/g" apps/bmi-e2e/coverage/lcov.info'
//               }
//             }
            stage('Sonar scan') {
              steps {

                // Prepare the Sonar properties file. This helper script will
                // scan through the source code for docstrings which flag
                // source files to be excluded from testing and builds up a
                // list of filenames to exclude.

                // The --on-branch switch, and the Bash variable magic gets a
                // "nice" name of the branch that Jenkins is building. This
                // will be put into the sonar properties as sonar.branch which
                // means we can scan branches without them affecting our main
                // code stats.
                sh 'node tools/sonarqube-config-helper --on-branch "${GIT_BRANCH#*/}" --output-sonar-properties >> sonar-project.properties'
                sonarScan('Sonar')
              }
            }
          }


          // Make test results visible in Jenkins UI if the install step completed successfully
          post {
            success {
              junit testResults: 'build/reports/**/TESTS-*.xml', allowEmptyResults: true
            }
          }
        }

        // You can use these stages if you would like to deploy to different dev environments depending on the current branch.
        // To use this, simply uncomment the blocks, and add the branch pattern (ANT style path glob). Make sure you remove the
        // "Deploy All" stage as well, or you will do the deployments twice.

        // stage ('Deploy Dev') {
        //   when { branch "feature/*" }
        //   steps {
        //       triggerSpinnakerDevDeployment(environments: ["dev"], secret: "2b6caae500756c935b8ea6ace231d882")
        //   }
        // }

        stage ('Deploy All') {
          when { branch 'master' }
          steps {
            // This step will automatically include the docker image stored in env $DOCKER_PUSH_TAG, or you can specify the image
            // parameter to this step to manually indicate the image.
            triggerSpinnakerDevDeployment(
                // The dev environments we are deploying to
                environments: ["dev"],
            )
          }
        }
      }
    }
  }

  post {
    always {
      notifyBuildEnd()
    }
  }
}
