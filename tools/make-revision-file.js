const fs = require('fs')
const fsp = fs.promises;
const git = require('simple-git/promise')();
const args = require('commander');

args
  .arguments('OUTPUT')
  .parse(process.argv);


async function main() {
  const outputFile = args.args[0] || 'revision.json';

  if (!outputFile) {
    throw new Error('Must provide an output file for revision info')
  }

  const log = (await git.log()).latest;
  const status = await git.status();

  const revisionData = {
    'git.branch': status.current,
    'git.commit.id': log.hash,
    'git.commit.message.short': log.message,
    'git.commit.time': log.date,
    'git.commit.user.name': log.author_name,
    'git.commit.user.email': log.author_email,
    'git.build.time': new Date().toISOString()
  };

  await fsp.writeFile(outputFile, JSON.stringify(revisionData, undefined, 2));
}


main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1)
  })
