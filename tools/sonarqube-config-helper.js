const shelljs = require('shelljs');
const fs = require('fs')
const fsp = fs.promises;
const commentParser = require('comment-parser');
const args = require('commander');

args
  .option('--output-exclusion-report', 'Produce a JSON report of exclusions')
  .option('--output-sonar-properties', 'Produce a sonar properties fragment')
  .option('--on-branch <branch>', 'Specify the branch in the config')
  .parse(process.argv);


async function main() {
  const filenames = shelljs.find(["{apps,libs}/**/*.ts"]).map(f => f);
  const sourceFiles = filenames.filter(f => !f.endsWith('.spec.ts'));

  const exclusions = [];

  for (const sourceFile of sourceFiles) {
    const fileContents = await fsp.readFile(sourceFile).then(b => b.toString());
    const testExclusion = await getTestExclusionInfo(sourceFile, fileContents);
    if (testExclusion) {
      exclusions.push(testExclusion);
    }
  }

  if (args.outputExclusionReport) {
    await process.stdout.write(JSON.stringify(exclusions, undefined, 2))
  } else if (args.outputSonarProperties) {
    if (exclusions.length > 0) {
      const joinedExclusions = exclusions.map(x => x.filename).join(',');
      process.stdout.write(`sonar.coverage.exclusions=${joinedExclusions}\n`);
    }
    if (args.onBranch) {
      process.stdout.write(`sonar.branch.name=${args.onBranch}\n`)
    }
  } else {
    console.error('Please choose an output mode like --output-sonar-properties')
  }
}



function getTestExclusionInfo(filename, contents) {
  const comments = matchDocComments(contents);
  for (const comment of comments) {
    for (const tag of comment.tags || []) {
      if (tag.tag.toLowerCase() === 'untested') {
        return {
          filename: filename,
          category: tag.type || 'other',
          description: tag.description
        };
      }
    }
  }
}


function matchDocComments(contents) {
  return (contents.match(/\/\*\*([\s\S]*?)\*\//g) || [])
    .map(comment => commentParser(comment))
    .reduce((acc, current) => acc.concat(current), []);
}


main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1)
  })
