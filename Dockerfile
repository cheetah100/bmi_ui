FROM node:10.7.0-alpine

#basic system hygeine
RUN apk update && apk upgrade

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY ./dist /usr/src/app/dist

# Run it!
EXPOSE 5000

CMD [ "node", "dist/apps/bmi-frontend-server/main.js", "--gravity-url", "http://bmi-gravity:8080/gravity" ]
