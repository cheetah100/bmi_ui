export interface GravityApiWidget {
  id: string;
  name: string;
  title: string;
  description: string;
  widgetType: string;
  applicationId: string;
  draggable?: boolean;
  group?: string;
  phase: string;
  width: number;
  height: number;

  fields: {[key: string]: any};
}
