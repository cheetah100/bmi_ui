
export type CardFields = {[fieldId: string]: any};

export interface CardData<TFields extends CardFields = CardFields> {
  id: string;
  board: string;
  phase: string;
  title?: string;
  template?: string;
  fields: TFields;
  color?: string;

  creator?: string;
  modifiedby?: string;
  created?: number;
  modified?: number;
  lock?: boolean | null;
  deleted?: boolean;
  alerted?: boolean;
}

export type GravityApiCardView = CardData<CardFields>;
