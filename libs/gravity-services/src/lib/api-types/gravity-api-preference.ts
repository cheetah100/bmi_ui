export interface PreferenceValue {
  [key: string]: any;
}

export interface Preference {
  id?: string;
  userId?: string;
  name: string;
  values: PreferenceValue;
}
