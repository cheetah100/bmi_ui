export interface GravityApiCondition {
  fieldName: string;
  operation: GravityApiConditionOperation;
  conditionType?: null | GravityApiConditionType;
  value: string;
}


export type GravityApiConditionOperation =
  'CONTAINS'
  | 'NOTCONTAINS'
  | 'EQUALTO'
  | 'NUMBEREQUALTO'
  | 'NOTEQUALTO'
  | 'NUMBERNOTEQUALTO'
  | 'GREATERTHAN'
  | 'GREATERTHANOREQUALTO'
  | 'LESSTHAN'
  | 'LESSTHANOREQUALTO'
  | 'ISNULL'
  | 'NOTNULL'
  | 'BEFORE'
  | 'AFTER'
  | 'AT'
  | 'COMPLETE'
  | 'INCOMPLETE'
  | 'ALERTS'
  | 'NOTPHASE'
  | 'IN'
  | 'NIN';


export type GravityApiConditionType =
  'PROPERTY'
  | 'TASK'
  | 'PHASE'
  | 'NOTIFICATION'
  | 'TIMER'
  | 'DYNAMIC';
