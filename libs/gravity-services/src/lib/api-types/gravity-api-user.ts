export interface GravityApiUser {
  id: string;
  name: string;
  email: string;
  firstname: string;
  surname: string;
  fullName: string;
  teams: {[team: string]: string};
}