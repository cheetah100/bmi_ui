import { Permissions } from '../core/permissions';
import { GravityApiAppVisualisation } from './gravity-api-app-visualisation';
import { GravityApiAppGroup } from './gravity-api-app-group';


export interface GravityApiApplication {
  id: string;
  name: string;
  description?: string;
  modules: { [key: string]: GravityApiApplicationSettings };
  groups: { [key: string]: GravityApiAppGroup };
  visualisation: { [key: string]: GravityApiAppVisualisation };
  permissions: Permissions;
}


// AKA "module"
export interface GravityApiApplicationSettings {
  id: string;
  name?: string;
  description?: string;
  ui: {[key: string]: any};
}
