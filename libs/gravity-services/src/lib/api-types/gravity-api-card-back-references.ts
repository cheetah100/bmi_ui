import { GravityApiCardView } from './gravity-api-cardview';

export interface GravityApiCardBackReferences {
  referringBoard: string;
  boardAccess: boolean;
  cards: GravityApiCardView[];
}
