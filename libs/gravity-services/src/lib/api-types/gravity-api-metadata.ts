export interface GravityApiMetadata {
  creator: string;
  created: Date;
  modifiedby: string;
  modified: Date;
}