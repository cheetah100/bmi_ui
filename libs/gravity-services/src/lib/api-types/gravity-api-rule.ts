import { GravityApiCondition } from './gravity-api-condition';
import { ObjectMap } from '@bmi/utils';
import { RuleType } from '../model/rule';


export interface GravityApiRule {
  id?: string;
  boardId?: string;
  name: string;
  ruleType: RuleType;
  index: number;
  description?: string;

  automationConditions: {[key: string]: GravityApiCondition};
  taskConditions: {[key: string]: GravityApiCondition};
  completionConditions: {[key: string]: GravityApiCondition};

  actions: {[key: string]: GravityApiAction};
  schedule?: string;

  /**
   * [Generated] This is a server generated property combining the board and
   * rule ID
   */
  uniqueId?: string;

  assignTo?: string;
}
export interface GravityApiActionConfig {
 [key: string]: any
}

export interface GravityApiAction {
  id?: string;
  name?: string;
  description?: string;
  type: string;
  order: number;
  config: GravityApiActionConfig;
}
