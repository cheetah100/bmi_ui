export type PluginFieldType =
  | 'STRING'
  | 'CREDENTIAL'
  | 'BOARD'
  | 'MAP'
  | 'FIELD'
  | 'LIST'
  | 'RESOURCE'
  | 'PHASE'
  | 'BOOLEAN'
  | 'CRONEXPRESSION';

export interface GravityApiPluginField {
  field: string;
  label: string;
  description: string;
  required: boolean;
  type: PluginFieldType;
  board: string;
}

export interface GravityApiPlugin {
  id: string;
  name: string;
  description: string;
  documentUrl: string;
  fields: GravityApiPluginField[];
  tags: string[];
}
