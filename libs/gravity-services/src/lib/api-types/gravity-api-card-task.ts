import { GravityApiCardEvent } from './gravity-api-card-event';

export interface GravityApiCardTask extends GravityApiCardEvent {
  taskid: string;
  complete: boolean;
}



export interface GravityApiCardTaskAction {
  taskAction: 'ASSIGN' | 'COMPLETE' | 'REVERT';
  user?: string;
}
