import { GravityApiCondition } from './gravity-api-condition';

export interface GravityApiFilter {
  id?: string;
  boardId: string;
  name?: string;
  expression?: string;
  owner?: string;
  phase?: string;
  conditions: {[_: string]: GravityApiCondition};
}


