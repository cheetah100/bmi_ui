import { Permissions } from '../core/permissions';
import { GravityApiMetadata } from '../api-types/gravity-api-metadata';

export interface GravityApiView {
  /**
   * The View ID in the backend. This will be auto-generated when posting a
   * view.
   */
  id: string;
  boardId: string;
  name: string;
  type: string;
  ui: { [key: string]: any };
  permissions?: Permissions;
  template?: string;

  /**
   * The fields in the view, indexed by their ID. The backend doesn't support
   * dots in the field ID, so these should be normalized to remove any
   * questionable characters.
   */
  fields: { [id: string]: GravityApiViewField };

  metadata?: GravityApiMetadata;
}


export interface GravityApiViewField {
  /**
   * The ID of the field on the view. This will be the field ID on the card.
   */
  id: string;

  /**
   * The name or path of the field to resolve on the card.
   */
  name: string;
  label: string;
  index: number;
  ui: { [key: string]: any };
}
