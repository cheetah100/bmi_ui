import { ObjectMap } from '@bmi/utils';

export interface GravityApiCredential {
  id: string;
  metadata: ObjectMap<string>;
  name: string;
  description: string;
  resource: string;
  method: string;
  requestMethod: string;
  identifier: string;
  secret: string;
  responseFields: ObjectMap<string>;
  permissions: ObjectMap<string>;
}
