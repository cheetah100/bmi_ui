import { Permissions } from '../core/permissions';
import { GravityApiView } from '../api-types/gravity-api-view';
import { GravityApiFilter } from '../api-types/gravity-api-filter';

export interface GravityApiBoard {
  id: string;
  name: string;
  boardType?: string;
  orderField: string;
  permissions?: Permissions;
  description?: string;
  /**
   * Deprecated. An alias in the Gravity API for permissions.
   */
  rootPermissions?: Permissions;

  phases: { [phaseId: string]: GravityApiPhase };
  templates: { [templateId: string]: GravityApiTemplate };
  views: { [viewId: string]: GravityApiView };
  filters: { [filterId: string]: GravityApiFilter };

  classification?: string;
  history?: boolean;

  fields?: { [fieldId: string]: GravityApiTemplateField };
}

export interface GravityApiTemplate {
  id: string;
  name: string;
  prefix?: string;
  type?: string;
  permissions?: Permissions;
  cardTitle?: string[];
  cardTitleFormat?: string;
  boardId?: string;
  groups: { [groupId: string]: GravityApiTemplateGroup };
  fields?: { [fieldId: string]: GravityApiTemplateField };
  ui?: GravityApiUiData;
}

export interface GravityApiPhase {
  id: string;
  name: string;
  index: number;
  description?: string;
  color?: string;
  invisible?: boolean;
  transitions?: string[];
  cards?: number;
}

export interface GravityApiTemplateGroup {
  id: string;
  name: string;
  description?: string;
  index?: number;
  fields: { [fieldId: string]: GravityApiTemplateField };
}

export interface GravityApiTemplateField {
  id: string;
  name: string;
  description?: string;
  label: string;
  type: string;
  ui?: GravityApiUiData;
  required?: boolean;
  requiredPhase?: string;
  optionlist?: string;
  optionlistfilter?: string;
  referenceField?: boolean;
  control?: string;
  immutable?: boolean;
  index?: number;
  indexed?: boolean;
  options?: { [key: string]: string };
  highlight?: string;
  length?: number;
  validation?: string;
  isMetadata?: boolean;
}

export interface GravityApiUiData {
  [key: string]: any;
}

export interface GravityApiTemplateUiData extends GravityApiUiData {
  businessContext?: string;
  singular?: string;
  plural?: string;
  color?: string;
  icon?: string;
}
