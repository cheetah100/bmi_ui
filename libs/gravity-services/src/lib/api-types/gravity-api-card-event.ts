export interface GravityApiCardEvent {
  uuid: string;
  user: string;
  occuredTime: number;  /* Yes. It is spelled incorrectly in the Gravity API */
  detail: string;
  level: string;
  category: string;
  board: string;
  phase: string;
  card: string,
  fields: {[id: string]: GravityApiCardEventField};
}


export interface GravityApiCardEventField {
  fieldName: string;
  label: string;
  before: string;
  after: string;
  reason: string;
}
