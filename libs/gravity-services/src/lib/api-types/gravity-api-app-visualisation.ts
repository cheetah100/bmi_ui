import { Permissions } from '../core/permissions';

export interface GravityApiAppVisualisation {
  id: string;
  name: string;
  type: string;
  applicationId: string;
  permissions: Permissions;
  ui: { [key: string]: any };
  urlId?: string;
}
