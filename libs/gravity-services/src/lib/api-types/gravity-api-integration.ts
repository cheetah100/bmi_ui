import { ObjectMap } from '@bmi/utils';
import { GravityApiAction } from './gravity-api-rule';


export interface GravityApiIntegration {
  id: string;
  name: string;
  connector: string;
  config: ObjectMap<string>;
  actions: ObjectMap<GravityApiAction>;
}
