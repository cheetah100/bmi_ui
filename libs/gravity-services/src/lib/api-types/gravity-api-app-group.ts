import { Permissions } from '../core/permissions';

export interface GravityApiAppGroup {
  id: string;
  name: string;
  icon: string;
  description: string;
  permissions: Permissions;

  order: number;

  // These seem to be unused in the current IDP implementation.
  parent?: string | null;
  boards?: string[] | null;

  items: { [id: string]: GravityApiAppGroupItem };
}

export interface GravityApiAppGroupItem {
  // These aren't defined in Gravity, as it just stores arbitrary JSON
  // objects, but these are the keys that we actually use in IDP it seems.
  type?: string;
  title?: string;
  value?: string;
  order?: string | number;

  [id: string]: any;
}