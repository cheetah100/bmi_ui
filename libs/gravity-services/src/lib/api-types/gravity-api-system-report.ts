import { GravityApiCardEvent } from './gravity-api-card-event';

export interface GravityApiSystemReport {
  server: string;
  leader: string;
  automation: string;
  automationbacklog: number;
  app_status: GravityApiSystemAppStatus;
  timestamp: number;
  couptime: number;
}


export interface GravityApiSystemAppStatus {
  alerts: {
    cards_in_alert: number;
    total_alerts: number;
  };

  queue_host: string;
  queue_status: any;
  queue_env: string;
  tasks: {
    total_tasks: number;
    Unassigned: number;
  },
}

export interface GravityApiSystemReportDetails {
  alertSummary: {[board: string]: number};
  alertDetails: {[board: string]: GravityApiCardEvent[]}
}


export interface GravityApiSystemRevisionInfo {
  'git.branch': string;
  'git.commit.id': string;
  'git.commit.message.short': string;
  'git.commit.time': string;
  'git.commit.user.name': string;
  'git.commit.user.email': string;
  'git.build.time': string;
}
