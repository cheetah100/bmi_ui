import { GravityApiRule, GravityApiAction } from '../api-types/gravity-api-rule';
import { Condition } from './condition';
import { sanitizeId, uniqueId, mapObjectValues } from '@bmi/utils';
import { cloneDeep } from 'lodash-es';

import { Action } from './action';

export type RuleType = 'VALIDATION' | 'COMPULSORY' | 'TASK' | 'SCHEDULED';


export class Rule {
  public id: string;
  public boardId: string;
  public name: string;
  public description: string;
  public ruleType: RuleType;
  public index: number;

  public automationConditions: Condition[] = [];
  public taskConditions: Condition[] = [];
  public completionConditions: Condition[] = [];
  public actions: Action[] = [];
  public schedule: string = '';

  static fromJSON(ruleJson: GravityApiRule): Rule {
    const rule = new Rule(ruleJson.id, ruleJson.name, ruleJson.boardId);
    rule.ruleType = ruleJson.ruleType;
    rule.index = ruleJson.index;
    rule.description = ruleJson.description;

    rule.automationConditions = Condition.fromJsonObject(ruleJson.automationConditions);
    rule.taskConditions = Condition.fromJsonObject(ruleJson.taskConditions);
    rule.completionConditions = Condition.fromJsonObject(ruleJson.completionConditions);
    rule.actions = Action.fromJsonObject(ruleJson.actions);
    rule.schedule = ruleJson.schedule;

    return rule;
  }

  constructor(
    id: string,
    name: string,
    boardId: string
  ) {
    this.id = id;
    this.name = name;
    this.boardId = boardId;
  }

  /**
   * Check this rule for basic validity and throw some errors if things are wrong
   */
  validate() {
    if (!this.id) {
      throw new Error('Rule validation: No rule ID');
    }

    if (!this.boardId) {
      throw new Error('Rule validation: No board ID');
    }
  }

  clone(): Rule {
    return Rule.fromJSON(this.toJSON());
  }

  toJSON(): GravityApiRule {
    return {
      id: this.id,
      name: this.name,
      boardId: this.boardId,
      ruleType: this.ruleType,
      index: this.index,
      description: this.description,
      automationConditions: Condition.makeJsonObject(this.automationConditions),
      taskConditions: Condition.makeJsonObject(this.taskConditions),
      completionConditions: Condition.makeJsonObject(this.completionConditions),
      actions: Action.toJsonObject(this.actions),
      schedule: this.schedule
    };
  }
}


