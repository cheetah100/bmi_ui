import {
  GravityApiCondition,
  GravityApiConditionOperation,
  GravityApiConditionType
} from '../api-types/gravity-api-condition';
import { mapObjectValues, sanitizeId, uniqueId } from '@bmi/utils';
import { getOperations, OperationInfo } from './condition-operations';

/**
 * @dynamic
 */
export class Condition {
  static fromJSON(cond: GravityApiCondition, conditionKey: string = null) {
    return new Condition(cond.fieldName, Condition.splitValue(cond.value), cond.operation, cond.conditionType, conditionKey);
  }

  /**
   * Splits a condition value into multiple segments on | characters.
   */
  static splitValue(value: string | string[]): string[] {
    // There may already be some configs that have an array stored, so uh, deal
    // with it?
    if (Array.isArray(value)) {
      return value;
    } else {
      return value ? value.split('|') : [];
    }
  }

  /**
   * Joins mulitple condition values into a single string.
   */
  static joinValue(multipleValues: string | string[]): string {
    if (typeof multipleValues === 'string') {
      return multipleValues;
    } else if (Array.isArray(multipleValues)) {
      return multipleValues.join('|');
    } else {
      return '';
    }
  }

  /**
   * Transform an array of Conditions into a map of Gravity JSON Conditions.
   */
  static makeJsonObject(conditions: Condition[]): { [id: string]: GravityApiCondition } {
    const obj: { [id: string]: GravityApiCondition } = {};
    for (const c of conditions) {
      obj[c.key] = c.toJSON();
    }
    return obj;
  }

  // Strict metadata checking in ngc will complain about functions that match a
  // certain shape, because these *might* be used in a way that could catch you
  // out. https://github.com/angular/angular/issues/19698#issuecomment-338340211
  // If you are only going to call this function from regular JS code, then you
  // can silence the error with @dynamic

  static fromJsonObject(conditionObj: { [id: string]: GravityApiCondition }): Condition[] {
    return mapObjectValues(conditionObj || {}, (c, id) => Condition.fromJSON(c, id));
  }

  static getValidOperations(conditionType: GravityApiConditionType, gravityType: string = null): OperationInfo[] {
    return getOperations(conditionType, gravityType);
  }

  constructor(
    public fieldName: string,
    public values: string[],
    public operation: GravityApiConditionOperation,
    public conditionType: GravityApiConditionType = null,
    public key: string = null
  ) {
    if (!this.key) {
      this.key = this.generateId();
    }
  }

  private generateId() {
    return sanitizeId(uniqueId([this.conditionType, this.fieldName].filter(x => !!x).join('_').toLowerCase()));
  }

  get value(): string {
    return Condition.joinValue(this.values);
  }

  set value(value: string) {
    this.values = Condition.splitValue(value);
  }

  clone(): Condition {
    return Condition.fromJSON(this.toJSON());
  }

  toJSON(): GravityApiCondition {
    return {
      fieldName: this.fieldName,
      value: Condition.joinValue(this.values),
      operation: this.operation,
      conditionType: this.conditionType
    };
  }
}


