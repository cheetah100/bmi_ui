import { GravityApiRule, GravityApiAction } from '../api-types/gravity-api-rule';
import { Condition } from './condition';
import { sanitizeId, uniqueId, mapObjectValues } from '@bmi/utils';
import { cloneDeep } from 'lodash-es';


export interface Action extends GravityApiAction {
}


export const Action = {
  fromJson(actionJson: GravityApiAction, actionKey: string = null) {
    const action = cloneDeep(actionJson);
    action.id = actionKey || action.id;
    return action;
  },

  toJson(action: Action): GravityApiAction {
    return cloneDeep(action);
  },

  toJsonObject(actions: Action[]): { [id: string]: GravityApiAction } {
    const obj: { [id: string]: GravityApiAction } = {};
    actions.map(a => Action.toJson(a))
      .forEach((aj, index) => {
        aj.order = index;
        obj[aj.id] = aj;
      });
    return obj;
  },

  fromJsonObject(actionsObj: { [id: string]: GravityApiAction }): Action[] {
    return mapObjectValues(actionsObj || {}, (a, id) => Action.fromJson(a, id))
      .sort((a, b) => a.order - b.order);
  },

  clone(action: Action) {
    return Action.fromJson(Action.toJson(action), action.id);
  },

  create(type: string, id: string = null): Action {
    return {
      id: id || sanitizeId(uniqueId(type)),
      type: type,
      order: 0,
      config: {
        method: '',
        parameters: [],
        resource: '',
        response: '',
        properties: {}
      }
    };
  }
}

