import {
  GravityApiConditionOperation,
  GravityApiConditionType
} from '../api-types/gravity-api-condition';
import { GravityApiFilter } from '../api-types/gravity-api-filter';

import { Condition } from './condition';
import { mapObjectValues } from '@bmi/utils';


/**
 * Represents a Gravity Filter
 *
 * This can parse/serialize to the API JSON format, but has some extra methods
 * for working with filters --- at least that's the intention! For now it just
 * makes it easier to add conditions.
 *
 * Gravity has a special mechanism for phase filtering, but this class attempts
 * to hide that as best as possible -- a phase filter will get represented as a
 * regular filter condition so to simplify the UI.
 *
 * When serializing the filter to JSON, the phase filter conditions will be
 * squeezed together into the 'phase' parameter of the filter.
 */
export class Filter {
  name: string;
  conditions: Condition[] = [];

  static fromJSON(filterJson: GravityApiFilter): Filter {
    const filter = new Filter(filterJson.boardId, filterJson.id);
    filter.name = filterJson.name;
    mapObjectValues(
      filterJson.conditions,
      (c, id) => filter.conditions.push(Condition.fromJSON(c, id)));

    if (filterJson.phase) {
      filter.addCondition('phase', filterJson.phase, 'EQUALTO');
    }

    return filter;
  }

  static fromFilterValues(board: string, filterValues: { [field: string]: string[] }) {
    const filter = new Filter(board, null);
    for (const [field, values] of Object.entries(filterValues)) {
      if (values) {
        filter.addCondition(field, values, 'EQUALTO');
      }
    }
    return filter;
  }

  constructor(
    public boardId: string,
    public id: string = null
  ) { }

  /**
   * Adds a condition to the filter.
   *
   * The Gravity API stores the filter conditions in a map and the key seems
   * pretty arbitrary. To avoid churning through values when updating an
   * existing filter we can keep the existing key.
   */
  addCondition(
    fieldName: string,
    value: string | string[],
    operation: GravityApiConditionOperation,
    conditionType: GravityApiConditionType = null,
    key: string = null
  ) {
    this.conditions.push(new Condition(fieldName, Condition.splitValue(value), operation, conditionType, key));
  }

  toJSON(): GravityApiFilter {
    const filterJson: GravityApiFilter = {
      boardId: this.boardId,
      name: this.name,
      conditions: this.makeConditionsJson()
    };

    if (this.id) {
      filterJson.id = this.id;
    }

    const phaseValues = this.getPhaseFilterValues();
    if (phaseValues) {
      filterJson.phase = phaseValues;
    }

    return filterJson;
  }

  private getPhaseFilterValues(): string {
    const phaseValues = [].concat(...this.conditions
      .filter(c => c.fieldName === 'phase')
      .map(c => Condition.splitValue(c.values)));

    if (phaseValues.length > 0) {
      return Condition.joinValue(phaseValues);
    } else {
      return null;
    }
  }

  private makeConditionsJson(excludePhase = true) {
    return Condition.makeJsonObject(this.conditions
      // We want to handle a phase condition specially, as Gravity has a
      // specific field for filtering phase. It's possible to use
      // 'metadata.phase' but this is exposing some internal details that may
      // change in future, so the special phase interface it is!
      .filter(condition => !excludePhase || condition.fieldName !== 'phase'));
  }
}


