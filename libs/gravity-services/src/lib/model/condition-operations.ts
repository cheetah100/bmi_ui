import { GravityApiConditionOperation, GravityApiConditionType} from '../api-types/gravity-api-condition';

export interface OperationInfo {
  value: GravityApiConditionOperation;
  name: string;
  conditionTypes: GravityApiConditionType[];
  validGravityTypes?: string[];
  requiresValue: boolean;
}

export const OPERATIONS: OperationInfo[] = [
  // The order of this list determines the order of the filters lists too. The
  // first valid operation will probably be considered the default, so I've put
  // these task conditions first because they make more sense than
  // EQUAL/NOTEQUAL as the default.
  { value: 'COMPLETE', name: 'Task Complete', requiresValue: true, conditionTypes: ['TASK']},
  { value: 'INCOMPLETE', name: 'Task Incomplete', requiresValue: true, conditionTypes: ['TASK']},

  { value: 'EQUALTO', name: 'Equal to', requiresValue: true, conditionTypes: ['PROPERTY', 'TASK', 'PHASE'] },
  { value: 'NOTEQUALTO', name: 'Not equal to', requiresValue: true, conditionTypes: ['PROPERTY', 'TASK', 'PHASE'] },

  { value: 'NOTNULL', name: 'Not null', requiresValue: false, conditionTypes: ['PROPERTY'] },
  { value: 'ISNULL', name: 'Is null', requiresValue: false, conditionTypes: ['PROPERTY'] },

  { value: 'CONTAINS', name: 'Contains', requiresValue: true, conditionTypes: ['PROPERTY'] },
  { value: 'NOTCONTAINS', name: 'Not contains', requiresValue: true, conditionTypes: ['PROPERTY'] },

  { value: 'GREATERTHAN', name: 'Greater than', requiresValue: true, conditionTypes: ['PROPERTY'], validGravityTypes: ['NUMBER', 'DATE'] },
  { value: 'LESSTHAN', name: 'Less than', requiresValue: true, conditionTypes: ['PROPERTY'], validGravityTypes: ['NUMBER', 'DATE'] },
  { value: 'GREATERTHANOREQUALTO', name: 'Greater than or equal', requiresValue: true, conditionTypes: ['PROPERTY'], validGravityTypes: ['NUMBER', 'DATE'] },
  { value: 'LESSTHANOREQUALTO', name: 'Less than or equal', requiresValue: true, conditionTypes: ['PROPERTY'], validGravityTypes: ['NUMBER', 'DATE'] },

  { value: 'AFTER', name: 'After', requiresValue: true, conditionTypes: ['PROPERTY'], validGravityTypes: ['DATE'] },
  { value: 'BEFORE', name: 'Before', requiresValue: true, conditionTypes: ['PROPERTY'], validGravityTypes: ['DATE'] },

];


export function supportsConditionType(conditionType: GravityApiConditionType) {
  return (op: OperationInfo) => op.conditionTypes && op.conditionTypes.includes(conditionType);
}


export function supportsGravityType(gravityType: string) {
  const normalizedGravityType = gravityType.toUpperCase();
  return (op: OperationInfo) => !op.validGravityTypes || op.validGravityTypes.includes(normalizedGravityType);
}


export function getOperations(conditionType: GravityApiConditionType, gravityType: string = null) {
  let ops = OPERATIONS.filter(supportsConditionType(conditionType));
  if (gravityType) {
    ops = ops.filter(supportsGravityType(gravityType));
  }
  return ops;
}
