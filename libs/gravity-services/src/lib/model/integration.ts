import { Omit } from '@bmi/utils';
import { GravityApiIntegration } from '../api-types/gravity-api-integration';
import { Action } from './action';
import pick from 'lodash-es/pick';


export interface Integration extends Omit<GravityApiIntegration, 'actions'> {
  isNew?: boolean;
  actions: Action[];
}

export interface IntegrationSummary {
  id: string;
  name: string;
  connector: string;
}


// In addition to the type, export some helper tools for serializing to/from the
// Gravity API format. This was formerly a class, but using plain objects +
// types works better with Reactive Forms, which is principally how we are using
// these types in the admin tools.
export const Integration = {
  fromJSON(json: GravityApiIntegration): Integration {
    return {
      id: json.id,
      name: json.name,
      connector: json.connector,
      config: json.config || {},
      actions: Action.fromJsonObject(json.actions || {}),
    };
  },

  toJSON(integration: Integration): GravityApiIntegration {
    return {
      ...pick(integration, ['id', 'name', 'config', 'connector']),
      actions: Action.toJsonObject(integration.actions)
    };
  },

  validate(integration: Integration): void {
    if (!this.id) {
      throw new Error('Integration must have an id');
    }

    if (!this.name) {
      throw new Error('Integration must have a name');
    }

    if (!this.connector) {
      throw new Error('Integration must have a connector type');
    }
  }
};
