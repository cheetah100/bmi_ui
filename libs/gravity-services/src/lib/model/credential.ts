import { Omit, ObjectMap } from '@bmi/utils';
import { GravityApiCredential } from '../api-types/gravity-api-credential';
import pick from 'lodash-es/pick';

export interface Credential extends GravityApiCredential {
    isNew?: boolean;
    updateIdentifier?: boolean;
    updateSecret?: boolean;
}

export interface CredentialSummary {
  id: string;
  name: string;
}

export const Credential = {
 fromJSON(json: GravityApiCredential): Credential {
        return {
            id: json.id,
            metadata: json.metadata,
            name: json.name,
            description: json.description,
            resource: json.resource,
            method: json.method,
            requestMethod: json.requestMethod,
            identifier: json.identifier,
            secret: json.secret,
            responseFields: json.responseFields,
            permissions: json.permissions
        };
  },

  toJSON(credential: Credential): GravityApiCredential {
    return {
      ...pick(credential, ['id', 
        'metadata', 
        'name', 
        'description',
        'resource',
        'method',
        'requestMethod',
        'identifier',
        'secret',
        'responseFields',
        'permissions']
        )
    };
  },

  validate(credential: Credential): void {
    if (!this.id) {
      throw new Error('Credential must have an id');
    }

    if (!this.name) {
      throw new Error('Credential must have a name');
    }

    if (!this.resource) {
      throw new Error('Credential must have a resource');
    }
  },

  clone(): Credential {
    return Credential.fromJSON(this.toJSON());
  },
}
