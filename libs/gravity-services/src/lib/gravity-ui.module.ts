import { NgModule } from '@angular/core';
import { PathPickerComponent } from './ui/path-picker/path-picker.component';
import { PathPickerSuggestionsComponent } from './ui/path-picker/path-picker-suggestions.component';
import { ConditionListComponent } from './ui/condition-list/condition-list.component';
import { ConditionListRowPropertyComponent } from './ui/condition-list/condition-list-row-property.component';
import { ConditionListRowDynamicComponent } from './ui/condition-list/condition-list-row-dynamic.component';
import { ConditionListRowPhaseComponent } from './ui/condition-list/condition-list-row-phase.component';
import { ConditionListRowTaskComponent } from './ui/condition-list/condition-list-row-task.component';
import { FilterConfigComponent } from './ui/filter-conditions/filter-config.component';
import { RuleConditionsComponent } from './ui/rule-conditions/rule-conditions.component';
import { PermissionsComponent } from './ui/permissions/permissions.component';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedUiModule, ModalService } from '@bmi/ui';
import { GravityModule } from './gravity.module';
import { SpreadsheetUploaderComponent } from './ui/spreadsheet-uploader/spreadsheet-uploader.component';
import { SpreadsheetUploaderModalComponent } from './ui/spreadsheet-uploader/spreadsheet-uploader-modal.component';
import { UploadStatusComponent } from './ui/upload-status/upload-status.component';
import { BulkDataManagerComponent } from './ui/bulk-data-manager/bulk-data-manager.component';
import { BulkDataManagerDirective } from './ui/bulk-data-manager/bulk-data-manager.directive';
import { PhasePickerComponent } from './ui/phase-picker/phase-picker.component';



@NgModule({
  declarations: [
    PathPickerComponent,
    PathPickerSuggestionsComponent,
    PhasePickerComponent,
    ConditionListComponent,
    ConditionListRowPropertyComponent,
    ConditionListRowDynamicComponent,
    ConditionListRowPhaseComponent,
    ConditionListRowTaskComponent,
    FilterConfigComponent,
    RuleConditionsComponent,
    PermissionsComponent,
    SpreadsheetUploaderComponent,
    SpreadsheetUploaderModalComponent,
    UploadStatusComponent,
    BulkDataManagerComponent,
    BulkDataManagerDirective
  ],

  providers: [
    ModalService
  ],
  exports: [
    PathPickerComponent,
    PhasePickerComponent,
    ConditionListComponent,
    FilterConfigComponent,
    RuleConditionsComponent,
    PermissionsComponent,
    SpreadsheetUploaderComponent,
    SpreadsheetUploaderModalComponent,
    UploadStatusComponent,
    BulkDataManagerComponent
  ],

  imports: [
    RouterModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedUiModule,
    GravityModule
  ],
  entryComponents: [
    PhasePickerComponent,
    UploadStatusComponent,
    SpreadsheetUploaderComponent,
    SpreadsheetUploaderModalComponent
  ]
})
export class GravityUiModule { }

