import { combineLatest, Observable, of } from 'rxjs';
import { publishReplay, refCount, first, flatMap, take, toArray, filter, switchMap } from 'rxjs/operators';
import { GravityCache } from '../core/gravity-cache';
import { GravityConfigService } from '../core/gravity-config.service';
import { Card } from '../core/card';
import { LoadingTaskPool } from '@bmi/utils';
import { Gravity } from '../gravity';
import { FieldPath } from '../field-path/field-path';
import { BoardPathResolver } from '../field-path/board-field-path';

export class CardDataContext {
  private card: Observable<Card>;

  get isLoading() {
    return this.loadingTasks.isLoading;
  }

  constructor(
    private gravityConfigService: GravityConfigService,
    private gravityCache: GravityCache,
    cardObservable: Observable<Card>,
    private loadingTasks = new LoadingTaskPool()
  ) {
    this.card = cardObservable.pipe(
      publishReplay(1),
      refCount()
    );

    this.card.pipe(
      this.loadingTasks.monitor(),
      first()
    ).subscribe();
  }

  /**
   * Creates a CardDataContext for a single card.
   */
  static forCard(gravity: Gravity, card: Card): CardDataContext {
    return new CardDataContext(gravity.configService, gravity.cache, of(card));
  }

  /**
   * Get the value of a path on this card, resolving any related cards as necessary.
   *
   * If the data is already cached, this should resolve synchronously, otherwise
   * this will use the GravityCache service to fetch the cards.
   *
   * If the path includes a LIST field, the results will be an array of values.
   *
   * The card observable that the data context was created with will determine
   * whether this emits a single value or a stream whenever the card changes.
   *
   * @param path the path on the board
   * @returns an observable that will emit the value for this path.
   */
  getField(path: string): Observable<any> {
    return this.card.pipe(
      switchMap(card => {
        const resolver = new BoardPathResolver(card.board, this.gravityConfigService);
        const fp = resolver.resolve(path);
        const values = this.resolveFieldImpl(card, fp.segments);
        if (fp.isMultiValue) {
          return values.pipe(toArray());
        } else {
          return values;
        }
      })
    );
  }

  /**
   * Gets the value of several fieldpaths.
   *
   * This effectively just calls combineLatest on getField.
   */
  getFields(paths: string[]): Observable<any[]> {
    if (paths.length === 0) {
      return of([]);
    } else {
      return combineLatest(
        ...paths.map(p => this.getField(p))
      );
    }
  }

  /**
   * This works recursively to resolve fields on a card. If it encounters a
   * multi-card relationship it will fan out, returning a sequence of values.
   *
   * This is the internal implementation of the field resolution algorithm and
   * probably shouldn't be called directly...
   *
   * This returns ResolvedField objects, containing the value, along with the
   * card and fieldName, giving a formatter full access to the config to do its
   * work.
   *
   * @param card - The card to resolve against
   * @param pathSegments - the pre-split array of fields to look up. The first
   *     element is the field to be looked up on this card.
   */
  private resolveFieldImpl(card: Card, pathSegments: FieldPath[]): Observable<any> {
    const segment = pathSegments[0];
    const remaining = pathSegments.slice(1);

    if (!card || !segment) {
      return of(null);
    }

    const cardReference = card.getReference(segment.fieldName);
    if (!cardReference) {
      return of(card[segment.fieldName]);
    } else {
      // We can handle single and multi relations the same way; the aggregation
      // happens at a higher level. If there are multiple ids, this will fan out
      // and return a sequence of ResolvedField
      return of(...cardReference.ids).pipe(
        flatMap(id => this.fetchCard(cardReference.board, id)),

        // If this segment is a list field but some of the referenced cards
        // could not be found, we will filter them out, rather than include a
        // null value in the collection. This is consistent with the behaviour
        // of the Card properties.
        //
        // Note: we don't want to do this for *all* null values though, because
        // if a regular optionlist is null, or if a subfield on the list
        // resolves to null then we want to include those in the results -- just
        // drop any cards that are missing from the collection.
        filter(c => !segment.isListField || !!c),

        flatMap(newCard => (remaining.length > 0) ? this.resolveFieldImpl(newCard, remaining) : of(newCard)),
      );
    }
  }

  private fetchCard(boardId: string, cardId: string): Observable<Card> {
    return this.gravityCache.get(boardId, cardId).pipe(
      // As nice as it would be to get updates when the card changes, the
      // implementation of the list field means that we want this observable to
      // complete once there is a result. This isn't strictly necessary, as the
      // service *should* complete the observables, but this take(1) should
      // ensure that we don't get caught out.
      take(1),
      this.loadingTasks.monitor()
    );
  }
}

