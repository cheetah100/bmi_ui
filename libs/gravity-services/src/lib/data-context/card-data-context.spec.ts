import { TestBed, waitForAsync } from '@angular/core/testing';

import { of } from 'rxjs';

import { gravityTestbed } from '../testing/gravity-test-tools';

import {
  MOCK_PROJECTS_BOARD_CONFIG,
  MOCK_INVESTMENTS_BOARD_CONFIG,
  MOCK_INVESTMENTS_CARDS,
  MOCK_PROJECTS_CARDS
} from '../testing/mock-investments';

import { QUARTER_BOARD_CONFIG, QUARTER_CARDS } from '../testing/model-board-configs';
import { GravityCache } from '../core/gravity-cache';
import { CardDataContext } from '../data-context/card-data-context';
import { Card } from '../core/card';
import { GravityConfigService } from '../core/gravity-config.service';

describe('CardDataContext', () => {
  let cache: GravityCache;
  let configService: GravityConfigService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        gravityTestbed({
          injectNullHttpClient: true,
          useFakeGravityService: true,
          boards: [
            MOCK_INVESTMENTS_BOARD_CONFIG,
            MOCK_PROJECTS_BOARD_CONFIG,
            QUARTER_BOARD_CONFIG
          ],

          cards: {
            investments: MOCK_INVESTMENTS_CARDS,
            projects: MOCK_PROJECTS_CARDS,
            quarters: QUARTER_CARDS
          }
        }),
      ]
    });

    cache = TestBed.get(GravityCache) as GravityCache;
    configService = TestBed.get(GravityConfigService) as GravityConfigService;
  });


  describe('Typical cases', () => {
    let testCard: Card;
    let dc: CardDataContext;
    beforeEach(() => {
      testCard = undefined;
      cache.get<Card>('investments', 'iv03').subscribe(c => testCard = c);
      dc = new CardDataContext(configService, cache, of(testCard));
    });

    /**
     * Synchronously get a value.
     *
     * This should work fine in the test suite as our fake gravity services all
     * behave synchronously.
     */
    function getField(path: string) {
      let value: any;
      dc.getField(path).subscribe(v => value = v);
      return value;
    }

    it('should resolve regular fields directly on the card', () => {
      expect(getField('value')).toBe(testCard.value);
    });

    it('should look up simple related fields', () => {
      expect(getField('project.name')).toBe(testCard.project.name);
    });

    it('should get list fields', () => {
      const quarters = getField('project.activeQuarters');
      expect(quarters).toEqual(testCard.project.activeQuarters);
    });

    it('should get sub-fields of lists', () => {
      const quarterNames = getField('project.activeQuarters.name');
      expect(quarterNames).toEqual(testCard.project.activeQuarters.map(q => q.name));
    });

    it('should return undefined if the field does not exist', waitForAsync(() => {
      let counter = 0;
      let value = true;
      dc.getField('doesnotexist').subscribe(v => {
        counter += 1;
        value = v;
      });

      expect(counter).toEqual(1);
      expect(value).toBeUndefined();
    }));

    describe('getFields()', () => {
      function getFields(paths: string[]): any[] {
        let values: any[];
        dc.getFields(paths).subscribe(v => values = v);
        return values;
      }

      it('should emit an empty array if no paths supplied', () => {
        expect(getFields([])).toEqual([]);
      });

      it('should resolve a single field', () => {
        expect(getFields(['value'])).toEqual([testCard.value]);
      });

      it('should resolve the same field multiple times', () => {
        const value = testCard.value;
        expect(getFields(['value', 'value', 'value'])).toEqual([value, value, value]);
      });

      it('should return the results in the same order as the fields', () => {
        expect(getFields(['value', 'project.name', 'project.id'])).toEqual(
          [testCard.value, testCard.project.name, testCard.project.id]
        );
      });

      it('should still return results if some fields are not found.', () => {
        expect(getFields(['value', 'doesnotexist', 'project.id'])).toEqual([testCard.value, undefined, testCard.project.id]);
      });
    });
  });
});
