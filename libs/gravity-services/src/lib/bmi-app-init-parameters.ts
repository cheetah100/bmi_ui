import { InjectionToken } from '@angular/core';

export interface BmiAppInitParameters {
  /**
   * The base URL for Gravity API calls.
   *
   * This will be used by the GravityUrlService to generate URLs, and hopefully
   * one day, will be used by anything needing to make a call to Gravity.
   */
  gravityBaseUrl: string;

  /**
   * The application id configured using gravity in the settings API.
   *
   * This will be used to dynamically determine which dev/stage/prod
   * functionality to use
   */
  lifecycle: string;
}


export const BMI_APP_INIT_PARAMETERS = new InjectionToken<BmiAppInitParameters>('BMI_APP_INIT_PARAMETERS');