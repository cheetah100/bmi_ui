import { Injectable } from '@angular/core';
import { Observable, of, interval } from 'rxjs';
import { map } from 'rxjs/operators';
import { PollingService, PropertyListItem } from '@bmi/ui';
import { ObservableMap, combineLatestObject, ObjectMap, describeTimestampFriendly, CacheEntryWatchOptions } from '@bmi/utils';
import { CardData } from '../api-types/gravity-api-cardview';
import { BoardService } from '../services/board.service';
import { GravityCardAlertService } from '../services/gravity-card-alert.service';
import { CardTaskService } from '../services/card-task.service';
import { CardDetails } from './card-details';
import ms from 'ms';

import pick from 'lodash-es/pick';

interface CardRef {
  board: string;
  id: string;
}



@Injectable({providedIn: 'root'})
export class CardDetailsService {
  constructor(
    private boardService: BoardService,
    private alertService: GravityCardAlertService,
    private pollingService: PollingService,
    private taskService: CardTaskService
  ) {}

  private cardDetailsMap = new ObservableMap<CardRef, CardDetails>({
    source: ({board, id}) => this.fetchDetails(board, id),
    keyFunction: ({board, id}) => `${board}/${id}`,
    maxAge: ms('1 seconds'),
    emitStaleEntries: true,
    // considerRefreshTrigger: this.pollingService.intervalTimer(ms('3 seconds'))
  });s

  get(board: string, id: string, opts?: CacheEntryWatchOptions) {
    // debugger
    return this.cardDetailsMap.watch({board, id}, opts);
  }

  refresh(board: string, id: string) {
    this.cardDetailsMap.refresh({board, id});
  }

  isLoading(board: string, id: string) {
    return this.cardDetailsMap.isLoading({board, id});
  }

  private fetchDetails(board: string, cardId: string) {
    return combineLatestObject({
      boardId: of(board),
      cardId: of(cardId),
      card: this.boardService.fetchById(board, cardId).pipe(
        map(card => {
          if (!card) {
            throw new Error('Card not found');
          } else {
            return card;
          }
        })
      ),
      alerts: this.alertService.getAlertsForCard(board, cardId),
      tasks: this.taskService.fetchTasks(board, cardId),
    }).pipe(
      map(details => {
        const card = details.card;
        return {
          ...details,
          usefulProperties: toPropList(pick(card,
            'id',
            'board',
            'phase',
            'title',
            'lock',
            'alerted'
          )),
          // TODO: make this a bit fancier, apply formatting based on template
          // info?
          cardFields: toPropList(card.fields),
          metadata: toPropList({
            'Created By': card.creator,
            'Created At': describeTimestampFriendly(card.created),
            'Modified By': card.modifiedby,
            'Modified At': describeTimestampFriendly(card.modified)
          }),
          weirdProperties: []
        };
      })
    );
  }
}


function toPropList(obj: ObjectMap<any>): PropertyListItem[] {
  return Object.entries(obj).map(([key, value]) => ({key: key, value: String(value)}))
}
