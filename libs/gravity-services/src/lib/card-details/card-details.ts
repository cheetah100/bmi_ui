import { CardData } from '../api-types/gravity-api-cardview';
import { GravityApiCardEvent } from '../api-types/gravity-api-card-event';
import { GravityApiCardTask } from '../api-types/gravity-api-card-task';

import { PropertyListItem } from '@bmi/ui';

/**
 * A roll-up of detailed information about a card. This is mostly used for
 * debug UIs.
 */
export interface CardDetails {
  boardId: string;
  cardId: string;
  card: CardData;
  alerts: GravityApiCardEvent[];
  tasks: GravityApiCardTask[];

  usefulProperties: PropertyListItem[];
  weirdProperties: PropertyListItem[];
  cardFields: PropertyListItem[];
  metadata: PropertyListItem[];

}


