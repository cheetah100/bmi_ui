
## The Problem
At the moment the api calls for the different data objects are all over the place. This make the code hard to understand/follow and there is no clear separation of concerns.

- For *cards* we use `GravityService`
- For *events* we use `CacheService`
- For *users* we use `UserTableComponent`

## The Solution
We should create a standard for how we pull back data while allowing different kinds of date, in this case *cards/board*, *events* and *users*.

I suggest we do the following:

- Rename `GravityService` to `BoardRepository`
- Create a `EventRepository`
- Create a `UserRepository`

## Implementation
```typescript
export class BoardRepository {
  getCard(boardName: string, cardId: string): Observable<GravityCard>;

  getCards(boardName: string): Observable<GravityCard[]>;

  addCard(boardName: string, fields: any, phase: string = 'current'): Observable<GravityCard>;

  updateCard(boardName: string, fields: any, phase: string, changeReasons: {[property: string]: string }): Observable<GravityCard>;

  move(cardId: string, boardName: string, newPhase: string): Observable<GravityCard>;

  moveWithReason(cardId: string, boardName: string, newPhase: string, reason: string): Observable<GravityCard>;

  moveMultipleCards(boardName: string, payload: any): Observable<void>;
}

export class EventRepository {
  getEvents(after: string, before: string): Observable<Event[]>;
}

export class UserRepository {

  getAll(): Observable<User[]>;

  get(userId: string): Observable<User>;

  add(user: User): Observable<User>;

  update(user: User): Observable<User>;

  delete(userId: string): Observable<void>;
}

export interface FormConfig {
  type: string,
  actions: Actions;
}

export interface Actions {
  get(dateType: string, id: string): Observable<any>;

  add(dataType: string, data: any): Observable<any>;

  update(dataType: string, data: any, changeReasons: {[property: string]: string}): Observable<any>;

  delete(dataType: string, id: string): Observable<void>;
}

export declare type FormConfigs = FormConfig[];

export function formConfigFactory(
  boardRepository: BoardRepository,
  userRepository: UserRepository
): FormConfigs {
  return [
    {
      type: 'board',
      actions: {
        get(dateType: string, id: string): Observable<any> {
          return boardRepository.getCard(dataType, id);
        }

        // TODO: we need to know about currentPhase
        add(dateType: string, data: any): Observable<any> {
          return boardRepository.addCard(dataType: card, data, 'current');
        }

        // TODO: we need to handle moving of phases
        update(dateType: string, data: any, changeReasons: {[property: string]: string}, id: string): Observable<any> {
          return boardRepository.updateCard(dataType, data, 'current', changeReasons);
        }

        delete(dataType: string, id: string): Observable<any> {
          return boardRepository.move(dataType: card, id, 'archive');
        }
      }
    },
    {
      type: 'user',
      actions: {
        get(dateType: string, id: string): Observable<any> {
          return userRepository.get(id);
        }

        add(dateType: string, data: any): Observable<any> {
          return userRepository.add(data);
        }

        update(dateType: string, data: any, changeReasons: {[property: string]: string}, id: string): Observable<any> {
          return userRepository.update(data);
        }

        delete(dataType: string, id: string): Observable<any> {
          return userRepository.delete(id);
        }
      }
    }
  ];
}