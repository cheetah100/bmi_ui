import { TestBed } from '@angular/core/testing';
import { sanitizeGravityApiId } from '@bmi/utils';
import { switchMap, take } from 'rxjs/operators';
import { CardData } from '../api-types/gravity-api-cardview';
import { CardCacheService } from '../services/card-cache.service';
import { BoardConfigBuilder } from '../testing/board-config-builder';
import { gravityTestbed2, makeBoard } from "../testing/gravity-testbed-2";
import { PathLookupService } from './path-lookup.service';

function mkCard<T extends {name: string}>(fields: T) {
  return {id: sanitizeGravityApiId(fields.name), title: fields.name, fields};
}

describe('Path Lookup service', () => {
  let pathLookupService: PathLookupService;
  let cacheService: CardCacheService;

  function getCard(boardId: string, cardId: string): Promise<CardData> {
    return cacheService.get(boardId, cardId).pipe(take(1)).toPromise();
  }

  function lookupPath(boardId: string, cardId: string, path: string): Promise<any> {
    return cacheService.get(boardId, cardId).pipe(
      switchMap(card => pathLookupService.getField(card, path)),
      take(1)
    ).toPromise();
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        gravityTestbed2({
          boards: [
            makeBoard({
              config: new BoardConfigBuilder('employee')
                .addField('name')
                .addField('dept', f => f.optionlist('department'))
                .addField('roles', f => f.optionlist('role').type('LIST')),

              cards: [
                {name: 'Joe', dept: 'sales', roles: ['salesperson', 'designer']},
                {name: 'Viv', dept: 'sales', roles: ['manager', 'salesperson']},
                {name: 'Liz', dept: 'marketing', roles: ['manager', 'designer']},
                {name: 'Ben', dept: 'engineering', roles: ['developer', 'tester']},
                {name: 'Meg', dept: 'engineering', roles: ['manager', 'project_manager']},
                {name: 'Kev', dept: null, roles: null},

                // This department does not exist here
                {name: 'Pam', dept: 'innovation', roles: ['designer', 'ideation']}
              ].map(mkCard)
            }),

            makeBoard({
              config: new BoardConfigBuilder('role')
                .addField('name'),

              cards: [
                {name: 'Manager'},
                {name: 'Developer'},
                {name: 'Salesperson'},
                {name: 'Designer'},
                {name: 'Project Manager'}
              ].map(mkCard)
            }),

            makeBoard({
              config: new BoardConfigBuilder('department')
                .addField('name')
                .addField('manager', f => f.optionlist('employee')),

              cards: [
                {name: 'Sales', manager: 'viv'},
                {name: 'Marketing', manager: 'liz'},
                {name: 'Engineering', manager: 'ben'}
              ].map(mkCard)
            })
          ]
        })
      ]
    });

    pathLookupService = TestBed.inject(PathLookupService);
    cacheService = TestBed.inject(CardCacheService);
  });

  it('should create the path lookup service', () => {
    expect(pathLookupService).toBeTruthy();
  });

  function forAllCases<T extends {it: string}>(testCases: T[], testFunction: (testCase: T) => any | Promise<any>) {
    for (const testcase of testCases) {
      it(testcase.it, testFunction.bind(this, testcase));
    }
  }

  const notFoundMessage = (board: string, card: string) => `Not found: ${board}:${card}`;

  forAllCases([
    {it: 'should look up direct fields', path: 'name', expected: 'Joe'},
    {it: 'should return the id when a reference field is requested directly', path: 'dept', expected: 'sales'},
    {it: 'should resolve paths on the referenced card', path: 'dept.name', expected: 'Sales'},
    {it: 'should get the ID metadata field as `id`', path: 'id', expected: 'joe'},
    {it: 'should get the board metadata as `board`', path: 'board', expected: 'employee'},
    {it: 'should return the list of IDs for a LIST field when accesed directly', path: 'roles', expected: ['salesperson', 'designer']},
    {it: 'should `map` over LIST reference fields to get nested paths', path: 'roles.name', expected: ['Salesperson', 'Designer']},
    {it: 'should support deep paths over mulitple cards', path: 'dept.manager.name', expected: 'Viv'},
    {it: 'should return an undefined value if a field does not exist', path: 'noexists', expected: undefined},
    {
      it: 'should return an undefined value for a chain of paths that do not exist -- ie.e short circuit',
      path: 'this.does.not.exist',
      expected: undefined,
    },

    {
      it: 'should return a null value if the field is defined but null',
      employeeId: 'kev',
      path: 'dept',
      expected: null
    },

    {
      it: 'should return undefined if trying to do a lookup on a null reference field',
      employeeId: 'kev',
      path: 'dept.name',
      expected: undefined
    },

    {
      it: 'should (by default) return an error string when looking up a reference field if target does not exist',
      employeeId: 'pam',
      path: 'dept.name',
      expected: notFoundMessage('department', 'innovation')
    },
    {
      it: 'should not error when requesting a reference field pointing to an invalid card',
      employeeId: 'pam',
      path: 'dept',
      expected: 'innovation'
    },
    {
      it: 'should return an error when doing lookups on a LIST field',
      employeeId: 'pam',
      path: 'roles.name',
      expected: ['Designer', notFoundMessage('role', 'ideation')]
    },

    {
      it: 'should return the IDs when a list field is requested even if they do not exist',
      employeeId: 'pam',
      path: 'roles',
      expected: ['designer', 'ideation']
    }
  ], async ({path, expected, employeeId}) => expect(await lookupPath('employee', employeeId ?? 'joe', path)).toEqual(expected));

});
