import { Injectable } from '@angular/core';

import { Observable, of, combineLatest, throwError } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { BoardConfig } from '../core/board-config';
import { BoardService } from '../services/board.service';
import { CardData } from '../api-types/gravity-api-cardview';

import { CardCacheService } from '../services/card-cache.service';
import flatten from 'lodash-es/flatten';


type LookupResult<T> = ValueEvent<T> | ErrorEvent;


interface ValueEvent<T> {
  type: 'value',
  value: T,
}

interface ErrorEvent {
  type: 'error',
  error: Error
}

function mkValueResult<T>(value: T): ValueEvent<T> {
  return {type: 'value', value};
}

function mkErrorResult(error: Error): ErrorEvent {
  return {type: 'error', error};
}


function getValueOrHandleError<T>(result: LookupResult<T>, handleError: (error: Error) => T): T {
  if (result === null || result === undefined) {
    return null;
  } else if (result.type === 'value') {
    return result.value;
  } else if (result.type === 'error') {
    return handleError(result.error);
  }
}

export interface PathLookupOptions {
  handleLookupError?: (error: Error) => any;
}


@Injectable({providedIn: 'root'})
export class PathLookupService {
  constructor (
    private boardService: BoardService,
    private cardCacheService: CardCacheService
  )  {}

  getField(card: CardData, path: string, options?: PathLookupOptions) {
    const errorFn = options?.handleLookupError ?? (error => error.message);

    return this.lookupCardField(card, path.split('.')).pipe(
      map(result => {
        if (Array.isArray(result)) {
          return result.map(r => getValueOrHandleError(r, errorFn));
        } else {
          return getValueOrHandleError(result, errorFn);
        }
      })
    );
  }

  private lookupCardField(card: CardData, pathSegments: string[]): Observable<LookupResult<any> | LookupResult<any>[]> {
    const field = pathSegments[0];
    const remainingSegments = pathSegments.slice(1);

    return this.boardService.getConfig(card.board).pipe(
      switchMap(boardConfig => {
        const fieldInfo = boardConfig.fields[field];
        const fieldValue = (fieldInfo && fieldInfo.isMetadata) ? card[field] :  (card[field] || card.fields[field]);

        if (fieldInfo && fieldInfo.optionlist && remainingSegments.length > 0) {
          if (fieldValue === null || fieldValue === undefined) {
            // Trying to do a nested lookup on a null/undefined field should be
            // specifically undefined.
            return of(mkValueResult(undefined));
          } else if (fieldInfo.type === 'LIST') {
            const values = Array.isArray(fieldValue) ? fieldValue : [];
            return this.lookupOverArray(fieldInfo.optionlist, values, remainingSegments);
          } else {
            return this.fetchAndLookup(fieldInfo.optionlist, fieldValue, remainingSegments);
          }
        } else {
          return of(mkValueResult(fieldValue));
        }
      })
    );
  }

  private fetchAndLookup(board: string, cardId: string, pathSegments: string[]) {
    return this.cardCacheService.get(board, cardId).pipe(
      switchMap(card => card
        ? this.lookupCardField(card, pathSegments)
        : of(mkErrorResult(new Error(`Not found: ${board}:${cardId}`)))
      )
    );
  }

  private lookupOverArray(board: string, cardIds: string[], pathSegments: string[]): Observable<LookupResult<any>[]> {
    if (cardIds.length === 0) {
      return of([]);
    } else {
      return combineLatest(
        cardIds.map(id => this.fetchAndLookup(board, id, pathSegments))
      ).pipe(
        map(results => {
          return flatten(results);
        })
      );
    }
  }
}
