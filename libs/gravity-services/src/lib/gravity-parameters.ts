import { InjectionToken } from '@angular/core';
import { GravityApiBoard } from './api-types/gravity-api-board-config';

export interface GravityParameters {
  /**
   * The base URL for Gravity API calls.
   *
   * This will be used by the GravityUrlService to generate URLs, and hopefully
   * one day, will be used by anything needing to make a call to Gravity.
   */
  baseUrl: string;

  /**
   * The preloaded board configs.
   */
  boardConfigs: { [id: string]: GravityApiBoard };

  /**
   * Custom Card classes to use on each board.
   *
   * This means that when the Gravity services create a Card from a board, it
   * will use this class instead. The current Gravity config service
   * implementation will monkey-patch and mutate these classes to add methods
   * and provide access to the current data store, which could cause issues if
   * you tried to reuse the same classes between two instances of the Gravity
   * services... maybe don't do that?
   *
   * These are a hold-over from IDP, and while useful for hard-coded
   * visualizations can be a bit confusing. In most cases, you probably don't
   * need to use these; the generic Card class provided by the config service
   * will work fine.
   */
  customCardModelClasses?: { [boardId: string]: Function};

  /**
   * The application id configured using gravity in the settings API.
   *
   * This variable is to be used to generate those API endpoints that have
   * application id. Eg. /gravity/spring/app/{appId}/ etc. to ensure all other
   * instances of the frontend application (eg. HR) don't CRUD on IDP's
   * application configuration.
   *
   */
  baseAppId: string;
}


export const GRAVITY_PARAMETERS = new InjectionToken<GravityParameters>('GRAVITY_PARAMETERS');
