import { mapObjectValues } from '@bmi/utils';
import { HttpParams, HttpRequest } from '@angular/common/http';


export function joinUrl(base: string, url: string) {
  if (url.startsWith('/')) {
    return base + url;
  } else {
    return base + '/' + url;
  }
}

export function queryParams(baseUrl: string, parameters: { [name: string]: string }): string {
  const params = mapObjectValues(
    parameters,
    (paramValue, paramName) => paramValue ? `${paramName}=${encodeURIComponent(paramValue)}` : null)
    .filter(p => p !== null);

  if (params.length > 0) {
    return baseUrl + '?' + params.join('&');
  } else {
    return baseUrl;
  }
}


export function extractParams(url: string): HttpParams {
  let queryString: string;
  if (url) {
    const qIndex = url.indexOf('?');
    queryString = qIndex >= 0 ? url.slice(qIndex + 1) : '';
  } else {
    queryString = '';
  }

  return new HttpParams({fromString: queryString});
}


export function requestMatcher(match: {url?: string, method?: string, params?: HttpParams | {[key: string]: string | string[]}}): (HttpRequest) => boolean {
  const expectedParams = match.params && (match.params instanceof HttpParams ? match.params : new HttpParams({fromObject: match.params}));
  return (request: HttpRequest<any>) => {
    if (match.method && request.method !== match.method.toUpperCase()) {
      return false;
    }

    if (match.url && match.url !== match.url.split('?')[0]) {
      return false;
    }

    if (expectedParams) {
      const actualParams = extractParams(request.urlWithParams);
      const allExpectedParamsMatch = expectedParams.keys().every(key => {
        const expected = expectedParams.getAll(key);
        const actual = actualParams.getAll(key);
        return expected && actual &&
            expected.length === actual.length &&
            expected.every(((value, index) => value === actual[index]));
      });

      if (!allExpectedParamsMatch) {
        return false;
      }
    }

    return true;
  };
}
