import { applyCardFilter } from './apply-gravity-filter';
import { makeCardJson } from '../testing/gravity-test-tools';
import { ObjectMap } from '@bmi/utils';
import { Filter } from '../model/filter';


interface FilteringTestCase {
  spec: string;
  filter: Filter;
  expected: string[];
}


describe('applyCardFilter', () => {
  function mkFilter(values: ObjectMap<string[]>) {
    return Filter.fromFilterValues('test', values);
  }

  const TEST_CARDS = makeCardJson('test', [
    {id: 'C1', fields: {name: 'foo', value: 6, active: false}},
    {id: 'C2', fields: {name: 'john', value: 88, active: true}},
    {id: 'C3', fields: {name: 'frank', value: 99, active: true}},
    {id: 'C4', fields: {name: 'sue', value: 101, active: true}},
    {id: 'C5', fields: {name: 'lev', value: -3, active: true}},
  ]);


  const TEST_CASES: FilteringTestCase[] = [
    {
      spec: 'should apply a simple string filter',
      filter: mkFilter({name: ['frank']}),
      expected: ['C3']
    },

    {
      spec: 'should filter on multiple values',
      filter: mkFilter({name: ['frank', 'john', 'sue']}),
      expected: ['C2', 'C3', 'C4']
    },

    {
      spec: 'should support numeric values',
      filter: mkFilter({value: ['88', '101']}),
      expected: ['C2', 'C4']
    },

    {
      spec: 'should handle negative numbers',
      filter: mkFilter({value: ['-3']}),
      expected: ['C5']
    },

    {
      spec: 'should support boolean true values',
      filter: mkFilter({active: ['true']}),
      expected: ['C2', 'C3', 'C4', 'C5']
    },

    {
      spec: 'should support boolean false values',
      filter: mkFilter({active: ['false']}),
      expected: ['C1']
    }
  ];


  for (const {spec, filter, expected} of TEST_CASES) {
    it(spec, () => {
      const filtered = applyCardFilter(TEST_CARDS, filter);
      expect(filtered.map(c => c.id)).toEqual(expected);
    })
  }
});
