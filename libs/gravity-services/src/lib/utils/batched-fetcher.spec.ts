import { Subject, Subscription, pipe } from 'rxjs';
import { tap, switchMapTo } from 'rxjs/operators';

import { BatchedFetcher } from './batched-fetcher';
import { ObjectMap } from '@bmi/utils';

import sortBy from 'lodash-es/sortBy';

class TestBatch<T> {
  public readonly results = new Subject<Map<string,T>>();
  constructor (public readonly items: string[]) {}

  sendResults(results: ObjectMap<T>) {
    this.results.next(new Map(Object.entries(results)));
    this.results.complete();
  }
}

describe("BatchedFetcher", () => {
  let batcher: BatchedFetcher<string>;
  let requestedBatches: TestBatch<string>[];
  let triggerAutoFlush: Subject<void>;
  let itemAddedCount: number;

  beforeEach(() => {
    requestedBatches = [];
    triggerAutoFlush = new Subject<void>();
    itemAddedCount = 0;
    batcher = new BatchedFetcher({
      fetch: items => {
        const testBatch = new TestBatch<string>(items);
        requestedBatches.push(testBatch      );
        return testBatch.results;
      },
      autoFlush: pipe(
        tap(() => itemAddedCount += 1),
        switchMapTo(triggerAutoFlush)
      )
    });
  });

  function requestItem(id: string): [string[], Subscription] {
    const receivedValues: string[] = [];
    const subscription = batcher.fetch(id).subscribe(value => receivedValues.push(value));
    return [receivedValues, subscription];
  }

  it('should have nothing to do initially', () => {
    expect(batcher.batchedItems).toEqual([]);
    expect(batcher.hasItemsToFetch).toBeFalsy();
  });

  it('should not initially trigger the autoflush pipeline', () => {
    expect(itemAddedCount).toBe(0);
  });

  it('should not flush if nothing has been requested', () => {
    batcher.flush();
    expect(requestedBatches.length).toBe(0);
  });

  describe('when requesting an item', () => {
    let receivedValues: string[];

    beforeEach(() => {
      [receivedValues] = requestItem('test-item');
    });

    it('should not return anything initially', () => expect(receivedValues).toEqual([]));
    it('should not start a batch automatically', () => expect(requestedBatches.length).toBe(0));
    it('should have items waiting to fetch', () => expect(batcher.hasItemsToFetch).toBeTruthy());

    it('should trigger the autoflush pipeline', () => {
      expect(itemAddedCount).toBe(1);
    });

    it('should trigger the autoflush pipeline every time an item is requested', () => {
      requestItem('foo');
      requestItem('bar');
      expect(itemAddedCount).toEqual(3); // the two above + 1 from beforeEach
    });

    it('should flush if the autoflush pipeline emits a value', () => {
      triggerAutoFlush.next();
      expect(requestedBatches.length).toBe(1);
    });

    it('should not request repeated batches if flush() called many times', () => {
      batcher.flush();
      batcher.flush();
      expect(requestedBatches.length).toBe(1);
    });

    it('should not request repeat batches if the auto trigger is called multiple times', () => {
      triggerAutoFlush.next();
      triggerAutoFlush.next();
      triggerAutoFlush.next();
      expect(requestedBatches.length).toBe(1);
    });

    describe('After calling .flush()', () => {
      beforeEach(() => batcher.flush());

      it('should open a batch', () => expect(requestedBatches.length).toBe(1));
      it('should pass the ids to the fetch function', () => {
        expect(requestedBatches[0].items).toEqual(['test-item']);
      });

      it('should have no batched items', () => expect(batcher.batchedItems).toEqual([]));
      it('should not have items to fetch', () => expect(batcher.hasItemsToFetch).toBeFalsy());

      it('should not emit any results yet', () => expect(receivedValues).toEqual([]));

      it('should put further requests into a new batch', () => {
        requestItem('foo');
        requestItem('bar');
        expect(batcher.batchedItems).toContain('foo');
        expect(batcher.batchedItems).toContain('bar');
      });

      describe('Then getting some results', () => {
        let batch: TestBatch<string>;
        beforeEach(() => batch = requestedBatches[0]);

        it('should emit the results matching the id', () => {
          batch.sendResults({
            'test-item': 'foobar'
          });

          expect(receivedValues).toEqual(['foobar']);
        });

        it('should return null if the result was not in the batch', () => {
          batch.sendResults({
            'foo': 'Some Value',
            'bar': 'thing2'
          });

          expect(receivedValues).toEqual([null]);
        });

        it('should return null if the response has null for that key', () => {
          batch.sendResults({
            'test-item': null
          });

          expect(receivedValues).toEqual([null]);
        });
      });
    });
  });

  it('should pass only distinct keys to fetch()', () => {
    requestItem('foo');
    requestItem('foo');
    requestItem('bar');
    requestItem('baz');
    batcher.flush();

    expect(sortBy(requestedBatches[0].items)).toEqual(['bar', 'baz', 'foo']);
  });



});
