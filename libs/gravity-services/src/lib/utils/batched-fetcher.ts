import { Subject, Observable, MonoTypeOperatorFunction, of, pipe, merge, NEVER, OperatorFunction, BehaviorSubject } from 'rxjs';
import { filter, map, mergeMap, catchError, retry, publishLast, refCount, take, auditTime, tap, publish, share } from 'rxjs/operators';

export type FetchFunction<T> = (batch: string[]) => Observable<Iterable<[string, T]>>;


export interface BatchedFetcherOptions<T> {
  fetch: FetchFunction<T>;
  autoFlush?: OperatorFunction<void, any>;
}


export class BatchedFetcher<T> {
  private requestSet = new Set<string>();

  // This awkward construction is needed because we add a new item to the set,
  // then trigger the event before the results stream is subscribed to. Using a
  // normal subject here will have no effect as there are no subscribers
  // watching. Instead, we use a BehaviorSubject to store whether a refresh is
  // needed, and when it's true, it will emit, but set itself back to false.
  private needToTriggerItemAdded = new BehaviorSubject<boolean>(false);
  private onItemAdded = this.needToTriggerItemAdded.pipe(
    filter(needsToTrigger => !!needsToTrigger),
    tap(() => this.needToTriggerItemAdded.next(false)),
    publish(),
    refCount()
  );

  private manualFlushTrigger = new Subject<void>();

  private results: Observable<Map<string, T>> = merge(
    this.manualFlushTrigger,
    this.options.autoFlush ? this.options.autoFlush(this.onItemAdded) : NEVER
  ).pipe(
    filter(() => this.hasItemsToFetch),
    mergeMap(() => {
      const ids = this.batchedItems;
      this.requestSet.clear();
      return this.requestBatch(ids);
    }),
    share()
  );

  constructor(private options: BatchedFetcherOptions<T>) {}

  private requestBatch(batchIds: string[]): Observable<Map<string, T>> {
    return this.options.fetch(batchIds).pipe(
      map(batchEntries => {
        // If we requested values, but they were not returned, assume that they
        // are not found. This behaviour is necessary due to the way the Gravity
        // card fetch API works -- if the card isn't found, it will be missing
        // from the results. This isn't a totally generic behaviour; and doesn't
        // handle incremental searches but these are unlikely to come up.
        const resultsMap = new Map<string, T>(batchEntries);
        for (const id of batchIds) {
          if (!resultsMap.has(id)) {
            resultsMap.set(id, null);
          }
        }
        return resultsMap;
      })
    );
  }

  flush() {
    this.manualFlushTrigger.next();
  }

  get batchedItems(): string[] {
    return Array.from(this.requestSet);
  }

  get hasItemsToFetch(): boolean {
    return this.requestSet.size > 0;
  }

  fetch(cardId: string): Observable<T> {
    this.requestSet.add(cardId);
    this.needToTriggerItemAdded.next(true);
    return this.results.pipe(
      filter(cards => cards.has(cardId)),
      map(cards => cards.get(cardId)),
      take(1),
      publishLast(),
      refCount()
    );
  }
}

