import { Card } from '../core/card';
import { Filter } from '../model/filter';
import { CardData } from '../api-types/gravity-api-cardview' ;
import { Condition } from '../model/condition';

/**
 * A utility for applying a Gravity filter to cards on the frontend.
 *
 * THIS IS AN INCOMPLETE IMPLEMENTATION AND IS REALLY ONLY INTENDED FOR TESTING
 * AND VERY RESTRICTED USE CASES!
 *
 * ONLY THE EQUALTO OPERATION IS SUPPORTED!!!
 * @deprecated this is the old IDP version of this.
 */
export function applyEqualToFilterOnFrontend<T extends Card = Card>(cards: T[], filter: Filter): T[] {
  return applyFilter(cards, filter, (card, field) => {
    const value = card[field];
    return (value && value instanceof Card ? value.id : value);
  });
}


export function applyFilter<T>(cards: T[], filter: Filter, getField: (card: T, field: string) => any): T[] {
  let filteredCards = cards;

  for (const condition of filter.conditions) {
    const values = condition.values;
    if (!values) {
      continue;
    } else if (values.length === 0) {
      // A filter without any values should not match anything. This case possibly
      // isn't even supported by Gravity due to the way multi-value filters are
      // implemented
      return [];
    } else {
      filteredCards = filteredCards.filter(makePredicate2(condition, getField));
    }
  }

  return filteredCards;
}


export function applyCardFilter<T extends CardData = CardData>(cards: T[], filter: Filter): T[] {
  return applyFilter(cards, filter, (c, f) => c.fields[f]);
}


function makePredicate2<T>(condition: Condition, getFieldValue: (card: T, field: string) => any): (x: T) => boolean {
  const field = condition.fieldName;
  if (condition.operation === 'EQUALTO') {
    const valueSet = new Set<any>(condition.values);
    // This is just relying on string comparison... which seems to work for the
    // basic types, but Gravity's actual behaviour is a little more
    // sophisticated than this...
    //
    // Numbers are the main issue that might give grief here, especially
    // non-integer values, but this seems like an uncommon use case
    return card => valueSet.has(String(getFieldValue(card, field)));
  } else if (condition.operation === 'NOTNULL') {
    return card => getFieldValue(card, field) !== null && getFieldValue(card, field) !== undefined;
  } else {
    throw new Error(`Unsupported condition operation: ${condition.operation}. Not all condition types can be applied on the frontend.`);
  }
}
