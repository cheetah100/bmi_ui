import { Observable } from 'rxjs';
import { GravityApiFilter } from '../api-types/gravity-api-filter';


export interface DataSourceGetOptions {
  gravityFilter?: GravityApiFilter;
}


export interface DataSource<T = any> {
  /**
   * The data source ID uniquely identifies this data source.
   *
   * This means it needs to encode enough information to specify the provider
   * type, the id or URL of the data source and any further options.
   *
   * The format of the string is arbitrary, left up to the data source
   * repositories to generate and parse.
   */
  dataSourceRef: string;

  /**
   * A user-friendly name for the data source
   */
  name: string;

  /**
   * An optional description of the data source.
   */
  description?: string;

  /**
   * The type of data provided by this source. This is used for determining
   * data source compatibility with charts, and data transformations.
   */
  dataType: string;

  /**
   * Executes the data source.
   *
   * This method takes an options object which can contain additional
   * information to control the desired fetch options, such as applying a
   * filter. The default DataSourceGetOptions interface includes provision for a
   * Gravity filter object, which should be used if the data source supports it.
   *
   * @param options
   * @returns an observable that will emit data from the source
   */
  getData(options?: DataSourceGetOptions): Observable<T>;
}
