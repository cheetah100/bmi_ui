# Data Sources

Unified API to enumerate available data sources


## What is a data source? ##

 * A board pivot
 * A transform
 * Cards from a board?

Some data sources can be easily enumerated (e.g. pivots), but the set of
possible data sources is potentially infinite, as sources could be configured
with specific combinations of parameters or complex options, depending on the
type of source.

A data source has an associated Repository service, which is responsible for
listing the available sources, or looking up data source references.

Data source repositories can be registered using Angular dependency injection
'multi' providers.

A data source can be identified using a DataSourceRef string. The format of
this string is left up to the data source repository that provides this type,
but it should uniquely identify the source and repository.

