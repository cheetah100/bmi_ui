import { Inject, Injectable } from '@angular/core';

import { combineLatest, Observable, of } from 'rxjs';
import { map, publishReplay, refCount } from 'rxjs/operators';

import { DataSource } from './data-source';
import { DATA_SOURCE_REPOSITORIES, DataSourceRepository } from './data-source-repository';


@Injectable()
export class DataSourceService implements DataSourceRepository {
  constructor(
    @Inject(DATA_SOURCE_REPOSITORIES) private dataSourceRepos: DataSourceRepository[]
  ) {
    if (!dataSourceRepos || dataSourceRepos.length === 0) {
      console.error('DataSourceService: No data source repositories are registered!');
    }
  }

  getSources(): Observable<DataSource[]> {
    return combineLatest(
      this.dataSourceRepos.map(repo => repo.getSources() || of([]))
    ).pipe(
      map(allSources => [].concat(...allSources)),
      publishReplay(1),
      refCount()
    );
  }

  lookup(dataSourceRef: string): Observable<DataSource> {
    if (!dataSourceRef) {
      throw new Error('Cannot lookup data source: DataSourceRef is falsy');
    }

    for (const repo of this.dataSourceRepos) {
      const lookupResult = repo.lookup(dataSourceRef);
      if (lookupResult) {
        return lookupResult.pipe(
          publishReplay(1),
          refCount()
        );
      }
    }

    throw new Error(`Unsupported Data Source: ${dataSourceRef}`);
  }
}
