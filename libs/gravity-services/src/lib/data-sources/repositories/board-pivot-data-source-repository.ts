import { Injectable } from'@angular/core';

import { Observable, of } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';

import { PivotData } from '../../services/gravity-transform.service';

import { DataSourceRepository } from '../data-source-repository';
import { DataSource, DataSourceGetOptions } from '../data-source';

import { GravityBoardPivotsService } from '../../services/gravity-board-pivots.service';


function parsePivotRef(ref: string) {
  const match = ref.match(new RegExp('^(gravity/board-pivot):(.*)$'));
  return match ? match[2] : null;
}

function generatePivotRef(url: string): string {
  return `gravity/board-pivot:${url}`;
}


class BoardPivotDataSource implements DataSource {
  readonly dataType = 'gravity/pivot-data';

  constructor(
    public readonly dataSourceRef: string,
    public pivotService: GravityBoardPivotsService,
    public readonly pivotUrl: string,
    public name: string = pivotUrl,
    public description: string = null
  ) {}

  getData(options: DataSourceGetOptions = {}): Observable<PivotData> {
    return this.pivotService.executePivot(this.pivotUrl, options && options.gravityFilter ? options.gravityFilter : null);
  }
}


@Injectable()
export class BoardPivotDataSourceRepository implements DataSourceRepository<BoardPivotDataSource> {
  private allPivotSources: Observable<BoardPivotDataSource[]> = this.pivotService.listAllPivots().pipe(
    map(pivotSummaries => {
      return pivotSummaries.map(s => new BoardPivotDataSource(generatePivotRef(s.url), this.pivotService, s.url, s.name))
    }),
    shareReplay(1)
  );

  constructor(private pivotService: GravityBoardPivotsService) {}

  getSources() {
    return this.allPivotSources;
  }

  lookup(dataSourceRef: string) {
    const url = parsePivotRef(dataSourceRef);
    if (!url) {
      return null;
    } else {
      return of(new BoardPivotDataSource(dataSourceRef, this.pivotService, url));
    }
  }
}
