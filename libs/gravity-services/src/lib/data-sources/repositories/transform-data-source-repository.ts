import { Injectable } from'@angular/core';

import { forkJoin, Observable, of } from 'rxjs';
import { map, publishLast, refCount } from 'rxjs/operators';

import { GravityTransformService, PivotData, TransformDetail } from '../../services/gravity-transform.service';
import { GravityUrlService } from '../../services/gravity-url.service';

import { DataSourceRepository } from '../data-source-repository';
import { DataSource, DataSourceGetOptions } from '../data-source';


const TRANSFORM_DS_TYPE = 'gravity/transform';
const TRANSFORM_REF_REGEX = /^(gravity\/transform):([\w\-\_]+)$/


function parseTransformRef(ref: string) {
  const match = ref.match(TRANSFORM_REF_REGEX);
  return match ? match[2] : null;
}

function generateTransformRef(transformId: string) {
  return `${TRANSFORM_DS_TYPE}:${transformId}`;
}

class TransformDataSource implements DataSource {

  constructor(
    public readonly dataSourceRef: string,
    private transformService: GravityTransformService,
    public readonly transformId: string,
    public readonly dataType: string,
    public name: string = transformId,
    public description: string = null
  ) {}

  getData(options: DataSourceGetOptions = {}) {
    return this.transformService.execute(this.transformId, options.gravityFilter);
  }
}


@Injectable()
export class TransformDataSourceRepository implements DataSourceRepository<TransformDataSource> {
  private allTransformSources = this.getValidTransforms().pipe(
    map(transforms => transforms.map(t => new TransformDataSource(
      generateTransformRef(t.id),
      this.transformService,
      t.id,
      this.getTransformDataType(t.type),
      t.title
    ))),
    publishLast(),
    refCount()
  );

  constructor(
    private transformService: GravityTransformService,
    private urls: GravityUrlService
  ) {}

  getTransformDataType(transformType: string) {
    return transformType === 'pivot' ? 'gravity/pivot-data' :`gravity/transform-${transformType}`;
  }

  getValidTransforms() {
    return forkJoin(
      ['pivot', 'transpose'].map(type => this.transformService.getTransformsByType(type))
    ).pipe(
      map(results => <TransformDetail[]>[].concat(...results))
    );
  }

  getSources() {
    return this.allTransformSources;
  }

  lookup(ref: string) {
    const transformId = parseTransformRef(ref);
    if (transformId) {
      return this.transformService.get(transformId).pipe(
        map(config => new TransformDataSource(
          generateTransformRef(transformId),
          this.transformService,
          transformId,
          this.getTransformDataType(config.type),
          config.name
        )),
        publishLast(),
        refCount()
      );
    } else {
      return null;
    }
  }
}
