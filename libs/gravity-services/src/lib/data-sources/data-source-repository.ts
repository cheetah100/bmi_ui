import { InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';

import { DataSource } from './data-source';


/**
 * The injection token for registering data source repositories with the Data Sources API.
 */
export const DATA_SOURCE_REPOSITORIES = new InjectionToken<DataSourceRepository<DataSource>[]>('Gravity Data Source Repositories');


export interface DataSourceRepository<T extends DataSource = DataSource> {
  /**
   * Get the known data sources from this repository.
   *
   * This method can return null if listing all sources doesn't make sense for
   * this type of repository; or it can emit an empty array.
   */
  getSources(): Observable<T[]> | null;


  /**
   * Look up a data source reference in this repository.
   *
   * This method needs to decide if this repository provides the requested
   * source. If not, (i.e. the type doesn't match) then return null
   * synchronously to pass responsibility along to the next registered
   * repository.
   *
   * Otherwise, return an observable that will emit the data source, or an error
   * if the data source cannot be found.
   */
  lookup(dataSourceId: string): Observable<T> | null;
}
