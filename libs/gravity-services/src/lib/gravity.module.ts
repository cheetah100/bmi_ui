import { NgModule, ModuleWithProviders } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BoardHistoryRepository } from './core/board-history.repository';
import { DashboardRepository } from './dashboard/dashboard.repository';
import { DataStore } from './core/data-store';
import { Gravity } from './gravity';
import { GravityCache } from './core/gravity-cache';
import { GravityConfigService } from './core/gravity-config.service';
import { GravityConverter } from './core/gravity-converter';
import { GravityService } from './core/gravity.service';
import { TransformRepository } from './core/transform.repository';
import { UserRepository } from './core/user.repository';

import { GravityPrefsRepository } from './services/gravity-prefs.repository';
import { GravityResourceService } from './services/gravity-resource.service';
import { GravityUrlService } from './services/gravity-url.service';
import { RulesService } from './services/rules.service';
import { GravityTransformService } from './services/gravity-transform.service';
import { GravityViewService } from './services/gravity-view.service';
import { GravityFilterService } from './services/gravity-filter.service';
import { GravityBoardPivotsService } from './services/gravity-board-pivots.service';
import { GravityWidgetService } from './services/gravity-widget.service';
import { GravityFileUploaderService } from './services/gravity-spreadsheet-uploader.service';
import { GravityUploadStatusService } from './services/gravity-upload-status.service';
import { GravityTimelineService } from './services/gravity-timeline.service';
import { BoardService } from './services/board.service';

import { DATA_SOURCE_REPOSITORIES } from './data-sources/data-source-repository';
import { DataSourceService } from './data-sources/data-source.service';
import { BoardPivotDataSourceRepository } from './data-sources/repositories/board-pivot-data-source-repository';
import { TransformDataSourceRepository } from './data-sources/repositories/transform-data-source-repository';
import { CredentialService } from './services/credential.service';
import { GravityHttpClient } from './core/gravity-http-client';

@NgModule({})
export class GravityModule {
  static forRoot(
    options = {
      registerDataSources: true
    }
  ): ModuleWithProviders<GravityModule> {
    return {
      ngModule: GravityModule,
      providers: [
        BoardHistoryRepository,
        DashboardRepository,
        DataStore,
        Gravity,
        GravityCache,
        GravityConfigService,
        GravityConverter,
        GravityPrefsRepository,
        GravityResourceService,
        GravityService,
        GravityUrlService,
        RulesService,
        TransformRepository,
        GravityTransformService,
        UserRepository,
        GravityViewService,
        GravityFilterService,
        GravityBoardPivotsService,
        GravityWidgetService,
        GravityFileUploaderService,
        GravityUploadStatusService,
        GravityTimelineService,
        DataSourceService,
        BoardService,
        CredentialService,


        options.registerDataSources
          ? [
              {
                provide: DATA_SOURCE_REPOSITORIES,
                multi: true,
                useClass: BoardPivotDataSourceRepository
              },
              {
                provide: DATA_SOURCE_REPOSITORIES,
                multi: true,
                useClass: TransformDataSourceRepository
              }
            ]
          : []
      ]
    };
  }
}
