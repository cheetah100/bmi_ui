import { Permissions } from '../core/board-config';

export interface DashboardWidget {
  widgetId: string;
  draggable: boolean;
  positionX: number;
  positionY: number;
  width: number;
  height: number;
}


export interface Dashboard {
  id: string,
  name: string,
  description: string,

  // Uh, for some reason there seems to be some confusion about what the
  // icon/image is called. I've added both properties for now, but Gravity seems
  // to use icon_path.
  imagePath?: string,
  icon_path?: string,
  widgets: DashboardWidget[],
  permissions?: Permissions
}
