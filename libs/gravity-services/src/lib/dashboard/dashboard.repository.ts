
import {refCount, publishLast, map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DataStore } from '../core/data-store';
import { Dashboard } from './dashboard';

@Injectable()
export class DashboardRepository {

  constructor(
    private http: HttpClient,
    private dataStore: DataStore,
  ) {}

  getAll(): Observable<Dashboard[]> {
    // TODO: eventually the backend should support a call to get the full dashboards in one api call
    // for now we will just do a transform to fake it
    const source = this.http.get<{[id: string]: Dashboard}>('/gravity/spring/dashboard').pipe(
      map(dashboards => Object.values(dashboards)),
      publishLast(),
      refCount());

    source.subscribe(dashboards => {
      this.dataStore.set('dashboards', dashboards);
    }, (error) => {});

    return source;
  }

  getDashboard(id: string): Observable<Dashboard> {
    const source = this.http.get<Dashboard>(`/gravity/spring/dashboard/${id}`).pipe(
      map(dashboard => {
        // TODO: remove these once the backend supports these
        dashboard.imagePath = '/assets/images/icon-IntegratedChangeView.png';
        dashboard.description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ullamcorper nulla nec urna aliquet pharetra. Pellentesque ac metus dui. Mauris nec malesuada arcu. Nulla sit amet dolor nec est pellentesque iaculis. Proin pharetra non leo in cursus. Fusce ultricies dolor vitae porta tempus. Phasellus ut pulvinar turpis.';
        return dashboard;
      }),
      publishLast(),
      refCount(),);

    source.subscribe(users => {}, (error) => {});
    return source;
  }

  save(dashboard: Dashboard): Observable<Dashboard> {
    const source = this.http.put<Dashboard>(`/gravity/spring/dashboard/${dashboard.id}`, dashboard).pipe(
      publishLast(),
      refCount()
    );

    source.subscribe(dashboard => {
      this.dataStore.addOrUpdate('dashboards', dashboard);
    }, (error) => {});

    return source;
  }

  create(dashboard: Dashboard): Observable<Dashboard> {
    const source = this.http.post<Dashboard>('/gravity/spring/dashboard', dashboard).pipe(
      publishLast(),
      refCount()
    );

    source.subscribe(createdDashboard => {
      this.dataStore.addOrUpdate('dashboards', createdDashboard);
    }, (error) => {});

    return source;
  }

  edit(dashboard: Dashboard): Observable<Dashboard> {
    return this.save(dashboard);
  }

  delete(dashboard: Dashboard): Observable<void> {
    const source = this.http.delete(`/gravity/spring/dashboard/${dashboard.id}`).pipe(
      publishLast(),
      refCount()
    );
    source.subscribe(dashboard => {
      this.dataStore.remove('dashboards', dashboard)
    }, (error) => { });
    return source;
  }
}
