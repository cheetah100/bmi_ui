import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { mapObjectValues, ObjectMap, Omit, sanitizeId, uniqueId } from '@bmi/utils';

import { GravityUrlService } from './gravity-url.service';
import { GravityApiFilter } from '../api-types/gravity-api-filter';
import { cloneDeep } from 'lodash-es';
import { Permissions } from '../..';


// including types with the service to allow library export with ng-packagr.
// suggest moving types to a types definition file when ng-packagr can handle it

export type TransformType = 'pivot' | 'transpose';

export type TransformData = PivotData | TransposeData | DataseriesData;

export interface TransformerConfig {
  // id?: string;
  configuration: ObjectMap<string | string[]>;
  order: number;
  transformer: string;
}

export interface PivotData {
  data: number[][];
  datasources: string[];
  xAxis: string[];
  yAxis: string[];
}

export interface TransposeDataObject {
  [key: string]: string | number | string[] | ObjectMap<string>;
}

export interface TransposeData {
  columns: string[];
  data: TransposeDataObject[];
}
export interface DataPoint {
  x: number;
  y: number;
}

export interface DataseriesDataObject {
  id: string;
  items: DataPoint[];
  fields: ObjectMap<any>;
}
export interface DataseriesData extends ObjectMap<DataseriesDataObject> {
}

export interface TransformDetail {
  title: string;
  id: string;
  type?: TransformType | string;
}

export interface TransformConfig {
  id: string;
  metadata: ObjectMap<number | string>;
  name: string;
  transforms: ObjectMap<TransformerConfig>;
  type: string;
  description: string;
  active: boolean;
  publish: boolean;
  uniqueid: string;
  version: string;
  ui: null;
  permissions: Permissions
}

export const TransformerConfig = {
  clone(transformer: TransformerConfig) {
    return TransformerConfig.fromJson(TransformerConfig.toJson(transformer));
  },

  create(transformer:string): TransformerConfig {
    return {
      configuration: {},
      order: 0,
      transformer: transformer
    }
  },

  fromJson(transformJson: TransformerConfig, actionKey: string = null) {
    const transformer = cloneDeep(transformJson);
    // transformer.id = actionKey || transformer.id;
    return transformer;
  },

  fromJsonObject(transformsObj: { [id: string]: TransformerConfig }): TransformerConfig[] {
    return mapObjectValues(transformsObj || {}, (a, id) => TransformerConfig.fromJson(a, id))
      .sort((a, b) => a.order - b.order);
  },

  toJson(transformer: TransformerConfig): TransformerConfig {
    return cloneDeep(transformer);
  },
}

export interface TransformJson extends Omit<TransformConfig, 'transforms'> {
  transforms: {},
}
export interface Transform extends Omit<TransformConfig, 'transforms'> {
  isNew: boolean;
  transforms: Array<TransformerConfig>;
  permissions: Permissions;
}

export interface Transformer extends TransformerConfig {
}

export const Transformer = {
  clone(transformer: TransformerConfig) {
    return TransformerConfig.fromJson(TransformerConfig.toJson(transformer));
  },

  create(transformer:string): TransformerConfig {
    return {
      configuration: {},
      order: 0,
      transformer: transformer
    }
  },

  fromJson(transformJson: TransformerConfig, actionKey: string = null) {
    const transformer = cloneDeep(transformJson);
    // transformer.id = actionKey || transformer.id;
    return transformer;
  },

  fromJsonObject(transformsObj: { [id: string]: TransformerConfig }): TransformerConfig[] {
    return mapObjectValues(transformsObj || {}, (a, id) => TransformerConfig.fromJson(a, id))
      .sort((a, b) => a.order - b.order);
  },

  toJson(transformer: TransformerConfig): TransformerConfig {
    return cloneDeep(transformer);
  },
}

export const Transform = {
  // clone(transform: Transform) {
  //   return Transform.fromJson(Transform.toJson(transform), true);
  // },

  create(id: string = null): Transform {
    return {
      uniqueid: sanitizeId(uniqueId(id)),
      id: id ,
      name: null,
      metadata: {},
      transforms: [],
      // permissions: {},
      type: null,
      ui: null,
      // parameters: {}
      isNew: true,
      description: '',
      active: true,
      publish: false,
      version: '',
      permissions: {}
    };
  },

  fromJson(json: TransformConfig):Transform  {
    return {
      isNew: false,
      id: json.id,
      name: json.name,
      metadata: json.metadata,
      type: json.type,
      description: json.description,
      active: json.active,
      publish: json.publish,
      uniqueid: json.uniqueid,
      version: json.version,
      ui: json.ui,
      transforms: Transformer.fromJsonObject(json.transforms || {}),
      permissions: json.permissions
    };
  },

  // fromJsonObject(actionsObj: { [id: string]: TransformConfig }): Transform[] {
  //   return mapObjectValues(actionsObj || {}, (a, id) => Transform.fromJson(a, id))
  //     // .sort((a, b) => a.order - b.order);
  // },


  toJson(transform: Transform): TransformJson {
    const newTransform:any = cloneDeep(transform);
    const keyGen = keyGenerator('step');
    const transformsObject = {};
    const newTransformers = newTransform.transforms || [];

    newTransformers.forEach((tr, index) => {
      transformsObject[`step_${index}`] = tr;
    });

    newTransform.transforms = transformsObject;
    return newTransform;
  },
}

function* keyGenerator(prefix:string = '') {
  let i:number = 0
  while(true) {
    i += 1;
    yield `${prefix}${i}`;
  }
}
@Injectable()
export class GravityTransformService {

  constructor(
    private http: HttpClient,
    private urls: GravityUrlService
  ) { }

  get(id: string): Observable<TransformConfig> {
    return this.http.get<TransformConfig>(this.urls.transform.byId(id).baseUrl);
  }

  getAll():Observable<TransformDetail[]> {
    return this.http.get<ObjectMap<string>>(this.urls.transform.list())
      .pipe(map(
        (transforms: ObjectMap<string>) => Object.entries(transforms).map(
          ([id, title]: [string, string]) => ({ id, title})
        )
      ));
  }

  getTransformsByType(type: TransformType | string): Observable<TransformDetail[]> {
    return this.http.get<ObjectMap<string>>(this.urls.transform.listByType(type))
      .pipe(map(
        (transforms: ObjectMap<string>) => Object.entries(transforms).map(
          ([id, title]: [string, string]) => ({ id, title, type })
        )
      ));
  }

  create(payload: TransformConfig): Observable<TransformConfig> {
    return this.http.post<TransformConfig>(`${this.urls.transform.baseUrl}?findParams=true`, payload);
  }

  update(id: string, payload: TransformConfig): Observable<TransformConfig> {
    return this.http.put<TransformConfig>(`${this.urls.transform.byId(id).baseUrl}?findParams=true`, payload);
  }

  delete(id: string) {
    return this.http.delete(this.urls.transform.byId(id).baseUrl);
  }

  preview(id: string): Observable<TransformData> {
    return this.http.get<TransformData>(this.urls.transform.byId(id).execute());
  }

  configPreview(config: TransformConfig): Observable<TransformData> {
    return this.http.post<TransformData>(this.urls.transform.execute(), config);
  }

  getIdFromApiUrl(transformUrl: string): string {
    const match = new RegExp('^/transform/([\\w\\_\\-]+)($|/.*)').exec(transformUrl);
    if (!match) {
      throw new Error(`Transform API URL not recognised: ${transformUrl}`);
    } else {
      return match[1];
    }
  }

  execute<T extends TransformData>(id: string, filter?: GravityApiFilter) {
    let url, payload;
    if (filter) {
      url = this.urls.transform.byId(id).executeFiltered();
      payload = filter;
    } else {
      url = this.urls.transform.byId(id).execute();
      payload = {};
    }

    return this.http.post<T>(url, payload);
  }
}
