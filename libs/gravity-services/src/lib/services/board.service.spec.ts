import { TestBed } from '@angular/core/testing';
import { HttpParams } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController, TestRequest } from '@angular/common/http/testing';
import { ReplaySubject, Observable } from 'rxjs'
import { count, last } from 'rxjs/operators';

import { BoardService, BoardQueryParams, BoardQueryBuilder } from './board.service';
import { GravityUrlService } from './gravity-url.service';
import { CardData } from '../api-types/gravity-api-cardview';
import { GravityApiCondition } from '../api-types/gravity-api-condition';

import { requestMatcher, extractParams } from '../utils/url-tools';
import { CardDataWithPartialMetadata, makeCardJson } from '../testing/gravity-test-tools';

import sortBy from 'lodash-es/sortBy';


function makeCard(boardId: string, card: CardDataWithPartialMetadata) {
  return makeCardJson(boardId, [card])[0];
}


describe('BoardService', () => {
  let service: BoardService;
  let urlService: GravityUrlService;
  let httpController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [BoardService, GravityUrlService]
    })

    service = TestBed.get(BoardService) as BoardService;
    httpController = TestBed.get(HttpTestingController) as HttpTestingController;
    urlService = TestBed.get(GravityUrlService) as GravityUrlService;
  });

  afterEach(() => httpController.verify());

  function boardSearchUrl(boardId: string) {
    return urlService.board(boardId).url('search');
  }

  describe('search()', () => {

    it('should not make requests until subscribed', () => {
      service.search({boardId: 'test_board'});
      httpController.verify();
    });

    describe('performing a simple search of the board', () => {
      let results: ReplaySubject<CardData[]>;

      beforeEach(() => {
        results = new ReplaySubject<CardData[]>();
        service.search({boardId: 'test_board',}).subscribe(results);
      });

      it('should POST to search URL', () => {
        httpController.expectOne(requestMatcher({url: boardSearchUrl('test_board'), method: 'POST'}));
      });

      it('should use the view "all" by default', () => {
        httpController.expectOne(requestMatcher({url: boardSearchUrl('test_board'), params: {'view': 'all'}}));
      });

      describe('when returning results', () => {
        // These aren't really valid cards, but let's just pretend.
        const TEST_DATA = [{id: 'card1'}, {id: 'card2'}] as CardData[];
        let request: TestRequest;
        beforeEach(() => {
          request = httpController.expectOne(requestMatcher({url: boardSearchUrl('test_board')}));
          request.flush(TEST_DATA);
        });

        it('should complete', () => {
          expect(results.isStopped).toBeTruthy();
        });

        it('should return a single result', () => {
          let resultCount: number;
          results.pipe(count()).subscribe(c => resultCount = c);
          expect(resultCount).toBe(1);
        });

        it('should return the HTTP result pretty much verbatim', () => {
          let cards: CardData[];
          results.pipe(last()).subscribe(r => cards = r);
          expect(cards).toEqual(TEST_DATA);
        });
      });


      describe('should not set other params', () => {
        let request: TestRequest;
        let params: HttpParams;
        beforeEach(() => {
          request = httpController.expectOne(requestMatcher({url: boardSearchUrl('test_board'), method: 'POST'}));
          params = extractParams(request.request.urlWithParams);
        });

        it('should not set the order parameter', () => expect(params.has('order')).toBeFalsy());
        it('should not set the descending parameter', () => expect(params.has('descending')).toBeFalsy());
      });
    });


    describe('order parameter', () => {
      it('should set a param in the request', () => {
        service.search({boardId: 'test_board', order: 'testfield'}).subscribe();
        httpController.expectOne(requestMatcher({params: {order: 'testfield'}}));
      });
    });

    describe('descending parameter', () => {
      it('should not set the parameter if it is not in the query', () => {
        service.search({boardId: 'test'}).subscribe();
        const request = httpController.expectOne(requestMatcher({url: boardSearchUrl('test')}));
        const params = extractParams(request.request.urlWithParams);
        expect(params.has('descending')).toBeFalsy();
      });

      it('should set the parameter if truthy', () => {
        service.search({boardId: 'test', descending: true}).subscribe();
        const request = httpController.expectOne(requestMatcher({url: boardSearchUrl('test')}));
        const params = extractParams(request.request.urlWithParams);
        expect(params.get('descending')).toBe('true');
      });
    });

    describe('with no filter conditions', () => {
      it('should set the conditions in the request body to {}', () => {
        service.search({boardId: 'test_board'}).subscribe();
        const request = httpController.expectOne(requestMatcher({url: boardSearchUrl('test_board')}));
        expect(request.request.body).toEqual({
          conditions: {}
        });
      });
    });

    describe('with filter conditions', () => {
      const TEST_CONDITIONS: GravityApiCondition[] = [
        { fieldName: 'otherfield', operation: "NOTNULL", value: null },
        { fieldName: 'test', operation: "EQUALTO", value: 'foobar' },
      ];

      it('should add the conditions in the body', () => {
        service.search({boardId: 'test_board', conditions: TEST_CONDITIONS}).subscribe();
        const request = httpController.expectOne(requestMatcher({url: boardSearchUrl('test_board')}));
        const bodyConditions = request.request.body.conditions;

        const sortedBodyConditions = sortBy(Object.values(bodyConditions), 'fieldName');
        expect(sortedBodyConditions).toEqual(sortBy(TEST_CONDITIONS, 'fieldName'));
      })
    });
  });


  describe('query()', () => {
    it('should set the board id', () => expect(service.query('testBoard').query.boardId).toBe('testBoard'));

    it('should initially create a minimal query', () => {
      expect(service.query('testBoard').query).toEqual({
        boardId: 'testBoard'
      });
    });

    it('should set the view id if requested', () => {
      expect(service.query('testBoard').view('test').query.viewId).toBe('test');
    });

    it('should execute the query', () => {
      service.query('testBoard').run().subscribe();
      httpController.expectOne(requestMatcher({url: boardSearchUrl('testBoard')}));
    });

    describe('Filtering', () => {
      let qb: BoardQueryBuilder;

      beforeEach(() => qb = service.query('test_board'));

      it('should add an array of filters', () => {
        qb.filterBy([
          { fieldName: 'test', operation: 'EQUALTO', value: 'test' },
          { fieldName: 'foobar', operation: 'EQUALTO', value: 'test' },
        ]);

        expect(qb.query.conditions).toBeDefined();
        expect(qb.query.conditions.length).toBe(2);
      });

      it('should add a single filter', () => {
        qb.filterBy({fieldName: 'test', operation: 'EQUALTO', value: 'foobar'});
        expect(qb.query.conditions).toBeDefined();
        expect(qb.query.conditions.length).toBe(1);
      });

      it('should support chaining', () => {
        const TEST_CONDITIONS: GravityApiCondition[] = [
          {fieldName: 'foo', operation: 'EQUALTO', value: 'foobar'},
          {fieldName: 'bar', operation: 'NOTCONTAINS', value: 'bat'}
        ];

        qb.filterBy(TEST_CONDITIONS[0])
          .filterBy(TEST_CONDITIONS[1]);

        expect(qb.query.conditions).toBeDefined();
        expect(qb.query.conditions).toEqual(TEST_CONDITIONS);
      });
    });
  });

  describe('saveCard()', () => {
    it('should not make any requests until subscribed', () => {
      const testCard = makeCard('test_board', {
        fields: {
          'test': 'foobar'
        }
      });

      service.saveCard('test_board', testCard);
      httpController.verify();
    });

    it('should do a POST if there is no card ID', () => {
      const testCard = makeCard('test_board', {id: null, fields: {}});
      service.saveCard('test_board', testCard).subscribe();
      httpController.expectOne(requestMatcher({
        method: 'POST',
        url: urlService.board('test_board').cards()
      }));
    });

    it('should do a PUT to the correct URL if there is an ID', () => {
      const testCard = makeCard('test_board', {id: 'C001', fields: {foo: 'bar'}});
      service.saveCard('test_board', testCard).subscribe();
      httpController.expectOne(requestMatcher({
        method: 'PUT',
        url: urlService.board('test_board').card('C001').baseUrl
      }));
    });


    describe('Observable behaviour: publishLast semantics', () => {
      const card = makeCard('test_board', {id: 'C001', fields: {foo: 'bar'}});
      let source: Observable<CardData>;

      beforeEach(() => {
        source = service.saveCard('test_board', card);
      });

      it('should not make any requests until a subscription is made', () => {
        httpController.verify();
      });

      describe('Once subscribed',
        () => {
          let emittedCards: CardData[];
          let hasCompleted = false;
          let req: TestRequest;

          beforeEach(
            () => {
              emittedCards = [];
              hasCompleted = false;
              source.subscribe(
                {
                  next: card =>
                    emittedCards.push(
                      card
                    ),
                  complete: () =>
                    (hasCompleted = true)
                }
              );

              req = httpController.expectOne(
                requestMatcher(
                  {
                    method:
                      'PUT'
                  }
                )
              );
            }
          );

          it('should complete after the HTTP response arrives', () => {
            req.flush(
              card
            );
            expect(
              hasCompleted
            ).toBeTruthy();
          });

          it('should emit the value from the HTTP response body', () => {
            req.flush(
              card
            );
            expect(
              emittedCards
            ).toEqual(
              [
                card
              ]
            );
          });

          /**
           * TODO: fix this test
           */
          // it('should not re-request on subsequent subscriptions, return the last result', () => {
          //   req.flush(card);
          //   let theCard: CardData = undefined;
          //   source.subscribe(value => theCard = value);
          //   expect(theCard).toEqual(card);
          // });
        });
    });


  });
});
