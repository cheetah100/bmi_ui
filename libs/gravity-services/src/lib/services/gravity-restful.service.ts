import { catchError, publishLast, refCount, retry, tap } from 'rxjs/operators';
import { EMPTY, Observable, of, throwError } from 'rxjs';
import { ObservableMap } from '@bmi/utils';
import { GravityHttpClient } from '../core/gravity-http-client';
import { HttpErrorResponse } from '@angular/common/http';

export type StringFunc = () => string;

export interface GravityRestfulServiceOptions<TKey> {
  /**
   * Create the url for an individual record
   * @param {TKey} ref
   */
  itemUrl: (ref: TKey) => string;
  httpClient: GravityHttpClient;
  collectionUrl?: StringFunc;
  listStyle?: string;
  /**
   * Create the TKey for the given record
   * @param record
   * @returns {TKey} key
   */
  keyFunction: (record: any) => TKey;
  considerRefreshTrigger?: Observable<any>;
  maxAge?: number;
  emitStaleEntries?: boolean;
}

/**
 * GravityRestfulService provides an HTTP service with built-in caching
 *
 * It can be used as a base or standalone class.

 * Example:
 * interface CardKey {
 *   board: string
 *   card string;
 * }
 * interface Card {
 *   board: string
 *   card: string;
 *   name: string;
 * }
 * const service = new GravityRestfulService<RecordKey, Record>({
 *     itemUrl: ref => `/board/${ref.board}/cards/${ref.card}`,
 *     collectionUrl: () => `/board`,
 *     httpClient: httpClient,
 *     keyFunction: record => {
 *       return {
 *         board: record.board,
 *         card: record.card
 *       }
 *     }
 *   });
 *
 * It uses 2 generics:
 * TKey: the data type used to identify individual records
 * T:    the record data type
 */
export class GravityRestfulService<TKey, T> {
  constructor(
    public options: GravityRestfulServiceOptions<TKey>,

  ) {}

  private baseUrl = ''
  private defaultOptions = {
    maxAge: 30000,
    emitStaleValues: false,
    ...this.options,
  };

  private cache = new ObservableMap<TKey, T>({
    source: id => this._fetchRecord(id),
    ...this.defaultOptions,
    considerRefreshTrigger: this.options?.considerRefreshTrigger || EMPTY,
    keyFunction: this.getMapKey
  });

  fetchRecordById(ref:TKey, {invalidateNow = false} = {}) {
    if (invalidateNow) {
      this.cache.invalidate(ref);
    }
    return this.cache.watch(ref)
  }

  fetchCollection(): Observable<T[]> {
    const url = this.getCollectionUrl();
    return this.options.httpClient.get<T[]>(url);
  }

  invalidateRecordById(ref:TKey) {
    this.cache.invalidate(ref);
  }

  refreshRecord(ref: TKey) {
    this.cache.refresh(ref);
  }

  isLoading(ref: TKey) {
    return this.cache.isLoading(ref);
  }

  saveRecord(record: T, isNew:boolean) {
    const ref = this.options.keyFunction(record);
    const source = isNew
      ? this.options.httpClient.post<T>(this.getCollectionUrl(), record)
      : this.options.httpClient.put<T>(this.getItemUrl(ref), record);

    return source.pipe(
      tap(() => {
        this.cache.invalidate(ref);
      })
    );
  }

  /**
   * Generic HTTP POST function
   * @param endPoint
   * @param payload
   */
  httpPost(endPoint:string, payload:any) {
    const url = `${this.baseUrl}${endPoint}`;
    return this.options.httpClient.post(url, payload);
  }

  private _fetchRecord(id): Observable<T> {
    const url = this.getItemUrl(id);
    console.log('_fetchRecord => url: ', url);
    return this.options.httpClient.get<T>(url).pipe(
      retry(3),
      publishLast(),
      refCount(),
      catchError( (err) => {
        this.handleError(err);
        return of<T>(null)
      }),
    );
  }

  private getMapKey(key: TKey) {
    if (typeof key === 'object') {
      return JSON.stringify(key)
    }
    return key;
  }

  private getItemUrl(id:TKey) {
    return `${this.baseUrl}${this.options.itemUrl(id)}`;
  }

  private getCollectionUrl() {
    return `${this.baseUrl}${this.options.collectionUrl()}`;
  }

  private handleError(error: HttpErrorResponse) {
    let errorMessage: string;
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }
}



