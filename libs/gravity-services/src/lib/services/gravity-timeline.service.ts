import { Injectable } from '@angular/core';
import { GravityUrlService } from '../services/gravity-url.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CardData } from '../api-types/gravity-api-cardview';
import { map, publishReplay, refCount } from 'rxjs/operators';

export interface Quarter {
  id: string;
  name: string;
  startDate: number;
  endDate: number;
  fiscalYear: number;
  fiscalQuarter: number;
}

/**
 * This utility exposes time based information related to quarters and fiscal months.
 * Time Calculations are performed here so the calculation logic remains central.
 */
@Injectable()
export class GravityTimelineService {

  constructor(
    private gravityUrlService: GravityUrlService,
    private http: HttpClient
  ) { }

  quarters: Observable<Quarter[]> = null;

  /**
   * Returns all the quarters available in the system
   * Sort: Latest First
   */
  getAllQuarters(): Observable<Quarter[]> {
    if (!this.quarters) {
      this.quarters = this.http.get<CardData<Quarter>[]>(this.gravityUrlService.url(`/board/quarter/cards?order=startDate&descending=true`))
      .pipe(
        map((cards: CardData<Quarter>[]) => cards.map(card => {
          return { ...card.fields, ...{id: card.id }};
        })),
        publishReplay(),
        refCount()
      );
    }
    return this.quarters;
  }

  /**
   * Returns the current quarter
   *
   * Note: This assumes the startDate and endDate of the quarter cards
   * (on the Quarter Gravity board) are correctly configured
   *
   * Calculation: Picks the first quarter card where the given day
   * lies in the startDate and endDate range (inclusive).
   */
  getCurrentQuarter(): Observable<Quarter> {
    return this.getAllQuarters().pipe(
      map((quarters: Quarter[]) => quarters.find(quarter => {
        return quarter.startDate <= Date.now() && quarter.endDate >= Date.now();
      })
    ));
  }

  /**
   * Returns the previous quarter
   *
   * Note: This assumes the startDate and endDate of the quarter cards
   * (on the Quarter Gravity board) are correctly configured
   */
  getPreviousQuarter(): Observable<Quarter> {
    return this.getAllQuarters().pipe(
      map((quarters: Quarter[]) => {
        const curQuarterIdx = quarters.findIndex(quarter => {
          return quarter.startDate <= Date.now() && quarter.endDate >= Date.now();
        });
        if (curQuarterIdx == -1) {
          // Current quarter not found
          return null;
        }

        const prevQuarterIdx = (curQuarterIdx + 1 >= quarters.length) ? -1 :
          (curQuarterIdx + 1);
        if (prevQuarterIdx == -1) {
          // No previous quarter
          return null;
        }
        return quarters[prevQuarterIdx];
      })
    );
  }

  /**
   * Returns the selected number of past quarters leading up to and
   * including the current quarter
   *
   * Note: This assumes the startDate and endDate of the quarter cards
   * (on the Quarter Gravity board) are correctly configured
   */
  getCurrentAndPastQuarters(numPastQuarters: number): Observable<Quarter[]> {
    return this.getAllQuarters().pipe(
      map((quarters: Quarter[]) => {
        const curIdx = quarters.findIndex(quarter => {
          return quarter.startDate <= Date.now() && quarter.endDate >= Date.now();
        });
        const endIdx = (curIdx + numPastQuarters >= quarters.length) ? quarters.length : (curIdx + numPastQuarters);
        return quarters.slice(curIdx, endIdx + 1);
      })
    );
  }

  /**
   * Returns the selected number of past quarters leading up to and
   * including the requested quarter
   *
   * Note: This assumes the startDate and endDate of the quarter cards
   * (on the Quarter Gravity board) are correctly configured
   */
  getQuarterAndPastQuarters(startQuarterId: string, numPastQuarters: number): Observable<Quarter[]> {
    return this.getAllQuarters().pipe(
      map((quarters: Quarter[]) => {
        const startIdx = quarters.findIndex(quarter => {
          return quarter.id === startQuarterId;
        });

        // Check if the quarter was not found
        if (startIdx < 0) {
          return [];
        }

        const endIdx = (startIdx + numPastQuarters >= quarters.length) ? quarters.length : (startIdx + numPastQuarters);
        return quarters.slice(startIdx, endIdx + 1);
      })
    );
  }

  /**
   * Returns the list of quarters from the range of the given quarter ids
   *
   * Note: startQuarterId signifies a quarter with a timestamp greater than
   * the quarter signified by endQuarterId
   *
   * Note: Assumes the startQuarterId is less than or equal to the endQuarterId
   * and both quarter ids are valid else an empty array of quarters is returned
   *
   * Note: This assumes the startDate and endDate of the quarter cards
   * (on the Quarter Gravity board) are correctly configured
   */
  getAllQuartersInThisRange(endQuarterId: string, startQuarterId: string): Observable<Quarter[]> {
    return this.getAllQuarters().pipe(
      map((quarters: Quarter[]) => {
        const startIdx = quarters.findIndex(quarter => {
          return quarter.id === startQuarterId;
        });
        const endIdx = quarters.findIndex(quarter => {
          return quarter.id === endQuarterId;
        });

        if (!endIdx || !startIdx || (endIdx < startIdx)) {
          return [];
        }
        return quarters.slice(startIdx, endIdx + 1);
      })
    );
  }

  /**
   * Returns the string representation of the yearId
   */
  getYearIdToString(yearId: number) {
    return `Q${yearId.toString().substr(4, 1)} FY${yearId.toString().substr(0, 4)}`;
  }
}
