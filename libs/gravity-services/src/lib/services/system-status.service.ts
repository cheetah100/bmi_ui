import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, interval } from 'rxjs';
import { GravityUrlService } from './gravity-url.service';
import { CacheEntry } from '@bmi/utils';
import ms from 'ms';
import { GravityApiSystemReport, GravityApiSystemReportDetails, GravityApiSystemRevisionInfo } from '../api-types/gravity-api-system-report';

@Injectable({providedIn: 'root'})
export class SystemStatusService {
  // We could auto-trigger a refresh here, but the CacheEntry has some issues
  // around error handling that can result in a flood of requests.
  private refreshTrigger = null;  //interval(ms('1 second'));

  private _systemStatus = new CacheEntry({
    source: () => this.fetchSystemInfo<GravityApiSystemReport>('/system'),
    maxAge: ms('15 seconds'),
    considerRefreshTrigger: this.refreshTrigger
  });

  private _detailedReport = new CacheEntry({
    source: () => this.fetchSystemInfo<GravityApiSystemReportDetails>('/system/report'),
    maxAge: ms('5 minutes'),
    considerRefreshTrigger: this.refreshTrigger,
  });

  private _revision = new CacheEntry({
    source: () => this.fetchSystemInfo<GravityApiSystemRevisionInfo>('/system/revision'),
    maxAge: ms('10 minutes'),
    considerRefreshTrigger: this.refreshTrigger
  });

  constructor(
    private http: HttpClient,
    private urls: GravityUrlService,
  ) {}


  getStatus() {
    this._systemStatus.refresh();
    return this._systemStatus.watch();
  }

  getDetails() {
    return this._detailedReport.watch();
  }

  getRevisionInfo() {
    return this._revision.watch();
  }

  /**
   * Force detailed report data to refresh
   */
  refreshDetails() {
    this._detailedReport.refresh();
  }

  private fetchSystemInfo<T>(url: string): Observable<T> {
    return this.http.get<T>(this.urls.url(url));
  }
}
