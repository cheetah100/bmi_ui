import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { GravityUrlService } from './gravity-url.service';
import { GravityApiCardEvent } from '../api-types/gravity-api-card-event';

@Injectable({providedIn: 'root'})
export class GravityCardAlertService {
  constructor (
    private http: HttpClient,
    private urls: GravityUrlService
  ) {}

  getAllAlertsForBoard(boardId: string): Observable<GravityApiCardEvent[]> {
    const url = this.urls.board(boardId).url('/alerts')
    return this.http.get<GravityApiCardEvent[]>(url);
  }

  getAlertsForCard(boardId: string, cardId: string): Observable<GravityApiCardEvent[]> {
    const url = this.urls.board(boardId).card(cardId).alerts();
    return this.http.get<GravityApiCardEvent[]>(url);
  }

  dismissCardAlert(boardId: string, cardId: string, alert: GravityApiCardEvent | string): Observable<void> {
    const alertId = typeof alert === 'string' ? alert : alert.uuid;
    const url = this.urls.board(boardId).card(cardId).dismissAlert(alertId);
    return this.http.get(url).pipe(map(() => undefined));
  }

  dismissCardAlerts(boardId: string, alertsToBeDeleted: string[]): Observable<void> {
    const url = this.urls.board(boardId).alerts() + `/dismiss`;
    const body = {
      alerts: alertsToBeDeleted,
      all: false
    }
    return this.http.post(url, body).pipe(map(() => undefined));
  }
}
