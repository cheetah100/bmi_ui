import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GravityUrlService } from './gravity-url.service';
import { GravityApiCardTask, GravityApiCardTaskAction } from '../api-types/gravity-api-card-task';

@Injectable({providedIn: 'root'})
export class CardTaskService {
  constructor(
    private http: HttpClient,
    private urls: GravityUrlService,
  ) {}

  fetchTasks(boardId: string, cardId: string): Observable<GravityApiCardTask[]> {
    return this.http.get<GravityApiCardTask[]>(this.urls.board(boardId).card(cardId).tasks());
  }

  performAction(boardId: string, cardId: string, taskId: string, action: GravityApiCardTaskAction): Observable<boolean> {
    const taskUrl = this.urls.board(boardId).card(cardId).task(taskId);
    return this.http.post<boolean>(taskUrl, action);
  }

  revertTaskOnCard(boardId: string, cardId: string, taskId: string): Observable<boolean> {
    return this.performAction(boardId, cardId, taskId, {taskAction: 'REVERT'});
  }

  completeTask(boardId: string, cardId: string, taskId: string): Observable<boolean> {
    return this.performAction(boardId, cardId, taskId, {taskAction: 'COMPLETE'});
  }

  revertTaskOnAllCards(boardId: string, taskId: string) {
    return this.http.post(this.urls.board(boardId).url('tasks/revert'), taskId);
  }
}
