import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import {
  catchError,
  filter,
  map,
  publishLast,
  refCount,
  retry,
  switchMap,
  auditTime,
} from 'rxjs/operators';

import { GravityConfigService } from '../core/gravity-config.service'
import { BoardConfig } from '../core/board-config';
import { GravityUrlService } from './gravity-url.service';
import { CardData, CardFields } from '../api-types/gravity-api-cardview';
import { GravityApiCondition } from '../api-types/gravity-api-condition';
import { GravityApiBoard } from '../api-types/gravity-api-board-config';

import { joinUrl, queryParams } from '../utils/url-tools';

import { createMapFrom, ObservableCache, ObjectMap } from '@bmi/utils';

import { BatchedFetcher } from '../utils/batched-fetcher';


export interface BoardQueryParams {
  boardId: string,
  viewId?: string,
  conditions?: GravityApiCondition[],
  order?: string,
  descending?: boolean
}


/**
 * Defines the public interface for the Board Service
 *
 * These methods should be provided by the FakeBoardService for the testbed.
 * Plenty of code is using the BoardService ctor as the injection token, so we
 * don't want to make this an abstract class.
 *
 * Typescript doesn't like us 'implementing' the BoardService from the fake one
 * because it requires all private fields to also be implemented. We won't see
 * these problems via Angular DI because it's not as strongly typed.
 */
export interface IBoardService {
  search<T extends CardFields = CardFields>(query: BoardQueryParams): Observable<CardData<T>[]>;
  query<T extends CardFields = CardFields>(boardId: string): BoardQueryBuilder<T>;
  getConfig(boardId: string): Observable<BoardConfig>;
  fetchById(boardId: string, cardId: string): Observable<CardData>;
  getTitles(boardId: string): Observable<ObjectMap<string>>;
  saveCard(boardId: string, card: CardData): Observable<CardData>;
}


/**
 * A lightweight service for accessing card data in Gravity.
 *
 * This is intended to replace the functionality in IDP's GravityService and
 * BMI's Gravity service.
 *
 * Unlike the aforementioned services, this should *just* service card data and
 * other basic operations on a board. There should be no caching or change
 * notification. It should be easy to mock out, and the APIs should be nice
 * to use.
 */
@Injectable({providedIn: 'root'})
export class BoardService implements IBoardService {
  private cardBatchers = new Map<string, CardFetchBatcher>();

  // The main role of this cache is just to publish the results of fetching the
  // board config. Once fetched it will just echo out any other changes to the
  // board config from the config service.
  //
  // If the board already exists, it will just watch that board for changes.
  private _boardFetchCache = new ObservableCache({
    factory: boardId => {
      if (this.configService.boardExists(boardId)) {
        return this.configService.watchBoardConfig(boardId);
      } else {
        return this._fetchAndRegisterBoardConfig(boardId).pipe(
          switchMap(() => this.configService.watchBoardConfig(boardId))
        );
      }
    }
  });

  constructor(
    private httpClient: HttpClient,
    private urlService: GravityUrlService,
    private configService: GravityConfigService
  ) {}

  search<T extends CardFields = CardFields>(query: BoardQueryParams): Observable<CardData<T>[]> {
    const baseUrl = this.urlService.board(query.boardId).url('search');

    const params = {
      order: query.order,
      descending: query.descending ? 'true' : undefined,
      view: query.viewId || 'all'
    };

    const payload = {
      conditions: {}
    };

    if (query.conditions) {
      query.conditions.forEach((c, index) => payload.conditions[`_${index}`] = c);
    }

    return this.httpClient.post<CardData<T>[]>(queryParams(baseUrl, params), payload);
  }

  query<T extends CardFields = CardFields>(boardId: string) {
    return new BoardQueryBuilder(this, boardId);
  }

  /**
   * Gets a board config, fetching if necessary.
   *
   * This may emit multiple updates, if the board config gets updated, as it
   * uses watchBoardConfig() on the GravityConfigService.
   *
   * If a config is already available locally in the GravityConfigService it
   * will use that, otherwise the config will be fetched and registered.
   *
   * This should be the preferred method for looking up board configs in BMI as
   * we don't want to pre-load all configs onto the client and the existing
   * interfaces in the GravityConfigService doesn't permit async fetching.
   */
  getConfig(boardId: string): Observable<BoardConfig> {
    if (!this._boardFetchCache.checkFreshness(boardId)) {
      this._boardFetchCache.refresh(boardId);
    }
    return this._boardFetchCache.watch(boardId);
  }

  /**
   * Change the phase of specified card
   * This method does not interact with the dataStore
   * @param boardName
   * @param cardId
   * @param newPhase
   */
  moveCard(boardName:string, cardId: string, newPhase:string) {
    return this.httpClient.post<CardData>(
      this.urlService.moveCard(boardName, cardId),
      { phase: newPhase}
    );

  }
  private _fetchAndRegisterBoardConfig(boardId: string): Observable<BoardConfig> {
    return this.httpClient.get<GravityApiBoard>(this.urlService.board(boardId).baseUrl).pipe(
      retry(3),
      map(config => this.configService.registerBoard(config)),
    );
  }

  deleteCard(boardId: string, cardId: string): Observable<CardData> {
    const boardUrl = this.urlService.board(boardId);
    const source = this.httpClient.delete<CardData>(boardUrl.card(cardId).baseUrl);

    return source;
  }
  /**
   * Fetch a card by ID, with a bit of smart request batching.
   *
   * This will batch up the cards that are being requested to avoid
   * over-fetching entire boards, but also to minimize the total number of
   * requests being made.
   *
   * Naturally, this needs to introduce a short delay to buffer up these
   * requests.
   */
  fetchById(boardId: string, cardId: string): Observable<CardData> {
    if (!this.cardBatchers.has(boardId)) {
      this.cardBatchers.set(boardId, new CardFetchBatcher(boardId, this.httpClient, this.urlService));
    }

    return this.cardBatchers.get(boardId).fetch(cardId);
  }

  getTitles(boardId: string): Observable<{[id: string]: string}> {
    const url = this.urlService.board(boardId).cardTitles();
    return this.httpClient.get<{[id: string]: string}>(url, {
      params: {
        allcards: "true"
      }
    }).pipe(
      publishLast(),
      refCount()
    );
  }

  /**
   * Saves a card to Gravity
   *
   * This should map closely to the low-level operations in Gravity. By building
   * on top of a simple function it should make it easier to mock out for
   * testing
   *
   * @param boardId the board to save to
   * @param cardData the card to save to Gravity
   * @returns a cold observable that will perform the update when subscribed
   */
  saveCard(boardId: string, cardData: CardData): Observable<CardData> {
    const isNew = !cardData.id;
    const boardUrl = this.urlService.board(boardId);

    const source = isNew ?
      this.httpClient.post<CardData>(boardUrl.cards(), cardData) :
      this.httpClient.put<CardData>(boardUrl.card(cardData.id).baseUrl, cardData.fields);

    return source;
  }
}


export class BoardQueryBuilder<T extends CardFields = CardFields> {
  public query: BoardQueryParams;

  constructor(private service: IBoardService, boardId: string) {
    this.query = {
      boardId: boardId
    };
  }

  view(viewId: string) {
    this.query.viewId = viewId;
    return this;
  }

  descending(sortDescending = true) {
    this.query.descending = sortDescending;
    return this;
  }

  orderBy(fieldName: string) {
    this.query.order = fieldName;
    return this;
  }

  filterBy(filterConditions: GravityApiCondition | GravityApiCondition[]) {
    this.query.conditions = [
      ...(this.query.conditions || []),
      ...(Array.isArray(filterConditions) ? filterConditions : [filterConditions])
    ];
    return this;
  }

  run<T>(): Observable<CardData<T>[]> {
    return this.service.search<T>(this.query);
  }
}


export class CardFetchBatcher {
  private fetchBatcher = new BatchedFetcher<CardData>({
    fetch: batch => this.fetchCardBatch(batch),
    autoFlush: auditTime(20)
  });

  constructor(
    private board: string,
    private http: HttpClient,
    private urlService: GravityUrlService
  ) {}

  fetch(cardId: string) {
    return this.fetchBatcher.fetch(cardId);
  }

  private fetchCardBatch(cardIds: string[]) {
    return this.http.post<CardData[]>(this.urlService.board(this.board).cardList(), cardIds).pipe(
      retry(3),
      catchError((err) => { console.log('Card batch error', err); return of(<CardData[]>[])}),
      map(cards => createMapFrom(cards, c => c.id))
    );
  }
}
