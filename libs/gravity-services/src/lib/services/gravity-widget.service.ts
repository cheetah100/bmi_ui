import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { publishLast, refCount } from 'rxjs/operators';

import { GravityUrlService } from './gravity-url.service';
import { GravityApiWidget } from '../api-types/gravity-api-widget';


@Injectable()
export class GravityWidgetService {
  constructor(
    private http: HttpClient,
    private url: GravityUrlService
  ) {}

  getAppWidgets(appId: string): Observable<GravityApiWidget[]> {
    return this.http.get(this.url.app(appId).allWidgets()).pipe(
      publishLast(),
      refCount()
    );
  }

  getWidget(appId: string, widgetId: string): Observable<GravityApiWidget> {
    return this.http.get(this.url.app(appId).widget(widgetId)).pipe(
      publishLast(),
      refCount()
    );
  }

  putWidget(appId: string, widgetId: string, widget: GravityApiWidget): Observable<GravityApiWidget> {
    return this.http.put(this.url.app(appId).widget(widgetId), widget).pipe(
      publishLast(),
      refCount()
    );
  }

  createWidget(appId: string, widget: GravityApiWidget): Observable<GravityApiWidget> {
    return this.http.post(this.url.app(appId).allWidgets(), widget).pipe(
      publishLast(),
      refCount()
    );
  }

  deleteWidget(appId: string, widgetId: string): Observable<void> {
    return this.http.delete(this.url.app(appId).widget(widgetId)).pipe(
      publishLast(),
      refCount()
    );
  }
}
