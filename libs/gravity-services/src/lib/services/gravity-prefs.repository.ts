import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, Subject } from 'rxjs';
import { tap, filter, publishLast, refCount } from 'rxjs/operators';

import { GravityUrlService } from '../services/gravity-url.service';
import { UserRepository } from '../core/user.repository';
import { Preference, PreferenceValue } from '../api-types/gravity-api-preference';



/**
 * A low level preferences service for interacting with the Gravity API
 *
 * Unlike the other preferences services in IDP that cache the results, this one
 * just handles updating data directly in Gravity, with no caching or
 * observables, debounced updates etc.
 *
 * This service can provide access to the preferences of other users too. In the
 * ideal world, Gravity's API would allow a user to update their own prefs via
 * the admin API too, but it doesn't, so this service also handles switching out
 * which API is called based on whether the userId is the current user.
 */
@Injectable()
export class GravityPrefsRepository {
  private readonly preferenceUpdateSubject = new Subject<Preference>();

  constructor(
    private urls: GravityUrlService,
    private http: HttpClient,
    private userRepository: UserRepository
  ) { }

  get onCurrentUserPrefsUpdated(): Observable<Preference> {
    return this.preferenceUpdateSubject.pipe(
      filter(pref => pref.userId === this.userRepository.currentUser.id)
    );
  }

  private isCurrentUser(userId: string) {
    return this.userRepository.currentUser.id === userId;
  }

  getPreference(userId: string, preferenceId: string): Observable<Preference> {
    if (this.isCurrentUser(userId)) {
      return this.http.get<Preference>(this.urls.prefForCurrentUser(preferenceId));
    } else {
      return this.http.get<Preference>(this.urls.prefForUser(userId, preferenceId));
    }
  }

  /**
   * Updates a preference, merging any keys with existing values.
   *
   * This relies on the Gravity Preferences API POST operation merging the
   * existing preferences. The behaviour of this API is a bit annoying, as the
   * PUT won't let us create new preferences either, and the distinction between
   * a preference's name and id is ridiculous and confusing!
   */
  updatePreference(userId: string, preferenceId: string, value: PreferenceValue): Observable<Preference> {
    const url = this.isCurrentUser(userId) ?
      this.urls.prefCollectionForCurrentUser()
      : this.urls.prefCollectionForUser(userId);

    const source = this.http.post<Preference>(url, <Preference>{
      // The current (2019-01-15) Gravity implementation has a different
      // behaviour when POSTing current user and "admin" preferences. For the
      // current user it will look at the id, whereas updating another user's
      // prefs will generate the id from the preferenceId.
      id: preferenceId,
      name: preferenceId,
      values: value
    }).pipe(
      tap(preference => this.preferenceUpdateSubject.next(preference)),
      publishLast(),
      refCount(),
    );

    source.subscribe();
    return source;
  }
}