import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { GravityApiView } from '../api-types/gravity-api-view';
import { Template, BoardConfig } from '../core/board-config';
import { GravityConfigService } from '../core/gravity-config.service';
import { ObjectMap, Option } from '@bmi/utils';
import { GravityUrlService } from './gravity-url.service';

@Injectable()
export class GravityViewService {

  constructor(
    private httpClient: HttpClient,
    private gravityConfigService: GravityConfigService,
    private gravityUrlService: GravityUrlService,
  ) { }

  getViewConfig(boardId: string, viewId: string): Observable<GravityApiView> {
    const url: string = this.gravityUrlService.board(boardId).view(viewId);
    return this.httpClient.get<GravityApiView>(url);
  }

  saveView(boardId: string, viewConfig: GravityApiView): Observable<GravityApiView> {
    const viewId: string = viewConfig.id;
    let httpRequest: Observable<GravityApiView> = null;
    if (viewId) {

      // It seems like Gravity might be behaving differently now.
      // Omitting the id causes gravity to generate a new id from name,
      // Saving a view using the new id and leaving the original view in place.

      // delete viewConfig.id;

      httpRequest = this.httpClient.put<GravityApiView>(
        this.gravityUrlService.board(boardId).view(viewId),
        viewConfig
      );
    } else {
      httpRequest = this.httpClient.post<GravityApiView>(
        this.gravityUrlService.board(boardId).viewList(),
        viewConfig
      );
    }
    return httpRequest;
  }

  getTemplateConfig(boardId: string): Template {
    const boardConfig: BoardConfig = this.gravityConfigService.getBoardConfig(boardId);
    return boardConfig.templates[boardId];
  }

  getBoardConfig(boardId: string): BoardConfig {
    return this.gravityConfigService.getBoardConfig(boardId);
  }

  getAllBoardInfo(board): Observable<any> {
    return this.httpClient.get(this.gravityUrlService.board(board).baseUrl);
  }

  /**
   * The api uses template field names as keys and not the board id.
   * It also only returns one entry for that board. So if multiple fields share the same board as optionlist,
   * All but one of those fields are missing in the response.
   * So, converting the key to board id. Then having to do a lookup on form events to work out the optionlist for a template field.
   */
  getAllBoardsOptions(boardId): Observable<any> {
    return this.httpClient
      .get(`gravity/spring/board/${boardId}/lists/form/listsforview`)
      .pipe(
        map(response => {
          const optionsList: ObjectMap<Option[]> = {};
          const template: Template = this.getTemplateConfig(boardId);
          Object.entries(response).forEach(([key, values]: [string, any]) => {
            const optionlist: string = template.fields[key].optionlist;
            optionsList[optionlist] = values.map(value => ({
              text: value.title,
              value: value.id
            }));
          });
          return optionsList;
        })
      );
  }

  // legacy
  getAllOptionListCards(boardId): Observable<any> {
    return this.httpClient.get(`gravity/spring/board/${boardId}/lists/form/listsforview`).pipe(map(response => {
      Object.keys(response).forEach(option => {
        response[option].map((item: ObjectMap<string>) => {
          item.value = item.id;
          item.text = item.title;
        });
      });
      return response;
    }));
  }

}
