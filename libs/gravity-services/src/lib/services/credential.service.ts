import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import ms from 'ms';
import { Credential, CredentialSummary } from '../model/credential';

import { GravityUrlService } from './gravity-url.service';
import { CacheEntry, ObjectMap, ObservableMap } from '@bmi/utils';
import { GravityApiCredential } from '../api-types/gravity-api-credential';
import { map, tap } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class CredentialService {
  private credentialSummaries = new CacheEntry({
    source: () => this.fetchCredentialSummaries(),
    maxAge: ms('60 minutes')
  });

  private credentials = new ObservableMap<string, Credential>({
    source: id => this.fetchCredential(id),
    maxAge: ms('60 minutes'),
  });

  constructor(private http: HttpClient, private urls: GravityUrlService) {}

  get(id: string, {invalidateNow = false} = {}): Observable<Credential> {
    if (invalidateNow) {
      this.credentials.invalidate(id);
    }

    return this.credentials.watch(id);
  }

  list(): Observable<CredentialSummary[]> {
    return this.credentialSummaries.watch();
  }

  delete(id: string) {
    const url = this.urls.url(`/cred/${id}`);
    return this.http.delete(url).pipe(
      tap(() => {
        this.credentials.invalidate(id);
        this.credentialSummaries.invalidate();
      }));
  }

  private fetchCredentialSummaries(): Observable<CredentialSummary[]> {
    const url = this.urls.credentialsList();
    return this.http.get<ObjectMap<string>>(url).pipe(
      map((creds: ObjectMap<string>) => {
        let summaries = [];
        for (let [key, value] of Object.entries(creds)) {
          summaries.push({"id": key, "name": value});
        }
        return summaries;
      })
    );
  }

  private fetchCredential(credentialId: string): Observable<Credential> {
    const url = this.urls.url(`/cred/${credentialId}`);
    return this.http.get<GravityApiCredential>(url).pipe(
      map(json => Credential.fromJSON(json))
    );
  }

  save(credential: Credential) {
    const json = Credential.toJSON(credential);
    const source = credential.isNew
      ? this.http.post<GravityApiCredential>(this.urls.url('/cred'), json)
      : this.http.put<GravityApiCredential>(this.urls.url(`cred/${credential.id}`), json);

    return source.pipe(
      tap(() => {
        this.credentials.invalidate(credential.id);
        this.credentialSummaries.invalidate();
      })
    )
  }
}
