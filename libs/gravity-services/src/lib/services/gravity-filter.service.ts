import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GravityUrlService } from './gravity-url.service';
import { Observable } from 'rxjs';

@Injectable()
export class GravityFilterService {

  constructor(
    private http: HttpClient,
    private settings: GravityUrlService
  ) {}

  getFiltersForBoard(boardId: string): Observable<any> {
    return this.http.get(this.settings.url(`/board/${boardId}/filters`));
  }

  getFilter(boardId: string, filterId: string): Observable<any> {
    return this.http.get(this.settings.url(`/board/${boardId}/filters/${filterId}`));
  }

  addFilter(boardId: string, filter: any): Observable<any> {
    return this.http.post(this.settings.url(`/board/${boardId}/filters`), filter);
  }

  updateFilter(boardId: string, filterId: string, filter: any): Observable<any> {
    return this.http.put(this.settings.url(`/board/${boardId}/filters/${filterId}`), filter);
  }

  deleteFilter(boardId: string, filterId: string): Observable<any> {
    return this.http.delete(this.settings.url(`/board/${boardId}/filters/${filterId}`));
  }
}
