import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, of as observableOf, combineLatest } from 'rxjs';
import { map, catchError, take, tap } from 'rxjs/operators';

import { GravityUrlService } from './gravity-url.service';

import { ObservableMap, sanitizeGravityApiId } from '@bmi/utils';

import sortBy from 'lodash-es/sortBy';


@Injectable()
export class GravityResourceService {
  private boardResources = new Map<string, ObservableMap<string, BoardResource>>();

  private boardResourceSummaries = new ObservableMap<string, BoardResourceSummary[]>({
    source: boardId => this.getResourceList(boardId),
    maxAge: 5 * 60 * 1000
  });

  constructor(
    private gravityUrl: GravityUrlService,
    private httpClient: HttpClient
  ) { }

  /** @deprecated */
  listResources(boardId: string): Observable<string[]> {
    return this.boardResourceSummaries.watch(boardId).pipe(
      map(summaries => summaries.map(s => s.id)),
      take(1)
    );
  }

  /** @deprecated */
  getResource(boardId: string, resourceId: string): Observable<string> {
    return this.httpClient.get(this.gravityUrl.boardResource(boardId, resourceId), { responseType: 'text' });
  }

  saveResource(boardId: string, resourceId: string, resourceContent: string): Observable<BoardResource> {
    return this.httpClient.post(
      this.gravityUrl.board(boardId).resource(resourceId) + '/text',
      resourceContent,
      { responseType: 'text' }
    ).pipe(
      map(response => {
        return {
          // The 'id' in the REST API is used as the name of the resource
          // internally in Gravity, and it sanitizes the ID by translating
          // non-word characters to underscores and lowercasing.
          //
          // This is kind of annoying because it happens silently, so if we POST
          // our resource, it will be different in the listing and won't match
          // up if searching by ID.
          //
          // Ideally, Gravity would give us a response like this with the real
          // ID, but for now we're just guessing. This sanitize function should
          // give the same results as the Gravity code.
          id: sanitizeGravityApiId(resourceId),
          name: resourceId,
          type: null,
          boardId: boardId,
          isNew: false,

          // The gravity API wouldn't give us back the content as a string like
          // this but it might be useful.
          resource: resourceContent,
          content: resourceContent,
        };
      })
    );
  }

  list(boardId: string, {refreshNow = false} = {}): Observable<BoardResourceSummary[]> {
    if (refreshNow) {
      this.boardResourceSummaries.refresh(boardId);
    }

    return this.boardResourceSummaries.watch(boardId);
  }

  get(boardId: string, resourceId: string, {invalidateNow = false} = {}): Observable<BoardResource> {
    if (invalidateNow) {
      this.getBoardResourceCache(boardId).invalidate(resourceId);
    }

    return this.getBoardResourceCache(boardId).watch(resourceId);
  }

  /**
   * Saves a board resource
   *
   * This returns a cold observable, so you must subscribe to it before the save
   * will take place. It will emit the saved board resource, and as a side
   * effect will cause the cache to refresh for this board.
   */
  save(boardId: string, resource: BoardResource): Observable<BoardResource> {
    return this.saveResource(boardId, resource.id, resource.resource).pipe(
      tap(result => {
        this.boardResourceSummaries.refresh(boardId);
        if (this.boardResources.has(boardId)) {
          this.boardResources.get(boardId).refresh(result.id);
        }
      }),
    );
  }

  delete(resource: BoardResource | BoardResourceSummary | ResourceHandle): Observable<void> {
    const url = this.gravityUrl.board(resource.boardId).resource(resource.id);
    return this.httpClient.delete(url).pipe(
      tap(() => {
        const boardId = resource.boardId;
        this.boardResourceSummaries.refresh(resource.boardId);
        if (this.boardResources.has(boardId)) {
          this.boardResources.get(boardId).invalidate(resource.id);
        }
      }),
      map(() => undefined)
    );
  }

  private getResourceList(boardId: string): Observable<BoardResourceSummary[]> {
    return this.httpClient.get<{ [id: string]: string }>(this.gravityUrl.boardResourceList(boardId)).pipe(
      map(resources => Object.entries(resources)
        .map(([id, name]) => <BoardResourceSummary>{
          id: id,
          name: name,
          type: null,
          boardId: boardId,
        })),
      map(resourceSummaries => sortBy(resourceSummaries, r => r.name))
    );
  }

  private fetchResource(boardId: string, resourceId: string): Observable<BoardResource> {
    const resourceUrl = this.gravityUrl.board(boardId).resource(resourceId);
    return combineLatest([
      this.httpClient.get(resourceUrl, { responseType: 'text'}),
      this.httpClient.get<BoardResource>(resourceUrl + '/details')
    ]).pipe(
      map(([resourceContent, resourceDetails]) => {
        return {
          id: resourceId,
          name: resourceId,
          boardId: boardId,
          type: null,
          isNew: false,

          // now include the details from the Gravity API. This could overwrite
          // the above
          ...resourceDetails,
          resource: resourceContent,
          content: resourceContent
        }
      })
    );
  }

  private getBoardResourceCache(boardId: string) {
    if (!this.boardResources.has(boardId)) {
      const cache = new ObservableMap<string, BoardResource>({
        source: cardId => this.fetchResource(boardId, cardId),
        maxAge: 5 * 60 * 1000,
        emitStaleEntries: true
      });
      this.boardResources.set(boardId, cache);
    }
    return this.boardResources.get(boardId);
  }

}
// Data uniquely identifying a Resource
export interface ResourceHandle {
  id: string;
  boardId: string;
}

export interface BoardResourceSummary extends ResourceHandle {
  name: string;
  type: string;
}



/**
 * Represents a Gravity board resource
 *
 * The Gravity API doesn't actually give a useful response like this so our
 * service on the frontend will fake it.
 */
export interface BoardResource extends BoardResourceSummary {
  /**
   * The Gravity ID of the resource.
   */
  id: string;

  /**
   * The contents of the resource, encoded as a string.
   */
  resource: string;

  /**
   * Indicates whether a resource should be created, or an existing one
   * overwritten. This doesn't exist in Gravity, but can be used when saving a
   * resource to determine whether it needs to be created -- as IDs are
   * user-provided, we cannot use a null ID to confirm this.
   */
  isNew?: boolean;

  /**
   * @deprecated: this is an alias for 'resource' and is being kept only to
   * allow compatibility with existing code (for now).
   *
   * Use `resource` instead.
   */
  content?: string;
}
