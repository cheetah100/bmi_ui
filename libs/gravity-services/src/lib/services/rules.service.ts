import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Rule } from '../model/rule';
import { GravityApiRule } from '../api-types/gravity-api-rule';

import { GravityUrlService } from './gravity-url.service';
import { sortBy } from '@bmi/utils';


@Injectable()
export class RulesService {
  constructor(
    private urls: GravityUrlService,
    private httpClient: HttpClient
  ) { }

  getRuleSummaries(boardId: string): Observable<RuleSummary[]> {
    return this.httpClient.get<GravityApiRule[]>(this.urls.board(boardId).ruleListFull()).pipe(
      map(results => results.map(rule => <RuleSummary>{
        id: rule.id,
        name: rule.name,
        boardId,
        ruleType: rule.ruleType,
        index: rule.index || 0
      })),

      // Sort the rules by their index first, then by name to break ties. Rule
      // index order is really only used by task rules at the moment, and will
      // default to 0 for the purpose of this sort.
      map(ruleSummaries => sortBy(ruleSummaries, s => [s.index, s.name]))
    );
  }

  getAllRules(boardId: string): Observable<Rule[]> {
    return this.httpClient.get<GravityApiRule[]>(this.urls.board(boardId).ruleListFull()).pipe(
      map(rulesJson => rulesJson.map(r => Rule.fromJSON(r)))
    );
  }

  getRule(boardId: string, ruleId: string): Observable<Rule> {
    return this.httpClient.get<GravityApiRule>(this.urls.board(boardId).rule(ruleId)).pipe(
      map(ruleJson => Rule.fromJSON(ruleJson))
    );
  }

  saveRule(rule: Rule): Observable<Rule> {
    const boardId = rule.boardId;

    rule.validate();

    return this.httpClient.post<GravityApiRule>(this.urls.board(boardId).ruleList(), rule.toJSON()).pipe(
      map(response => Rule.fromJSON(response))
    );
  }

  deleteRule(boardId: string, ruleId: string) {
    return this.httpClient.delete(this.urls.board(boardId).rule(ruleId));
  }
}


export interface RuleSummary {
  id: string;
  name: string;
  boardId: string;
  ruleType: 'COMPULSORY' | 'VALIDATION' | 'TASK' | 'SCHEDULED'
  index: number;
}
