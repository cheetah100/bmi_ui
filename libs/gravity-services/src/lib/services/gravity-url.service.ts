import { Injectable, Inject, Optional } from '@angular/core';
import { GRAVITY_PARAMETERS, GravityParameters } from '../gravity-parameters';
import { mapObjectValues } from '@bmi/utils';
import { joinUrl, queryParams } from '../utils/url-tools';

/**
 * Base class for URL generators.
 */
abstract class UrlGenerator {
  abstract get baseUrl(): string;

  url(apiUrl: string) {
    return joinUrl(this.baseUrl, apiUrl);
  }
}

class DataSourceUrlGenerator extends UrlGenerator {
  constructor(public readonly baseUrl: string) {
    super();
  }

  execute() {
    return this.url('/execute');
  }

  executeFiltered() {
    return this.url('/execute/filter');
  }
}

class TransformUrlGenerator extends DataSourceUrlGenerator {
  constructor(baseUrl: string) {
    super(baseUrl);
  }
}

class TransformCollectionUrlGenerator extends UrlGenerator {
  constructor(public readonly baseUrl: string) {
    super();
  }

  byId(id: string) {
    return new TransformUrlGenerator(this.url(`/${id}`));
  }

  list():string {
    return this.url('');
  }

  listByType(type: string) {
    return this.url(`/type/${type}`);
  }

  execute() {
    return this.url('/execute');
  }
}

/**
 * Gets sub-URLs relating to a particular board.
 */
class BoardUrlGenerator extends UrlGenerator {
  constructor(public readonly baseUrl: string) {
    super();
  }

  archiveCards() {
    return this.url('/archivecards')
  }

  get allCards() {
    return new CardUrlGenerator(this.url('cards')).all;
  }

  get bulkUpdateCards() {
    return new CardUrlGenerator(this.url('cards')).bulkUpdate;
  }
  filters() {
    return this.url('/filters');
  }

  phase(phaseId: string) {
    return this.url(`/phases/${phaseId}`);
  }

  phases() {
    return this.url('/phases');
  }

  templates() {
    return this.url('/templates');
  }

  template(templateId: string) {
    return this.url(`/templates/${templateId}`);
  }

  resourceList() {
    return this.url('/resources');
  }

  resource(resourceId: string) {
    return this.url(`/resources/${resourceId}`);
  }

  rule(ruleId: string) {
    return this.url(`/rules/${ruleId}`);
  }

  ruleList() {
    return this.url('/rules');
  }

  ruleListFull() {
    return this.url('/rules/full');
  }

  viewList() {
    return this.url('/views');
  }

  view(viewId: string) {
    return this.url(`/views/${viewId}`);
  }

  viewTemplate(viewId: string) {
    return this.url(`/views/${viewId}/template`);
  }

  pivot(pivotId: string) {
    return new DataSourceUrlGenerator(this.url(`/pivots/${pivotId}`));
  }

  cardList() {
    return new CardUrlGenerator(this.url('cards')).cardList
  }

  cardTitles() {
    return new CardUrlGenerator(this.url('cards')).titles;
  }

  cards() {
    return new CardUrlGenerator(this.url('cards')).baseUrl;
  }

  card(id: string) {
    return new CardUrlGenerator(this.url(`cards/${id}`));
  }

  alerts() {
    return  this.url('alerts');
  }

  get viewAllCards() {
    return new CardUrlGenerator(this.url('cards')).baseUrl + '?view=all';
  }
}


class CardUrlGenerator extends UrlGenerator {
  constructor(public readonly baseUrl: string) {
    super();
  }

  get cardList() {
    return this.url('cardlist');
  }

  tasks() {
    return this.url('tasks');
  }

  task(taskId: string) {
    return this.url(`tasks/${taskId}`);
  }

  get all() {
    return this.url('all');
  }

  get bulkUpdate() {
    return this.url('bulkupdate');
  }

  alerts() {
    return this.url('alerts');
  }

  dismissAlert(alertId: string) {
    return this.url(`alerts/${alertId}/dismiss`);
  }

  moveCard() {
    return this.url('move');
  }

  get titles() {
    return this.url('list');
  }

}

class AppUrlGenerator extends UrlGenerator {
  constructor(public readonly baseUrl: string) {
    super();
  }

  allWidgets() {
    return this.url('/widgets');
  }

  listWidgets() {
    return this.url('/widgets/list');
  }

  widget(widgetId: string) {
    return this.url(`/widgets/${widgetId}`);
  }
}

/**
 * Provides generators for Gravity URLs
 *
 * Rather than hard-coding Gravity URLs into the program, a better approach is
 * to delegate to a shared service to generate common URLs, making it easier
 * and safer to update URLs across the app.
 */
@Injectable({ providedIn: 'root' })
export class GravityUrlService {
  public readonly baseUrl: string;
  public readonly baseAppId: string;

  static forBaseUrl(baseUrl: string): GravityUrlService {
    return new GravityUrlService({ baseUrl });
  }

  constructor(
    @Optional()
    @Inject(GRAVITY_PARAMETERS)
    gravityParameters: Partial<GravityParameters>
  ) {
    gravityParameters = gravityParameters || {};
    this.baseUrl = gravityParameters.baseUrl ? gravityParameters.baseUrl : '';
    this.baseAppId = gravityParameters.baseAppId
      ? gravityParameters.baseAppId
      : '';
  }

  /**
   * Generates a Gravity API URL
   *
   * This prepends the apiUrl with the baseUrl from the GravityParameters.
   *
   * @param apiUrl the API URL path fragment. This should have a leading '/'
   *     (e.g. '/board/foobar')
   * @returns the full Gravity URL
   */
  url(apiUrl: string): string {
    return joinUrl(this.baseUrl, apiUrl);
  }

  listApps() {
    return this.url('/app');
  }

  /**
   * Gets the URL for the current app settings.
   */
  appUrl(appId: string = this.baseAppId) {
    return this.app(appId).baseUrl;
  }

  /**
   * Gets the URL for archiveCards endpoint
   * @param boardName
   */
  archiveCards(boardName: string) {
    return this.board(boardName).archiveCards();
  }

  /**
   * Get a URL generator for app URLs
   */
  app(appId: string): AppUrlGenerator {
    return new AppUrlGenerator(this.url(`/app/${appId}`));
  }

  /**
   * Generates Gravity API URL to connect to the App Settings
   * that must include appId (provided by baseAppId eg. idp or hr)
   *
   * This method ensures the baseAppId (dynamically injected
   * during app load) is used to create the API endpoint.
   *
   * @param apiUrl the API URL path fragment. This should have a leading '/'
   *
   *  (e.g. '/board/foobar')
   */
  settingsUrl(apiUrl: string): string {
    return joinUrl(this.appUrl(), apiUrl);
  }

  /**
   * All boards end point
   */
  allBoards() {
    return this.url('/board')
  }

  /**
   * Gets URLs for a specific board.
   * @param boardId
   */
  board(boardId: string): BoardUrlGenerator {
    return new BoardUrlGenerator(this.url(`/board/${boardId}`));
  }

  /**
   * Deprecated, use the board() method
   * @deprecated
   * @param boardId
   * @param resourceId
   */
  boardResource(boardId: string, resourceId: string) {
    return this.board(boardId).resource(resourceId);
  }

  /**
   * Deprecated, use the board() method
   * @deprecated
   * @param boardId
   */
  boardResourceList(boardId: string) {
    return this.board(boardId).resourceList();
  }

  /**
   * Deprecated, use the board() method
   * @deprecated
   * @param boardId
   * @param ruleId
   */
  boardRule(boardId: string, ruleId: string) {
    return this.board(boardId).rule(ruleId);
  }

  /**
   * Deprecated, use the board() method
   * @deprecated
   * @param boardId
   */
  boardRulesList(boardId: string) {
    return this.board(boardId).ruleList();
  }

  /**
   * Deprecated, use the board() method
   * @deprecated
   * @param boardId
   * @param viewId
   */
  boardViewTemplate(boardId: string, viewId: string) {
    return this.board(boardId).viewTemplate(viewId);
  }

  listPivots() {
    return this.url('/datasource/pivots');
  }

  pivotByUrl(pivotUrl: string): DataSourceUrlGenerator {
    return new DataSourceUrlGenerator(this.url(pivotUrl));
  }

  get transform(): TransformCollectionUrlGenerator {
    return new TransformCollectionUrlGenerator(this.url('/transform'));
  }

  transformByUrl(transformUrl: string): TransformUrlGenerator {
    return new TransformUrlGenerator(this.url(transformUrl));
  }

  allBoardConfigs() {
    return this.url('/board/configs');
  }

  boardConfig(boardId: string) {
    return this.url(`/board/${boardId}`);
  }

  heatmapList() {
    return this.url(`/heatmap`);
  }

  heatmapConfig(heatmapId: string) {
    return this.url(`/heatmap/${heatmapId}`);
  }

  moveCard(boardName: string, cardId:string) {
    return this.board(boardName).card(cardId).moveCard();
  }

  prefForCurrentUser(preferenceId: string) {
    return this.url(`/preferences/${preferenceId}`);
  }

  prefForUser(userId: string, preferenceId: string) {
    return this.url(`/preferences/user/${userId}/${preferenceId}`);
  }

  prefCollectionForCurrentUser() {
    return this.url(`/preferences`);
  }

  prefCollectionForUser(userId: string) {
    return this.url(`/preferences/user/${userId}`);
  }

  cardSearch(board: string, view: string = 'all', filterId: string = null) {
    return queryParams(this.url(`/board/${board}/search`), {
      view,
      filter: filterId
    });
  }

  getChildrenCardsByReference(boardId: string, cardId: string) {
    return this.url(
      `/board/${boardId}/cards/${cardId}/references/consolidated`
    );
  }

  getLoggedInUser() {
    return this.url('/user/current');
  }

  credentialsList() {
    return this.url('/cred');
  }
}
