import { Injectable } from '@angular/core';

import { Observable, EMPTY, interval } from 'rxjs';
import { retry, share } from 'rxjs/operators';

import { BoardService, BoardQueryParams } from './board.service';
import { CardData } from '../api-types/gravity-api-cardview';

import { ObservableMap } from '@bmi/utils';

import stableStringify from 'fast-json-stable-stringify';


function minutes(minutes: number): number {
  return minutes * 60 * 1000;
}


function refreshOrInvalidate<K, V>(map: ObservableMap<K, V>, key: K, shouldInvalidate: boolean): void {
  if (shouldInvalidate) {
    map.invalidate(key);
  } else {
    map.refresh(key);
  }
}


/**
 * Provides a lightweight cache for card data
 *
 * This builds on the batched fetching capabilities in the BoardService and
 * caches the results, refreshing them if stale.
 *
 * This version of the card caching service does not support querying 'all'
 * cards from a board -- that's an inherently problematic access pattern.
 * Instead this is intended for use when you need specific details from
 * individual cards, or for use when doing frontend lookups of cards.
 */
@Injectable({providedIn: 'root'})
export class CardCacheService {
  private boards = new Map<string, ObservableMap<string, CardData>>();

  private searchCache = new ObservableMap<BoardQueryParams, CardData[]>({
    source: query => this.boardService.search(query),
    keyFunction: query => stableStringify(query),
    maxAge: minutes(5)
  });

  constructor(private boardService: BoardService) {}

  private getBoardCache(boardId: string) {
    if (!this.boards.has(boardId)) {
      const cache = new ObservableMap<string, CardData>({
        source: cardId => this.boardService.fetchById(boardId, cardId),
        maxAge: minutes(5),
        emitStaleEntries: true
      });
      this.boards.set(boardId, cache);
    }
    return this.boards.get(boardId);
  }

  get(boardId: string, cardId: string): Observable<CardData> {
    return this.getBoardCache(boardId).watch(cardId);
  }

  search(query: BoardQueryParams): Observable<CardData[]> {
    return this.searchCache.watch(query);
  }

  /**
   * Refreshes board in the card and search caches.
   *
   * This will cause refreshes of any searches that are still being observed,
   * and it will refresh cards in the card cache too.
   *
   * If the invalidate parameter is true (default: false) it will also mark the
   * existing value as invalid, preventing it from being given out to new
   * subscriptions.
   */
  refreshBoard(boardId: string, invalidate = false) {
    const cardMap = this.boards.get(boardId);
    if (cardMap) {
      for (const cardId of cardMap.keys()) {
        refreshOrInvalidate(cardMap, cardId, invalidate);
      }
    }

    for (const search of this.searchCache.keys()) {
      if (search.boardId === boardId) {
        refreshOrInvalidate(this.searchCache, search, invalidate);
      }
    }
  }

  /**
   * Refreshes a cached search.
   *
   * This will refresh a particular search in the cache. If the invalidate
   * parameter is true (default: false) it will also mark the existing value as
   * invalid, preventing it from being given out to new subscriptions.
   */
  refreshSearch(query: BoardQueryParams, invalidate = false) {
    refreshOrInvalidate(this.searchCache, query, invalidate);
  }
}
