import { TestBed } from '@angular/core/testing';

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { GravityPrefsRepository } from '../services/gravity-prefs.repository';
import { Preference, PreferenceValue } from '../api-types/gravity-api-preference';
import { mockGravityProviders } from '../testing/gravity-test-tools';


const EXAMPLE_PREFERENCE: Preference = {
  id: 'test_preference',
  name: 'test_preference',
  userId: 'mock-user',
  values: { foo: 'bar' }
};

function updatePref(pref: Preference, values: PreferenceValue, userId: string = null): Preference {
  return {
    ...pref,
    userId: userId || pref.userId,
    values: {
      ...pref.values,
      ...values
    }
  };
}

describe('GravityPrefsRepository', () => {
  let httpTestingController: HttpTestingController;
  let userPrefsService: GravityPrefsRepository;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        ...mockGravityProviders(),
        GravityPrefsRepository
      ]
    });

    httpTestingController = TestBed.get(HttpTestingController) as HttpTestingController;
    userPrefsService = TestBed.get(GravityPrefsRepository) as GravityPrefsRepository;
  });

  // Ensure there are no unexpected requests still waiting after each test
  // completes.
  afterEach(() => httpTestingController.verify());

  it('should create the service', () => {
    expect(userPrefsService).toBeDefined();
  });

  it('should use the current user prefs API for MY preferences', () => {
    userPrefsService.getPreference('mock-user', 'test_preference').subscribe(pref => {
      expect(pref.id).toBe('test_preference');
    });

    const req = httpTestingController.expectOne('/preferences/test_preference');
    expect(req.request.method).toBe('GET');
    req.flush(EXAMPLE_PREFERENCE);
  });

  it('should use the admin prefs API for accessing other user preferences', () => {
    userPrefsService.getPreference('johndoe', 'test_preference').subscribe(pref => {
      expect(pref.id).toBe('test_preference');
    });

    const req = httpTestingController.expectOne('/preferences/user/johndoe/test_preference');
    expect(req.request.method).toBe('GET');
    req.flush({ ...EXAMPLE_PREFERENCE, userId: 'johndoe' });
  });

  it('should use current user API when updating MY prefs', () => {
    userPrefsService.updatePreference('mock-user', 'test_preference', { test: 'blah' }).subscribe(pref => {
      expect(pref.values.foo).toBe('bar');
      expect(pref.values.test).toBe('blah');
    });

    const req = httpTestingController.expectOne('/preferences');
    expect(req.request.method).toBe('POST');

    const body = req.request.body as Preference;
    expect(body).toEqual({
      id: 'test_preference',
      name: 'test_preference',
      values: {
        test: 'blah'
      }
    });

    req.flush(updatePref(EXAMPLE_PREFERENCE, body.values));
  });

  it('should use admin user API when updating other users prefs', () => {
    userPrefsService.updatePreference('johndoe', 'test_preference', { test: 'blah' }).subscribe(pref => {
      expect(pref.values.foo).toBe('bar');
      expect(pref.values.test).toBe('blah');
    });

    const req = httpTestingController.expectOne('/preferences/user/johndoe');
    expect(req.request.method).toBe('POST');

    const body = req.request.body as Preference;
    expect(body).toEqual({
      id: 'test_preference',
      name: 'test_preference',
      values: {
        test: 'blah'
      }
    });

    req.flush(updatePref(EXAMPLE_PREFERENCE, body.values, 'johndoe'));
  });

  it('should update prefs without requiring an external subscription', () => {
    userPrefsService.updatePreference('mock-user', 'test_preference', { foo: 'bar' });
    httpTestingController.expectOne('/preferences');
    expect(true).toBeTruthy();  // Sssh about no expectations!
  });

  it('should notify when the my preferences are updated', () => {
    let notificationCount = 0;
    userPrefsService.onCurrentUserPrefsUpdated.subscribe(pref => {
      expect(pref.values.foo).toBe('updated');
      notificationCount += 1;
    });

    userPrefsService.updatePreference('mock-user', 'test_preference', { foo: 'updated' });
    const req = httpTestingController.expectOne('/preferences');
    const body = req.request.body as Preference;
    req.flush(updatePref(EXAMPLE_PREFERENCE, body.values));

    expect(notificationCount).toBe(1);
  });

  it('should not notify when other user prefs are updated', () => {
    let notificationCount = 0;
    userPrefsService.onCurrentUserPrefsUpdated.subscribe(pref => {
      notificationCount += 1;
    });

    userPrefsService.updatePreference('johndoe', 'test_preference', { foo: 'updated' });
    const req = httpTestingController.expectOne('/preferences/user/johndoe');
    const body = req.request.body as Preference;
    req.flush(updatePref(EXAMPLE_PREFERENCE, body.values, 'johndoe'));

    expect(notificationCount).toBe(0);
  })
});
