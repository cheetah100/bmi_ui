import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { ObjectMap } from '@bmi/utils';
import mapValues from 'lodash-es/mapValues';
import uniqBy from 'lodash-es/uniqBy';
import {
  GravityApiPluginField,
  GravityApiPlugin
} from '../api-types/gravity-api-plugin-field';
import { GravityUrlService } from './gravity-url.service';
import { cloneDeep } from 'lodash-es';

export type PluginCategory = 'action' | 'integration' | 'transformer';

// This metadata will get 'infused' with the data coming from Gravity, allowing
// us to fine-tune the output and fill in missing gaps. We can use this to push
// for better metadata coming from the backend.
const METADATA_HINTS: Record<
  PluginCategory,
  ObjectMap<Partial<PluginMetadata>>
> = {
  action: {
    getcard: {
      name: 'Find Card',
      description: 'Find a card from a board'
    },
    persist: {
      name: 'Save Fields to Card',
      description: 'Saves fields from the action context onto the card.'
    },
    move: {
      name: 'Change phase',
      description: `Changes the card's phase`
    },
    property: {
      name: 'Update Context Fields',
      description: 'Sets the values of fields in the action context'
    },
    unique: {
      name: 'Verify Uniqueness',
      description: `Verifies that the combination of values from the specified fields are unique amongst cards on this board.`,
      tags: ['action:validator']
    }
  },

  integration: {
    http: { name: 'HTTP' },
    polling_http: { name: 'HTTP (Polling)' },
    polling_jdbc: { name: 'JDBC Database (Polling)' },
    polling_mongo: { name: 'Mongo Database (Polling)' }
  },

  transformer: {}
};

function generatePluginMetadataFromFields(
  pluginCategory: PluginCategory,
  pluginType: string,
  apiMetadata: GravityApiPlugin
): PluginMetadata {
  // Combine the data coming from the API with (partial) data form the
  // METADATA_HINTS object (above). We should give Gravity data precedence
  const builtinMetadata = METADATA_HINTS[pluginCategory][pluginType] || {};
  return {
    id: apiMetadata.id || pluginType,
    name: apiMetadata.name || builtinMetadata.name || pluginType,
    description: apiMetadata.description || builtinMetadata.description || null,
    tags: apiMetadata.tags || builtinMetadata.tags || [],
    fields: uniqBy(
      [...apiMetadata.fields, ...(builtinMetadata.fields || [])],
      f => f.field
    )
  };
}

function generateMetadataFromApi(
  category: PluginCategory,
  metadata: ObjectMap<GravityApiPlugin>
): ObjectMap<PluginMetadata> {
  return mapValues(metadata, (apiMetadata, pluginType) =>
    generatePluginMetadataFromFields(category, pluginType, apiMetadata)
  );
}

@Injectable({ providedIn: 'root' })
export class PluginMetadataService {
  fieldsToIgnore:string[] = [
    'lookup.field',
    'group.field',
    'dataseries.field',
    'transpose.field',
  ]
  private metadata = {
    action: this.fetchMetadata('action', '/system/automation/plugins/metadata'),
    transformer: this.fetchMetadata(
      'transformer',
      '/transform/plugins'
    ),
    integration: this.fetchMetadata('integration', '/integration/plugins')
  };

  constructor(
    private httpClient: HttpClient,
    private urls: GravityUrlService
  ) {}

  get(
    category: PluginCategory,
    pluginType: string
  ): Observable<PluginMetadata> {
    return this.metadata[category].pipe(map(metadata => metadata[pluginType]));
  }

  list(category: PluginCategory): Observable<PluginMetadata[]> {
    return this.metadata[category].pipe(
      map(metadata => Object.values(metadata))
    );
  }

  findByTags(
    category: PluginCategory,
    tags: string[]
  ): Observable<PluginMetadata[]> {
    return this.metadata[category].pipe(
      map(metadata =>
        Object.values(metadata).filter(plugin =>
          tags.every(tag => plugin.tags.includes(tag))
        )
      )
    );
  }

  private fetchMetadata(category: PluginCategory, apiUrl: string) {
    const url = this.urls.url(apiUrl);
    return this.httpClient.get<ObjectMap<GravityApiPlugin>>(url).pipe(
      map(metadata => generateMetadataFromApi(category, metadata)),
      shareReplay(1)
    );
  }

  /**
   * Remove all fields to be ignored
   * @param metadata
   * @private
   */
  private filterMetadataFields(metadata: ObjectMap<GravityApiPlugin>):ObjectMap<GravityApiPlugin> {
    const filteredMetadata: ObjectMap<GravityApiPlugin> = {};
    Object.keys(metadata).forEach( key => {
      const plugin = metadata[key];
      plugin.fields = plugin.fields.filter(f => !this.fieldsToIgnore.includes(f.field))
      filteredMetadata[key] = plugin;
    })

    return filteredMetadata;
  }
}

export interface PluginMetadata {
  id: string;
  name: string;
  description: string;
  tags: string[];
  fields: PluginMetadataField[];
}

export type PluginMetadataField = GravityApiPluginField;
