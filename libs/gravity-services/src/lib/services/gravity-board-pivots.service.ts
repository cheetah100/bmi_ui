import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { map, publishLast, refCount } from 'rxjs/operators';

import { GravityUrlService } from './gravity-url.service';

import { GravityApiFilter } from '../api-types/gravity-api-filter';
import { PivotData } from './gravity-transform.service';


@Injectable()
export class GravityBoardPivotsService {
  constructor(
    private urlService: GravityUrlService,
    private httpClient: HttpClient,
  ) {}

  listAllPivots(): Observable<PivotSummary[]> {
    return this.httpClient.get<{[url: string]: string}>(this.urlService.listPivots()).pipe(
      map(data => Object.entries(data).map(([url, name]) => <PivotSummary>{url, name})),
      publishLast(),
      refCount()
    );
  }

  executePivot(pivotUrl: string, filter?: GravityApiFilter): Observable<PivotData> {
    if (filter) {
      const url = this.urlService.pivotByUrl(pivotUrl).executeFiltered();
      return this.httpClient.post<PivotData>(url, filter).pipe(
        publishLast(),
        refCount()
      );
    } else {
      const url = this.urlService.pivotByUrl(pivotUrl).execute();
      return this.httpClient.get<PivotData>(url).pipe(
        publishLast(),
        refCount()
      );
    }

  }
}


export interface PivotSummary {
  url: string;
  name: string;
}
