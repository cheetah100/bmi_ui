import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {
  GRAVITY_PARAMETERS,
  GravityApiIntegration, GravityParameters,
  Integration,
  IntegrationsService,
  IntegrationSummary
} from '@bmi/gravity-services';
import { TestBed } from '@angular/core/testing';
import { cloneDeep } from 'lodash-es';
import { GravityHttpClient } from '../core/gravity-http-client';

const EXAMPLE_GRAVITY_API_INTEGRATION: GravityApiIntegration = {
  id: 'get_hcd_lake_levels',
  name: 'get_hcd_lake_levels',
  connector: 'polling_http',
  config: {
    url: 'https://api.watercare.co.nz/lake/lakelevel/hcd',
    list: '',
    schedule: '0 45 16 * * ?'
  },
  actions: {
    "create": {
      id: 'create',
      name: 'create',
      description: 'Create Card',
      order: 0,
      type: 'createcard',
      config: {
        response: null,
        method: 'all',
        resource: 'lakelevels',
        properties: {
          reading_date: '#readingDate',
          created_date: '#createdDate',
          lake_code: '#hcd'
        },
        parameters: null
      }
    }
  }
};
const EXAMPLE_INTEGRATION:Integration = {
  id: 'get_hcd_lake_levels',
  name: 'get_hcd_lake_levels',
  connector: 'polling_http',
  config: {
    url: 'https://api.watercare.co.nz/lake/lakelevel/hcd',
    list: '',
    schedule: '0 45 16 * * ?'
  },
  actions: [
    {
      id: 'create',
      name: 'create',
      description: 'Create Card',
      order: 0,
      type: 'createcard',
      config: {
        response: null,
        method: 'all',
        resource: 'lakelevels',
        properties: {
          reading_date: '#readingDate',
          created_date: '#createdDate',
          lake_code: '#hcd'
        },
        parameters: null
      }
    }
  ]
}

const EXAMPLE_INTEGRATION_SUMMARY:IntegrationSummary[] = [
  {
    connector: '',
    id: "software_load_notification",
    name: "Software Load Notification"
  },
  {
    connector: '',
    id: "esp_datamart_endpoints_query",
    name: "esp_datamart_endpoints_query"
  }];


describe('integration service', () => {
  let httpTestingController: HttpTestingController;
  let integrationsService: IntegrationsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        // ...mockGravityProviders(),
        IntegrationsService,
        GravityHttpClient,
        {
          provide: GRAVITY_PARAMETERS,
          useValue: <GravityParameters>{
            baseAppId: 'admin',
            baseUrl: '',
            boardConfigs: {},
          }
        }
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController) as HttpTestingController;
    integrationsService = TestBed.inject(IntegrationsService) as IntegrationsService;
  });

  afterEach(() => httpTestingController.verify());

  it('should create the service', () => {
    expect(integrationsService).toBeDefined();
  });

  it('should get an integration', () => {
    integrationsService.get('get_hcd_lake_levels').subscribe(integration => {
      expect(integration.id).toBe('get_hcd_lake_levels');
      expect(integration.actions instanceof Array).toBe(true);
      expect(integration.actions.length).toBe(1);
      expect(integration)
    })

    const req = httpTestingController.expectOne('/integration/get_hcd_lake_levels');
    expect(req.request.method).toBe('GET');
    req.flush(EXAMPLE_GRAVITY_API_INTEGRATION);
  })

  it('should cache a request', () => {
    integrationsService.get('get_hcd_lake_levels').subscribe();

    const req = httpTestingController.expectOne('/integration/get_hcd_lake_levels');
    req.flush(EXAMPLE_GRAVITY_API_INTEGRATION);

    let currentIntegration;
    integrationsService.get('get_hcd_lake_levels').subscribe(integration => currentIntegration = integration );

    httpTestingController.expectNone('/integration/get_hcd_lake_levels');

  });

  it('should force a reload when specifying invalidateNow', () => {
    integrationsService.get('get_hcd_lake_levels').subscribe();

    const req = httpTestingController.expectOne('/integration/get_hcd_lake_levels');
    req.flush(EXAMPLE_GRAVITY_API_INTEGRATION);

    let currentIntegration;
    integrationsService.get('get_hcd_lake_levels').subscribe(integration => currentIntegration = integration );
    httpTestingController.expectNone('/integration/get_hcd_lake_levels');
    integrationsService.get('get_hcd_lake_levels', {invalidateNow: true}).subscribe();
    const req3 = httpTestingController.expectOne('/integration/get_hcd_lake_levels');
    req3.flush(EXAMPLE_GRAVITY_API_INTEGRATION);
  });

  it('should save a change and invalidate the cache', () => {


    const integration = cloneDeep(EXAMPLE_INTEGRATION);
    // Add another field
    integration.actions[0].config.properties.testfield = '#testfield';
    integrationsService.save(integration).subscribe();

    //get integration from server
    integrationsService.get('get_hcd_lake_levels').subscribe();
    const requests = httpTestingController.match('/integration/get_hcd_lake_levels');
    expect(requests.length).toEqual(2);

    requests[0].flush(integration);
    requests[1].flush(EXAMPLE_INTEGRATION);
  })

  it('should list a summary of all integrations', () => {
    integrationsService.list().subscribe(results => {
      expect(results.length).toEqual(EXAMPLE_INTEGRATION_SUMMARY.length);
    })

    const req = httpTestingController.expectOne('/integration');
    expect(req.request.method).toBe('GET');
    req.flush(EXAMPLE_INTEGRATION_SUMMARY);
  })

  it('should execute an integration', () => {
    integrationsService.execute('get_hcd_lake_levels').subscribe()

    const req = httpTestingController.expectOne('/integration/execute');
    expect(req.request.method).toBe('POST');
    req.flush(null);

  });


})
