import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { GravityHttpClient } from '../core/gravity-http-client';
import { GRAVITY_PARAMETERS, GravityParameters, GravityUrlService } from '@bmi/gravity-services';

const baseUrl = '/gravity/spring';

describe( 'GravityUrlService', () => {
  let urlService:GravityUrlService;
  const boardName = 'Test Board';
  const cardId = 'Test Card';
  const appId = 'Demo app';

  beforeEach(() => {

    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [
        GravityHttpClient,
        {
          provide: GRAVITY_PARAMETERS,
          useValue: <GravityParameters>{
            baseAppId: 'admin',
            baseUrl: '/gravity/spring',
            boardConfigs: {},
          }
        }
      ]
    });

    urlService = TestBed.inject(GravityUrlService);

  });


  it('should return a valid all boards end point', () => {
    const expectedUrl = '/gravity/spring/board';
    expect(urlService.allBoards()).toBe(expectedUrl);
  });

  it('should return a valid get card end point', () => {
    const expectedUrl = `/gravity/spring/board/${boardName}/cards/${cardId}`;
    expect(urlService.board(boardName).card(cardId).baseUrl).toBe(expectedUrl);
  });

  it('should return a valid move card/phase end point', () => {
    const expectedUrl = `/gravity/spring/board/${boardName}/cards/${cardId}/move`;
    expect(urlService.moveCard(boardName, cardId)).toBe(expectedUrl);
  });

  it('should return a valid archive Cards end point', () => {
    const expectedUrl = `/gravity/spring/board/${boardName}/archivecards`;
    expect(urlService.archiveCards(boardName)).toBe(expectedUrl);
  });

  it('should return a valid app end point', () => {
    const expectedUrl = `/gravity/spring/app/${appId}`;

    expect(urlService.appUrl(appId)).toBe(expectedUrl);
  });

  it('should return a valid card filter end point', () => {
    const expectedUrl = `/gravity/spring/board/${boardName}/filters`;
    expect(urlService.board(boardName).filters()).toBe(expectedUrl);
  });

  it('should return a valid card list end point', () => {
    const expectedUrl = `/gravity/spring/board/${boardName}/cards/cardlist`;
    expect(urlService.board(boardName).cardList()).toBe(expectedUrl);
  });

  it('should return a valid card view all end point', () => {
    const expectedUrl = `/gravity/spring/board/${boardName}/cards?view=all`;
    expect(urlService.board(boardName).viewAllCards).toBe(expectedUrl)
  });

  it('should return a valid get all cards end point', () => {
    const expectedUrl = `/gravity/spring/board/${boardName}/cards/all`;

    expect(urlService.board(boardName).allCards).toBe(expectedUrl);
  });

  it('should return a valid url for the bulkupdate end point', () => {
    const expectedUrl = `/gravity/spring/board/${boardName}/cards/bulkupdate`;
    expect(urlService.board(boardName).bulkUpdateCards).toBe(expectedUrl);
  });
})
