import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GravityUrlService } from './gravity-url.service';
import { Observable } from 'rxjs';
import { GravityApiFilter } from '../api-types/gravity-api-filter';


@Injectable()
export class GravityUploadStatusService {

  constructor(
    private http: HttpClient,
    private settings: GravityUrlService
  ) { }

  /**
   * Returns list of workbook uploads and their statuses for the corresponding user.
   * TODO: Currently returns all the uploads, funtionality can be enhanced to
   * filter uploads for a specific time duration
   */
  getUploadStatusList() {
    return this.http.get(this.settings.url(`/import/status/myupload`));
  }

  /**
   * Returns the log of failed rows at a sheet level.
   * TODO: Replace with API that downloads the error log file.
   * @param workbookId
   * @param sheetId
   */
  downloadFailedUploadErrorLog(workbookId: string, sheetId: string) {
    return this.http.get(this.settings.url(`/import/status/myupload/${workbookId}/${sheetId}/errors`));
  }
}
