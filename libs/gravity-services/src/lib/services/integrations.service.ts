import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import pick from 'lodash-es/pick';
import ms from 'ms';

import { Integration, IntegrationSummary } from '../model/integration';
import { GravityApiIntegration } from '../api-types/gravity-api-integration';
import { GravityUrlService } from './gravity-url.service';

import { CacheEntry, ObservableMap } from '@bmi/utils';
import { GravityRestfulService } from './gravity-restful.service';
import { GravityHttpClient } from '../core/gravity-http-client';

export interface IntegrationRef {
  id: string;
}
@Injectable({providedIn: 'root'})
export class IntegrationsService extends GravityRestfulService<string, GravityApiIntegration> {

  constructor(
    private http: GravityHttpClient,
  ) {
    super({
      itemUrl: (ref: string) => `/integration/${ref}`,
      collectionUrl: () => '/integration',
      httpClient: http,
      keyFunction: (integration: GravityApiIntegration) => integration.id,
      maxAge: ms('5 minutes'),
    })
  }

  private integrationSummaries = new CacheEntry({
    source: () => this.fetchSummaries(),
    maxAge: ms('5 minutes'),
  });

  private integrations = new ObservableMap<string, Integration>({
    source: id => this.fetchIntegration(id),
    maxAge: ms('5 minutes'),
  });

  list(): Observable<IntegrationSummary[]> {
    return this.integrationSummaries.watch();
  }

  get(id: string, {invalidateNow = false} = {}): Observable<Integration> {
    if (invalidateNow) {
      // Invalidate the Intergration
      this.integrations.invalidate(id);
      // Invalidate the underlying GravityApiIntegration
      this.invalidateRecordById(id)
    }

    return this.integrations.watch(id);
  }

  save(integration: Integration) {
    const json = Integration.toJSON(integration);
    const source = this.saveRecord(json, integration.isNew);

    return source.pipe(
      tap(() => {
        this.integrationSummaries.invalidate();
      })
    );
  }

  execute(id: string) {
    const endpoint = '/integration/execute';
    return this.httpPost(endpoint, id);
  }

  delete(id: string) {
    const endpoint = `/integration/${id}`;
    return this.http.delete(endpoint).pipe(
      tap(() => {
        this.integrations.invalidate(id);
        this.integrationSummaries.invalidate();
      }));
  }

  private fetchSummaries(): Observable<IntegrationSummary[]> {
    return this.fetchCollection().pipe(
      map(result => result.map(r => pick(r, 'id', 'name', 'connector')))
    );
  }

  private fetchIntegration(integrationId: string): Observable<Integration> {
    return this.fetchRecordById(integrationId).pipe(
      map(json => Integration.fromJSON(json))
    );
  }
}
