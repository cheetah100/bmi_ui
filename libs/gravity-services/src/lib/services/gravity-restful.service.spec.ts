import { GravityRestfulService } from './gravity-restful.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { fakeAsync, TestBed } from '@angular/core/testing';
import { GRAVITY_PARAMETERS, GravityParameters } from '@bmi/gravity-services';
import { cloneDeep } from 'lodash-es';
import { GravityHttpClient } from '../core/gravity-http-client';

interface ITestData {
  id: string,
  name: string
}
interface ITestDataRef {
  id: string;
}

const EXAMPLE_DATA: ITestData = {
  id: '1',
  name: 'Barney'
}
const EXAMPLE_REF: ITestDataRef = {
  id: '1'
}

const EXAMPLE_COLLECTION: ITestData[] = [
  {
    id: '1',
    name: 'Barney',
  },
  {
    id: '2',
    name: 'Fred',
  },
  {
    id: '3',
    name: 'Wilma',
  },
  {
    id: '4',
    name: 'Pebbles',
  },
  {
    id: '5',
    name: 'Bam Bam',
  },
]

const baseUrl = '/gravity/spring';

describe( 'GravityRestfulService', () => {
  let boardConfigService: GravityRestfulService<ITestDataRef, ITestData>;
  let httpClient: GravityHttpClient;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {

    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [
        GravityHttpClient,
        {
          provide: GRAVITY_PARAMETERS,
          useValue: <GravityParameters>{
            baseAppId: 'admin',
            baseUrl: '/gravity/spring',
            boardConfigs: {},
          }
        }
      ]
    });

    // Inject the http service and test controller for each test
    httpClient = TestBed.inject(GravityHttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);

    boardConfigService = new GravityRestfulService<ITestDataRef, ITestData>({
      itemUrl: ref => `/board/${ref.id}`,
      collectionUrl: () => `/board`,
      httpClient: httpClient,
      listStyle: 'list',
      keyFunction: record => {
        return { id: record.id }
      }
    });
  });

  afterEach(() => {
    console.log('afterEach => called: ');
    httpTestingController.verify()
  });

  it('should create the service', () => {
    expect(boardConfigService).toBeDefined();
  });

  it('should fetch a record with a given id from the backend', () => {
    boardConfigService.fetchRecordById(EXAMPLE_REF)
      .subscribe(data =>
        // When observable resolves, result should match test data
        expect(data).toEqual(EXAMPLE_DATA)
      );

    const req = httpTestingController.expectOne(`${baseUrl}/board/1`);
    req.flush(EXAMPLE_DATA);
  });

  it('should create a new record', () => {
    const record = cloneDeep(EXAMPLE_DATA);
    record.id = '10';
    record.name = 'new record';

    boardConfigService.saveRecord(record, true).subscribe();
    boardConfigService.fetchRecordById({ id: record.id}).subscribe();
    const requests = httpTestingController.match(`${baseUrl}/board`);
    expect(requests.length).toBe(1);
    expect(requests[0].request.method).toBe('POST');
    const requests2 = httpTestingController.match(`${baseUrl}/board/10`);
    expect(requests2[0].request.method).toBe('GET');
    requests[0].flush(record);
    requests2[0].flush(record);
  });

  it('should refresh a record', () => {
    boardConfigService.fetchRecordById(EXAMPLE_REF).subscribe();
    const req = httpTestingController.expectOne(`${baseUrl}/board/1`);
    req.flush(EXAMPLE_DATA);

    boardConfigService.refreshRecord(EXAMPLE_REF);


    boardConfigService.fetchRecordById(EXAMPLE_REF).subscribe();
    const req2 = httpTestingController.expectOne(`${baseUrl}/board/1`);
    req2.flush(EXAMPLE_DATA);

  });

  describe('Test Caching', () => {
    const testData = EXAMPLE_DATA;
    // afterEach(() => httpTestingController.verify());
    it('should cache the requested record', () => {
      const expectedUrl = `${baseUrl}/board/1`;
      boardConfigService.fetchRecordById(EXAMPLE_REF).subscribe();
      const req = httpTestingController.expectOne(expectedUrl);
      req.flush(EXAMPLE_DATA);

      let currentData;
      boardConfigService.fetchRecordById(EXAMPLE_REF)
        .subscribe(data => currentData = data );

      httpTestingController.expectNone(expectedUrl)
    });

    it('should refresh the entry when requesting a refresh', () => {
      boardConfigService.fetchRecordById(EXAMPLE_REF, {invalidateNow: true})
        .subscribe(data => {
            console.log(`refresh`);
            expect(data).toEqual(testData)
          }
        );
      const req = httpTestingController.expectOne(`${baseUrl}/board/1`);
      expect(req.request.method).toEqual('GET');
      req.flush(testData);
    });

    it('should update the watched record when a change is made', fakeAsync(() => {
      const data = cloneDeep(EXAMPLE_DATA);
      data.name = 'Wilma';
      boardConfigService.saveRecord(data, false).subscribe( updatedRecord => {
        expect(updatedRecord).toEqual(data);
      })

      boardConfigService.fetchRecordById(EXAMPLE_REF).subscribe();
      const requests = httpTestingController.match(`${baseUrl}/board/1`);
      expect(requests.length).toEqual(2);

      requests[0].flush(data);
      requests[1].flush(EXAMPLE_DATA);
    }));

  });

  describe('Collection tests', () => {
    // afterEach(() => httpTestingController.verify());
    it('should get a collection of records', () => {
      boardConfigService.fetchCollection().subscribe(
        records => expect(records.length).toEqual(EXAMPLE_COLLECTION.length)
      )
      const req = httpTestingController.expectOne(`${baseUrl}/board`);
      req.flush(EXAMPLE_COLLECTION);
    });
  });


});
