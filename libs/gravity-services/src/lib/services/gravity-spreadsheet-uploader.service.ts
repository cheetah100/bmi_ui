import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GravityUrlService } from './gravity-url.service';
import { Observable } from 'rxjs';
import { GravityApiFilter } from '../api-types/gravity-api-filter';

@Injectable()
export class GravityFileUploaderService {

  constructor(
    private http: HttpClient,
    private settings: GravityUrlService
  ) {}

  getFiltersForBoard(boardId: string): Observable<any> {
    return this.http.get(this.settings.url(`/board/${boardId}/filters`));
  }

  uploadSpreadsheet(file: File) {
    const formData: FormData = new FormData();
    formData.append('file', file, file.name);
    return this.http.post(this.settings.url(`/import/submit`), formData, { responseType: 'text' });
  }

  getPollingStatus(transmissionType: TransmissionType) {
    if (transmissionType === 'UPLOAD') {
      return this.http.get(this.settings.url(`/import/status`));
    } else if (transmissionType === 'DOWNLOAD') {
      return this.http.get(this.settings.url(`/export/status`));
    }
  }

  initiateDownloadJob(boardId: string, filterConditions: GravityApiFilter) {
    if (filterConditions) {
      return this.http.post(this.settings.url(`/export/${boardId}/xlsx/filter/startjob`), filterConditions, { responseType: 'text' });
    } else {
      return this.http.get(this.settings.url(`/export/${boardId}/xlsx/startjob`), { responseType: 'text' });
    }
  }

  downloadSpreadsheet(boardId: string) {
    return this.http.get(this.settings.url(`/export/${boardId}/xlsx/download`));
  }
}

export type TransmissionType = 'UPLOAD' | 'DOWNLOAD';

export type UploadStatus = 'READY' | 'INPROGRESS' | 'ERROR' | 'FAILED';
