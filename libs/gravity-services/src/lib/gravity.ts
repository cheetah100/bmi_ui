import { Injectable, Optional } from '@angular/core';
import { Observable } from 'rxjs';
import { GravityService } from './core/gravity.service';
import { GravityCache } from './core/gravity-cache';
import { DataStore } from './core/data-store';
import { DataChange } from './core/data-change';
import { UserRepository } from './core/user.repository';
import { BoardHistoryRepository } from './core/board-history.repository';
import { GravityConfigService } from './core/gravity-config.service';
import { TransformRepository } from './core/transform.repository';
import { DashboardRepository } from './dashboard/dashboard.repository';
import { GravityUrlService } from './services/gravity-url.service';

@Injectable()
export class Gravity {

  constructor(
    private gravityConfigService: GravityConfigService,
    private gravityService: GravityService,
    private gravityCache: GravityCache,
    private userRepository: UserRepository,
    @Optional() private boardHistoryRepository: BoardHistoryRepository,
    @Optional() private dashboardRepository: DashboardRepository,
    @Optional() private transformRepository: TransformRepository,
    private dataStore: DataStore,
    public readonly url: GravityUrlService) {
  }

  get cache(): GravityCache {
    return this.gravityCache;
  }

  get boards(): GravityService {
    return this.gravityService;
  }

  get users(): UserRepository {
    return this.userRepository;
  }

  get boardHistory(): BoardHistoryRepository {
    return this.boardHistoryRepository;
  }

  get dashboards(): DashboardRepository {
    return this.dashboardRepository;
  }

  get configService(): GravityConfigService {
    return this.gravityConfigService;
  }

  get transforms(): TransformRepository {
    return this.transformRepository;
  }

  changes(dataType: string): Observable<DataChange> {
    return this.dataStore.changes(dataType);
  }
}
