import { Inject, Injectable, EventEmitter } from '@angular/core';

import { Observable, throwError, of, BehaviorSubject } from 'rxjs';

import { GravityConverter } from '../core/gravity-converter';
import { GRAVITY_TESTBED_OPTIONS_PROVIDER, GravityTestbedOptions } from '../testing/gravity-testbed-options';
import { applyEqualToFilterOnFrontend } from '../utils/apply-gravity-filter';
import { Filter } from '../model/filter';
import { DataStore } from '../core/data-store';
import { Card } from '../core/card';


export interface FakeGravityServiceEvent {
  type: 'getCard' | 'getCards' | 'getCardsFiltered';
  board: string;
  cardId?: string | null;
  filter?: Filter;
}

/**
 * A partial mock read only implementation of GravityService which uses
 * predefined JSON card data.
 *
 * This implements the getCard() and getCards() methods which should allow the
 * Gravity cache to be used. This replicates some of the behaviours present in
 * the real GravityService, but honestly this is bad, and the real GravityService
 * needs refactoring to be more testable -- the actual interaction with the
 * backend server should be delegated to a separate service. It's more work BUT
 * makes the app more testable!
 */
@Injectable()
export class FakeGravityService {
  private traceEventEmitter = new EventEmitter<FakeGravityServiceEvent>();

  constructor(
    private gravityConverter: GravityConverter,
    private dataStore: DataStore,
    @Inject(GRAVITY_TESTBED_OPTIONS_PROVIDER) private testbedOptions: GravityTestbedOptions
  ) { }

  get traceEvents(): Observable<FakeGravityServiceEvent> {
    return this.traceEventEmitter.asObservable();
  }

  private get cards() {
    return this.testbedOptions.cards;
  }

  private getBoardCards(boardId: string) {
    const cards = this.cards[boardId];
    if (!cards) {
      throw new Error('FakeGravityService: Board not found');
    } else {
      return cards;
    }
  }

  getCard(boardId: string, cardId: string, validateReferences: boolean = false): Observable<Card> {
    this.traceEventEmitter.emit({ type: 'getCard', board: boardId, cardId });

    try {
      const cardsData = this.getBoardCards(boardId);
      const cardJson = cardsData.find(c => c.id === cardId);
      if (!cardJson) {
        throw new Error('Card not found');
      }
      const card = this.gravityConverter.fromJson(boardId, cardJson);
      this.dataStore.addOrUpdate(boardId, card.clone());
      return of(card);
    } catch (error) {
      return throwError(error);
    }
  }

  getCards(boardId: string): Observable<Card[]> {
    this.traceEventEmitter.emit({ type: 'getCards', board: boardId });

    try {
      const cards = this.getBoardCards(boardId).map(c => this.gravityConverter.fromJson(boardId, c));
      this.dataStore.set(boardId, cards.map(c => c.clone()));
      return of(cards);
    } catch (error) {
      return throwError(error);
    }
  }

  getCardsFiltered(board: string, filter: Filter): Observable<Card[]> {
    this.traceEventEmitter.emit({ type: 'getCardsFiltered', board: board, filter: filter });
    try {
      const cards = this.getBoardCards(board).map(c => this.gravityConverter.fromJson(board, c));
      const filteredCards = applyEqualToFilterOnFrontend(cards, filter);
      filteredCards.forEach(c => this.dataStore.addOrUpdate(board, c));
      return of(filteredCards);
    } catch (error) {
      return throwError(error);
    }
  }
}
