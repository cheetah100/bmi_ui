import {
  GravityApiBoard,
  GravityApiTemplate,
  GravityApiTemplateGroup,
  GravityApiTemplateField,
  GravityApiPhase,
  GravityApiUiData,
  GravityApiTemplateUiData
} from '../api-types/gravity-api-board-config';
import { Permissions } from '../core/permissions';


import pickBy from 'lodash-es/pickBy';
import isFinite from 'lodash-es/isFinite';
import cloneDeep from 'lodash-es/cloneDeep';
import sortBy from 'lodash-es/sortBy';

type BuilderFunction<T> = (builder: T) => void;


type IndexedObjectMap = { [key: string]: { index?: number } };


function getNextIndex(obj: IndexedObjectMap) {
  let maxIndex = 0;
  for (const value of Object.values(obj)) {
    const index = value.index || 0;
    if (index >= maxIndex) {
      maxIndex = index;
    }
  }
  return maxIndex + 1;
}


function fixIndexes(obj: IndexedObjectMap) {
  sortBy(Object.values(obj), v => isFinite(v.index) ? v.index : Infinity)
    .forEach((value, index) => value.index = index + 1);
}



abstract class ConfigBuilder<T> {
  protected abstract get config(): T;
  abstract finish(): T;

  /**
   * Assigns
   */
  assign(config: Partial<T>) {
    Object.assign(this.config, pickBy(config, value => value !== undefined));
    return this;
  }
}


/**
 * Builds a Gravity Template field
 */
class TemplateFieldBuilder extends ConfigBuilder<GravityApiTemplateField> {
  config: GravityApiTemplateField = {
    id: null,
    name: null,
    label: null,
    type: 'STRING'
  };

  constructor(config?: GravityApiTemplateField) {
    super();
    if (config) {
      this.config = config;
    }
  }

  /**
   * Sets the field id
   */
  id(id: string) {
    this.config.id = id;
    return this;
  }

  /**
   * Sets the name of the field
   */
  name(name: string) {
    this.config.name = name;
    return this;
  }

  /**
   * Sets the user-facing label for the field.
   */
  label(label: string) {
    this.config.label = label;
    return this;
  }

  /**
   * Sets the basic data type for the field.
   */
  type(type: 'STRING' | 'BOOLEAN' | 'NUMBER' | 'DATE' | 'LIST') {
    this.config.type = type;
    return this;
  }

  /**
   * Sets the optionlist board to use for this field.
   *
   * If set, this will make the field a reference field.
   */
  optionlist(optionlistBoard: string) {
    this.config.optionlist = optionlistBoard;
    return this;
  }

  /**
   * Sets whether the field is required.
   */
  required(isRequired: boolean = true) {
    this.config.required = isRequired;
    return this;
  }

  /**
   * Sets the type of control to use when editing this field.
   */
  control(controlType: 'INPUT' | 'TEXTBOX' | 'DROPDOWN' | 'SELECTION' | 'DATESELECTOR' | 'MULTI' | null) {
    this.config.control = controlType;
    return this;
  }

  /**
   * Sets the index of the field.
   *
   * Setting this to a non-null value will override the default assigned index,
   * and will affect any subsequently created fields that do not have an index.
   */
  index(index: number) {
    this.config.index = index;
    return this;
  }

  /**
   * Finishes the build process, performing validation and returning the config.
   *
   * You don't need to call this manually.
   */
  finish() {
    if (!this.config.id) {
      throw new Error(`Template Field must have id`);
    }

    if (!this.config.name) {
      this.config.name = this.config.id;
    }

    if (!this.config.label) {
      this.config.label = this.config.id;
    }

    this.config.referenceField = !!this.config.optionlist;
    return this.config;
  }
}


class TemplateGroupBuilder extends ConfigBuilder<GravityApiTemplateGroup> {
  protected config: GravityApiTemplateGroup = {
    id: null,
    name: null,
    fields: {},
    index: undefined
  };

  constructor(templateGroup: GravityApiTemplateGroup = null) {
    super();
    if (templateGroup) {
      this.config = templateGroup;
    }
  }

  id(id: string) {
    this.config.id = id;
    return this;
  }

  name(name: string) {
    this.config.name = name;
    return this;
  }

  index(index: number) {
    this.config.index = index;
    return this;
  }

  addField(id: string, fn: BuilderFunction<TemplateFieldBuilder> = null) {
    const fieldBuilder = new TemplateFieldBuilder(this.config.fields[id]);
    fieldBuilder.id(id);
    if (fn) {
      fn(fieldBuilder);
    }
    const field = fieldBuilder.finish();
    if (!isFinite(field.index)) {
      field.index = getNextIndex(this.config.fields);
    }

    this.config.fields[field.id] = field;
    return this;
  }

  deleteField(fieldId: string) {
    delete this.config.fields[fieldId];
    return this;
  }

  optimizeIndexes() {
    fixIndexes(this.config.fields);
    return this;
  }

  finish() {
    if (!this.config.id) {
      throw new Error('Template group requires an id');
    }

    if (!this.config.name) {
      this.config.name = this.config.id;
    }

    return this.config;
  }
}


class TemplateBuilder extends ConfigBuilder<GravityApiTemplate> {
  protected config: GravityApiTemplate = {
    id: null,
    name: null,
    groups: {},
    fields: {}
  };

  constructor(template: GravityApiTemplate = null) {
    super();
    if (template) {
      this.config = template;
    }
  }

  group(id: string, fn: BuilderFunction<TemplateGroupBuilder>) {
    const groupBuilder = new TemplateGroupBuilder(this.config.groups[id]);
    groupBuilder.id(id);
    fn(groupBuilder);
    const group = groupBuilder.finish();

    if (!isFinite(group.index)) {
      group.index = getNextIndex(this.config.groups);
    }
    this.config.groups[group.id] = group;
    return this;
  }

  groups(fn: BuilderFunction<TemplateGroupBuilder>) {
    Object.keys(this.config.groups).forEach(id => this.group(id, fn));
    return this;
  }

  defaultGroup(fn: BuilderFunction<TemplateGroupBuilder>) {
    return this.group('default', fn);
  }

  id(id: string) {
    this.config.id = id;
    return this;
  }

  addField(id: string, fn: BuilderFunction<TemplateFieldBuilder> = null) {
    const existingFieldGroup = this.findGroupWithField(id);
    if (existingFieldGroup) {
      this.group(existingFieldGroup, g => g.addField(id, fn));
    } else {
      this.defaultGroup(g => g.addField(id, fn));
    }
    return this;
  }

  deleteField(fieldId: string) {
    return this.groups(g => g.deleteField(fieldId));
  }

  ui(uiData: Partial<GravityApiTemplateUiData>) {
    this.config.ui = this.config.ui || {};
    Object.assign(this.config.ui, uiData);
  }

  optimizeIndexes() {
    fixIndexes(this.config.groups);
    for (const group of Object.keys(this.config.groups)) {
      this.group(group, g => g.optimizeIndexes());
    }
    return this;
  }

  private findGroupWithField(fieldId: string): string {
    const groupEntry = Object.entries(this.config.groups)
      .find(([id, group]) => !!group.fields[fieldId]);

    return groupEntry ? groupEntry[0] : null;
  }

  finish() {
    if (!this.config.id) {
      throw new Error('Template id must be defined');
    }

    if (!this.config.name) {
      this.config.name = this.config.id;
    }

    // For compatibility with the Gravity API output, we compute the list of
    // fields for all groups.

    // TODO: sort these by index? This functionality
    // isn't really used by IDP anyway so this doesn't matter yet.
    this.config.fields = {};
    for (const group of Object.values(this.config.groups)) {
      Object.assign(this.config.fields, group.fields);
    }

    return this.config;
  }
}

class PhaseBuilder extends ConfigBuilder<GravityApiPhase> {
  protected config: GravityApiPhase = {
    id: null,
    name: null,
    index: null,
    invisible: false,
    cards: null,
  };

  constructor(config?: GravityApiPhase) {
    super();
    if (config) {
      this.config = config;
    }
  }

  id(id: string) {
    this.config.id = id;
    return this;
  }

  name(name: string) {
    this.config.name = name;
    return this;
  }

  invisible(isInvisible: boolean = true) {
    this.config.invisible = isInvisible;
    return this;
  }

  index(index: number) {
    this.config.index = index;
    return this;
  }

  finish() {
    if (!this.config.id) {
      throw new Error('Phase must have an id');
    }

    if (!this.config.name) {
      this.config.name = this.config.id;
    }

    return this.config;
  }
}


/**
 * A helper for building Gravity board configs
 *
 * This was designed to help with building board configs for use in unit tests.
 * It uses a fluent-builder pattern, allowing you to construct a board config in
 * a single chained expression.
 *
 * It's designed to be convenient, while still supporting the more advanced
 * features of Gravity.
 *
 * To use this, create an instance of the builder, then chain together methods
 * to add fields to the board. When finished, call finish() to receive your
 * completed config.
 *
 * Some of the methods take a function parameter, which will provide access to
 * a sub-builder, for example allowing you to customize a field.
 *
 * This builder supports Templates and Groups, but it also has a convenience
 * addField() method which will make a default template and group for you.
 *
 * For examples of use, see the unit tests.
 */
export class BoardConfigBuilder extends ConfigBuilder<GravityApiBoard> {
  protected config: GravityApiBoard = {
    id: null,
    name: null,
    permissions: null,
    orderField: null,
    templates: {},
    phases: {},
    filters: {},
    boardType: null,
    views: {}
  };

  constructor(id_or_config: string | GravityApiBoard = null) {
    super();
    if (typeof(id_or_config) === 'string') {
      this.id(id_or_config);
    } else if (id_or_config) {
      this.config = cloneDeep(id_or_config);
    }
  }

  /**
   * Creates or extends a Template for this board using a sub-builder.
   *
   * If you pass the ID of an existing template, the sub-builder will update the
   * existing config.
   *
   * @param id the template id
   * @fn the builder function.
   */
  template(id: string, fn: BuilderFunction<TemplateBuilder>) {
    const templateBuilder = new TemplateBuilder(this.config.templates[id]);
    templateBuilder.id(id);
    fn(templateBuilder);
    const template = templateBuilder.finish();
    this.config.templates[template.id] = template;
    return this;
  }

  /**
   * Builds the default template for this board
   *
   * The default template has the same id as the board, therefore it's necessary
   * to set the board id before calling this method.
   *
   * The defaultTemplate() method is also used by addField() so the same
   * limitation applies. Set the id first.
   */
  defaultTemplate(fn: BuilderFunction<TemplateBuilder>) {
    return this.template(this.config.id, fn);
  }

  /**
   * Sets the board id.
   */
  id(id: string) {
    this.config.id = id;
    return this;
  }

  /**
   * Sets the board id.
   */
  name(name: string) {
    this.config.name = name;
    return this;
  }

  permissions(permissions: Permissions) {
    this.config.permissions = permissions;
    return this;
  }

  /**
   * Adds a field to the default template
   *
   * This will add a field to the default template in the default group. For
   * simple boards like the ones IDP uses, this will be fine because we don't
   * really use multiple groups or templates.
   *
   * Before calling addField() (or defaultTemplate()) it's necessary to set the
   * id on the board by calling .id(...)
   *
   * The new field will be created with the given id, and if not overridden the
   * new field's name and label will be set to the id.
   *
   * Further properties on the field can be customized by providing a function
   * as the second param and using the field builder.
   *
   * @param id the field id
   */
  addField(id: string, fn: BuilderFunction<TemplateFieldBuilder> = null) {
    return this.defaultTemplate(t => t.addField(id, fn));
  }

  addPhase(id: string, fn: BuilderFunction<PhaseBuilder> = null) {
    const phaseBuilder = new PhaseBuilder(this.config.phases[id]);
    phaseBuilder.id(id);
    if (fn) {
      fn(phaseBuilder);
    }

    const phase = phaseBuilder.finish();
    if (!isFinite(phase.index)) {
      phase.index = getNextIndex(this.config.phases);
    }

    this.config.phases[phase.id] = phase;
    return this;
  }

  templates(fn: BuilderFunction<TemplateBuilder>): this {
    Object.keys(this.config.templates).forEach(id => this.template(id, fn));
    return this;
  }

  /**
   * Adds 'current' and 'archive' phases to this board config.
   */
  addDefaultPhases() {
    return this
      .addPhase('current')
      .addPhase('archive', p => p.invisible());
  }

  deleteField(fieldId: string) {
    return this.templates(t => t.deleteField(fieldId));
  }

  deletePhase(phaseId: string): this {
    delete this.config.phases[phaseId];
    return this;
  }

  optimizeIndexes() {
    fixIndexes(this.config.phases);
    this.templates(t => t.optimizeIndexes());
    return this;
  }

  finish() {
    return this.config;
  }
}
