import { Injectable, Inject, Optional } from '@angular/core';
import { Observable, EMPTY, of } from 'rxjs';

import { ObjectMap, uniqueId } from '@bmi/utils';

import { GravityApiBoard } from '../api-types/gravity-api-board-config';
import { CardData, CardFields } from '../api-types/gravity-api-cardview';
import { BoardQueryBuilder, BoardQueryParams, IBoardService } from '../services/board.service';

import { GravityConfigService } from '../core/gravity-config.service';
import { FakeBoard, GravityTestbed2Options, GRAVITY_TESTBED_2_OPTIONS_PROVIDER } from './gravity-testbed-options';
import { applyCardFilter } from '../utils/apply-gravity-filter';
import { Filter } from '../model/filter';

import sortBy from 'lodash-es/sortBy';
import cloneDeep from 'lodash-es/cloneDeep';
import keyBy from 'lodash-es/keyBy';
import mapValues from 'lodash-es/mapValues';


@Injectable()
export class FakeBoardService implements IBoardService {
  public boards: FakeBoard[] = [];

  constructor (
    private boardConfigService: GravityConfigService,
    @Inject(GRAVITY_TESTBED_2_OPTIONS_PROVIDER) private testbedOptions: GravityTestbed2Options
  ) {
    if (testbedOptions) {
      this.boards = testbedOptions.boards || [];
      this.boards.forEach(b => b.cards = b.cards || []);
    }
  }

  search<T extends CardFields = CardFields>(query: BoardQueryParams): Observable<CardData<T>[]> {
    if (query.viewId && query.viewId !== 'all') {
      throw new Error('FakeBoardService: fake search does not support views!');
    }

    let cards = [...this.getBoardCards(query.boardId)];

    const filter = new Filter(query.boardId);
    if (query.conditions) {
      query.conditions.forEach(c => filter.addCondition(c.fieldName, c.value, c.operation, c.conditionType));
    }

    cards = applyCardFilter(cards, filter);

    if (query.order) {
      const isSortingId = query.order === 'id' || query.order === '_id';
      if (isSortingId) {
        cards = sortBy(cards, c => c.id);
      } else {
        // Something weird is happening with type inference here due to the type
        // being returned by the sort key lambda... I'm really not sure why, but
        // telling it we're sorting CardData stops it inferring CardData |
        // number
        cards = sortBy<CardData>(cards, c => c.fields[query.order]);
      }
    }

    if (query.descending) {
      cards.reverse();
    }

    return of(cards as CardData<T>[]);
  }

  query(boardId: string): BoardQueryBuilder {
    return new BoardQueryBuilder(this, boardId);
  }

  getConfig(boardId: string) {
    return this.boardConfigService.watchBoardConfig(boardId);
  }

  fetchById(boardId: string, cardId: string) {
    return of(this.getBoardCards(boardId).find(card => card.id === cardId) || null);
  }

  saveCard(boardId: string, card: CardData): Observable<CardData> {
    const board = this.getBoard(boardId);
    if (!board) {
      throw new Error(`Board does not exist: ${boardId}`);
    }

    const clonedCard = cloneDeep(card);
    if (!card.id) {
      clonedCard.id = uniqueId('C', 4);
    }

    // Remove the existing card from the list by ID (in case we are updating an
    // existing card...), then add the updated card.
    board.cards = board.cards.filter(c => c.id !== clonedCard.id);
    board.cards.push(clonedCard);

    return of(clonedCard);
  }

  getTitles(boardId: string): Observable<ObjectMap<string>> {
    return of(
      mapValues(
        keyBy(this.getBoardCards(boardId), c => c.id),
        c => c.title
      )
    );
  }

  public getBoard(boardId: string): FakeBoard {
    return this.boards.find(x => x.config.id === boardId);
  }

  public getBoardCards(boardId: string): CardData[] {
    const board = this.getBoard(boardId);
    return (board && board.cards) || [];
  }

  deleteCard(boardId: string, cardId: string): Observable<CardData> {
    console.log(`deleteCard`)
    const board = this.getBoard(boardId);
    if (!board) {
      throw new Error(`Board does not exist: ${boardId}`);
    }

    const card = board.cards.find(c => c.id === cardId);
    if (!card) {
      throw new Error(`Card does not exist: ${cardId}`);
    }

    board.cards = board.cards.filter(c => c.id !== cardId);
    return of(card);
  }
}
