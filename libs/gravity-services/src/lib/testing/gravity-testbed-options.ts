import { InjectionToken } from '@angular/core';
import { GravityApiCardView, CardData, CardFields } from '../api-types/gravity-api-cardview';
import { GravityApiBoard } from '../api-types/gravity-api-board-config';

export const GRAVITY_TESTBED_OPTIONS_PROVIDER = new InjectionToken('Gravity testbed options');

export const GRAVITY_TESTBED_2_OPTIONS_PROVIDER = new InjectionToken('Gravity testbed 2 options');


export interface GravityTestbedOptions {
  baseUrl: string;
  cards: { [boardId: string]: GravityApiCardView[] };
  boards: GravityApiBoard[];
  useFakeGravityService?: boolean;
  injectNullHttpClient?: boolean;
  customModelClasses?: {[boardId: string]: Function};
}



// These are for the new BMI-flavoured Gravity testbed. This should be
// preferred for new code, and the old testbed/code phased out
export interface FakeBoard<T extends CardFields = CardFields> {
  config: GravityApiBoard;
  cards?: CardData[];
}


export interface GravityTestbed2Options {
  baseUrl: string;
  boards: FakeBoard[];
}
