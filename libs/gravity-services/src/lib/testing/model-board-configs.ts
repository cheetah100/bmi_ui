import { BoardConfigBuilder } from '../testing/board-config-builder';
import { makeCardJson } from './gravity-test-tools';

export const QUARTER_BOARD_CONFIG = new BoardConfigBuilder()
  .id('quarters')
  .addField('name', f => f.required())
  .addField('start_date', f => f.label('Start Date').type('DATE'))
  .addField('end_date', f => f.label('End Date').type('DATE'))
  .addField('show_in_filters', f => f.type('BOOLEAN').required())
  .addField('show_in_roadmap', f => f.type('BOOLEAN').required())
  .addPhase('current')
  .addPhase('archive', p => p.invisible())
  .finish();


export const FISCAL_MONTHS_BOARD_CONFIG = new BoardConfigBuilder()
  .id('fiscal_months')
  .name('Fiscal Months')
  .addField('name', f => f.label('Name'))
  .addField('quarter', f => f.optionlist('quarters').label('Quarter'))
  .addField('start_date', f => f.type('DATE'))
  .addPhase('current')
  .addPhase('archive', p => p.invisible())
  .finish();


export const QUARTER_CARDS = makeCardJson('quarters', [
  {
    'id': 'Q1FY18',
    'template': 'quarters',
    'creator': 'admin',
    'created': 1491704776039,
    'modifiedby': 'hbajes',
    'modified': 1533220058704,
    'color': 'white',
    'fields': {
      'name': 'Q1FY18',
      'start_date': 1501398000000,
      'end_date': 1509260400000,
      'show_in_roadmap': true,
      'show_in_filters': true,
      'id': 'Q1FY18',
      'title': 'Q1FY18',
      'uuid': 'bc73ccbc-03d4-495c-94db-99b12dc06e7c'
    },
    'board': 'quarters',
    'title': 'Q1FY18',
    'lock': null,
    'phase': 'current',
    'deleted': false,
    'alerted': false
  },
  {
    'id': 'Q2FY18',
    'template': 'quarters',
    'creator': 'admin',
    'created': 1491704776230,
    'modifiedby': 'hbajes',
    'modified': 1533220058747,
    'color': 'white',
    'fields': {
      'name': 'Q2FY18',
      'start_date': 1509260400000,
      'end_date': 1517126400000,
      'show_in_roadmap': true,
      'show_in_filters': true,
      'id': 'Q2FY18',
      'title': 'Q2FY18',
      'uuid': '5fb87b36-d575-4fd3-99e1-493398d349a0'
    },
    'board': 'quarters',
    'title': 'Q2FY18',
    'lock': null,
    'phase': 'current',
    'deleted': false,
    'alerted': false
  },
  {
    'id': 'Q3FY18',
    'template': 'quarters',
    'creator': 'admin',
    'created': 1491704776829,
    'modifiedby': 'hbajes',
    'modified': 1533220058759,
    'color': 'white',
    'fields': {
      'name': 'Q3FY18',
      'start_date': 1517126400000,
      'end_date': 1524985200000,
      'show_in_roadmap': true,
      'show_in_filters': true,
      'id': 'Q3FY18',
      'title': 'Q3FY18',
      'uuid': '2be61978-1906-4a26-8192-70e75bb6fe24'
    },
    'board': 'quarters',
    'title': 'Q3FY18',
    'lock': null,
    'phase': 'current',
    'deleted': false,
    'alerted': false
  },
  {
    'id': 'Q4FY18',
    'template': 'quarters',
    'creator': 'admin',
    'created': 1493594664951,
    'modifiedby': 'hbajes',
    'modified': 1533220058770,
    'color': 'white',
    'fields': {
      'name': 'Q4FY18',
      'start_date': 1524985200000,
      'end_date': 1532761200000,
      'show_in_roadmap': true,
      'show_in_filters': true,
      'group-detail': 'Quarters',
      'id': 'Q4FY18',
      'title': 'Q4FY18',
      'order': '0'
    },
    'board': 'quarters',
    'title': 'Q4FY18',
    'lock': null,
    'phase': 'current',
    'deleted': false,
    'alerted': false
  },
  {
    'id': 'Q1FY19',
    'template': 'quarters',
    'creator': 'admin',
    'created': 1509049640963,
    'modifiedby': 'hbajes',
    'modified': 1533220058781,
    'color': 'white',
    'fields': {
      'name': 'Q1FY19',
      'start_date': 1532847600000,
      'end_date': 1540623600000,
      'show_in_roadmap': true,
      'show_in_filters': true,
      'title': 'Q1FY19'
    },
    'board': 'quarters',
    'title': 'Q1FY19',
    'lock': null,
    'phase': 'current',
    'deleted': false,
    'alerted': false
  },
  {
    'id': 'Q2FY19',
    'template': 'quarters',
    'creator': 'admin',
    'created': 1509049641169,
    'modifiedby': 'hbajes',
    'modified': 1533220058797,
    'color': 'white',
    'fields': {
      'name': 'Q2FY19',
      'start_date': 1540710000000,
      'end_date': 1548489600000,
      'show_in_roadmap': true,
      'show_in_filters': true,
      'title': 'Q2FY19'
    },
    'board': 'quarters',
    'title': 'Q2FY19',
    'lock': null,
    'phase': 'current',
    'deleted': false,
    'alerted': false
  },
  {
    'id': 'Q3FY19',
    'template': 'quarters',
    'creator': 'admin',
    'created': 1509049641271,
    'modifiedby': 'hbajes',
    'modified': 1533220058809,
    'color': 'white',
    'fields': {
      'name': 'Q3FY19',
      'start_date': 1548576000000,
      'end_date': 1556348400000,
      'show_in_roadmap': true,
      'show_in_filters': true,
      'title': 'Q3FY19'
    },
    'board': 'quarters',
    'title': 'Q3FY19',
    'lock': null,
    'phase': 'current',
    'deleted': false,
    'alerted': false
  },
  {
    'id': 'Q4FY19',
    'template': 'quarters',
    'creator': 'admin',
    'created': 1509049641388,
    'modifiedby': 'hbajes',
    'modified': 1533220058819,
    'color': 'white',
    'fields': {
      'name': 'Q4FY19',
      'start_date': 1556434800000,
      'end_date': 1564210800000,
      'show_in_roadmap': true,
      'show_in_filters': true,
      'title': 'Q4FY19'
    },
    'board': 'quarters',
    'title': 'Q4FY19',
    'lock': null,
    'phase': 'current',
    'deleted': false,
    'alerted': false
  }
]);

export const FISCAL_MONTH_CARDS = makeCardJson('fiscal_months', [
  { 'id': 'FM000013', 'template': 'fiscal_months', 'creator': 'mejackso', 'created': 1493601062175, 'modifiedby': null, 'modified': null, 'color': null, 'fields': { 'name': 'Q1FY18 Aug', 'quarter': 'Q1FY18', 'start_date': 1501398000000, 'title': 'Q1FY18 Aug' }, 'board': 'fiscal_months', 'title': 'Q1FY18 Aug', 'lock': null, 'phase': 'current', 'deleted': false, 'alerted': false },
  { 'id': 'FM000014', 'template': 'fiscal_months', 'creator': 'mejackso', 'created': 1493601095489, 'modifiedby': null, 'modified': null, 'color': null, 'fields': { 'name': 'Q1FY18 Sep', 'quarter': 'Q1FY18', 'start_date': 1503817200000, 'title': 'Q1FY18 Sep' }, 'board': 'fiscal_months', 'title': 'Q1FY18 Sep', 'lock': null, 'phase': 'current', 'deleted': false, 'alerted': false },
  { 'id': 'FM000015', 'template': 'fiscal_months', 'creator': 'mejackso', 'created': 1493601128669, 'modifiedby': null, 'modified': null, 'color': null, 'fields': { 'name': 'Q1FY18 Oct', 'quarter': 'Q1FY18', 'start_date': 1506236400000, 'title': 'Q1FY18 Oct' }, 'board': 'fiscal_months', 'title': 'Q1FY18 Oct', 'lock': null, 'phase': 'current', 'deleted': false, 'alerted': false },
  { 'id': 'FM000016', 'template': 'fiscal_months', 'creator': 'mejackso', 'created': 1493601149355, 'modifiedby': null, 'modified': null, 'color': null, 'fields': { 'name': 'Q2FY18 Nov', 'quarter': 'Q2FY18', 'start_date': 1509260400000, 'title': 'Q2FY18 Nov' }, 'board': 'fiscal_months', 'title': 'Q2FY18 Nov', 'lock': null, 'phase': 'current', 'deleted': false, 'alerted': false },
  { 'id': 'FM000017', 'template': 'fiscal_months', 'creator': 'mejackso', 'created': 1493601181106, 'modifiedby': null, 'modified': null, 'color': null, 'fields': { 'name': 'Q2FY18 Dec', 'quarter': 'Q2FY18', 'start_date': 1511683200000, 'title': 'Q2FY18 Dec' }, 'board': 'fiscal_months', 'title': 'Q2FY18 Dec', 'lock': null, 'phase': 'current', 'deleted': false, 'alerted': false },
  { 'id': 'FM000018', 'template': 'fiscal_months', 'creator': 'mejackso', 'created': 1493601231056, 'modifiedby': null, 'modified': null, 'color': null, 'fields': { 'name': 'Q2FY18 Jan', 'quarter': 'Q2FY18', 'start_date': 1514102400000, 'title': 'Q2FY18 Jan' }, 'board': 'fiscal_months', 'title': 'Q2FY18 Jan', 'lock': null, 'phase': 'current', 'deleted': false, 'alerted': false },
  { 'id': 'FM000019', 'template': 'fiscal_months', 'creator': 'mejackso', 'created': 1493601253736, 'modifiedby': null, 'modified': null, 'color': null, 'fields': { 'name': 'Q3FY18 Feb', 'quarter': 'Q3FY18', 'start_date': 1517126400000, 'title': 'Q3FY18 Feb' }, 'board': 'fiscal_months', 'title': 'Q3FY18 Feb', 'lock': null, 'phase': 'current', 'deleted': false, 'alerted': false },
  { 'id': 'FM000020', 'template': 'fiscal_months', 'creator': 'mejackso', 'created': 1493601289236, 'modifiedby': null, 'modified': null, 'color': null, 'fields': { 'name': 'Q3FY18 Mar', 'quarter': 'Q3FY18', 'start_date': 1519545600000, 'title': 'Q3FY18 Mar' }, 'board': 'fiscal_months', 'title': 'Q3FY18 Mar', 'lock': null, 'phase': 'current', 'deleted': false, 'alerted': false },
  { 'id': 'FM000021', 'template': 'fiscal_months', 'creator': 'mejackso', 'created': 1493601318735, 'modifiedby': null, 'modified': null, 'color': null, 'fields': { 'name': 'Q3FY18 Apr', 'quarter': 'Q3FY18', 'start_date': 1521961200000, 'title': 'Q3FY18 Apr' }, 'board': 'fiscal_months', 'title': 'Q3FY18 Apr', 'lock': null, 'phase': 'current', 'deleted': false, 'alerted': false },
  { 'id': 'FM000022', 'template': 'fiscal_months', 'creator': 'mejackso', 'created': 1493601370334, 'modifiedby': null, 'modified': null, 'color': null, 'fields': { 'name': 'Q4FY18 May', 'quarter': 'Q4FY18', 'start_date': 1524985200000, 'title': 'Q4FY18 May' }, 'board': 'fiscal_months', 'title': 'Q4FY18 May', 'lock': null, 'phase': 'current', 'deleted': false, 'alerted': false },
  { 'id': 'FM000023', 'template': 'fiscal_months', 'creator': 'mejackso', 'created': 1493601393715, 'modifiedby': null, 'modified': null, 'color': null, 'fields': { 'name': 'Q4FY18 Jun', 'quarter': 'Q4FY18', 'start_date': 1527404400000, 'title': 'Q4FY18 Jun' }, 'board': 'fiscal_months', 'title': 'Q4FY18 Jun', 'lock': null, 'phase': 'current', 'deleted': false, 'alerted': false },
  { 'id': 'FM000024', 'template': 'fiscal_months', 'creator': 'mejackso', 'created': 1493601436240, 'modifiedby': null, 'modified': null, 'color': null, 'fields': { 'name': 'Q4FY18 Jul', 'quarter': 'Q4FY18', 'start_date': 1529823600000, 'title': 'Q4FY18 Jul' }, 'board': 'fiscal_months', 'title': 'Q4FY18 Jul', 'lock': null, 'phase': 'current', 'deleted': false, 'alerted': false }
]);
