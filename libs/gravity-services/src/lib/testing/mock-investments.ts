import { BoardConfigBuilder } from '../testing/board-config-builder';
import { makeCardJson } from '../testing/gravity-test-tools';


export const MOCK_INVESTMENTS_BOARD_CONFIG = new BoardConfigBuilder('investments')
  .addField('project', f => f.optionlist('projects'))
  .addField('value', f => f.type('NUMBER'))
  .addField('quarter', f => f.optionlist('quarters'))
  .addPhase('current')
  .addPhase('archive', p => p.invisible())
  .finish();


export const MOCK_PROJECTS_BOARD_CONFIG = new BoardConfigBuilder()
  .id('projects')
  .addField('name')
  .addField('activeQuarters', f => f.optionlist('quarters').type('LIST'))
  .addPhase('current')
  .addPhase('archive', p => p.invisible())
  .finish();


export const MOCK_INVESTMENTS_CARDS = makeCardJson('investments', [
  { id: 'iv01', fields: { project: 'p01', quarter: 'Q1FY19', value: 400 } },
  { id: 'iv02', fields: { project: 'p01', quarter: 'Q2FY19', value: 800 } },
  { id: 'iv03', fields: { project: 'p02', quarter: 'Q2FY18', value: 3000000 } },
  { id: 'iv04', fields: { project: 'p03', quarter: 'Q4FY18', value: 80000 } },
]);

export const MOCK_PROJECTS_CARDS = makeCardJson('projects', [
  { id: 'p01', phase: 'current', fields: { name: 'Test Project', activeQuarters: ['Q1FY19', 'Q2FY19'] } },
  { id: 'p02', phase: 'archive', fields: { name: 'Colonize Venus', activeQuarters: ['Q4FY17', 'Q1FY18', 'Q2FY18'] } },
  { id: 'p03', phase: 'current', fields: { name: 'Moon Base', activeQuarters: ['Q3FY18', 'Q4FY18', 'Q1FY19', 'Q2FY19'] } },
  { id: 'p04', phase: 'current', fields: { name: 'Blah', activeQuarters: [] } }
]);
