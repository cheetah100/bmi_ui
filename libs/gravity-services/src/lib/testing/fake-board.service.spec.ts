import { TestBed } from '@angular/core/testing';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { BoardQueryParams } from '../services/board.service';
import { FakeBoardService } from './fake-board.service';
import { CardData } from '../api-types/gravity-api-cardview';

import { makeCardJson } from './gravity-test-tools';
import { gravityTestbed2, makeBoard } from '../testing/gravity-testbed-2';
import { BoardConfig } from '../core/board-config';
import { BoardConfigBuilder } from './board-config-builder';

import sortBy from 'lodash-es/sortBy';


function expectEmitted<T>(observable: Observable<T>) {
  const results: T[] = [];
  let hasCompleted = false;
  observable.subscribe({
    next: result => results.push(result),
    complete: () => hasCompleted = true
  });

  expect(hasCompleted).toBeTruthy();
  return expect(results);
}

function expectOne<T>(observable: Observable<T>) {
  return expect(subscribeOne(observable));
}

function subscribeOne<T>(observable: Observable<T>): T {
  const results: T[] = [];
  let hasCompleted = false;
  observable.subscribe({
    next: result => results.push(result),
    complete: () => hasCompleted = true
  });

  expect(results.length).toBe(1);
  expect(hasCompleted).toBeTruthy();
  return results[0];
}


describe('FakeBoardService', () => {
  let service: FakeBoardService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        gravityTestbed2({
          boards: [
            makeBoard({
              config: new BoardConfigBuilder('test')
                .addField('name')
                .addField('value', f => f.type('NUMBER'))
                .addField('active', f => f.type('BOOLEAN')),

              cards: [
                {id: 'C1', title: 'Card 1', fields: {name: 'foo', value: 6, active: false}},
                {id: 'C2', title: 'Card 2', fields: {name: 'john', value: 88, active: true}},
                {id: 'C3', title: 'Card 3', fields: {name: 'frank', value: 99, active: true}},
                {id: 'C4', title: 'Card 4', fields: {name: 'sue', value: 101, active: true}},
                {id: 'C5', title: 'Card 5', fields: {name: 'lev', value: -3, active: true}},
              ]
            }),
          ]
        })
      ]
    });

    service = TestBed.get(FakeBoardService) as FakeBoardService;
  });

  describe('fetchById', () => {
    it('should get cards by id', () => {
      expectEmitted(service.fetchById('test', 'C2').pipe(map(c => c.id))).toEqual(['C2']);
    });

    it('should return null if the card does not exist on the board', () => {
      expectEmitted(service.fetchById('test', 'invalid-id')).toEqual([null]);
    });
  });

  describe('search', () => {
    let allCards: CardData[];

    beforeEach(() => {
      allCards = service.getBoardCards('test');
    });

    function expectSearch(query: BoardQueryParams) {
      return expectEmitted(service.search(query));
    }

    it('should return the full list of cards if no conditions provided', () => {
      expectOne(service.search({boardId: 'test'})).toEqual(allCards);
    });

    it('should apply filters', () => {
      expectOne(service.search({
        boardId: 'test',
        conditions: [
          {fieldName: 'name', value: 'frank|lev', operation: 'EQUALTO'}
        ]
      })).toEqual(allCards.filter(c => ['frank', 'lev'].includes(c.fields['name'])))
    });

    it('should throw an error if you try to use a view', () => {
      expect(() => service.search({
        boardId: 'test',
        viewId: 'someview'
      })).toThrow();
    });

    it('should support sorting by fields', () => {
      expectOne(service.search({
        boardId: 'test',
        order: 'name'
      })).toEqual(sortBy<CardData>(allCards, c => c.fields['name']));
    });

    it('should support descending sort order', () => {
      expectOne(service.search({
        boardId: 'test',
        order: 'name',
        descending: true,
      })).toEqual(sortBy<CardData>(allCards, c => c.fields['name']).reverse());
    });
  });

  describe('getConfig', () => {
    it('should emit the latest config for a known board synchronously', () => {
      let emitted: BoardConfig[] = [];
      let hasCompleted = false;
      service.getConfig('test').subscribe({
        next: config => emitted.push(config),
        complete: () => hasCompleted = true
      });

      expect(emitted.length).toBe(1);
    });
  });

  describe('saveCard()', () => {
    let card: CardData;
    beforeEach(() => {
      card = makeCardJson('test', [{fields: {name: 'new', value: 9000, active: true}}])[0];
    });

    it('should generate an ID if no id is provided', () => {
      const addedCard = subscribeOne(service.saveCard('test', card));
      expect(addedCard.id).toMatch(/.+/);
    });

    it('should add the card to the board, so it can be fetched', () => {
      const addedCard = subscribeOne(service.saveCard('test', card));
      const fetchedCard = subscribeOne(service.fetchById('test', addedCard.id));

      expect(addedCard).toEqual(fetchedCard);
    });

    it('should replace an existing card of the same ID', () => {
      const existingCard = subscribeOne(service.fetchById('test', 'C3'));
      const updatedCard = {
        ...existingCard,
        fields: {
          ...existingCard.fields,
          active: false,
        }
      };

      const savedCard = subscribeOne(service.saveCard('test', updatedCard));
      const refetchedCard = subscribeOne(service.fetchById('test', savedCard.id));
      expect(savedCard.id).toBe(updatedCard.id);
      expect(refetchedCard).toEqual(savedCard);
    });
  });
});
