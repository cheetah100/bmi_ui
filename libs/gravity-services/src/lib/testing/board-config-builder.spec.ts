import { BoardConfigBuilder } from './board-config-builder';
import { GravityApiBoard, GravityApiTemplateField } from '../api-types/gravity-api-board-config';
import cloneDeep from 'lodash-es/cloneDeep';
import sortBy from 'lodash-es/sortBy';

describe('[Gravity Config Builders] Building board configs', () => {
  it('should work!', () => {
    const builder = new BoardConfigBuilder();
    const config = builder
      .id('test_board')
      .name('Test Board')
      .addField('id', f => f.type('STRING'))
      .finish();

    expect(config.id).toBe('test_board');
    expect(config.name).toBe('Test Board');
    expect(config.templates['test_board']).toBeDefined();
    expect(Object.values(config.templates['test_board'].groups).length).toBe(1);
  });

  it('should use the board id as the default template id and name', () => {
    const builder = new BoardConfigBuilder();
    const config = builder.id('test').defaultTemplate(() => {}).finish();
    const defaultTemplate = config.templates['test'];
    expect(defaultTemplate).toBeDefined();
    expect(defaultTemplate.id).toBe('test');
    expect(defaultTemplate.name).toBe('test');
  });

  it('should throw an error if you create a default template without setting id', () => {
    expect(() => {
      new BoardConfigBuilder().defaultTemplate(() => {}).finish();
    }).toThrowError();
  });
});


describe('[Board Config Builder] Adding fields', () => {
  it('should set default values for name and label to the id if not specified', () => {
    const config = new BoardConfigBuilder()
      .id('test')
      .addField('testField')
      .finish();

    const field = config.templates['test'].fields['testField'];
    expect(field.name).toBe('testField');
    expect(field.label).toBe('testField');
  });

  it('should not override manually set names', () => {
    const config = new BoardConfigBuilder()
    .id('test')
    .addField('testField', f => f.name('OVERRIDE'))
    .finish();

    const field = config.templates['test'].fields['testField'];
    expect(field.name).toBe('OVERRIDE');
  });

  it('should not override manually set labels', () => {
    const config = new BoardConfigBuilder()
    .id('test')
    .addField('testField', f => f.label('OVERRIDE'))
    .finish();

    const field = config.templates['test'].fields['testField'];
    expect(field.label).toBe('OVERRIDE');
  });

  it('should assign ascending indexes to fields', () => {
    const config = new BoardConfigBuilder()
      .id('test')
      .addField('test1')
      .addField('test2')
      .finish();

    const field1 = config.templates['test'].fields['test1'];
    const field2 = config.templates['test'].fields['test2'];

    expect(field1.index).toBe(1);
    expect(field2.index).toBe(2);
  });

  it('should not override manually set field indexes', () => {
    const config = new BoardConfigBuilder()
      .id('test')
      .addField('test1', f => f.index(27))
      .finish();

    expect(config.templates['test'].fields['test1'].index).toBe(27);
  });

  it('should assign subsequent fields higher indexes', () => {
    const config = new BoardConfigBuilder()
      .id('test')
      .addField('test1', f => f.index(27))
      .addField('test2')
      .finish();

    expect(config.templates['test'].fields['test1'].index).toBe(27);
    expect(config.templates['test'].fields['test2'].index).toBe(28);
  });

  it('should not throw an error if a field is redefined', () => {
    expect(() => {
      new BoardConfigBuilder()
        .id('test')
        .addField('test1')
        .addField('test1')
        .finish();
    }).not.toThrowError();
  });

  it('should modify and extend an existing field', () => {
    const config = new BoardConfigBuilder('foo')
      .addField('test', f => f.required())
      .addField('test', f => f.optionlist('test-board'))
      .addField('test', f => f.name('Test'))
      .finish();

    const field = config.templates['foo'].fields['test'];
    expect(field).toBeTruthy();
    expect(field.name).toBe('Test');
    expect(field.optionlist).toBe('test-board');
    expect(field.required).toBeTruthy();
  });
});


describe('[Board Config Builder] Template Groups', () => {
  it('should populate the template fields from the default group', () => {
    const config = new BoardConfigBuilder()
      .id('test')
      .addField('testField')
      .finish();

    const template = config.templates['test'];
    expect(template.fields).toEqual(template.groups['default'].fields);
  });

  it('should merge fields from different groups', () => {
    const config = new BoardConfigBuilder()
      .id('test')
      .defaultTemplate(t => {
        t.group('group1', g => g.addField('group1Field'));
        t.group('group2', g => g.addField('group2Field'));
      })
      .finish();

    const template = config.templates['test'];
    expect(template.fields.group1Field).toBeDefined();
    expect(template.fields['group1Field']).toBe(template.groups['group1'].fields['group1Field']);

    expect(template.fields.group2Field).toBeDefined();
    expect(template.fields['group2Field']).toBe(template.groups['group2'].fields['group2Field']);
  });

  it('should allow subsequent edits to the same group', () => {
    const config = new BoardConfigBuilder()
      .id('test')
      .defaultTemplate(t => {
        t.group('testgroup', g => g.addField('testField'));
        t.group('testgroup', g => g.name('Test Group'));
      })
      .finish();

    const template = config.templates['test'];
    expect(template.groups['testgroup'].name).toBe('Test Group');
    expect(template.groups['testgroup'].fields['testField']).toBeDefined();
  });

  it('should assign ascending indexes to groups', () => {
    const config = new BoardConfigBuilder()
      .id('test')
      .defaultTemplate(t => {
        t.group('g1', () => {});
        t.group('g2', () => {});
      })
      .finish();

    const template = config.templates['test'];
    const group1 = template.groups['g1'];
    const group2 = template.groups['g2'];

    expect(group1.index).toBe(1);
    expect(group2.index).toBe(2);
  });
});


describe('[Board Config Builder] Modifying existing configs', () => {
  const TEST_CONFIG = new BoardConfigBuilder('test_board')
    .name('Original Name')
    .addField('field1', f => f.required().optionlist('opt'))
    .addField('field2', f => f.type('NUMBER'))
    .addDefaultPhases()
    .finish();

  let builder: BoardConfigBuilder;
  let originalConfig = cloneDeep(TEST_CONFIG);
  beforeEach(() => {
    builder = new BoardConfigBuilder(originalConfig);
  });

  it('should not make modifications automatically', () => {
    expect(builder.finish()).toEqual(originalConfig);
  });

  it('should clone the configuration', () => {
    expect(builder.finish()).not.toBe(originalConfig);
  });


  it('should support adding phases', () => {
    builder.addPhase('defunct', p => p.name('Defunct'));
    const conf = builder.finish();
    expect(Object.keys(conf.phases).sort()).toEqual(['archive', 'current', 'defunct']);
  })

  it('should support deleting fields', () => {
    builder.deleteField('field1');
    const config = builder.finish();
    const fields = Object.keys(config.templates['test_board'].fields);
    expect(fields).not.toContain('field1');
    expect(fields).toContain('field2');
  });

  it('should support deleting phases', () => {
    builder.deletePhase('current');
    const config = builder.finish();
    const phases = Object.keys(config.phases);
    expect(phases).not.toContain('current');
    expect(phases).toContain('archive');
  });
});

describe('[Board Config Builder] Optimize indexes', () => {
  let unfixed: GravityApiBoard;
  let fixed: GravityApiBoard;
  let sortedFields: GravityApiTemplateField[];

  beforeEach(() => {
    const builder = new BoardConfigBuilder('test')
      .addField('test1', f => f.index(100))
      .addField('test2')
      .addField('test3', f => f.index(20))
      .defaultTemplate(t => t.group('details', g => g.index(-1)));

    unfixed = cloneDeep(builder.finish());
    fixed = builder.optimizeIndexes().finish();
    sortedFields = sortBy(Object.values(fixed.templates['test'].fields), f => f.index);
  });

  it('should order the fields by index', () => {
    expect(sortedFields.map(f => f.id)).toEqual(['test3', 'test1', 'test2']);
  })

  it('should number the fields starting from 1', () => {
    expect(sortedFields.map(f => f.index)).toEqual([1, 2, 3]);
  })
});
