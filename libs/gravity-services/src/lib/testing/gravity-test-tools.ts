import { Provider } from '@angular/core';
import { GravityApiUser } from '../api-types/gravity-api-user';
import { CardData, GravityApiCardView } from '../api-types/gravity-api-cardview';
import { GravityUrlService } from '../services/gravity-url.service';
import { GravityApiBoard } from '../api-types/gravity-api-board-config';
import { GravityParameters, GRAVITY_PARAMETERS } from '../gravity-parameters';
import { keyBy } from 'lodash-es';
import { cloneDeep } from 'lodash-es';
import { DashboardRepository } from '../dashboard/dashboard.repository';
import { FakeGravityService } from '../testing/fake-gravity-service';
import { GravityTestbedOptions, GRAVITY_TESTBED_OPTIONS_PROVIDER } from '../testing/gravity-testbed-options';
import { HttpClient } from '@angular/common/http';
import { Gravity } from '../gravity';
import { GravityConfigService } from '../core/gravity-config.service';
import { DataStore } from '../core/data-store';
import { GravityConverter } from '../core/gravity-converter';
import { UserRepository } from '../core/user.repository';
import { GravityCache } from '../core/gravity-cache';
import { GravityService } from '../core/gravity.service';
import { ObjectMap } from '@bmi/utils';


export const TEST_USER: GravityApiUser = {
  id: 'mock-user',
  firstname: 'Terry',
  surname: 'Tester',
  fullName: 'Terry Tester',
  teams: {
    'administrators': '',
    'readonly': ''
  },
  email: 'terrytes@brand.com.invalid',
  name: 'mock-user'
};


/**
 * Gets the necessary providers for a 'simple' Gravity test environment
 *
 * This really only sets up enough functionality to use the GravityConfigService
 * and create cards.
 *
 * This setup should work without an HTTP provider for the basic functionality,
 * although some of the logic in GravityConfigService makes HTTP requests which
 * will fail if there is no Http service provided in the testbed.
 */
export function simpleGravitySetup(boardConfigs: GravityApiBoard[] = null, extraParameters?: Partial<GravityParameters>): Provider[] {
  const gravityParameters: GravityParameters = Object.assign({
    baseUrl: '',
    baseAppId: 'testapp',
    boardConfigs: keyBy(boardConfigs || [], c => c.id),
  }, extraParameters);

  return [
    { provide: 'USER_PROVIDER', useValue: TEST_USER },
    { provide: GRAVITY_PARAMETERS, useValue: gravityParameters },
    { provide: GravityUrlService, useValue: GravityUrlService.forBaseUrl(gravityParameters.baseUrl) },
    GravityConfigService,
    DataStore,
    GravityConverter
  ];
}


const DEFAULT_MOCK_GRAVITY_OPTIONS: GravityTestbedOptions = {
  baseUrl: '',
  cards: {},
  boards: [],
  useFakeGravityService: true,
  customModelClasses: null
};

export function gravityTestbed(options: Partial<GravityTestbedOptions>) {
  const opts = Object.assign(cloneDeep(DEFAULT_MOCK_GRAVITY_OPTIONS), options);
  const providers = [
    { provide: GRAVITY_TESTBED_OPTIONS_PROVIDER, useValue: opts },
    ...simpleGravitySetup(opts.boards, {
      baseUrl: opts.baseUrl,
      customCardModelClasses: opts.customModelClasses
    }),
    UserRepository,
    DashboardRepository,
    GravityCache,
    Gravity
  ];

  if (opts.useFakeGravityService) {
    providers.push(FakeGravityService);
    providers.push({ provide: GravityService, useExisting: FakeGravityService });
  } else {
    providers.push(GravityService);
  }

  if (opts.injectNullHttpClient) {
    providers.push({ provide: HttpClient, useValue: null });
  }

  return providers;
}


/**
 * Builds on the simpleGravitySetup with more providers.
 *
 * This list of providers can be used when instantiating a TestBed that needs
 * to cover Gravity functionality.
 *
 * You may need to provide additional functionality to actually use these, for
 * example, importing HttpClient or using the HttpClientTestingModule. This
 * leaves things somewhat flexible.
 */
export function mockGravityProviders(baseUrl = '', boardConfigs: GravityApiBoard[] = null): Provider[] {
  return gravityTestbed({
    baseUrl,
    boards: boardConfigs
  });
}


export type CardDataWithPartialMetadata<T = ObjectMap<any>> = Partial<CardData<T>> & {fields: T};


/**
 * Makes mock card API data from partial card definitions.
 *
 * This allows test cases to define a minimal set of fields needed for testing
 * while the rest can get populated with sane defaults in one place, reducing
 * coupling in the test suite.
 *
 * The default phase for the cards will be 'current' but this can be overriden
 * by setting the phase property on the input card.
 *
 * @param board the board to use for all of the cards.
 * @param cards the partial card
 * @returns a list of card JSON results
 */
export function makeCardJson<T>(board: string, cards: CardDataWithPartialMetadata<T>[]): CardData<T>[] {
  return cards.map(c => completeCardDetails(board, c));
}


export function completeCardDetails<T>(board: string, c: CardDataWithPartialMetadata<T>): CardData<T> {
  return {
    id: c.id,
    phase: c.phase || 'current',
    board: c.board || board,
    fields: cloneDeep(c.fields),
    creator: c.creator || TEST_USER.id,
    modifiedby: c.modifiedby || TEST_USER.id,
    created: c.created || new Date().getTime(),
    modified: c.modified || new Date().getTime()
  }
}
