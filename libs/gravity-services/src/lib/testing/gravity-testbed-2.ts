import { Provider } from '@angular/core';

import { GRAVITY_PARAMETERS, GravityParameters } from '../gravity-parameters';

import { GravityTestbed2Options, FakeBoard, GRAVITY_TESTBED_2_OPTIONS_PROVIDER } from './gravity-testbed-options';
import { GravityApiBoard } from '../api-types/gravity-api-board-config';
import { CardData } from '../api-types/gravity-api-cardview';
import { TEST_USER, makeCardJson, CardDataWithPartialMetadata } from './gravity-test-tools';


import { GravityUrlService } from '../services/gravity-url.service';
import { GravityConfigService } from '../core/gravity-config.service';
import { BoardService } from '../services/board.service';
import { FakeBoardService } from './fake-board.service';
import { CardCacheService } from '../services/card-cache.service';

import { BoardConfigBuilder } from './board-config-builder';

import cloneDeep from 'lodash-es/cloneDeep';
import keyBy from 'lodash-es/keyBy';
import { ObjectMap } from '@bmi/utils';


export const DEFAULT_OPTIONS: GravityTestbed2Options = {
  baseUrl: '',
  boards: []
};


/**
 * This is the 'new' Gravity testbed for BMI code.
 *
 * It's similar to the existing IDP gravity testbed, but excludes a lot of the
 * legacy services in favour of code based around the new BoardService.
 *
 * We should refactor/phase out the original Gravity testbed ASAP, or at least
 * do some renaming to give this one a more fitting name
 */
export function gravityTestbed2(options: Partial<GravityTestbed2Options>): Provider[] {
  const opt = {
    ...DEFAULT_OPTIONS,
    ...(options || {}),
  };

  // We don't want any side-effects from tests to influence our input boards, so
  // preemptively cloning this here to stop that. Using an immutability
  // enforcing Proxy might work instead?
  opt.boards = cloneDeep(opt.boards || []);

  const gravityParameters: GravityParameters = {
    baseAppId: 'test-app',
    baseUrl: opt.baseUrl,
    boardConfigs: keyBy(opt.boards.map(b => b.config), c => c.id),
    customCardModelClasses: null
  };

  return [
    {provide: 'USER_PROVIDER', useValue: TEST_USER},
    {provide: GRAVITY_PARAMETERS, useValue: gravityParameters},
    {provide: GRAVITY_TESTBED_2_OPTIONS_PROVIDER, useValue: opt},
    {provide: GravityUrlService, useValue: GravityUrlService.forBaseUrl(opt.baseUrl)},

    GravityConfigService,

    // The board service is the core API for interacting with Gravity
    // boards/cards that our new BMI code should be using. We can provide a fake
    // version here in the testbed
    FakeBoardService,
    {provide: BoardService, useExisting: FakeBoardService},

    CardCacheService,
  ];
}


export function makeBoard<T = ObjectMap<any>>(opts: {config: GravityApiBoard | BoardConfigBuilder, cards: CardDataWithPartialMetadata<T>[]}): FakeBoard {
  const conf = opts.config instanceof BoardConfigBuilder ? opts.config.finish() : opts.config;
  return {
    config: conf,
    cards: makeCardJson(conf.id, opts.cards)
  };
}

function mkCard<T extends {name: string}>(fields: T): Partial<CardData<T>> {
  return {id: fields.name.toLowerCase(), title: fields.name, fields};
}
