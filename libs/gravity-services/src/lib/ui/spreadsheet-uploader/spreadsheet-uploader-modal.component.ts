import { Component, Input, ViewChild } from '@angular/core';
import { of } from 'rxjs';
import { Modal } from '../upload-status/model';
import { ModalComponent } from '@bmi/ui';

/**
 * Shell Component that wraps the SpreadsheetUploaderComponent
 * alongwith the capability to show it in a Modal.
 * This satisfies uses cases where the SpreadsheetUploaderComponent can be shown
 * directly on the screen (BMI App) as well as in a popup (Gravity Admin app)
 */
@Component({
  selector: 'gravity-spreadsheet-modal-uploader',
  template: `
  <ui-modal>
      <gravity-spreadsheet-uploader
        [boardId]="boardId"
        [filterConditions]="filterConditions"
        [stepIndex]="stepIndex"
      ></gravity-spreadsheet-uploader>
  </ui-modal>
  `,
  styleUrls: ['./spreadsheet-uploader.component.scss']
})
export class SpreadsheetUploaderModalComponent implements Modal {

  @ViewChild(ModalComponent, { static: true }) modalComponent: ModalComponent;
  @Input() boardId: string;
  @Input() filterConditions: any;
  @Input() stepIndex: number = 1;

  constructor() {}

  show() {
    this.modalComponent.show();
    return of([]);
  }

  onModalClosed() {
    this.modalComponent.close();
  }


}
