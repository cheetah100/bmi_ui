import { Injectable, ComponentFactoryResolver, Injector } from '@angular/core';
import { ModalService } from '@bmi/ui';
import { SpreadsheetUploaderModalComponent } from './spreadsheet-uploader-modal.component';

@Injectable({
  providedIn: 'root'
})
export class SpreadsheetUploadModalService {

  constructor(
    private modalService: ModalService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private injector: Injector
  ) { }

  showPopup(boardId: string) {
    const modalRef = this.modalService.createModal(SpreadsheetUploaderModalComponent, this.componentFactoryResolver, this.injector);
    const modal = modalRef.instance;
    modal.boardId = boardId;
    modal.show();
  }
}
