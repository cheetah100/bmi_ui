import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { GravityFileUploaderService, TransmissionType } from '../../services/gravity-spreadsheet-uploader.service';
import { timer } from 'rxjs';
import { concatMap, takeWhile } from 'rxjs/operators';

@Component({
  selector: 'gravity-spreadsheet-uploader',
  templateUrl: './spreadsheet-uploader.component.html',
  styleUrls: ['./spreadsheet-uploader.component.scss']
})
export class SpreadsheetUploaderComponent implements OnInit {

  @Input() boardId: string;
  @Input() filterConditions: any;
  @Input() stepIndex: number = 1;

  constructor(
    private fileUploaderService: GravityFileUploaderService
  ) {}

  fileToUpload: File = null;
  fileUploading = false;
  fileDownloading = false;
  enable = false;
  statusMessage = undefined;

  ngOnInit() {
  }

  loadFile(file: FileList) {
    this.fileToUpload = file.item(0);
    this.fileUploaderService.uploadSpreadsheet(this.fileToUpload)
    .subscribe(response => {
      this.updateStatusMessage('default', `Upload Request submitted, click 'Upload Status' for updates`);
      /*
      Commenting out polling mechanism, as the upload status would be tracked by the Upload Status List
      this.fileUploading = true;
      this.pollApi('UPLOAD').subscribe((pollingResponse: any) => {
        this.updateStatusMessage('default', 'Waiting for file upload to complete...');
        if (pollingResponse.status === 'READY') {
          this.updateStatusMessage('success', pollingResponse.messages[0]);
          this.fileUploading = false;
        }
      }); */
    }, error => {
      this.updateStatusMessage('error', 'Error in uploading the file');
      this.fileUploading = false;
    });
  }

  downloadTemplate(boardId: string) {
    if (boardId) {
      this.fileDownloading = true;
      this.updateStatusMessage('default', 'Initiated process to download file');
      /**
       * Triggers API that starts a download job
       */
      this.fileUploaderService.initiateDownloadJob(boardId, this.filterConditions)
      .subscribe(response => {
        /**
         * Poll Download Status API which returns status as READY
         * after file to be downloaded is ready
         */
        this.pollApi('DOWNLOAD').subscribe((pollingResponse: any) => {
          this.updateStatusMessage('default', 'Waiting to download file ...');
          if (pollingResponse.status === 'READY') {
            this.fileDownloading = false;
            const link = document.createElement('a');
            /**
             * Call API that explicitly downloads the file
             */
            link.setAttribute('href', `/gravity/spring/export/${boardId}/xlsx/download`);
            link.setAttribute('style', 'display: none');
            link.setAttribute('target', '_blank');
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
            this.updateStatusMessage('success', 'File has been downloaded. Kindly update spreadsheet and upload for changes to be reflected.');
          }
        }, error => {
          this.updateStatusMessage('error', 'Error in downloading file');
          console.error(error);
        });
      });
    } else {
      this.updateStatusMessage('error', 'Board missing');
    }

  }

  pollApi(transmissionType: TransmissionType) {
    return timer(0, 3000).pipe(
      takeWhile(val => (this.fileUploading || this.fileDownloading)),
      concatMap(() => this.fileUploaderService.getPollingStatus(transmissionType))
    );
  }

  updateStatusMessage(statusType: string, statusMessage: string) {
    this.statusMessage = {
      type: statusType,
      description: statusMessage
    };
  }
}
