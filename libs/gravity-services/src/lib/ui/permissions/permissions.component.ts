import { Component, Input, Output, SimpleChanges, forwardRef, EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { FormArray, FormGroup, FormControl, NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

import { BehaviorSubject, ReplaySubject, Subscription } from 'rxjs';
import { distinctUntilChanged, filter, map, tap } from 'rxjs/operators';

import { Permissions, Permission, isValidPermission } from '../../core/permissions';

interface PermissionDescription {
  id: string,
  permission: Permission
}

import {
  BaseControlValueAccessor,
  clearFormArray,
  Option,
  OptionGroup,
  provideAsControlValueAccessor,
  TemplatedFormArray, TypedFormControl
} from '@bmi/ui';
import { sortBy } from '@bmi/utils';

import { isEqual } from 'lodash-es';

import { Team, User } from '../../core/user.repository';
import { GravityCache } from '../../core/gravity-cache';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gravity-permissions-editor',
  templateUrl: 'permissions.component.html',
  styleUrls: ['permissions.component.scss'],
  providers: [
    provideAsControlValueAccessor(forwardRef(() => PermissionsComponent))
  ]
})
export class PermissionsComponent extends BaseControlValueAccessor<Permissions> implements OnInit, OnDestroy {
  @Input() allowRemoveAdmin:boolean = true;

  permissions: Permissions;

  optionsData:OptionGroup[] = [];

  optionGroups: BehaviorSubject<OptionGroup[]> = new BehaviorSubject<OptionGroup[]>(null);

  teamsSet = new Set<Team>();

  teamsMap = new Map<string, Team>();

  // These are users/teams that are built-in to Gravity and will probably come
  // back if you try to remove them.
  systemPrincipals = new Set<string>([
    'system', 'admin', 'administrators'
  ]);

  readonly permissionsForm = new TemplatedFormArray<PermissionDescription>(() => new TypedFormControl(null));
  principalToAdd: string = null;

  private subscriptions: Subscription[] = [];

  constructor(
    private gravityCache: GravityCache
  ) {
    super();
  }

  ngOnInit() {
    this.subscriptions.push(
      this.gravityCache.getAll<Team>('teams').subscribe(teams => {
        // const options = teams.map(t => {
        //   return <Option>{value: t.id, text: `${t.title} (${t.id})`}
        // });
        const optionTeamsGroup = {
          title: 'Teams',
          options: [],
          collapsible: true
        }
        this.optionsData.push(optionTeamsGroup);
        teams.forEach(t => this.teamsMap.set(t.id, t));
        this.optionGroups.next(this.optionsData);
      }),

      this.permissionsForm.valueChanges.pipe(
        filter(() => !this.isWriteValueInProgress)
      ).subscribe( permissionDescriptions => {
        const permissions = this.createPermissionsFromFormValue(permissionDescriptions);
        this.onChanged(permissions)
      }),
      this.optionGroups.pipe(
        map(optionGroups => {return optionGroups ? optionGroups.map(og => {
            const newOptions = [];
            // Add the teams which do not have permissions to the options list
            for (const team of this.teamsMap.values()) {
              const listedPermission = this.permissionsForm.value.find(po => po.id === team.id);
              if (!listedPermission) {
                newOptions.push(<Option>{value: team.id, text: `${team.title} (${team.id})`});
              }
            }
            og.options = newOptions;
            return og;
          }) : null
        })
      ).subscribe()
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  initForm(permissions: Permissions) {
    clearFormArray(this.permissionsForm);
    if (permissions) {
      for (const [id, permission] of sortBy(Object.entries(permissions), (e) => e[0])) {
        this.addPermissionEntry(id, permission);
      }
    }
  }

  isAdmin(team:Team) {
    const id = team.id || '';
    return id === 'administrators';
  }

  private createPermissionsFromFormValue(formValue: PermissionDescription[]): Permissions {
    const result: Permissions = { };
    for (const {id, permission} of formValue) {
      if (isValidPermission(permission)) {
        result[id] = permission;
      }
    }
    return result;
  }

  addPermissionEntry(id: string, permission: Permission) {
    this.permissionsForm.push(new FormGroup({
      id: new FormControl(id),
      permission: new FormControl(permission),
    }));
  }

  getPrincipalType(team:Team) {
    if (this.teamsMap.has(team.id)) {
      return 'team'
    } else if (this.systemPrincipals.has(team.id)) {
      return 'system';
    } else {
      return this.teamsMap.has(team.id) ? 'team' : 'user';
    }
  }

  removeEntry(entry: FormGroup) {
    if ( !this.allowRemoveAdmin && this.isAdmin(entry.value)) {
      return;
    }
    const index = this.permissionsForm.controls.findIndex(c => c === entry);
    if (index >= 0) {
      this.permissionsForm.removeAt(index);
      this.optionGroups.next(this.optionsData);
    }
  }

  addEntryClicked() {
    if (this.principalToAdd) {
      this.addPermissionEntry(this.principalToAdd, 'READ');
      this.principalToAdd = null;
      this.optionGroups.next(this.optionsData);
    }
  }

  updateComponentValue(permissions: Permissions) {
    this.initForm(permissions);
  }
}
