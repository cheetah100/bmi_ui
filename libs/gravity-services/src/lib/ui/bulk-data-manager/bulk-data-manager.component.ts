import { Component, OnInit, ViewChild, ComponentFactoryResolver, OnDestroy, Input } from '@angular/core';
import { ModalComponent } from '@bmi/ui';
import { BulkDataManagerDirective } from './bulk-data-manager.directive';
import { UploadStatusComponent } from '../upload-status/upload-status.component';
import { SpreadsheetUploaderComponent } from '../spreadsheet-uploader/spreadsheet-uploader.component';

@Component({
  selector: 'gravity-bulk-data-manager',
  templateUrl: './bulk-data-manager.component.html',
  styleUrls: ['./bulk-data-manager.component.scss']
})
export class BulkDataManagerComponent implements OnInit, OnDestroy {
  @ViewChild(ModalComponent, { static: true }) private modal: ModalComponent;
  @ViewChild(BulkDataManagerDirective, { static: true }) bulkHost: BulkDataManagerDirective;
  @Input() showBulkManagerButton = false;

  componentRef: any;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver
  ) { }

  ngOnInit() {
  }

  loadBulkDataManager() {
    this.loadComponent(SpreadsheetUploaderComponent);
  }

  loadUploadStatus() {
    this.loadComponent(UploadStatusComponent);
  }

  loadComponent(component) {

    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(component);

    const viewContainerRef = this.bulkHost.viewContainerRef;
    viewContainerRef.clear();

    this.componentRef = viewContainerRef.createComponent(componentFactory);
    this.modal.show();
  }

  ngOnDestroy(): void {
    if (this.componentRef) {
      this.componentRef.destroy();
    }
  }
}
