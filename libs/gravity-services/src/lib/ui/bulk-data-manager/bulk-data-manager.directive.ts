import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[bulk-host]'
})
export class BulkDataManagerDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
