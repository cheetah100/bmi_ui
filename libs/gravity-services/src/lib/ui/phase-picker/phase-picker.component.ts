import {  Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subscription, combineLatest } from 'rxjs';
import { Option } from '@bmi/utils';
import { ModalContext, IdpValidators } from '@bmi/ui';
import { BoardService } from '../../../lib/services/board.service';
import { map } from 'rxjs/operators';

@Component({
  templateUrl: './phase-picker.component.html',
  styleUrls: ['./phase-picker.component.scss'],
})
export class PhasePickerComponent implements OnInit, OnDestroy {

  constructor(
    private modalContext: ModalContext,
    private boardService: BoardService,
  ) {}

  boardId:string;
  cardId: string;
  isLoading = false;

  phase = new FormControl(undefined, {
    validators: [
      IdpValidators.required,
      IdpValidators.mustBeNonBlank(),
      IdpValidators.satisfies(phase => !!this.phaseOptions?.find(p => p.value === phase)),
    ]
  });

  phaseOptions: Option[] = []

  private subscription = new Subscription();

  ngOnInit() {
    // Create the options from the specified phases
    const modalData = this.modalContext.data;
    this.isLoading = true;
    if (modalData?.boardId && modalData?.cardId) {
      this.boardId = modalData.boardId;
      this.cardId = modalData.cardId;
      this.subscription.add(
        combineLatest([
          this.boardService.fetchById(this.boardId, this.cardId),
          this.boardService.getConfig(this.boardId),
        ]).subscribe(([card, config]) => {
          // Get list of possible phases for the specified board
            this.phaseOptions = Object.keys(config.phases).map(phase => <Option>{
              value: phase, text: phase
            });
            this.isLoading = false;

          // Set the value for current phase if specified
          this.phase.setValue(card.phase);
        })
      );
    } else {
      console.error(`PhasePickerComponent: boardId or cardId not specified`);
    }

    this.modalContext.updateOptions({
      actions: [
        {
          action: 'save',
          buttonText: 'Change Phase',
          buttonStyle: 'primary',
          invoke: () => this.saveClicked(),
          isEnabled: this.phase.statusChanges.pipe(map(status => status === 'VALID'))
        },
        {action: 'cancel', buttonText: 'Cancel', buttonStyle: 'ghost'}
      ]
    })
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  saveClicked() {
    return {
        action: 'save',
        data: this.phase.value
    };
  }
}
