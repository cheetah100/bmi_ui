import { DebugElement } from '@angular/core';

/**
 * A helper for creating an event.
 *
 * This is roughly equivalent to new Event(eventName) with better browser compat
 * (e.g. PhantomJS). This code is from the Angular guide source.
 */
export function newEvent(eventName: string, bubbles = false, cancelable = false) {
  const evt = document.createEvent('CustomEvent');  // MUST be 'CustomEvent'
  evt.initCustomEvent(eventName, bubbles, cancelable, null);
  return evt;
}

/**
 * A quick little helper to extract a native element from a debugElement.
 */
export function getDOMElement<T extends HTMLElement>(element: DebugElement | T): T {
  return element instanceof DebugElement ? element.nativeElement as T : element;
}

/**
 * Sets the value of an input element and triggers some DOM events.
 *
 * Merely updating the DOM element directly won't trigger Angular's bindings
 * so we need to make sure the right events get run.
 */
export function setInputText(element: DebugElement | HTMLInputElement, text: string) {
  const inputElement = getDOMElement(element);
  inputElement.value = text;

  // The input event should be fired whenever text is updated in the value, and
  // this will trigger the form bindings to update. Without this, Angular won't
  // see the input.
  inputElement.dispatchEvent(newEvent('input'));
  inputElement.dispatchEvent(newEvent('change'));
}

export function clickElement(element: DebugElement | HTMLElement) {
  getDOMElement(element).click();
}
