import { Injectable, ComponentFactoryResolver, Injector } from '@angular/core';
import { ModalService } from '@bmi/ui';
import { UploadStatusComponent } from './upload-status.component';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UploadStatusModalService {

  constructor(
    private modalService: ModalService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private injector: Injector
  ) { }

  showPopup() {
    const modalRef = this.modalService.createModal(UploadStatusComponent, this.componentFactoryResolver, this.injector);
    const modal = modalRef.instance;
    modal.show();
  }
}
