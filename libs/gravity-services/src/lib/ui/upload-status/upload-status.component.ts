import { Component, OnInit, ViewChild, EventEmitter } from '@angular/core';
import { UploadStatusService } from './upload-status.service';
import { GravityUploadStatusService } from '../../services/gravity-upload-status.service';
import { TableModel, TableColumn, SimpleTableModel, Formatters } from '@bmi/ui';
import { UploadStatus, SheetUploadStatus, WorkbookUploadStatus, SheetUploadStatusRow } from './upload-status.model';
import { Modal } from './model';
import { ModalComponent } from '@bmi/ui';
import { of } from 'rxjs';

@Component({
  selector: 'gravity-upload-status',
  templateUrl: './upload-status.component.html',
  styleUrls: ['./upload-status.component.scss']
})
export class UploadStatusComponent implements Modal {

  @ViewChild(ModalComponent, { static: true }) private modalComponent: ModalComponent;
  public uploadStatusTable: TableModel;
  public loading = true;
  modalClosed = new EventEmitter<void>();

  constructor(
    private statusService: UploadStatusService,
    private gravityUploadStatusService: GravityUploadStatusService
  ) { }

  ngOnInit() {
    this.buildDataTable();
  }

  buildDataTable() {
    this.uploadStatusTable = new SimpleTableModel([
      new TableColumn('Upload Time', false, '1fr', Formatters.date, -1),
      new TableColumn('Workbook Name', true),
      new TableColumn('Sheet Id'),
      new TableColumn('Sheet Name'),
      new TableColumn('Upload Status'),
      new TableColumn('Message', false, '2fr')
    ]);
    this.statusService.getSheetStatusData().subscribe((sheetUploadStatuses: SheetUploadStatusRow[]) => {
      this.loading = false;
      sheetUploadStatuses.forEach((sheetStatus: SheetUploadStatusRow) => {
        const row = [];
        Object.keys(sheetStatus).forEach(key => {
          if (key === 'status') {
            row.push(this.transformCellForTypeColor(sheetStatus[key]));
          } else if(key === 'message') {
            row.push(this.transformCellForTypeAction(sheetStatus['status'], sheetStatus[key]));
          } else {
            if (key !== 'fileId') {
              row.push(sheetStatus[key]);
            }
          }
        });
        this.uploadStatusTable.addRow(row);
        this.uploadStatusTable.update();
      });
    }, error => {
      this.loading = false;
      console.error(error);
    });
  }

  handleTableAction(event) {
    if (event && event.row && event.row.rawData && event.row.rawData.length > 2) {
      const workbookId = event.row.rawData[0];
      const sheetId = event.row.rawData[2];
      this.gravityUploadStatusService.downloadFailedUploadErrorLog(workbookId, sheetId).subscribe((errorLog: any[]) => {
        /**
         * TODO:
         * The below code shows an alert box containing the error log on top of a popup which is terrible UX.
         * This is put in as a temporary log display until a more efficient download API is developed which returns a file.
         */
        let logString: string = `Error Log for failed records \n\n`;
        errorLog.forEach(log => logString += `${log.errorCard.id}: ${log.errorMessage} \n`);
        alert(logString);
      });
    }
  }

  /**
   * Based on type of status, show corresponding icon and color
   * @param cellValue: Display value
   */
  transformCellForTypeColor(cellValue: any) {
    const cellObject = {
      type: 'color',
      title: cellValue,
    };
    switch (cellValue) {
      case 'SUCCESSFUL':
        return { ...cellObject, color: 'status-green', icon: 'check'};
      case 'ERROR':
        return { ...cellObject, color: 'status-red', icon: 'bug'};
      case 'FAILED':
          return { ...cellObject, color: 'status-red', icon: 'bug'};
      case 'INPROGRESS':
        return { ...cellObject, color: 'status-yellow', icon: 'exclamation-circle'};
      case 'PARTIAL':
          return { ...cellObject, color: 'status-yellow', icon: 'exclamation-circle'};
      default:
        return { title: cellValue };
    }
  }

  /**
   * Based on corresponding status of upload, show commentary and download error log button
   * @param statusType
   * @param cellValue
   */
  transformCellForTypeAction(statusType: string, cellValue: any) {
    if (statusType === 'ERROR' || statusType === 'FAILED' || statusType === 'PARTIAL') {
      return {
        type: 'actions',
        buttons: [
          { icon: 'download', action: 'download', details: cellValue + ' Download error log.' },
        ]
      };
    }
    return cellValue;
  }

  show() {
    this.modalComponent.show();
    return of([]);
  }
}
