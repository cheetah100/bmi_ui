import { of, Observable } from 'rxjs';

export const SAMPLE_DATA: WorkbookUploadStatus[] = [
  {
    id: '9a61a011-4083-4d67-b0dc-257e2acd44ac',
    metadata: {
      version: 2,
      creator: 'sidsaman',
      created: 1566330340731,
      modifiedby: 'sidsaman',
      modified: 1566330340819
    },
    totalRows: 3,
    successRows: 3,
    errorRows: 0,
    createRows: 3,
    updateRows: 0,
    message: 'Nicely done! 3 quarters.xlsx were imported successfully.',
    fileName: 'quarters.xlsx',
    sheets: [
      {
        uniqueid: 'quarters_quarters_1',
        id: 'quarters_1',
        boardId: 'quarters',
        name: 'quarters',
        totalRows: 3,
        successRows: 3,
        errorRows: 0,
        createRows: 3,
        updateRows: 0,
        message: 'Nicely done! 3 quarters were imported successfully.',
        status: 'SUCCESSFUL'
      },
      {
        uniqueid: 'quarters_quarters_2',
        id: 'quarters_2',
        boardId: 'quarters',
        name: 'quarters',
        totalRows: 3,
        successRows: 3,
        errorRows: 0,
        createRows: 3,
        updateRows: 0,
        message: 'Nicely done! 3 quarters were imported successfully.',
        status: 'SUCCESSFUL'
      }
    ],
    status: 'SUCCESSFUL'
  },
  {
    id: '9a61a011-4083-4d67-b0dc-257e2acd44ad',
    metadata: {
      version: 2,
      creator: 'sidsaman',
      created: 1566330340731,
      modifiedby: 'sidsaman',
      modified: 1566330340819
    },
    totalRows: 3,
    successRows: 3,
    errorRows: 0,
    createRows: 3,
    updateRows: 0,
    message: 'Nicely done! 3 quarters.xlsx were imported successfully.',
    fileName: 'quarters.xlsx',
    sheets: [
      {
        uniqueid: 'quarters_quarters_1',
        id: 'quarters_1',
        boardId: 'quarters',
        name: 'quarters',
        totalRows: 3,
        successRows: 2,
        errorRows: 1,
        createRows: 3,
        updateRows: 0,
        message: 'Upload partially succeeded. 2 records uploaded successfully while 1 failed.',
        status: 'ERROR'
      },
      {
        uniqueid: 'quarters_quarters_2',
        id: 'quarters_2',
        boardId: 'quarters',
        name: 'quarters',
        totalRows: 3,
        successRows: 3,
        errorRows: 0,
        createRows: 3,
        updateRows: 0,
        message: 'Upload is currently in progress, kindly check again in a while',
        status: 'INPROGRESS'
      }
    ],
    status: 'ERROR'
  }
];

export const getSampleData = (): Observable<WorkbookUploadStatus[]> => {
  return of([...SAMPLE_DATA]);
};

export interface WorkbookUploadStatus {
  id: string;
  metadata: any;
  totalRows: number;
  successRows: number;
  errorRows: number;
  createRows: number;
  updateRows: number;
  message: string;
  fileName: string;
  sheets: SheetUploadStatus[];
  status: UploadStatus;
}

export interface SheetUploadStatus {
  uniqueid: string;
  id: string;
  boardId: string;
  name: string;
  totalRows: number;
  successRows: number;
  errorRows: number;
  createRows: number;
  updateRows: number;
  message: string;
  status: UploadStatus;
}

export interface SheetUploadStatusRow {
  createdAt: Date;
  fileId: string;
  fileName: string;
  sheetId: string;
  sheetName: string;
  status: UploadStatus;
  message: string;
}

export type UploadStatus = 'READY' | 'INPROGRESS' | 'ERROR' | 'SUCCESSFUL';

/**
 * Mapping between Table Column Titles and corresponding values from backend
 */
export const UploadStatusTableConfig: {[key: string]: string} = {
  fileName: 'Workbook Name',
  sheetId: 'Sheet Id',
  sheetName: 'Sheet Name',
  status: 'Upload Status',
  message: 'Commentary'
};
