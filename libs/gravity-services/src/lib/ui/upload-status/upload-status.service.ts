import { Injectable } from '@angular/core';
import { SAMPLE_DATA, getSampleData } from './upload-status.model';
import { of, Observable } from 'rxjs';
import { map, sample } from 'rxjs/operators';
import { UploadStatus, SheetUploadStatus, WorkbookUploadStatus, SheetUploadStatusRow } from './upload-status.model';
import { GravityUploadStatusService } from '../../services/gravity-upload-status.service';

@Injectable({
  providedIn: 'root'
})
export class UploadStatusService {

  constructor(
    private uploadStatusService: GravityUploadStatusService
  ) { }

  /**
   * Utility that converts workbook level
   * status into sheet level upload status.
   */
  getSheetStatusData(): Observable<SheetUploadStatusRow[]> {
    return this.uploadStatusService.getUploadStatusList().pipe(
      map((sampleData: WorkbookUploadStatus[]) => {
        const sheetData: SheetUploadStatusRow[] = [];
        sampleData.forEach(file => {
          file.sheets.forEach(sheet => {
            sheetData.push({
              createdAt: file.metadata.created,
              fileId: file.id,
              fileName: file.fileName,
              sheetId: sheet.id,
              sheetName: sheet.name,
              status: sheet.status,
              message: sheet.message
            });
          });
        });
        return sheetData;
      })
    );
  }
}
