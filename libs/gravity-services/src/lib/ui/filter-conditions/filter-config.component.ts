import { Component, Input } from '@angular/core';
import { TemplatedFormArray } from '@bmi/ui';
import { GravityApiCondition } from '../../api-types/gravity-api-condition';

@Component({
  selector: 'gravity-filter-config, idp-filter-config-control',
  templateUrl: './filter-config.component.html',
  styleUrls: ['../styles/forms.scss'],
})
export class FilterConfigComponent {

  @Input() filterConditions: TemplatedFormArray<GravityApiCondition>;
  @Input() board: string;

  constructor() { }


  addCondition(fieldName: string = null, dynamic: boolean = false): void {
    const newCondition: GravityApiCondition = {
      fieldName,
      operation: 'EQUALTO',
      value: null,
      conditionType: dynamic ? 'DYNAMIC' : null,
    };
    this.filterConditions.pushValue(newCondition);
  }

  addDynamicCondition() {
    this.addCondition(null, true);
  }

  addPhaseCondition() {
    this.addCondition('phase');
  }
}


