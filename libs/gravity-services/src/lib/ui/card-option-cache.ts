import { Gravity } from '../gravity';
import { Card } from '../core/card';
import { BoardPathResolver } from '../field-path/board-field-path';
import { PathResolver } from '../field-path/path-resolver';
import { Option } from '@bmi/utils';

/**
 * Caches a list of Option objects for paths on a board.
 *
 * This handles phases as a special case, looking up the phase names from the
 * board config.
 */
export class CardOptionCache {
  public board: string;
  private pathResolver: PathResolver;
  private valueOptions: Map<string, Option[]> = new Map();

  constructor(private gravity: Gravity) { }

  setBoard(boardId: string) {
    this.setResolver(new BoardPathResolver(boardId, this.gravity.configService))
  }

  setResolver(resolver: PathResolver) {
    if (resolver !== this.pathResolver) {
      this.pathResolver = resolver;
      this.clear();
    }
  }

  clear() {
    this.valueOptions.clear();
  }

  getOptions(fieldName: string): Option[] {
    return this.valueOptions.get(fieldName);
  }

  fetchOptions(fieldName: string) {
    if (!this.pathResolver || this.valueOptions.has(fieldName)) {
      return;
    }

    const path = (fieldName || '').replace(/\.$/, '').trim();
    if (!path) {
      return;
    }

    const fieldPath = this.pathResolver.resolve(path);
    if (!fieldPath || !fieldPath.isValid) {
      return;
    }

    if (fieldPath.fieldName === 'phase' && fieldPath.boardId) {
      const boardConfig = this.gravity.configService.getBoardConfig(fieldPath.boardId);
      this.valueOptions.set(
        fieldPath.toString(),
        Object.values(boardConfig.phases).map(phase => {
          return {
            text: phase.name,
            value: phase.id
          };
        }));
    } else if (fieldPath.isReferenceField) {
      this.gravity.cache.getAll(fieldPath.targetBoard).subscribe(cards => {
        this.valueOptions.set(fieldPath.toString(), cards.map((c: Card) => {
          return {
            text: c.toString(),
            value: c.id,
          };
        }));
      });
    }
  }
}
