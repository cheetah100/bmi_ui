import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Gravity } from '../../gravity';
import { Condition } from '../../model/condition';
import { OperationInfo } from '../../model/condition-operations';
import { CardOptionCache } from '../card-option-cache';
import { getFormValue, Option } from '@bmi/ui';

@Component({
  selector: 'gravity-condition-list-row-phase',
  template: `
    <ng-container [formGroup]="formGroup">
      <ui-form-field label="Field">
        <div class="readonly-field">Phase</div>
      </ui-form-field>

      <ui-form-field label="Operation">
        <select [formControlName]="'operation'">
          <option
            *ngFor="let operation of OPERATIONS"
            value="{{ operation.value }}">
            <!--(change)="changeOperation(i, operation)"-->
            {{ operation.name }}
          </option>
        </select>
      </ui-form-field>

      <ui-form-field *ngIf="requiresValue()" label="Value">
        <ui-fallback-multi-select formControlName="value" [options]="getOptions()"></ui-fallback-multi-select>
      </ui-form-field>
    </ng-container>
  `,
  styleUrls: [
    '../styles/forms.scss',
    './condition-row.scss',
  ],
})
export class ConditionListRowPhaseComponent implements OnInit, OnChanges {
  @Input() formGroup: FormGroup;
  @Input() board: string;

  OPERATIONS: OperationInfo[] = Condition.getValidOperations('PHASE');

  private cardOptionCache = new CardOptionCache(this.gravity);

  get selectedField(): string {
    return 'phase';
  }

  get selectedOperation(): string {
    return getFormValue<string>(this.formGroup, 'operation');
  }

  constructor(private gravity: Gravity) { }

  ngOnInit() {
    this.cardOptionCache.setBoard(this.board);
    this.updateOptions();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['board']) {
      this.cardOptionCache.setBoard(this.board);
      this.updateOptions();
    }
  }

  requiresValue(): boolean {
    const selectedOperation = this.selectedOperation;
    const matchedOp = this.OPERATIONS.find(o => o.value === selectedOperation);
    return !matchedOp || matchedOp.requiresValue;
  }

  getOptions(): Option[] {
    return this.requiresValue ? this.cardOptionCache.getOptions(this.selectedField) : null;
  }

  updateOptions() {
    this.cardOptionCache.fetchOptions(this.selectedField);
  }

}
