import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FieldPath } from '../../field-path/field-path';

@Component({
  selector: 'gravity-condition-list-row-dynamic',
  template: `
    <ng-container [formGroup]="formGroup">
      <ui-form-field label="Field">
        <gravity-path-picker
          formControlName="fieldName"
          [board]="board"
          (pathPicked)="pathPicked($event)">
        </gravity-path-picker>
      </ui-form-field>

      <ui-form-field label="Condition Type">
        <div class="readonly-field">DYNAMIC</div>
      </ui-form-field>

      <ui-form-field label="Value">
        <div class="readonly-field">{{ formGroup?.get('value')?.value }}</div>
      </ui-form-field>
    </ng-container>
  `,
  styleUrls: [
    '../styles/forms.scss',
    './condition-row.scss',
  ],
})
export class ConditionListRowDynamicComponent {
  @Input() formGroup: FormGroup;
  @Input() board: string;


  pathPicked(fieldPath: FieldPath) {
    this.formGroup.get('value').setValue(`$board=${fieldPath.targetBoard}`);
  }
}
