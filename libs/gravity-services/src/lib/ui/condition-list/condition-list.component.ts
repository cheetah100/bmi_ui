import { Component, Input } from '@angular/core';
import { TemplatedFormArray } from '@bmi/ui';
import { Gravity } from '../../gravity';
import { GravityApiCondition } from '../../api-types/gravity-api-condition';

@Component({
  selector: 'gravity-condition-list',
  templateUrl: './condition-list.component.html',
  styleUrls: [
    '../styles/forms.scss',
    './condition-list.component.scss',
  ],
})
export class ConditionListComponent {

  @Input() conditions: TemplatedFormArray<GravityApiCondition>;
  @Input() board: string;

  constructor(private gravity: Gravity) { }
}


