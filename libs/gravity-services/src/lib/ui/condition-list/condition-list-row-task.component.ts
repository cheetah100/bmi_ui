import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { catchError } from 'rxjs/operators';
import { Condition } from '../../model/condition';
import { RulesService, RuleSummary } from '../../services/rules.service';
import { Option } from '@bmi/utils';
import { getFormValue } from '@bmi/ui';
import { of } from 'rxjs';
import { OperationInfo } from '../../model/condition-operations';

@Component({
  selector: 'gravity-condition-list-row-task',
  template: `
    <ng-container [formGroup]="formGroup">
      <ui-form-field label="Condition Type">
        <div class="readonly-field">Task</div>
      </ui-form-field>

      <ui-form-field label="Operation">
        <select [formControlName]="'operation'">
          <option
            *ngFor="let operation of OPERATIONS"
            value="{{ operation.value }}">
            <!--(change)="changeOperation(i, operation)"-->
            {{ operation.name }}
          </option>
        </select>
      </ui-form-field>

      <ui-form-field *ngIf="requiresValue() && taskOptions?.length > 0" label="Value">
        <ui-single-select [floating]="true" formControlName="fieldName" [options]="taskOptions"></ui-single-select>
      </ui-form-field>
    </ng-container>
  `,
  styleUrls: [
    '../styles/forms.scss',
    './condition-row.scss',
  ],
})
export class ConditionListRowTaskComponent implements OnInit, OnChanges {
  @Input() formGroup: FormGroup;
  @Input() board: string;

  OPERATIONS: OperationInfo[] = Condition.getValidOperations('TASK');

  taskOptions: Option[] = [];

  get selectedOperation(): string {
    return getFormValue<string>(this.formGroup, 'operation');
  }

  constructor(private rulesService: RulesService) { }

  ngOnInit() {
    this.updateOptions();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['board']) {
      this.updateOptions();
    }
  }

  requiresValue(): boolean {
    const selectedOperation = this.selectedOperation;
    const matchedOp = this.OPERATIONS.find(o => o.value === selectedOperation);
    return !matchedOp || matchedOp.requiresValue;
  }

  updateOptions() {
    if (this.board) {
      this.rulesService.getRuleSummaries(this.board).pipe(
        catchError(err => of<RuleSummary[]>([])),
      ).subscribe(ruleSummary => {
        this.taskOptions = ruleSummary.map(rs => {
          return {
            value: rs.id,
            text: rs.name
          };
        });
      });
    } else {
      this.taskOptions = [];
    }
  }

}
