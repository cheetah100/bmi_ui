import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Gravity } from '../../gravity';
import { Condition } from '../../model/condition';
import { CardOptionCache } from '../card-option-cache';
import { Option } from '@bmi/utils';
import { getFormValue } from '@bmi/ui';
import { OperationInfo } from '../../model/condition-operations';

@Component({
  selector: 'gravity-condition-list-row-property',
  template: `
    <ng-container [formGroup]="formGroup">
      <ui-form-field label="Field">
        <gravity-path-picker
          formControlName="fieldName"
          [board]="board"
          (pathPicked)="pathPicked()">
        </gravity-path-picker>
      </ui-form-field>

      <ui-form-field label="Operation">
        <select [formControlName]="'operation'">
          <option
            *ngFor="let operation of OPERATIONS"
            value="{{ operation.value }}">
            <!--(change)="changeOperation(i, operation)"-->
            {{ operation.name }}
          </option>
        </select>
      </ui-form-field>

      <ui-form-field *ngIf="requiresValue()" label="Value">
        <ui-fallback-multi-select formControlName="value" [options]="getOptions()"></ui-fallback-multi-select>
      </ui-form-field>
    </ng-container>
  `,
  styleUrls: [
    '../styles/forms.scss',
    './condition-row.scss',
  ],
})
export class ConditionListRowPropertyComponent implements OnInit, OnChanges {
  @Input() formGroup: FormGroup;
  @Input() board: string;


  OPERATIONS: OperationInfo[] = Condition.getValidOperations('PROPERTY');

  private cardOptionCache = new CardOptionCache(this.gravity);

  get selectedField(): string {
    return getFormValue<string>(this.formGroup, 'fieldName');
  }

  get selectedOperation(): string {
    return getFormValue<string>(this.formGroup, 'operation');
  }

  constructor(private gravity: Gravity) { }

  ngOnInit() {
    this.cardOptionCache.setBoard(this.board);
    this.updateOptions();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['board']) {
      this.cardOptionCache.setBoard(this.board);
      this.updateOptions();
    }
  }

  requiresValue(): boolean {
    const selectedOperation = this.selectedOperation;
    const matchedOp = this.OPERATIONS.find(o => o.value === selectedOperation);
    return !matchedOp || matchedOp.requiresValue;
  }

  getOptions(): Option[] {
    return this.requiresValue ? this.cardOptionCache.getOptions(this.selectedField) : null;
  }

  updateOptions() {
    this.cardOptionCache.fetchOptions(this.selectedField);
  }

  pathPicked() {
    this.updateOptions();
  }
}
