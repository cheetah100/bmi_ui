import { NO_ERRORS_SCHEMA, ChangeDetectionStrategy } from '@angular/core';
import { ComponentFixture, TestBed, fakeAsync, tick, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';

import { Subject, Observable } from 'rxjs';

import { PathPickerComponent } from './path-picker.component';
import { PathPickerSuggestionsComponent } from './path-picker-suggestions.component';
import { FieldPath } from '../../field-path/field-path';
import { gravityTestbed } from '../../testing/gravity-test-tools';
import { BoardConfigBuilder } from '../../testing/board-config-builder';
import { CLOSE_DIALOG_SERVICE_TOKEN, CloseDialogService, setInputText, clickElement } from '@bmi/ui';


const TEST_BOARDS = [
  new BoardConfigBuilder()
    .id('stoplights')
    .name('Stoplights')
    .addField('name')
    .addField('color')
    .finish(),

  new BoardConfigBuilder()
    .id('test-status')
    .name('Test Status')
    .addField('dev_joy', f => f.optionlist('stoplight'))
    .addField('pm_joy', f => f.optionlist('stoplight'))
    .finish(),

  new BoardConfigBuilder()
    .id('project')
    .name('Project')
    .addField('testing')
    .addField('name')
    .addField('status', f => f.label('Status').optionlist('test-status').required())
    .finish()
];

export class MockCloseDialogService implements CloseDialogService {
  private closeDialogSubject = new Subject<string>();

  get onClose(): Observable<string> {
    return this.closeDialogSubject.asObservable();
  }

  public triggerClose(dialogId: string = 'test') {
    this.closeDialogSubject.next(dialogId);
  }
}

describe('PathPickerComponent', () => {
  let fixture: ComponentFixture<PathPickerComponent>;
  let component: PathPickerComponent;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule],
      declarations: [
        PathPickerComponent,
        PathPickerSuggestionsComponent
      ],

      schemas: [NO_ERRORS_SCHEMA],

      providers: [
        gravityTestbed({
          boards: TEST_BOARDS,
          injectNullHttpClient: true,
        }),
        MockCloseDialogService,
        { provide: CLOSE_DIALOG_SERVICE_TOKEN, useExisting: MockCloseDialogService }
      ]
    });

    TestBed.overrideComponent(PathPickerComponent, {
      set: {
        changeDetection: ChangeDetectionStrategy.Default
      }
    });

    TestBed.compileComponents();

    fixture = TestBed.createComponent(PathPickerComponent);
    component = fixture.componentInstance;
  }));

  afterEach(() => fixture.destroy());

  it('should support setting the board by id', () => {
    component.board = 'project';
    fixture.detectChanges();
    expect(component.pathResolver).toBeTruthy();
    expect(component.pathResolver.dataSourceName).toBe('Project');
  });

  describe('with a blank path set', () => {
    beforeEach(() => {
      component.board = 'project';
      fixture.detectChanges();
    });

    it('should give suggestions for the root board', () => {
      expect(component.hasCompletions).toBeTruthy();
      const completions = component.completions;
      expect(completions.every(p => p.boardId === 'project')).toBeTruthy();
      expect(completions.find(p => p.fieldName === 'name')).toBeTruthy();
    });

    it('should include metadata fields', () => {
      expect(component.completions.some(p => p.isMetadata)).toBeTruthy();

      const fieldNames = component.completions.map(p => p.fieldName);

      expect(fieldNames).toContain('id');
      expect(fieldNames).toContain('created');
      expect(fieldNames).toContain('phase');
    });

    it('should open when clicked', waitForAsync(() => {
      const pathPickerElement = fixture.debugElement.query(By.css('.form-input'));
      clickElement(pathPickerElement);
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(component.editMode).toBeTruthy();
      });
    }));


    describe(`'use this field' item`, () => {
      function findUseThisFieldElement() {
        return fixture.debugElement.query(By.css('.use-this-card'));
      }

      function editPath(path: string) {
        component.writeValue(path);
        component.editAsPath();
        component.startEditing();
        fixture.detectChanges();
        return fixture.whenStable();
      }

      it('should not display the use this card element when there is no path', waitForAsync(() => {
        editPath('').then(() => {
          expect(findUseThisFieldElement()).toBeFalsy();
        });
      }));

      it('should display when a field is chosen', waitForAsync(() => {
        editPath('status').then(() => {
          expect(findUseThisFieldElement()).toBeTruthy();
        });
      }));

      it('should close the path picker when clicked', waitForAsync(() => {
        editPath('status').then(() => {
          const element = findUseThisFieldElement();
          clickElement(element);
          expect(component.editMode).toBeFalsy();
          expect(component.fieldPath.path).toBe('status');
        });
      }));
    });

    describe('When a completion is clicked', () => {
      let completions: FieldPath[];

      function clickOnSuggestion(index: number) {
        const suggestionElements = fixture.debugElement.queryAll(By.css('.suggestions > li'));
        clickElement(suggestionElements[index]);
        fixture.detectChanges();
      }

      beforeEach(() => {
        component.editAsPath();
        component.startEditing();
        fixture.detectChanges();

        completions = component.completions;
      });

      it('should set the path', waitForAsync(() => {
        clickOnSuggestion(0);
        fixture.whenStable().then(() => {
          expect(component.path).toBe(completions[0].path);
        });
      }));

      it('should stay in edit mode if there are more completions', waitForAsync(() => {
        clickOnSuggestion(0);
        fixture.whenStable().then(() => {
          expect(component.editMode).toBe(true);
        });
      }));

      it('should close the editor if there are no further completions', waitForAsync(() => {
        const firstRegularFieldIndex = completions.findIndex(p => !p.isReferenceField);
        expect(firstRegularFieldIndex >= 0).toBeTruthy();
        clickOnSuggestion(firstRegularFieldIndex);

        fixture.whenStable().then(() => {
          expect(component.editMode).toBe(false);
        });
      }));
    });

    describe('Completion ordering', () => {
      let fields: string[];
      const fieldIndex = (name: string) => fields.findIndex(f => f === name);

      beforeEach(() => {
        fields = component.completions.map(p => p.fieldName);
      });

      it('should put reference fields before regular fields', () => {
        expect(fieldIndex('status') < fieldIndex('name')).toBeTruthy();
      });

      it('should put regular fields before metadata fields', () => {
        expect(fieldIndex('name') < fieldIndex('id')).toBeTruthy();
        expect(fieldIndex('name') < fieldIndex('phase')).toBeTruthy();
      });

      it('should order fields of the same type alphabetically', () => {
        expect(fieldIndex('name') < fieldIndex('testing')).toBeTruthy();
        expect(fieldIndex('id') < fieldIndex('phase')).toBeTruthy();
        expect(fieldIndex('created') < fieldIndex('modified')).toBeTruthy();
      });
    });

    describe('Manual path editing', () => {

      beforeEach(waitForAsync(() => {
        component.editAsText();
        component.startEditing();
        fixture.detectChanges();
      }));

      function enterPathText(path: string) {
        const textInput = fixture.debugElement.query(By.css('.path-text-input'));
        setInputText(textInput, path);
        fixture.detectChanges();
        return fixture.whenStable();
      }

      it('should update the path when a valid path is entered', waitForAsync(() => {
        enterPathText('status.dev_joy').then(() => {
          expect(component.path).toBe('status.dev_joy');
          expect(component.fieldPath).toBeTruthy();
          expect(component.fieldPath.path).toBe('status.dev_joy');
        });
      }));

      it('should resolve an invalid path if an invalid path is entered', waitForAsync(() => {
        enterPathText('foobar').then(() => {
          expect(component.path).toBe('foobar');
          expect(component.fieldPath).toBeTruthy();
          expect(component.fieldPath.isValid).toBeFalsy();
        });
      }));

      it('should clear the fieldpath if a blank path is entered', waitForAsync(() => {
        enterPathText('foobar').then(() => enterPathText('')).then(() => {
          expect(component.path).toBe('');
          expect(component.fieldPath).toBeFalsy();
        });
      }));

      it('should return null for a garbage path', waitForAsync(() => {
        enterPathText('.&?z ==/').then(() => {
          expect(component.fieldPath).toBeFalsy();
        });
      }));

      it('should remain in manual editing mode if the control value is updated', waitForAsync(() => {
        enterPathText('foobar').then(() => {
          component.writeValue('foobar');
          fixture.detectChanges();
          return fixture.whenStable();
        }).then(() => {
          expect(component.mode).toBe('text');
        })
      }));

      it('should return null for a single . (i.e. the segment must have a word character)', waitForAsync(() => {
        enterPathText('.').then(() => {
          expect(component.fieldPath).toBeFalsy();
        });
      }));
    });
  });


  describe('Close dialog interface', () => {
    let dialogService: MockCloseDialogService;
    beforeEach(() => {
      dialogService = TestBed.get(MockCloseDialogService) as MockCloseDialogService;
      component.board = 'stoplights';
      fixture.detectChanges();
    });

    it('should do nothing if the path picker is closed', () => {
      dialogService.triggerClose();
      expect(component.editMode).toBeFalsy();
    });

    it('should close the path picker if it is open', fakeAsync(() => {
      component.editAsPath();
      component.startEditing();
      tick(1000);
      dialogService.triggerClose();
      expect(component.editMode).toBeFalsy();
    }));

    it('should not close the picker if it has just opened', fakeAsync(() => {
      component.editAsPath();
      component.startEditing();
      tick(5);
      dialogService.triggerClose();
      tick(1000);
      expect(component.editMode).toBeTruthy();
    }));
  });

  describe('Angular form control API', () => {
    beforeEach(() => {
      component.board = 'project';
      fixture.detectChanges();
    });

    it('should update the value when writeValue is called', waitForAsync(() => {
      component.writeValue('status');
      expect(component.path).toBe('status');
      expect(component.fieldPath.path).toBe('status');
    }));
  });
});
