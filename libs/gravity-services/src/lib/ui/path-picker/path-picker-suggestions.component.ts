import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FieldPath } from '../../field-path/field-path';

@Component({
  selector: 'gravity-path-picker-suggestions',
  templateUrl: './path-picker-suggestions.component.html',
  styleUrls: ['./path-picker-suggestions.component.scss']
})
export class PathPickerSuggestionsComponent {
  @Input()
  suggestions: FieldPath[] = [];

  @Output()
  pathSelected = new EventEmitter<FieldPath>();

  selectPath(path: FieldPath) {
    this.pathSelected.emit(path);
  }

  getFieldType(fieldPath: FieldPath): string {
    if (!fieldPath) {
      return '';
    } else {
      let typeString = fieldPath.isReferenceField ? fieldPath.targetBoard : fieldPath.gravityType;
      if (fieldPath.isListField) {
        typeString += '[]';
      }

      if (fieldPath.isMetadata) {
        typeString += ', metadata';
      }

      return typeString;
    }
  }

  getFieldIcon(fieldPath: FieldPath) {
    if (fieldPath.isReferenceField) {
      return 'fa-plus-square-o';
    } else if (fieldPath.isMetadata) {
      return 'fa-circle-thin';
    } else {
      return 'fa-circle';
    }
  }

  trackSuggestion(suggestion: FieldPath) {
    return suggestion && suggestion.path;
  }

}
