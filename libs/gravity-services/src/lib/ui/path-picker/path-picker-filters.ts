import { FieldPath } from '../../field-path/field-path';

export type PathPickerFilter = ((fieldPath: FieldPath) => boolean);

/**
 * Path filters to use with the PathPicker Component.
 *
 * These filter the paths that will be shown in the suggestionsn.
 */
export class PathPickerFilters {
  /**
   * Only show optionlist fields
   */
  public static optionlist(): PathPickerFilter {
    return (fieldPath: FieldPath): boolean => !!fieldPath.targetBoard;
  }

  public static whitelistFields(whitelist: string[]): PathPickerFilter {
    return path => whitelist.includes(path.fieldName);
  }

  /**
   * Show optionlist fields only, also add in whitelisted non-optionlist fields
   */
  public static optionlistOrWhitelist(whitelist: string[] = []): PathPickerFilter {
    return PathPickerFilters.or(
      PathPickerFilters.optionlist(),
      PathPickerFilters.whitelistFields(whitelist)
    );
  }

  /**
   * 1 will allow only field ids that exist on the template.
   * No dotted values / related fields.
   * Can't think of a reason to use a different number for now, but go crazy
   */
  public static pathLength(length: number = 1): PathPickerFilter {
    return (fieldPath: FieldPath): boolean => fieldPath.segments.length <= length;
  }

  /**
   * Optionlist only, AND choose max path length
   */
  public static optionlistPathLength(length: number = 1): PathPickerFilter {
    return PathPickerFilters.and(
      PathPickerFilters.optionlist(),
      PathPickerFilters.pathLength(length)
    );
  }

  /**
   * Combines several filter functions, requiring all to pass (AND)
   * @param filters
   */
  public static and(...filters: PathPickerFilter[]): PathPickerFilter {
    return path => filters.every(f => f(path));
  }

  /**
   * Combines several filter functions, requiring at least one to pass (OR)
   * @param filters
   */
  public static or(...filters: PathPickerFilter[]): PathPickerFilter {
    return path => filters.some(f => f(path));
  }
}