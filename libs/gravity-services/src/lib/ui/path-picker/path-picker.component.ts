import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit, OnDestroy, OnChanges,
  forwardRef,
  ViewChild,
  ElementRef,
  SimpleChanges,
  ChangeDetectorRef,
  Inject,
  Optional
} from '@angular/core';
import { Subscription } from 'rxjs';
import { Gravity } from '../../gravity';
import { FieldPath } from '../../field-path/field-path';
import { PathResolver } from '../../field-path/path-resolver';
import { BoardPathResolver } from '../../field-path/board-field-path';
import { CloseDialogService, CLOSE_DIALOG_SERVICE_TOKEN } from '@bmi/ui';
import { sortBy } from '@bmi/utils';
import { PathPickerFilter } from './path-picker-filters';


// Paths matching this format should be "pickable". Identifiers separated by
// dots. This will also accept a blank path, as blank is also accepted.
//
// If the path doesn't match this regex, the control will display as a text
// input instead.
const FIELD_PATH_REGEX = /^([\w\-]+\.?)*$/;

@Component({
  templateUrl: './path-picker.component.html',
  styleUrls: ['./path-picker.component.scss'],
  selector: 'gravity-path-picker, idp-path-picker',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PathPickerComponent),
      multi: true
    }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PathPickerComponent implements ControlValueAccessor, OnInit, OnDestroy, OnChanges {

  // In order to pick a path, this component needs to have the root board ID
  // set. This is used to create a `BoardPathResolver` which does the actual
  // work of looking up the paths.
  //
  // A better approach would be to accept a PathResolver object as an input to
  // this component which would allow different implementations to be used, e.g.
  // to look up fields from a transform output.
  @Input() public path = '';

  /**
   * The path resolver implementation to use to find paths.
   */
  // tslint:disable-next-line:no-input-rename
  @Input('resolver') public userDefinedResolver: PathResolver = null;

  /**
   * Optional: a simple way to pick paths off a board.
   *
   * If this input parameter is set, AND the resolver property is null, then the
   * path picker will create an instance of BoardPathResolver to use.
   */
  @Input() public board?: string = null;

  /**
   * Accepts a function that will filter the suggestions shown.
   *
   * This doesn't perform validation, this is simply a way to control which
   * suggestions appear in the list.
   */
  @Input() public filter: PathPickerFilter = null;

  /**
   * This event will be triggered when the USER picks a path
   *
   * This will *NOT* be triggered by programatically updating the value of
   * the form control.
   *
   * If a root path is selected, this will emit a null value.
   */
  @Output() public pathPicked = new EventEmitter<FieldPath>();

  // This view query is used to select the text when switching into manual path
  // editing mode.
  @ViewChild('textPathInput') pathTextInputElement: ElementRef;



  public fieldPath: FieldPath;
  public editMode = false;

  private _boardPathResolver: BoardPathResolver = null;

  get pathResolver() {
    return this.userDefinedResolver || this._boardPathResolver;
  }

  private _completions: FieldPath[] = null;

  public mode: 'picker' | 'text' = 'picker';

  private closeSubscription: Subscription = null;
  private hasJustOpened = false;

  constructor(
    private gravity: Gravity,
    @Optional() @Inject(CLOSE_DIALOG_SERVICE_TOKEN) private dialogManagerService: CloseDialogService,
    private changeDetector: ChangeDetectorRef,
  ) { }

  ngOnInit() {
    this.update();
    if (this.dialogManagerService) {
      this.closeSubscription = this.dialogManagerService.onClose.subscribe(() => {
        if (!this.hasJustOpened) {
          this.finishEditing();
        }
      });
    }
  }

  ngOnDestroy() {
    if (this.closeSubscription) {
      this.closeSubscription.unsubscribe();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.filter) {
      this.flushCompletions();
    }
    if (changes.userDefinedResolver || changes.board) {
      this.update();
    }
  }

  /**
   * Updates the path picker state
   *
   * This should be called if the path variable or resolver is changed as it
   * will update all the things.
   */
  update() {
    const path = this.path || '';

    // If we're using the shorthand method of creating a path picker from a
    // board ID, then update the path resolver implementation.
    if (this.board && !this.userDefinedResolver) {
      this._boardPathResolver = new BoardPathResolver(this.board, this.gravity.configService);
    } else {
      this._boardPathResolver = null;
    }

    this.fieldPath = this.pathResolver ? this.pathResolver.resolve(path) : null;

    if (this.isPickerMode && !this.isPickablePath) {
      this.mode = 'text';
    }

    this.flushCompletions();
  }

  get isPickablePath(): boolean {
    return this.pathResolver != null && (this.path || '').match(FIELD_PATH_REGEX) != null;
  }

  get isPickerMode() {
    return this.mode === 'picker';
  }

  get selectedPathMessage(): string {
    if (!this.pathResolver) {
      return 'No board selected';
    } else {
      return `Fields for '${this.fieldPath || this.pathResolver.dataSourceName}'`;
    }
  }

  get hasCompletions() {
    return this.completions && this.completions.length > 0;
  }

  startEditing() {
    this.editMode = true;
    this.propagateTouch();
    this.hasJustOpened = true;
    setTimeout(() => this.hasJustOpened = false, 50);
    this.changeDetector.markForCheck();
  }

  finishEditing() {
    this.editMode = false;
    this.propagateTouch();
    this.changeDetector.markForCheck();
  }

  editAsText() {
    this.editMode = true;
    this.mode = 'text';
    setTimeout(() => {
      if (this.pathTextInputElement) {
        const pathInputElement = this.pathTextInputElement.nativeElement as HTMLInputElement;
        pathInputElement.focus();
        pathInputElement.select();
      }
    }, 10);
    this.changeDetector.markForCheck();
  }

  editAsPath() {
    this.editMode = true;
    this.mode = 'picker';
    this.flushCompletions();
    this.changeDetector.markForCheck();
  }

  selectPath(fieldPath: FieldPath) {
    this.fieldPath = fieldPath;
    this.path = this.fieldPath ? this.fieldPath.path : '';
    this.pathUpdated();
    this.flushCompletions();

    // If there are no further questions...
    if (this.completions.length === 0) {
      this.finishEditing();
    }
  }

  pathUpdated() {
    this.propagateChange(this.path);
    this.pathPicked.emit(this.fieldPath);
    this.changeDetector.markForCheck();
  }

  pathManuallyEdited() {
    this.pathUpdated();
    if (this.isPickablePath && this.pathResolver) {
      this.fieldPath = this.pathResolver.resolve(this.path);
    }
  }

  private flushCompletions() {
    this._completions = null;
    this.changeDetector.markForCheck();
  }

  get pathSegments(): FieldPath[] {
    return this.fieldPath ? this.fieldPath.segments : [];
  }

  get completions(): FieldPath[] {
    if (!this._completions && this.pathResolver) {
      this._completions = sortBy(
        this.pathResolver.getChildPaths(this.fieldPath),
        fieldPath => [!fieldPath.isReferenceField, fieldPath.isMetadata, fieldPath.fieldName]);
      if (this.filter) {
        this._completions = this._completions.filter(this.filter);
      }
    }
    return this._completions;
  }

  writeValue(value: string) {
    if (!value) { return; }
    this.path = value || '';
    this.update();
  }

  propagateChange = (_: any) => { };

  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  propagateTouch = () => { };

  registerOnTouched(fn) {
    this.propagateTouch = fn;
  }
}
