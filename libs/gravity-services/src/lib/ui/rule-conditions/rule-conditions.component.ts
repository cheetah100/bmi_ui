import { Component, Input } from '@angular/core';
import { GravityApiCondition, GravityApiConditionType } from '../../api-types/gravity-api-condition';
import { Gravity } from '../../gravity';
import { Condition } from '../../model/condition';
import { Option, TemplatedFormArray } from '@bmi/ui';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gravity-rule-conditions',
  templateUrl: './rule-conditions.component.html',
  styleUrls: [
    './rule-conditions.component.scss',
  ],
})
export class RuleConditionsComponent {

  @Input() conditions: TemplatedFormArray<GravityApiCondition>;
  @Input() board: string;

  conditionTypes: Option[] = [
    { value: 'PROPERTY', text: 'Property (regular)' },
    { value: 'PHASE', text: 'Phase Condition' },
    { value: 'TASK', text: 'Task Status Condition' },
  ];

  selectedConditionType: GravityApiConditionType = 'PROPERTY';

  constructor(private gravity: Gravity) { }

  addCondition(type: string) {
    switch (type) {
      case 'PROPERTY':
        this.addConditionImpl(null, type);
        break;
      case 'PHASE':
        this.addConditionImpl('phase', type);
        break;
      case 'TASK':
        this.addConditionImpl(null, type);
        break;
      default:
        break;
    }
  }

  addConditionImpl(fieldName: string = null, conditionType: GravityApiConditionType = 'PROPERTY'): void {
    const operations = Condition.getValidOperations(conditionType);
    const defaultOperation = operations[0] ? operations[0].value : 'EQUALTO';
    const newCondition: GravityApiCondition = {
      fieldName,
      operation: defaultOperation,
      value: null,
      conditionType,
    };
    this.conditions.pushValue(newCondition);
    this.conditions.markAsTouched();
    this.conditions.markAsDirty();
  }

}


