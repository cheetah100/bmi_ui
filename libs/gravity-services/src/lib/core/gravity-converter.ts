import { Injectable } from '@angular/core';

import { Card } from './card';
import { GravityConfigService } from './gravity-config.service';
import { GravityApiCardView } from '../api-types/gravity-api-cardview';

import { cloneDeep } from 'lodash-es';

@Injectable()
export class GravityConverter {

  constructor(private configService: GravityConfigService) { }

  fromJson(boardName: string, json: GravityApiCardView): Card {
    const boardClass = this.configService.getBoardClass(boardName);
    const boardConfig = this.configService.getBoardConfig(boardName);

    const card: Card = new boardClass();
    card.id = json.id;
    card.phase = json.phase;
    card.board = json.board;
    card.created = json.created ? new Date(json.created) : undefined;
    card.creator = json.creator;
    card.modified = json.modified ? new Date(json.modified) : undefined;
    card.modifiedBy = json.modifiedby;
    /** HACK: Some cards have card.title, some have card.fields.title while some have card.fields.name
     * Adding the code below, till the backend normalizes all cards to one format.
     */
    card.title = card.gravityTitle = this.getGravityTitle(json);

    Object.assign(card.fields, cloneDeep(json.fields));

    for (const field of Object.values(boardConfig.fields)) {
      // Never look up metadata fields when converting card JSON. There are some
      // cards which have a "phase" value stored in their fields, but this
      // causes bugs, as Gravity doesn't care about that.
      if (field.isMetadata) {
        continue;
      }

      const value = json.fields[field.id];
      if (value === null || value === undefined) {
        continue;
      }

      if (field.optionlist) {
        card.setReference(field.id, value);
      }
    }

    return card;
  }

  getGravityTitle(json: GravityApiCardView) {
    return (json.title && json.title.length > 0) ? json.title : (json.fields.name) ? json.fields.name : json.fields.title || '';
  }

  toJson(boardName: string, card: Card): { [field: string]: any } {
    const json = {};
    const boardConfig = this.configService.getBoardConfig(boardName);

    Object.keys(boardConfig.fields).map(id => boardConfig.fields[id]).forEach(field => {
      json[field.id] = cloneDeep(card.fields[field.id]);
    });

    return json;
  }
}
