import { Permissions } from './permissions';
import { GravityApiPhase, GravityApiTemplateField, GravityApiBoard, GravityApiTemplate } from '../api-types/gravity-api-board-config';
import { GravityApiView } from '../api-types/gravity-api-view';
export { Permissions } from './permissions';

import cloneDeep from 'lodash-es/cloneDeep';

export interface MetaData {
  id: Field;
  gravityTitle: Field;
  title: Field;
  created: Field;
  modified: Field;
  modifiedBy: Field;
  phase: Field;
}

export class BoardConfig {

  readonly _metaDataFields: MetaData = {
    id: {
      id: 'id',
      name: 'id',
      label: 'Card ID',
      type: 'STRING',
      required: true,
      requiredPhase: null,
      optionlist: null,
      referenceField: false,
      isMetadata: true
    },
    gravityTitle: {
      id: 'gravityTitle',
      name: 'gravityTitle',
      label: 'Gravity Title',
      type: 'STRING',
      required: true,
      requiredPhase: null,
      optionlist: null,
      referenceField: false,
      isMetadata: true
    },
    title: {
      id: 'title',
      name: 'title',
      label: 'Title',
      type: 'STRING',
      required: true,
      requiredPhase: null,
      optionlist: null,
      referenceField: false,
      isMetadata: true
    },
    phase: {
      id: 'phase',
      name: 'phase',
      label: 'Phase',
      type: 'STRING',
      required: true,
      requiredPhase: null,
      optionlist: null,
      referenceField: false,
      isMetadata: true
    },
    created: {
      id: 'created',
      name: 'created',
      label: 'Created',
      type: 'DATE',
      required: false,
      requiredPhase: null,
      optionlist: null,
      referenceField: false,
      isMetadata: true
    },
    modified: {
      id: 'modified',
      name: 'modified',
      label: 'Modified',
      type: 'DATE',
      required: false,
      requiredPhase: null,
      optionlist: null,
      referenceField: false,
      isMetadata: true
    },
    modifiedBy: {
      id: 'modifiedBy',
      name: 'modifiedBy',
      label: 'Modified By',
      type: 'STRING',
      required: false,
      requiredPhase: null,
      optionlist: null,
      referenceField: false,
      isMetadata: true
    }
  };

  private _cachedFields: Fields = null;

  static fromConfig(originalConfig: GravityApiBoard): BoardConfig {
    const config = cloneDeep(originalConfig);
    if (!config.id) {
      throw new Error(`Can't register board without an id`);
    }

    const boardConfig = new BoardConfig(config);
    if (boardConfig.defaultTemplate && boardConfig.defaultTemplate.fields) {
      // There is still some code that is accessing these board configs directly
      // and rely on the default template fields being available like this, even
      // though Gravity doesn't supply them.
      // We want to get rid of this. and this class no longer needs the field.
      config.fields = boardConfig.defaultTemplate.fields;
      return boardConfig;
    } else {
      throw new Error(`No template associated with ${config.id}`);
    }
  }

  constructor(
    public readonly config: GravityApiBoard
  ) { }

  get id(): string {
    return this.config.id;
  }

  get name(): string {
    return this.config.name;
  }

  get classification() {
    return this.config?.classification || null;
  }

  get description() {
    return this.config.description;
  }

  get boardType(): string {
    return this.config.boardType;
  }

  get boardName(): string {
    return this.config.id;
  }

  get permissions(): Permissions {
    return this.config.permissions;
  }

  get defaultTemplate(): Template {
    return this.templates[this.id];
  }

  get type():string {
    return this.config?.boardType || null;
  }

  get ui(): BoardUiData {
    return this.defaultTemplate.ui || {};
  }

  /**
   * Get the fields for this board.
   *
   * This includes entries for the metadata fields, even though they do not
   * exist on the board template.
   */
  get fields(): Fields {
    // We want to augment the fields on the board template with the hardcoded
    // metadata fields that will be present on every card.
    if (!this._cachedFields) {
      this._cachedFields = Object.assign({}, this.defaultTemplate.fields, this._metaDataFields);
    }
    return this._cachedFields;
  }

  get phases(): Phases {
    return this.config.phases;
  }

  get views(): Views {
    return this.config.views;
  }

  get templates(): Templates {
    return this.config.templates;
  }

  get orderField(): string {
    return this.config.orderField;
  }

  get metaData(): MetaData {
    return this._metaDataFields;
  }
}

export interface Field extends GravityApiTemplateField {
  isMetadata?: boolean;
}
export interface FieldViewModel extends Field {
  isNew?: boolean;
}

export type Phase = GravityApiPhase;

export interface PhaseViewModel extends Phase {
  isNew?: boolean;
}


export interface Template extends GravityApiTemplate {
  ui?: BoardUiData;
}

export interface BoardUiData {
  icon?: string;
  color?: string;
  singular?: string;
  plural?: string;
}

export interface Fields { [fieldName: string]: Field; }
export interface Phases { [phaseName: string]: Phase; }
export interface Views { [viewName: string]: GravityApiView; }
export interface Templates { [template: string]: Template; }
