
import {refCount, publishLast, map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { DataStore } from './data-store';

@Injectable()
export class TransformRepository {

  constructor(
    private http: HttpClient,
    private dataStore: DataStore
  ) {}

  getTransforms(): Observable<any> {
    return this.http.get('/gravity/spring/transform')
      .pipe(
        map((teams: any) => {
          return Object.keys(teams).map(team => {
            return {
              id: team,
              title: teams[team]
            }
          });
        }),
        publishLast(),
        refCount()
      );
  }
}
