
import {of as observableOf,  Observable, ReplaySubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { UserRepository } from './user.repository';
import { DashboardRepository } from '../dashboard/dashboard.repository';
import { GravityService } from './gravity.service';
import { DataStoreItem, DataStore } from './data-store';
import { GravityConfigService } from './gravity-config.service';

@Injectable()
export class GravityCache {

  // Eventually want to deprecate this in favor of a more efficient approach
  private readySubject = new ReplaySubject(1);
  loadingData = {};

  constructor(
    private gravityService: GravityService,
    private gravityConfigService: GravityConfigService,
    private userRepository: UserRepository,
    private dashboardRepository: DashboardRepository,
    private dataStore: DataStore) {

      this.initialize();
  }

  private initialize() {
    this.readySubject.next();
  }

  // Eventually want to deprecate this in favor of a more efficient approach
  ready(): Observable<any> {
    return this.readySubject.asObservable();
  }

  get<T extends DataStoreItem = any>(dataType: string, id: string): Observable<T> {
    // A better implementation of this wouldn't fetch all the cards, rather
    // it could just request the specific cards needed, potentially debouncing
    // these requests to batch them up.
    return this.getAll<T>(dataType).pipe(
      map(() => this.dataStore.getById(dataType, id))
    );
  }

  getAll<T extends DataStoreItem = any>(dataType: string, refresh = false): Observable<T[]> {
    if (
      ['users', 'teams', 'dashboards'].indexOf(dataType) === -1
      && this.gravityConfigService.getRegisteredBoards().indexOf(dataType) === -1
    ) {
      return observableOf([]);
    }

    if (!refresh && this.dataStore.hasAll(dataType)) {
      return observableOf(this.dataStore.getAll(dataType));
    }

    if (this.loadingData[dataType]) {
      return this.loadingData[dataType] as Observable<T[]>;
    } else {
      switch (dataType) {
        case 'users':
          this.loadingData[dataType] = this.userRepository.getAll();
          break;
        case 'teams':
          this.loadingData[dataType] = this.userRepository.getAllTeams();
          break;
        case 'dashboards':
          this.loadingData[dataType] = this.dashboardRepository.getAll();
          break;
        default:
          this.loadingData[dataType] = this.gravityService.getCards(dataType);
          break;
      }

      // If the source completes synchronously, this was previously clearing the
      // loading data observable immediately, causing this method to return null.
      // Instead now, this will capture the new value before subscribing.
      const source = this.loadingData[dataType] as Observable<T[]>;
      source.subscribe(() => {
        this.loadingData[dataType] = null;
      }, () => {
        this.loadingData[dataType] = null;
      });

      return source;
    }
  }

  removeAll(boardId: string): void {
    this.dataStore.removeAll(boardId);
  }

}
