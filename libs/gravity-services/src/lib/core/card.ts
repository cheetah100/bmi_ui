import { BoardConfig, Field } from './board-config';
import { DataStore } from './data-store';

import { isEqual } from 'lodash-es';
import { cloneDeep } from 'lodash-es';

import * as tslib from 'tslib';

/** @deprecated This is an old IDP thing */
export abstract class Card {

  /**
   * These are the names of the metadata fields defined below.
   *
   * This is used by the unit tests and the clone() method.
   */
  public static readonly METADATA_FIELDS = ['id', 'phase', 'board', 'creator', 'gravityTitle', 'modified', 'created', 'modifiedBy'];

  [field: string]: any;
  phase: string;
  board: string;
  id: string;
  creator: string;
  modifiedBy: string;
  created: Date;
  modified: Date;
  gravityTitle: string;
  title?: string;

  /** Provides direct access to the card fields. */
  public readonly fields: { [field: string]: any } = {};

  abstract get dataStore(): DataStore;

  // Relationships gets used internally by each card to store what the relationships are.
  // This should never be used directly. Instead use the setReference and getRelatedCards.
  protected readonly _relationships: { [property: string]: string | string[] } = {};

  getConfig(): BoardConfig {
    return this.constructor['config'];
  }

  // This will get the value of the card based on it's path, for example: workstream.cdo_track.value_stream.cdo_platform
  getValueFromPath(path: string): any {
    const parts = path.split('.');
    let value = this;
    for (let i = 0; i < parts.length; i++) {
      value = value[parts[i]];
      if (!value) {
        return value;
      }
    }

    return value;
  }

  getReference(property): CardRelationship {
    if (!this._relationships || !this._relationships[property]) {
      return null;
    } else {
      const referenceValue = this._relationships[property];
      const fieldConfig = this.getConfig().fields[property];
      return new CardRelationship(
        (typeof (referenceValue) === 'string' ? 'one' : 'many'),
        fieldConfig.optionlist,
        referenceValue
      );
    }
  }

  setReference(property: string, references: string | string[]) {
    this.fields[property] = references;
    this._relationships[property] = references;
  }

  getRelatedCards(field: Field): any | any[] {
    const fieldId = field.id;
    const relationship = this._relationships && this._relationships[fieldId];

    // NOTE: If the relationship is null, that means there is no relationship. This is different.
    // from if the relationship is typeof undefined, which would indicate that the relationship hasn't been.
    // resolved. This might happen when we haven't downloaded all the fields for this card.
    if (typeof relationship === 'undefined') {
      return undefined;
    } else if (relationship === null) {
      return null;
    } else if (typeof relationship === 'string') {
      return this.dataStore.getById(field.optionlist, relationship);
    } else if (Array.isArray(relationship)) {
      return relationship
        .map(reference => this.dataStore.getById(field.optionlist, reference))
        .filter(v => !!v && v.phase !== 'archive');
    } else {
      // We should never hit this, if we do, either there is a bug in this code
      // or something is corrupting _relationships.
      throw new Error('Not sure how to get related cards');
    }
  }

  /**
   * This will make an immutable clone of the current card, including relationships.
   */
  clone(): Card {
    // Create an instance.
    const instance = new (<any>this.constructor)() as Card;

    Object.assign(instance.fields, cloneDeep(this.fields));
    Object.assign(instance._relationships, cloneDeep(this._relationships));
    Card.METADATA_FIELDS.forEach(field => instance[field] = this[field]);

    return instance;
  }

  /**
   * Returns true if this card has different values from another card.
   */
  hasChangedFrom(previous: Card): boolean {
    // Previously this used a comparison of the card's modification timestamp,
    // but this lead to issues if the card didn't have a timestamp -- it would
    // miss updates. Instead, this does a shallow comparison of the object (using
    // lodash isEqual).
    return !isEqual(this, previous);
  }


  toString(): string {
    // This is an attempt to provide a sensible summary for a card.
    // Unfortunately the gravityTitle isn't always popuplated.
    return this.gravityTitle || this['title'] || this['name'] || this.id;
  }

  /**
   * Prepares a Card model class for use
   *
   * This will define the dynamic properties based on the board config and bind
   * the dataStore (for access from the model instance).
   *
   * If provided, the `modelClass` is the custom card implementation to use,
   * otherwise this will just define a subclass of Card.
   *
   * @param boardConfig the board config object; the fields from this will be turned into card properties
   * @param dataStore the datastore instance to use when accessing related cards.
   * @param modelClass optionally, use this as the base class. This allows custom properties to be defined.
   * @returns a parameterless constructor function for the Card class.
   */
  static prepareCardClass(boardConfig: BoardConfig, dataStore: DataStore, modelClass: Function = null): Function {
    let boardClass: Function;
    if (modelClass) {
      boardClass = modelClass;
    } else {
      boardClass = class extends Card {
        // This will get patched over anyway.
        get dataStore() { return dataStore; }

        constructor() {
          super();
        }
      }
    }

    Object.keys(boardConfig.fields).map(id => boardConfig.fields[id]).filter(f => !f.isMetadata).forEach(field => {
      const fieldId = field.id;
      if (field.optionlist) {
        Object.defineProperty(boardClass.prototype, fieldId, {
          get: function () {
            return (<Card>this).getRelatedCards(field);
          },
          enumerable: true,
          configurable: true
        });

        Object.defineProperty(boardClass.prototype, 'set_' + fieldId, {
          set: function (value) {
            this.setReference(fieldId, value);
          },
          enumerable: true,
          configurable: true
        });
      } else {
        Object.defineProperty(boardClass.prototype, fieldId, {
          get: function () { return (<Card>this).fields[fieldId]; },
          set: function (value) { (<Card>this).fields[fieldId] = value; },
          enumerable: true,
          configurable: true
        });
      }

    });

    Object.defineProperty(boardClass, 'name', { value: boardConfig.boardName, configurable: true });
    Object.defineProperty(boardClass, 'config', { value: boardConfig, configurable: true });

    // Make the data store instance available on this Card object.
    // This was previously done using a singleton on the DataStore, and while
    // this is a slight improvement, it's still not a great solution. As IDP's
    // data model has become more dynamic, this style of card class seems to
    // have fallen out of favour anyway and could probably afford to be
    // simplified further.
    Object.defineProperty(boardClass.prototype, 'dataStore', {
      get: () => dataStore,
      configurable: true,
    });

    return boardClass;
  }
}

export class CardRelationship {
  get isSingleCard() {
    return this.type === 'one';
  }

  get id(): string {
    return typeof (this.value) === 'string' ? this.value : null;
  }

  get ids(): string[] {
    const value = this.value;
    if (typeof (value) === 'string') {
      return [value];
    } else {
      return value;
    }
  }

  constructor(
    public type: 'one' | 'many',
    public board: string,
    public value: string | string[]
  ) { }
}
