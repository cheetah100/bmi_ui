import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { publishLast, refCount, map, tap } from 'rxjs/operators';

import { DataStore } from './data-store';
import { GravityApiUser } from '../api-types/gravity-api-user';
import { mapObjectValues } from '@bmi/utils';


function subscribeAndSuppressError<T>(observable: Observable<T>) {
  observable.subscribe(() => { }, () => { });
  return observable;
}

@Injectable()
export class UserRepository {

  public readonly currentUser: User;

  constructor(
    private http: HttpClient,
    private dataStore: DataStore,
    @Inject('USER_PROVIDER') currentUser: GravityApiUser
  ) {
    this.currentUser = this.transformFromApi(currentUser);
  }

  getAllTeams(saveToStore = true): Observable<Team[]> {
    const source = this.http.get<{ [teamId: string]: string }>('/gravity/spring/team').pipe(
      map(teams => Object.entries(teams).map(([id, title]) => <Team>{ id, title })),
      publishLast<Team[]>(),
      refCount());

    if (saveToStore) {
      source.subscribe(teams => {
        this.dataStore.set('teams', teams);
      }, (error) => { });
    }

    return source;
  }

  getAll(): Observable<User[]> {
    const source = this.http.get<{ [userId: string]: GravityApiUser }>('/gravity/spring/user/userdetails').pipe(
      map(users => mapObjectValues(users, user => this.transformFromApi(user))),
      tap(users => this.dataStore.set('users', users)),
      publishLast<User[]>(),
      refCount());

    return subscribeAndSuppressError(source);
  }

  /**
   * Combines User Object with the corresponding preferences
   * @param preferences: Can either be
   * 1. []: Empty array, returns only the user object
   * 2. ['preferenceId']: Accepts a list of preference ids, which would be added in the response
   * 3. null: Returns all the user objects and all their respective preferences
   */
  getUsersWithPreference(preferences: string[] = []): Observable<User[]> {
    const payload = preferences;
    const source = this.http.post<GravityApiUser[]>('/gravity/spring/user/preference/all', payload).pipe(
      map(users => users.map(u => this.transformFromApi(u))),
      publishLast<User[]>(),
      refCount());

    return source;
  }

  get(userId: string): Observable<User> {
    const source = this.http.get<GravityApiUser>(`/gravity/spring/user/${userId}`).pipe(
      map(user => this.transformFromApi(user)),
      publishLast<User>(),
      refCount());

    return subscribeAndSuppressError(source);
  }

  add(user: User): Observable<User> {
    const source = this.http.post<GravityApiUser>(`/gravity/spring/user`, this.transformToApi(user)).pipe(
      map(addedUser => this.transformFromApi(addedUser)),
      tap(addedUser => this.dataStore.addOrUpdate('users', addedUser)),
      publishLast<User>(),
      refCount());

    return subscribeAndSuppressError(source);
  }

  update(userId: string, user: User): Observable<User> {
    const source = this.http.put<GravityApiUser>(`/gravity/spring/user/${userId}`, this.transformToApi(user)).pipe(
      map(updatedUser => this.transformFromApi(updatedUser)),
      tap(updatedUser => this.dataStore.addOrUpdate('users', updatedUser)),
      publishLast<User>(),
      refCount());

    return subscribeAndSuppressError(source);
  }

  delete(userId: string): Observable<void> {
    const source = this.http.delete(`/gravity/spring/user/${userId}`).pipe(
      map(() => undefined),
      tap(() => this.dataStore.remove('users', userId)),
      publishLast<void>(),
      refCount());

    return subscribeAndSuppressError(source);
  }

  private transformToApi(user: User): any {
    const teams = {};
    user.teams.forEach(team => {
      teams[team] = team === 'administrators' ? 'ADMIN' : '';
    });

    return {
      ...user,
      teams: teams,
    };
  }

  private transformFromApi(payload: GravityApiUser): User {
    return {
      ...payload,
      teams: Object.keys(payload.teams)
    };
  }
}

export interface Team {
  id: string;
  title: string;
}

export interface User {
  id: string;
  name: string;
  email: string;
  firstname: string;
  surname: string;
  fullName: string;
  teams: string[];
  preference?: Preference[];
}

export type UserAndPreference = User;

interface Preference {
  uniqueid?: string;
  id: string;
  values: any;
  userId?: string;
  name?: string;
}
