import { TestBed } from '@angular/core/testing';

import { gravityTestbed } from '../testing/gravity-test-tools';
import { QUARTER_BOARD_CONFIG, QUARTER_CARDS } from '../testing/model-board-configs';
import { GravityCache } from '../core/gravity-cache';
import { Card } from '../core/card';

import {
  MOCK_INVESTMENTS_BOARD_CONFIG,
  MOCK_PROJECTS_BOARD_CONFIG,
  MOCK_PROJECTS_CARDS,
  MOCK_INVESTMENTS_CARDS
} from '../testing/mock-investments';


const TEST_CONFIGS = [
  QUARTER_BOARD_CONFIG,
  MOCK_INVESTMENTS_BOARD_CONFIG,
  MOCK_PROJECTS_BOARD_CONFIG
];

const TEST_CARDS = {
  quarters: QUARTER_CARDS,
  projects: MOCK_PROJECTS_CARDS,
  investments: MOCK_INVESTMENTS_CARDS
};


describe('Gravity Card Implementation', () => {
  let cache: GravityCache;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        gravityTestbed({
          boards: TEST_CONFIGS,
          cards: TEST_CARDS,
          injectNullHttpClient: true,
          useFakeGravityService: true
        })
      ]
    });
    cache = TestBed.get(GravityCache) as GravityCache;
  });

  function getCard(board: string, cardId: string) {
    let card: Card;
    cache.get<Card>(board, cardId).subscribe(c => card = c);
    return card;
  }

  function cacheCards(...boards: string[]) {
    boards.forEach(b => cache.getAll(b).subscribe());
  }

  describe('Related card access', () => {

    it('should get related cards if present in cache', () => {
      let investment: Card;
      cacheCards('projects');
      cache.get<Card>('investments', 'iv01').subscribe(iv => investment = iv);

      expect(investment.project).toBeDefined();
      expect(investment.project.id).toBe('p01');
    });

    it(`should return an undefined value if the card is not in the cache`, () => {
      let investment: Card;
      cache.get<Card>('investments', 'iv01').subscribe(iv => investment = iv);

      expect(investment.project).toBeUndefined();
    });

    describe('List field', () => {
      let project: Card;
      beforeEach(() => {
        cacheCards('projects', 'quarters');
        project = getCard('projects', 'p01');
      });

      it('should return a list of values for list fields', () => {
        expect(Array.isArray(project.activeQuarters)).toBeTruthy();
      });

      it('should return the related card instance from the datastore', () => {
        const q1fy19 = getCard('quarters', 'Q1FY19');
        expect(project.activeQuarters).toContain(q1fy19);
      });

      it('should retain the order from the original data', () => {
        // This project has more activeQuarters
        project = getCard('projects', 'p03');
        expect(project.activeQuarters.map(q => q.id)).toEqual(['Q3FY18', 'Q4FY18', 'Q1FY19', 'Q2FY19']);
      });
    });
  });

  describe('Modifying related cards', () => {
    let investment: Card;
    beforeEach(() => {
      cacheCards('projects', 'quarters', 'investments');
      investment = getCard('investments', 'iv04');
    });

    it('should reflect changes made when assigning the set_... properties', () => {
      investment.set_project = 'p01';
      expect(investment.project.id).toBe('p01');
    });

    describe('Updating array values', () => {
      let project: Card;
      beforeEach(() => {
        project = getCard('projects', 'p03');
      });

      it('should handle changing the values', () => {
        project.set_activeQuarters = ['Q1FY19'];
        expect(project.activeQuarters.length).toBe(1);
        expect(project.activeQuarters[0].id).toBe('Q1FY19');
      });

      it('should handle setting the value to an empty array', () => {
        project.set_activeQuarters = [];
        expect(project.activeQuarters.length).toBe(0);
      });
    });
  });

  describe('Card fields object', () => {
    let card: Card;
    beforeEach(() => {
      cache.getAll('quarters').subscribe();
      cache.getAll('projects').subscribe();
      card = getCard('investments', 'iv04');
    });

    it('should populate the fields object with the raw card data', () => {
      expect(card.fields).toEqual(TEST_CARDS['investments'].find(c => c.id === card.id).fields);
    });

    it('should update the fields object when modifying a card field via property', () => {
      card.value = 100;
      expect(card.value).toBe(100);
      expect(card.fields['value']).toBe(100);
    });

    it('should reflect manual changes to the card fields in the getter properties', () => {
      card.fields['value'] = 100;
      expect(card.value).toBe(100);
    });

    it('should update the fields object when changing a related card via a `set_...` property', () => {
      card.set_project = 'p01';
      expect(card.fields['project']).toBe('p01');
    });
  });

  describe('Cloning', () => {
    let originalCard: Card;
    let clonedCard: Card;
    beforeEach(() => {
      originalCard = clonedCard = undefined;
      cache.getAll('quarters').subscribe();
      cache.getAll('projects').subscribe();
      originalCard = getCard('investments', 'iv04');
      clonedCard = originalCard.clone();
    });

    it('should return a different instance', () => {
      expect(clonedCard).not.toBe(originalCard);
    });

    describe('copying metadata', () => {
      for (const field of Card.METADATA_FIELDS) {
        it(`should copy the ${field} metadata field`, () => {
          expect(clonedCard[field]).toBe(originalCard[field]);
        });
      }
    });

    it('should use a different field object instance', () => {
      expect(originalCard.fields).not.toBe(clonedCard.fields);
    });

    it('should copy the fields into the cloned instance', () => {
      expect(originalCard.fields).toEqual(clonedCard.fields);
    });

    it('should have access to related cards', () => {
      expect(clonedCard.project).toBeDefined();
      expect(clonedCard.project).toBe(originalCard.project);
    });

    describe('modifying after cloning', () => {
      it('should not affect field values', () => {
        originalCard.value = 20000;
        expect(clonedCard.value).toBe(80000);
      });

      it('should not affect relationships', () => {
        originalCard.set_project = 'p01';
        expect(originalCard.project.id).toBe('p01');
        expect(clonedCard.project.id).toBe('p03');
      });
    });
  });
});

