
import { Injectable, Inject, Optional } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { DataStore } from '../core/data-store';
import { GravityParameters, GRAVITY_PARAMETERS } from '../gravity-parameters';
import { Card } from './card';
import { BoardConfig } from './board-config';
import * as tslib from 'tslib';

import { Observable, of, Subject, concat } from 'rxjs';
import { filter, map, publishReplay, refCount, debounceTime } from 'rxjs/operators';
import { GravityApiBoard } from '../api-types/gravity-api-board-config';

@Injectable({providedIn: 'root'})
export class GravityConfigService {

  private boardClasses = {};
  private boardUpdatesSubject: Subject<string> = new Subject<string>();

  private get customModelClasses() {
    return this.gravityParameters.customCardModelClasses || {};
  }

  constructor(
    @Optional() @Inject(GRAVITY_PARAMETERS) private gravityParameters: GravityParameters,
    @Optional() private dataStore: DataStore
  ) {
    if (gravityParameters && gravityParameters.boardConfigs) {
      const configs = gravityParameters.boardConfigs;
      Object.keys(configs).forEach(boardName => {
        const config = configs[boardName];
        try {
          this.registerBoard(config);
        } catch (err) {
          console.error(`Error when registering board ${boardName}:`, String(err));
        }
      });
    }
  }

  getRegisteredBoards(): string[] {
    return Object.keys(this.boardClasses);
  }

  createCard(boardName: string): Card {
    const boardClass = this.getBoardClass(boardName);
    return new boardClass();
  }

  getBoardConfig(boardName: string): BoardConfig {
    return this.getBoardClass(boardName).config;
  }

  getBoardConfigs(): BoardConfig[] {
    return this.getRegisteredBoards().map(id => this.getBoardConfig(id));
  }

  boardExists(boardName: string): boolean {
    return !!this.boardClasses[boardName];
  }

  getBoardClass(boardName: string) {
    if (!this.boardClasses[boardName]) {
      throw Error(`Unknown board ${boardName}`);
    }

    return this.boardClasses[boardName];
  }

  boardUpdatesObservable(): Observable<string> {
    return this.boardUpdatesSubject.asObservable();
  }

  /**
   * Watches for changes to a board's config
   *
   * This will get the configuration for a board, and then emit further values
   * when the config is updated.
   *
   * @param boardId the board to watch
   * @returns an Observable of board configs
   */
  watchBoardConfig(boardId: string): Observable<BoardConfig> {
    return concat(
      of(this.getBoardConfig(boardId)),
      this.boardUpdatesObservable().pipe(
        filter(changedBoardId => changedBoardId === boardId),
        map(changedBoardId => this.getBoardConfig(boardId))
      )
    ).pipe(
      publishReplay(1),
      refCount()
    );
  }

  watchAllConfigs(): Observable<BoardConfig[]> {
    return concat(
      of(this.getBoardConfigs()),
      this.boardUpdatesObservable().pipe(
        debounceTime(0),
        map(() => this.getBoardConfigs())
      )
    ).pipe(
      publishReplay(1),
      refCount()
    );
  }

  emitUpdatedBoardName(boardId: string): void {
    this.boardUpdatesSubject.next(boardId);
  }

  public registerBoard(config: GravityApiBoard, emitEvent = false) {
    const boardConfig = BoardConfig.fromConfig(config);
    const modelClass = this.customModelClasses[boardConfig.boardName] || null;
    this.boardClasses[boardConfig.boardName] = Card.prepareCardClass(boardConfig, this.dataStore, modelClass);

    if (emitEvent) {
      this.emitUpdatedBoardName(boardConfig.id);
    }

    return boardConfig;
  }
}
