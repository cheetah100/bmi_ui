import { QUARTER_BOARD_CONFIG, QUARTER_CARDS } from '../testing/model-board-configs';
import { gravityTestbed } from '../testing/gravity-test-tools';
import { GravityCache } from '../core/gravity-cache';
import { TestBed } from '@angular/core/testing';
import { GravityConverter } from '../core/gravity-converter';
import { Card } from '../core/card';

import {
  MOCK_INVESTMENTS_BOARD_CONFIG,
  MOCK_PROJECTS_BOARD_CONFIG,
  MOCK_PROJECTS_CARDS,
  MOCK_INVESTMENTS_CARDS
} from '../testing/mock-investments';


const TEST_CONFIGS = [
  QUARTER_BOARD_CONFIG,
  MOCK_INVESTMENTS_BOARD_CONFIG,
  MOCK_PROJECTS_BOARD_CONFIG,
];

const TEST_CARDS = {
  quarters: QUARTER_CARDS,
  projects: MOCK_PROJECTS_CARDS,
  investments: MOCK_INVESTMENTS_CARDS
};


describe('GravityConverter', () => {
  let cache: GravityCache;
  let converter: GravityConverter;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        gravityTestbed({
          boards: TEST_CONFIGS,
          cards: TEST_CARDS,
          injectNullHttpClient: true,
          useFakeGravityService: true
        })
      ]
    });
    cache = TestBed.get(GravityCache) as GravityCache;
    converter = TestBed.get(GravityConverter) as GravityConverter;
  });

  function getCard(board: string, cardId: string) {
    let card: Card;
    cache.get<Card>(board, cardId).subscribe(c => card = c);
    return card;
  }


  describe('toJson()', () => {
    let card: Card;
    let json: { [field: string]: any };
    beforeEach(() => {
      cache.getAll('projects').subscribe();
      card = getCard('investments', 'iv04');
      json = converter.toJson('investments', card);
    });

    it('should copy the regular fields', () => {
      expect(json['value']).toBe(card.value);
    });

    it('should copy reference field ids', () => {
      expect(json['project']).toBe(card.project.id);
    });

    it('should copy reference field ids, even if the object is not in the data store', () => {
      expect(json['quarter']).toBe('Q4FY18');
    });

    it('should save list reference fields', () => {
      const project = getCard('projects', 'p01');
      const projectJson = converter.toJson('projects', project);
      expect(projectJson['activeQuarters']).toEqual(['Q1FY19', 'Q2FY19']);
    });
  });
});
