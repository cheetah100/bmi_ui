import { Inject, Injectable, InjectionToken, Optional } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GRAVITY_PARAMETERS, GravityParameters } from '../gravity-parameters';
import { GravityUrlService } from '../../lib/services/gravity-url.service';


@Injectable({
  providedIn: 'root',
})
export class GravityHttpClient {

  constructor(
    private httpClient: HttpClient,
    private gravityUrlService: GravityUrlService
  ) {}

  get<T>(endPoint: string, options= {}): Observable<T> {
    const url = this.getFullUrl(endPoint);
    return this.httpClient.get<T>(url, options)
  }

  post<T>(endPoint: string, body:any, options= {}): Observable<T> {
    const url = this.getFullUrl(endPoint);
    return this.httpClient.post<T>(url, body, options);
  }

  put<T>(endPoint: string, body:any, options= {}): Observable<T> {
    const url = this.getFullUrl(endPoint);
    return this.httpClient.put<T>(url, body, options);
  }

  delete<T>(endPoint: string, options= {}): Observable<T> {
    const url = this.getFullUrl(endPoint);
    return this.httpClient.delete<T>(url, options)
  }

  private getFullUrl(endPoint: string) {
    return `${this.gravityUrlService.baseUrl}${endPoint}`;
  }
}
