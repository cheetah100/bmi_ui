export type Permission = 'ADMIN' | 'WRITE' | 'READ' | 'ROLLUP';
export interface Permissions { [team: string]: Permission; }

export const PERMISSION_ORDER = ['ROLLUP', 'READ', 'WRITE', 'ADMIN'];
const VALID_PERMISSIONS = [...PERMISSION_ORDER];


export function isValidPermission(p: string): p is Permission {
  return VALID_PERMISSIONS.includes(p);
}

export function maxPermission(a: Permission, b: Permission): Permission {
  const aIndex = PERMISSION_ORDER.findIndex(x => x === a);
  const bIndex = PERMISSION_ORDER.findIndex(x => x === b);
  return aIndex >= bIndex ? a : b;
}

export function minPermission(a: Permission, b: Permission): Permission {
  const aIndex = PERMISSION_ORDER.findIndex(x => x === a);
  const bIndex = PERMISSION_ORDER.findIndex(x => x === b);
  return aIndex <= bIndex ? a : b;
}

/**
 * Calculates the intersection of two sets of permissions.
 *
 * This takes the intersection of two permissions sets, and takes the lesser
 * permission (e.g. READ and WRITE = READ)
 *
 * @param p1 Permissions object
 * @param p2 Permissions object
 * @returns the intersected set of permissions
 */
export function narrowPermissions(p1: Permissions, p2: Permissions) {
  // NARROW the permissions. i.e. intersect and take the LESSER permission
  if (!p1 || !p2) {
    return {};
  }

  const result: Permissions = {};
  for (const [team, permission] of Object.entries(p2)) {
    const perm = minPermission(p1[team], permission);
    if (perm) {
      result[team] = perm;
    }
  }

  return result;
}


/**
 * Calculates the union of two sets of permissions.
 *
 * This is the union of all of the permissions object, and if the same
 * permission is in both sets then the greater access will apply (e.g. ADMIN
 * trumps WRITE).
 *
 * @param p1 Permissions object
 * @param p2 Permissions object
 * @returns the union set of permissions
 */
export function expandPermissions(p1: Permissions, p2: Permissions) {
  // EXPAND the permissions. union and take the GREATER permission.
  if (!p1 && !p2) {
    return {};
  } else if (!p1) {
    return {...p2};
  } else if (!p2) {
    return {...p1};
  } else {
    const result: Permissions = {...p1};
    for (const [team, permission] of Object.entries(p2)) {
      result[team] = maxPermission(p1[team], permission);
    }
    return result;
  }

}
