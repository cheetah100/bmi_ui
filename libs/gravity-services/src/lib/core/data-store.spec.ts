import { DataStore, DataStoreItem } from '../core/data-store';
import { Card } from '../core/card';
import { BoardConfigBuilder } from '../testing/board-config-builder';
import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { mockGravityProviders, makeCardJson } from '../testing/gravity-test-tools';
import { GravityConverter } from '../core/gravity-converter';
import { DataChange } from '../core/data-change';


const ITEM_WITH_CURRENT_PHASE = { id: 'test1', phase: 'current' };
const ITEM_WITH_NO_PHASE = { id: 'test2' };
const ARCHIVED_ITEM = { id: 'test3', phase: 'archive' };

const TEST_ITEMS: DataStoreItem[] = [
  ITEM_WITH_CURRENT_PHASE,
  ITEM_WITH_NO_PHASE,
  ARCHIVED_ITEM
];


const TEST_BOARDS = [
  new BoardConfigBuilder()
    .id('testBoard')
    .addField('name')
    .addPhase('current')
    .addPhase('archive', p => p.invisible())
    .finish()
];


const EXAMPLE_TEST_CARDS = makeCardJson('testBoard', [
  {
    id: 'TC01',
    fields: { name: 'Hello' }
  },
  {
    id: 'TC02',
    fields: { name: 'World' }
  }
]);

describe('[DataStore] getAll', () => {

  let dataStore: DataStore;
  beforeEach(() => {
    dataStore = new DataStore();
    dataStore.set('testItems', [...TEST_ITEMS]);
    dataStore.set('users', [...TEST_ITEMS]);
    dataStore.set('dashboards', [...TEST_ITEMS]);
  });

  it('should return an empty array when type is not in the datastore', () => {
    expect(dataStore.getAll('foobar')).toEqual([]);
  });

  it(`should filter out any items with phase 'archive'`, () => {
    const items = dataStore.getAll('testItems');
    expect(items).not.toContain(ARCHIVED_ITEM);
  });

  it(`should filter out in any items without a phase property`, () => {
    // This doesn't seem right to me, the behaviour caught me by surprise.
    // Commit in question:  7c2395a35d800db297bbcc1f5b0d0ae0cdf7dc4b
    const items = dataStore.getAll('testItems');
    expect(items).not.toContain(ITEM_WITH_NO_PHASE);
  });

  it('should leave any items with any other phase value', () => {
    expect(dataStore.getAll('testItems')).toContain(ITEM_WITH_CURRENT_PHASE);
  });

  it('should not filter archived users', () => {
    const users = dataStore.getAll('users');
    expect(users).toEqual(TEST_ITEMS);
  });

  it('should not filter archived dashboards', () => {
    const dashboards = dataStore.getAll('dashboards');
    expect(dashboards).toEqual(TEST_ITEMS);
  });

  it('should return a copy of the data, avoiding risk of accidental mutation', () => {
    const items = dataStore.getAll('users');
    items.splice(1);

    // If the data store is returning the same array, the above splice should
    // have removed all but the first item.
    const items2 = dataStore.getAll('users');
    expect(items).not.toBe(items2);
    expect(items2.length).toBe(3);
    expect(items2).toContain(ITEM_WITH_CURRENT_PHASE);
  })
});


describe('[DataStore] getById()', () => {
  let dataStore: DataStore;
  beforeEach(() => {
    dataStore = new DataStore();
    dataStore.set('testItems', [...TEST_ITEMS]);
  });

  it('should return undefined if the data type is not registered', () => {
    expect(dataStore.getById('missing', 'someId')).toBeUndefined();
  });

  it('should return undefined if no item matches the id', () => {
    expect(dataStore.getById('testItems', 'this_id_is_missing')).toBeUndefined();
  });

  it('should return the item matching the id', () => {
    expect(dataStore.getById('testItems', ITEM_WITH_NO_PHASE.id)).toBe(ITEM_WITH_NO_PHASE);
  });
});

describe('[DataStore] set()', () => {
  let dataStore: DataStore;
  beforeEach(() => {
    dataStore = new DataStore();
    dataStore.set('testItems', [...TEST_ITEMS]);
  });

  it(`throws an error if any item doesnt have an id`, () => {
    expect(() => {
      dataStore.set('test', [{ id: 'test1', foo: 'foo' }, { foo: 'bar' }]);
    }).toThrowError();

    expect(dataStore.getAll('test')).toEqual([]);
  });
});


describe('[DataStore] remove() and removeAll()', () => {
  let dataStore: DataStore;
  beforeEach(() => {
    dataStore = new DataStore();
    dataStore.set('users', [...TEST_ITEMS]);
  });

  it(`removeAll() should do nothing if the data type isn't registered`, () => {
    dataStore.removeAll('missing');
  });

  it(`removeAll() should remove all of the data items for a registered type`, () => {
    dataStore.removeAll('users');
    expect(dataStore.getAll('users')).toEqual([]);
    expect(dataStore.getById('user', 'test1')).toBeUndefined();
  });

  it('should remove an existing item by ID', () => {
    dataStore.remove('users', ITEM_WITH_CURRENT_PHASE.id);
    const users = dataStore.getAll('users');
    expect(users).not.toContain(ITEM_WITH_CURRENT_PHASE);
    expect(users).toContain(ITEM_WITH_NO_PHASE);
    expect(users).toContain(ARCHIVED_ITEM);
  });

  it('should notify when removing an item', () => {
    let notificationCount = 0;
    let dataChange: DataChange = null;
    dataStore.changes('users').subscribe(change => {
      notificationCount += 1;
      dataChange = change;
    });

    dataStore.remove('users', ITEM_WITH_CURRENT_PHASE.id);
    expect(notificationCount).toBe(1);
    expect(dataChange.changeType).toBe('delete');
    expect(dataChange.previousValue).toBe(ITEM_WITH_CURRENT_PHASE);
    expect(dataChange.currentValue).toBe(null);
  });
});


describe('[DataStore] addOrUpdate()', () => {
  let dataStore: DataStore;
  let httpTestingController: HttpTestingController;
  let gravityConverter: GravityConverter;
  let exampleCards: Card[];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        ...mockGravityProviders('', TEST_BOARDS),
      ]
    });

    dataStore = TestBed.get(DataStore) as DataStore;
    gravityConverter = TestBed.get(GravityConverter) as GravityConverter;
    httpTestingController = TestBed.get(HttpTestingController) as HttpTestingController;

    exampleCards = EXAMPLE_TEST_CARDS.map(c => gravityConverter.fromJson('testBoard', c));

    dataStore.set('testBoard', exampleCards);
    dataStore.set('users', [...TEST_ITEMS]);
  });

  const makeTestCard = (id: string, fields: { [id: string]: any }, phase = 'current') => {
    const cardJson = makeCardJson('testBoard', [{ id, phase, fields }])[0];
    return gravityConverter.fromJson('testBoard', cardJson);
  };

  afterEach(() => httpTestingController.verify());


  it('should add a new card to the collection', () => {
    const newCard = makeTestCard('TC03', { name: 'Bob' });
    dataStore.addOrUpdate('testBoard', newCard);

    const allCards = dataStore.getAll('testBoard');
    expect(allCards.length).toBe(3);
    expect(allCards).toContain(newCard);
  });

  it('should throw an error if adding a card without an id', () => {
    expect(() => {
      dataStore.addOrUpdate('testBoard', { name: 'foo' });
    }).toThrowError();
  });

  it('should throw an error if adding a card with a non-string ID', () => {
    expect(() => {
      dataStore.addOrUpdate('testBoard', { id: 34, name: 'test' });
    }).toThrowError();
  });

  it('should update existing cards if their IDs match (even if they are separate objects)', () => {
    const newCard = makeTestCard('TC02', { name: 'foobar' });
    dataStore.addOrUpdate('testBoard', newCard);

    const updatedCard = dataStore.getById('testBoard', 'TC02') as Card;
    expect(updatedCard.name).toBe('foobar');
  });

  it('should remove an existing card if its phase is changed to archive', () => {
    const newCard = makeTestCard('TC02', { name: 'foobar' }, 'archive');
    dataStore.addOrUpdate('testBoard', newCard);

    const updatedCard = dataStore.getById('testBoard', 'TC02');
    expect(updatedCard).toBeUndefined();
  });

  it('should notify when a card value is changed', () => {
    let notificationCount = 0;
    dataStore.changes('testBoard').subscribe(() => {
      notificationCount += 1;
    });

    const newCard = makeTestCard('TC02', { name: 'foobar' });
    dataStore.addOrUpdate('testBoard', newCard);

    expect(notificationCount).toBe(1);
  });

  it('should send an update change when a card is updated', () => {
    let dataChange: DataChange = null;
    dataStore.changes('testBoard').subscribe(change => {
      dataChange = change;
    });

    const originalCard = exampleCards.find(c => c.id === 'TC02');
    expect(originalCard).toBeDefined();

    const newCard = makeTestCard('TC02', { name: 'foobar' });

    dataStore.addOrUpdate('testBoard', newCard);

    expect(dataChange).toBeTruthy();
    expect(dataChange.changeType).toBe('update');
    expect(dataChange.currentValue).toBe(newCard);
    expect(dataChange.previousValue).toBe(exampleCards.find(c => c.id === 'TC02'));
  });

  it('should send a create change when adding a new card', () => {
    let dataChange: DataChange = null;
    dataStore.changes('testBoard').subscribe(change => {
      dataChange = change;
    });

    const newCard = makeTestCard('TC38', { name: 'a new card!' });
    dataStore.addOrUpdate('testBoard', newCard);

    expect(dataChange).toBeDefined();
    expect(dataChange.changeType).toBe('create');
    expect(dataChange.previousValue).toBeNull();
    expect(dataChange.currentValue).toBe(newCard);
  });

  it('should not send a create change when adding a card with in the archive phase', () => {
    let notificationCount = 0;
    dataStore.changes('testBoard').subscribe(change => {
      notificationCount += 1;
    });

    const newCard = makeTestCard('TC38', { name: 'a new card!' }, 'archive');
    dataStore.addOrUpdate('testBoard', newCard);

    expect(notificationCount).toBe(0);
  });

  it('should send a delete change when updating a card to the archive phase', () => {
    let dataChange: DataChange = null;
    dataStore.changes('testBoard').subscribe(change => {
      dataChange = change;
    });

    const newCard = makeTestCard('TC02', { name: 'foobar' }, 'archive');
    dataStore.addOrUpdate('testBoard', newCard);

    expect(dataChange).toBeTruthy();
    expect(dataChange.changeType).toBe('delete');
    expect(dataChange.currentValue).toBeNull();
    expect(dataChange.previousValue).toBe(exampleCards.find(c => c.id === 'TC02'));
  });

  it('should send update changes for non-card objects', () => {
    let notificationCount = 0;
    let dataChange: DataChange = null;
    dataStore.changes('users').subscribe(change => {
      notificationCount += 1;
      dataChange = change;
    });

    const updatedItem = { ...ITEM_WITH_CURRENT_PHASE, name: 'test' };
    dataStore.addOrUpdate('users', updatedItem);
    expect(notificationCount).toBe(1);
    expect(dataChange.changeType).toBe('update');
    expect(dataChange.previousValue).toBe(ITEM_WITH_CURRENT_PHASE);
    expect(dataChange.currentValue).toBe(updatedItem);
  });


  it('should send create changes for non-card objects', () => {
    let notificationCount = 0;
    let dataChange: DataChange = null;
    dataStore.changes('users').subscribe(change => {
      notificationCount += 1;
      dataChange = change;
    });

    const newItem = { id: 'new1', name: 'test', phase: 'current' };
    dataStore.addOrUpdate('users', newItem);
    expect(notificationCount).toBe(1);
    expect(dataChange.changeType).toBe('create');
    expect(dataChange.previousValue).toBeNull();
    expect(dataChange.currentValue).toBe(newItem);
  });
});
