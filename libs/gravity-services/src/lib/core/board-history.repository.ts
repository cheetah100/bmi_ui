import { refCount, publishLast, map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class BoardHistoryRepository {

  private readonly boardMappings = {
    'pain_points': 'Pain Point',
    'business_outcomes': 'Business Outcome',
    'capabilities': 'Capability',
    'feature_impact_assessments': 'Feature with Change Impacts',
    'features': 'Feature',
    'impacts': 'Impacts',
    'maturity_details': 'BSF Maturity Grade',
    'offers': 'Offer',
    'acquisition_offers': 'Acquisition Offers',
    'product_offers': 'Product Offers',
    'service_offers': 'Service Offers',
    'cdo_milestones': 'CDO Milestones',
    'cdo_workstreams': 'CDO Workstreams'
  };

  constructor(private http: HttpClient) { }

  get(after: string, before: string): Observable<BoardHistory[]> {
    return this.http.get<any>(`/gravity/spring/history/board-history/dbexecute?after=${after}&before=${before}`).pipe(
      map(data => {
        return data.filter(item => item.user !== 'system').map(item => {
          let boardName = (this.boardMappings[item.board] || item.board);
          return {
            category: item.category,
            card: item.card,
            date: new Date(item.occuredTime),
            user: item.user,
            board: boardName,
            fields: item.fields,
            detail: item.detail
          };
        });
      }),
      publishLast(),
      refCount());
  }
}


interface BoardHistory {
  category: string;
  card: string;
  date: Date;
  user: string;
  board: string;
  fields: any;
  detail: string;
}
