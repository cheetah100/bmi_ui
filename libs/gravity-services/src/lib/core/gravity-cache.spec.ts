import { TestBed, fakeAsync, tick } from '@angular/core/testing';
import { gravityTestbed, makeCardJson, TEST_USER } from '../testing/gravity-test-tools';
import { QUARTER_BOARD_CONFIG, QUARTER_CARDS } from '../testing/model-board-configs';
import { GravityCache } from '../core/gravity-cache';
import { Card } from '../core/card';
import { User } from '../core/user.repository';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { Team } from '../core/user.repository';


describe('[GravityCache]', () => {

  describe('For regular boards', () => {
    let cache: GravityCache;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [
          ...gravityTestbed({
            useFakeGravityService: true,
            boards: [
              QUARTER_BOARD_CONFIG,
            ],

            cards: {
              quarters: QUARTER_CARDS
            }
          }),
        ]
      });

      cache = TestBed.get(GravityCache) as GravityCache;
    });

    it('should request cards if the value is not in the cache', fakeAsync(() => {
      let quarterCards: Card[];
      cache.getAll('quarters').subscribe(cards => {
        quarterCards = cards;
      });

      tick();

      expect(quarterCards).toBeDefined();
      expect(quarterCards.length > 0).toBe(true);
      expect(quarterCards.find(q => q.id === 'Q1FY19')).toBeTruthy();
    }));

    it('should fetch a card by Id even if not in cache', fakeAsync(() => {
      let quarter: Card;
      cache.get('quarters', 'Q2FY19').subscribe(q => {
        quarter = q as Card;
      });

      tick();

      expect(quarter).toBeDefined();
      expect(quarter.id).toBe('Q2FY19');
    }));
  });

  describe('Special data types', () => {
    // These tests will use the HttpClient mocking to ensure that the correct
    // requests are being made from the repositories. This isn't an ideal
    // solution ...
    let cache: GravityCache;
    let httpTestingController: HttpTestingController;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [
          ...gravityTestbed({
            useFakeGravityService: false
          }),
        ]
      });

      cache = TestBed.get(GravityCache) as GravityCache;
      httpTestingController = TestBed.get(HttpTestingController) as HttpTestingController;
    });

    afterEach(() => httpTestingController.verify());

    describe('Users', () => {
      it('should fetch users', () => {
        let fetchedUsers: User[];
        cache.getAll('users').subscribe(users => {
          fetchedUsers = users;
        });

        const req = httpTestingController.expectOne('/gravity/spring/user/userdetails');
        expect(req.request.method).toBe('GET');
        req.flush({
          'mock-user': TEST_USER
        });

        expect(fetchedUsers).toBeDefined();
        expect(fetchedUsers.length).toBe(1);
        expect(fetchedUsers[0].id).toBe('mock-user');
      });

      it('should cache users after the first request', () => {
        cache.getAll('users').subscribe();
        const req = httpTestingController.expectOne('/gravity/spring/user/userdetails');
        req.flush({ 'mock-user': TEST_USER });

        let cachedUsers: User[];
        cache.getAll('users').subscribe(users => {
          cachedUsers = users;
        });

        httpTestingController.expectNone('/gravity/spring/user/userdetails');

        expect(cachedUsers).toBeDefined();
        expect(cachedUsers.length).toBe(1);
      });
    });


    describe('Teams', () => {
      const exampleTeams = {
        'administrators': 'Administrators',
        'readonly': 'Read Only Users'
      };

      it('should fetch teams', () => {
        let cachedTeams: Team[];
        cache.getAll('teams').subscribe(teams => cachedTeams = teams);
        const req = httpTestingController.expectOne('/gravity/spring/team');
        req.flush(exampleTeams);

        expect(cachedTeams).toBeDefined();
        expect(cachedTeams.length).toBe(2);
        expect(cachedTeams.find(t => t.id === 'readonly')).toBeDefined();
        expect(cachedTeams.find(t => t.id === 'readonly').title).toBe('Read Only Users');
      });

      it('should cache teams', () => {
        cache.getAll('teams').subscribe();
        const req = httpTestingController.expectOne('/gravity/spring/team');
        req.flush(exampleTeams);

        let cachedTeams: Team[];
        cache.getAll('teams').subscribe(teams => cachedTeams = teams);
        httpTestingController.expectNone('/gravity/spring/team');
        expect(cachedTeams.length).toBe(2);
      });
    });

    // TODO: dashboards
  });
});
