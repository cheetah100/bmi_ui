
import { refCount, publishLast, map, delay, } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Card } from './card';
import { GravityConverter } from './gravity-converter';
import { DataStore } from './data-store';

import { Observable, of, } from 'rxjs';

import { Filter } from '../model/filter';
import { GravityUrlService } from '../services/gravity-url.service';
import { GravityApiCardView } from '../api-types/gravity-api-cardview';
import { ObjectMap } from '@bmi/utils';
import { GravityApiTemplate, GravityApiBoard } from '../api-types/gravity-api-board-config';
import { GravityApiCardBackReferences } from '../api-types/gravity-api-card-back-references';

/**
 * Corresponds to the API output for the consolidated card references API in
 * gravity, but uses Card objects instead.
 */
export interface CardBackReferences {
  referringBoard: string;
  boardAccess: boolean;
  cards: Card[];
}


/** @deprecated */
@Injectable()
export class GravityService {

  constructor(
    private http: HttpClient,
    private gravityConverter: GravityConverter,
    private dataStore: DataStore,
    private urls: GravityUrlService
  ) { }

  getBoards(): Observable<any> {
    const url = this.urls.allBoards()
    return this.http.get(url);
  }

  getCard(boardName: string, cardId: string, validateReferences: boolean = false): Observable<Card> {

    let url = this.urls.board(boardName).card(cardId).baseUrl;
    if (validateReferences) {
      url = url + `?validateReferences=true`;
    }
    const source = this.http.get<GravityApiCardView>(url).pipe(map(item => {
      return this.gravityConverter.fromJson(boardName, item);
    }), publishLast(), refCount());

    source.subscribe(card => {
      this.dataStore.addOrUpdate(boardName, card.clone());
    }, (error) => { });

    return source;
  }

  getCardsById(boardName: string, ids: string[]): Observable<Card[]> {
    const url = this.urls.board(boardName).cardList();
    const source = this.http.post<GravityApiCardView[]>(url, ids).pipe(
      map(data => data.map(item => this.gravityConverter.fromJson(boardName, item))),
      publishLast(),
      refCount());

    source.subscribe(cards => {
      cards.forEach(card => {
        this.dataStore.addOrUpdate(boardName, card.clone());
      });
    }, (error) => { });

    return source.pipe(map(cards => cards.filter(card => card.phase !== 'archive')));
  }

  getCards(boardName: string): Observable<Card[]> {

    const devCache = !!localStorage.getItem('devCardCache');

    const url = this.urls.board(boardName).viewAllCards;
    const cachedData = sessionStorage.getItem('gravityCache:' + url);
    let source;
    if (devCache && !!cachedData) {
      source = of(JSON.parse(cachedData)).pipe(
        delay(1),
        map((data: any) => data.map(item => this.gravityConverter.fromJson(boardName, item))),
        publishLast(),
        refCount(),
      );
      console.log('Getting from cache: ' + url);
    } else {
      source = this.http.get(url).pipe(map((data: any) => {
        if (devCache) {
          try {
            sessionStorage.setItem('gravityCache:' + url, JSON.stringify(data));
          } catch {
            console.log('No more space in session storage. Not caching ' + url);
          }
        }
        return data.map(item => this.gravityConverter.fromJson(boardName, item));
      }), publishLast(), refCount());
    }

    source.subscribe(cards => {
      this.dataStore.set(boardName, cards.map(card => card.clone()));
    }, (error) => { });
    return source.pipe(map((cards: Card[]) => cards.filter(card => card.phase !== 'archive')));
  }


  getCardsFiltered(board: string, filter: Filter): Observable<Card[]> {
    const source = this.http.post<GravityApiCardView[]>(this.urls.cardSearch(board), filter.toJSON()).pipe(
      map(cardData => cardData.map(c => this.gravityConverter.fromJson(board, c)).filter(c => c.phase !== 'archive')),
      publishLast<Card[]>(),
      refCount()
    );

    source.subscribe(cards => {
      cards.forEach(c => this.dataStore.addOrUpdate(board, c.clone()));
    });
    return source;
  }

  /**
   * Not filtering out hidden ('archive') phases.
   *
   * That puts this out of line with other card getters here. Should the frontend be doing that at all?
   */
  getCardSearch(
    board: string,
    view: string = 'all',
    filter: string = null,
  ): Observable<Card[]> {
    return this.http.post<GravityApiCardView[]>(
      this.urls.cardSearch(board, view, filter),
      { view, filter }
    ).pipe(
      map(
        (apiCards: GravityApiCardView[]) => apiCards.map(
          (apiCard: GravityApiCardView) => this.gravityConverter.fromJson(board, apiCard)
        )
      ),
      publishLast<Card[]>(),
      refCount(),
    );
  }

  /**
   * Gets a map of the cards that reference a given card across Gravity
   *
   * For example, for a given project card, this would return all 'child' cards
   * that reference that project (e.g. the Business Outcomes, Investments etc.)
   */
  getReferencingCards(board: string, cardId: string): Observable<CardBackReferences[]> {
    const url = this.urls.getChildrenCardsByReference(board, cardId);
    const source = this.http.get<GravityApiCardBackReferences[]>(url).pipe(
      map(referencingCards => {
        return referencingCards.map(ref => {
          const refBoard = ref.referringBoard;
          return <CardBackReferences>{
            ...ref,
            cards: ref.cards.map(c => this.gravityConverter.fromJson(refBoard, c))
          };
        });
      }),
      publishLast<CardBackReferences[]>(),
      refCount()
    );

    source.subscribe(results => {
      for (const ref of results) {
        ref.cards.forEach(c => this.dataStore.addOrUpdate(ref.referringBoard, c));
      }
    });

    return source;
  }

  getBoardConfig(boardId: string) {
    const source = this.http.get<GravityApiBoard>(this.urls.boardConfig(boardId)).pipe(
      publishLast<GravityApiBoard>(),
      refCount()
    );
    source.subscribe();
    return source;
  }

  getBoardConfigs(): Observable<ObjectMap<GravityApiBoard>> {
    const devCache = !!localStorage.getItem('devCardCache');
    const url = this.urls.allBoardConfigs();
    const cachedData = sessionStorage.getItem('gravityCache:' + url);
    let source;
    if (devCache && !!cachedData) {
      source = of(JSON.parse(cachedData)).pipe(
        delay(1),
        map((data: any) => data),
        publishLast(),
        refCount(),
      );
      console.log('Getting from cache: ' + url);
    } else {
      source = this.http.get(url).pipe(map((data: any) => {
        if (devCache) {
          try {
            sessionStorage.setItem('gravityCache:' + url, JSON.stringify(data));
          } catch {
            console.log('No more space in session storage. Not caching ' + url);
          }
        }
        return data;
      }), publishLast(), refCount());
    }

    source.pipe(
        publishLast<ObjectMap<GravityApiBoard>>(),
        refCount()
    );
    // const source = this.http.get<ObjectMap<GravityApiBoard>>(this.urls.allBoardConfigs()).pipe(
    // );

    source.subscribe();
    return source;
  }

  invalidateBoardConfigs() {
    // Hack to force reload of board configs
    const devCache = !!localStorage.getItem('devCardCache');
    if (devCache) {
      const url = this.urls.allBoardConfigs();
      sessionStorage.removeItem('gravityCache:' + url);
    }
  }

  getViewTemplate(boardId: string, viewId: string): Observable<GravityApiTemplate> {
    const url = this.urls.boardViewTemplate(boardId, viewId);
    const source = this.http.get<GravityApiTemplate>(url).pipe(
      publishLast<GravityApiTemplate>(),
      refCount()
    );

    source.subscribe();
    return source;
  }

  addMultipleCards(
    boardName: string,
    data: any[]
  ) {
    const payload = {
      items: data.map(fields => {
        return {
          template: boardName,
          fields: fields
        };
      })
    };

    return this.saveMultipleCardsToStore(boardName, payload);
  }

  updateMultipleCards(
    boardName: string,
    data: any[],
  ) {
    const payload = {
      items: data.map(fields => {
        const clone = Object.assign({}, fields);
        return {
          id: fields.id,
          path: `/board/${boardName}/cards/${fields.id}`,
          template: boardName,
          fields: clone
        };
      })
    };

    return this.saveMultipleCardsToStore(boardName, payload);
  }

  saveMultipleCardsToStore(boardId: string, payload: any): Observable<any[]> {
    const url = this.urls.board(boardId).allCards;
    const source = <Observable<any[]>>this.http.post(url, payload).pipe(map((items: any) => {
      items.filter(item => item.success).forEach(item => {
        item.card = this.gravityConverter.fromJson(boardId, item.card);
      });
      return items;
    }), publishLast(), refCount());

    source.subscribe(items => {
      items.filter(item => item.success).map(item => item.card).forEach(card => {
        this.dataStore.addOrUpdate(boardId, card.clone());
      });
    }, (error) => { });
    return source;
  }

  bulkEditCards(
    boardName: string,
    fields: any,
    cardIds: string[]
  ) {

    const payload = {
      ids: cardIds,
      fields: fields
    };
    const url = this.urls.board(boardName).bulkUpdateCards;
    const source = this.http.put(url, payload).pipe(
      map(failedIds => {
        const updated = { successes: [], failures: [] };
        cardIds.forEach(id => {
          if (!!failedIds[id]) {
            updated.failures.push({ id: id, reason: failedIds[id] });
          } else {
            updated.successes.push(id);
          }
        });
        return updated;
      }),
      publishLast(),
      refCount());

    source.subscribe((updated: any) => {
      this.getCardsById(boardName, updated.successes);
    }, (error) => { });

    return source;

  }

  addCard(
    boardName: string,
    card: any,
    phase: string | null = null,
  ): Observable<Card> {

    const fields = this.gravityConverter.toJson(boardName, card);

    const payload = {
      template: boardName,
      fields: fields,
    };

    if (phase) {
      payload['phase'] = phase;
    } else {
      delete payload['phase'];
    }

    const url = this.urls.board(boardName).cards();
    const source = this.http.post<GravityApiCardView>(url, payload).pipe(map(item => {
      return this.gravityConverter.fromJson(boardName, item);
    }), publishLast(), refCount());

    source.subscribe(newCard => {
      this.dataStore.addOrUpdate(boardName, newCard.clone());
    }, (error) => { });

    return source;
  }

  updateCard(
    boardName: string,
    card: any,
    phase: string = null,
    changeReasons: { [property: string]: string }
  ): Observable<Card> {

    const fields = this.gravityConverter.toJson(boardName, card);
    const transformed = Object.assign({}, fields);

    if (phase) {
      transformed.phase = phase;
    }

    if (changeReasons) {
      transformed.reasons = changeReasons;
    }
    let payload = transformed;

    if (phase === 'archive') {
      payload = {
        reasons: transformed.reasons,
        phase: 'archive'
      };
    }

    const url = this.urls.board(boardName).card(card.id).baseUrl;
    const source = this.http.put<GravityApiCardView>(url, payload).pipe(map(item => {
      return this.gravityConverter.fromJson(boardName, item);
    }), publishLast(), refCount());

    source.subscribe(newCard => {
      this.dataStore.addOrUpdate(boardName, newCard.clone());
    }, (error) => { });

    return source;
  }

  move(cardId: string, boardName: string, newPhase: string): Observable<Card> {

    const card = this.dataStore.getById(boardName, cardId) as Card;
    if (card === null || card === undefined) {
      throw Error('Could not find the card in the DataStore.');
    }

    const url = this.urls.moveCard(boardName, cardId)
    const source = this.http.get<GravityApiCardView>(url).pipe(map(item => {
      card.phase = item.phase;
      return card;
    }), publishLast(), refCount());

    // tslint:disable-next-line:no-shadowed-variable
    source.subscribe(item => {
      card.phase = item.phase;
      if (newPhase === 'archive') {
        this.dataStore.remove(boardName, cardId);
      } else {
        this.dataStore.addOrUpdate(boardName, card.clone());
      }
    }, (error) => { });

    return source;
  }

  // Still needs to be implemented on the backend
  moveWithReason(cardId: string, boardName: string, newPhase: string, reason: string): Observable<Card> {

    const card = this.dataStore.getById(boardName, cardId);
    if (!card) {
      throw new Error(('Could not find the card in the DataStore, so unable to moveWithReason.'));
    }

    const url = this.urls.moveCard(boardName, cardId)

    const source = this.http.post(url, { phase: newPhase, reasons: reason }).pipe(
      map((item: any) => {
        card.phase = item.phase;
        return card;
      }), publishLast(), refCount());

    source.subscribe(card => {
      this.dataStore.addOrUpdate(boardName, card.clone());
    }, (error) => { });

    return source;
  }

  moveWithReasons(cardId: string, boardName: string, newPhase: string, reason: string): Observable<void> {

    const url = this.urls.moveCard(boardName, cardId)

    const source = this.http.post(url, { phase: newPhase, reasons: reason }).pipe(
      map((item: any) => { }), publishLast(), refCount());

    return source;
  }

  moveMultipleCards(boardName: string, payload: any): Observable<void> {

    const url = this.urls.archiveCards(boardName);
    const source = <Observable<void>>this.http.post(url, payload).pipe(map(response => {
      return;
    }), publishLast(), refCount());

    return source;
  }

  getVisualizations(appId: string) {
    const url = this.urls.appUrl(appId);
    return this.http.get(url).pipe(map(response => {
      return response;
    }));
  }

  getBoardFilters(boardId): Observable<any> {
    const url = this.urls.board(boardId).filters();
    return this.http.get(url);
  }

  // app: should this be separate?
  //  [Sam]: yes, they probably should. This service has too many
  //  responsibilities already and needs to be split up.
  getApps(): Observable<ObjectMap<string>> {
    return this.http.get(this.urls.listApps()) as Observable<ObjectMap<string>>;
  }

  getAppConfig(app: string) {
    return this.http.get(this.urls.app(app).baseUrl);
  }

}
