export interface DataChange {
  previousValue: any,
  currentValue: any,
  dataType: string,
  changeType: 'delete' | 'create' | 'update'
}