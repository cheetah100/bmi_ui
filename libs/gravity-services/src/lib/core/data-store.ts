import { filter } from 'rxjs/operators';
import { Card } from './card';
import { DataChange } from './data-change';
import { Subject, Observable } from 'rxjs';
import { createGroupedMapFrom } from '@bmi/utils';


export interface DataStoreItem {
  id: string;
  phase?: string;
}

/**
 * A definition for a data store index.
 *
 * This defines the type, key function and filter function needed to perform an
 * efficient, cached secondary key lookup on the data store (i.e. find an item
 * in O(1) time using a field other than id).
 *
 *  See the JSDoc for the DataStore#lookupBy method for more details.
 */
export interface DataStoreIndex<T extends DataStoreItem = DataStoreItem> {
  /**
   * The type (e.g. a board) of data item to index
   */
  type: string;

  /**
   * A function that will provide the secondary key value for an item
   *
   * This is used to build the index.
   */
  key: (item: T) => string;

  /**
   * An optional filter function, which will filter the items before indexing.
   */
  filter?: (item: T) => boolean;
}


export class DataStore {
  // this stores the actual data
  private data: Map<string, { [id: string]: any }> = new Map();
  private dataTypesSet: { [dataType: string]: boolean } = {};

  private changesSubject = new Subject<DataChange>();

  private indexes = new Map<DataStoreIndex, Map<string, DataStoreItem[]>>();

  constructor() {
    this.changesSubject.subscribe(change => {
      this.invalidateIndexes(change.dataType);
    });
  }

  getById(type: string, id: string): any {

    if (!this.data.has(type)) {
      return undefined;
    }

    return this.data.get(type)[id];
  }

  getAll<T extends DataStoreItem = any>(type: string): T[] {
    if (!this.data.has(type)) {
      return [];
    }

    const data = this.data.get(type);
    return Object.keys(data)
      .filter(key => {
        // TODO: this seems very hacky, and the behaviour seems a little bit odd.
        return (
          !this.isGravityCard(type) || (data[key]['phase'] && data[key]['phase'] !== 'archive')
        );
      })
      .map(key => data[key]);
  }

  /**
   * Performs an indexed lookup of a data item
   *
   * This can be used to create a secondary index to look up items by another
   * property, or even on another related card.
   *
   * Because these lookups are mostly being done from the model classes, it was
   * a little tricky to figure out a nice way to set up these indexes to avoid
   * relying on global state while still keeping the interface easy to use.
   *
   * The index info object is stateless, and stores the data type (i.e. board),
   * a key function and an optional filter.
   *
   * The object identity of the index info object is used to locate the index
   * within this data store index -- reuse the same config object each time you
   * call lookupBy, otherwise it will just create new indexes!
   *
   * The index is created on-demand, and is invalidated by any change events for
   * that data type.
   *
   * The index info must provide a key function, which returns a string for a
   * given item. This is the value you want to look up.
   *
   * The index info may also provide a filter function, which is applied to all
   * items before the index is built. You can use this to exclude anything that
   * isn't valid to include in the index, or where the key wouldn't be defined.
   *
   * @param indexInfo - the identity of the index, providing type and key/filter functions
   * @param value - the value to look up in the index
   * @returns a list of the matching items, or an empty array if nothing was found.
   */
  lookupBy<T extends DataStoreItem>(indexInfo: DataStoreIndex<T>, value: string): T[] {
    if (!this.indexes.has(indexInfo)) {
      let items = this.getAll<T>(indexInfo.type);
      if (indexInfo.filter) {
        items = items.filter(indexInfo.filter);
      }
      const idx = createGroupedMapFrom(items, indexInfo.key);
      this.indexes.set(indexInfo, idx);
    }
    const index = this.indexes.get(indexInfo);
    const foundItems = index.get(value) || [];
    return foundItems as T[];
  }

  remove(type: string, id: string) {
    if (this.data.has(type)) {
      const data = this.data.get(type);
      if (data[id]) {
        this.changesSubject.next({
          changeType: 'delete',
          dataType: type,
          previousValue: data[id],
          currentValue: null
        });
        delete data[id];
      }
    }
  }

  private invalidateIndexes(type: string) {
    const toInvalidate: DataStoreIndex[] = [];
    for (const index of this.indexes.keys()) {
      if (index.type === type) {
        toInvalidate.push(index);
      }
    }
    toInvalidate.forEach(idx => this.indexes.delete(idx));
  }

  // invalidate cache for one item. Created for removing all cached cards for a
  // board/template that has been altered by the admin user
  removeAll(type: string): void {
    if (this.data.has(type)) {
      this.data.delete(type);
    }
  }

  addOrUpdate(type: string, item: any) {
    let data = {};
    if (!this.data.has(type)) {
      this.data.set(type, data);
    } else {
      data = this.data.get(type);
    }

    if (!item.id || typeof (item.id) !== 'string') {
      throw new Error('Invalid item id');
    }

    const isGravityCard = this.isGravityCard(type);

    const previous = data[item.id];
    let isModified;
    if (previous) {
      // A new item got update
      //
      // update any relationships
      // emit the update or delete (delete for archive phase) event
      // add it to the store
      if (isGravityCard) {
        const itemCard = <Card>item;
        const previousCard = <Card>previous;
        if (itemCard.phase === 'archive') {
          this.remove(type, item.id);
        } else {
          // Checking if the value has been updated is done by comparing the modification timestamp.
          // TODO: Some cards have an undefined timestamp, so if the old card doesn't have a timestamp, assume modified.
          // Maybe we should be doing more robust change detection here too.
          isModified = itemCard.hasChangedFrom(previousCard);
          if (isModified) {
            data[item.id] = item;
            this.changesSubject.next({
              changeType: 'update',
              dataType: type,
              previousValue: previous,
              currentValue: item
            });
          }
        }
      } else if (previous.constructor.name === 'Object') {
        // TODO: We should really be doing a deep comparison on the object to check if it has changed
        // but at the time of implementing this, the only objects we had that weren't gravity cards are
        // teams and users, which only get refreshed on the user maintain tab. So to save time we can implement this
        // at another stage.
        isModified = true;
        if (isModified) {
          data[item.id] = item;
          this.changesSubject.next({
            changeType: 'update',
            dataType: type,
            previousValue: previous,
            currentValue: item
          });
        }
      } else {
        throw new Error('The datastore doesn\'t know how to handle this object type');
      }
    } else {
      // A new item got added
      //
      // update any relationships
      // emit the add event
      // add it to the store
      isModified = true;
      if (isModified) {
        data[item.id] = item;
      }

      if ((isGravityCard && item.phase !== 'archive') || !isGravityCard) {
        this.changesSubject.next({
          changeType: 'create',
          dataType: type,
          previousValue: null,
          currentValue: item
        });
      }
    }
  }

  set(type: string, items: any[]) {
    this.dataTypesSet[type] = true;
    const data = {};
    items.forEach(item => {
      if (!item.id) {
        throw new Error('All items require an id');
      }
      data[item.id] = item;
    });
    this.data.set(type, data);
  }


  changes(type: string): Observable<DataChange> {
    return this.changesSubject.asObservable().pipe(filter(change => change.dataType === type));
  }

  hasAll(type: string): boolean {
    return this.dataTypesSet.hasOwnProperty(type);
  }

  private isGravityCard(type: string) {
    return ['users', 'teams', 'dashboards'].indexOf(type) === -1;
  }
}

