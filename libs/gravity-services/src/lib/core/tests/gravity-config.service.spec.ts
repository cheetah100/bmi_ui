import { TestBed } from '@angular/core/testing';

import { GravityConfigService } from '../gravity-config.service';
import { MOCK_BOARD_CONFIGS } from './mock-board-configs';
import { Card } from '../card';
import { simpleGravitySetup } from '../../testing/gravity-test-tools';
import { QUARTER_BOARD_CONFIG } from '../../testing/model-board-configs';


abstract class Quarter extends Card {
  constructor() {super();}
}

const TEST_BOARDS = [
  ...Object.values(MOCK_BOARD_CONFIGS),
  QUARTER_BOARD_CONFIG
]

describe('[GravityConfigService]', () => {
  let configService: GravityConfigService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: simpleGravitySetup(TEST_BOARDS, {
        customCardModelClasses: {
          quarters: Quarter
        }
      })
    });
    configService = TestBed.get(GravityConfigService) as GravityConfigService;
  });

  describe('Basic functionality', () => {
    it(`should get registered boards`, () => {
      expect(configService.getRegisteredBoards()).toContain('basic-service-functions');
      expect(configService.getRegisteredBoards()).toContain('process-bundles');
    });

    it(`should ignore boards without templates`, () => {
      expect(configService.getRegisteredBoards()).not.toContain('board-without-template');
    });

    it(`should getBoardConfig() correctly`, () => {
      const config = configService.getBoardConfig('basic-service-functions');
      expect(config.boardName).toBe('basic-service-functions');
      expect(config.name).toBe('Basic Service Functions');
    });

    it(`should getBoardClass() that extends card`, () => {
      const boardClass = configService.getBoardClass('basic-service-functions');
      const prototype = Object.getPrototypeOf(boardClass);
      expect(prototype).toBe(Card);

      const card: Card = new boardClass();
      expect(card.getConfig().boardName).toBe('basic-service-functions');
    });

    it(`should createCard() correctly`, () => {
      const card = configService.createCard('basic-service-functions');
      expect(card.getConfig().boardName).toBe('basic-service-functions');
    });
  });

  describe('with Custom Model Classes', () => {
    it('should get the custom boardClass for quarters', () => {
      expect(configService.getBoardClass('quarters')).toBe(Quarter);
    });
  });
});

