import {
  Permissions,
  expandPermissions,
  maxPermission,
  minPermission,
  narrowPermissions
} from '../permissions';


describe('Find Min Permission', () => {
  it('should pick the lowest permission', () => {
    expect(minPermission('READ', 'WRITE')).toEqual('READ');
    expect(minPermission('READ', 'ADMIN')).toEqual('READ');
    expect(minPermission('ADMIN', 'WRITE')).toEqual('WRITE');
  });

  it('should treat null and undefined permissions as minimum', () => {
    expect(minPermission('ADMIN', null)).toEqual(null);
    expect(minPermission(null, 'ADMIN')).toEqual(null);
    expect(minPermission(null, 'READ')).toEqual(null);
    expect(minPermission('READ', undefined)).toBe(undefined);
  });

  it('should handle equal parameters', () => {
    expect(minPermission('READ', 'READ')).toEqual('READ');
    expect(minPermission('ADMIN', 'ADMIN')).toEqual('ADMIN');
    expect(minPermission(null, null)).toEqual(null);
    expect(minPermission(undefined, null)).toBeFalsy();
  });
});


describe('Find Max Permission', () => {
  it('should pick the highest permission', () => {
    expect(maxPermission('READ', 'WRITE')).toEqual('WRITE');
    expect(maxPermission('READ', 'ADMIN')).toEqual('ADMIN');
    expect(maxPermission('ADMIN', 'WRITE')).toEqual('ADMIN');
  });

  it('should treat null and undefined permissions as minimum', () => {
    expect(maxPermission('ADMIN', null)).toEqual('ADMIN');
    expect(maxPermission(null, 'ADMIN')).toEqual('ADMIN');
    expect(maxPermission(null, 'READ')).toEqual('READ');
  });
});


describe('Narrow Permissions', () => {
  it('should handle empty permissions objects', () => {
    expect(narrowPermissions({}, {})).toEqual({});
  });

  it('should treat null objects the same as {}', () => {
    expect(narrowPermissions({'users': 'WRITE'}, null)).toEqual({});
    expect(narrowPermissions(null, {'users': 'WRITE'})).toEqual({});
    expect(narrowPermissions(null, null)).toEqual({});
  });

  it('should take the min permission if the team is in both objects', () => {
    expect(narrowPermissions({'devs': 'ADMIN'}, {'devs': 'READ'})).toEqual({'devs': 'READ'});
    expect(narrowPermissions({'users': 'WRITE'}, {'users': 'READ'})).toEqual({'users': 'READ'});
  });

  it('should exclude entries that are only in one object', () => {
    expect(narrowPermissions({'devs': 'ADMIN'}, {})).toEqual({});
    expect(narrowPermissions({}, {'devs': 'ADMIN'})).toEqual({});
    expect(narrowPermissions({'users': 'READ'}, {'users': 'READ', 'devs': 'ADMIN'})).toEqual({'users': 'READ'});
    expect(narrowPermissions({'users': 'READ', 'devs': 'ADMIN'}, {'users': 'READ'})).toEqual({'users': 'READ'});
  });
});


describe('Expand permissions', () => {
  it('should handle empty permissions objects', () => {
    expect(expandPermissions({}, {})).toEqual({});
  });

  it('should treat null permissions objects the same as {}', () => {
    expect(expandPermissions(null, null)).toEqual({});
    expect(expandPermissions({'devs': 'READ'}, null)).toEqual({'devs': 'READ'});
    expect(expandPermissions(null, {'devs': 'READ'})).toEqual({'devs': 'READ'});
  });

  it('should clone the objects if one of the parameters is null', () => {
    const perms: Permissions = {'devs': 'ADMIN'};
    expect(expandPermissions(perms, null)).not.toBe(perms);
  });

  it('should take the max permission if the team is in both objects', () => {
    expect(expandPermissions({'devs': 'READ'}, {'devs': 'ADMIN'})).toEqual({'devs': 'ADMIN'});
    expect(expandPermissions(
      {'devs': 'READ', 'users': 'READ'},
      {'devs': 'ADMIN', 'users': 'WRITE'})
    ).toEqual({'devs': 'ADMIN', 'users': 'WRITE'});
  });

  it('should include entries that are only in one object', () => {
    expect(expandPermissions({'devs': 'WRITE'}, {'users': 'READ'}))
      .toEqual({'devs': 'WRITE', 'users': 'READ'});
  });
});
