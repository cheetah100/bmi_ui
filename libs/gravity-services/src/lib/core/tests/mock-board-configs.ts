import { BoardConfigBuilder } from '../../testing/board-config-builder';

export const MOCK_BOARD_CONFIGS = {
    'board-without-template': new BoardConfigBuilder()
        .id('board-without-template')
        .finish(),

    'process-bundles': new BoardConfigBuilder()
        .id('process-bundles')
        .permissions({
            'system': 'WRITE',
            'admin': 'ADMIN',
            'administrators': 'ADMIN',
            'readonly': 'READ'
        })
        .addField('name', f => f.label('Name').control('INPUT').type('STRING'))
        .addField('title', f => f.label('Title'))
        .addField('basic_service_functions', field => {
            field.type('LIST')
                .control('DROPDOWN')
                .optionlist('basic_service_functions')
                .required();
        })
        .addField('show_in_change_heatmap', f => f.type('BOOLEAN').control('DROPDOWN').required())
        .addField('show_in_maturity_heatmap', f => f.type('BOOLEAN').control('DROPDOWN').required())
        .addPhase('unpublished')
        .addPhase('current')
        .addPhase('archive', p => p.invisible())
        .addPhase('published')
        .finish(),

    'basic-service-functions': new BoardConfigBuilder()
        .id('basic-service-functions')
        .name('Basic Service Functions')
        .permissions({
            'system': 'WRITE',
            'admin': 'ADMIN',
            'administrators': 'ADMIN',
            'readonly': 'READ'
        })
        .addField('name', f => f.label('Name').required())
        .addField('title', f => f.label('Label').required())
        .addPhase('current')
        .addPhase('archive', p => p.invisible())
        .finish()
};
