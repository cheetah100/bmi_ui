import { mapObjectValues, sanitizeId } from '@bmi/utils';
import { GravityApiView, GravityApiViewField } from '../api-types/gravity-api-view';

export class ViewField {
  index: number = null;
  ui: { [key: string]: any } = {};

  constructor(
    public id: string,
    public fieldPath: string,
    public label: string = null
  ) { }
}


/**
 * Represents a Gravity View.
 *
 * This is intended to abstract away some of the quirks of creating views in
 * Gravity, such as providing the fields as as array rather than a map, for more
 * convenient editing.
 *
 * You can create a View instance from a Gravity View JSON config using the
 * View.fromJson(...) static method.
 *
 * The View object has a .toJSON() method which will produce the Gravity JSON
 * from this object.
 */
export class View {
  fields: ViewField[] = [];
  ui: { [key: string]: any } = {};

  /**
   * Creates a View object from the Gravity view JSON config.
   *
   * The fields will be sorted by their index property.
   */
  static fromJSON(viewJson: GravityApiView): View {
    const view = new View(viewJson.boardId, viewJson.name, viewJson.id, viewJson.type);
    const fields = mapObjectValues(viewJson.fields, f => f).sort((a, b) => a.index - b.index);
    fields.forEach(f => {
      const vf = new ViewField(f.id, f.name, f.label);
      vf.index = f.index;
      Object.assign(vf.ui, f.ui);
      view.fields.push(vf);
    });
    return view;
  }

  constructor(
    public boardId: string,
    public name: string,
    public id: string = null,
    public type: string = null,
  ) { }

  /**
   * Add a field to the view.
   *
   * This supports dotted paths, and can let Gravity handle this, or add config
   * to the UI section to resolve the field on the front-end.
   *
   * The field will be added to the end of the list.
   *
   * @param id - the ID for the field. If unspecified, this will be derived
   *     automatically from the fieldPath.
   * @param fieldPath - the path to the value on the card. This can be a dotted
   *     path, which can be resolved in Gravity, or on the front-end.
   * @param resolveOnFrontEnd - if true, this will add the field property to the
   *     UI data so that tables resolve this field on the front-end
   */
  addField(id: string, fieldPath: string, resolveOnFrontEnd = false): ViewField {
    const fieldId = sanitizeId(id || fieldPath);
    const field = new ViewField(fieldId, fieldPath);

    if (resolveOnFrontEnd) {
      field.ui.field = fieldPath;
    }

    this.fields.push(field);
    return field;
  }

  toJSON(): GravityApiView {
    return {
      id: this.id,
      boardId: this.boardId,
      name: this.name,
      type: this.type,
      fields: this.makeFieldsJson(),
      ui: this.ui
    };
  }

  private makeFieldsJson(): { [id: string]: GravityApiViewField } {
    const fieldObject = {};
    this.fields.forEach((field, index) => {
      field.index = index;
      fieldObject[field.id] = {
        id: field.id,
        name: field.fieldPath,
        label: field.label,
        ui: field.ui,
        // We store the fields as an array to make reordering easier, so fix up
        // the index now so that it corresponds with the field array.
        index,
      };
    });
    return fieldObject;
  }
}


