import { FieldPath } from './field-path';

export interface PathResolver<T extends FieldPath = FieldPath> {
  /**
   * A display name of the data source for this path resolver.
   *
   * For a board data source, this should be the board name; for a transform
   * result this would be the name of the transform.
   *
   * This should just be treated as a display value for convenience, not an ID
   * with any special meaning. A specific path resolver implementation may
   * expose implementation-specific properties, and you should use those
   * instead.
   */
  dataSourceName: string;

  /**
   * Look up a field path.
   *
   * If the path is empty, this will return null.
   */
  resolve(path: string): T;

  /**
   * Finds the children of a specified field path.
   *
   * If the supplied fieldPath is null, return all of the top-level paths.
   */
  getChildPaths(fieldPath: T): T[];
}


export class PathResolutionError extends Error {
  constructor(message: string) {
    super(message);
    Object.setPrototypeOf(this, PathResolutionError.prototype);
  }
}
