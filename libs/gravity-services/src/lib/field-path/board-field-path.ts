import { FieldPath } from './field-path';
import { BoardConfig, Field } from '../core/board-config';
import { GravityConfigService } from '../core/gravity-config.service';
import { PathResolver, PathResolutionError } from './path-resolver';

import { mapObjectValues } from '@bmi/utils';

/**
 * A field path implementation that provides access to Gravity board and field
 * configs.
 *
 * This implementation of field paths for a board is intended to provide a
 * better experience when using the Path Picker UI components, and to also
 * make it easier to validate and work with paths among Gravity boards.
 *
 * The FieldPath interface exposes the higher-level functionality, but this
 * class provides access to the board and field configs.
 *
 * The previous FieldPath implementation had a separate class for representing
 * each segment of the path. This implementation takes a different strategy
 * and each segment is represented by its own field path.
 *
 * This 'recursive' approach means that any of the segments can be used as
 * valid field paths to browse up the tree, and it makes it easier to expand
 * the sub-paths below any given path.
 *
 * This object only represents the path; it doesn't have any resolution logic --
 * that's been separated into the `BoardPathResolver` object.
 */
export class BoardFieldPath implements FieldPath {

  // TODO: can a base class be extracted here that handles the `parent` logic
  // and error handling?

  public readonly path: string;
  public readonly segments: BoardFieldPath[];
  public readonly rootBoard: BoardConfig;
  private readonly _invalidityReason: string = null;

  constructor(
    public readonly fieldName: string,
    public readonly boardConfig: BoardConfig,
    public readonly fieldConfig: Field,
    public readonly parent: BoardFieldPath,
    invalidityReason: string = null
  ) {
    if (parent) {
      this.path = parent.path + '.' + fieldName;
      this.segments = [...parent.segments, this];
      this.rootBoard = parent.rootBoard;
    } else {
      this.path = fieldName;
      this.segments = [this];
      this.rootBoard = boardConfig;
    }
    this._invalidityReason = invalidityReason;
  }

  toString() {
    return this.path;
  }

  get isReferenceField(): boolean {
    return this.fieldConfig && (!!this.fieldConfig.referenceField || !!this.fieldConfig.optionlist);
  }

  get isMetadata(): boolean {
    return this.fieldConfig && !!this.fieldConfig.isMetadata;
  }

  /**
   * Does this path contain any fields with multiple values?
   *
   * In Gravity terms, this would mean a field with a type of LIST.
   */
  get isMultiValue(): boolean {
    return this.segments.some(f => f.isListField);
  }

  get isListField(): boolean {
    return this.fieldConfig && this.fieldConfig.type === 'LIST';
  }

  /**
   * Gets the optionlist board ID if this is a reference field.
   *
   * If this isn't a reference field, this will return null.
   */
  get targetBoard(): string {
    return this.fieldConfig && this.fieldConfig.optionlist;
  }

  get boardId(): string {
    return this.boardConfig ? this.boardConfig.id : null;
  }

  get gravityType(): string {
    return this.fieldConfig && this.fieldConfig.type;
  }

  get invalidityReason(): string {
    return this._invalidityReason || (this.parent ? this.parent.invalidityReason : null);
  }

  get isValid(): boolean {
    return !this.invalidityReason;
  }
}


function makeInvalidPath(fieldName: string, invalidityReason: string, parent: BoardFieldPath, boardConfig: BoardConfig = null) {
  // Including the board config here so that an invalid first segment will still
  // preserve the root board config.

  // We want an invalid fieldpath to propagate the error message from the first
  // erroneous segment -- all subsequent path segments cannot be validated so
  // only the first reason is meaningful.
  const isParentValid = !parent || parent.isValid;
  return new BoardFieldPath(fieldName, boardConfig, null, parent, isParentValid ? invalidityReason : null);
}


export class BoardPathResolver implements PathResolver<BoardFieldPath> {

  constructor(
    public readonly boardId: string,
    private readonly configService: GravityConfigService
  ) { }

  // user may edit boardConfig so not storing it here
  get boardConfig(): BoardConfig {
    return this.configService.getBoardConfig(this.boardId);
  }

  get dataSourceName() {
    return this.boardConfig.name;
  }

  resolve(path: string): BoardFieldPath {
    const splitPath = path.split('.');
    if (splitPath[0]) {
      let fieldPath = this.lookupField(this.boardConfig, splitPath[0]);
      for (let i = 1; i < splitPath.length; i++) {
        fieldPath = this.lookupSubField(fieldPath, splitPath[i]);
      }

      return fieldPath;
    } else {
      return null;
    }
  }

  getChildPaths(fieldPath: BoardFieldPath): BoardFieldPath[] {
    if (fieldPath && !fieldPath.isReferenceField) {
      return [];
    } else {
      const boardConfig = (fieldPath && fieldPath.isReferenceField) ? this.lookupBoard(fieldPath.targetBoard) : this.boardConfig;
      return mapObjectValues(boardConfig.fields, field => this.lookupField(boardConfig, field.name, fieldPath));
    }
  }

  private lookupField(boardConfig: BoardConfig, fieldName: string, parent: BoardFieldPath = null): BoardFieldPath {
    const fieldConfig = boardConfig.fields[fieldName];
    if (fieldConfig) {
      return new BoardFieldPath(fieldConfig.name, boardConfig, fieldConfig, parent);
    } else {
      return makeInvalidPath(fieldName, `Field '${fieldName}' does not exist`, parent, boardConfig);
    }
  }

  private lookupSubField(fieldPath: BoardFieldPath, fieldName: string): BoardFieldPath {
    try {
      if (!fieldPath.isValid) {
        throw new PathResolutionError(`Parent field is invalid`);
      }
      if (!fieldPath.isReferenceField) {
        throw new PathResolutionError(`Cannot lookup '${fieldName}' as it's not a reference field (i.e. has no optionlist)`);
      }
      return this.lookupField(this.lookupBoard(fieldPath.targetBoard), fieldName, fieldPath);
    } catch (error) {
      if (error instanceof PathResolutionError) {
        return makeInvalidPath(fieldName, error.message, fieldPath);
      } else {
        throw error;
      }
    }
  }

  private lookupBoard(boardId: string) {
    try {
      return this.configService.getBoardConfig(boardId);
    } catch (error) {
      throw new PathResolutionError(`Invalid board ${boardId}`);
    }
  }
}
