import { TestBed } from '@angular/core/testing';
import { simpleGravitySetup } from '../testing/gravity-test-tools';
import { BoardConfigBuilder } from '../testing/board-config-builder';
import { GravityConfigService } from '../core/gravity-config.service';

import { BoardPathResolver } from '../field-path/board-field-path';


const TEST_BOARDS = [
  new BoardConfigBuilder()
    .id('stoplights')
    .name('Stoplights')
    .addField('name')
    .addField('color')
    .finish(),

  new BoardConfigBuilder()
    .id('board-with-regular-fields')
    .addField('test1')
    .addField('test2', f => f.type('NUMBER'))
    .finish(),

  new BoardConfigBuilder()
    .id('board-with-reference-fields')
    .addField('name')
    .addField('stoplight', f => f.optionlist('stoplights'))
    .addField('test', f => f.optionlist('board-with-regular-fields'))
    .addField('recursive', f => f.optionlist('recursive-board'))
    .addField('plainField')
    .finish(),

  new BoardConfigBuilder()
    .id('board-with-no-fields')
    .defaultTemplate(() => { })
    .finish(),

  new BoardConfigBuilder()
    .id('recursive-board')
    .addField('name')
    .addField('deeper', f => f.optionlist('recursive-board'))
    .addField('stoplight', f => f.optionlist('stoplight'))
    .finish()
];


function setUpTests() {
  TestBed.configureTestingModule({
    providers: [
      ...simpleGravitySetup(TEST_BOARDS)
    ],
  });
  const configService = TestBed.get(GravityConfigService) as GravityConfigService;
  return {
    configService,
    makeResolver: (board: string) => new BoardPathResolver(board, configService)
  };
}


describe('[BoardPathResolver].resolve()', () => {
  let configService: GravityConfigService;
  let makeResolver: (board: string) => BoardPathResolver;
  beforeEach(() => ({ configService, makeResolver } = setUpTests()));

  it('should give the board name as the dataSourceName', () => {
    expect(makeResolver('stoplights').dataSourceName).toBe('Stoplights');
  });

  it('should provide access to the current board config', () => {
    expect(makeResolver('stoplights').boardConfig).toBe(configService.getBoardConfig('stoplights'));
  });

  it('should resolve simple extant paths', () => {
    const resolver = makeResolver('stoplights');
    const fieldPath = resolver.resolve('color');

    expect(fieldPath).toBeDefined();
    expect(fieldPath.isValid).toBeTruthy();
    expect(fieldPath.path).toBe('color');
  });

  it('should resolve optionlist paths', () => {
    const resolver = makeResolver('board-with-reference-fields');
    const path = resolver.resolve('stoplight.color');
    expect(path.isValid).toBeTruthy();
    expect(path.segments.length).toBe(2);
  });


  it('should handle really long, recursive paths', () => {
    const resolver = makeResolver('recursive-board');
    const path = resolver.resolve('deeper.deeper.deeper.deeper.deeper.deeper.name');
    expect(path.segments.length).toBe(7);
  });
});


describe('BoardFieldPath', () => {
  let configService: GravityConfigService;
  let makeResolver: (board: string) => BoardPathResolver;
  beforeEach(() => ({ configService, makeResolver } = setUpTests()));


  describe('.gravityType', () => {
    it('should get the gravity type of the field', () => {
      const resolver = makeResolver('board-with-regular-fields');
      expect(resolver.resolve('test1').gravityType).toBe('STRING');
      expect(resolver.resolve('test2').gravityType).toBe('NUMBER');
    });

    it('should get the gravity type of the final field in the path', () => {
      const resolver = makeResolver('board-with-reference-fields');
      expect(resolver.resolve('test.test2').gravityType).toBe('NUMBER');
    });
  });

  describe('.targetBoard', () => {
    it('should return null for a normal field', () => {
      const resolver = makeResolver('board-with-regular-fields');
      expect(resolver.resolve('test1').targetBoard).toBeFalsy();
    });

    it('should return the optionlist for a reference field', () => {
      const resolver = makeResolver('board-with-reference-fields');
      expect(resolver.resolve('stoplight').targetBoard).toBe('stoplights');
    });
  });

  describe('.isReferenceField', () => {
    let resolver: BoardPathResolver;
    beforeEach(() => resolver = makeResolver('board-with-reference-fields'));

    it('should return true if the field refers to another board', () => {
      expect(resolver.resolve('stoplight').isReferenceField).toBeTruthy();
    });

    it(`should return false if it's a regular field`, () => {
      expect(resolver.resolve('plainField').isReferenceField).toBeFalsy();
    });

    it(`should return false for a regular field on a referred to board`, () => {
      expect(resolver.resolve('stoplight.color').isReferenceField).toBeFalsy();
    });
  });

  describe('.isValid', () => {
    let resolver: BoardPathResolver;
    beforeEach(() => resolver = makeResolver('board-with-reference-fields'));
    const path = (pathString: string) => resolver.resolve(pathString);

    it('should return true for simple existing paths', () => {
      expect(path('plainField').isValid).toBeTruthy();
    });

    it('should return true for longer paths', () => {
      expect(path('stoplight.color').isValid).toBeTruthy();
    });

    it(`should return false if the field doesn't exist`, () => {
      expect(path('asdfg').isValid).toBeFalsy();
    });

    it('should return false if a middle segment is invalid', () => {
      expect(path('recursive.derper.stoplight.color').isValid).toBeFalsy();
    });

    it('should be true for the initial valid segments of a bad path', () => {
      const badPath = path('recursive.deeper.foo.bar');
      expect(badPath.segments[0].isValid).toBeTruthy();
      expect(badPath.segments[1].isValid).toBeTruthy();
      expect(badPath.segments[2].isValid).toBeFalsy();
      expect(badPath.segments[3].isValid).toBeFalsy();
    });
  });

  describe('.invalidityReason', () => {
    let resolver: BoardPathResolver;
    beforeEach(() => resolver = makeResolver('board-with-reference-fields'));
    const path = (pathString: string) => resolver.resolve(pathString);

    it('should be falsy for valid paths', () => {
      expect(path('stoplight.color').invalidityReason).toBeFalsy();
    });

    it('should give a message for an invalid path', () => {
      expect(typeof path('stoplight.collar').invalidityReason).toBe('string');
    });

    it('should give an error if the first segment is invalid', () => {
      expect(path('flibberty').isValid).toBeFalsy();
    });

    it('should carry through the first invalidity reason on a long path', () => {
      const badPath = path('recursive.dropper.stoplight');
      expect(badPath.segments[0].invalidityReason).toBeFalsy();
      expect(badPath.segments[1].invalidityReason).toBe(badPath.invalidityReason);
      expect(badPath.segments[2].invalidityReason).toBe(badPath.invalidityReason);
    });
  });
});



