import { FieldPath } from './field-path';
import { PathResolver } from './path-resolver';


/**
 * Combines the results of multiple path resolvers
 *
 * This combines the output from an array of path resolvers. The order of this
 * array determines the priority of the matches -- the first resolver takes
 * precedence.
 *
 * This is intended to be used to resolve fields from concatenated boards, where
 * you want to pick the fields that exist on the union of board configs.
 */
export class MultiPathResolver implements PathResolver {
  constructor(private pathResolvers: PathResolver[]) {}

  get dataSourceName() {
    return this.pathResolvers.map(r => r.dataSourceName).join(', ') || 'No data sources available';
  }

  resolve(path: string): FieldPath {
    // Preferably, take the first valid path. Failing that, take the first not
    // null path.
    const paths = this.pathResolvers.map(r => r.resolve(path));
    return paths.find(p => p && p.isValid) || paths.find(p => !!p);
  }

  getChildPaths(parent: FieldPath = null): FieldPath[] {
    const existingPaths = new Set<string>();
    const uniquePaths: FieldPath[] = [];
    for (const resolver of this.pathResolvers) {
      const resolvedPaths = resolver.getChildPaths(parent);
      for (const path of resolvedPaths) {
        if (!existingPaths.has(path.path)) {
          existingPaths.add(path.path);
          uniquePaths.push(path);
        }
      }
    }
    return uniquePaths;
  }
}
