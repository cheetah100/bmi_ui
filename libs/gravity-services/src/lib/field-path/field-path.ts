/**
 * Represents a generic field path.
 *
 * This represents a split dotted field path. It's broken down into segments to
 * allow metadata for each segment to be accessed.
 *
 * This is the generic interface which doesn't know about the specific data type
 * the field path is for, so it can only provide broad information.
 *
 * Field paths are immutable.
 */
export interface FieldPath {
  // The funky self-referential generic type just means that the segments array
  // in the interface can be made strongly typed in implementations too.

  /**
   * The parent of this field path, or null/undefined if the parent is blank.
   *
   * This should be equivalent to segments[segments.length - 2]
   */
  parent: FieldPath;

  /**
   * Gets the segments that make up this path.
   *
   * Each entry is a field path, and the last entry is `this` object.
   */
  segments: FieldPath[];

  /**
   * The field name of the target segment of this path.
   */
  fieldName: string;

  /**
   * The dotted path to this field.
   */
  path: string;

  /**
   * Does this field reference another card?
   *
   * In Gravity terms, is there an optionlist?
   */
  isReferenceField: boolean;

  /**
   * The board that this field exists on.
   *
   * This is an optional field, as while this makes sense for field paths on
   * boards, it may not make sense if it refers to a field of a transform result
   * etc.
   */
  boardId?: string;

  /**
   * If this is a reference field, this is the target board.
   */
  targetBoard: string;

  /**
   * The Gravity data type of this field.
   */
  gravityType: string;

  /**
   * Does this path include any list fields that could produce multiple values?
   */
  isMultiValue: boolean;

  /**
   * Is this a list field? I.e. does the target of this path refer to a field
   * that has multiple values?
   */
  isListField: boolean;

  /**
   * Is this a metadata field?
   *
   * Metadata fields sometimes need special handling, as not all Gravity APIs
   * support them consistently. In IDP, we're treating these metadata fields as
   * if they are properties of a card, but this isn't always the case on the
   * backend.
   */
  isMetadata: boolean;

  /**
   * Is this a valid path?
   */
  isValid: boolean;

  /**
   * Gets an error string if the path is invalid.
   */
  invalidityReason: string | null;

  /**
   * Returns the field path string.
   */
  toString(): string;
}


