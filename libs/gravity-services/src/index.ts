/*
 * Public API Surface of gravity-lib
 */

// Core services
import { ResourceHandle } from './lib/services/gravity-resource.service';

export { Gravity } from './lib/gravity';
export {
  GravityParameters,
  GRAVITY_PARAMETERS
} from './lib/gravity-parameters';
export * from './lib/bmi-app-init-parameters';
export {
  DataStore,
  DataStoreIndex,
  DataStoreItem
} from './lib/core/data-store';
export { GravityConfigService } from './lib/core/gravity-config.service';
export { GravityConverter } from './lib/core/gravity-converter';
export { GravityCache } from './lib/core/gravity-cache';
export { GravityModule } from './lib/gravity.module';
export { GravityService, CardBackReferences } from './lib/core/gravity.service';

// Gravity object model classes.

export * from './lib/model/condition';
export * from './lib/model/credential'
export * from './lib/model/rule';
export * from './lib/model/action';
export * from './lib/model/filter';
export * from './lib/model/condition-operations';
export * from './lib/model/integration';
export * from './lib/model/transform';

// These are deprecated IDP things
export { Card } from './lib/core/card';
export { Dashboard, DashboardWidget } from './lib/dashboard/dashboard';

//
export * from './lib/data-context/card-data-context';

export {
  BoardPathResolver,
  BoardFieldPath
} from './lib/field-path/board-field-path';
export { FieldPath } from './lib/field-path/field-path';
export { PathResolver } from './lib/field-path/path-resolver';
export { MultiPathResolver } from './lib/field-path/multi-path-resolver';

// Gravity services and 'repositories'
export {
  GravityResourceService,
  BoardResource,
  BoardResourceSummary,
  ResourceHandle
} from './lib/services/gravity-resource.service';
export { RulesService, RuleSummary } from './lib/services/rules.service';
export * from './lib/services/card-task.service';
export { GravityUrlService } from './lib/services/gravity-url.service';
export { GravityFilterService } from './lib/services/gravity-filter.service';
export * from './lib/services/gravity-transform.service';
export * from './lib/services/gravity-view.service';
export * from './lib/services/gravity-board-pivots.service';
export { GravityWidgetService } from './lib/services/gravity-widget.service';
export {
  GravityCardAlertService
} from './lib/services/gravity-card-alert.service';
export { IntegrationsService } from './lib/services/integrations.service';
export { SystemStatusService } from './lib/services/system-status.service';
export { CredentialService } from './lib/services/credential.service';
export {
  PluginMetadataService,
  PluginMetadata,
  PluginMetadataField
} from './lib/services/plugin-metadata.service';

export {
  GravityTimelineService,
  Quarter
} from './lib/services/gravity-timeline.service';
export {
  BoardService,
  BoardQueryParams,
  IBoardService
} from './lib/services/board.service';
export { CardCacheService } from './lib/services/card-cache.service';
export { PathLookupService } from './lib/path-lookup/path-lookup.service';

export {
  GravityPrefsRepository
} from './lib/services/gravity-prefs.repository';
export { DashboardRepository } from './lib/dashboard/dashboard.repository';
export { BoardHistoryRepository } from './lib/core/board-history.repository';
export { TransformRepository } from './lib/core/transform.repository';
export {
  UserRepository,
  Team,
  User,
  UserAndPreference
} from './lib/core/user.repository';
export {
  UploadStatusModalService
} from './lib/ui/upload-status/upload-status-modal.service';
export {
  SpreadsheetUploadModalService
} from './lib/ui/spreadsheet-uploader/spreadsheet-upload-modal.service';
export {
  SpreadsheetUploaderModalComponent
} from './lib/ui/spreadsheet-uploader/spreadsheet-uploader-modal.component';

// Data Sources API: the repositories here are actually kind of different from
// the dashboard and user repositories from above.
export { DataSourceService } from './lib/data-sources/data-source.service';
export {
  DataSource,
  DataSourceGetOptions
} from './lib/data-sources/data-source';
export {
  DATA_SOURCE_REPOSITORIES,
  DataSourceRepository
} from './lib/data-sources/data-source-repository';

// There are a bunch of interface types here for dealing with board configs
// and views.
export * from './lib/core/board-config';

// API types. Ideally these would be a secondary endpoint with ng-packagr (e.g.
// @bmi/gravity-services/api-types) to keep them all together but I couldn't
// figure out how to reference these from the main module as well.
export * from './lib/api-types/gravity-api-app-group';
export * from './lib/api-types/gravity-api-app-visualisation';
export * from './lib/api-types/gravity-api-application';
export * from './lib/api-types/gravity-api-board-config';
export * from './lib/api-types/gravity-api-cardview';
export * from './lib/api-types/gravity-api-card-back-references';
export * from './lib/api-types/gravity-api-condition';
export * from './lib/api-types/gravity-api-filter';
export * from './lib/api-types/gravity-api-metadata';
export * from './lib/api-types/gravity-api-preference';
export * from './lib/api-types/gravity-api-rule';
export * from './lib/api-types/gravity-api-user';
export * from './lib/api-types/gravity-api-view';
export * from './lib/api-types/gravity-api-widget';
export * from './lib/api-types/gravity-api-card-event';
export * from './lib/api-types/gravity-api-integration';
export * from './lib/api-types/gravity-api-transformer';
export * from './lib/api-types/gravity-api-system-report';
export * from './lib/api-types/gravity-api-plugin-field';
export * from './lib/api-types/gravity-api-card-task';

// Support unit testing with gravity outside this library
export {
  gravityTestbed,
  makeCardJson,
  TEST_USER,
  CardDataWithPartialMetadata,
} from './lib/testing/gravity-test-tools';
export { gravityTestbed2, makeBoard } from './lib/testing/gravity-testbed-2';
export {
  GravityTestbed2Options,
  FakeBoard
} from './lib/testing/gravity-testbed-options';
export { BoardConfigBuilder } from './lib/testing/board-config-builder';
export { FakeBoardService } from './lib/testing/fake-board.service';

export { applyEqualToFilterOnFrontend } from './lib/utils/apply-gravity-filter';

// UI components. Like the API types, these would be best in a secondary
// endpoint, but the way this is implemented in ngpackagr makes this difficult.
export {
  ConditionListComponent
} from './lib/ui/condition-list/condition-list.component';
export {
  PathPickerComponent
} from './lib/ui/path-picker/path-picker.component';
export {
  FilterConfigComponent
} from './lib/ui/filter-conditions/filter-config.component';
export {
  SpreadsheetUploaderComponent
} from './lib/ui/spreadsheet-uploader/spreadsheet-uploader.component';
export { GravityUiModule } from './lib/gravity-ui.module';
export * from './lib/ui/path-picker/path-picker-filters';
export * from './lib/core/view';
export * from './lib/ui/card-option-cache';

export * from './lib/card-details/card-details';
export * from './lib/card-details/card-details.service';
export * from './lib/testing/model-board-configs';
