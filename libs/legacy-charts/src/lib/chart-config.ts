/**
 * The configuration for a @bmi/legacy-charts chart.
 *
 * This is separate from the Gravity widget configuration: a chart config may be
 * stored in the Gravity Widget's fields, but it could also be stored elsewhere.
 *
 * This interface is generic to allow the chart implementation to specify a type
 * for the expected ChartSettings object to satisfy. There is no mechanism to
 * enforce this typing, but it's convenient from a development perspective.
 */
export interface ChartConfig<TSettings extends ChartSettings = ChartSettings> {
  /**
   * The type of chart to display.
   *
   * This should correspond to a valid ChartTypeConfiguration.
   *
   * Built-in charts should follow the naming convention
   `@bmi/legacy-charts:<chart type id>-v<version number>`, for example:
   * `@bmi/legacy-charts:stacked-bar-chart-v1`.
   *
   * The exact format of the name isn't codified, but following a format like
   * this should help with future-proofing the library, and easing chart config
   * migrations.
   */
  chartType: string;

  /**
   * The data source definitions for this chart.
   *
   * Most charts will probably only require a single data source, but the
   * facility is provided for multiple sources, if needed in future types of
   * charts.
   */
  dataSources: ChartDataSource[];

  /**
   * The settings for this chart.
   *
   * The schema of this object is defined by the chart implementation.
   */
  chartSettings: TSettings;
}


/**
 * Identifies the data source for a chart.
 *
 * This is probably a Gravity transform. This structure aims to provide enough
 * information to set up the transform pipeline without needing to query the
 * source for metadata.
 *
 * Further options could be added to this in future to customize the data
 * transformations.
 */
export interface ChartDataSource {
  /**
   * An arbitrary ID for the data source. Can be used to reference it from the
   * chart settings
   */
  id?: string;

  /**
   * The Data Source identifier.
   *
   * These charts build on the Data Sources API in @bmi/gravity-services and this
   * data source reference is used to identify a data source. It's not a URL,
   * as it also needs to encode information about which data source repository
   * to use (e.g. gravity pivot, transform, JSON data source etc.)
   */
  dataSourceRef: string;

  /**
   * Optional filter conditions that will be passed through when fetching the
   * data.
   */
  filters?: ChartDataFilterCondition[];

  /**
   * This optional mapping is used to translate the chart-wide filters to names
   * specific to this data source.
   *
   * For example, a global filter could use the name 'service' but for this
   * specific source the fieldName is 'service_id', you could set:
   *
   *     globalFilterFieldMap: {
   *       'service': 'service_id'
   *     }
   *
   * This mapping does NOT affect the `filters` property for this data source,
   * only how the chart-wide filters get applied to the source.
   *
   * The ChartDataSourceService has an option that will control the behaviour if
   * a field is not present in this map -- including unexpected filters might
   * lead to unintended results.
   */
  globalFilterFieldMap?: {[field: string]: string};

  /**
   * The data translator takes the data from the source and prepares it for the
   * chart.
   *
   * This config sets up which translator is used, as well as any additional
   * configuration for the translation.
   *
   * The translator can handle extracting series from the chart -- this is
   * trivial with Pivot data, but advanced transform that produce data records
   * may require further mapping.
   *
   * Doing this translation as part of the data stage simplifies chart
   * implementations.
   *
   * Specifying the translator is good practice here, as there could be multiple
   * translators available for the data source/chart combination (or new ones
   * added in future).
   */
  translator?: ChartDataTranslatorConfig;
}

// At the moment there are no common chart settings; this is entirely left up
// to the chart implementation.
export type ChartSettings = {};


export interface ChartDataFilterCondition {
  fieldName: string;
  operation: string;
  value: string | string[];
}


export interface ChartDataTranslatorConfig {
  id: string;
  [key: string]: any;
}
