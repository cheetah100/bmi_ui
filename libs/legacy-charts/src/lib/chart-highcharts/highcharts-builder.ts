import * as Highcharts from 'highcharts';
import lodashMerge from 'lodash-es/merge';
import { ObjectMap } from '@bmi/utils';

type BuilderFunction<T> = (builder: T) => void;


abstract class BaseBuilder<T extends {}> {
  public abstract get options(): T;

  public set(options: Partial<T>, mergeOptions = true) {
    if (mergeOptions) {
      lodashMerge(this.options, options);
    } else {
      Object.assign(this.options, options);
    }
    return this;
  }
}


class AxisBuilder<T extends Highcharts.AxisOptions> extends BaseBuilder<Highcharts.AxisOptions> {
  constructor(public options: T) {
    super();
  }

  title(title: string) {
    return this.set({
      title: {
        text: title
      }
    });
  }

  categories(categories: string[]) {
    this.options.categories = categories;
  }
}


class SeriesBuilder extends BaseBuilder<Highcharts.SeriesOptions> {
  constructor(public options: Highcharts.SeriesOptions) {
    super();
  }

  id(id: string) {
    this.options.id = id;
    return this;
  }

  type(type: string): this {
    this.options.type = type;
    return this;
  }

  name(name: string): this {
    this.options.name = name;
    return this;
  }

  color(color: string): this {
    this.options['color'] = color;
    return this;
  }

  data(data: any[]): this {
    this.options['data'] = data;
    return this;
  }

  zones(zones: ObjectMap<number | string>[]): this {
    this.options['zones'] = zones;
    return this;
  }

  connectNulls(connectNulls: boolean): this {
    this.options['connectNulls'] = connectNulls;
    return this;
  }

  visible(visible: boolean): this {
    this.options['visible'] = visible;
    return this;
  }

  zoneAxis(zoneAxis: 'x' | 'y'): this {
    this.options['zoneAxis'] = zoneAxis;
    return this;
  }
}


export class HighchartsBuilder extends BaseBuilder<Highcharts.Options> {
  constructor(public options: Highcharts.Options = {}) {
    super();
  }

  title(title: string) {
    return this.set({
      title: {
        text: title
      }
    });
  }

  chart(chartType: string, polar: boolean = false) {
    return this.set({
      chart: {
        type: chartType,
        polar,
      }
    });
  }

  plotOptions(stacked: 'normal' | 'percent') {
    /**
     * We could be smarter, and insert these just for the series.types that are actually being used.
     * But for now, including the basic chart types by default
     */
    const stacking: 'normal' | 'percent' = stacked || undefined;
    return this.set({
      plotOptions: {
        area: { stacking },
        areaspline: { stacking },
        column: { stacking },
        line: { stacking },
        spline: { stacking },
        scatter: { stacking },
      }
    });
  }

  xAxis(builderFunction: BuilderFunction<AxisBuilder<Highcharts.XAxisOptions>>) {
    if (Array.isArray(this.options.xAxis)) {
      throw new Error('Multiple axes are not supported');
    }
    this.set({ xAxis: {} });
    const axisBuilder = new AxisBuilder(this.options.xAxis);
    builderFunction(axisBuilder);
    return this;
  }

  yAxis(builderFunction: BuilderFunction<AxisBuilder<Highcharts.YAxisOptions>>) {
    if (Array.isArray(this.options.yAxis)) {
      throw new Error('Multiple axes are not supported');
    }
    this.set({ yAxis: {} });
    const axisBuilder = new AxisBuilder(this.options.yAxis);
    builderFunction(axisBuilder);
    return this;
  }

  addSeries(builderFunction: BuilderFunction<SeriesBuilder>) {
    if (!this.options.series) {
      this.options.series = [];
    }

    // There are specific types of series options, and Highcharts has a bunch of
    // interfaces that are 'switched' using the mandatory type parameter, but
    // this will just keep it simple with the basic options.
    const builder = new SeriesBuilder({ type: undefined });
    builderFunction(builder);

    // The Type property strikes again. Casting to any to shut this up.
    this.options.series.push(builder.options as any);
    return this;
  }
}

