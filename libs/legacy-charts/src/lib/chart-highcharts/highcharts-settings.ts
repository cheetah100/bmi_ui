import { ChartSettings } from '../chart-config';

export interface HighchartsSettings extends ChartSettings {
  chartTitle: string;
  polar: boolean;
  selectedOnly: boolean;
  seriesSettings?: SeriesSettings[];
  stacked: 'normal' | 'percent';
  type: string;
  xAxisTitle: string | null;
  yAxisTitle: string | null;
}


/**
 * Configuration for an individual series on a graph.
 *
 * These will be matched up against the data from the pivot yAxis and can be
 * used to customize the display style of data series.
 */
export interface SeriesSettings {
  /**
   * The series "id" value. This will be compared to the Pivot's yAxis array to
   * identify the series.
   */
  value: string;

  /**
   * A custom label for the series. If not provided, the yAxis value will be
   * used.
   */
  label?: string;

  /**
   * A hex colour string that will be used for the series colour.
   */
  color?: string;
  zones?: any[];
  type?: string;
  connectNulls?: boolean;
  visible?: boolean;
  zoneAxis?: 'x' | 'y';
}

export interface HighchartsChartTypeOptions {
  /**
   * The type of Highcharts-chart to display.
   */
  type: string;
}
