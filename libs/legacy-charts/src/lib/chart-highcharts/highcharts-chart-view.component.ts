import { Component, ElementRef, Input, OnInit, OnDestroy, ViewChild, OnChanges } from '@angular/core';

import { ChartConfig } from '../chart-config';
import { ChartType } from '../chart-type';
import { ChartData, Pivot2D, ChartDataWithSeries, ChartDataSeries } from '../chart-data';
import { HighchartsSettings, HighchartsChartTypeOptions, SeriesSettings } from './highcharts-settings';

import { COLOR_GROUPS } from '@bmi/ui';
import { createMapFrom } from '@bmi/utils';

import * as Highcharts from 'highcharts';

import { HighchartsBuilder } from './highcharts-builder';



@Component({
  selector: 'bmi-highcharts-chart-view',
  templateUrl: './highcharts-chart-view.component.html'
})
export class HighchartsChartViewComponent implements OnInit, OnDestroy, OnChanges {
  @Input() config: ChartConfig<HighchartsSettings>;
  @Input() chartType: ChartType<HighchartsChartTypeOptions>;
  @Input() data: ChartDataWithSeries[];

  @ViewChild('chartContainer', { static: true }) chartContainer: ElementRef;

  private chartInstance: Highcharts.Chart;
  private chartColours = COLOR_GROUPS.find(g => g.name === 'Chart Colors').colors;

  ngOnInit() {
  }

  destroyChart() {
    if (this.chartInstance) {
      try {
        this.chartInstance.destroy();
      } catch (e) {
        console.error({ highchartsDestroyError: e });
      }
    }
  }

  ngOnDestroy() {
    this.destroyChart();
  }

  ngOnChanges() {
    if (this.config && this.config.chartSettings && this.data) {
      this.destroyChart();
      this.initHighcharts();
    }
  }

  makeChartConfig(): Highcharts.Options {
    const chartSettings = this.config.chartSettings;

    const builder = new HighchartsBuilder()
      .title(chartSettings.chartTitle)
      .xAxis(x => x.title(chartSettings.xAxisTitle))
      .yAxis(y => y.title(chartSettings.yAxisTitle))
      .chart(chartSettings.type, chartSettings.polar)
      .plotOptions(chartSettings.stacked)
      .set({
        credits: {
          enabled: false,
        },
        plotOptions: {
          series: {
            states: {
              select: {
                enabled: false
              },
              inactive: {
                opacity: 0.6
              }
            }
          }
        }
      });

    this.createChartSpecificConfig(builder);

    return builder.options;
  }

  createChartSpecificConfig(builder: HighchartsBuilder): HighchartsBuilder {
    switch (this.chartType.options.type) {
      case 'line':
      case 'column':
        return this.makeRegularChart(builder);
      default:
        throw new Error('Unsupported chart type');
    }
  }

  makeRegularChart(builder: HighchartsBuilder): HighchartsBuilder {
    const highchartsType = this.chartType.options.type;
    builder.chart(this.config.chartSettings.type, this.config.chartSettings.polar);
    builder.plotOptions(this.config.chartSettings.stacked);

    this.createSeries(builder, this.data[0], highchartsType);

    // If the data source uses categorical data, then set up the X axis
    // categories. If we wanted to support multiple data sources on this chart,
    // we will need to combine and remap all of the categories and data points
    // to ensure consistency.
    if (this.data[0].hasCategories) {
      builder.xAxis(x => x.categories(this.data[0].categories));
    }

    return builder;
  }

  /**
   * Just check whether id on a config defined series exists in the series data
   */
  seriesIdHasConfig(id: string, series: ChartDataSeries[]): boolean {
    return !!series.find(s => s.seriesId === id);
  }

  /**
   * Check whether ANY config series have a match with data src
   */
  someSeriesMatchData(dataSeries: ChartDataSeries[]): boolean {
    const configSeries = this.config.chartSettings.seriesSettings;
    return configSeries.some(s => this.seriesIdHasConfig(s.value, dataSeries));
  }

  createSeries(builder: HighchartsBuilder, chartData: ChartDataWithSeries, seriesType: string): HighchartsBuilder {
    // console.log({
    //   config: this.config,
    //   chartData,
    //   someMatch: this.someSeriesMatchData(chartData.series),
    // })
    const configSeries = this.config.chartSettings.seriesSettings || [];
    const seriesSettings = createMapFrom(configSeries || [], s => s.value);
    let counter = 0;
    // just add series chosen / ordered by user
    let orderedKeys: string[] = configSeries.map(s => s.value);
    // User has unchecked (Only show selected series) ...
    // So, add in all unselected series, after user-configured ones
    if (!this.config.chartSettings.selectedOnly) {
      orderedKeys = Array.from(
        new Set(orderedKeys.concat(chartData.series.map(s => s.seriesId)))
      );
    }

    const defaultSeriesSettings: SeriesSettings = {
      label: null,
      color: null,
      value: null,
      connectNulls: false,
      type: seriesType,
      zoneAxis: 'y',
      visible: true,
      zones: []
    };

    // const getSeriesValue = (series: ChartDataSeries = null, property: string): string => {
    //   if (!series || !series[property]) {
    //     return defaultSeriesSettings[property];
    //   }
    //   return series[property];
    // }

    for (const key of orderedKeys) {
      const series = chartData.series.find(s => s.seriesId === key);
      if (series) {
        const seriesId = series.seriesId;
        const ss = seriesSettings.get(seriesId) || defaultSeriesSettings;
        const color = ss && ss.color ? ss.color : this.chartColours[counter % this.chartColours.length].cssColor;

        // This isn't the most scalable way to get the chart data, but it has the
        // most features. These data points have x and y attributes, and may also
        // have a name property. Further config is possible per-data point.
        //
        // Using the tuple format may be more efficient, and we can probably make
        // it support [name, x, y] as well.
        const seriesData = series.asDatapoints();

        builder.addSeries(s => s
          .id(seriesId)
          .data(seriesData)
          .type(ss.type)
          .name(ss.label || seriesId)
          .color(color)
          .zones(ss.zones)
          .connectNulls(ss.connectNulls)
          .visible(ss.visible)
          .zoneAxis(ss.zoneAxis)
        );

        counter += 1;
      }
    }
    // console.log({
    //   orderedKeys,
    //   seriesSettings,
    //   builder,
    // })
    return builder;
  }

  initHighcharts() {
    Highcharts.chart(
      this.chartContainer.nativeElement as HTMLElement,
      this.makeChartConfig(),
      chart => this.onChartCreated(chart)
    );
  }

  onChartCreated(chart) {
    // Highcharts will automatically clean up an existing chart instance in the
    // container, if the chart was reinitialized; we just need to track the
    // chart reference so that we can destroy it when the component is
    // destroyed.
    this.chartInstance = chart;
  }
}
