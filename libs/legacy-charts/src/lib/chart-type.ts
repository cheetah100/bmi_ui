export interface  ChartType<TOptions = {}> {
  /**
   * The chart type id.
   */
  id: string;

  /**
   * A friendly user-facing name for the chart type
   */
  name: string;

  /**
   * A Markdown formatted description of the chart type
   */
  description?: string;

  /**
   * The chart engine that this chart uses.
   *
   * At the moment, only highcharts is supported here.
   */
  chartEngine: 'highcharts';

  /**
   * Default settings for a chart. These will get mixed in with the chart config
   */
  options: TOptions;
}
