export interface ChartData<T extends {} = {}> {
  type: string;
  data: T;
}


export interface Pivot2D {
  xAxis: string[];
  yAxis: string[];
  data: any[][];
}



export interface ChartDataWithSeries extends ChartData {
  series: ChartDataSeries[];
  hasCategories: boolean;
  categories: string[];
}



export interface ChartDataSeries {
  seriesId: string;
  asTuples(): any[][];
  asDatapoints(): SeriesDataPoint[];
}


export interface SeriesDataPoint {
  x: any;
  y: any;
  name?: string;
  sourceRecord?: any;
}


