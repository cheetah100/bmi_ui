import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedUiModule, CLOSE_DIALOG_SERVICE_TOKEN, SimpleCloseDialogService } from '@bmi/ui';
import { GravityUiModule, GravityModule } from '@bmi/gravity-services';

import { BmiChartsModule } from './bmi-charts.module';

import { ChartEditorComponent } from './chart-editor/chart-editor.component';
import { BasicHighchartsEditorComponent } from './chart-editor/chart-settings-editor/basic-highcharts-editor/basic-highcharts-editor.component';
import { SeriesMapperEditorComponent } from './chart-data-translators/series-mapper-editor.component';
import { ZonesControlComponent } from './chart-editor/zones-editor/zones-control.component';

import { DataSourceEditorComponent } from './chart-editor/chart-data-sources-editor/data-source-editor.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedUiModule,
    GravityModule,
    GravityUiModule,
    BmiChartsModule
  ],
  declarations: [
    ChartEditorComponent,
    BasicHighchartsEditorComponent,
    DataSourceEditorComponent,
    SeriesMapperEditorComponent,
    ZonesControlComponent
  ],
  exports: [
    ChartEditorComponent,
    BasicHighchartsEditorComponent,
    DataSourceEditorComponent,
    ZonesControlComponent
  ],
  providers: []
})
export class BmiChartsEditorModule { }
