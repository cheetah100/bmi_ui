import { Injectable, Inject } from '@angular/core';

import { combineLatest, of, Observable, EMPTY } from 'rxjs';
import { switchMap, map, publishReplay, refCount } from 'rxjs/operators';

import { DataSourceService, DataSource, GravityApiFilter, GravityApiConditionOperation } from '@bmi/gravity-services';
import { ChartData, Pivot2D } from '../chart-data';
import { ChartDataFilterCondition, ChartDataSource } from '../chart-config';
import { ChartType } from '../chart-type';
import { ChartTypeService } from '../chart-type.service';

import { ChartDataTranslatorService } from '../chart-data-translators/chart-data-translator.service';
import { DataTranslator } from '../chart-data-translators/data-translator';

import { sanitizeId } from '@bmi/utils';


export type ChartFilterConditionPredicate = (condition: ChartDataFilterCondition, dataSource: ChartDataSource) => boolean;


interface GlobalChartDataFilterOptions {
  filterConditions?: ChartDataFilterCondition[],
  includeGlobalConditionPredicate?: ChartFilterConditionPredicate
}


@Injectable()
export class ChartDataSourceService {
  constructor(
    private dataSourceService: DataSourceService,
    private dataTranslatorService: ChartDataTranslatorService,

  ) {}

  /**
   * Get all enumerable data sources.
   *
   * This combines the results of all of the data source services, and will
   * return a listing of the *known* sources, such as pivots and transforms,
   * suitable for displaying in a picker.
   */
  allSources(): Observable<DataSource[]> {
    return this.dataSourceService.getSources();
  }

  /**
   * Look up a data source by reference.
   *
   * This will try and find the service that 'answers for' this type of data,
   * then delegate the lookup to that service. If no suitable service can be
   * found then this method will throw an error (synchronously).
   */
  lookup(ref: string): Observable<DataSource> {
    return this.dataSourceService.lookup(ref);
  }

  /**
   * Constructs an observable pipeline to fetch the data for a number of chart
   * data sources.
   *
   * This will emit an array of ChartData objects which correspond to the data
   * source definitions. This uses combineLatest, so if data sources emit
   * multiple times the entire array will be re-emitted with the latest values.
   *
   * This supports filtering, both at a global level, and by specific filters in
   * the chart data source configuration.
   *
   * The global filter conditions allow chart-wide filtering, but it's not safe
   * to just apply a global filter to every data source -- if it doesn't support
   * the field, or the field has a different meaning then it could limit the
   * data returned in a way that wasn't intended.
   *
   * Instead, a data source needs to 'opt in' to these global filter conditions
   * by listing the field in the source's globalFilterFieldMap. This also gives
   * an opportunity to map the global field to the correct local name for the
   * data source (e.g. a global "service" filter might get mapped to
   * "service_id" for a particular data source.)
   *
   * By default, conditions for fields not present in this mapping will NOT get
   * added, but this behaviour can be customized using the
   * includeGlobalConditionPredicate predicate.
   *
   * The default behaviour is essentially:
   *
   *   (condition, dataSource) => dataSource.globalFilterFieldMap && !!dataSource.globalFilterFieldMap[condition.fieldName]
   *
   * @param chartDataSources the data sources to fetch
   * @param globalFilterConditions an optional list of global filter conditions
   * @param includeGlobalConditionPredicate if provided, this lets you customize
   *     whether a global filter condition is included for a data source.
   * @returns an Observable which will emit an array of ChartData corresponding-
   *     to the data sources.
   */
  public getChartData(
    chartDataSources: ChartDataSource[],
    filterConditions?: ChartDataFilterCondition[],
    includeGlobalConditionPredicate?: ChartFilterConditionPredicate
  ): Observable<ChartData[]> {
    return combineLatest(chartDataSources.map(cds => this.prepareChartDataSource(cds, {filterConditions, includeGlobalConditionPredicate})));
  }

  private prepareChartDataSource(chartDataSource: ChartDataSource, globalFilterOptions: GlobalChartDataFilterOptions = {}): Observable<ChartData> {
    const gravityFilter = this.prepareDataSourceFilter(chartDataSource, globalFilterOptions);

    // Try and find the translator for this source. Ideally the CDS config
    // should specify this to keep things unambiguous.
    const configuredTranslatorId = (chartDataSource.translator && chartDataSource.translator.id) ? chartDataSource.translator.id : null;
    let translator: DataTranslator = null;
    if (configuredTranslatorId) {
      translator = this.dataTranslatorService.getTranslatorById(configuredTranslatorId);
      if (!translator) {
        throw new Error(`Unsupported Chart Data Translator: ${configuredTranslatorId}`);
      }
    }

    // We may not have a translator, but we need to know more about the data
    // source before we can guess. If no translator is configured, and no
    // suitable one can be found then a fallback one will be used that passes on
    // the response verbatim.
    return this.lookup(chartDataSource.dataSourceRef).pipe(
      switchMap(dataSource => dataSource.getData({gravityFilter: gravityFilter}).pipe(
        switchMap(data => {
          if (translator) {
            // Do some sanity checking to catch bugs and configuration errors.
            if (!translator.acceptedDataTypes.includes(dataSource.dataType)) {
              console.warn(`Chart Data Translator ${translator.translatorId} does not accept the source type ${dataSource.dataType}!`);
            }

            // TODO: check that the translator can meet the chart's requirements
            // too. This should just be delegated to the translator service.
            return translator.translate(data, chartDataSource, dataSource);
          } else {
            // Try and find a suitable translator
            const translators = this.dataTranslatorService.getSuitableTranslatorsForSource(dataSource.dataType, ['series']);
            if (translators.length === 0) {
              console.warn('Chart Data Service: No suitable translator found for source');
              return of(<ChartData>{
                type: dataSource.dataType,
                data: data
              });
            } else {
              const guessedTranslator = translators[0];
              if (translators.length > 1) {
                console.warn('Multiple suitable translators found, picking ${guessedTranslator.id}');
              }
              return guessedTranslator.translate(data, chartDataSource, dataSource);
            }
          }
        })
      )),
    );
  }

  /**
   * Combines the chart-specific and global filter conditions into a Gravity filter.
   *
   * The behaviour of this are outlined in the docstring for the getChartData().
   *
   * @param chartDataSource the chart data source configuration
   * @param globalFilterConditions a list of filter conditions to combine with
   *     the chart data source conditions
   * @param includeGlobalConditionPredicate a predicate to determine
   *     whether a condition should be included.
   * @returns a Gravity Filter JSON compatible object
   */
  prepareDataSourceFilter(
    chartDataSource: ChartDataSource,
    filterOptions: GlobalChartDataFilterOptions = {}
  ): GravityApiFilter {
    // Each data source can define its own filters, but we also have some
    // chart-wide filter options.
    const filters: ChartDataFilterCondition[] = [];
    if (chartDataSource.filters) {
      filters.push(...chartDataSource.filters);
    }

    if (filterOptions.filterConditions) {
      const fieldMap = chartDataSource.globalFilterFieldMap || {};

      // Default behaviour is to only include a condition if it's present in the
      // data source's condition map.
      let shouldIncludeCondition = filterOptions.includeGlobalConditionPredicate;
      if (!shouldIncludeCondition) {
        shouldIncludeCondition = condition => !!fieldMap[condition.fieldName];
      }

      for (const condition of filterOptions.filterConditions) {
        const fieldName = condition.fieldName;
        const translatedFieldName = fieldMap[fieldName];
        if (shouldIncludeCondition(condition, chartDataSource)) {
          filters.push({
            ...condition,
            fieldName: translatedFieldName || fieldName
          });
        }
      }
    }

    return this.toGravityFilter(filters);
  }

  private toGravityFilter(filterConditions: ChartDataFilterCondition[]) {
    if (!filterConditions || filterConditions.length === 0) {
      return null;
    } else {
      const filter: GravityApiFilter = {
        boardId: null,
        conditions: {}
      };

      for (const cond of filterConditions) {
        filter.conditions[sanitizeId(cond.fieldName)] = {
          fieldName: cond.fieldName,
          operation: <GravityApiConditionOperation>cond.operation,
          value: Array.isArray(cond.value) ? cond.value.join('|') : cond.value
        };
      }

      return filter;
    }
  }


  sourcesForType(chartRequirements: string[]): Observable<DataSource[]> {
    const compatibleSourceTypes = this.dataTranslatorService.getCompatibleSourceTypes(chartRequirements);
    return this.allSources().pipe(
      map(sources => sources.filter(s => compatibleSourceTypes.has(s.dataType)))
    );
  }
}
