import { ChartDataSourceService, ChartFilterConditionPredicate } from './chart-data-source.service';
import { ChartDataSource, ChartDataFilterCondition } from '../chart-config';
import { GravityApiFilter } from '@bmi/gravity-services';

describe('ChartDataSourceService', () => {
  describe('prepareDataSourceFilter', () => {
    let service: ChartDataSourceService;
    beforeEach(() => {
      // Not setting up a full testbed for this, as we don't need any of the
      // Gravity testbed for this functionality.
      service = new ChartDataSourceService(null);
    });

    function testDataSource(filters?: ChartDataFilterCondition[], fieldMap?: {[field: string]: string}): ChartDataSource {
      return {
        type: 'test',
        href: 'test',
        filters: filters,
        globalFilterFieldMap: fieldMap
      };
    }

    function makeCondition(fieldName: string, value: string | string[] = 'test'): ChartDataFilterCondition {
      return {
        fieldName,
        value,
        operation: 'EQUALTO'
      };
    }

    function getFilterFieldNames(filter: GravityApiFilter) {
      return filter ? Object.values(filter.conditions).map(c => c.fieldName) : [];
    }

    describe('With no global filters set', () => {
      it('should return a null filter if null conditions are provided', () => {
        expect(service.prepareDataSourceFilter(testDataSource(null))).toBe(null);
      });

      it('should return a null filter if an empty list of filter conditions', () => {
        expect(service.prepareDataSourceFilter(testDataSource([]))).toBe(null);
      });

      it('should return a filter for the chart filter conditions', () => {
        const filter = service.prepareDataSourceFilter(testDataSource([makeCondition('foo'), makeCondition('bar')]));
        const filteredFields = getFilterFieldNames(filter);
        expect(filteredFields).toContain('foo');
        expect(filteredFields).toContain('bar');
      });
    });

    describe('With global filter conditions', () => {
      const globalFilters = [
        makeCondition('service'),
        makeCondition('year')
      ];

      function prepareFilterForTest(dataSource: ChartDataSource, globalConditions?: ChartDataFilterCondition[], predicate?: ChartFilterConditionPredicate): string[] {
        const filter = service.prepareDataSourceFilter(dataSource, globalConditions, predicate);
        return getFilterFieldNames(filter);
      }

      it('should not be included if the data source has no filter and fieldMap is undefined', () => {
        const filteredFields = prepareFilterForTest(testDataSource([]), globalFilters);
        expect(filteredFields.length).toBe(0);
      });

      it('should not be included if the data source has a filter but no fieldMap', () => {
        const filteredFields = prepareFilterForTest(testDataSource([makeCondition('test')]), globalFilters);
        expect(filteredFields).toContain('test');
        expect(filteredFields).not.toContain('service');
        expect(filteredFields).not.toContain('year');
      });

      it('should only include filterConditions from the fieldMap', () => {
        const filteredFields = prepareFilterForTest(
          testDataSource([makeCondition('test')], {'service': 'service'}),
          globalFilters
        );
        expect(filteredFields).toContain('service');
        expect(filteredFields).not.toContain('year');
      });

      it('should translate the field names according to the field map', () => {
        const filteredFields = prepareFilterForTest(
          testDataSource([makeCondition('test')], {'service': 'service_id'}),
          globalFilters
        );
        expect(filteredFields).toContain('service_id');
      });

      describe('Overriding the predicate', () => {

        function prepareWithPredicate(predicate: ChartFilterConditionPredicate) {
          return prepareFilterForTest(
            testDataSource([makeCondition('test')], {'service': 'service_id'}),
            globalFilters,
            predicate
          );
        }

        it('should still include the chart sources when denying global filters', () => {
          const fields = prepareWithPredicate(() => false);
          expect(fields).toContain('test');
          expect(fields).not.toContain('service_id');
          expect(fields).not.toContain('year');
        });

        it('should control the inclusion of global conditions', () => {
          const fields = prepareWithPredicate(condition => condition.fieldName === 'year');
          expect(fields).toContain('year');
          expect(fields).toContain('test');
          expect(fields).not.toContain('service_id');
        });

        it('should still apply the field translation', () => {
          const fields = prepareWithPredicate(condition => condition.fieldName === 'service');
          expect(fields).toContain('service_id');
        });
      });
    });
  });
});
