import { Injectable } from '@angular/core';
import { EMPTY, Observable, of } from 'rxjs';

import { DataSource, DataSourceRepository } from '@bmi/gravity-services';

const DATA_SOURCE_TYPE = '@bmi/legacy-charts:json-source';
const DATA_SOURCE_REGEX = new RegExp(`^${DATA_SOURCE_TYPE}:([\\w/\\-]+):(.*)\$`);

function parseJsonRefRef(ref: string) {
  const match = ref.match(DATA_SOURCE_REGEX);
  if (!match) {
    return null;
  } else {
    return {
      dataType: match[1],
      jsonData: match[2]
    };
  }
}

function generateRef(dataType: string, json: string) {
  return `${DATA_SOURCE_TYPE}:${dataType}:${json}`;
}


class JsonDataSource implements DataSource<any> {
  public readonly dataSourceRef: string;
  public readonly data: any;
  public readonly dataType: string;
  public readonly name = 'JSON Data';

  constructor(jsonData: string, dataType: string) {
    this.dataSourceRef = generateRef(dataType, jsonData);
    this.data = JSON.parse(jsonData);
    this.dataType = dataType;
  }

  getData(): Observable<any> {
    return of(this.data);
  }
}


/**
 * Provides a dummy data source that allows JSON data to be stored directly in a
 * config.
 */
@Injectable()
export class JsonDataSourceService implements DataSourceRepository {
  getSources() {
    return null;
  }

  lookup(ref: string) {
    const parsed = parseJsonRefRef(ref);
    if (parsed) {
      return of(new JsonDataSource(parsed.jsonData, parsed.dataType));
    } else {
      return null;
    }
  }
}
