import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';

import { PivotData } from '@bmi/gravity-services';

import { DataTranslator } from './data-translator';
import { ChartDataSource } from '../chart-config';
import { PivotChartData } from '../pivot-chart-data';

@Injectable()
export class PivotDataTranslator implements DataTranslator<PivotChartData> {
  public readonly translatorId = 'pivot-translator';
  public readonly name = 'Pivot Translator';
  public readonly acceptedDataTypes = ['gravity/pivot-data', 'pivot-2d'];
  public readonly capabilities = ['pivot-2d', 'series'];
  public readonly editorUiComponentId = null;

  translate(data: PivotData): Observable<PivotChartData> {
    return of(new PivotChartData(data));
  }
}
