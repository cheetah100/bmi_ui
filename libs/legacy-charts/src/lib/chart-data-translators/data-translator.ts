import { InjectionToken, Type } from '@angular/core';

import { Observable } from 'rxjs';

import { DataSource } from '@bmi/gravity-services';

import { ChartData } from '../chart-data'
import { ChartDataSource } from '../chart-config';


export const CHART_DATA_TRANSLATORS = new InjectionToken<DataTranslator>('BMI Charts Chart Data Translators');


/**
 * Defines a service for converting raw data source output into Chart Data
 *
 * The translator takes the data from the source and can perform translations on
 * it to produce a ChartData object that a chart can consume.
 *
 * The Translator service can perform some negotiation to try and match the
 * requirements of the chart type to the available data sources. This is based
 * on the list of accepted data types, and the tags that this translator
 * produces.
 *
 * For example, a Translator may take 'pivot-data' and produce ['pivot-2d',
 * 'series', 'categorical']. A chart type can define the type of data that it
 * needs, e.g. ['series'], and the translator service can trace that back to the
 * valid input data types. A chart could define multiple requirements which must
 * all be met, e.g. ['series', 'pivot-2d'] which would restrict the set of
 * possible sources.
 *
 * A translator may need config to work. This is stored in the ChartDataSource,
 * which is provides when performing a translation.
 *
 * Configuring these options may also need a UI component to show in the data
 * source editor. Ideally this would be dynamically loaded, and provided by the
 * translator. For the MVP release this is hard coded, using an ngSwitch and the
 * editorUiComponentId field. If registering translators is made part of the
 * public API then we should add dynamic loading here.
 */
export interface DataTranslator<T extends ChartData = ChartData> {
  /**
   * The ID for the translator. This will be used in chart configs to identify
   * which translator is expected to be used.
   */
  translatorId: string;

  /**
   * A user-friendly name for the translator. This might be displayed in the
   * editor if the user needs to pick which translator to use.
   */
  name: string;

  /**
   * A list of the data source types that the translator can accept.
   */
  acceptedDataTypes: string[];

  /**
   * A list of chart data 'capability tags' which are used to match up this
   * translator with compatible charts
   */
  capabilities: string[];

  /**
   * Determines which UI to use when editing the translator options.
   */
  editorUiComponentId: null | 'series-mapper';

  /**
   * Perform the translation, producing chart data.
   *
   * The result of this is observable in case further async requests are
   * necessary. The ChartDataSource config, as well as the @bmi/gravity-services
   * DataSource are provided for reference/metadata.
   *
   * @param data the fetched data from the source
   * @param chartDataSource the config for the chart data source, including any
   *     translator options.
   * @param dataSource the Data Sources API object where the data was fetched
   *     from. This is provided in case needed for its metadata.
   * @returns an Observable which will emit chart data.
   */
  translate(data: any, chartDataSource: ChartDataSource, dataSource: DataSource): Observable<T>;
}
