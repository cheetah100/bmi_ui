import { Injectable, Inject } from '@angular/core';

import { CHART_DATA_TRANSLATORS,  DataTranslator } from './data-translator';


@Injectable()
export class ChartDataTranslatorService {
  constructor(
    @Inject(CHART_DATA_TRANSLATORS) private translators: DataTranslator[]
  ) {}

  getTranslatorById(translatorId: string) {
    return this.translators.find(t => t.translatorId === translatorId);
  }

  /**
   * Look up the source types that are supported for a chart's requirements.
   *
   * A chart may require ['series', 'categorical']; this means we need to find
   * translators that can provide *all* of the required capabilities.
   *
   * This will then return the set of valid Data Source data types that can be
   * fed into this chart, and this list can be used to filter the displayed data
   * source choices.
   *
   * @param chartRequirements the capability tags that this data translator
   *     must provide for the chart
   * @returns a list of valid data types that can be used.
   */
  getCompatibleSourceTypes(chartRequirements: string[]): Set<string> {
    const dataTypeSet = new Set<string>();
    for (const translator of this.getTranslatorsWhichProvide(chartRequirements)) {
      for (const dataType of translator.acceptedDataTypes) {
        dataTypeSet.add(dataType);
      }
    }
    return dataTypeSet;
  }

  getTranslatorsWhichProvide(requirements: string[]): DataTranslator[] {
    return this.translators.filter(t => requirements.every(c => t.capabilities.includes(c)))
  }

  /**
   * Gets a list of suitable translators for a particular type of data and chart
   * requirements.
   *
   * If no suitable translator can be found, this will return an empty list.
   *
   * @param sourceDataType the type of data the source provides
   * @param chartRequirements the capabilities the translator must provide for the chart
   * @returns a list of suitable translators.
   */
  getSuitableTranslatorsForSource(sourceDataType: string, chartRequirements: string[]): DataTranslator[] {
    return this.getTranslatorsWhichProvide(chartRequirements)
      .filter(t => t.acceptedDataTypes.includes(sourceDataType));
  }
}
