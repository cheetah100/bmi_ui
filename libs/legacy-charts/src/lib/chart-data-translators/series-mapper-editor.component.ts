import { Component, Input, forwardRef, HostListener } from '@angular/core';
import { NG_VALUE_ACCESSOR, NG_VALIDATORS, Validator, FormControl } from '@angular/forms';

import { BaseControlValueAccessor, TypedFormGroup, TypedFormControl, TemplatedFormArray, IdpValidators, provideAsValidatingCVA } from '@bmi/ui';

import { SeriesMapperTranslator, SeriesMapperConfig, SeriesMapperSerie } from './series-mapper-translator';

@Component({
  selector: 'bmi-charts-series-mapper-editor',
  templateUrl: './series-mapper-editor.component.html',
  styleUrls: ['./series-mapper-editor.component.scss'],
  providers: [
    provideAsValidatingCVA(forwardRef(() => SeriesMapperEditorComponent))
  ]
})
export class SeriesMapperEditorComponent extends BaseControlValueAccessor<SeriesMapperConfig> implements Validator {

  readonly form = new TypedFormGroup<SeriesMapperConfig>({
    id: new TypedFormControl<string>(''),
    isCategorical: new TypedFormControl<boolean>(true),
    xAxisField: new TypedFormControl<string>('', [IdpValidators.required, IdpValidators.mustBeNonBlank()]),
    series: new TemplatedFormArray(serie => new TypedFormGroup({
      seriesId: new TypedFormControl(serie.seriesId),
      yAxisField: new TypedFormControl(serie.yAxisField, [IdpValidators.required, IdpValidators.mustBeNonBlank()]),
      nameField: new TypedFormControl(serie.nameField)
    }))
  });

  private subscription = null;

  get seriesArray() {
    return this.form.controls.series as TemplatedFormArray<SeriesMapperSerie>;
  }

  ngOnInit() {
    this.subscription = this.form.valueChanges.subscribe(config => {
      this.onChanged(config);
    });
  }

  ngOnDestroy() {
    this.preventFurtherValidation();
    this.subscription.unsubscribe();
  }

  @HostListener('click')
  componentClicked() {
    this.onTouched();
  }

  updateComponentValue(config: SeriesMapperConfig) {
    if (config) {
      this.form.patchValue(config, { emitEvent: false });
    } else {
      this.form.reset(undefined, {emitEvent: false});
    }
  }

  validateSelf() {
    if (this.form.valid) {
      return null;
    } else {
      return {
        'seriesMapperInvalid': 'Series Mapper config is not valid'
      };
    }
  }


  addSeriesMapping() {
    this.seriesArray.pushValue({
      seriesId: '',
      yAxisField: '',
      nameField: ''
    })
  }
}
