import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';

import { PivotData } from '@bmi/gravity-services';

import { DataTranslator } from './data-translator';
import { ChartDataSource, ChartDataTranslatorConfig } from '../chart-config';
import { RecordChartData, DataRecordTransform } from '../record-chart-data';


export interface SeriesMapperConfig extends ChartDataTranslatorConfig {
  xAxisField: string;
  isCategorical: boolean;
  series: SeriesMapperSerie[];
}


export interface SeriesMapperSerie {
  seriesId?: string;
  yAxisField: string;
  nameField?: string;
}


@Injectable()
export class SeriesMapperTranslator implements DataTranslator<RecordChartData> {
  public readonly translatorId = 'series-mapper';
  public readonly name = 'Series Mapper';
  public readonly acceptedDataTypes = ['gravity/transform-transpose', 'gravity/records-transform'];
  public readonly capabilities = ['series'];
  public readonly editorUiComponentId = 'series-mapper';

  translate(data: DataRecordTransform, chartDataSource: ChartDataSource): Observable<RecordChartData> {
    const translatorConfig = chartDataSource.translator as SeriesMapperConfig;
    if (!translatorConfig || !translatorConfig.xAxisField) {
      throw new Error(`Series Mapper: X axis field must be defined`);
    }

    const chartData = new RecordChartData(data, translatorConfig.xAxisField, translatorConfig.isCategorical, true);

    if (translatorConfig.series) {
      for (const series of translatorConfig.series) {
        chartData.addSeries(series.seriesId || series.yAxisField, series.yAxisField, series.nameField);
      }
    }

    return of(chartData);
  }
}
