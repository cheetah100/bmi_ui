import { ChartData, ChartDataWithSeries, Pivot2D, ChartDataSeries, SeriesDataPoint } from './chart-data';


export class PivotSeries implements ChartDataSeries {
  constructor(
    private pivotData: Pivot2D,
    private seriesIndex
  ) {}

  get seriesId() {
    return this.pivotData.yAxis[this.seriesIndex];
  }

  get row() {
    return this.pivotData.data[this.seriesIndex];
  }

  asTuples(): [any,any][] {
    return this.row.map((value, index) => <[any, any]>[index, value]);
  }

  asDatapoints(): SeriesDataPoint[] {
    const xAxis = this.pivotData.xAxis;
    return this.row.map((value, index) => <SeriesDataPoint>{
      x: index,
      y: value
    });
  }
}


export class PivotChartData implements ChartData<Pivot2D>, ChartDataWithSeries {
  type = 'pivot-2d';
  public readonly series = this.data.yAxis.map((seriesId, index) => new PivotSeries(this.data, index));

  hasCategories = true;

  get categories(): string[] {
    return this.data.xAxis;
  }

  constructor (
    public readonly data: Pivot2D
  ) {}
}

