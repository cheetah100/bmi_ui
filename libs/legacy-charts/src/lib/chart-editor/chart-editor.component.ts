import { Component, OnInit, forwardRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';

import { combineLatest, EMPTY } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, filter, switchMap, tap } from 'rxjs/operators';

import lodashIsEqual from 'lodash-es/isEqual';

import { TemplatedFormArray, Option, TypedFormGroup, TypedFormControl, BaseControlValueAccessor, provideAsValidatingCVA, IdpValidators } from '@bmi/ui';

import { ChartEditorService } from './chart-editor.service';
import { ChartConfig, ChartDataSource, ChartSettings } from '../chart-config';
import { ChartData } from '../chart-data';
import { ChartTypeService } from '../chart-type.service';


@Component({
  selector: 'bmi-chart-selector, bmi-chart-editor',
  templateUrl: './chart-editor.component.html',
  styleUrls: ['./chart-editor.component.scss'],
  providers: [
    provideAsValidatingCVA(forwardRef(() => ChartEditorComponent))
  ]
})
export class ChartEditorComponent extends BaseControlValueAccessor<ChartConfig> implements OnInit {

  readonly form = new TypedFormGroup({
    chartType: new TypedFormControl<string>(null),
    dataSources: new TemplatedFormArray<ChartDataSource>(cds => new FormControl(cds)),
    chartSettings: new TypedFormControl<ChartSettings>(null, IdpValidators.required)
  });

  chartTypeOptions: Option[];
  chartData: ChartData[];
  previewConfig: ChartConfig = null;

  get dataSources() {
    return this.form.get('dataSources') as TemplatedFormArray<ChartDataSource>;
  }

  get chartType() {
    return this.form.get('chartType') as FormControl;
  }

  get chartSettings() {
    return this.form.controls.chartSettings;
  }

  get isLoadingPreview() {
    return this.chartEditorService.isLoadingNow;
  }

  get isPreviewAvailable() {
    return this.previewConfig && (this.chartData || this.isLoadingPreview);
  }

  constructor(
    private chartEditorService: ChartEditorService,
    private chartTypeService: ChartTypeService,
  ) {
    super();
  }

  ngOnInit() {
    this.chartTypeOptions = this.chartTypeService.getChartTypes().map(ct => <Option>{
      value: ct.id,
      text: ct.name
    });
    // preselect when testing, only one type for now anyway
    this.form.get('chartType').setValue(this.chartTypeOptions[0].value, { emitEvent: false });


    // This will add an empty data source. We should only be doing this when
    // creating a blank chart; as the TemplatedFormArray will adapt to the
    // default config.
    this.dataSources.pushValue({
      dataSourceRef: null
    });

    // We need to fetch the data sources for the chart so that we can show a
    // preview and allow the chart settings editor access to the data, for
    // configuring series etc.
    //
    // We only want to update this if the data sources are valid, and don't want
    // to be spamming requests, hence the deep comparison, debounce. Switch map
    // helps manage subscriptions, and will terminate any incomplete requests if
    // the value changes again.
    this.dataSources.valueChanges.pipe(
      filter(() => this.dataSources.valid),
      distinctUntilChanged((a, b) => lodashIsEqual(a, b)),
      debounceTime(500),
    ).subscribe(dataSources => {
      this.chartEditorService.setDataSources(dataSources);
    });

    this.form.valueChanges.pipe(
      debounceTime(500),
      filter(() => this.form.valid)
    ).subscribe(() => {
      this.previewConfig = this.form.getRawValue();
    });

    this.chartEditorService.chartData.subscribe(chartData => {
      this.chartData = chartData
    });

    this.form.statusChanges.subscribe(status => {
      this.needsRevalidation();
    });

    this.form.valueChanges.pipe(
      debounceTime(10)
    ).subscribe(value => {
      this.onChanged(this.form.getRawValue());
    });
  }

  updateComponentValue(chartConfig: ChartConfig) {
    if (chartConfig) {
      this.form.patchValue(chartConfig, { emitEvent: false });
    } else {
      this.form.reset(undefined, { emitEvent: false });
    }
  }

  validateSelf() {
    // console.log(this, "Validating chart selector", this.form.status)
    if (this.form.valid) {
      return null;
    } else {
      const combinedErrors = Object.assign({}, this.form.controls.chartSettings.errors, this.dataSources.errors);
      if (Object.keys(combinedErrors).length === 0) {
        combinedErrors['chartEditor'] = 'Chart configuration is invalid';
      }
      return combinedErrors;
    }
  }
}
