import { Component, Input, forwardRef, OnDestroy } from '@angular/core';
import { HighchartsColorZone } from '../chart-settings-editor/basic-highcharts-editor/basic-highcharts-editor.component';
import {
  ControlValueAccessor,
  FormArray,
  FormControl,
  FormGroup,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  ValidationErrors,
  Validator,
  Validators,
} from '@angular/forms';
import { ObjectMap } from '@bmi/utils';
import { Subscription } from 'rxjs';
import { BaseControlValueAccessor } from '@bmi/ui';

@Component({
  selector: 'bmi-zones-control',
  templateUrl: './zones-control.component.html',
  styleUrls: ['./zones-control.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => ZonesControlComponent),
    multi: true
  },
  {
    provide: NG_VALIDATORS,
    useExisting: forwardRef(() => ZonesControlComponent),
    multi: true
  }]
})
export class ZonesControlComponent extends BaseControlValueAccessor<HighchartsColorZone[]> implements ControlValueAccessor, OnDestroy, Validator {

  /**
   * zones: value means 'up to'
   * there is no lower bound.
   * zones have to be ordered correctly in the data.
   * lowest => highest value. With default last.
   * lack of value denotes the zone as default.
   *
   * valueRange: we don't know what the range will be for a series.
   * starting with an arbitary 0-100. And first value split at 50.
   * We'll move the upper bound (r) if zones are set. So that zones make up most of the width.
   * And we'll extend the lower bound (l) below zero if required by negative or low values.
   */

  @Input() defaultColor = '#049FD9';
  zones: HighchartsColorZone[] = [];
  valueRange: ObjectMap<number>;
  form: FormArray;
  subscription: Subscription;

  constructor() {
    super();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  getFlex(value: number, index: number): number {
    if (value === undefined) {
      return 1;
    }
    const range = this.valueRange.max - this.valueRange.min;
    const previous: HighchartsColorZone = this.zones[index - 1];
    const width = value - (!!previous ? previous.value : this.valueRange.min);
    return 6 * width / range + (index ? 0 : 1);
  }

  sortZones(): void {
    this.zones.sort((a: HighchartsColorZone, b: HighchartsColorZone) => {
      return (a.value || Infinity) > (b.value || Infinity) ? 1 : -1;
    });
  }

  sortFormZones(): void {
    const getValue = (fg: FormGroup): number => {
      return isNaN(fg.value.value) ? Infinity : fg.value.value;
    }
    this.form.controls.sort((a: FormGroup, b: FormGroup) => {
      return getValue(a) > getValue(b) ? 1 : -1;
    });
    this.form.updateValueAndValidity();
  }

  canSplitZone(i: number): boolean {
    if (i === 0) {
      return true;
    }
    const value: number = this.form.controls[i].value.value;
    if (typeof (value) === undefined) {
      return false;
    }
    return (value - this.form.controls[i - 1].value.value > 1);
  }

  splitZone(i: number, zone: FormGroup): void {
    const previous = (i > 0) ? this.form.controls[i - 1].value.value : this.valueRange.min;
    const midpoint = Math.round(((zone.value.value || this.valueRange.max || 100) + previous) / 2);
    const newZone: HighchartsColorZone = { value: midpoint, color: zone.value.color };
    this.form.insert(i, this.createZoneForm(newZone));
    this.updateForm();
  }

  deleteZone(i: number): void {
    this.form.removeAt(i);
    this.updateForm();
  }

  updateForm(): void {
    this.sortFormZones();
    this.zones = this.form.value;
    this.updateRange();
  }

  checkZoneOrder(open: boolean): void {
    if (!open) {
      this.updateForm();
    }
  }

  updateRange(): void {
    if (!this.zones.length) {
      this.valueRange = { min: 0, max: 100, };
    }
    this.valueRange = Object.values(this.zones)
      .filter((zone: HighchartsColorZone) => zone.value || zone.value === 0)
      .reduce(
        (a: ObjectMap<number>, c: HighchartsColorZone) => {
          return {
            min: Math.min(a.min, c.value),
            max: Math.max(a.max, c.value),
          }
        }, { min: 0, max: 0 }
      );
  }

  createZoneForm(zone: HighchartsColorZone): FormGroup {
    const formGroup = new FormGroup({
      color: new FormControl(zone.color, Validators.required),
    });
    if (zone.value || zone.value === 0) {
      formGroup.addControl('value', new FormControl(zone.value, Validators.required));
    }
    return formGroup;
  }

  createForm(zones: HighchartsColorZone[]): void {
    if (!zones.length || !zones.find(zone => (!zone.value && zone.value !== 0))) {
      zones.push({ color: this.defaultColor });
    }
    this.form = new FormArray(zones.map((zone: HighchartsColorZone) => {
      return this.createZoneForm(zone);
    }));
    this.subscription = this.form.valueChanges.subscribe((changedZones: HighchartsColorZone[]) => {
      this.onChanged(changedZones)
    });
  }

  updateComponentValue(zonesValue: HighchartsColorZone[]) {
    console.log(this, zonesValue, this.defaultColor);

    // Don't do anything if the zones value is null. We don't want this to emit
    // a value because it can mess with the parent form logic. Ideally we
    // wouldn't be emitting events as part of this process, but the way this
    // form is constructed makes that extremely difficult to ensure --
    // procedurally generating the form can trigger unwanted events happening
    // and I don't understand the logic well enough to modify it yet.
    if (Array.isArray(zonesValue)) {
      this.createForm([...zonesValue]);
      this.updateForm();
    }
  }

  validateSelf(): ValidationErrors | null {
    if (this.form.valid) {
      return null;
    }
    return {
      invalid: 'Color zones are not valid',
    };
  }

}
