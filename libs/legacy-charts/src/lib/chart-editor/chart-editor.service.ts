import { Injectable } from '@angular/core';
import { BehaviorSubject, of, Observable, ReplaySubject } from 'rxjs';
import { switchMap, publishReplay, refCount, tap, map, filter } from 'rxjs/operators';
import { ChartTypeService } from '../chart-type.service';
import { ChartDataSourceService } from '../data-sources/chart-data-source.service';
import { ChartData } from '../chart-data';
import { ChartDataSource } from '../chart-config';

export interface ChartDataResponse {
  resolvedData: ChartData[];
  dataSources: ChartDataSource[];
}

/**
 * A service for communications within a ChartEditor instance.
 *
 * There are a lot of moving pieces within a ChartEditor and this service allows
 * for communication between the chart editor and its children.
 */
@Injectable()
export class ChartEditorService {
  private dataSourcesSubject = new ReplaySubject<ChartDataSource[]>(1);
  private isLoadingSubject = new BehaviorSubject<boolean>(false);

  private chartDataObservable: Observable<ChartDataResponse> = this.dataSourcesSubject.pipe(
    tap((d) => this.isLoadingSubject.next(true)),
    switchMap(dataSources => {
      if (!dataSources) {
        return of(null);
      } else {
        return this.chartDataSourceService.getChartData(dataSources).pipe(
          map(chartData => ({ resolvedData: chartData, dataSources, }))
        );
      }
    }),
    tap(() => this.isLoadingSubject.next(false)),
    publishReplay(1),
    refCount()
  );

  constructor(
    private chartTypeService: ChartTypeService,
    private chartDataSourceService: ChartDataSourceService
  ) { }

  /**
   * Reset the chart editor service, e.g. when loading a different chart.
   *
   * This is needed because Angular likes to reuse components, but when loading
   * a different widget we want to reset the chart data and sources.
   */
  public reset() {
    this.setDataSources(null);
    this.isLoadingSubject.next(false);
  }

  setDataSources(chartDataSources: ChartDataSource[]) {
    this.dataSourcesSubject.next(chartDataSources);
  }

  /**
   * Gets the latest chart data for the editor instance.
   */
  get chartData(): Observable<ChartData[]> {
    return this.chartDataObservable.pipe(map(chartDataResponse => {
      return chartDataResponse ? chartDataResponse.resolvedData : null;
    }));
  }

  get resolvedChartData(): Observable<ChartDataResponse> {
    return this.chartDataObservable;
  }

  /**
   * Gets the currently active data source configs.
   *
   * These are the data sources which are used for generating the preview data.
   */
  get dataSources(): Observable<ChartDataSource[]> {
    return this.dataSourcesSubject.asObservable();
  }

  get isLoading(): Observable<boolean> {
    return this.isLoadingSubject.asObservable();
  }

  get isLoadingNow(): boolean {
    return this.isLoadingSubject.value;
  }
}
