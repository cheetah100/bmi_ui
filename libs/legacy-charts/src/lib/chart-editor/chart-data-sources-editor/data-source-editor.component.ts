import { Component, EventEmitter, forwardRef, Input } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ControlValueAccessor, NG_VALUE_ACCESSOR, NG_VALIDATORS, Validator } from '@angular/forms';

import { Observable, of, BehaviorSubject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, catchError, switchMap, tap, flatMap } from 'rxjs/operators';

import { TypedFormGroup, Option, TemplatedFormArray, BaseControlValueAccessor, provideAsValidator, provideAsControlValueAccessor } from '@bmi/ui';
import { ChartDataSourceService } from '../../data-sources/chart-data-source.service';
import { ChartDataSource, ChartDataFilterCondition } from '../../chart-config';

import { DataTranslator } from '../../chart-data-translators/data-translator';
import { ChartDataTranslatorService } from '../../chart-data-translators/chart-data-translator.service';

import { DataSource } from '@bmi/gravity-services';
import { sortBy } from '@bmi/utils';

import { IdpValidators } from '@bmi/ui';


interface GlobalFilterEntry {
  globalFilterId: string;
  dataSourceField: string;
}

function toGlobalFilterEntryList(fieldMap: {[key: string]: string}) {
  return Object.entries(fieldMap || {}).map(entry => <GlobalFilterEntry>{
    globalFilterId: entry[0],
    dataSourceField: entry[1]
  });
}

function toFieldMap(globalFilters: GlobalFilterEntry[]) {
  const obj: {[key: string]: string} = {};
  globalFilters.forEach(f => obj[f.globalFilterId] = f.dataSourceField);
  return obj;
}


@Component({
  selector: 'bmi-charts-data-source-editor',
  templateUrl: './data-source-editor.component.html',
  styleUrls: ['./data-source-editor.component.scss'],
  providers: [
    provideAsControlValueAccessor(forwardRef(() => DataSourceEditorComponent)),
    provideAsValidator(forwardRef(() => DataSourceEditorComponent)),
  ]
})
export class DataSourceEditorComponent extends BaseControlValueAccessor<ChartDataSource> implements Validator {
  @Input() requirements: string[] = ['series'];

  readonly form = new TypedFormGroup({
    id: new FormControl(),
    dataSourceRef: new FormControl(null, [IdpValidators.required, IdpValidators.mustBeNonBlank()]),

    translatorId: new FormControl(null),
    translator: new FormControl(null),

    filters: new TemplatedFormArray<ChartDataFilterCondition>(c => new TypedFormGroup({
      fieldName: new FormControl(c && c.fieldName, [IdpValidators.required, IdpValidators.mustBeNonBlank()]),
      value: new FormControl(c && c.value, [IdpValidators.required, IdpValidators.mustBeNonBlank()]),
      operation: new FormControl(c && c.operation || 'EQUALTO', [IdpValidators.required, IdpValidators.mustBeNonBlank()]),
    })),

    globalFilterMapping: new TemplatedFormArray<GlobalFilterEntry>(entry => new TypedFormGroup({
      globalFilterId: new FormControl(entry.globalFilterId),
      dataSourceField: new FormControl(entry.dataSourceField),
    })),

  });

  dataSourceOptions = new BehaviorSubject<Option[]>([]);
  knownDataSources: DataSource[] = [];

  private availableTranslators: DataTranslator[] = [];
  public translatorOptions: Option[] = [];

  public showFilteringOptions = false;

  private triggerOnChanged = new EventEmitter<void>();

  get filtersArray() {
    return this.form.controls.filters as TemplatedFormArray<ChartDataFilterCondition>;
  }

  get globalFilterMapping() {
    return this.form.controls.globalFilterMapping as TemplatedFormArray<GlobalFilterEntry>;
  }

  get selectedTranslatorId() {
    return this.form.value.translatorId;
  }

  get activeTranslator() {
    const selectedId = this.selectedTranslatorId;
    return this.availableTranslators.find(t => t.translatorId === selectedId);
  }

  constructor(
    private chartDataService: ChartDataSourceService,
    private translatorService: ChartDataTranslatorService

  ) { super(); }

  ngOnInit() {
    this.chartDataService.sourcesForType(this.requirements).pipe(
      catchError(() => of(new Array<DataSource>())),
    ).subscribe(sources => {
      this.knownDataSources = sources;
      const options: Option[] = [];
      for (const ds of sources) {
        options.push({
          value: ds.dataSourceRef,
          text: ds.name
        });
      }

      this.dataSourceOptions.next(options);
    });

    // When the data source changes, look up the available translators so that
    // we can display any options if needed.
    this.form.controls.dataSourceRef.valueChanges.pipe(
      distinctUntilChanged(),
      switchMap(dataSourceRef => {
        if (dataSourceRef) {
          const foundDataSource = this.knownDataSources.find(ds => ds.dataSourceRef === dataSourceRef);
          if (foundDataSource) {
            return of(foundDataSource);
          } else {
            return this.chartDataService.lookup(dataSourceRef).pipe(catchError(() => of(<DataSource>null)));
          }
        } else {
          return of(<DataSource>null);
        }
      }),
    ).subscribe(dataSource => {
      if (!dataSource) {
        this.availableTranslators = [];
        this.translatorOptions = [];
      } else {
        this.availableTranslators = this.translatorService.getSuitableTranslatorsForSource(dataSource.dataType, this.requirements);
        this.translatorOptions = this.availableTranslators.map(t => <Option>{
          value: t.translatorId,
          text: t.name
        });
      }

      const selectedTranslatorId = this.selectedTranslatorId;
      if (!this.availableTranslators.find(t => t.translatorId === selectedTranslatorId)) {
        const firstTranslator = this.availableTranslators[0];
        console.log(`Translator ${selectedTranslatorId} not valid for this source`, firstTranslator);

        //TODO: move this translator switching code to a separate event?

        // Switching translators may result in the UI component providing
        // translator options changing. This can cause a number of problems (and
        // Angular bugs!), so we need to take some care here.
        //
        // If a component has registered validators (e.g. itself) on the
        // associated form control, these validators can hang around after the
        // component has been destroyed and continue to validate against a
        // defunct instance of the component.
        //
        // Second, there is a sort of 'race condition' here. When Angular
        // re-renders this component it will load the new translator UI and
        // destroy the old one, but before that happens, the old translator UI
        // will get this new input, and will try to validate it. Worse, if the
        // `writeValue()` method of the control causes a change event, this will
        // be propagated up to this control and can mangle our clean new config.
        //
        // The validation issue can be solved by discarding all validators on
        // that control. It will immediately update validity, and when the new
        // component gets loaded in, it should have a clean slate to add its own
        // validators.
        //
        // Second, the CVA should not propagate a change during writeValue().
        // This is essentially a Model -> View -> Model update and can have
        // quite confusing results, AND trigger rare Angular bugs such as if
        // replacing the FormControl instance within a form group.
        this.form.controls.translator.setValidators(null);
        this.form.patchValue({
          translatorId: firstTranslator ? firstTranslator.translatorId : null,
          translator: {}
        });
      }
    });

    // Rather than directly call onChanged() to pass the value along, we want to
    // debounce these changes, then pass the final value. The above logic
    // updates the translator based on the chosen data source, and this can
    // cause a cascade of changes to the form value, which don't get processed
    // in the right order.
    //
    // Using a debounce to defer these changes seems like the most robust
    // solution given the potential complexity of the form.
    this.form.valueChanges.subscribe(() => this.triggerOnChanged.next());

    this.triggerOnChanged.pipe(
      debounceTime(10)
    ).subscribe(() => {
      const formValue = this.form.value;
      const newDataSource: ChartDataSource = {
        id: formValue.id,
        dataSourceRef: formValue.dataSourceRef,
        filters: formValue.filters,
        globalFilterFieldMap: toFieldMap(formValue.globalFilterMapping),
        translator: formValue.translatorId ? {
          ...formValue.translator,
          id: formValue.translatorId
        } : null
      };

      this.onChanged(newDataSource);
    });
  }

  addCondition() {
    this.filtersArray.pushValue({
      fieldName: '',
      operation: 'EQUALTO',
      value: ''
    });
  }

  addGlobalFilterMapping() {
    this.globalFilterMapping.pushValue({
      dataSourceField: '',
      globalFilterId: ''
    });
  }

  toggleFilteringOptions() {
    this.showFilteringOptions = !this.showFilteringOptions;
  }

  validateSelf(control: AbstractControl) {
    if (this.form.valid) {
      return null;
    } else {
      return {
        'dataSourceInvalid': 'Data source configuration is not valid'
      };
    }
  }

  updateComponentValue(value: ChartDataSource) {
    this.form.reset(undefined, {emitEvent: false});
    if (value) {
      this.form.patchValue({
        ...value,
        translatorId: value.translator && value.translator.id,
        globalFilterMapping: toGlobalFilterEntryList(value.globalFilterFieldMap)
      }, {emitEvent: true});
    }
  }

  getFilterSummary() {
    const value = this.form.value;
    const filterConditions = value.filters
      .filter(c => c.fieldName && c.operation && c.value)
      .map(c => `${c.fieldName} ${c.operation} ${c.value}`);
    const globalFilterMapping = value.globalFilterMapping
      .filter(gf => gf.dataSourceField && gf.globalFilterId)
      .map(gf => `${gf.globalFilterId} as ${gf.dataSourceField}`);

    const clauses: string[] = [];
    if (filterConditions.length > 0) {
      clauses.push('Where: ' + filterConditions.join(', '));
    }

    if (globalFilterMapping.length > 0) {
      clauses.push('Allowing global: ' + globalFilterMapping.join(', '));
    }

    if (clauses.length > 0) {
      return clauses.join('; ');
    } else {
      return 'No filters active'
    }
  }
}

