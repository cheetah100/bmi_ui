import {
  Component,
  OnInit,
  forwardRef,
  OnDestroy,
} from '@angular/core';
import {
  FormControl,
  FormArray,
  ValidationErrors
} from '@angular/forms';
import { BaseControlValueAccessor, TypedFormGroup, TemplatedFormArray, provideAsValidatingCVA } from '@bmi/ui';
import { Option } from '@bmi/utils';
import { ColorSeries } from './color-series';
import { ChartDataWithSeries, ChartDataSeries } from '../../../chart-data';
import { ChartEditorService, ChartDataResponse } from '../../chart-editor.service';
import { Subscription } from 'rxjs';
import { debounceTime, filter, distinctUntilChanged } from 'rxjs/operators';
import { HighchartsSettings } from '../../../chart-highcharts/highcharts-settings';

/**
 * Basic highcharts: column, line, radar.
 * There will probably be more charts that can use this component, investigation needed.
 * Will add other chart settings editors for incompatable highcharts, also for charts using other 3rd party charting libraries
 */

export interface HighchartsColorZone {
  value?: number;
  color: string;
}

export interface HighchartsSeries {
  value: string;
  label?: string;
  type?: string;
  color?: string;
  connectNulls?: boolean;
  visible?: boolean;
  zoneAxis?: 'x' | 'y';
  zones?: HighchartsColorZone[];
}

@Component({
  selector: 'bmi-basic-highcharts-editor',
  templateUrl: './basic-highcharts-editor.component.html',
  styleUrls: ['./basic-highcharts-editor.component.scss'],
  providers: [provideAsValidatingCVA(forwardRef(() => BasicHighchartsEditorComponent))]
})
export class BasicHighchartsEditorComponent extends BaseControlValueAccessor<HighchartsSettings> implements OnInit, OnDestroy {

  emptySettings: HighchartsSettings = {
    chartTitle: 'Chart',
    xAxisTitle: 'X axis label',
    yAxisTitle: 'Y axis label',
    polar: false,
    stacked: undefined,
    selectedOnly: true,
    seriesSettings: [],
    type: 'column',
  };

  errorMessage: string = null;
  seriesMessage: string = null;

  readonly defaultColor = null;
  readonly seriesTypes = [
    'area',
    'areaspline',
    'column',
    'line',
    'scatter',
    'spline',
  ];
  readonly seriesThreshold = 8;
  unusedSeries: string[] = [];
  stackingOptions: Option[] = [
    { text: 'Do not stack', value: undefined, },
    { text: 'Normal stacking', value: 'normal', },
    { text: 'Percentage stacking', value: 'percent', },
  ];

  chartData: ChartDataWithSeries[];
  chartSettings: HighchartsSettings = this.emptySettings;

  private subscriptions: Subscription[] = [];

  get data(): ChartDataWithSeries {
    return this.chartData && this.chartData[0];
  }

  readonly form = new TypedFormGroup<HighchartsSettings>({
    chartTitle: new FormControl(null),
    xAxisTitle: new FormControl(null),
    yAxisTitle: new FormControl(null),
    type: new FormControl('column'),
    selectedOnly: new FormControl(true),
    polar: new FormControl(false),
    stacked: new FormControl(null),
    seriesSettings: new TemplatedFormArray<HighchartsSeries>(series => new TypedFormGroup({
      value: new FormControl(series.value),
      label: new FormControl(series.label),
      color: new FormControl(series.color),
      type: new FormControl(series.type),
      connectNulls: new FormControl(series.connectNulls),
      visible: new FormControl(series.visible),
      zoneAxis: new FormControl(series.zoneAxis),
      zones: new FormControl(series.zones)
    }))
  });

  get seriesSettings() {
    return this.form.controls.seriesSettings as TemplatedFormArray<HighchartsSeries>;
  }

  constructor(
    private chartEditorService: ChartEditorService
  ) {
    super();
  }

  ngOnInit() {
    this.subscriptions = [
      this.form.valueChanges.pipe(debounceTime(50)).subscribe(() => {
        const value = this.form.getRawValue();
        // console.log(this, "Propagating", value);
        this.onChanged(value)
      }),

      this.form.statusChanges.pipe(debounceTime(50)).subscribe(status => {
        this.needsRevalidation();
      }),

      this.chartEditorService.resolvedChartData.pipe(
        filter(Boolean),
        distinctUntilChanged((last: ChartDataResponse, changed: ChartDataResponse) => (
          last.dataSources[0].dataSourceRef === changed.dataSources[0].dataSourceRef
          && this.filtersMatch(changed.dataSources[0].filters, last.dataSources[0].filters)
        )),
      ).subscribe((chartData: ChartDataResponse) => {
        this.chartData = chartData.resolvedData as ChartDataWithSeries[];
        this.replaceAllChartSerieses();
      }),
    ];
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  filtersMatch(filters1, filters2): boolean {
    return (
      filters1.length === filters2.length
      && (
        !filters1.length
        || JSON.stringify(filters1) === JSON.stringify(filters2)
      )
    );
  }

  clearSeries() {
    this.seriesSettings.patchValue([]);
    this.updateUnusedSeries();
  }

  updateForm() {
    const settings = this.chartSettings;
    this.form.reset({}, { emitEvent: false });
    this.form.patchValue({
      xAxisTitle: settings.xAxisTitle,
      yAxisTitle: settings.yAxisTitle,
      chartTitle: settings.chartTitle,
      selectedOnly: settings.selectedOnly,
      polar: settings.polar,
      stacked: settings.stacked,
      seriesSettings: settings.seriesSettings || [],
      type: settings.type,
    }, { emitEvent: false });

    this.replaceAllChartSerieses();
  }

  replaceAllChartSerieses() {
    if (this.chartData) {
      const dataSeries = this.data.series;
      const existingSeries = this.seriesSettings.value || [];

      const results: HighchartsSeries[] = [];

      // When adding new series, we'll try and match the existing type. There
      // should be smarter logic here, but for now just choose the first
      // existing series with a type defined.
      const existingSeriesType = existingSeries.map(s => s.type).filter(type => !!type)[0];

      // Preserve any existing series, if they are present in the new data
      // source, retaining their configured order.

      existingSeries
        .filter(s => dataSeries.find(ds => ds.seriesId === s.value))
        .forEach(s => results.push(s));

      if (!results.length) {
        // no configured series. OR no matches between data and config series

        // Now add 'new' series up to the max number of series we want to
        // automatically show on a chart (to prevent things getting too janky or
        // outright crashing the tab)
        this.getUnusedSeries()
          .slice(0, this.seriesThreshold - results.length)
          .forEach(s => results.push(this.createSeriesWithDefaults(s, existingSeriesType)));

        if (results.length < dataSeries.length) {
          this.seriesMessage = `There are ${dataSeries.length} series in this data. Only adding ${this.seriesThreshold} to the chart view initially`;
        } else {
          this.seriesMessage = null;
        }

      }

      this.seriesSettings.setValue([], { emitEvent: false });
      this.seriesSettings.patchValue(results, { emitEvent: true });
    }

    this.updateUnusedSeries();
  }

  createSeriesWithDefaults(dataSeries: ChartDataSeries, seriesType = null): HighchartsSeries {
    return {
      value: dataSeries.seriesId,
      label: dataSeries.seriesId,
      type: seriesType,
      color: null,
      zones: null,
      zoneAxis: null,
      visible: true,
      connectNulls: false
    };
  }

  updateUnusedSeries() {
    this.unusedSeries = this.getUnusedSeries().map(s => s.seriesId);
  }

  addSeries(seriesId: string): void {
    const series = this.data && this.data.series.find(s => s.seriesId === seriesId);
    if (series) {
      this.seriesSettings.pushValue(this.createSeriesWithDefaults(series));
    }
    this.updateUnusedSeries();
  }

  addAllSeries() {
    this.seriesSettings.patchValue([
      ...this.seriesSettings.value,
      ...this.getUnusedSeries().map(s => this.createSeriesWithDefaults(s))
    ]);

    this.unusedSeries = [];
  }

  getUnusedSeries() {
    if (this.data) {
      const existingSeriesIds = new Set<string>(this.seriesSettings.value.map(s => s.value));
      return this.data.series.filter(series => !existingSeriesIds.has(series.seriesId));
    } else {
      return [];
    }
  }

  toggleZones(series: TypedFormGroup<HighchartsSeries>): void {
    if (series.value.zones) {
      series.controls.zones.clearValidators();
      series.controls.zones.reset();
    } else {
      series.patchValue({
        zones: [],
        zoneAxis: series.value.zoneAxis || 'y'
      });
    }
  }

  get typeOptions(): Option[] {
    return this.seriesTypes.map((t: string) => ({
      value: t,
      text: t,
    }));
  }

  randomColors() {
    const cs = new ColorSeries();
    const series = this.form.get('seriesSettings') as FormArray;
    const colorArray: string[] = cs.getSteps(series.length);
    series.controls.forEach((s, i) => {
      s.get('color').setValue(colorArray[i]);
    });
  }

  updateComponentValue(chartSettings: HighchartsSettings) {
    this.errorMessage = null;
    this.chartSettings = Object.assign({}, this.emptySettings, chartSettings);
    this.updateForm();
  }

  validateSelf(): ValidationErrors | null {
    if (this.form.valid) {
      return null;
    }
    return {
      invalid: 'Chart settings are not valid',
    }
  }

}
