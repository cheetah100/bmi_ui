export class ColorSeries {

  readonly colors: number[][] = [
    [0, 75, 175],
    [4, 159, 217],
    [196, 214, 237],
    [0, 173, 11],
    [171, 194, 51],
    [139, 16, 1],
    [255, 115, 0],
    [255, 160, 0],
    [128, 84, 200],
    [232, 109, 150],
    [255, 204, 0]
  ];

  selectComplementary(first: number[]): number[] {
    const [diff, index]: number[] = first.reduce((a, c, i) => {
      const offset = c - 128;
      if (Math.abs(offset) > Math.abs(a[0])) {
        return [offset, i];
      }
      return a;
    }, [0, 0]);
    const sign = (diff / Math.abs(diff));
    const complementaryColors = this.colors.filter(c => (c[index] - 128) * sign < 0);
    return this.selectRandom(complementaryColors);
  }

  selectRandom(options: number[][] = this.colors) {
    return options[Math.floor(Math.random() * options.length)];
  }

  selectRange(): number[][] {
    const first: number[] = this.selectRandom();
    const second: number[] = this.selectComplementary(first);
    return [first, second];
  }

  jumpElement(first: number, second: number, fraction: number): number {
    return Math.round(first + (second - first) * fraction);
  }

  jumpColor(first: number[], second: number[], fraction: number): number[] {
    return first.map((n: number, i: number) => this.jumpElement(first[i], second[i], fraction));
  }

  getSteps(steps: number): string[] {
    const [first, second] = this.selectRange();
    const range: number[][] = [first];
    for (let i = 1; i < steps - 1; i++) {
      range.push(this.jumpColor(first, second, i / (steps - 1)));
    }
    range.push(second);
    return range.map((c: number[]) => this.rgbToHex(c));
  }

  rgbToHex(rgb: number[]): string {
    const hex: string = rgb.map((n: number) => (n | 1 << 8)
      .toString(16)
      .toUpperCase()
      .slice(1))
      .join('');
    return `#${hex}`;
  }

}
