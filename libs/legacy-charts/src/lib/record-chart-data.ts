import { ChartData,  ChartDataSeries,  ChartDataWithSeries, SeriesDataPoint } from './chart-data';

import uniq from 'lodash-es/uniq';

export interface DataRecordTransform {
  columns: string[];
  data: DataRecord[];
}


export interface DataRecord {
  [key: string]: any;
}



export class RecordChartData implements ChartData<DataRecordTransform>, ChartDataWithSeries {
  type = 'data-records';
  public readonly categories: string[] = null;

  categoryIndexMap = new Map<string, number>();
  public readonly series: DataRecordSeries[] = [];

  constructor (
    public readonly data: DataRecordTransform,
    public readonly xAxisField: string,
    public readonly hasCategories: boolean = false,
    sortCategories: boolean = true
  ) {
    // If the data is categorical, then we need to extract these categories from
    // the transform so that they can be used when constructing the series data.
    // Highcharts doesn't do this for us automatically, as it expects the X axis
    // data to be numerical.
    const xAxisValues = data.data.map(r => r[xAxisField])
      .filter(value => value !== null && value !== undefined);
    const hasNonNumericalXValues = xAxisValues.some(v => typeof(v) !== 'number');
    if (!hasCategories && hasNonNumericalXValues) {
      throw new Error('Chart Data error: X axis contains non-numerical values, but this source is not configured as categorical data.')
    }
    if (hasCategories) {
      const categories = uniq(xAxisValues).map(value => String(value));
      // TODO: we might want more configurable sorting here. Perhaps the config
      // could list the order that categories should appear?
      if (sortCategories) {
        categories.sort((a, b) => a.localeCompare(b));
      }

      for (let i = 0; i < categories.length; i++) {
        this.categoryIndexMap.set(categories[i], i);
      }

      this.categories = categories;
    }
  }

  addSeries(seriesId: string, yAxis: string, nameField?: string) {
    this.series.push(
      new DataRecordSeries(this, seriesId, yAxis, nameField)
    );
  }
}


export class DataRecordSeries implements ChartDataSeries {
  constructor(
    private readonly chartData: RecordChartData,
    public readonly seriesId: string,
    private readonly yField,
    private readonly nameField = undefined
  ) {}

  asTuples(): [any, any][] {
    return this.asDatapoints().map(d => <[any,any]>[d.x, d.y]);
  }

  asDatapoints(): SeriesDataPoint[] {
    const xField = this.chartData.xAxisField;
    const yField = this.yField;
    const nameField = this.nameField;

    const transformData = this.chartData.data.data;
    const dataPoints = transformData
      .map(record => <SeriesDataPoint>{
        x: record[xField],
        y: record[yField],
        sourceRecord: record
      })
      // Filter out any data points with null/undefined X values. We should have
      // smarter handling of these, but this at least should avoid causing buggy
      // charts (highcharts doesn't handle these consistently.
      .filter(p => p.x !== null && p.x !== undefined);

    const dataPointsLength = dataPoints.length;
    if (nameField) {
      for (let i = 0; i < dataPointsLength; i++) {
        dataPoints[i].name = dataPoints[i].sourceRecord[nameField];
      }
    }

    if (this.chartData.hasCategories) {
      const indexMap = this.chartData.categoryIndexMap;
      for (let i = 0; i < dataPointsLength; i++) {
        const point = dataPoints[i];
        point.x = indexMap.get(point.x);
      }
    }

    // Some chart types expect the X axis to be sorted (e.g. stacked charts in
    // Highcharts.) This is a bit of extra computation, but it should ensure that data
    // series from arbitrary transforms should be compatible
    dataPoints.sort((a, b) => a.x - b.x);
    return dataPoints;
  }
}
