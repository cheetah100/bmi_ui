import { NgModule, ModuleWithProviders, Inject, InjectionToken, Optional } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedUiModule } from '@bmi/ui';

import { ChartType } from './chart-type';
import { ChartTypeService, BMI_CHART_TYPE } from './chart-type.service';

import { ChartViewerComponent } from './chart-viewer/chart-viewer.component';
import { HighchartsChartViewComponent } from './chart-highcharts/highcharts-chart-view.component';

import { HighchartsChartTypeOptions } from './chart-highcharts/highcharts-settings';

import { ChartDataSourceService } from './data-sources/chart-data-source.service';
import { JsonDataSourceService } from './data-sources/json-data-source.service';

import { CHART_DATA_TRANSLATORS } from './chart-data-translators/data-translator';
import { ChartDataTranslatorService } from './chart-data-translators/chart-data-translator.service';

import { PivotDataTranslator } from './chart-data-translators/pivot-data-translator';
import { SeriesMapperTranslator } from './chart-data-translators/series-mapper-translator';

import { GravityModule, DATA_SOURCE_REPOSITORIES } from '@bmi/gravity-services';

import * as Highcharts from 'highcharts';
import hcMore from 'highcharts/highcharts-more.src';


type HighchartsModule = (hc: typeof Highcharts) => void;

/**
 * This token can be used to optionally inject additional Highcharts modules to
 * load. This is treated as a multi provider, allowing apps to add additional
 * modules.
 *
 * These modules take effect globally, so if the app has another instance of
 * Highcharts, or loads modules through another mechanism (e.g.
 * angular-highcharts) then these modules will (unfortunately) affect BMI Charts
 * too.
 *
 * Unfortunately, trying to load these Highcharts modules within the BMI Charts
 * Angular library causes issues with tree-shaking optimizations: the libraries
 * will not initialize properly.
 */
export const BMI_CHARTS_HIGHCHARTS_MODULE = new InjectionToken<HighchartsModule>('BMI Charts Highcharts Modules');


@NgModule({
  imports: [
    CommonModule,
    SharedUiModule,
    GravityModule
  ],
  declarations: [
    ChartViewerComponent,
    HighchartsChartViewComponent
  ],

  exports: [
    ChartViewerComponent,
  ]
})
export class BmiChartsModule {
  constructor(@Optional() @Inject(BMI_CHARTS_HIGHCHARTS_MODULE) hcModules: HighchartsModule[]) {
    // Load any libraries needed for built-in Highcharts options. The
    // 'highcharts-more' module is needed for polar charts (and probably
    // others). It would be more elegant if we could just use DI for this, but
    // there seems to be an Angular/Webkit bug that breaks this when using build
    // optimization --- { provide: ..., useValue: hcMore } from this module
    // results in a null value being injected. This seems like a bug with the
    // Angular compiler not handling the case of providing an imported value
    // directly like that.
    hcMore(Highcharts);

    // This mechanism just makes it convenient to load any further modules that
    // the app might need
    if (Array.isArray(hcModules)) {
      hcModules.forEach(mod => mod(Highcharts));
    }
  }

  static forRoot(): ModuleWithProviders<BmiChartsModule> {
    return {
      ngModule: BmiChartsModule,
      providers: [
        ChartTypeService,
        ChartDataSourceService,
        ChartDataTranslatorService,

        // Register the chart data translator services
        {
          provide: CHART_DATA_TRANSLATORS,
          multi: true,
          useClass: PivotDataTranslator
        },

        {
          provide: CHART_DATA_TRANSLATORS,
          multi: true,
          useClass: SeriesMapperTranslator
        },

        { provide: DATA_SOURCE_REPOSITORIES, multi: true, useClass: JsonDataSourceService },

        // Here we register the chart types
        {
          provide: BMI_CHART_TYPE,
          multi: true,
          useValue: {
            id: '@bmi/legacy-charts:highcharts-basic-chart-v1',
            chartEngine: 'highcharts',
            name: 'Basic Chart',
            options: <HighchartsChartTypeOptions>{
              type: 'column'
            }
          }
        }
      ]
    }
  }
}
