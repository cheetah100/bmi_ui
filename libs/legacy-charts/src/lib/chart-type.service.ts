import { Injectable, InjectionToken, Inject, Optional } from '@angular/core';

import { ChartType } from './chart-type';

export const BMI_CHART_TYPE = new InjectionToken<ChartType>('BMI Chart Types');

@Injectable()
export class ChartTypeService {
  private chartTypes = new Map<string, ChartType>();

  constructor(
    @Optional() @Inject(BMI_CHART_TYPE) injectedChartTypes: ChartType[]
  ) {
    if (injectedChartTypes) {
      injectedChartTypes.forEach(ct => this.registerChartType(ct));
    }
  }

  /**
   * Gets the chart type corresponding to the id.
   *
   * If the chart type doesn't exist, this will throw an error.
   */
  public getChartType(id: string): ChartType {
    if (!this.chartTypes.has(id)) {
      throw new Error(`Chart type ${id} is not supported`);
    } else {
      return this.chartTypes.get(id);
    }
  }

  /**
   * Gets all registered chart types
   */
  public getChartTypes(): ChartType[] {
    return Array.from(this.chartTypes.values());
  }

  private registerChartType(chartType: ChartType): void {
    if (!chartType || !chartType.id) {
      throw new Error('No chart type ID specified');
    } else if (this.chartTypes.has(chartType.id)) {
      throw new Error(`Chart type ${chartType.id} is already registered`);
    } else {
      this.chartTypes.set(chartType.id, chartType);
    }
  }
}
