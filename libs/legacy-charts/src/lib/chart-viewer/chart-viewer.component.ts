import { Component, Input, OnChanges } from '@angular/core';

import { ChartConfig } from '../chart-config';
import { ChartType } from '../chart-type';
import { ChartData } from '../chart-data'

import { ChartTypeService } from '../chart-type.service';

/**
 * Displays a BMI Chart
 *
 * This is the host component for displaying a BMI chart. When passed a chart
 * config and chart data as inputs, it will display the appropriate type of
 * chart component.
 */
@Component({
  selector: 'bmi-chart-viewer',
  template: `
    <ng-container *ngIf="config && data && chartType">
      <bmi-highcharts-chart-view [config]="config" [data]="data" [chartType]="chartType"></bmi-highcharts-chart-view>
    </ng-container>
  `
})
export class ChartViewerComponent implements OnChanges {
  @Input() config: ChartConfig;
  @Input() data: ChartData[];

  public chartType: ChartType;

  constructor(
    private chartTypeService: ChartTypeService
  ) { }

  ngOnChanges(changes) {
    this.onChartChanges();
  }

  onChartChanges() {
    this.chartType = this.chartTypeService.getChartType(this.config.chartType);
  }
}
