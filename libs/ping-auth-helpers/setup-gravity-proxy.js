
/**
 * Creates the proxy config for mounting a Gravity instance
 *
 * This config is compatible with http-proxy-middleware and thus can be used
 * with the Webpack devserver (e.g. in the proxy config for an Angular app).
 *
 * If the authToken parameter is provided, this will add an onProxyReq function
 * to inject an authorization header. The value can be a string, or it can be a
 * PingClientAuthToken, which will provide a Bearer token value.
 *
 * @param {string} targetUrl the target Gravity instance to proxy to, usually
 * this would include the /gravity portion of the URL
 * @param {string} path the local proxy path, where to mount this instance (e.g.
 * /gravity). This is used for rewriting request URLs
 * @param {?} authToken a string bearer token, or a PingClientAuth token object.
 * @returns a proxy config object
 */
function makeGravityProxyConfig(targetUrl, path = '/gravity', authToken = null) {
  const config = {
    "target": targetUrl,
    "secure": true,
    "pathRewrite": {[`^${path}`] : ""},
    "changeOrigin": true,
    "proxyTimeout": 60000,
    "logLevel": "error"
  };

  if (authToken) {
    config.onProxyReq = (proxyReq, req, res) => {
      const bearerToken = typeof authToken === 'string' ? authToken : authToken.bearerToken;
      if (bearerToken) {
        proxyReq.setHeader('Authorization', `Bearer ${bearerToken}`);
      }
    };
  }

  return config;
}

module.exports = {
  makeGravityProxyConfig
};
