#!/usr/bin/env node

const commander = require('commander');
const fs = require('fs');
const { PingClientAuthToken } = require('./ping-client-auth-token');
const { loadSecretsConfig } = require('./load-secrets-config');

// TODO: Specify the following 2 consts
const PROD_SSO_TOKEN_URL = 'PROD_SSO_TOKEN_URL';
const NPROD_SSO_TOKEN_URL = 'NPROD_SSO_TOKEN_URL';

commander
  .option('--prod', 'Use the prod CloudSSO server')
  .option('--nprod', 'Use the nonprod CloudSSO server (this is the default)')
  .option('--token-url <url>', 'Use a custom token URL (overrides --prod or --nprod)')
  .arguments("<secretsFile>")
  .parse(process.argv);


let tokenUrl;
if (commander.prod) {
  tokenUrl = PROD_SSO_TOKEN_URL;
} else if (commander.tokenUrl) {
  tokenUrl = commander.tokenUrl;
} else {
  tokenUrl = NPROD_SSO_TOKEN_URL;
}

const secretsFileName = commander.args[0];

try {
  const secretsConfig = loadSecretsConfig(secretsFileName);
  const pingToken = new PingClientAuthToken(tokenUrl, secretsConfig, false);
  pingToken.refreshToken().then(token => {
    console.log(token.access_token);
    process.exit(0);
  });
} catch (e) {
  console.error(`ping-client-auth: ${e.message}`);
  process.exit(1);
}
