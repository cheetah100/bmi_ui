const fs = require('fs');

/**
 * Loads a client secrets JSON config, with some error handling.
 * 
 * This will check if the client_secret property is defined on the 
 * config object.
 * 
 * If the file is not found, or cannot be loaded an error is raised.
 * 
 * @param {string} filename 
 * @returns a JSON config
 */
function loadSecretsConfig(filename) {
  if (!filename) {
    throw new Error('Secrets file not specified');
  }

  if (!fs.existsSync(filename)) {
    throw new Error(`Secrets file not found: ${filename}`);
  }

  try {
    const contents = fs.readFileSync(filename).toString();
    const config = JSON.parse(contents);

    if (!config.client_secret) {
      throw new Error('client_secret property is not defined');
    }

    return config;
  } catch (e) {
    throw new Error('Error loading secrets file: ' + e.message);
  }
}

module.exports = {
  loadSecretsConfig
};