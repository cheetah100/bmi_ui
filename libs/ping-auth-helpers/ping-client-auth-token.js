const got = require('got');


function httpBasicAuth(secret, userId = undefined) {
  let authToken;
  if (secret && userId) {
    authToken = Buffer.from(`${userId}:${secret}`).toString('base64');
  } else if (secret) {
    authToken = secret;
  } else {
    throw new Error('Cannot perform HTTP Basic Auth: must supply encoded secret, or userId and secret')
  }

  return `Basic ${authToken}`;
}


/**
 * A self-refreshing Ping client auth token.
 *
 * Once start() has been called, this will refresh on an interval (default: 30 minutes).
 * A better solution would be to use the token expiration time to affect
 * the schedule, but this is simple enough.
 *
 * The Config object for this should satisfy the type:
 *
 * interface ClientAuthSecret {
 *   client_id?: string;
 *   client_secret: string;
 *   refreshMinutes?: number;
 * }
 *
 * If both client_id and client_secret are provided, these will be used to
 * generate HTTP Basic Auth credentials. If just client_secret is provided,
 * this is assumed to be an already encoded Basic auth credentials, and will be
 * used directly.
 *
 * The refreshMinutes option allows the refresh time to be customized, but by
 * default this will be 30 minutes.
 */
class PingClientAuthToken {

  /**
   * Create a Ping Client Auth token.
   * @param {string} tokenUrl the token URL
   * @param {*} config the config object containing the credentials
   * @param {*} autoRefresh if true (default) this token will auto refresh. Call stop() to end this.
   */
  constructor(tokenUrl, config, autoRefresh = true) {
    this.tokenUrl = tokenUrl;
    this.accessToken = null;
    this.config = config;
    this._refreshIntervalId = null;
    if (autoRefresh) {
      this.refreshToken();
      this.start();
    }
  }

  start() {
    this.stop();
    const refreshMinutes = this.config.refreshMinutes || 30;
    this._refreshIntervalId = setInterval(() => this.refreshToken(), refreshMinutes * 60 * 1000);
  }

  /**
   * Cancels any further refreshes
   */
  stop() {
    if (this._refreshIntervalId) {
      clearInterval(this._refreshIntervalId);
      this._refreshIntervalId = null;
    }
  }

  refreshToken() {
    return got.post(this.tokenUrl, {
      form: true,
      body: {
        grant_type: "client_credentials",
      },
      headers: {
        "Authorization": httpBasicAuth(this.config.client_secret, this.config.client_id)
      }
    }).then(response => {
      const token = JSON.parse(response.body);
      this.accessToken = token;
      return token;
    }).catch(error => {
      console.error('PingClientAuthToken error:', error.message, error);
    });
  }

  get bearerToken() {
    return this.accessToken ? this.accessToken.access_token : null;
  }
}


module.exports = {
  PingClientAuthToken
};
