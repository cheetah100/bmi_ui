import { guessPlural, guessSingular } from './plurals';


describe('guessSingular()', () => {
  it('should provide a rules generated singular verison of the provided word', () => {
    expect(guessSingular('capabilities')).toBe('capability');
    expect(guessSingular('capability')).toBe('capability');
    expect(guessSingular('processes')).toBe('process');
    expect(guessSingular('process')).toBe('process');
    expect(guessSingular('mindsets')).toBe('mindset');
    expect(guessSingular('mindset')).toBe('mindset');
  });
});

describe('guessPlural()', () => {
  it('should provide a rules generated plural verison of the provided word', () => {
    expect(guessPlural('capabilities')).toBe('capabilities');
    expect(guessPlural('capability')).toBe('capabilities');
    expect(guessPlural('processes')).toBe('processes');
    expect(guessPlural('process')).toBe('processes');
    expect(guessPlural('mindsets')).toBe('mindsets');
    expect(guessPlural('mindset')).toBe('mindsets');
  });
});
