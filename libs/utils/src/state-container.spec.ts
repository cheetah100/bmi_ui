import { Subject, Subscription } from 'rxjs';
import { StateContainer } from './state-container';
import { ObservableWatcher } from './observable-watcher';


interface ExampleState {
  foo: string;
  bar: string;
  nested?: {
    a: number;
    b: boolean;
  }
}


describe('State Container', () => {
  let initialState: ExampleState;
  let state: StateContainer<ExampleState>;
  let watch: ObservableWatcher<ExampleState>;
  let fooWatcher: ObservableWatcher<string>;
  let barWatcher: ObservableWatcher<string>;

  beforeEach(() => {
    initialState = {
      foo: 'Hello World',
      bar: 'Testing'
    };
    state = new StateContainer(initialState);
    watch = new ObservableWatcher(state.state);
    fooWatcher = new ObservableWatcher(state.select(s => s.foo));
    barWatcher = new ObservableWatcher(state.select(s => s.bar));
  });

  it('should expose the current state', () =>
    expect(state.currentState).toEqual(initialState));

  it('should emit the initial state first', () =>
    expect(watch.values).toEqual([initialState]));

  describe('setting values', () => {
    beforeEach(() => {
      state.set({
        bar: 'Updated'
      })
    });

    it('should update the current state', () =>
      expect(state.currentState).toEqual({foo: 'Hello World', bar: 'Updated'}));

    it('should emit the changed state', () =>
      expect(watch.latestValue).toBe(state.currentState));

    it('should not cause a select to fire if the values did not change', () =>
      expect(fooWatcher.values).toEqual(['Hello World']));

    it('should fire a select if the value did change', () =>
      expect(barWatcher.values).toEqual(['Testing', 'Updated']));
  });

  describe('State modifiers', () => {
    let modifierSubject: Subject<Partial<ExampleState>>;
    let modifierSub: Subscription;

    beforeEach(() => {
      modifierSubject = new Subject();
      modifierSub = state.addModifier(modifierSubject);
    });

    it('should not emit anything after adding a modifier which has not emitted yet', () =>
      expect(watch.values.length).toBe(1));

    describe('after emitting an update', () => {
      beforeEach(() => modifierSubject.next({bar: 'Updated by modifier'}));

      it('should emit the updated value', () =>
        expect(watch.latestValue.bar).toBe('Updated by modifier'));

      it('should update after subsequent modifications', () => {
        modifierSubject.next({bar: 'Update 2'});
        expect(watch.values.map(s => s.bar)).toEqual(['Testing', 'Updated by modifier', 'Update 2'])
      });

      it('should revert the modifications once the modifier is unsubscribed', () => {
        modifierSub.unsubscribe();
        expect(watch.latestValue).toEqual(initialState);
      });
    });
  })
});
