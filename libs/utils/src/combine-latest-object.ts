import { combineLatest, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

type ObjectWithObservableValues<T> = {[P in keyof T]: Observable<T[P]>};

/**
 * RxJS combineLatest, but with an object rather than array.
 *
 * This is a wrapper around combineLatest that takes an object of observables,
 * and emits a corresponding object of the latest values from each source, just
 * like combineLatest does.
 *
 * Conveniently, this preserves type information which makes it really nice to
 * use.
 *
 * This feature is coming in a future version of RxJS, but we can have it now.
 */
export function combineLatestObject<T>(observables: ObjectWithObservableValues<T>): Observable<T> {
  const keys = Object.keys(observables);
  if (keys.length === 0) {
    return of<T>({} as T);
  } else {
    return combineLatest(keys.map(k => observables[k])).pipe(
      map(results => {
        const resultObj = {};
        keys.forEach((key, index) => resultObj[key] = results[index]);
        return resultObj as T;
      })
    );
  }
}
