import { parseInteger } from './parse-integer';


describe('parseInteger()', () => {
  it('should return a numerical value unchanged, including whole numbers', () => {
    expect(parseInteger(-1.2)).toBe(-1);
    expect(parseInteger(0.9001)).toBe(0);
    expect(parseInteger(3e6)).toBe(3000000);
  });
  it('should return the default for a non-numerical string', () => {
    expect(parseInteger('q?2~~~')).toBeNaN();
    expect(parseInteger('~~~', 2)).toBe(2);
  });
  it('should return a parsed integer for a numerical string', () =>{
    expect(parseInteger('9001')).toBe(9001);
    expect(parseInteger(-2.9)).toBe(-2);
    expect(parseInteger('9000.1')).toBe(9000);
  });
});
