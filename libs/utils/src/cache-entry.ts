import { Observable, BehaviorSubject, Subject, merge, EMPTY, of, combineLatest, defer } from 'rxjs';
import { catchError, distinctUntilChanged, filter, finalize, map, materialize, share, startWith, switchMap, tap } from 'rxjs/operators';

import lodashDefaults from 'lodash-es/defaults';


export interface CacheEntryState<T> {
  value: T;
  error?: any;
  isValid: boolean;
  updatedTimestamp: number;
  refreshInProgress: boolean;
  refreshRequested: boolean;
}


export interface CacheEntryOptions<T> {
  source?: () => Observable<T>;
  considerRefreshTrigger?: Observable<any>;
  getCurrentTime?: () => number;
  maxAge?: number;
  emitStaleEntries?: boolean;
}


export interface CacheEntryWatchOptions {
  refreshOnSubscribe?: boolean;
  invalidateOnSubscribe?: boolean;
  throwErrors?: boolean;
  maxAge?: number;
  emitStaleEntries?: boolean;
}


/**
 * Manages the state of a cached value from an observable source
 *
 * This is the foundation of the ObservableMap object and can be used standalone
 * for managing a single data item.
 *
 * This handles refreshing the data, invalidation and deciding if the data is
 * stale. When a refresh is triggered, it will call the source generator
 * function again, and subscribe to the returned observable.
 *
 * This object doesn't mantain any subscriptions to the source; instead it
 * relies on watchers. If nothing is watching it will unsubscribe from the
 * source (this is handled internally by publish/refcount operators)
 *
 * You can use this object without providing a source function. In this mode,
 * the data can be manually set by calling update(). Obviously no refreshing
 * will happen in this case!
 *
 * When using a source, it will listen for multiple signals that it might need
 * to refresh the data -- calling refresh() will trigger a refresh if anything
 * is watching, or will mark that a refresh was requested so that the next
 * subscription will refresh.
 *
 * You can also set an external signal to 'consider' refreshing. The intended
 * use for this is to connect it to an interval() observable source (potentially
 * shared amongst many cache entries) -- when this observable emits, the cache
 * will consider if a refresh is necessary, but only if something is watching.
 *
 * This reactive system means that a big cache (e.g. many many cards) will only
 * listen to the interval if something is subscribed, the dormant cards will
 * have no overhead.
 */
export class CacheEntry<T> {
  private _state = new BehaviorSubject<CacheEntryState<T>>({
    value: undefined,
    isValid: false,
    refreshInProgress: false,
    refreshRequested: false,
    updatedTimestamp: undefined
  });

  private options: Readonly<CacheEntryOptions<T>>;
  private sourcePipeline: Observable<any>;
  private manualRefreshTrigger = new Subject<string>();

  get considerRefreshTrigger() {
    return this.options.considerRefreshTrigger || EMPTY;
  }

  readonly isLoading = this._state.pipe(
    map(s => s.refreshInProgress),
    distinctUntilChanged()
  );

  constructor(options: CacheEntryOptions<T> = {}) {
    this.options = lodashDefaults({}, options, {
      emitStaleEntries: true,
      maxAge: null,
      getCurrentTime: () => new Date().getTime()
    });

    if (this.options.source) {
      // We don't want our cache entry to subscribe directly to the upstream
      // source -- we want it to be refcounted so that refreshes/updates only
      // happen when something is looking.
      //
      // The publish/refcount is doing the heavy lifting here! It will ensure
      // there's only one subscription to the source/refresh triggers; and in
      // watch() we subscribe to this pipeline to register interest.
      //
      // This first merge combines together all of the triggers that
      // might cause a refresh to happen:
      this.sourcePipeline = merge(
        // When the source pipeline is 'activated' by a subscription, we'll
        // always consider a refresh.
        of('start'),

        // The manual trigger allows the external API to trigger a refresh ---
        // this is also used when an entry is manually invalidated.
        this.manualRefreshTrigger,

        // This can also be externally triggered, e.g. using an interval timer
        // or something.
        this.considerRefreshTrigger
      ).pipe(
        filter(() => this.needsRefresh && !this.stateSnapshot.refreshInProgress),
        tap(() => this.markRefreshInProgress()),
        switchMap(() => this.options.source().pipe(
          finalize(() => { this.markRefreshCompleted() }),
          materialize(),
        )),
        tap(notification => {
          if (notification.kind === 'N') {
            this.update(notification.value);
          } else if (notification.kind === 'E') {
            this.setError(notification.error);
          }
        }),
        share()
      );
    } else {
      this.sourcePipeline = EMPTY;
    }
  }

  update(value: T): void {
    this.updateState({
      value: value,
      error: undefined,
      isValid: true,
      refreshInProgress: false,
      updatedTimestamp: this.currentTimestamp()
    });
  }

  setError(error: any) {
    this.updateState({
      error: error,
      isValid: false,
      refreshInProgress: false,
    })
  }

  invalidate(): void {
    this.updateState({
      isValid: false
    });
    this.refresh();
  }

  isStale(state = this.stateSnapshot, maxAge = this.options.maxAge): boolean {
    if (state.updatedTimestamp === null || state.updatedTimestamp === undefined) {
      return true;
    } else if (maxAge) {
      const age = this.currentTimestamp() - state.updatedTimestamp;
      return age >= maxAge;
    } else {
      return false;
    }
  }

  get needsRefresh() {
    const state = this.stateSnapshot;
    return !state.isValid || state.refreshRequested || this.isStale(state);
  }

  get stateSnapshot(): CacheEntryState<T> {
    return this._state.value;
  }

  get isValid() {
    return this.stateSnapshot.isValid;
  }

  get error() {
    return this.stateSnapshot.error;
  }

  get hasError() {
    return !!this.error;
  }

  refresh(): void {
    this.markAsRefreshRequested();
    this.manualRefreshTrigger.next('refresh');
  }

  watch(options?: CacheEntryWatchOptions): Observable<T> {
    const opts = lodashDefaults({}, options, {
      emitStaleEntries: this.options.emitStaleEntries,
      maxAge: this.options.maxAge,
      throwErrors: false,
    });

    const shouldEmitStaleValues = opts.emitStaleEntries;
    const staleAge = opts.maxAge;

    return defer(() => {
      if (opts.invalidateOnSubscribe) {
        this.invalidate();
      } else if (opts.refreshOnSubscribe) {
        this.refresh();
      }

      return combineLatest([
        this._state,

        // We don't actually care about this value, but we're using this for
        // subscription management -- we only want to refresh and fetch stuff if
        // something is watching.
        this.sourcePipeline.pipe(startWith(null))
      ]).pipe(
        map(([state,]) => state),
        filter(state =>
          (state.isValid && (shouldEmitStaleValues || !this.isStale(state, staleAge)))
          || (state.error && opts.throwErrors)
        ),
        map(state => {
          if (!!state.error && opts.throwErrors) {
            throw state.error;
          } else {
            return state.value;
          }}),
        distinctUntilChanged(),
      );
    });
  }

  private updateState(state: Partial<CacheEntryState<T>>) {
    this._state.next({
      ...this.stateSnapshot,
      ...state
    });
  }

  private currentTimestamp(): number {
    return this.options.getCurrentTime();
  }

  private markRefreshInProgress() {
    this.updateState({
      error: undefined,
      refreshInProgress: true,
      refreshRequested: false
    });
  }

  private markRefreshCompleted() {
    this.updateState({
      refreshInProgress: false
    });
  }

  private markAsRefreshRequested() {
    this.updateState({
      refreshRequested: true
    });
  }
}
