import { LoadingTaskPool } from './loading-task-pool';

describe('Loading task pool', () => {
  it('should not be loading if there are no tasks in progress', () => {
    const taskPool = new LoadingTaskPool(0);
    expect(taskPool.isLoading).toBe(false);
  });

  it('should be loading if there is a task in progress', () => {
    const taskPool = new LoadingTaskPool(0);
    taskPool.startTask();
    expect(taskPool.isLoading).toBe(true);
  });

  it('should not be loading once active tasks have been completed', () => {
    const taskPool = new LoadingTaskPool(0);
    const task1 = taskPool.startTask();
    expect(taskPool.isLoading).toBe(true);
    const task2 = taskPool.startTask();
    task1.complete();
    expect(taskPool.isLoading).toBe(true);
    task2.complete();
    expect(taskPool.isLoading).toBe(false);
  });

  it('should time out if tasks are not completed in time', done => {
    // Kill tasks if not complete after 50msec
    const taskPool = new LoadingTaskPool(50);
    const task = taskPool.startTask();

    // Wait a bit; the task should be killed by now
    setTimeout(() => {
      expect(taskPool.isLoading).toBe(false);
      done();
    }, 200);
  });

  it('should trigger a tasks updated event when a task starts', () => {
    const taskPool = new LoadingTaskPool(0);
    let updateCount = 0;
    taskPool.onTasksUpdated.subscribe(() => {
      updateCount += 1;
    });

    taskPool.startTask();
    expect(updateCount).toBe(1);
  });

  it('should trigger a tasks updated event when a task starts and ends', () => {
    const taskPool = new LoadingTaskPool(0);
    let updateCount = 0;
    taskPool.onTasksUpdated.subscribe(() => {
      updateCount += 1;
    });

    const task = taskPool.startTask();
    task.complete();
    expect(updateCount).toBe(2);
  });

  it('should be incomplete before any tasks are started', () => {
    const taskPool = new LoadingTaskPool(0);
    expect(taskPool.isComplete).toBe(false);
  });

  it('should be incomplete while tasks are in progress', () => {
    const taskPool = new LoadingTaskPool(0);
    const task = taskPool.startTask();
    expect(taskPool.isComplete).toBe(false);
  });

  it('should be complete once all tasks are finished', () => {
    const taskPool = new LoadingTaskPool(0);
    const task1 = taskPool.startTask();
    const task2 = taskPool.startTask();
    task2.complete();
    expect(taskPool.isComplete).toBe(false);
    task1.complete();
    expect(taskPool.isComplete).toBe(true);
  });

  it('should allow tasks to be marked as complete multiple times', () => {
    const taskPool = new LoadingTaskPool(0);
    const task = taskPool.startTask();
    task.complete();
    task.complete();
    expect(taskPool.isComplete).toBe(true);
  });

  it('should allow task names to be set', () => {
    const taskPool = new LoadingTaskPool(0);
    const task = taskPool.startTask('Test Task');
    expect(task.name).toBe('Test Task');
  });

  it('should notify observable once all tasks have completed', () => {
    const taskPool = new LoadingTaskPool(0);
    let completionCount = 0;
    taskPool.onCompleted.subscribe(() => completionCount++);
    const task = taskPool.startTask();
    const task2 = taskPool.startTask();
    task2.complete();
    expect(completionCount).toBe(0);
    task.complete();
    expect(completionCount).toBe(1);
  });

  it('should notify completion observable after task pool is completed too', () => {
    const taskPool = new LoadingTaskPool(0);
    taskPool.startTask().complete();

    let completionCount = 0;
    taskPool.onCompleted.subscribe(() => completionCount++);
    expect(completionCount).toBe(1);
  });

  it('should trigger onTaskTimedOut observable when a task times out', done => {
    const taskPool = new LoadingTaskPool(50);
    const task = taskPool.startTask('Doomed to fail');

    const timeoutId = setTimeout(() => {
      fail();
      done();
    }, 200);

    taskPool.onTaskTimedOut.subscribe(timedOutTask => {
      clearTimeout(timeoutId);
      expect(timedOutTask).toBe(task);
      done();
    });
  });

  it('should notify onTaskFinished when a task is completed', () => {
    const taskPool = new LoadingTaskPool(0);
    const task = taskPool.startTask();

    let finishedTask = null;
    let finishCount = 0;
    taskPool.onTaskFinished.subscribe(t => {
      finishCount += 1;
      finishedTask = t;
    });

    task.complete();

    expect(finishedTask).toBe(task);
    expect(finishCount).toBe(1);
  });
});
