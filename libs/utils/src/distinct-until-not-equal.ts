import { MonoTypeOperatorFunction } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';
import lodashIsEqual from 'lodash-es/isEqual';


/**
 * A variation of the distinctUntilChanged operator which compares using Lodash
 * isEqual.
 */
export function distinctUntilNotEqual<T>(): MonoTypeOperatorFunction<T> {
  return distinctUntilChanged<T>((a, b) => lodashIsEqual(a, b));
}
