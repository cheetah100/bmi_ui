import { Observable, BehaviorSubject, ReplaySubject, Subject, Subscription, EMPTY } from 'rxjs'
import { catchError, filter, finalize, switchMap, take, tap, } from 'rxjs/operators';


interface CacheEntry<T> {
  data: Subject<T>;
  triggerRefresh: Subject<void>;
  refreshInProgress?: boolean;
  lastUpdatedTimestamp?: number;
}


interface ObservableCacheOptions<T, TKey> {
  factory: (key: TKey) => Observable<T>;
  subjectFactory?: () => Subject<T>;
  handleError?: (key: TKey, error: any) => Observable<T>;
}

/**
 * Caches the output of observables across a domain of values.
 *
 * The intended use for this is caching the result of many requests based on a
 * parameter, which is the key of this map. For example, caching data for each
 * board in IDP, but where you want to be able to show previously fetched values
 * before the latest data becomes available.
 *
 * You can customize the behaviour of this cache by providing a subject factory
 * as the second parameter in the constructor; similar to the multicast()
 * operator in RxJS. By default, a ReplaySubject(1) is used, so that the cache
 * always emits the latest value, but will wait for the first refresh.
 */
export class ObservableCache<T, TKey = string> {
  private cache = new Map<TKey, CacheEntry<T>>();
  private subscriptions: Subscription[] = [];

  private options: ObservableCacheOptions<T, TKey> = {
    factory: null,
    handleError: () => EMPTY,
    subjectFactory: () => new ReplaySubject<T>(1)
  };

  /**
   * Creates an observable cache.
   *
   * @param observableFactory fetches the data associated with a particular key
   * @param subjectFactory creates a subject for handling the cached data
   * @param handleError is called to provide the result for a catchError()
   *     operator. By default this returns EMPYT to silently handle errors
   */
  constructor(factoryOrOptions: ((key: TKey) => Observable<T>) | ObservableCacheOptions<T, TKey>) {
    if (typeof factoryOrOptions === 'function') {
      this.options.factory = factoryOrOptions;
    } else {
      this.options = {
        ...this.options,
        ...factoryOrOptions,
      };
    }
  }

  /**
   * Complete all of the cache subjects, then unsubscribe from the sources.
   *
   * After calling this method, the cache will be emptied, and any existing
   * subscriptions will have ended. Further refreshing or watching may recreate
   * these resources, but the behaviour after the cache has been destroyed isn't
   * guaranteed and shouldn't be relied on.
   */
  destroy() {
    for (const entry of this.cache.values()) {
      entry.data.complete();
    }

    this.subscriptions.forEach(s => s.unsubscribe());

    this.cache.clear();
    this.subscriptions.splice(0);
  }

  /**
   * Watches a key in the cache for updates.
   *
   * Calling this will not trigger an update to happen; to do that, call the
   * refresh() method.
   *
   * @param key the key to watch
   */
  watch(key: TKey): Observable<T> {
    return this.getCacheEntry(key).data.asObservable();
  }

  /**
   * Refreshes a cache key from the observable source.
   *
   * Use the watch(key) method to see the results.
   *
   * @param key the key to refresh data for.
   * @param force a refresh, even if there's already one in progress
   */
  refresh(key: TKey, force = false) {
    const entry = this.getCacheEntry(key);
    if (force) {
      entry.refreshInProgress = false;
    }
    entry.triggerRefresh.next();
  }

  /**
   * Checks the freshness of a cache entry, returning time since last updated
   *
   * This returns the duration since the last successful update (in
   * milliseconds), or null if the cache entry doesn't exist or hasn't been
   * refreshed yet.
   *
   * @param key the cache key to check
   * @returns the time since last successfully updated, or null if no data is
   *     available.
   */
  checkFreshness(key: TKey): number | null {
    const entry = this.cache.get(key);
    if (!entry || !entry.lastUpdatedTimestamp) {
      return null;
    } else {
      return new Date().getTime() - entry.lastUpdatedTimestamp;
    }
  }

  private getCacheEntry(key: TKey): CacheEntry<T> {
    return this.cache.has(key) ? this.cache.get(key) : this.createCacheEntry(key);
  }

  private createCacheEntry(key: TKey): CacheEntry<T> {
    const triggerSubject = new Subject<void>();
    const entry: CacheEntry<T> = {
      triggerRefresh: triggerSubject,
      data: this.options.subjectFactory()
    };
    this.cache.set(key, entry);

    // Watch for refreshes, then apply a fresh observable, sending the output to
    // the cache entry's subject. We could get a similar behaviour using
    // multicast + refCount, but
    const sub = entry.triggerRefresh.pipe(
      filter(() => !entry.refreshInProgress),
      tap(() => entry.refreshInProgress = true),
      switchMap(() => this.options.factory(key).pipe(
        catchError(error => this.options.handleError(key, error)),
        tap(() => {
          // This will handle the case of a successful update. We set the
          // timestamp now so that we can report the freshness of a cache entry.
          entry.lastUpdatedTimestamp = new Date().getTime();
          entry.refreshInProgress = false;
        }),
        finalize(() => entry.refreshInProgress = false)
      )),
    ).subscribe(value => entry.data.next(value));
    this.subscriptions.push(sub);

    return entry;
  }
}
