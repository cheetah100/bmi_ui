/**
 * Translates a value from one numeric range to another.
 *
 * This performs linear interpolation to take a value on one scale (given by the
 * from range) and map it onto another range (the to range).
 *
 * The values aren't clamped to these ranges, so values outside will get extrapolated.
 *
 * @param value the number to extrapolate, in the 'from' range
 * @param from a tuple that defines the original range of values
 * @param to a tuple that defines the target range of values
 * @returns a number, linearly interpolated using these ranges
 */
export function mapRange(value: number, from: [number, number], to: [number, number]): number {
  // This implementation is taken from https://rosettacode.org/wiki/Map_range#JavaScript
  return to[0] + (value - from[0]) * (to[1] - to[0]) / (from[1] - from[0]);
}


/**
 * Calculates the weighted geometric mean of several values.
 *
 * The geometric mean is multiplicative rather than additive. This means that a
 * value of 0 will give a result of 0, regardless of the weight or other values.
 *
 * The sum of weights must be non-zero
 *
 * @param values an array of [value, weight] tuples
 * @returns the geometric mean of the values
 */
export function weightedGeometricMean(values: [number, number][]): number {
  let weightSum = 0;
  let product = 1;
  for (const [value, weight] of values) {
    product *= Math.pow(value, weight);
    weightSum += weight;
  }

  return Math.pow(product, 1 / weightSum);
}

/**
 * Calculates the weighted arithmetic mean of several values
 *
 * This is the regular additive mean, but with weighting given to each value.
 *
 * @param values an array of [value, weight] tuples
 * @returns the mean of the values
 */
export function weightedArithmeticMean(values: [number, number][]): number {
  let weightSum = 0;
  let sum = 0;
  for (const [value, weight] of values) {
    sum += value * weight;
    weightSum += weight;
  }

  return sum / weightSum;
}
