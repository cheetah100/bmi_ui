import { groupBy as lodashGroupBy } from 'lodash-es';
import { isEqual as lodashIsEqual } from 'lodash-es';


/**
 * An object map with values of type T
 */
export interface ObjectMap<T> {
  [field: string]: T;
}


export function groupBy<T>(items: T[], keyFunction: (item: T) => string): { [key: string]: T[] } {
  return lodashGroupBy(items, keyFunction);
}


export function isEqual<T>(a: T, b: T): boolean {
  return lodashIsEqual(a, b);
}


export function mapObjectValues<T, V>(obj: { [key: string]: T }, fn: (value: T, key?: string, obj?: { [key: string]: T }) => V): V[] {
  return Object.keys(obj).map(key => fn(obj[key], key, obj));
}

export function getObjectValues<T>(obj: { [key: string]: T }): T[] {
  return Object.keys(obj).map(key => obj[key]);
}


/**
 * Create an id -> item mapping from a list of objects with ids.
 *
 * @param items The list of items to create an ID mapping for
 * @returns A Map from id -> item
 */
export function idMap<T extends { id: string }>(items: T[]): Map<string, T> {
  const result = new Map<string, T>();
  for (const item of items) {
    result.set(item.id, item);
  }

  return result;
}

/**
 * Creates a Map object for a sequence of items, using a function to generate
 * the key.
 *
 * This can be done using the Map constructor, but the type inference doesn't
 * seem to behave right and requires a cast, which SonarQube's ruleset thinks
 * isn't necessary.
 *
 * Making a util function is much tidier anyway.
 */
export function createMapFrom<TKey, T, TVal = T>(
  items: ReadonlyArray<T>,
  keyFunction: (value: T) => TKey,
  valueFunction?: (value: T) => TVal
): Map<TKey, TVal> {
  const map = new Map<TKey, TVal>();
  for (const item of items) {
    const key = keyFunction(item);
    const value = valueFunction ? valueFunction(item) : <unknown>item as TVal;
    map.set(key, value);
  }
  return map;
}


export function createGroupedMapFrom<TKey, T, TVal = T>(
  items: ReadonlyArray<T>,
  keyFunction: (value: T) => TKey,
  valueFunction?: (value: T) => TVal
): Map<TKey, TVal[]> {
  const map = new Map<TKey, TVal[]>();
  for (let i = 0; i < items.length; i++) {
    const item = items[i];
    const key = keyFunction(item);
    const value = valueFunction ? valueFunction(item) : <unknown>item as TVal;
    if (map.has(key)) {
      map.get(key).push(value);
    } else {
      map.set(key, [value]);
    }
  }
  return map;
}
