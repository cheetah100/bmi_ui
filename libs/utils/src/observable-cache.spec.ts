import { ObservableCache } from './observable-cache';
import { of, NEVER, Subject, ReplaySubject } from 'rxjs';
import { finalize } from 'rxjs/operators';

describe('ObservableCache', () => {

  let countingCache: ObservableCache<number>;
  beforeEach(() => {
    let counter = 0;
    countingCache = new ObservableCache<number>(
      key => of(++counter)
    );
  });

  it('should not create an observable when just watching', () => {
    let value;
    countingCache.watch('a').subscribe(v => value = v);
    expect(value).toBeUndefined();
  });

  it('should allow refresh when there are no subscribers', () => {
    let value;
    countingCache.refresh('a');
    countingCache.watch('a').subscribe(v => value = v);
    expect(value).toBe(1);
  });

  it('should create a new source observable when watching a key', () => {
    let aValue, bValue;
    countingCache.watch('a').subscribe(v => aValue = v);
    countingCache.watch('b').subscribe(v => bValue = v);

    countingCache.refresh('a');
    countingCache.refresh('b');

    expect(aValue).toBe(1);
    expect(bValue).toBe(2);
  });

  it('should return the cached value after resubscribing', () => {
    let initialValue, cachedValue;
    const sub = countingCache.watch('a').subscribe(v => initialValue = v);
    countingCache.refresh('a');

    sub.unsubscribe();

    countingCache.watch('a').subscribe(v => cachedValue = v);

    expect(initialValue).toBe(1);
    expect(cachedValue).toBe(1);
  });

  it('should create a new source each time refresh() is called', () => {
    let values = [];
    countingCache.watch('a').subscribe(v => values.push(v));
    countingCache.refresh('a');
    countingCache.refresh('a');
    expect(values).toEqual([1, 2]);
  });

  describe('Subject Factory', () => {
    function makeCounter(subjectFactory: () => Subject<number>) {
      let counter = 0;
      return new ObservableCache<number>({
        factory: () => of(++counter),
        subjectFactory: subjectFactory
      });
    }

    it('should allow the subject type to be customized', () => {
      const cache = makeCounter(() => new ReplaySubject());
      let values = [];
      cache.refresh('a');
      cache.refresh('a');
      cache.watch('a').subscribe(v => values.push(v));

      // A replaysubject without a limit will replay all values
      expect(values).toEqual([1, 2]);
    });
  });

  describe('prevent refreshes while one is in progress', () => {
    let updateSubject: Subject<string>;
    let cache: ObservableCache<string>;
    let refreshCount: number;
    let outputValue: string;
    beforeEach(() => {
      refreshCount = 0;
      updateSubject = new Subject<string>();
      cache = new ObservableCache(() => {
        refreshCount += 1;
        return updateSubject;
      });

      cache.watch('test').subscribe(value => outputValue = value);
    });

    it('should start a refresh when triggered', () => {
      cache.refresh('test');
      expect(refreshCount).toBe(1);
    });

    it('should not start a subsequent refresh while one is in progress', () => {
      cache.refresh('test');
      cache.refresh('test');
      expect(refreshCount).toBe(1);
    });

    it('should allow a second refresh after the first emits', () => {
      cache.refresh('test');
      updateSubject.next('foo');
      cache.refresh('test');
      expect(refreshCount).toBe(2);
    });

    it('should allow a refresh to be forced', () => {
      cache.refresh('test');
      cache.refresh('test', true);
      expect(refreshCount).toBe(2);
    });

    it('should allow a refresh after the observable completes without emitting', () => {
      cache.refresh('test');
      updateSubject.complete();
      cache.refresh('test');
      expect(refreshCount).toBe(2);
    });

    it('should allow a refresh after the observable errors', () => {
      cache.refresh('test');
      updateSubject.error(new Error('OH NO'));
      cache.refresh('test');
      expect(refreshCount).toBe(2);
    });
  });

  describe('destroy', () => {
    it('should complete any watchers', () => {
      let isComplete = false;
      countingCache.watch('a').subscribe(null, null, () => { isComplete = true; });
      countingCache.destroy();
      expect(isComplete).toBeTruthy();
    });

    it('should close any running sources', () => {
      let closedSource = false;
      const cache = new ObservableCache(
        () => NEVER.pipe(finalize(() => closedSource = true))
      );

      cache.refresh('a');
      expect(closedSource).toBeFalsy();
      cache.destroy();
      expect(closedSource).toBeTruthy();
    });
  });

  describe('cache freshness', () => {
    let updateSubject: Subject<string>;
    let cache: ObservableCache<string>;
    beforeEach(() => {
      updateSubject = new Subject<string>();
      cache = new ObservableCache(() => updateSubject);
    });

    it('should return null for a missing key', () => {
      expect(cache.checkFreshness('foobar')).toBeNull();
    });

    it('should return null if the source has not emitted a value', () => {
      cache.refresh('foobar');
      expect(cache.checkFreshness('foobar')).toBeNull();
    });

    it('should return a number once the cache has emitted', () => {
      cache.refresh('foobar');
      updateSubject.next('foo');
      expect(typeof cache.checkFreshness('foobar')).toBe('number');
    });

    it('should return a short positive duration immediately after checking', () => {
      cache.refresh('foobar');
      updateSubject.next('foo');
      // We'll expect it to be in the range of 0 - 1000 ms. The latter is really
      // high, but this is counting wall-time and I don't want tests failing due
      // to some kind of scheduling delay.
      expect(cache.checkFreshness('foobar')).toBeGreaterThanOrEqual(0);
      expect(cache.checkFreshness('foobar')).toBeLessThanOrEqual(1000);
    });

    it('should return a duration that matches reality', (done) => {
      cache.refresh('foobar');
      const startTime = new Date().getTime();
      updateSubject.next('foo');

      setTimeout(() => {
        const measuredDuration = new Date().getTime() - startTime;
        const freshness = cache.checkFreshness('foobar');
        expect(Math.abs(measuredDuration - freshness)).toBeLessThan(50);
        done();
      }, 250);

    })
  });
})
