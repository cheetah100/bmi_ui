import * as Fuse from 'fuse.js/dist/fuse.js';
import * as Fuzzysort from 'fuzzysort';
import { mapRange } from './math';
import { sortBy } from './sort-by';

interface FuseResult<T> {
  item: T;
  score: number;
}

interface FuzzysortResult<T> {
  indexes: number[];
  obj: T;
  score: number;
  target: string;
}

interface SearchResult<T> {
  score: number;
  item: T;
}

interface ValueScore {
  score: number;
  value: string;
}

export class FuzzySearcher<T> {
  fuseThreshold = 0.4;
  fuzzysortThreshold = -8000;
  maxResults = 10;
  private _items: T[] = [];

  set items(items: T[]) {
    this._items = items;
  }

  private geoMeanScore(
    results: SearchResult<T>[],
    itemValue: string,
    key: string
  ): number {
    const scores: SearchResult<T>[] = results.filter(
      (result: SearchResult<T>) => result.item[key] === itemValue
    );
    // not worried about sqrt. We're only concerned with a single threshold.
    return scores.reduce((a, c) => a * c.score, 1);
  }

  private fuseSearch(searchTerm: string, key: string): SearchResult<T>[] {
    const fuse = new Fuse(this._items, {
      threshold: this.fuseThreshold,
      includeScore: true,
      keys: [key],
      distance: 50
    });
    const fuseResult = fuse.search(searchTerm).map((result: FuseResult<T>) => ({
      score: result.score,
      item: result.item
    })) as SearchResult<T>[];
    return fuseResult;
  }

  private fuzzysortSearch(searchTerm: string, key: string): SearchResult<T>[] {
    const fuzzysortOptions = {
      key: key,
      allowTypo: true,
      threshold: this.fuzzysortThreshold
    };
    const fuzzysortResult = Fuzzysort.go(
      searchTerm,
      this._items,
      fuzzysortOptions
    ).map((result: FuzzysortResult<T>) => ({
      score: mapRange(
        result.score,
        [0, this.fuzzysortThreshold],
        [0.1, this.fuseThreshold]
      ),
      item: result.obj
    })) as SearchResult<T>[];
    return fuzzysortResult;
  }

  /**
   * Not dealing with multiple keys or nested objects / keys yet.
   * First use case is for select components, where we are searching on option.text
   * But this is simplistic. Need to be able to have multiple keys and any structure
   * @param searchTerm
   * @param key
   */
  public search(searchTerm: string, key: string): T[] {
    const allResults: SearchResult<T>[] = [
      ...this.fuseSearch(searchTerm, key),
      ...this.fuzzysortSearch(searchTerm, key)
    ];
    const resultKeys = [
      ...new Set(allResults.map((result: SearchResult<T>) => result.item[key]))
    ];
    const combinedResults: ValueScore[] = [];
    resultKeys.forEach((value: string) => {
      const score = this.geoMeanScore(allResults, value, key);
      combinedResults.push({ value, score });
    });
    const sortedResults = sortBy(combinedResults, 'score')
      .filter((valueScore: ValueScore) => valueScore.score <= 0.3)
      .slice(0, this.maxResults);
    return sortedResults.map((vs: ValueScore) =>
      this._items.find((item: T) => item[key] === vs.value)
    );
  }
}
