import { Observable, Notification, Subject } from 'rxjs';
import { materialize } from 'rxjs/operators';
import { combineLatestDeferred } from './combine-latest-deferred';

function wait(millis: number = 20) {
  return new Promise((resolve, reject) => setTimeout(() => resolve(), millis));
}

function subscribeToEvents<T>(observable: Observable<T>): Notification<T>[] {
  const events: Notification<T>[] = [];
  observable.pipe(materialize()).subscribe(event => events.push(event));
  return events;
}

function range(n: number): number[] {
  return Array.from({length: n}, (x, i) => i);
}


describe('combineLatestDeferred', () => {

  describe('empty array', () => {
    let events: Notification<any[]>[];

    beforeEach(() => events = subscribeToEvents(combineLatestDeferred([])));

    it('should emit an empty array', done => {
      wait().then(() => {
        expect(events[0].value).toEqual([]);
        done();
      });
    });

    it('should emit a value then complete', () => {
      expect(events.map(e => <string>e.kind)).toEqual(['N', 'C']);
    });
  });

  describe('combining multiple subjects', () => {
    let events: Notification<string[]>[];
    let subjects: Subject<string>[];

    beforeEach(() => {
      subjects = range(10).map(() => new Subject<string>());
      events = subscribeToEvents(combineLatestDeferred(subjects));
    });

    function nextAll(message: string) {
      subjects.forEach(s => s.next(message));
    }

    it('should not emit a value initially ', done => {
      wait().then(() => {
        console.log(events);
        expect(events.length).toBe(0);
        done();
      });
    });

    it('should emit a value after all observables have fired once', done => {
      subjects.forEach((s, index) => s.next(String(index)));
      wait().then(() => {
        expect(events.length).toBe(1);
        expect(events[0].value).toEqual(range(10).map((x, index) => String(index)));
        done();
      });
    });

    describe('Deferring the combined events', () => {

      beforeEach(() => {
        range(5).forEach(i => nextAll(`Update #${i + 1}`));
      });

      it('should not output anything synchronously', () => {
        expect(events.length).toBe(0);
      });

      it('should output the latest event only after waiting a little bit', done => {
        wait().then(() => {
          expect(events.length).toBe(1);
          expect(events[0].value).toEqual(range(10).map(x => 'Update #5'));
          done();
        });
      });

      it('should continue to output events in the next batch', done => {
        wait().then(() => {
          nextAll('Again!');
          return wait();
        }).then(() => {
          expect(events.length).toBe(2);
          expect(events[1].value).toEqual(range(10).map(() => 'Again!'));
          done();
        });
      });
    });

    describe('Completing', () => {
      it('should complete synchronously if all sources complete without emitting anything', () => {
        subjects.forEach(s => s.complete());
        expect(events[0].kind).toBe('C');
      });


      describe('once everything has emitted at least once', () => {
        beforeEach(() => nextAll('First!'));

        it('should still emit events for live sources if one completes', done => {
          wait().then(() => {
            subjects[0].complete();
            subjects.slice(1).forEach(s => s.next('test'));
            return wait();
          }).then(() => {
            expect(events.length).toBe(2);
            expect(events[1].value).toEqual([
              'First!',
              ...range(subjects.length - 1).map(() => 'test')
            ]);
            done();
          });
        });

        it('should complete after everything completes', done => {
          subjects.forEach(s => s.complete());
          wait().then(() => {
            expect(events.length).toBe(2);
            expect(events.map(e => String(e.kind))).toEqual(['N', 'C']);
            done();
          });
        });
      });
    });
  });
});
