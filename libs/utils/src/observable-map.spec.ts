import { ObservableMap, ObservableMapOptions } from './observable-map';
import { Subject } from 'rxjs';
import { onSubscribe } from './debug-observable-events';


function expectKeysToEqual<K, V>(map: ObservableMap<K, V>, expectedKeys: K[]) {
  expect(Array.from(map.keys()).sort()).toEqual([...expectedKeys].sort());
}


describe('ObservableMap', () => {
  describe('With no source', () => {
    let map: ObservableMap<string, string>;
    let currentTime: number;

    beforeEach(() => {
      currentTime = 0;
      map = new ObservableMap({
        getCurrentTime: () => ++currentTime,
      });
    });

    it('should return undefined when requesting a key that does not exist', () => {
      expect(map.get('does not exist')).toBe(undefined);
    });

    it('should add an entry when setting a key', () => {
      map.set('test', 'foobar');
      expect(map.get('test')).toBe('foobar');
    });

    it('should emit no value when watching a key that did not exist', () => {
      const values = [];
      map.watch('test').subscribe(value => values.push(value));
      expect(values).toEqual([]);
    });

    it('should emit the latest value when watching a key that already exists', () => {
      map.set('test', 'Foo');
      map.set('test', 'Bar');
      const values = [];
      map.watch('test').subscribe(value => values.push(value));
      expect(values).toEqual(['Bar']);
    });

    it('should emit when a value is set', () => {
      map.set('test', 'Foo');
      const values = [];
      map.watch('test').subscribe(value => values.push(value));
      map.set('test', 'bar');
      map.set('test', 'Hello World');
      expect(values).toEqual(['Foo', 'bar', 'Hello World']);
    });


    describe('Invalidating keys', () => {
      let emittedValues: string[];
      beforeEach(() => {
        emittedValues = [];
        map.set('test', 'foo');
        map.invalidate('test');
        map.watch('test').subscribe(value => emittedValues.push(value));
      });

      it('should not emit a value once the key is invalidated', () => {
        expect(emittedValues).toEqual([]);
      });

      it('should emit a valid value once the key is set again', () => {
        map.set('test', 'valid');
        expect(emittedValues).toEqual(['valid']);
      });
    });

    describe('keys()', () => {
      it('should return empty for an empty map', () => {
        expectKeysToEqual(map, []);
      });

      it('should contain the keys added to the map', () => {
        map.set('foo', 'a');
        map.set('bar', 'b');
        expectKeysToEqual(map, ['foo', 'bar']);
      });

      it('should have Set-like behaviour if the entry is set multiple times', () => {
        map.set('foo', 'a');
        map.set('foo', 'a');
        expectKeysToEqual(map, ['foo']);
      });
    });

  });

  describe('Key transform function', () => {
    it('should support string -> string functions', () => {
      const map = new ObservableMap<string, string>({
        keyFunction: key => key.toUpperCase()
      });

      map.set('test', 'Foo');
      expect(map.has('Test')).toBeTruthy();
    });

    it('should support array keys if a suitable transform provided', () => {
      const map = new ObservableMap<string[], string>({
        keyFunction: key => key.join('|')
      });

      map.set(['foo', 'bar'], 'Testing');
      map.set(['blah'], 'Something');
      expect(map.get(['foo', 'bar'])).toEqual('Testing');
    });

    describe('keys() with a transform function', () => {
      let map: ObservableMap<string, string>;
      beforeEach(() => {
        map = new ObservableMap<string, string>({
          keyFunction: key => key.toUpperCase()
        });
      });

      it('should add the original keys to the set', () => {
        map.set('foo', 'blah');
        map.set('bar', 'test');
        expectKeysToEqual(map, ['foo', 'bar']);
      });

      it('should only use the first seen key in the case of normalized keys', () => {
        // This one needs more explaining... If the keyFunction maps multiple
        // keys onto the same entry (e.g. normalizes to uppercase), then our
        // keyset should contain the FIRST object only.
        //
        // This prevents it getting crazy where TKey is an object that may have
        // different identities, but should be considered equivalent .
        map.set('foo', 'x');
        map.set('Bar', 'y');
        map.set('FoO', 'z');
        expectKeysToEqual(map, ['foo', 'Bar']);
      });
    });
  });

  describe('With a source generator function', () => {
    let map: ObservableMap<string, string>;
    let sources: Map<string, Subject<string>>;
    let currentTime: number;
    let sourceSubscriptions: string[];

    beforeEach(() => {
      currentTime = 0;
      sources = new Map<string, Subject<string>>();
      sourceSubscriptions = [];
      map = new ObservableMap({
        source: key => {
          sources.set(key, new Subject<string>());
          return sources.get(key).pipe(
            onSubscribe(() => sourceSubscriptions.push(key))
          );
        },
        getCurrentTime: () => ++currentTime
      });
    });

    it('should not create a source when getting something that does not exist', () => {
      map.get('test');
      expect(sources.has('test')).toBeFalsy();
    });

    describe('When watching a key', () => {
      let emitted: string[];
      beforeEach(() => {
        emitted = [];
        map.watch('test').subscribe(value => emitted.push(value));
      });

      it('should create a source subscription when watching a key', () =>  expect(sources.has('test')).toBeTruthy());
      it('should initially emit nothing', () => expect(emitted).toEqual([]));

      it('should emit the values coming from the source', () => {
        sources.get('test').next('Foo');
        sources.get('test').next('Bar');
        expect(emitted).toEqual(['Foo', 'Bar']);
      });

      it('should subscribe to the source being watched', () => {
        expect(sourceSubscriptions).toEqual(['test']);
      });

      describe('Refreshing a watched value', () => {

        beforeEach(() => {
          sources.get('test').next('initial');
        });

        it('should not resubscribe when refresh is called if it is already refreshing', () => {
          map.refresh('test');
          expect(sourceSubscriptions).toEqual(['test', 'test']);
        })

        it('should emit the new value when refresh is called', () => {
          map.refresh('test');
          sources.get('test').next('refreshing');
          expect(emitted).toEqual(['initial', 'refreshing']);
        });

        it('should recreate the source when refreshing', () => {
          const initialSource = sources.get('test');
          map.refresh('test');
          expect(sources.get('test')).not.toBe(initialSource);
        });
      });

    });
  });
});
