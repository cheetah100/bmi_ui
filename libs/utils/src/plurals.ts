/**
 * Best guess for spelling if we don't have user entered string for single item
 * @param name - Could be plural or singular.
 */
export function guessSingular(name: string): string {
  if (name.endsWith('ies')) {
    return `${name.slice(0, -3)}y`;
  }
  if (/(?:(ss)|x|y|(ch)|(sh))es$/.test(name)) {
    return `${name.slice(0, -2)}`;
  }
  if (!name.endsWith('s') || name.endsWith('ss')) {
    return name;
  }
  return `${name.slice(0, -1)}`;
}

/**
 * Best guess for spelling if we don't have user entered spelling for multiple items
 * @param name - Could be plural or singular.
 */
export function guessPlural(name: string): string {
  if (/(?:[bcdfghjklmnpqrstvwxyz])y$/.test(name)) {
    return `${name.slice(0, -1)}ies`;
  }
  if (/(?:(ss)|x|y|(ch)|(sh))$/.test(name)) {
    return `${name}es`;
  }
  if (name.endsWith('s')) {
    return name;
  }
  return `${name}s`;
}
