export interface Option {
  value: string;
  text: string;
  hidden?: boolean;
}

export interface Point {
  x: number;
  y: number;
}