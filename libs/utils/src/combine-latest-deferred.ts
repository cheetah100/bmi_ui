import { combineLatest, asapScheduler, Observable, of,  ObservableInput, ObservedValueOf } from 'rxjs';
import { debounceTime } from 'rxjs/operators';


// These type definitions were just adapted from rxjs/internal/observable/combineLatest.ts
export function combineLatestDeferred<O1 extends ObservableInput<any>>(sources: [O1]): Observable<[ObservedValueOf<O1>]>;
export function combineLatestDeferred<O1 extends ObservableInput<any>, O2 extends ObservableInput<any>>(sources: [O1, O2]): Observable<[ObservedValueOf<O1>, ObservedValueOf<O2>]>;
export function combineLatestDeferred<O1 extends ObservableInput<any>, O2 extends ObservableInput<any>, O3 extends ObservableInput<any>>(sources: [O1, O2, O3]): Observable<[ObservedValueOf<O1>, ObservedValueOf<O2>, ObservedValueOf<O3>]>;
export function combineLatestDeferred<O1 extends ObservableInput<any>, O2 extends ObservableInput<any>, O3 extends ObservableInput<any>, O4 extends ObservableInput<any>>(sources: [O1, O2, O3, O4]): Observable<[ObservedValueOf<O1>, ObservedValueOf<O2>, ObservedValueOf<O3>, ObservedValueOf<O4>]>;
export function combineLatestDeferred<O1 extends ObservableInput<any>, O2 extends ObservableInput<any>, O3 extends ObservableInput<any>, O4 extends ObservableInput<any>, O5 extends ObservableInput<any>>(sources: [O1, O2, O3, O4, O5]): Observable<[ObservedValueOf<O1>, ObservedValueOf<O2>, ObservedValueOf<O3>, ObservedValueOf<O4>, ObservedValueOf<O5>]>;
export function combineLatestDeferred<O1 extends ObservableInput<any>, O2 extends ObservableInput<any>, O3 extends ObservableInput<any>, O4 extends ObservableInput<any>, O5 extends ObservableInput<any>, O6 extends ObservableInput<any>>(sources: [O1, O2, O3, O4, O5, O6]): Observable<[ObservedValueOf<O1>, ObservedValueOf<O2>, ObservedValueOf<O3>, ObservedValueOf<O4>, ObservedValueOf<O5>, ObservedValueOf<O6>]>;
export function combineLatestDeferred<O extends ObservableInput<any>>(sources: O[]): Observable<ObservedValueOf<O>[]>;


/**
 * combineLatest, but with deferred async output and works with empty arrays
 *
 * This is a variation of combineLatest that addresses two significant issues
 * when used with programmatically generated lists of observables:
 *
 * 1) Empty list behaviour: the standard combineLatest operator has an
 *    unexpected behaviour when called with an empty list. It will return EMPTY
 *    (completes without emitting) rather than emitting an empty array. This
 *    behaviour is inconsistent with Promise.all() and a hazard when used with
 *    config-driven lists of data sources. If an empty list is provided, this
 *    can cause the stream to hang or end without results depending on the
 *    operators used. Not good!
 *
 * 2) Spammy output. The first emission with combineLatest is fine because it
 *    just counts until everything has emitted. Subsequent changes however can
 *    cause a wave of updates.  combineLatest([O1 ... On]) can cause n updates
 *    as each of those updates in response to refreshing data. This is
 *    undesirable, but it's easy to overlook adding an auditTime() with suitable
 *    scheduler.
 *
 * Naming this function was a real challenge combineLatestDeferred doesn't
 * really capture the full changes from the standard Rx combinator
 */
export function combineLatestDeferred(observables: Observable<any>[]): Observable<any[]> {
  // By default, combineLatest on an empty array will return an observable that
  // completes immediately. That behaviour is not consistent with Promise.all()
  // and
  if (observables.length === 0) {
    return of([]);
  } else {
    return combineLatest(observables).pipe(
      // Combine latest can be very spammy when the observables are updating,
      // especially if there are a lot all updating within the same frame. We
      // want to introduce just enough asynchrony to cut down on a lot of the
      // spam here. asapScheduler with 0 delay queues this as a microtask that
      // should happen hopefully once these updates are all finished with.
      debounceTime(0, asapScheduler)
    );
  }
}
