import { ensureIdUniqueIn } from './id-utils';

describe('ensureIdUniqueIn', () => {
  const others = [
    'foo',
    'foo1',
    'bar2'
  ];


  it('should use the id directly if it is not in the set', () => {
    expect(ensureIdUniqueIn('test', others)).toBe('test');
  });

  it('should pick the next available string by appending numbers', () => {
    expect(ensureIdUniqueIn('foo', others)).toBe('foo2');
  });
});
