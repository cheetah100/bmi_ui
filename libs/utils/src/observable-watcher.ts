import { Observable, Notification, Subscription, Unsubscribable } from 'rxjs';
import { take, materialize } from 'rxjs/operators';



/**
 * A helper to monitor an observable and test its state.
 */
export class ObservableWatcher<T> implements Unsubscribable {
  private _values: ReadonlyArray<T> = [];
  private _events: Readonly<Notification<T>[]> = [];

  private subscription = new Subscription();

  constructor (observable: Observable<T>) {
    this.subscription.add(observable.pipe(
      materialize(),
    ).subscribe(event => {
      this._events = [...this._events, event];
      if (event.kind === 'N') {
        this._values = [...this._values, event.value];
      }
    }));
  }

  unsubscribe() {
    this.subscription.unsubscribe();
  }

  /**
   * Gets an array of the values that have been emitted until now.
   *
   * This is immutable, so storing the array returned can allow comparisons
   * before and after an operation.
   */
  get values() {
    return this._values;
  }

  get latestValue() {
    return this._values[this._values.length - 1];
  }

  get isComplete() {
    return this._events.some(n => n.kind === 'C');
  }

  get hasError() {
    return this._events.some(n => n.kind === 'E');
  }

  get error() {
    const errorEvent = this._events.find(n => n.kind === 'E');
    return errorEvent && errorEvent.error;
  }

  get hasFinished() {
    return this.isComplete || this.hasError;
  }

  get hasEmittedValue() {
    return this._values.length > 0;
  }
}
