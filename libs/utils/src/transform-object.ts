import mapValues from 'lodash-es/mapValues';


type MatcherPredicate = (x: unknown) => boolean;
type TransformFunction<T, R> = (x: T) => R;

/**
 * Performs a recursive, bottom-up transform of an object.
 *
 * This takes a value (expected: JSON-ish object tree) and a transform function,
 * and performs a deep traversal of the object, applying a transform at each
 * step. It's expected that this transform function will inspect the value and
 * decide what to do with it, behaving as the identity function if nothing needs
 * to change.
 *
 * This is a *bottom-up* traversal, which means it will explore down the tree,
 * transforming the leaves, then transforming the array/object that contains
 * them as it merges back upwards.
 *
 * The implication of this is that when the transform is called on an object;
 * everything inside it has already been potentially transformed.
 *
 * The transform function should treat the input as immutable, returning an
 * updated version (e.g. using spread/clone). If you mutate, you must take
 * responsibility for the side-effects!
 *
 * The current implementation of transformObject does NOT act as an identity if
 * no changes are made as it uses map on objects and arrays. This could change
 * in future if immutability is guaranteed -- if no changes are made, the
 * original, untransformed object/array can be used instead, meaning that
 * overall object identity could be preserved.
 */
export function transformObject(
  input: unknown,
  transform: TransformFunction<unknown, unknown>
): unknown {
  if (Array.isArray(input)) {
    return transform(input.map(x => transformObject(x as unknown, transform)));
  } else if (input && typeof input === 'object') {
    return transform(mapValues(input, x => transformObject(x, transform)));
  } else {
    return transform(input);
  }
}


/**
 * Defines a conditional transform: if it matches, apply the transform, else
 * identity.
 *
 * This is intended for use with the `transformObject` function where you are
 * targeting a particular type T, giving a result R. This isn't particularly
 * strongly typed, but they are a convenience here for cases where you're
 * expecting a certain shape from the objects you're interested in.
 */
export function defineTransform<T = unknown, R = T>(params: {
  match: MatcherPredicate
  transform: TransformFunction<T, R>
}): TransformFunction<T, T | R> {
  const isMatch = params.match;
  return x => isMatch(x) ? params.transform(x) : x;
}
