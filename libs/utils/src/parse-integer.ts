
/**
 * A less fussy parseInt.
 *
 * This can take a number OR a string. If the value is already a number it will
 * be returned verbatim. If the value is a string but it cannot be parsed by
 * parseInt(value), then the defaultValue parameter will be returned -- by
 * default this is NaN.
 *
 * @param value - the value to parse, this can be a string or a number
 * @param defaultValue - the value to return if the number cannot be parsed
 * @returns The parsed number, or the default.
 */
export function parseInteger(value: string | number | null, defaultValue: number = NaN): number {
  if (typeof(value) === 'number') {
    return Math.trunc(value);
  } else {
    const result = parseInt(value, 10);
    if (!isNaN(result)) {
      return result;
    } else {
      return defaultValue;
    }
  }
}
