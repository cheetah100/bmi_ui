import { transformObject, defineTransform } from './transform-object';
import { pipe } from 'rxjs';

import matches from 'lodash-es/matches'


interface TransformTestCase {
  spec: string;
  transform: (x: unknown) => unknown;
  input: any;
  expected: any;
}


function identity<T>(x: T): T {
  return x;
}

function uppercaser(x: unknown): string | unknown {
  return typeof x === 'string' ? x.toUpperCase() : x;
}


describe('transformObject', () => {
  const TEST_CASES: TransformTestCase[] = [
    {
      spec: 'should transform a string value',
      input: 'test',
      transform: uppercaser,
      expected: 'TEST'
    },

    {
      spec: 'should pass null through (with identity transform)',
      input: null,
      transform: identity,
      expected: null
    },

    {
      spec: 'should pass undefined through (with identity transform)',
      input: undefined,
      transform: identity,
      expected: undefined
    },

    {
      spec: 'should transform object keys',
      input: {
        foo: 'hello',
        bar: 'world'
      },
      transform: uppercaser,
      expected: {
        foo: 'HELLO',
        bar: 'WORLD'
      }
    },

    {
      spec: 'should recursively transform objects',
      input: {
        foo: 'hello',
        number: 44,
        bar: {
          array: ['a', 'b', 'c'],
          arrayOfObjects: [{test: 'a'}, {test: 'b'}]
        }
      },
      transform: uppercaser,
      expected: {
        foo: 'HELLO',
        number: 44,
        bar: {
          array: ['A', 'B', 'C'],
          arrayOfObjects: [{test: 'A'}, {test: 'B'}]
        }
      }
    }
  ];

  for (const {spec, transform, input, expected} of TEST_CASES) {
    it(spec, () => {
      const actual = transformObject(input as unknown, transform);
      expect(actual).toEqual(expected);
    });
  }

  describe('Calling the transform function', () => {
    let transformLog: unknown[];

    function loggedIdentity(x: unknown): unknown {
      transformLog.push(x);
      return x;
    }

    beforeEach(() => {
      transformLog = [];
    });

    it('should pass undefined values to the transform', () => {
      transformObject(undefined, loggedIdentity);
      expect(transformLog).toEqual([undefined]);
    });

    it('should pass the contents of an array, then the array to transform', () => {
      transformObject([1, 2, 3], loggedIdentity);
      expect(transformLog).toEqual([1, 2, 3, [1, 2, 3]]);
    });

    it('should pass the transformed array to the transformer (where the items have been transformed)', () => {
      const result = transformObject([1, 2, 3], pipe(
        loggedIdentity,
        x => typeof x === 'number' ? x + 1 : x
      ));

      expect(result).toEqual([2, 3, 4]);
      expect(transformLog).toEqual([1, 2, 3, [2, 3, 4]]);
    })
  });
});


describe('defineTransform', () => {
  it('should be the identity if predicate is always false', () => {
    const f = defineTransform({
      match: () => false,
      transform: () => { throw Error('Transform should never be called'); }
    });

    const valuesToTest = [
      1, 0, -1, 'foo', ['a', 'b'], undefined, null, true, false, {blah: 'foo'}
    ];

    for (const x of valuesToTest) {
      expect(f(x)).toBe(x);
    }
  });


  it('should apply the transform when the predicate is true', () => {
    const f = defineTransform<any>({
      match: x => typeof x === 'number',
      transform: x => x + 1
    });

    expect(f(5)).toBe(6);
    expect(f('foo')).toBe('foo')
  });
});


interface FakeBinding {
  bindingType: string;
  path: string;
  source?: string;
}


describe('transformObject + defineTransform + _.matches', () => {

  it('should all work nicely together to transform some page-like things', () => {

    const transform = defineTransform<FakeBinding, FakeBinding>({
      match: matches({bindingType: 'data-path'}),
      transform: binding => ({
        ...binding,
        source: binding.path.split('.')[0]
      })
    });

    const result = transformObject({
      things: [{
        value: {bindingType: 'data-path', path: 'this.foo.bar'}
      }]
    }, transform);

    expect(result).toEqual({
      things: [{
        value: {bindingType: 'data-path', source: 'this', path: 'this.foo.bar'}
      }]
    });
  });
})
