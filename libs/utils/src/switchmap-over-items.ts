import { Observable, OperatorFunction, of} from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { combineLatestDeferred } from './combine-latest-deferred';


export function switchMapOverItems<T,R>(fn: (x: T) => Observable<R>): OperatorFunction<T[],R[]> {
  return source => source.pipe(
    switchMap(items => items && items.length > 0
      ? combineLatestDeferred(items.map(fn))
      : of([]))
  )
}
