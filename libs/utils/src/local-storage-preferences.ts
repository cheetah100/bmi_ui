interface UsedValueItem {
  lastUsed: number;
  counter: number;
}

interface PreferenceItem {
  [key: string]: UsedValueItem | any;
}

interface Preferences {
  [key: string]: PreferenceItem;
}

const STORAGE_KEY = 'bmi-user-preferences';

/**
 * Trying this syncronously. But might need to async if there's any performance
 * hit reading / writing to localstorage
 */
const getPreferences = (): Preferences => {
    const preferencesString: string = localStorage.getItem(STORAGE_KEY);
  if (preferencesString) {
    return JSON.parse(preferencesString);
  }
  return {};
}

const getPreference = (preference: string): PreferenceItem => {
  const preferences = getPreferences();
  return preferences[preference] || {};
}

const setPreference = (preference: string, key: string, value: UsedValueItem | any): void => {
  const preferences = getPreferences();
  preferences[preference] = preferences[preference] || {};
  preferences[preference][key] = value;
  const serialized = JSON.stringify(preferences);
  localStorage.setItem(STORAGE_KEY, serialized);
}

// Initially for select options. But should be useable anywhere.
// Just stores a count for item selection by user, and last date used
const setUsedItemPreference = (preference: string, key: string): void => {
  const pref = getPreference(preference) || {};
  const prefItem = (pref[key] || {}) as UsedValueItem;
  prefItem.counter = (prefItem.counter || 0) + 1;
  prefItem.lastUsed = new Date().getTime();
  pref[key] = prefItem;
  setPreference(preference, key, prefItem);
}

export {
  UsedValueItem,
  PreferenceItem,
  Preferences,
  getPreference,
  setPreference,
  setUsedItemPreference,
}
