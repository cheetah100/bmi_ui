import * as moment from 'moment';
import 'moment-timezone';
import isNil from 'lodash-es/isNil';

type ValidDateType = 'date' | 'datetime' | 'longDate' | 'relative';

const DATE_FORMATS = {
  date: 'MMM D, YYYY',
  datetime: 'MMM D, YYYY hh:mmA z',
  longDate: 'MMM D, YYYY'
};


// This is the Unix timestamp for Jan 1st, 3000. It's an arbitrary point in time
// which should allow us to determine whether a numeric timestamp should be
// interpreted as seconds or milliseconds. If the time is before this date, we
// should assume it's probably a unix timestamp.
const FAR_FUTURE_UNIX_DATE = 32503633200;

/**
 * Converts a date, a time value, or Unix timestamp into a Time Value, in
 * milliseconds since Jan 1st 1970.
 *
 * We expect this to be used on contemporary dates, so it will make a guess
 * about whether a numeric input is in the Unix range, or the ECMAScript
 * unix*1000 range.  We do this by comparing the date against an arbitrary point
 * in time, in the far future (Jan 1st, 3000); times smaller than that are
 * assumed to be Unix timestamps.
 *
 * This means there is a small period of ambiguity between 1970 and about 1971,
 * which is unavoidable, as the ranges converge at this point.
 */
export function toTimestampMs(timestampOrDate: number | Date): number {
  if (timestampOrDate instanceof Date) {
    return timestampOrDate.getTime();
  } else {
    if (Math.abs(timestampOrDate) < FAR_FUTURE_UNIX_DATE) {
      return timestampOrDate * 1000;
    } else {
      return timestampOrDate;
    }
  };
}


export function formatDate(
  timestampOrDate: number | Date,
  type: ValidDateType = 'date',
  defaultValue = '-'
): string {
  const timestamp = toTimestampMs(timestampOrDate);
  if ((isNil(timestamp) || timestamp === 0) && defaultValue) {
    return defaultValue;
  } else {
    const format = DATE_FORMATS[type];
    if (format) {
      return moment(timestamp)
        .tz('America/Los_Angeles')
        .format(format);
    } else if (type === 'relative') {
      return moment(timestamp).fromNow();
    } else {
      throw new Error('Invalid date format specified');
    }
  }
}

/**
 * Gives a friendly, locale specific timestamp. This is used for modification dates etc.
 */
export function describeTimestampFriendly(timestampOrDate: number | Date): string {
  const timestamp = toTimestampMs(timestampOrDate);
  if (isNil(timestamp) || timestamp === 0) {
    return 'Never';
  } else {
    const t = moment(timestamp);
    const isRecent = t.isAfter(moment().subtract(1, 'day')) && t.isBefore(moment().add(1, 'day'));
    const absTime = isRecent ? t.format('h:mm:ssa') : t.format('YYYY-MM-DD');
    const relativeTime = t.fromNow();
    return `${relativeTime} (${absTime})`;
  }
}
