// Some of these types are available in later versions of Typescript, and can be removed once we upgrade

/**
 * Construct a type with the properties of T except for those in type K.
 */
export type Omit<T, K extends keyof any> = Pick<T, Exclude<keyof T, K>>;
