import { Observable, BehaviorSubject, Subscription, combineLatest, of, isObservable, Unsubscribable } from 'rxjs'
import { map, switchMap } from 'rxjs/operators';
import { distinctUntilNotEqual } from './distinct-until-not-equal';


export class StateContainer<TState> implements Unsubscribable {
  constructor(
    private _initialState: TState
  ) {
    this.subscription.add(
      combineLatest(
        this.baseState,
        this.mergedModifiers
      ).pipe(
        map(([baseState, modifiers]) => ({...baseState, ...modifiers}))
      ).subscribe(this.combinedState)
    );
  }

  private subscription = new Subscription();

  private baseState = new BehaviorSubject<TState>(this._initialState);

  // State modifiers are observables that produce config changes that affect
  // this state. They get composed together to derive the latest combined state.
  private stateModifiers = new BehaviorSubject<Observable<Partial<TState>>[]>([]);

  private mergedModifiers = this.stateModifiers.pipe(
    switchMap(modifiers => modifiers.length > 0 ? combineLatest(modifiers) : of(<TState[]>[])),
    map(modifications => Object.assign({}, ...modifications) as Partial<TState>),
    distinctUntilNotEqual(),
  );

  // The current composed state: base state + modifiers. The hook-up for this
  // happens in the constructor, but keeping this a separate subject allows the
  // current value to be inspected if needed.
  private combinedState = new BehaviorSubject(this.baseState.value);

  readonly state = this.combinedState.asObservable();

  get currentState() {
    return this.combinedState.value;
  }

  /**
   * Modifies the base state
   *
   * This updates (without mutation) the base state. Any modifiers are applied
   * atop this.
   */
  set(valuesToUpdate: Partial<TState>) {
    this.baseState.next({
      ...this.baseState.value,
      ...valuesToUpdate
    });
  }

  /**
   * Projects data out of the state using the map operator, and checks for
   * distinctness.
   *
   * This is a useful shorthand for extracting the state values you are
   * interested in.
   */
  select<R>(projectionFn: (state: TState) => R): Observable<R> {
    return this.state.pipe(
      map(projectionFn),
      distinctUntilNotEqual()
    );
  }

  /**
   * Adds a modifier to the state, which gets merged atop the base state
   *
   * A modifier lets you control values in the state in a reversible way. This
   * method returns a subscription, and when it is unsubscribed, the modifier
   * will be removed, nullifying its effect.
   *
   * Modifiers stack in the order they are applied, with the last taking
   * precedence for any value.
   *
   * Modifiers can either be a partial state, or it can be an observable, which
   * will emit changes to the state -- if a plain object is passed, this is
   * lifted with `of(state)`.
   *
   * Observables will be subscribed to once, and unsubscribed when the
   * Subscription is ended, or when the state container is disposed.
   *
   * @param modifier - the partial state update, or a stream of updates.
   */
  addModifier(modifier: Partial<TState> | Observable<Partial<TState>>): Subscription {
    const modifierSource = isObservable(modifier) ? modifier : of(modifier);
    this.stateModifiers.next([...this.stateModifiers.value, modifierSource]);
    return new Subscription(() => this.removeModifier(modifierSource));
  }

  /**
   * Destroys the state container and cleans up any subscriptions
   */
  unsubscribe() {
    this.subscription.unsubscribe();
    this.combinedState.complete();
  }

  private removeModifier(modifier: Observable<Partial<TState>>) {
    this.stateModifiers.next(this.stateModifiers.value.filter(x => x !== modifier));
  }
}
