import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ObjectMap } from './collection-utils';
import { distinctUntilNotEqual } from './distinct-until-not-equal';

/**
 * Watches parameters from the ActivatedRoute paramMap.
 *
 * This extracts the specific values into an object, then compares them for
 * changes.
 *
 * This uses some type inference so that the return type is inferred based on
 * the param keys provided, e.g:
 *
 * ```
 * watchParams(activatedRoute, 'foo', bar').subscribe(({foo, bar}) => console.log(...));
 * ```
 *
 * @param activatedRoute the route service to watch
 * @param ...params a number of parameter IDs to extract
 * @returns a stream of objects {[params]: string}
 */
export function watchParams<TKeys extends string>(activatedRoute: ActivatedRoute, ...params: TKeys[]): Observable<{[P in TKeys]: string}> {
  return activatedRoute.paramMap.pipe(
    map(paramMap => {
      const result: ObjectMap<string> = {};
      for (const param of params) {
        result[param] = paramMap.get(param);
      }
      return result as {[P in TKeys]: string};
    }),
    distinctUntilNotEqual(),
  );
}
