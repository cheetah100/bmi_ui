import { Observable, MonoTypeOperatorFunction, Subscriber, identity } from 'rxjs';
import { tap } from 'rxjs/operators';

/**
 * Debug helper to log all events from an observable to the console
 *
 * This is simply a tap operator that hooks up the next, complete and error
 * callbacks to write a log message to the console.
 *
 * This is useful when trying to figure out what an observable is actually doing
 * as they can be quite hard to step through and debug using devtools alone.
 *
 * You can customize the prefix for the message when tracing multiple
 * observables at a time.
 *
 * Example:
 *
 *     someObservable.pipe(
 *       debugEvents('SomeObservable')
 *     ).subscribe(...);
 *
 */
export function debugObservableEvents<T>(prefix: string = 'Observable debugEvents()', logSubscriptions = false): MonoTypeOperatorFunction<T> {
  return (source: Observable<T>) => source.pipe(
    tap({
      next: value => console.log(`${prefix}: next`, value),
      error: err => console.log(`${prefix}: error`, err),
      complete: () => console.log(`${prefix}: complete`)
    }),
    logSubscriptions ? onSubscribe((subscriber) => console.log(`${prefix}: subscribe`, subscriber)) : identity
  );
}


export function onSubscribe<T>(callback: (subscriber: Subscriber<T>) => void): MonoTypeOperatorFunction<T> {
  return (observable: Observable<T>) => {
    return new Observable<T>(subscriber => {
      callback(subscriber);
      return observable.subscribe(subscriber);
    })
  };
}
