import { combineLatestObject } from './combine-latest-object';
import { of } from 'rxjs';

// Testing this is a little tricky because combineLatest has undefined behaviour
// when being applied synchronously -- the number of emissions depends on the
// scheduler used for the sources.

describe('combineLatestObject', () => {
  it('should emit results that correspond to the input keys', () => {
    let results = [];

    combineLatestObject({
      foo: of(1),
      bar: of('a')
    }).subscribe(result => results.push(result));

    expect(results).toEqual([
      {foo: 1, bar: 'a'}
    ]);
  });

  it('should emit an empty object if an empty object is provided', () => {
    let results = [];
    combineLatestObject({}).subscribe(result => results.push(result));
    expect(results).toEqual([{}]);
  });
});
