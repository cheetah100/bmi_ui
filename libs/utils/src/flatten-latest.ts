import { Observable, of, combineLatest } from 'rxjs';
import { map, startWith} from 'rxjs/operators';

import flatten from 'lodash-es/flatten';


/**
 * Combine the latest values from several streams, then flatten together
 *
 * This is like combineLatest -> Array.flat(), but with some other niceties.
 * Unlike combineLatest, it doesn't wait for every source to emit first.
 *
 * It will also strip out any falsy values from the results -- if one of the
 * sources emits null, this will be stripped out before flattening.
 *
 * If an empty list of observables is provided, this will emit []
 */
export function flattenLatest<T>(observables: Observable<T[]>[]): Observable<T[]> {
  const sources = observables.map(obs => obs.pipe(startWith<T[]>([])));
  if (sources.length === 0) {
    return of([]);
  } else {
    return combineLatest(sources).pipe(
      map(results => flatten(results.filter(x => x)))
    );
  }
}
