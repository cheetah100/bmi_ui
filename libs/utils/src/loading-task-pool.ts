import { Observable, Subscription, of, concat, Subject } from 'rxjs';
import { filter, map } from 'rxjs/operators';

export class LoadingTask {
  _taskCompleted = new Subject<void>();
  startedTimestamp: number;
  completedTimestamp: number | null = null;

  constructor(public readonly name: string) {
    this.startedTimestamp = new Date().getTime();
  }

  get duration(): number {
    if (this.completedTimestamp) {
      return this.completedTimestamp - this.startedTimestamp;
    } else {
      return new Date().getTime() - this.startedTimestamp;
    }
  }

  get taskCompleted(): Observable<void> {
    return this._taskCompleted;
  }

  complete() {
    this.completedTimestamp = new Date().getTime();
    this._taskCompleted.next();
    this._taskCompleted.complete();
  }
}


/**
 * Tracks the completion status of a pool of ongoing tasks.
 *
 * The use case for this is where there can be many asynchronous loading tasks
 * and you want to show a loading spinner while these are in flight. This also
 * has a timeout on tasks so that a failed task doesn't stay loading forever.
 *
 * To use this, call `startTask()`, which will return a task object. Call the
 * `complete()` method on this task to mark it as completed.
 *
 * The `isLoading` property will tell you if there are any currently loading
 * tasks.
 */
export class LoadingTaskPool {
  activeTasks = new Set<LoadingTask>();
  private timeoutId: any = null;

  private hasStarted = false;
  private _tasksUpdatedEvent = new Subject<void>();
  private _taskTimedOutEvent = new Subject<LoadingTask>();
  private _taskFinishedEvent = new Subject<LoadingTask>();

  /**
   * Is the task pool complete?
   *
   * Completion means that at least one task has started, and there are no
   * active tasks.
   */
  get isComplete(): boolean {
    return this.hasStarted && !this.isLoading;
  }

  /**
   * Notifies when the task pool reaches the complete state.
   *
   * This observable will fire even if the task pool has completed before the
   * subscription is made.
   */
  get onCompleted(): Observable<void> {
    // Ensure that this observable always gets a chance to evaluate. This could
    // probably be done with a behaviour subject instead.
    return concat(of(null), this.onTasksUpdated).pipe(
      filter(() => this.isComplete)
    );
  }

  get onTaskTimedOut(): Observable<LoadingTask> {
    return this._taskTimedOutEvent;
  }

  get onTaskFinished(): Observable<LoadingTask> {
    return this._taskFinishedEvent;
  }

  /**
   * Are there currently tasks in progress?
   */
  get isLoading(): boolean {
    return this.activeTasks.size > 0;
  }

  /**
   * Fires whenever a task is started or completed.
   */
  get onTasksUpdated(): Observable<void> {
    return this._tasksUpdatedEvent;
  }

  constructor(public timeout = 10000) { }

  private bumpTimer() {
    if (this.timeout) {
      this.reapTasks();

      if (this.isLoading && !this.timeoutId) {
        this.timeoutId = setTimeout(() => {
          this.timeoutId = null;
          this.bumpTimer();
        }, 50);
      }
    }
  }

  private reapTasks() {
    this.activeTasks.forEach(task => {
      if (task.duration > this.timeout) {
        task.complete();
        this._taskTimedOutEvent.next(task);
      }
    });
  }

  /**
   * Starts a task. Call complete() on the returned task to mark it as done.
   */
  startTask(taskName: string = null): LoadingTask {
    const task = new LoadingTask(taskName);
    this.activeTasks.add(task);
    this.hasStarted = true;
    this._tasksUpdatedEvent.next();

    let subscription: Subscription = null;

    const endTask = () => {
      this.activeTasks.delete(task);
      subscription.unsubscribe();
      this._tasksUpdatedEvent.next();
      this._taskFinishedEvent.next(task);
    };

    subscription = task.taskCompleted.subscribe(endTask, endTask, endTask);
    this.bumpTimer();
    return task;
  }


  /**
   * A pipeable operator to watch for an observable to emit a value, or error,
   * or complete.
   */
  monitor<T>(): (source: Observable<T>) => Observable<T> {
    const loadingPool = this;
    return function (source: Observable<T>) {
      return new Observable<T>(subscriber => {
        const task = loadingPool.startTask();

        // End the task on any event from the source observable.
        const subscription = source.subscribe(
          (value) => { task.complete(); subscriber.next(value); },
          (err) => { task.complete(); subscriber.error(err); },
          () => { task.complete(); subscriber.complete(); }
        );

        return () => {
          task.complete();
          subscription.unsubscribe();
        };
      });
    };
  }
}
