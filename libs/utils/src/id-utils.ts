// Using this restricted set of characters because they are unambiguous. This is
// actually the set of characters used in MS product keys because it's easy to
// read correctly. It doesn't have as much entropy as using the full
// alphanumeric set though.
const ID_CHARACTERS = '2346789BCDFGHJKMPQRTVWXY';


/**
 * Generates a unique ID with an optional prefix string.
 *
 * Example: prefix_WG49JB7D
 *
 * If the prefix is not blank an underscore will be added between the prefix and
 * random characters.
 *
 * @param prefix - the ID prefix string. If non-blank, an underscore will be
 *     added between the prefix and random chars
 * @param length - the number of random characters to add. If 0, no underscore
 *     will be added after the prefix -- i.e. this function will just return the
 *     prefix.
 */
export function uniqueId(prefix: string = '', length = 8): string {
  let id = (prefix && length > 0) ? prefix + '_' : prefix;
  for (let i = 0; i < length; i++) {
    id += ID_CHARACTERS[Math.floor(Math.random() * ID_CHARACTERS.length)];
  }
  return id;
}


/**
 * Generates an ID based on the current client timestamp.
 *
 * Example: prefix_1533510454274_P2CF
 *
 * By default, this will have random characters appended to ensure uniqueness.
 *
 * @param prefix - the ID prefix string. If not blank, an underscore will be
 *     added between the prefix and timestamp.
 * @param randomLength - the number of random characters to append. If not 0, an
 *     underscore will separate the timestamp from the random characters.
 */
export function timestampId(prefix: string = '', randomLength = 4): string {
  const idPrefix = prefix ? prefix + '_' : '';
  return uniqueId(idPrefix + new Date().getTime(), randomLength);
}


/**
 * Sanitizes an ID, i.e. for use with the Gravity API.
 *
 * This will replace any non-word characters -- i.e. ^[A-Za-z0-9_] with
 * underscores.
 */
export function sanitizeId(id: string) {
  // TODO: maybe we should replace this with the gravitySanitizeId
  //       implemantation for consistency with the backend. I didn't do this
  //       here because we are close to a release and it could have implications
  //       for other parts of the application that rely on this function.
  return id.replace(/\W+/g, '_');
}

/**
 * Sanitizes an ID *just like Gravity does*
 *
 * This is a bit of a bodge, but it implements the same ID sanitization that
 * Gravity uses. In most cases, Gravity respects user-supplied IDs and it
 * doesn't matter; but in some cases (i.e. board resources) the "id" from the
 * API is actually treated as the "name" and then a sanitized ID is created
 * from that.
 *
 * This was added specifically for the board resources case which is a pain in
 * the ass because the ID must be user specified, but Gravity silently
 * transforms it, and fixing this behaviour on the backend would likely break
 * other rules that were developed with the sloppy ID handling in place.
 */
export function sanitizeGravityApiId(id: string) {
  // This looks very similar to our sanitizeId function above, but we don't
  // 'squeeze' out repeated underscores, and we lowercase the result.
  return id.replace(/\W/g, '_').toLowerCase();
}

export function probablyValidGravityId(id: string): boolean {
  return id && !!id.match(/^\w\w+$/);
}


/**
 * Makes a "nice looking" ID from an input string, suitable for URLs or similar.
 */
export function nameBeautifier(name: string) {
  return sanitizeId(name).replace(/_+/g, '-').toLowerCase();
}

export function ensureIdUniqueIn(id: string, others: string[] | Set<string>, separator = ''): string {
  const existingIds = Array.isArray(others) ? new Set(others) : others;

  if (!existingIds.has(id)) {
    return id;
  } else {
    let numericSuffix = 1;
    while (true) {
      const generatedId = `${id}${separator}${numericSuffix}`;
      if (!existingIds.has(generatedId)) {
        return generatedId;
      } else {
        numericSuffix += 1;
      }
    }
  }

}
