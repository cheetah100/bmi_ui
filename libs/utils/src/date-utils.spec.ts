import { toTimestampMs, describeTimestampFriendly } from './date-utils';
import * as moment from 'moment';


describe('toTimestampMs', () => {
  let now: Date;
  beforeEach(() => now = new Date());

  it('should handle Date objects, equivalent to calling getTime()', () =>
    expect(toTimestampMs(now)).toEqual(now.getTime()));

  describe('For a plausible contemporary date', () => {
    const someDate = moment('2020-10-13');
    const unixTime = someDate.unix();
    const timeValueMs = someDate.valueOf();

    it('should give the right instant if given milliseconds as input', () =>
      expect(toTimestampMs(timeValueMs)).toEqual(timeValueMs));

    it('should give the right instant if given seconds (unix time) as input', () =>
      expect(toTimestampMs(unixTime)).toEqual(timeValueMs));
  });
});


describe('describeTimestampFriendly', () => {

  describe('Cases where it should return the string "Never"', () => {


    it('should when the date is null', () =>
      expect(describeTimestampFriendly(null)).toEqual('Never'));

    it('should when the date is undefined', () =>
      expect(describeTimestampFriendly(undefined)).toEqual('Never'));

    it('should when the date is 0 (technically not correct, but extremely unlikely to be a real data point)', () =>
      expect(describeTimestampFriendly(0)).toEqual('Never'));
  });
})
