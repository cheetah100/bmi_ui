import { before } from 'lodash-es';
import { createMapFrom, createGroupedMapFrom } from './collection-utils';

interface FruitRanking {
  variety: string;
  type: string;
  rating: number;
}

const FRUIT_RANKING: FruitRanking[] = [
  {variety: 'Braeburn', type: 'apple', rating: 3.5},
  {variety: 'Red Delicious', type: 'apple', rating: 0},
  {variety: 'Jazz', type: 'apple', rating: 4},
  {variety: 'Golden Queen', type: 'peach', rating: 5},
  {variety: 'Gem', type: 'avocado', rating: 4},
  {variety: 'Hass', type: 'avocado', rating: 4},
  {variety: 'Mon Thong', type: 'durian', rating: undefined},
  {variety: 'Cavendish', type: 'banana', rating: 3},
  {variety: 'Gros Michel', type: 'banana', rating: undefined}
];

const FRUIT_TYPES = new Set(FRUIT_RANKING.map(f => f.type));


describe('createMapFrom', () => {
  describe('With no value function provided', () => {
    let fruitByVariety: Map<string, FruitRanking>;
    beforeEach(() => fruitByVariety = createMapFrom(FRUIT_RANKING, f => f.variety));

    it('should have the same number of entries as the array as there are no duplicates', () => {
      expect(fruitByVariety.size).toEqual(FRUIT_RANKING.length);
    });

    it('should map the key to the actual object', () =>
      expect(fruitByVariety.get('Jazz')).toBe(FRUIT_RANKING.find(f => f.variety === 'Jazz')));
  });

  describe('With a value function', () => {
    let ratingByVariety: Map<string, number>;
    beforeEach(() => ratingByVariety = createMapFrom(FRUIT_RANKING, f => f.variety, f => f.rating));

    it('should map to the right value', () =>
      expect(ratingByVariety.get('Red Delicious')).toBe(0));
  });
});


describe('createGroupedMapFrom', () => {
  describe('With no value function', () => {
    let fruitByType: Map<string, FruitRanking[]>;
    beforeEach(() => fruitByType = createGroupedMapFrom(FRUIT_RANKING, f => f.type));

    it('should have one group for each unique key', () =>
      expect(fruitByType.size).toBe(FRUIT_TYPES.size));

    it('should have an array for each entry the map', () => {
      expect(Array.from(fruitByType.values()).every(value => Array.isArray(value))).toBeTruthy();
    });


    it('should preserve the original order within the groups', () =>
      expect(fruitByType.get('apple').map(f => f.variety)).toEqual(['Braeburn', 'Red Delicious', 'Jazz']));
  });

  describe('With a value function', () => {
    let varietiesByType: Map<string, string[]>;
    beforeEach(() => varietiesByType = createGroupedMapFrom(FRUIT_RANKING, f => f.type, f => f.variety));

    it('should have one group for each unique key', () =>
      expect(varietiesByType.size).toBe(FRUIT_TYPES.size));

    it('should have the expected items in the group', () =>
      expect(varietiesByType.get('avocado')).toEqual(['Gem', 'Hass']));
  });

  describe('Non-string keys', () => {
    let varietiesByRating: Map<number, string[]>;
    beforeEach(() => varietiesByRating = createGroupedMapFrom(FRUIT_RANKING, f => f.rating, f => f.variety));

    it('should handle `0` as a valid key', () =>
      expect(varietiesByRating.get(0)).toEqual(['Red Delicious'])); // it's the worst apple.

    it('should do the grouping like you would expect, preserving array order', () =>
      expect(varietiesByRating.get(4)).toEqual(['Jazz', 'Gem', 'Hass']));

    it('should support undefined keys', () =>
      expect(varietiesByRating.get(undefined)).toEqual(['Mon Thong', 'Gros Michel']));
  })
});
