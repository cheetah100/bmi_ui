import { CacheEntry, CacheEntryWatchOptions } from './cache-entry';
import { Observable, Subject, Subscription, Notification } from 'rxjs';
import { finalize, materialize, take } from 'rxjs/operators';

import { ObservableWatcher } from './observable-watcher';


describe('CacheEntry', () => {

  // This tests some of the basic behaviour. You need to manually call update
  // to add a value, there's no automatic refreshing logic. In this mode it's
  // kind of like a BehaviourSubject on steroids...
  describe('With no source pipeline', () => {
    let cache: CacheEntry<string>;
    let currentTime: number;
    let emittedValues: string[];

    beforeEach(() => {
      currentTime = 0;
      emittedValues = [];
      cache = new CacheEntry({
        source: null,

        // Use a fake timesource in the tests. Increments every call should
        // ensure some time progression
        getCurrentTime: () => currentTime++,
        maxAge: 10000
      });
      cache.watch().subscribe(value => emittedValues.push(value));
    });


    it('should initially emit nothing', () => expect(emittedValues).toEqual([]));

    it('should initially have an invalid state', () => expect(cache.stateSnapshot.isValid).toBeFalsy());

    it('should emit a value when update is called', () => {
      cache.update('Foo');
      expect(emittedValues).toEqual(['Foo']);
    });

    it('should correctly emit subsequent updates too', () => {
      cache.update('Foo');
      cache.update('Bar');
      expect(emittedValues).toEqual(['Foo', 'Bar']);
    });

    it('should not emit a value when calling invalidate', () => {
      cache.update('Foo');
      cache.invalidate();
      expect(emittedValues).toEqual(['Foo']);
    });

    it('should emit the latest value to new subscriptions', () => {
      cache.update('Foo');
      cache.update('Bar');
      const values = [];
      cache.watch().subscribe(value => values.push(value));
      expect(values).toEqual(['Bar']);
    });

    it('should not emit invalidated values to new subscriptions', () => {
      cache.update('Foo');
      cache.update('Bar')
      cache.invalidate();
      const values = [];
      cache.watch().subscribe(value => values.push(value));
      expect(values).toEqual([]);
    });

    it('should only emit distinct values', () => {
      cache.update('Foo');
      cache.update('Foo');
      cache.update('Foo');
      cache.update('Bar');
      expect(emittedValues).toEqual(['Foo', 'Bar']);
    });

    it('should do nothing when refresh is called', () => {
      cache.update('Foo');
      cache.refresh();
      expect(emittedValues).toEqual(['Foo']);
    });

    describe('Stale value handling', () => {
      beforeEach(() => {
        cache.update('Foo');
        currentTime += 20000;
      });

      function doesEmitValue(options: CacheEntryWatchOptions): boolean {
        const results: string[] = [];
        cache.watch(options).pipe(take(1)).subscribe(value => results.push(value));
        return results.length > 0;
      }

      it('should be currently stale', () => {
        expect(cache.isStale()).toBeTruthy();
      });

      it('should emit stale values by default', () => {
        expect(doesEmitValue({})).toBeTruthy();
      });

      it('should not emit the stale value if option is set to truthy', () => {
        expect(doesEmitValue({emitStaleEntries: true})).toBeTruthy();
      });

      it('should not emit the stale value if option is set to false', () => {
        expect(doesEmitValue({emitStaleEntries: false})).toBeFalsy();
      });

      it('should emit if maxAge is provided to watch', () => {
        expect(doesEmitValue({emitStaleEntries: false, maxAge: 500000})).toBeTruthy();
      });

      it('should treat a null maxAge as indefinite (always fresh)', () => {
        expect(doesEmitValue({emitStaleEntries: false, maxAge: null})).toBeTruthy();
      });
    });


    describe('Setting an error', () => {
      let error: Error;
      let watcher: ObservableWatcher<string>;
      let watcherWithErrors: ObservableWatcher<string>;

      beforeEach(() => {
        watcher = new ObservableWatcher(cache.watch());
        watcherWithErrors = new ObservableWatcher(cache.watch({throwErrors: true}));
        cache.update('Foo');
        currentTime += 1000;
        error = new Error('It went wrong');
        cache.setError(error);
      });

      it('should not be valid', () =>
        expect(cache.isValid).toBeFalsy());

      it('should not affect a default watch (no throw by default)', () =>
        expect(watcher.hasError).toBeFalsy())

      it('should throw an error if watch() is called with the throwErrors: true option', () =>
        expect(watcherWithErrors.hasError).toBeTruthy());

      it('should throw the error object that was put in', () =>
        expect(watcherWithErrors.error).toBe(error));

      it('should return true from the hasError property', () =>
        expect(cache.hasError).toBeTruthy());

      it('should return the error object from the error property', () =>
        expect(cache.error).toBe(error));

      describe('On calling update() after an error', () => {
        beforeEach(() => cache.update('Updated'));

        it('should clear the error state', () =>
          expect(cache.hasError).toBeFalsy());

        it('should return undefined from the error property', () =>
          expect(cache.error).toBeUndefined());

        it('should emit the new value from the default watcher', () =>
          expect(watcher.values).toEqual(['Foo', 'Updated']));
      })
    });
  });

  function onSubscribe<T>(fn: () => void) {
    return (source: Observable<T>) => {
      fn(); return source;
    }
  }


  // This is the interesting mode. We can provide it with an observable that
  // will be used as the source of data.
  describe('With a source', () => {
    let cache: CacheEntry<string>;
    let sourceSubjects: Subject<string>[];
    let refreshTrigger: Subject<void>;
    let subscriptionCount: number;
    let closedCount: number;
    let currentTime: number;

    function addSourceSubject() {
      const subject = new Subject<string>();
      sourceSubjects.push(subject);
      return subject;
    }

    function latestSourceSubject(): Subject<string> {
      return sourceSubjects[sourceSubjects.length - 1];
    }

    beforeEach(() => {
      subscriptionCount = 0;
      closedCount = 0;
      currentTime = 0;
      sourceSubjects = [];
      refreshTrigger = new Subject<void>();
      cache = new CacheEntry<string>({
        source: () => {
          return addSourceSubject().pipe(
            onSubscribe(() => subscriptionCount += 1),
            finalize(() => closedCount += 1)
          );
        },
        considerRefreshTrigger: refreshTrigger,
        getCurrentTime: () => currentTime += 1,
        maxAge: 10000
      });
    });

    it('should not subscribe to the source immediately', () => {
      expect(subscriptionCount).toEqual(0);
    });

    it('should subscribe once you watch', () => {
      cache.watch().subscribe();
      expect(subscriptionCount).toBe(1);
    });

    it('should not subscribe multiple times', () => {
      cache.watch().subscribe();
      cache.watch().subscribe();
      expect(subscriptionCount).toBe(1);
    });

    it('should unsubscribe from the source once the watch subscription ends', () => {
      const watchSub = cache.watch().subscribe();
      expect(subscriptionCount).toBe(1);
      expect(closedCount).toBe(0);

      watchSub.unsubscribe();

      expect(closedCount).toBe(1);
    });

    describe('Emitting values', () => {
      let emittedValues: string[];
      beforeEach(() => {
        emittedValues = [];
        cache.watch().subscribe(value => emittedValues.push(value));
      });

      it('should not emit anything before the source produces a value', () => {
        expect(emittedValues).toEqual([]);
      });

      it('should emit the values produced by the source', () => {
        latestSourceSubject().next('Foo');
        expect(emittedValues).toEqual(['Foo']);
      });

      it('should handle multiple values emitted from the source', () => {
        latestSourceSubject().next('Foo');
        latestSourceSubject().next('Bar');
        expect(emittedValues).toEqual(['Foo', 'Bar']);
      });
    });

    describe('Refreshing', () => {
      let watchSub: Subscription;
      beforeEach(() => {
        watchSub = cache.watch().subscribe();
        latestSourceSubject().next('Initial');
      });

      it('should initially subscribe to the source', () => {
        expect(subscriptionCount).toBe(1);
        expect(closedCount).toBe(0);
      });

      describe('things that should trigger a refresh', () => {
        function expectItRefreshedOnce()  {
          expect(subscriptionCount).toBe(2);
          expect(closedCount).toBe(1);
        }

        it('should refresh: .refresh()', () => {
          cache.refresh();
          expectItRefreshedOnce();
        });

        it('should refresh: options.considerRefreshTrigger IF data is stale', () => {
          currentTime += 1000000;
          refreshTrigger.next();
          expectItRefreshedOnce();
        });

        it('should refresh: .invalidate()', () => {
          cache.invalidate();
          expectItRefreshedOnce();
        });

        it('should refresh: stale data when starting a new connection', () => {
          watchSub.unsubscribe();
          currentTime += 1000000;

          cache.watch().subscribe();
          expectItRefreshedOnce();
        });

        it('should NOT refresh: fresh-enough data when starting a new connection', () => {
          watchSub.unsubscribe();
          currentTime += 1000;
          cache.watch().subscribe();

          expect(subscriptionCount).toBe(1);
          expect(closedCount).toBe(1);
        });

        it('should NOT refresh: fresh-enough data when using options.considerRefreshTrigger', () => {
          currentTime += 1000;
          refreshTrigger.next();
          expect(subscriptionCount).toBe(1);
          expect(closedCount).toBe(0  );
        });
      })
    });

    describe('Refresh and invalidate on watch()', () => {
      let existingWatcher: ObservableWatcher<string>;

      beforeEach(() => {
        // Initially, we have created and finished a source, so there is an
        // existing value. Our subscription/close counts should be balanced.
        existingWatcher = new ObservableWatcher(cache.watch());
        latestSourceSubject().next('Value');
        latestSourceSubject().complete();

        // Check some assumptions
        expect(sourceSubjects.length).toBe(1);
        expect(subscriptionCount).toBe(1);
        expect(subscriptionCount).toEqual(closedCount);
      });

      describe('refreshOnSubscribe', () => {
        let observable: Observable<string>;

        beforeEach(() => {
          observable = cache.watch({refreshOnSubscribe: true, emitStaleEntries: true});
        });

        it('should not have created a new source or subscription if we have not subcribed', () => {
          expect(subscriptionCount).toBe(1);
        });

        it('should not have requested a refresh before subscribing', () =>
          expect(cache.stateSnapshot.refreshRequested).toBeFalsy());

        describe('Once subscribed', () => {
          let watcher: ObservableWatcher<string>;
          beforeEach(() => watcher = new ObservableWatcher(observable));

          it('should create a subscription', () =>
            expect(subscriptionCount).toBe(2));

          it('should emit the existing value because it is not stale', () =>
            expect(watcher.values).toEqual(['Value']));

          it('should have a refresh in progress', () =>
            expect(cache.stateSnapshot.refreshInProgress));

          it('should emit any newly emitted values', () => {
            latestSourceSubject().next('Updated');
            latestSourceSubject().next('Updated Again');
            expect(watcher.values).toEqual(['Value', 'Updated', 'Updated Again']);
          });

          describe('A subsequent subscription to the same observable, after the refresh is done', () => {
            let watcher2: ObservableWatcher<string>;
            beforeEach(() => {
              latestSourceSubject().next('Updated');
              latestSourceSubject().complete();
              currentTime += 1000;

              watcher2 = new ObservableWatcher(observable);
            });

            it('should have started another refresh', () =>
              expect(cache.stateSnapshot.refreshInProgress));
          });
        });
      });

      describe('invalidateOnSubscribe', () => {
        let observable: Observable<string>;

        beforeEach(() => {
          observable = cache.watch({invalidateOnSubscribe: true});
        });

        it('should not have created a new source or subscription if we have not subcribed', () => {
          expect(subscriptionCount).toBe(1);
        });

        describe('After subscribing', () => {
          let watcher: ObservableWatcher<string>;
          beforeEach(() => watcher = new ObservableWatcher(observable));

          it('should mark the cache as invalid', () =>
            expect(cache.isValid).toBeFalsy());

          it('should not emit the current value, because it is invalid', () =>
            expect(watcher.values).toEqual([]));
        })
      });

    });

    describe('handling errors and completion', () => {
      let emittedEvents: Notification<string>[];
      let subscription: Subscription;

      beforeEach(() => {
        emittedEvents = [];
        subscription = cache.watch().pipe(
          materialize()
        ).subscribe(event => emittedEvents.push(event));

      });

      describe('If the source completes', () => {
        beforeEach(() => latestSourceSubject().complete());

        it('should not emit anything', () => {
          expect(emittedEvents).toEqual([]);
        });

        it('should open a new source subscription when refreshed', () => {
          cache.refresh();
          expect(sourceSubjects.length).toEqual(2);
        });

        it('should emit new values after being refreshed', () => {
          cache.refresh();
          latestSourceSubject().next('Foobar');
          expect(emittedEvents[0].value).toEqual('Foobar');
        });
      });

      describe('if the subscription terminates during a refresh', () => {
        beforeEach(() => {
          cache.refresh();
          subscription.unsubscribe();
        });

        it('should no longer consider a refresh to be in progress', () => {
          expect(cache.stateSnapshot.refreshInProgress).toBeFalsy();
        });

        it('should still think a refresh was requested', () => {
          expect(cache.stateSnapshot.refreshRequested).toBeTruthy();
        });
      });

      describe('If the source errors', () => {
        beforeEach(() => {
          latestSourceSubject().error(new Error('Oops'));
        });

        it('should not emit an error event', () => {
          expect(emittedEvents.length).toBe(0);
        });

        it('should still think a refresh is necessary', () => {
          expect(cache.needsRefresh).toBeTruthy();
        });

        it('should emit values on subsequent refreshes', () => {
          cache.refresh();
          latestSourceSubject().next('Foobar');
          expect(emittedEvents[0].value).toEqual('Foobar');
        });
      });
    });
  });
});
