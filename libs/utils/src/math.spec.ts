import { mapRange, weightedArithmeticMean, weightedGeometricMean } from './math';


describe('mapRange()', () => {
  it('should map the value to the new range correctly', () => {
    expect(mapRange(3, [0, 10], [0, 20])).toBe(6);
    expect(mapRange(-3, [-10, 0], [-20, -10])).toBe(-13);
    expect(mapRange(3, [-10, 0], [0, 20])).toBe(26);
  });
});

describe('weightedGeometricMean', () => {
  const toFixed1 = (num: number): number => Math.round(num * 10) / 10;
  it('should give a weighted multiplicative mean', () => {
    expect(weightedGeometricMean([])).toBeNaN();
    expect(weightedGeometricMean([[1, 0]])).toBeNaN();
    expect(weightedGeometricMean([[1, 9]])).toBe(1);
    expect(toFixed1(weightedGeometricMean([[5, 1], [20, 2]]))).toBe(12.6)
    expect(toFixed1(weightedGeometricMean([[1,1], [2, 1]]))).toBe(1.4);
    expect(toFixed1(weightedGeometricMean([[1, 3], [10, 6], [200, 0]]))).toBe(4.6);
  });
});

describe('weightedArithmeticMean', () => {
  it('should give a weighted mean', () => {
    expect(weightedArithmeticMean([])).toBeNaN();
    expect(weightedArithmeticMean([[1, 0]])).toBeNaN();
    expect(weightedArithmeticMean([[1, 9]])).toBe(1);
    expect(weightedArithmeticMean([[5, 1], [20, 2]])).toBe(15)
    expect(weightedArithmeticMean([[1,1], [2, 1]])).toBe(1.5);
    expect(weightedArithmeticMean([[1, 3], [10, 6], [200, 0]])).toBe(7);
  });
});
