import { Observable, BehaviorSubject } from 'rxjs';
import { distinctUntilChanged, map, share, switchMap } from 'rxjs/operators';

import { CacheEntry, CacheEntryOptions, CacheEntryWatchOptions } from './cache-entry';

import lodashDefaults from 'lodash-es/defaults';
import lodashPick from 'lodash-es/pick';


export interface ObservableMapOptions<TKey, T> {
  source?: (key: TKey) => Observable<T>;
  keyFunction?: (key: TKey) => any;
  considerRefreshTrigger?: Observable<any>;
  maxAge?: number;
  emitStaleEntries?: boolean;
  getCurrentTime?: () => number;
}


export class ObservableMap<TKey, T> {
  private readonly options: Readonly<ObservableMapOptions<TKey, T>> = {};

  private readonly entryDefaultOptions: CacheEntryOptions<T>;
  private readonly keyFunction: (key: TKey) => any;
  private readonly map = new Map<any, CacheEntry<T>>();

  // The set of key objects that have been added to the map. This is used to
  // implement the keys() method
  private keySet = new Set<TKey>();

  private keysChanged = new BehaviorSubject<void>(undefined);

  constructor(options: ObservableMapOptions<TKey, T> = {}) {
    this.options = {
      ...this.options,
      ...options
    };
    this.keyFunction = this.options.keyFunction;
    this.entryDefaultOptions = {
      considerRefreshTrigger: this.options.considerRefreshTrigger ? this.options.considerRefreshTrigger.pipe(share()) : undefined,
      maxAge: this.options.maxAge,
      emitStaleEntries: this.options.emitStaleEntries,
      getCurrentTime: this.options.getCurrentTime
    };
  }

  get(key: TKey): T {
    const entry = this.getEntry(key)
    if (entry && entry.stateSnapshot.isValid) {
       return entry.stateSnapshot.value;
    } else {
      return undefined;
    }
  }

  set(key: TKey, value: T): void {
    const entry = this.getOrCreateEntry(key);
    entry.update(value);
  }

  has(key: TKey): boolean {
    return this.map.has(this.mapKey(key));
  }

  /**
   * Triggers a refresh on a map key.
   *
   * This will try and re-subscribe to the source to get a new value, but
   * existing values will still be emitted until this new value comes out.
   */
  refresh(key: TKey): void {
    const entry = this.getEntry(key);
    if (entry) {
      entry.refresh();
    }
  }

  /**
   * Invalidates a given entry if it exists.
   *
   * This is a stronger form of refresh; it marks the current value as invalid
   * then refreshes. By invalidating the value, it means it won't be given to
   * new subscriptions --- they will wait for the new value.
   */
  invalidate(key: TKey): void {
    const entry = this.getEntry(key);
    if (entry) {
      entry.invalidate();
    }
  }

  keys(): IterableIterator<TKey> {
    return this.keySet.values();
  }

  watch(key: TKey, options?: CacheEntryWatchOptions): Observable<T> {
    return this.getOrCreateEntry(key).watch(options);
  }

  isLoading(key: TKey): Observable<boolean> {
    return this.keysChanged.pipe(
      map(() => this.getEntry(key)),
      distinctUntilChanged(),
      switchMap(entry => entry.isLoading),
    );
  }

  private getEntry(key: TKey): CacheEntry<T> {
    return this.map.get(this.mapKey(key));
  }

  private getOrCreateEntry(key: TKey): CacheEntry<T> {
    const mapKey = this.mapKey(key);
    if (!this.map.has(mapKey)) {
      this.keySet.add(key);
      this.map.set(mapKey, this.makeEntry(key));
      this.keysChanged.next();
    }
    return this.map.get(mapKey);
  }

  private makeEntry(key: TKey): CacheEntry<T> {
    const sourceFunction = this.options.source;
    return new CacheEntry<T>(
      lodashDefaults({
        source: sourceFunction ? () => sourceFunction(key) : undefined
      }, this.entryDefaultOptions)
    );
  }

  private mapKey(key: TKey): any {
    const fn = this.keyFunction;
    if (!fn) {
      // The default behaviour is identity mapping TKey -> UKey. Maps don't
      // really care about the key type, as long as they are equal so while not
      // strictly typesafe this assumption is good enough for our purposes.
      return key;
    } else {
      return fn(key);
    }
  }
}
