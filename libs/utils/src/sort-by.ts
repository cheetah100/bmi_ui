export type SortableValue = number | string | boolean | Date;


interface SortData<T> {
  row: T;
  index: number;
  sortValues: SortableValue[];
}


type Comparator<T> = (a: T, b: T) => number;


function prepareSortData<T>(data: T[], generateSortKey: (row: T, rowIndex: any) => SortableValue | SortableValue[]): SortData<T>[] {
  return data.map((row, index) => {
    const sortKey = generateSortKey(row, index);
    return <SortData<T>>{
      row: row,
      index: index,
      sortValues: Array.isArray(sortKey) ? sortKey : [sortKey]
    };
  });
}


function makeSortDataComparer<T>(direction: number): Comparator<SortData<T>> {
  return (a, b) => {
    const numOfSortValues = Math.max(a.sortValues.length, b.sortValues.length);
    for (let i = 0; i < numOfSortValues; i++) {
      const aValue = a.sortValues[i];
      const bValue = b.sortValues[i];

      const aIsNull = aValue === null || aValue === undefined;
      const bIsNull = bValue === null || bValue === undefined;

      let comparisonResult: number;
      if (aValue === bValue || (aIsNull && bIsNull)) {
        comparisonResult = 0;
      } else if (aValue > bValue || (typeof (aValue) === 'number' && typeof (bValue) === 'string') || bIsNull) {
        // Checking bIsNull will put null values last in the ordering.
        comparisonResult = 1;
      } else {
        comparisonResult = -1;
      }

      if (comparisonResult !== 0) {
        return comparisonResult * direction;
      } else {
        continue;
      }
    }

    // Items are equal, so compare the index to ensure the sort order is stable.
    return (a.index - b.index) * direction;
  };
}

export type SortValueFunction<T> = (row: T, index: number) => SortableValue | SortableValue[];


/**
 * @deprecated Use lodash instead.
 * Sorts an array using a value from the item as the sort key.
 *
 * This uses a function or property lookup on each item to determine the value
 * to sort by. Sorting by multiple values can be done by returning an array of
 * values from the sort function -- these will be compared starting at index 0.
 *
 * The sort value function is called once per item.
 *
 * This sort implementation is stable. If the items are equal, then ties will be
 * broken using the index from the original data array.
 *
 * @param data an array of items to sort
 * @param sortKey a string (property lookup) or function to determine the value to use as the sort key.
 * @returns a sorted version of the data array. The original array is unmodified.
 */
export function sortBy<T>(data: T[], sortKey: string | SortValueFunction<T>, sortDirection: number = 1): T[] {
  let sortValueFunction: SortValueFunction<T>;
  if (typeof (sortKey) === 'string') {
    sortValueFunction = (row: any) => [row[sortKey]];
  } else {
    sortValueFunction = sortKey;
  }

  const sortData = prepareSortData(data, sortValueFunction);
  sortData.sort(makeSortDataComparer(sortDirection));
  return sortData.map(d => d.row);
}
