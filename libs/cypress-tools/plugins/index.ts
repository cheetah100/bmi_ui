import { RouteParam } from '../mocking/data-server';
import { hasExtension, hasJsonExtension } from '../helper-functions';
import { Gravity } from '../mocking/gravity';
import { FixtureCache, FixtureCacheEntry } from '@bmi/cypress/mocking/fixture-cache';
const fs = require('fs');
const fse = require('fs-extra');

const nullTasks = {
  resetCoverage({ isInteractive }) {
    console.log(`resetCoverage`);
    return null;
  },
  combineCoverage(sentCoverage) {
    console.log(`combineCoverage`);
    return null;
  },
  coverageReport() {
    console.log(`coverageReport`);
    return null;
  },
};

const { preprocessTypescript } = require('@nrwl/cypress/plugins/preprocessor');
const registerCodeCoverageTasks = require('@cypress/code-coverage/task');

const proxyDisabled = process.env['DISABLE_DEV_PROXY'] || false;
const generateCoverage =  process.env['GENERATE_CYPRESS_COVERAGE_REPORT'] || false;

export interface CreateFixtureOptions {
  routes: RouteParam | RouteParam[];
  fixturesFolder: string;
  basePath?: string;
}

export const plugins = (on, config) => {
  /* Controls the coverage generation at runtime.
  * Aim is to reduce time it takes to run the tests while writing the specs
  * The Test runner has to be re-started for this to work, use this command
  * npm run cy-open-admin
  * This command assumes that ng serve is running, which is automatically
  * started by
  * npx ng e2e bmi-admin-e2e --watch  --browser chrome
  */
  if (!generateCoverage) {
    // This setting should flow through to the code coverage task.
    // It does not.
    on('file:preprocessor', preprocessTypescript(config));
    config.env.coverage = false;
    config.env.codeCoverageTasksRegistered = true;
    on('task', nullTasks);
  } else {
    on('file:preprocessor', preprocessTypescript(config));
    registerCodeCoverageTasks(on, config);
  }

  on('task', {
    createFixtures( options: CreateFixtureOptions) {
      const routes:RouteParam[]= Array.isArray(options.routes) ? options.routes : [options.routes];
      console.log('createFixtures task => requested routes: ', routes.map(route => route.alias).join(', '));
      // const basePath = options?.basePath || '';
      // console.log('createFixtures => basePath: ', basePath)
      const promises = []
      return new Promise((resolve, reject) => {
        const createdFixtures = routes.filter( (route) => {
          const rootPath = __dirname;
          let fixtureFilePath = `${options.fixturesFolder}/${route.fixture}`;
          if (!hasExtension(fixtureFilePath)) {
            fixtureFilePath += '.json'
          }
          console.log(' => fixtureFilePath: ', fixtureFilePath);
          if (route.content) {
            return false;
          }
          if (!fs.existsSync(fixtureFilePath)) {
            if (proxyDisabled) {
              console.log(`PROXY DISABLED: fixture: ${fixtureFilePath} does not exist`);
              return false;
            } else {
              console.log('does not exist: ', route)
              promises.push(Gravity.callAPI(route).then(response => {
                // Only format json before writing it to file
                let data;
                if (hasJsonExtension(fixtureFilePath)) {
                  const jsonObject = JSON.parse(response.body);
                  data = JSON.stringify(jsonObject, null, 4)
                } else {
                  data = response.body;
                }
                fse.outputFileSync(fixtureFilePath, data);
                console.log(`fixture created `, route.alias);
              }).catch(error => console.error(error)));
              return true;
            }
          } else {
            console.log('createFixtures => fixtureFile exists: ', fixtureFilePath)
            return false;
          }
        });
        Promise.all(promises).then( () => {
          console.log(`created fixtures`, createdFixtures)

          resolve(createdFixtures);
        })

      })
    },


    /**
     * Read fixture from server
     * @param route
     */
    readFixture(route: RouteParam) {
      const cache = FixtureCache.getInstance();
      console.log(`reading fixture ${route.alias}`)
      return new Promise((resolve, reject) => {
        console.log(`Trying to get data from cache`)
        const cachedEntry:FixtureCacheEntry = cache.getEntry(route.alias)
        if (cachedEntry !== null) {
          console.log(`found in cache`)
          const dataAsString = JSON.stringify(cachedEntry.data);
          console.log('data received: ', dataAsString.substr(0,100));
          resolve(cachedEntry.data)
        } else {
          console.log(`Not found in cache`)
          Gravity.callAPI(route).then(response => {
            // console.log('getting data from server; status: ', response)
            console.log('data received: ', response.body.substr(0,100));
            const parsedBody = JSON.parse(response.body);
            cache.addEntry(route.alias, parsedBody);
            resolve(parsedBody);
          }).catch(error => {
            console.log(`read fixture error`);
            reject(error);
          })
        }
      });
    }
  });

  return config;
}
