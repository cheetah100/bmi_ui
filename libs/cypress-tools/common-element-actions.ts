import TypeOptions = Cypress.TypeOptions;


const dataCy = (selector:string):string => {
  return `[data-cy="${selector}"]`;
}

const formFieldSelector = (label:string) => {
  return dataCy(`ui-form-field-${label}`);
}
interface TypeFormFieldinputOptions extends TypeOptions {
  fieldType?: string;
}

const typeFormFieldInput = (label: string, value: string, options?: Partial<TypeFormFieldinputOptions>) => {
  console.log(' => options: ', options);
  const fieldType = (options && options.fieldType) ?? null;
  console.log(' => fieldType: ', fieldType);
  if (fieldType) {
    delete options.fieldType;
  }
  cy.get(`${formFieldSelector(label)} input`).type(value, options);
}

const checkFormFieldInput = (label:string, value: string) => {
  cy.get(`${formFieldSelector(label)} input`).should('have.value', value);
}

const selectInput = (dataCySelector: string, formFieldPrefix = false) => {
  return cy.get(`${formFieldSelector(dataCySelector)} input`);
}

const assertInputEmpty = (dataCySelector: string) => {
  return selectInput(dataCySelector).should('be.empty');
}

const enterInputValue = (selector: string, value: string) => {
  return selectInput(selector)
    .clear({force: true})
    .type(value);
}

const checkInputValue = (selector: string, value: string) => {
  return selectInput(selector).should('have.value', value);
}

const getUiSelect = (selector: string) => cy.get(`${selector} ui-select-toggler`);

const getUiList = (selector: string) => cy.get(`${selector}`);

const uiSelectContains = (selector: string, contains: string) => {
  cy.get(`${selector} ui-select-toggler`).contains(contains)
  return getUiSelect(selector).contains(contains);
}

const pickUiOption = (
  selector: string,
  optionText: string,
  force: boolean = true
): void => {

  // We need to close existing open selects first.
  // Otherwise, this dropdown might be hidden behind one.
  cy.get('body').click();

  getUiSelect(selector).click();

  // Make sure we only check the exact option text
  // If we just use the option text, contains can do a partially match
  const optionTextRegExp = new RegExp(`^${optionText}$`);
  cy.get(`${selector} .select-options__option`)
    .contains(optionTextRegExp)
    .scrollIntoView()
    .click({force});

}


export {
  dataCy,
  formFieldSelector,
  selectInput,
  assertInputEmpty,
  enterInputValue,
  checkInputValue,
  getUiSelect,
  getUiList,
  uiSelectContains,
  pickUiOption,
  typeFormFieldInput,
  checkFormFieldInput,
};
