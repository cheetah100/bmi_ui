import { getMock, hasExtension } from '../helper-functions';
import { CreateFixtureOptions } from '@bmi/cypress/plugins';
import { Method } from 'axios';
import { cloneDeep } from 'lodash-es';
import { Subject, Observable, ReplaySubject } from 'rxjs';
import { Gravity } from '@bmi/cypress/mocking/gravity';

const DEBUG = true;

export type GenericRouteType =
  | 'app'
  | 'descriptions'
  | 'current-user'
  | 'configs';

export type RouteType = 'generic' | 'test';

export interface RouteParam {
  // Server endPoint
  endPoint: string;
  // Name of file storing the fixture data
  // If this is not specfiied the file name will be derived from the alias.
  fixture?: string;
  // alias for this route
  alias?: string;
  // HTTP method for communicates with the server
  method?: Method;
  // Body sent to the server if required
  payLoad?: string | object;
  // indicates whether the route will be mocked
  enabled?: boolean;
  // dynamic data returned by the callback. This overrides the fixture file
  content?: (() => object) | string;
}
export interface DataServerOptions {
  // switch to control automatic mocking when the DataServer is instantiated
  enableMocking?: boolean;
  // basePath for storing fixtures. This will be prepended to the filePath generated
  // from the RouteParam
  basePath?: string;
  // Requested generic routes
  genericRoutes?: GenericRouteType[];
  // List of names for 'all card data'
  allCardNames?: string[];
}

interface RouteDuplicateDectectionEntry {
  alias: string;
  method: string;
}

const endPointAsRegExp = (endPoint: string): RegExp => {
  return new RegExp(`.*${endPoint.replace('?', '\\?')}$`);
};

/**
 * DataServer takes care of mocking routes
 * In dev mode the data for the requested routes are downloaded from the server if
 * the fixture does not exists
 */
export class DataServer {
  allRoutes: RouteParam[];
  enabledTestRoutes: RouteParam[];
  requestedGenericRoutes: RouteParam[];
  testSpecificRoutes: RouteParam[];


  specName = Cypress.spec.name;

  mockSubject: ReplaySubject<string[]> = new ReplaySubject();

  genericFixturesFolder: string =
    Cypress.config('fixturesFolder') || '../../cypress/fixtures';
  testFixturesFolder: string =
    Cypress.config('fileServerFolder') || './src/fixtures';

  availableGenericRoutes: RouteParam[] = [
    getMock('/gravity/spring/app'),
    getMock('/gravity/spring/app/descriptions'),
    getMock('/gravity/spring/user/current', 'current-user'),
    getMock('/gravity/spring/board/configs'),
  ];


  allCardRoutes: RouteParam[];
  allCardNames: string[];

  constructor(
    private routes: RouteParam[],
    private options: DataServerOptions = {}
  ) {
    // Set the fixture path for all generic routes
    this.availableGenericRoutes.forEach((route) => route.fixture = this.getGenericFixturePath(route));
    // Create a list of RouteParams for the requested generic routes
    const requestedGenericRouteNames = options.genericRoutes || [];
    this.requestedGenericRoutes = cloneDeep(
      this.availableGenericRoutes.filter((routeParam) =>
        requestedGenericRouteNames.includes(
          routeParam.alias as GenericRouteType
        )
      )
    );

    /**
     * All Cards is a key value pair for all cards in a board,
     * The 'card ID' is the key and the value is taken from the 'name' field
     * or null if there isn't one
     *
     * All Cards are requested through the DataServerOptions by specifying a list of names
     */
    // Set the fixture path for the All Card Routes
    this.allCardNames = options.allCardNames ?? [];
    // Create the routes for the list of names
    this.allCardRoutes = this.allCardNames.map(card => {
      const route = getMock(`/gravity/spring/board/${card}/cards/list?allcards=true`, card );
      // Fixtures for this route will be stored in the fixtures/all-cards folder
      route.fixture = this.getAllCardsFixturePath(route);
      return route;
    })
    // Disable mocking required for open sourcing BMI UI
    // this.enableMocking = false; //options?.enableMocking || true;
    // Process the spec specific routes
    // Make a copy so that the original are updated
    this.testSpecificRoutes = cloneDeep(routes);
    this.testSpecificRoutes.forEach((route) => route.fixture = this.getTestSpecificFixturePath(route));
    this.enabledTestRoutes = this.testSpecificRoutes;
    // TODO Discuss this approach with team mates
    // .filter(
    //   (route) => route?.enabled === undefined || route.enabled
    // );
    if (this.hasDuplicateAliases()) {
      console.log('!!!!! DUPLICATE ROUTE ALIASES DETECTED !!!!!');
    }

    this.allRoutes = [
      ...this.requestedGenericRoutes,
      ...this.enabledTestRoutes,
      ...this.allCardRoutes,
    ];
    const createFixturesOptions: CreateFixtureOptions = {
      // Only create create fixtures for routes who do not supply data dynamically
      routes: this.allRoutes.filter(route => route?.content === undefined || route?.content === null),
      fixturesFolder: Cypress.config('fixturesFolder') as string,
    };
    if (options?.basePath) {
      createFixturesOptions.basePath = options.basePath;
    }

    if (this.mockingEnabled) {
      cy
        .task('createFixtures', createFixturesOptions, { log: DEBUG })
        .then(createdFixtures => {
          // @ts-ignore
          const createdFixturesNames = createdFixtures.map(fixture => fixture.alias).join(', ');
          if (createdFixturesNames.length > 0) {
            cy.log(`Created Fixtures: ${createdFixturesNames}`)
          } else {
            cy.log('All requested fixtures exist.')
          }


          this.mockRoutes();
        })
    } else {
      console.log(`updating mocked subject`)
      this.mockSubject.next(['test']);
    }

  }

  /**
   * Mock the enabled routes
   */
  mockRoutes() {
    const routesMap = this.allRoutes.map((route) => {
      const fixtureFile = route.fixture;
      const method = route?.method || 'GET';
      cy.intercept(method, endPointAsRegExp(route.endPoint), (req) => {
        console.log(`intercepted ${route.endPoint}`)
        // cy.log(`intercepted ${route.endPoint}`)
        let reply;
        if (route?.content) {
          const body =
            typeof route.content === 'function'
              ? route.content()
              : route.content;
          console.log(`return body: ${body}`)
          // cy.log(`return body: ${body}`)
          reply = { body };
        } else {
          console.log(`return file ${fixtureFile}`);
            reply = { fixture: fixtureFile };
        }
        req.reply(reply);
      }).as(route.alias);
      return `@${route.alias}`;
    });
    cy.log(
      `mocked routes: ${this.enabledTestRoutes
        .map((route) => route.alias)
        .join(', ')}`
    );
    // resolve(routesMap);
    this.mockSubject.next(routesMap);
  }

  /**
   * Get card name for the given board and
   * @param board board name
   * @param id    card id
   * @return Observable yielding the name
   */
  getCardName(board: string, id:string): Observable<string> | null {

    // check if board is mocked
    if (this.allCardNames.find(name => name === board)) {
      const observable = new Observable<string>(observer => {
        console.log("- Observable running");

        this.readFixture(board).then(data => {
          console.log('getCardValue => id => ${id}; data: ', data);
          observer.next(data[id] ?? null);
        })
      });
      return observable;
    }
    return null;
  }

  /**
   * Wait for the routes to be mocked.
   *
   * This method should be called before any fixtures are read to make
   * sure the fixture exists
   *
   * @returns Observable containing the list of aliases mocked
   */
  waitforRoutesToBeMocked(): Observable<string[]> {
    return this.mockSubject.asObservable();
  }

  /**
   * Wait for all card routes to be mocked
   */
  waitForAllCardRoutes() {
    console.log(`all card names`, this.allCardNames);
    const aliases = this.allCardNames.map(name => `@${name}`);
    if (aliases.length > 0) {
      console.log(`waiting for all card routes`)
      cy.wait(aliases);
    } else {
      console.log(`nothing to wait for`)
    }
  }


  /**
   * Read a fixture file
   * This could be either of the generic fixtures, even the ones not mocked
   * or one of the test specific ones.
   * @param routeAlias
   */
  readFixture(routeAlias: string) {
    // const extension = hasExtension(fixtureName) ? '' : '.json';
    // Get the route
    const routesToBeChecked = [...this.testSpecificRoutes, ...this.availableGenericRoutes, ...this.allCardRoutes];
    // console.log(routesToBeChecked)
    const route = routesToBeChecked.find((route) => route.alias === routeAlias);
    if (route === undefined) {
      const msg = `readFixture => routeAlias: ${routeAlias} 'not found'`;
      console.error(msg)
      cy.log(msg);
      return null;
    }
    cy.log(`reading fixture ${routeAlias}; file: ${route.fixture}`);

    if (this.mockingEnabled) {
      // Get the data from the fixture
      console.log(`Reading fixture ${route.alias} from fixture`)
      return cy.fixture(route.fixture);
    } else {
      // get the data from the server
      console.log(`Reading fixture ${route.alias} from server`);
      console.log(route.endPoint)
      const result = cy.task('readFixture', route);
      return result;
    }
  }

  /**
   * Wait for the route requests to complete
   *
   * Use this with care. Waiting for mocked routes is usually not required.
   * See https://docs.cypress.io/guides/references/best-practices.html#Unnecessary-wait-for-cy-request
   */
  waitForRoutes() {
    const aliases = this.enabledTestRoutes.map((route) => `@${route.alias}`);
    if (aliases.length > 0) {
      cy.wait(aliases);
    }
  }

  /**
   * Check if there are duplicate aliases specified
   * @private
   * @returns boolean indicating if duplicates have been found
   */
  private hasDuplicateAliases(): boolean {

    const reducer = (accumulator: RouteDuplicateDectectionEntry[], currentValue) => {
      const entry:RouteDuplicateDectectionEntry = {
        alias: currentValue.alias,
        method: currentValue.method
      }
      if (accumulator.includes(entry)) {
        cy.log(`duplicate alias detected: ${currentValue.alias}`);
      } else {
        accumulator.push(currentValue.alias);
      }
      return accumulator;
    };
    const aliases = this.routes.reduce(reducer, []);
    return this.routes.length !== aliases.length;
  }

  /**
   * Get fixture path for generic routes
   * @param route
   * @private
   */
  private getGenericFixturePath(route: RouteParam): string {
    let fixturePath = route?.fixture || route.alias;
    if (!hasExtension(fixturePath)) {
      fixturePath += '.json';
    }
    return fixturePath;
  }

  /**
   * Get fixture path for all cards routes
   * @param route
   * @private
   */
  private getAllCardsFixturePath(route: RouteParam): string {
    let fixturePath = route?.fixture || route.alias;
    if (!hasExtension(`all-cards${fixturePath}`)) {
      fixturePath += '.json';
    }
    return fixturePath;
  }

  /**
   * get fixture path for test specific routes
   * @param route
   * @private
   */
  private getTestSpecificFixturePath(route: RouteParam): string {
    const fixtureName = route?.fixture || route.alias;
    let fixturePath = `${this.projectRoot}/${this.basePath}/${fixtureName}`;
    if (!hasExtension(fixturePath)) {
      fixturePath += '.json';
    }
    return fixturePath;
  }

  /**
   * Return the project root
   * @private
   */
  private get projectRoot() {
    const tokens = Cypress.config('fileServerFolder').split(/[\/\\]/);
    const root = tokens[tokens.length - 1];
    return root;
  }

  /**
   * Get basePath
   * @private
   */
  private get basePath():string {
    if (this.options?.basePath) {
      return this.options.basePath;
    } else {
      const tokens = this.specName.split('.');
      return tokens[0];
    }
  }

  private get mockingEnabled(): boolean {
    let mockingRequested = Cypress.env('enableMocking');
    console.log(`mockingRequested: ${mockingRequested}`)
    if (mockingRequested === null) {
      mockingRequested = true;
    }
    return mockingRequested;
  }


}

