import { RouteParam } from './data-server';
import * as Cypress from 'cypress';
import RequestOptions = Cypress.RequestOptions;
import { loadSecretsConfig } from '@bmi/ping-auth-helpers';
const got = require('got');

function httpBasicAuth(secret, userId = undefined) {
  let authToken;
  if (secret && userId) {
    authToken = Buffer.from(`${userId}:${secret}`).toString('base64');
  } else if (secret) {
    authToken = secret;
  } else {
    throw new Error(
      'Cannot perform HTTP Basic Auth: must supply encoded secret, or userId and secret'
    );
  }

  return `Basic ${authToken}`;
}
interface Token {
  access_token:string,
  token_type:string
  expires_in: number; // seconds
}

/**
 * provides gravity access to loading API data
 */
export class Gravity {
  static token:Token = null;
  // time when token was refreshed (in milli seconds)
  static nextTokenRefreshTime:number;
  // TODO Please specfiy
  static BASE_URL = 'BASE_GRAVITY_SERVICE_URL';

  static async callAPI(route: RouteParam) {
    // make sure the auth token is present
    if (!Gravity.hasValidToken()) {
      console.log(`calling get token`);
      await Gravity.getToken();
    } else {
      console.log(`valid token available`)
    }
    const method = route?.method || 'GET';
    let url;
    // Check if the endPoint is a full URL
    if (route.endPoint.startsWith('http')) {
      url = `${route.endPoint}`;
    } else {
      url = `${Gravity.BASE_URL}${route.endPoint}`;
    }
    const options: Partial<RequestOptions> = {
      method,
      url,
      headers: {
        Authorization: `Bearer ${Gravity.token.access_token}`
      }
    };
    if (method === 'POST') {
      // We assume JSON format for post requests
      options.headers = {
        ...options.headers,
        'Content-Type': 'application/json',
        Accept: 'application/json',
      };
      if (route?.payLoad) {
        options.body = route.payLoad;
      }
    }

    console.log(`options: `, options)
    return got(options).catch(error => console.log('got error: ', error))
  }

  static getToken() {
    const secretsFilePath = `${__dirname}/../../../../bmi-dev-secrets.json`;
    const config = loadSecretsConfig(secretsFilePath);
    // TODO Please specify
    const url = 'SPECIFY_TOKEN_URL';
    return (
      got.post(url, {
        form: true,
        body: {
          grant_type: 'client_credentials',
        },
        headers: {
          Authorization: httpBasicAuth(
            config.client_secret,
            config.client_id
          ),
        },
      })
        .then((response) => {
          Gravity.token = JSON.parse(response.body);
          // Set the next token refresh time to 5 seconds before the deadline
          Gravity.nextTokenRefreshTime = new Date().getTime() + ((Gravity.token.expires_in - 5) * 1000);
          // console.log(`token response body`, response.body)
          console.log(' => token: ', Gravity.token);
          return Gravity.token;
        })
        .catch(error => {
          console.log(`error getting token: `, error)
        })
    );
  }

  static hasValidToken() {

    if (Gravity.token) {
      const currentTime = new Date().getTime();
      // Following console logs are used to check the validity of the Auth token
      console.log(`Grav token: `, Gravity.token);
      console.log(`current time: ${currentTime}`)
      console.log(`token refresh time: ${Gravity.nextTokenRefreshTime}`)
      console.log(`compare result: ${Gravity.nextTokenRefreshTime > currentTime}`)
      console.log(`diff ${Gravity.nextTokenRefreshTime - currentTime}`);
      return (Gravity.nextTokenRefreshTime > currentTime);
    }
    return false;
  }
}
