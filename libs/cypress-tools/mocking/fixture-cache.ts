const fse = require('fs-extra');

export interface FixtureCacheEntry {
  alias:string;
  data: any;
}

/**
 * Implement a caching mechanism for NodeJS
 */
export class FixtureCache {
  private fixtureCache = {};
  private cacheName = 'fixtureCache';
  static instance:FixtureCache = null;

  constructor() {
    const persistedCache = this.retrieve();
    this.fixtureCache = persistedCache || {};
    console.log(`cache contents:`, Object.keys(this.fixtureCache));
  }

  /**
   * Get the singleton instance of the class.
   * Create the instance if it does not exists
   */
  static getInstance():FixtureCache {
    if(!FixtureCache.instance) {
      FixtureCache.instance = new FixtureCache();
    }
    return FixtureCache.instance;
  }

  /**
   * Add new cache entry
   * @param alias cache entry name
   * @param data  data to be cached
   */
  addEntry(alias:string, data:any):void {
    console.log(`Adding ${alias} to cache`)
    this.fixtureCache[alias] = {alias, data};
    this.persist();
  }

  /**
   * Clear all cache entries
   */
  clear():void {
    console.log(`clearing cache`)
    this.fixtureCache = {};
    this.remove();
  }

  /**
   * Delete a cache entry
   * @param alias name of cache entry
   */
  deleteEntry(alias:string):void {
    console.log(`Deleting entry ${alias}`)
    if (this.hasEntry(alias)) {
      delete this.fixtureCache[alias];
      this.persist();
    }
  }

  /**
   * Get entry for given alias
   * @param alias entry name
   */
  getEntry(alias:string):FixtureCacheEntry {
    console.log(`Getting entry ${alias}`)
    if (this.hasEntry(alias)) {
      return this.fixtureCache[alias];
    }
    return null;
  }

  /**
   * check if entry exists
   * @param alias entry name
   */
  hasEntry(alias:string):boolean {
    console.log(`Checking entry ${alias}`)
    return this.fixtureCache.hasOwnProperty(alias);
  }

  /**
   * Persist the in memory cache
   * @private
   */
  private persist():void {
    fse.outputFileSync(this.cachePath, JSON.stringify(this.fixtureCache));
  }

  /**
   * Retrieve the persisted cache
   * @private
   */
  private retrieve(): string | null {
    const path = `${this.cacheName}.json`;
    if (fse.pathExistsSync(this.cachePath)) {
      console.log(`cache exists`)
      return fse.readJsonSync(this.cachePath)
    }
    console.log(`no cache`)
    return null;
  }

  /**
   * Get path of filename containing the persisted cache
   * @private
   * @return string
   */
  private get cachePath():string {
    return `${this.cacheName}.json`;
  }

  /**
   * Remove persisted cache
   * @private
   */
  private remove():void {
    fse.removeSync(this.cachePath);
  }
}
