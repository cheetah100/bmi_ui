const fs = require("fs");

const args = process.argv;

if (args.length < 3) {
  console.log(`usage: node showCache.js cachePath key`);
  process.exit()
}
const cacheFilePath = args[2];
// Read users.json file
fs.readFile(cacheFilePath, 'utf8', function(err, data) {
  const parsedData = JSON.parse(data);
  if (args.length >= 4) {
    console.log(parsedData[args[3]])
  } else {
    console.log(Object.keys(parsedData));
  }
});
