Cypress.Commands.overwrite("wait", (originalFn, ms, options) => {
  const enableMocking: boolean = Cypress.env('enableMocking') ?? true;

  console.log(`wait: enableMocking: ${enableMocking}`)
  if (enableMocking) {
    return originalFn(ms, options);
  } else {
    return;
  }
})
