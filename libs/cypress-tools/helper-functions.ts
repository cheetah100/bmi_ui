import { DataServer, RouteParam } from './mocking/data-server';
import base = Mocha.reporters.base;
import { BoardConfig } from '@bmi/gravity-services';
import { Subscription } from 'rxjs';
import { dataCy, formFieldSelector, getUiList, uiSelectContains } from './common-element-actions';

/**
 * MockOptions interface
 */
export interface MockOptions {
  enabled?: boolean
}

/**
 * Determines if a file has a json extension
 * @param fileName name of the file to be checked
 */
export const hasJsonExtension = (fileName: string):boolean => {
  return hasExtension(fileName, 'json');
}

/**
 * Determines if a fileName has an extension
 * @param fileName name of the file to be checked
 * @param extension extension to check for
 *                  defaults to any extension
 */
export const hasExtension = (fileName: string, extension:string = null):boolean => {
  // If no extension has been specified we look for any extension
  if (extension === null) {
    extension = '.+';
  }
  const re = `.*[.]${extension}`;
  const found = fileName.match(re);
  return !!found;
}

/**
 * Create an alias from the last component of the endPoint
 * @returns alias
 */
const createAliasFromEndPoint = ( (route: string):string => {
  const tokens = route.split('/');
  return tokens[tokens.length-1]
})

/**
 * Create a RouteParam structure for a GET http request using the params passed
 * @param endPoint URl to be mocked
 * @param alias alias for the mocked route.
 *              must be unique
 *              defaults to the last component of the endPoint
 * @param fixtureName Name of the fixture used to store the mocked data
 *                    Defaults to the alias name
 * @param content Function or string to provide the contect of the mock
 * @param options Addtional options
 */
export const getMock = (
  endPoint: string,
  alias: string = null,
  fixtureName:string = null,
  content: (() => object) | string = null,
  options: MockOptions = {}
): RouteParam => {
  const routeParam:RouteParam = {
    endPoint,
    alias: alias || createAliasFromEndPoint(endPoint),
    method: 'GET'
  };
  if (fixtureName) {
    routeParam.fixture = fixtureName;
  }
  if (content) {
    routeParam.content = content;
  }
  if (options?.enabled !== null ) {
    routeParam.enabled = options.enabled;
  }
  return routeParam;
};

/**
 * Create a RouteParam structure for a POST http request using the params passed
 * @param endPoint URl to be mocked
 * @param alias alias for the mocked route.
 *              must be unique
 * @param body payload for the POST request
 * @param content Function or string to provide the contect of the mock
 */
export const postMock = (
  endPoint: string,
  alias: string,
  body: string,
  content: (() => object) | string = null
): RouteParam => {
  const options: RouteParam = {
    endPoint,
    alias,
    method: 'POST',
    payLoad: body,
  };
  if (content) {
    options.content = content;
  }
  return options;
};

export interface CheckCardViewOptions {
  includedFields?: Array<string>,
  excludedFields?: Array<string>,
}
/**
 * Check currently rendered Card View
 * @param boardConfig
 * @param card
 * @param dataServer
 */
export const checkCardView = (boardConfig: BoardConfig, card, dataServer: DataServer, options: CheckCardViewOptions = {}) => {
  console.log(' => boardConfig: ', boardConfig);
  dataServer.waitForAllCardRoutes();
  console.log(`all routes done`);
  const filterFieldSelector = `${dataCy('ui-form-field-Filter')} bmi-ui-select-input input`;
  const filterValueSelector = `${dataCy('ui-form-field-Filter')} ${dataCy('map-editor-value-input')}`;
  const checkMapItem = (index: number, key: string, value: string): void => {
    console.log(`checking map item key ${key}; value ${value}`)
    cy.get(filterFieldSelector).eq(index).should('have.value', key);
    cy.get(`${filterValueSelector}`).eq(index)
      .should('have.value', value);
  }
  const checkListItem = (index: number, value: string): void => {
    getUiList(`${dataCy('ui-form-field-Source Entity Field')} bmi-ui-select-input input`).eq(index).should('have.value', value);
  }

  const excludedFields = options.excludedFields ?? ['id', 'gravityTitle'];
  const includedFields = options?.includedFields;
  let fieldsToProcess;
  if (includedFields) {
    fieldsToProcess = Object.values(boardConfig.fields).filter(field => includedFields.includes(field.id))
  } else {
    fieldsToProcess = Object.values(boardConfig.fields)
  }
  fieldsToProcess = fieldsToProcess.filter(field => !excludedFields.includes(field.id) && !field.isMetadata)
  console.log(' => fieldsToProcess.length: ', fieldsToProcess.length);

  console.log(' => metricCard: ', card);
  fieldsToProcess.map( (field) => {
      const subscription = new Subscription()
      console.log(`processing => item: ${field.id}; type: ${field.type}`, );
      // console.log(card)
      const value:string | string[] = card.fields[field.id] || '';
      console.log('   => value: ', value);
      console.log('   => value type: ', typeof value);
      const label = field.label;
      switch (field.type) {

        case 'STRING':
          if (field.optionlist) {
            console.log(`   => optionlist: ${field.optionlist}; id: ${value}`);
            if (value !== '') {
              const cardId = value as string;

              subscription.add(dataServer.getCardName(field.optionlist, cardId).subscribe(entity => {
                uiSelectContains(formFieldSelector(label), entity)
              }))
            }
          } else {
            console.log(`string`)
            cy.log('string')
            cy.get(dataCy(`form-control:text-input:${label}`))
              .should('have.value', value )
          }
          break;
        case 'MAP':
          let index = 0;
          console.log(`MAP`);
          if (typeof value === 'object') {
            Object.entries(value).map(([key, val]) => {
              console.log(`index ${index}`)
              checkMapItem(index, key, (val as string));
              index++;
            })
          } else {
            console.log(`Could not process field ${field.id} with type of MAP and value type of ${typeof value}`);
          }
          break
        case 'LIST':
          console.log(`list`)
          if (Array.isArray(value)) {
            const entries: string[] = value as string[]
            entries.forEach( (entry, valueIndex) => {
              console.log(`index: `, valueIndex)
              checkListItem(valueIndex, entry);
            })
          }

          break
        default:
          console.log(`unknown`)
          console.error(`unknown type ${field.type}`)
      }
      subscription.unsubscribe();
    }
  );

}
