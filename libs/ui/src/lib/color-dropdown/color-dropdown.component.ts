import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Component, forwardRef, Input, Output, EventEmitter } from '@angular/core';

import { COLOR_GROUPS, ColorPalette, ColorDefinition, isHexColor } from '../colors';

@Component({
  templateUrl: './color-dropdown.component.html',
  styleUrls: ['./color-dropdown.component.scss'],
  selector: 'ui-color-dropdown',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ColorDropdownComponent),
      multi: true
    }
  ]
})
export class ColorDropdownComponent implements ControlValueAccessor {
  @Input() colorPalettes: ColorPalette[] = COLOR_GROUPS;
  @Input() nullColorMessage = 'No color';
  @Output() colorPicked = new EventEmitter<ColorDefinition>();

  selectedColor: ColorDefinition;

  get selectedColorString(): string | null {
    return this.selectedColor ? this.selectedColor.cssColor : null;
  }

  selectColor(color: ColorDefinition) {
    this.selectedColor = color;
    this.propagateChange(this.selectedColorString);
    this.colorPicked.emit(color);
  }

  findColor(cssColor: string) {
    for (const palette of this.colorPalettes) {
      const foundColor = palette.colors.find(c => c.cssColor === cssColor);
      if (foundColor) {
        return foundColor;
      }
    }

    if (isHexColor(cssColor)) {
      // allow correct hex string even if not in palette
      // in this case, the previously saved color will show as selected
      // but not in the dropdown
      return new ColorDefinition(cssColor, cssColor);
    } else {
      return null;
    }
  }

  writeValue(value: string) {
    this.selectedColor = this.findColor(value);
  }

  propagateChange = (_: any) => { };

  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  propagateTouch = () => { };

  registerOnTouched(fn) {
    this.propagateTouch = fn;
  }
}
