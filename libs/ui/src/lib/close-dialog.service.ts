import { InjectionToken, Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';


export interface CloseDialogService {
  onClose: Observable<string>;
}


export const CLOSE_DIALOG_SERVICE_TOKEN = new InjectionToken<CloseDialogService>('Close Dialog Service Provider');


@Injectable()
export class SimpleCloseDialogService implements CloseDialogService{
  private closeDialogSubject = new Subject<string>();

  get onClose(): Observable<string> {
    return this.closeDialogSubject.asObservable();
  }

  public triggerClose(dialogId: string = 'test') {
    this.closeDialogSubject.next(dialogId);
  }
}