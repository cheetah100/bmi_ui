import { ObjectMap } from '@bmi/utils';

export interface ColorPalette {
  name: string;
  gridWidth: number;
  colors: ColorDefinition[];
}

export class ColorDefinition {
  constructor(
    public readonly name: string,
    public readonly cssColor: string,
    public readonly whiteText: boolean = false,
    public readonly cssColorClass: string = null,
  ) { }

  get displayName(): string {
    return this.name || this.cssColor;
  }
}



const LEGACY_COLORS = {
  // Core Palette
  black: new ColorDefinition('Black', '#000000', true),
  dkGray2: new ColorDefinition('Dark Gray 2', '#58585B', true),
  medGray2: new ColorDefinition('Medium Gray 2', '#9E9EA2', true),
  ltGray2: new ColorDefinition('Light Gray 2', '#C6C7CA'),
  paleGray1: new ColorDefinition('Pale Gray 1', '#E8EBF1'),
  white: new ColorDefinition('White', '#FFFFFF'),
  lakeBlue: new ColorDefinition('Lake Blue', '#004BAF', true),
  riverBlue: new ColorDefinition('River Blue', '#097DBC', true),
  brandBlue: new ColorDefinition('Brand Blue', '#049FD9', true),
  icyBlue: new ColorDefinition('Icy Blue', '#C4D6ED'),


  // Extended Palette
  mediumBlue: new ColorDefinition('Medium Blue', '#34ADDE'),
  statusBlue: new ColorDefinition('Status Blue', '#64BBE3'),
  dkGray1: new ColorDefinition('Dark Gray 1', '#39393B', true),
  dkGray3: new ColorDefinition('Dark Gray 3', '#626469', true),
  ltGray3: new ColorDefinition('Light Gray 3', '#E9E9E9'),
  dkGreen: new ColorDefinition('Dark Green', '#008516', true),
  fernGreen: new ColorDefinition('Fern Green', '#00AD0B', true),
  medGreen: new ColorDefinition('Medium Green', '#6CC04A'),
  sageGreen: new ColorDefinition('Sage Green', '#ABC233'),


  // Digital Accessible Palette
  dkBlue: new ColorDefinition('Dark Blue', '#2B5592', true),
  deepGreen: new ColorDefinition('Deep Green', '#005C21', true),
  deepRed: new ColorDefinition('Deep Red', '#8B1001', true),
  statusOrange: new ColorDefinition('Status Orange', '#FF7300'),
  orange: new ColorDefinition('Orange', '#FFA000'),
  ltGray1: new ColorDefinition('Light Gray 1', '#A7A9AB'),


  // Additional Status colors
  statusGreen: new ColorDefinition('Status Green', '#42A942'),
  statusYellow: new ColorDefinition('Status Yellow', '#FFCC00'),
  statusRed: new ColorDefinition('Status Red', '#CF2030', true),
  statusTurq: new ColorDefinition('Status Turquoise', '#14A792'),

  // Higcharts Color Palette
  chartMediumPurple: new ColorDefinition('Medium Purple', '#8D9EEA'),
  chartDarkPurple: new ColorDefinition('Dark Purple', '#8054C8'),
  chartLightPurple: new ColorDefinition('Light Purple', '#B593CD'),
  chartLightPink: new ColorDefinition('Light Pink', '#ED99B6'),
  chartDarkPink: new ColorDefinition('Dark Pink', '#E86D96'),
  chartRed: new ColorDefinition('Red', '#E22F00'),
  chartOliveGreen: new ColorDefinition('Olive Green', '#C0D452'),
  chartDarkGreen: new ColorDefinition('Dark Green', '#6CC049'),
  chartLightGreen: new ColorDefinition('Light Green', '#38D07E'),
  chartMediumGreenishBlue: new ColorDefinition('Dark Cyan', '#00CFD2'),
  chartLightGreenishBlue: new ColorDefinition('Cyan', '#70DAD9'),
  chartLightBluishGray: new ColorDefinition('Light Cyan', '#A2D7C6'),
  chartMediumBluishGray: new ColorDefinition('Bluish Gray', '#93C7E8'),
  chartBlue: new ColorDefinition('Blue', '#47BDFB'),
  chartDarkBluishGray: new ColorDefinition('Dark Blue', '#34ACDE'),
  chartNavyBlue: new ColorDefinition('Navy Blue', '#067CBB'),
  chartDarkGray: new ColorDefinition('Dark Gray', '#4A6D7A'),
  chartMediumGray: new ColorDefinition('Medium Gray', '#8599B6'),
  chartLightGray: new ColorDefinition('Light Gray', '#BCC4D0'),
  chartLighterGray: new ColorDefinition('Pearl Gray', '#C5D6EC'),
  chartLightTeal: new ColorDefinition('Light Teal', '#B6B496'),
  chartDarkTeal: new ColorDefinition('Dark Teal', '#88855A'),
  chartDarkBrown: new ColorDefinition('Dark Brown', '#A45B10'),
  chartLightBrown: new ColorDefinition('Light Brown', '#D0B16A'),
};

export const COLORS = {
  ...LEGACY_COLORS,
  // Brand
  sky: new ColorDefinition('Sky', '#00bceb'),
  midnight: new ColorDefinition('Midnight', '#0d274d', true),
  ocean: new ColorDefinition('Ocean', '#1e4471', true),
  // Theme
  primary: new ColorDefinition('Primary', '#00bceb'),
  secondary: new ColorDefinition('Secondary', '#1e4471', true),
  tertiary: new ColorDefinition('Tertiary', '#0d274d', true),
  success: new ColorDefinition('Success', '#6abf4b'),
  danger: new ColorDefinition('Danger', '#e2231a'),
  warningAlt: new ColorDefinition('Warning - Alt', '#eed202'),
  warning: new ColorDefinition('Warning', '#fbab18'),
  dark: new ColorDefinition('Dark', '#495057', true),
  light: new ColorDefinition('Light', '#ced4da'),
  // Grayscale
  white: new ColorDefinition('White', '#ffffff'),
  gray100: new ColorDefinition('Gray - 100', '#f8f8f8'),
  gray200: new ColorDefinition('Gray - 200', '#f2f2f2'),
  gray300: new ColorDefinition('Gray - 300', '#dee2e6'),
  gray400: new ColorDefinition('Gray - 400', '#ced4da'),
  gray500: new ColorDefinition('Gray - 500', '#adb5bd'),
  gray600: new ColorDefinition('Gray - 600', '#6c757d', true),
  gray700: new ColorDefinition('Gray - 700', '#495057', true),
  gray800: new ColorDefinition('Gray - 800', '#343a40', true),
  gray900: new ColorDefinition('Gray - 900', '#212529', true),
  black: new ColorDefinition('Black', '#000000', true),
  // Status
  info: new ColorDefinition('Info', '#64bbe3'),
  // Misc
  link: new ColorDefinition('Link', '#0175a2', true),
};

const COLOR_PALETTES: ObjectMap<ColorPalette> = {
  brand: {
    name: 'Brand',
    gridWidth: 3,
    colors: [
      COLORS.sky,
      COLORS.midnight,
      COLORS.ocean,
    ]
  },

  theme: {
    name: 'Theme',
    gridWidth: 8,
    colors: [
      COLORS.primary,
      COLORS.secondary,
      COLORS.tertiary,
      COLORS.success,
      COLORS.danger,
      COLORS.warningAlt,
      COLORS.warning,
      COLORS.dark,
      COLORS.light,
    ]
  },

  grayscale: {
    name: 'Grayscale',
    gridWidth: 8,
    colors: [
      COLORS.white,
      COLORS.gray100,
      COLORS.gray200,
      COLORS.gray300,
      COLORS.gray400,
      COLORS.gray500,
      COLORS.gray600,
      COLORS.gray700,
      COLORS.gray800,
      COLORS.gray900,
      COLORS.black,
    ]
  },

  status: {
    name: 'Status',
    gridWidth: 5,
    colors: [
      COLORS.success,
      COLORS.info,
      COLORS.warning,
      COLORS.warningAlt,
      COLORS.danger,
    ]
  },

  misc: {
    name: 'Misc',
    gridWidth: 6,
    colors: [
      COLORS.statusRed,
      COLORS.statusOrange,
      COLORS.statusYellow,
      COLORS.statusGreen,
      COLORS.statusTurq
    ]
  },

  chartColors: {
    name: 'Chart Colors',
    gridWidth: 8,
    colors: [
      COLORS.chartNavyBlue,
      COLORS.chartDarkGreen,
      COLORS.chartDarkGray,
      COLORS.chartDarkPurple,
      COLORS.statusOrange,
      COLORS.chartDarkBluishGray,
      COLORS.sageGreen,
      COLORS.chartLightTeal,
      COLORS.chartLightGray,
      COLORS.chartDarkTeal,
      COLORS.chartDarkBrown,
      COLORS.chartMediumBluishGray,
      COLORS.chartLighterGray,
      COLORS.statusTurq,
      COLORS.chartLightBrown,
      COLORS.chartMediumGreenishBlue,
      COLORS.chartLightBluishGray,
      COLORS.chartLightPink,
      COLORS.chartLightGreen,
      COLORS.chartMediumGray,
      COLORS.chartRed,
      COLORS.chartBlue,
      COLORS.chartDarkPink,
      COLORS.chartOliveGreen,
      COLORS.statusYellow,
      COLORS.chartMediumPurple,
      COLORS.chartLightGreenishBlue,
      COLORS.chartLightPurple,
    ]
  }
};


export const COLOR_GROUPS: ColorPalette[] = [
  COLOR_PALETTES.brand,
  COLOR_PALETTES.theme,
  COLOR_PALETTES.grayscale,
  COLOR_PALETTES.status,
  COLOR_PALETTES.misc,
  COLOR_PALETTES.chartColors,
];


const HEX_COLOR_REGEX = /^\#([A-Fa-f0-9]{3,4}|[A-Fa-f0-9]{6}|[A-Fa-f0-9]{8})$/;

export function isHexColor(color: string): boolean {
  return !!color && typeof (color) === 'string' && !!color.match(HEX_COLOR_REGEX);
}
