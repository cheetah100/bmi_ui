import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PortalModule } from '@angular/cdk/portal';
import { OverlayModule } from '@angular/cdk/overlay';
import { MatDialogModule } from '@angular/material/dialog';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';

import { FormFieldComponent } from './form-field/form-field.component';

import { LoaderComponent } from './loader/loader.component';
import { LoadingContainerComponent } from './loader/loading-container.component';
import { ObservableLoaderComponent } from './loader/observable-loader.component';

import { CloseButtonComponent } from './misc/close-button.component';
import { HelpIconComponent } from './misc/help-icon.component';

import { TooltipModule } from './tooltip/tooltip.module';

import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';
import { BaseDropdownComponent } from './base-dropdown/base-dropdown.component';

import { ArrayEditComponent } from './array-edit/array-edit.component';
import { ColorDropdownComponent } from './color-dropdown/color-dropdown.component';

import { HighlightTermPipe } from './pipes/highlight-term.pipe';
import { SingleSelectComponent } from './select-controls/single-select.component';
import { SelectTogglerComponent } from './select-controls/select-toggler.component';
import { FallbackMultiSelectComponent } from './select-controls/fallback-multi-select.component';

import { TabbedSelectComponent } from './select-controls/tabbed-select.component';
import { SelectOptionListComponent } from './select-controls/select-option-list.component';

import { ModalComponent } from './modal/modal.component';
import { ConfirmationModalComponent } from './confirmation-modal/confirmation-modal.component';
import { StackerListComponent } from './stacker-box/stacker-list.component';
import { StackerBoxComponent } from './stacker-box/stacker-box.component';
import { CheckboxComponent } from './checkbox/checkbox.component';
import { DatepickerComponent } from './datepicker/datepicker.component';

import { DataTableComponent } from './data-table/data-table.component';
import { DataTablePaginationComponent } from './data-table-pagination/data-table-pagination.component';

import { SearchInputComponent } from './search-input/search-input.component';

import { AlertBoxComponent } from './alert-box/alert-box.component';
import { ValidationSummaryComponent } from './validation-summary/validation-summary.component';
import { FormSectionHeaderComponent } from './form-section-header/form-section-header.component';

import { BmiUiCollapsibleSectionModule } from './collapsible-section/collapsible-section.module';
import { IconSelectComponent } from './icon-select/icon-select.component';
import { SelectComponent } from './select-controls/select/select.component';

import { StatusIndicatorComponent } from './status-indicator/status-indicator.component';

import { DropdownMenuComponent } from './dropdown-menu/dropdown-menu.component';
import { DropdownMenuItemComponent } from './dropdown-menu/dropdown-menu-item.component';

import { ListGroupComponent } from './list-group/list-group.component';
import { ListGroupItemDirective } from './list-group/list-group-item.directive';
import { RowWithMenuComponent } from './row-with-menu/row-with-menu.component';

import { IconComponent } from './icon/icon.component';
import { ICON_LIBRARY_TOKEN } from './icon/icon-library';
import { SharedUiIconLibrary } from './icon/shared-ui-icon-library';
import { BrandIconLibrary } from './icon/brand-icon-library';
import { FontAwesome4IconLibrary } from './icon/font-awesome-4-icon-library';
import { MultiSelectComponent } from './select-controls/multi-select/multi-select.component';
import { MultiSelectOptionListComponent } from './select-controls/multi-select/multi-select-option-list.component';
import { ListEditorComponent } from './list-editor/list-editor.component';
import { MapEditorComponent } from './map-editor/map-editor.component';

import { MaterialDialogModalComponent } from './modal/material-dialog-modal.component';
import { SelectInputComponent } from './select-controls/select/select-input.component';
import { CronEditorComponent } from './cron/cron-editor.component';
import { ErrorModalComponent } from './error-modal/error-modal.component';

import { PropertyListComponent } from './property-list/property-list.component';

import { DynamicEditorComponent } from './dynamic-editor/dynamic-editor.component';
import { DynamicEditorListComponent } from './dynamic-editor-list/dynamic-editor-list.component';
import { UsingDynamicEditorsForDirective } from './dynamic-editor/using-dynamic-editors-for.directive';
import { DynamicEditorAddMenuComponent } from './dynamic-editor-add-menu/dynamic-editor-add-menu.component';

import { ButtonToolbarComponent } from './button-toolbar/button-toolbar.component';

import { ClickToEditComponent } from './click-to-edit/click-to-edit.component';

@NgModule({
  declarations: [
    FormFieldComponent,
    LoaderComponent,
    LoadingContainerComponent,
    ObservableLoaderComponent,
    CloseButtonComponent,
    HelpIconComponent,
    BreadcrumbsComponent,
    BaseDropdownComponent,
    DataTableComponent,
    SelectComponent,
    SelectInputComponent,
    SingleSelectComponent,
    MultiSelectComponent,
    SelectTogglerComponent,
    FallbackMultiSelectComponent,
    DataTablePaginationComponent,
    SelectOptionListComponent,
    MultiSelectOptionListComponent,
    TabbedSelectComponent,

    ArrayEditComponent,
    ColorDropdownComponent,
    IconSelectComponent,
    ListEditorComponent,
    MapEditorComponent,

    ModalComponent,
    ConfirmationModalComponent,

    StackerListComponent,
    StackerBoxComponent,

    CheckboxComponent,
    SearchInputComponent,
    AlertBoxComponent,
    ValidationSummaryComponent,

    HighlightTermPipe,

    FormSectionHeaderComponent,
    StatusIndicatorComponent,

    IconComponent,
    DropdownMenuComponent,
    DropdownMenuItemComponent,
    ListGroupComponent,
    ListGroupItemDirective,
    RowWithMenuComponent,
    DatepickerComponent,
    MaterialDialogModalComponent,
    CronEditorComponent,
    ErrorModalComponent,
    PropertyListComponent,
    DynamicEditorComponent,
    DynamicEditorListComponent,

    ButtonToolbarComponent,
    ClickToEditComponent,
    UsingDynamicEditorsForDirective,
    DynamicEditorAddMenuComponent,
  ],


  imports: [
    CommonModule,
    TooltipModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    BmiUiCollapsibleSectionModule,
    PortalModule,
    OverlayModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],

  exports: [
    FormFieldComponent,
    TooltipModule,
    MatDialogModule,
    LoaderComponent,
    LoadingContainerComponent,
    ObservableLoaderComponent,
    CloseButtonComponent,
    HelpIconComponent,
    BreadcrumbsComponent,
    BaseDropdownComponent,
    DataTableComponent,
    SelectComponent,
    SelectInputComponent,
    SingleSelectComponent,
    MultiSelectComponent,
    SelectTogglerComponent,
    FallbackMultiSelectComponent,
    SearchInputComponent,
    DataTablePaginationComponent,
    SelectOptionListComponent,
    MultiSelectOptionListComponent,
    TabbedSelectComponent,

    ArrayEditComponent,
    ColorDropdownComponent,
    IconSelectComponent,
    ListEditorComponent,
    MapEditorComponent,

    CheckboxComponent,

    ModalComponent,
    ConfirmationModalComponent,

    StackerListComponent,
    StackerBoxComponent,

    AlertBoxComponent,
    ValidationSummaryComponent,

    HighlightTermPipe,
    FormSectionHeaderComponent,

    BmiUiCollapsibleSectionModule,
    StatusIndicatorComponent,

    IconComponent,
    DropdownMenuComponent,
    DropdownMenuItemComponent,
    ListGroupComponent,
    ListGroupItemDirective,
    RowWithMenuComponent,
    DatepickerComponent,
    CronEditorComponent,
    PropertyListComponent,
    DynamicEditorComponent,
    DynamicEditorListComponent,

    ButtonToolbarComponent,
    ClickToEditComponent,
    UsingDynamicEditorsForDirective,
    DynamicEditorAddMenuComponent,
  ],

  providers: [
    {
      provide: ICON_LIBRARY_TOKEN,
      multi: true,
      useExisting: SharedUiIconLibrary
    },
    { provide: ICON_LIBRARY_TOKEN, multi: true, useExisting: BrandIconLibrary },
    {
      provide: ICON_LIBRARY_TOKEN,
      multi: true,
      useExisting: FontAwesome4IconLibrary
    }
  ]
})
export class SharedUiModule {}
