import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ui-form-section-header',
  templateUrl: './form-section-header.component.html',
  styleUrls: ['./form-section-header.component.scss']
})
export class FormSectionHeaderComponent implements OnInit {

  @Input() sectionIndex: number;
  @Input() sectionTitle: string;

  constructor() { }

  ngOnInit() {
  }

}
