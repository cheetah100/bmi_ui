import { Type, InjectionToken, StaticProvider } from '@angular/core';

import { ObjectMap, Omit } from '@bmi/utils';


export interface DynamicEditorDefinition<TConfig = any> {
  /**
   * The type ID.
   */
  type: string;

  /**
   * The user-facing name for this editor type.
   */
  name: string;

  /**
   * An icon ID to use in the "Add" menu (etc.) [optional]
   */
  icon?: string;

  /**
   * A value to influence sorting. Lower values will get sorted first in menu listings.
   */
  sortWeight?: number;

  /**
   * Editor component to use. This must be listed as an entry component to be
   * dynamically loadable.
   */
  component: Type<any>;

  /**
   * Additional options.
   *
   * This allows static configuration options to be passed along to the
   * component, intended to allow the same component to be used for different
   * types with minor variations.
   */
  options?: ObjectMap<any>;

  defaultConfig: Partial<TConfig>;
}



export interface DynamicEditorDefinitionService<
    TConfig = ObjectMap<any>,
    TDefinition extends DynamicEditorDefinition = DynamicEditorDefinition>
{
  getDefinition(config: TConfig): TDefinition;
  getAdditionalProviders?(): StaticProvider[];
  onConfigUpdated?(config: TConfig, def: TDefinition): TConfig;
  getAvailableDefinitions?(): TDefinition[];
  createConfig?(def: TDefinition): TConfig
}


export const ACTIVE_DYNAMIC_EDITOR_SERVICE = new InjectionToken<DynamicEditorDefinitionService>('Active Dynamic Editor Definition Service')


export function provideAsDynamicEditorService(type: Type<DynamicEditorDefinitionService>): StaticProvider {
  return {
    provide: ACTIVE_DYNAMIC_EDITOR_SERVICE,
    useExisting: type
  };
}
