import { Inject, Injectable, InjectionToken, StaticProvider } from '@angular/core';
import { createGroupedMapFrom } from '@bmi/utils';
import { DynamicEditorDefinition } from "./dynamic-editor-definition";

interface DynamicEditorRegistryEntry<TDef extends DynamicEditorDefinition = DynamicEditorDefinition> {
  category: string;
  definition: TDef;
}

export const DYNAMIC_EDITOR_DEFS_TOKEN = new InjectionToken('@bmi/ui dynamic editor definitions');


/**
 * [macro] Registers a dynamic editor definition within a category.
 *
 * This will create the DI Providers to register this editor def, so that it can
 * be retrieved using the generic registry service.
 *
 * You can further reduce boilerplate by creating a purpose-specific function
 * that calls this, binding the category.
 *
 * @param category The category of editor
 * @param def The editor metadata to register.
 */
export function registerDynamicEditor<TDef extends DynamicEditorDefinition>(category: string, def: TDef): StaticProvider[] {
  return [
    {
      provide: DYNAMIC_EDITOR_DEFS_TOKEN,
      multi: true,
      useValue: <DynamicEditorRegistryEntry>{
        category: category,
        definition: def,
      }
    }
  ];
}


/**
 * A registry for 'generic' dynamic editors, to help eliminate boilerplate.
 *
 * The dynamic editor system is a useful pattern, but it's proved to include a
 * cumbersome amount of boilerplate involved with registering the metadata.
 * Initially this was because the system was replacing flairs which had some
 * slight differences from an ideal generic design.
 *
 * This allows editors to be registered into a category (e.g. 'event-action')
 * and then all the definitions for that category can be retrieved. There is a
 * corresponding directive which will use this service as the source for dynamic
 * editors.
 */
@Injectable({providedIn: 'root'})
export class DynamicEditorRegistryService {
  constructor(
    @Inject(DYNAMIC_EDITOR_DEFS_TOKEN) private injectedDefinitions: DynamicEditorRegistryEntry[]
  ) {}

  private defsByCategory = createGroupedMapFrom(
    this.injectedDefinitions,
    d => d.category,
    d => d.definition
  );

  getDefinitions(domain: string): DynamicEditorDefinition[] {
    return this.defsByCategory.get(domain) ?? [];
  }

  getDefinition(category: string, type: string): DynamicEditorDefinition {
    return this.getDefinitions(category).find(d => d.type === type);
  }
}
