import { Component, Input, Injector, OnChanges, Injectable, forwardRef, Inject, Optional } from '@angular/core';
import { ComponentPortal, Portal } from '@angular/cdk/portal';
import { BehaviorSubject, combineLatest, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { distinctUntilNotEqual, combineLatestObject } from '@bmi/utils';
import { BaseControlValueAccessor } from '../utilities/base-control-value-accessor';
import { provideAsControlValueAccessor } from '../utilities/form-control-macros';
import { DynamicEditorDefinition, DynamicEditorDefinitionService, ACTIVE_DYNAMIC_EDITOR_SERVICE } from './dynamic-editor-definition';


export class DynamicEditorContext<TConfig, TDefinition extends DynamicEditorDefinition = DynamicEditorDefinition> {
  constructor(
    public readonly definition: TDefinition,
    public readonly config: Observable<TConfig>,
    public readonly setConfig: (config: TConfig) => void
  ) {}
}


@Component({
  selector: 'ui-dynamic-editor',
  template: `
    <ng-container *ngIf="collapsible; else notCollapsible">
      <ui-collapsible-section [label]="title | async" type="minimal" [initiallyOpen]="initiallyOpen">
        <ng-container *cdkPortalOutlet="portal | async"></ng-container>
      </ui-collapsible-section>
    </ng-container>
    <ng-template #notCollapsible>
      <h4 *ngIf="showTitle">{{title | async }}</h4>
      <ng-container *cdkPortalOutlet="portal | async"></ng-container>
    </ng-template>
  `,
  providers: [provideAsControlValueAccessor(forwardRef(() => DynamicEditorComponent))]
})
export class DynamicEditorComponent<TConfig = any> extends BaseControlValueAccessor<TConfig> {
  constructor(
    private injector: Injector,
    @Inject(ACTIVE_DYNAMIC_EDITOR_SERVICE) @Optional() private injectedEditorService: DynamicEditorDefinitionService<TConfig>
  ) { super(); }

  @Input() showTitle = false;
  @Input('defService') defServiceInput: DynamicEditorDefinitionService<TConfig> = undefined;
  @Input() collapsible = false;
  @Input() initiallyOpen = false;

  private config = new BehaviorSubject<TConfig>(undefined);
  private defServiceSubject = new BehaviorSubject(this.defService);


  editorDefinition = combineLatestObject({
    config: this.config,
    defService: this.defServiceSubject,
  }).pipe(
    // We only want to recreate the portal if the type actually changes. To do
    // this, whenever the current config (or editor service) changes, we
    // reconsider the right editor def, then compare. We do an equality
    // comparison here in case the service returns new objects each time.
    map(({config, defService}) =>  ({defService, def: this.getDefinition(config, defService)})),
    distinctUntilNotEqual(),
  );

  title = this.editorDefinition.pipe(
    map(({def}) => def && def.name)
  );

  portal: Observable<Portal<any>> = this.editorDefinition.pipe(
    map(({def, defService}) => this.createPortal(def, defService))
  );

  get defService() {
    return this.defServiceInput || this.injectedEditorService;
  }

  ngOnChanges() {
    this.defServiceSubject.next(this.defService);
  }

  updateComponentValue(config: TConfig) {
    this.config.next(config);
  }

  private onConfigUpdated(config: TConfig, def: DynamicEditorDefinition) {
    // We can give the definition service a chance to finalize the config after
    // it's been edited. The main purpose of this is so that the right type can
    // be patched into the config. Ideally we'd standardize on the same property
    // everywhere but many existing configs in BMI use a specific property for
    // this type, e.g. widgetType, flairType, formatterType, etc. Being able to
    // take those jobs is important, but we don't want to lean heavily on config
    // migrations to make it happen.
    const updatedConfig = this.defService.onConfigUpdated
      ? this.defService.onConfigUpdated(config, def)
      : config;

    this.onChanged(updatedConfig);
    this.config.next(updatedConfig);
  }

  private getDefinition(config: TConfig, defService: DynamicEditorDefinitionService<TConfig>): DynamicEditorDefinition {
    return defService ? defService.getDefinition(config) : undefined;
  }

  private createPortal(def: DynamicEditorDefinition, defService: DynamicEditorDefinitionService<TConfig>) {
    if (def && def.component) {
      const additionalProviders = defService.getAdditionalProviders
        ? defService.getAdditionalProviders()
        : [];

      const injector = Injector.create({
        name: 'Dynamic editor injector',
        parent: this.injector,
        providers: [
          {
            provide: DynamicEditorContext,
            useValue: new DynamicEditorContext<TConfig>(
              def,
              // TODO: we ought to be filtering this config stream to ensure
              // that if the type is changed, the new config won't get sent to
              // the wrong type of component
              this.config,
              config => this.onConfigUpdated(config, def)
            )
          },
          ...additionalProviders
        ]
      });
      return new ComponentPortal(def.component, undefined, injector);
    } else {
      return undefined;
    }
  }
}
