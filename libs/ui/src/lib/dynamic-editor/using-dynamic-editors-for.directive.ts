import { Directive, forwardRef, Input } from "@angular/core";
import sortBy from 'lodash-es/sortBy';
import { DynamicEditorDefinition, DynamicEditorDefinitionService, provideAsDynamicEditorService } from './dynamic-editor-definition';
import { DynamicEditorRegistryService } from './dynamic-editor-registry.service';


interface ConfigWithType {
  type: string;
}


@Directive({
  selector: '[bmiUsingDynamicEditorsFor]',
  providers: [
    provideAsDynamicEditorService(forwardRef(() => UsingDynamicEditorsForDirective))
  ]
})
export class UsingDynamicEditorsForDirective<TConfig extends ConfigWithType = ConfigWithType> implements DynamicEditorDefinitionService<TConfig> {
  constructor(
    private dynamicEditorRegistryService: DynamicEditorRegistryService,
  ) {}

  /**
   * The category of editors to use.
   *
   * This is intended to support many different categories of editors to
   * eliminate the repeated boilerplate of handling registrations and lookups,
   * which is all remarkably similar and boring.
   */
  @Input('bmiUsingDynamicEditorsFor') category: string;

  get defs() {
    return this.dynamicEditorRegistryService.getDefinitions(this.category);
  }

  getDefinition(config: TConfig): DynamicEditorDefinition {
    return this.dynamicEditorRegistryService.getDefinition(this.category, config?.type);
  }

  onConfigUpdated(config: TConfig, def: DynamicEditorDefinition) {
    return {
      ...config,
      type: def.type
    };
  }

  getAvailableDefinitions() {
    return sortBy(this.defs, [d => d.sortWeight ?? 0, d => d.name]);
  }

  createConfig(def: DynamicEditorDefinition) {
    return <TConfig>{
      ...def.defaultConfig || {},
      type: def.type,
    };
  }
}
