import { Component, Input, forwardRef, SimpleChanges, OnChanges, OnInit } from '@angular/core';
import { Option } from '@bmi/utils';
import { BaseControlValueAccessor } from '../utilities/base-control-value-accessor'
import { provideAsControlValueAccessor } from '../utilities/form-control-macros';

/**
 * Select component that displays the icon with each option.
 * By default, icon element will look like <span class="{{ iconId }}"></span>
 * You can format these classes - pass in a classFormat() function
 * Same for display name, with displayFormat()
 */
@Component({
  selector: 'ui-icon-select',
  styleUrls: ['./icon-select.component.scss'],
  templateUrl: './icon-select.component.html',
  providers: [
    provideAsControlValueAccessor(forwardRef(() => IconSelectComponent))
  ],
})
export class IconSelectComponent extends BaseControlValueAccessor<string> implements OnInit {

  selectedIcon: string = null;
  options: Option[] = [];
  search = '';

  @Input() icons: string[] = [];
  @Input() classFormat: (s: string) => string = s => s;
  @Input() displayFormat: (s: string) => string = s => s;

  ngOnInit() {
    this.options = this.icons.sort().map((iconId: string) => ({
      text: this.getDisplayName(iconId),
      value: iconId,
    }));
  }

  selectIcon(iconId: string) {
    this.selectedIcon = iconId;
    this.onChanged(iconId);
  }

  getClasses(iconId: string): string {
    const classes = this.classFormat(iconId);
    return this.stringOrFallback(classes, iconId);
  }

  getDisplayName(iconId: string): string {
    const name = this.displayFormat(iconId);
    return this.stringOrFallback(name, iconId);
  }

  /**
   * Ignore an incorrect Input() function output
   */
  stringOrFallback(derived: unknown, fallback: string): string {
    if (typeof derived === 'string') {
      return derived;
    }
    return fallback;
  }

  updateComponentValue(iconId: string) {
    this.selectedIcon = iconId;
  }

  newSearch(searchString: string): void {
    this.search = searchString.trim().toLowerCase();
  }

  filteredOptions() {
    if (this.search.trim()) {
      return this.options.filter(
        (opt: Option) => (opt.text.toLowerCase().indexOf(this.search) > -1)
      );
    }
    return this.options;
  }

  clearSearch() {
    this.search = '';
  }

}
