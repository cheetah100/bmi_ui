import { Component } from '@angular/core';

@Component({
  selector: 'ui-help-icon, idp-help-icon',
  template: '<i class="fa fa-question"></i>',
  styleUrls: ['./help-icon.component.scss']
})
export class HelpIconComponent {}
