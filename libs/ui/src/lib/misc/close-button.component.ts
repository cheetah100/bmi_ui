import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'ui-close-button, idp-close-button',
  template: `
    <button type="button">
      <i class="fa fa-close"></i>
    </button>
  `,
  styleUrls: ['./close-button.component.scss']
})
export class CloseButtonComponent {}
