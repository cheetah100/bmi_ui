import { Component, Input, Output, EventEmitter, HostListener, OnDestroy, HostBinding, OnInit, SimpleChanges } from '@angular/core';
import { Subscription, timer, Subject, race, merge } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { uniqueId } from '@bmi/utils';
import { SimpleCloseDialogService } from '../close-dialog.service';

@Component({
  selector: 'ui-click-to-edit',
  templateUrl: './click-to-edit.component.html',
  styleUrls: ['./click-to-edit.component.scss']
})
export class ClickToEditComponent implements OnInit, OnDestroy {
  constructor(
    private dialogService: SimpleCloseDialogService,
  ) {}

  @Input() text: string;
  @Output() isEditingChange = new EventEmitter<boolean>();
  @Output() onceOpen = new EventEmitter<void>();

  /**
   * If this input is true, the control will be forced to stay in edit mode,
   * This can be used to keep a new/invalid control editable.
   */
  @Input() forceIsEditing = false;

  private inputEventsThatShouldPreventFocusGrab = new Subject<void>();
  private hasJustOpened = false;

  private _isEditing = false;

  get isEditing() {
    return this._isEditing;
  }

  @Input() set isEditing(value: boolean) {
    this.setEditMode(value);
  }

  // Ensure the host element is focusable when we aren't editing. This
  // allows us to trap the `focus` event on the component.
  @HostBinding('attr.tabindex')
  get tabIndex() {
    return !this.isEditing ? 0 : undefined;
  }

  private dialogKey = uniqueId();
  private subscription = new Subscription();

  ngOnInit() {
    this.subscription.add(
      this.dialogService.onClose.pipe(
        filter(dialogKey => dialogKey !== this.dialogKey),
        filter(() => !this.hasJustOpened)
      ).subscribe(() => this.setEditMode(false))
    );

    this.subscription.add(this.onceOpen.subscribe(() => this.hasJustOpened = false));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  @HostListener('click', ['$event'])
  public onClick(event: MouseEvent) {
    event.stopPropagation();
    this.dialogService.triggerClose(this.dialogKey);
    if (!this.isEditing) {
      this.setEditMode(true);
    }
  }

  @HostListener('mousedown')
  public onMouseDown() {
    this.deferAction(() => this.inputEventsThatShouldPreventFocusGrab.next());
  }

  @HostListener('focus')
  public onFocusGained() {
    // We want the click to edit to pop open when it receives focus, but doing
    // this can have a bizarre consequence: the focus event will cause a
    // re-render, removing the <h3> element from the document which is probably
    // the thing the user clicked.
    //
    // A "click" is a async sequence of events, and focus happens in the mouse
    // down phase; click happens after the mouse is released. If the initial
    // target of a click is removed, then the following events such as 'click'
    // never happen, and thus do not bubble up. The onClick listener never
    // fires, and a click does not reach the document.
    //
    // This is counter-intuitive, as elements further up the tree were still
    // clicked, indirectly. The click processing is tied to the target element,
    // not the overall document.
    //
    // Here, we're really only interested in the keyboard focus, although we
    // aren't given the power to directly respond to that. Instead, we'll
    // trigger edit mode, but only after a short delay that will be cancelled if
    // the component opens, OR we get a signal from other events like mousedown
    // that imply a click might be coming.
    this.subscription.add(
      timer(50).pipe(
        takeUntil(
          merge(
            this.onceOpen,
            this.inputEventsThatShouldPreventFocusGrab
          )
        ),
      ).subscribe(() => this.setEditMode(true))
    );
  }

  private setEditMode(isEditing: boolean) {
    const targetMode = isEditing || this.forceIsEditing;
    const prev = this._isEditing;
    if (targetMode !== prev) {
      this._isEditing = targetMode;
      this.isEditingChange.emit(targetMode);
      if (targetMode) {
        this.hasJustOpened = true;
        this.deferAction(() => this.onceOpen.emit());
      }
    }
  }

  private deferAction(task: () => void, delay = 0) {
    this.subscription.add(timer(delay).subscribe(() => task()));
  }
}
