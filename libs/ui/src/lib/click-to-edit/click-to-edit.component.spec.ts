import { Component, ViewChild, ElementRef } from '@angular/core';
import { TypedFormControl, TypedFormGroup } from "../utilities/typed-form-controls";
import { Option } from '../select-controls/option';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { SharedUiModule } from '../shared-ui.module';
import { ClickToEditComponent } from './click-to-edit.component';
import { ComponentHarness } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Subject } from 'rxjs';
import { SimpleCloseDialogService } from '../close-dialog.service';

@Component({
  selector: 'ui-click-to-edit-test-host',
  template: `
    <ui-click-to-edit [(isEditing)]="isEditing" [formGroup]="form" [text]="displayText" (onceOpen)="onOpened.next()">
      <ui-form-field label="Name">
        <input #nameInput formControlName="name">
      </ui-form-field>

      <ui-form-field label="Class">
        <ui-select #classSelect formControlName="class" [options]="classOptions"></ui-select>
      </ui-form-field>
    </ui-click-to-edit>
  `
})
class TestHostComponent {
  @ViewChild(ClickToEditComponent) clickToEdit: ClickToEditComponent;
  @ViewChild('#nameInput') nameInput: ElementRef;

  form = new TypedFormGroup({
    name: new TypedFormControl<string>(),
    class: new TypedFormControl<string>(),
  });

  isEditing = false;

  onOpened = new Subject<void>();

  classOptions = [
    'Fighter',
    'Cleric',
    'Ranger',
    'Wizard',
    'Rogue'
  ].map(o => <Option>{text: o, value: o.toLowerCase()});

  get displayText() {
    const value = this.form.value;
    return `${value.name ?? 'Unnamed Hero'} (${value.class ?? 'No class'})`
  }
}

class TestHostHarness extends ComponentHarness {
  static hostSelector = 'ui-click-to-edit-test-host';

  clickToEditElement = this.locatorFor('ui-click-to-edit');
  clickToEditTitleElement = this.locatorForOptional('ui-click-to-edit > h3');
  nameInput = this.locatorForOptional('input[formControlName="name"]');
  classSelect = this.locatorForOptional('ui-select[formControlName="class"]');

  async clickAway() {
    await (await this.host()).click();
  }
}

describe('Click to edit component', () => {
  let host: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;
  let harness: TestHostHarness;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [SharedUiModule, FormsModule, ReactiveFormsModule],
      providers: [
        SimpleCloseDialogService,
      ],
      declarations: [TestHostComponent]
    });

    fixture = TestBed.createComponent(TestHostComponent);
    harness = await TestbedHarnessEnvironment.harnessForFixture(fixture, TestHostHarness);
    host = fixture.componentInstance;

    host.form.setValue({
      name: 'Silas',
      class: 'rogue'
    });

    fixture.detectChanges();
  });

  afterEach(() => fixture.destroy());

  it('should initially be closed (not editing)', () => {
    expect(host.clickToEdit.isEditing).toBe(false);
  });


  it('should be displaying the title text', async () => {
    const titleElement = await harness.clickToEditTitleElement();
    expect(titleElement).toBeTruthy();
    expect(await titleElement.text()).toEqual(host.displayText);
  });

  it('should open when the ui-click-to-edit receives focus', async () => {
    const el = await harness.clickToEditElement();
    await el.focus();
    expect(host.isEditing).toBeTruthy();
  });

  describe('When the title is clicked', () => {
    let onceOpenedNotificationCount: number;

    beforeEach(async () => {
      onceOpenedNotificationCount = 0;
      host.onOpened.subscribe(() => onceOpenedNotificationCount++);

      const titleElement = await harness.clickToEditTitleElement();
      await titleElement.click();
    });
    it('should set the isEditing state to true', async () => {
      expect(host.clickToEdit.isEditing).toBe(true);
    });

    it('should emit the isEditingChange event to enable 2-way binding', () => {
      expect(host.isEditing).toBe(true);
    });

    it('should emit the onceOpened event ', () => {
      expect(onceOpenedNotificationCount).toBe(1);
    });
  });

  describe('When open', () => {
    beforeEach(async () => {
      host.isEditing = true;
      fixture.detectChanges();

      // Wait for any timers to fire. This is needed to ensure the onOpened
      // event occurs, so that the component isn't considered 'just opened'. The
      // "when clicked" test cases above don't have this issue because awaiting
      // the click etc. handles all of this.
      await fixture.whenStable();
    });

    it('should be showing the contents (e.g the form)', async () => {
      expect(await harness.nameInput()).toBeTruthy();
      expect(await harness.classSelect()).toBeTruthy();
    });

    it('should no longer be displaying the title', async () => {
      const titleElement = await harness.clickToEditTitleElement();
      expect(titleElement).toBeFalsy();
    });

    it('should close when the dialog service triggers a close', async () => {
      TestBed.inject(SimpleCloseDialogService).triggerClose();
      expect(host.clickToEdit.isEditing).toBeFalsy();
    });

  })
});
