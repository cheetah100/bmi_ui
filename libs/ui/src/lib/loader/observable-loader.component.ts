import { Component, Input, TemplateRef, ContentChild, OnInit, OnChanges, OnDestroy, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { trigger, state, style, animate, transition, group, keyframes } from '@angular/animations';

import { Observable, BehaviorSubject, Subscription, EMPTY, Notification, NEVER, of, isObservable} from 'rxjs';
import { filter, map, materialize, pairwise, shareReplay, startWith, switchMap, withLatestFrom, distinctUntilChanged, debounceTime, catchError } from 'rxjs/operators';


export interface LoaderState<T = any> {
  mode: 'ready' | 'loading' | 'error' | 'message';
  value?: T;
  message?: string;
}


@Component({
  selector: 'ui-observable-loader',
  template: `
    <ui-loader
        *ngIf="!(isContentReady | async)"
        [state]="mode | async"
        [message]="errorMessage | async"
        [@loaderVisibility]="mode | async"
      >
    </ui-loader>
    <div class="loading-container-content" [@contentVisibility]="mode | async">
      <ng-container
        *ngIf="isContentReady | async"
        [ngTemplateOutlet]="contentTemplate"
        [ngTemplateOutletContext]="templateContext | async">
      </ng-container>
    </div>
  `,
  animations: [
    trigger('loaderVisibility', [
      state('loading, error, empty', style({'opacity': 1})),
      transition('* => void', animate('150ms ease-in-out', style({'opacity': 0}))),
    ]),
    trigger('contentVisibility', [
      state('loading', style({'opacity': 0})),
      transition('void => *, loading => ready', animate('150ms ease-in-out', style({'opacity': 1}))),
    ]),
  ],
  styleUrls: ['./observable-loader.component.scss']
})
export class ObservableLoaderComponent implements OnInit, OnChanges, OnDestroy {
  @Input() source: Observable<any>;
  @Input() state: LoaderState | Observable<LoaderState>;
  @Output() stateChanged = new EventEmitter<LoaderState>();

  @ContentChild(TemplateRef) contentTemplate: TemplateRef<{}>;

  private subscription = new Subscription();


  /**
   * A higher-order observable of things that are controlling the loader state.
   *
   * Because this component supports two input modes -- either directly
   * controlling the state, or controlling it by watching an observable,
   * we use this to keep track of what is currently driving it.
   */
  private readonly _stateSourceSubject = new BehaviorSubject<Observable<LoaderState>>(undefined);

  /**
   * The current state of the loader. This can be updated either by the `source` or the `state` inputs.
   */
  private readonly _loaderState = new BehaviorSubject<LoaderState>({mode: 'loading'});


  readonly mode = this._loaderState.pipe(
    map(s => s.mode ?? 'loading')
  );

  readonly isContentReady = this.mode.pipe(
    map(s => s === 'ready')
  );

  readonly templateContext = this._loaderState.pipe(
    map(loaderState => ({$implicit: loaderState.value}))
  );

  readonly hasError = this._loaderState.pipe(
    map(loaderState => loaderState.mode === 'error')
  );

  readonly errorMessage = this._loaderState.pipe(
    map(loaderState => loaderState.message)
  );


  ngOnInit() {
    this.subscription.add(
      this._stateSourceSubject.pipe(
        distinctUntilChanged(),
        switchMap(stateSource => (stateSource ?? NEVER).pipe(
          catchError((err) => {
            console.error('Observable Loader Component: the input observable emitted an error', err);
            return EMPTY;
          }))
        ),
      ).subscribe(s => this._loaderState.next(s))
    )

    this.subscription.add(this._loaderState.subscribe(s => this.stateChanged.emit(s)));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.source || changes.state) {
      if (this.source && this.state) {
        throw new Error('Loader Component: Both `source` and `state` inputs were set, but these are mutually exclusive.')
      } else if (this.source) {
        this._stateSourceSubject.next(getLoaderStateFromObservable(this.source, this._loaderState));
      } else if (this.state) {
        this._stateSourceSubject.next(isObservable(this.state) ? this.state : of(this.state));
      } else {
        this._stateSourceSubject.next(undefined);
      }
    }
  }
}



function getLoaderStateFromObservable<T>(obs: Observable<T>, stateObservable: Observable<LoaderState>): Observable<LoaderState<T>> {
  return obs.pipe(
    materialize(),
    withLatestFrom(stateObservable),
    map<[Notification<any>, LoaderState], LoaderState>(([event, currentState]) => {
      switch (event.kind) {
        case 'N':
          return {mode: 'ready', value: event.value};
        case 'E':
          return {mode: 'error', message: String(event.error)};
        case 'C':
          // Currently, completion will just keep the loader in the same state
          // that it was. A smarter behaviour here would be to introduce a "no
          // data" state that we can go to if we were previously loading.
          return currentState;
      }
    }),
  )
}
