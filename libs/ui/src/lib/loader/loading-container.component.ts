import { Component, Input, TemplateRef, ContentChild } from '@angular/core';
import { trigger, state, style, animate, transition, group, keyframes } from '@angular/animations';


@Component({
  selector: 'ui-loading-container, idp-loading-container',
  template: `
    <ui-loader
        *ngIf="!isContentReady"
        [hasError]="hasError"
        [errorMessage]="errorMessage"
        [text]="loadingMessage"
        [type]="type"
        [@loaderVisibility]="state"
      >
    </ui-loader>
    <div class="loading-container-content" [@contentVisibility]="state">
      <ng-container *ngIf="isContentReady" [ngTemplateOutlet]="contentTemplate"></ng-container>
    </div>
  `,
  animations: [
    trigger('loaderVisibility', [
      state('loading', style({'opacity': 1})),
      transition('* => void', animate('150ms ease-in-out', style({'opacity': 0}))),
    ]),
    trigger('contentVisibility', [
      state('loading', style({'opacity': 0})),
      transition('void => *, loading => ready', animate('150ms ease-in-out', style({'opacity': 1}))),
    ]),
  ],
  styleUrls: ['./loading-container.component.scss']
})
export class LoadingContainerComponent {
  @Input() loadingMessage: string;
  @Input() errorMessage: string;

  @Input() isLoading = false;
  @Input() hasError = false;

  @Input() type: 'spinner' | 'waves' = 'spinner';

  @ContentChild(TemplateRef) contentTemplate: TemplateRef<{}>;

  get isContentReady() {
    return this.state === 'ready';
  }

  get state(): 'loading' | 'ready' | 'error' {
    if (this.hasError) {
      return 'error';
    } else if (this.isLoading) {
      return 'loading';
    } else {
      return 'ready';
    }
  }
}
