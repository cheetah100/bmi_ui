import { ObservableLoaderComponent, LoaderState } from './observable-loader.component';
import { Component } from '@angular/core';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { SharedUiModule } from '../shared-ui.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { Observable, Subject, of } from 'rxjs';
import { before } from 'lodash-es';


@Component({
  template: `
    <ui-observable-loader [state]="state" [source]="source" (stateChanged)="onStateChanged($event)">
      <ng-template>
        <h1>Content</h1>
      </ng-template>
    </ui-observable-loader>

  `
})
class LoaderTestHostComponent {
  source: Observable<any>;
  state: LoaderState | Observable<LoaderState>;

  states: LoaderState[] = [];

  onStateChanged(state: LoaderState) {
    this.states.push(state);
  }

  get latestState() {
    return this.states[this.states.length - 1];
  }

  get currentMode() {
    return this.latestState?.mode;
  }
}


describe('Observable Loader', () => {
  let fixture: ComponentFixture<LoaderTestHostComponent>;
  let host: LoaderTestHostComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        SharedUiModule
      ],

      declarations: [
        LoaderTestHostComponent,
      ]
    });

    fixture = TestBed.createComponent(LoaderTestHostComponent);
    host = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => fixture.destroy());

  it('should initially be in the loading state', () => {
    expect(host.currentMode).toEqual('loading');
  });

  it('should throw an error if you try to set both `source` and `state` inputs', () => {
    host.source = of(null);
    host.state = of({mode: 'ready', value: 'foo'});
    expect(() => fixture.detectChanges()).toThrow();
  });


  describe('Controlling via the `source` input', () => {
    let subject: Subject<string>;

    beforeEach(() => {
      subject = new Subject<string>();
      host.source = subject;
      fixture.detectChanges();
    });

    it('should not emit anything before the source fires', () => {
      expect(host.states.length).toBe(1);
    });

    it('should move to the ready state when the source fires', () => {
      subject.next('foo');
      expect(host.states.length).toBe(2);
      expect(host.states[1].mode).toBe('ready');
      expect(host.states[1].value).toBe('foo');
    });

    it('should move to an error state if the source emits an error', () => {
      subject.error(new Error('Uh oh'));
      expect(host.states.length).toBe(2);
      expect(host.states[1].mode).toBe('error');
      expect(host.states[1].message).toContain('Uh oh');
    });

    it('should stay in the current state when the source completes', () => {
      const currentState = host.latestState;
      subject.complete();
      expect(host.latestState).toBe(currentState);
    })

    describe('If a new source is set up after an error', () => {
      let secondSubject: Subject<string>;
      beforeEach(() => {
        subject.error(new Error('Uh oh'));
        secondSubject = new Subject();
        host.source = secondSubject;
        fixture.detectChanges();
      });

      it('should go to ready once a new value is emitted', () => {
        secondSubject.next('all g');
        expect(host.currentMode).toBe('ready');
      });

      it('should reflect a new error that is thrown', () => {
        secondSubject.error(new Error('The front fell off'));
        expect(host.latestState.mode).toBe('error');
        expect(host.latestState.message).toContain('The front fell off');
      });
    })
  });

  describe('Controlling via the `state` input', () => {
    let stateSubject: Subject<LoaderState>;

    beforeEach(() => {
      host.state = stateSubject = new Subject();
      fixture.detectChanges();
    });

    it('should go into the indicated state', () => {
      stateSubject.next({mode: 'ready', value: 'foo'});
      expect(host.latestState.mode).toBe('ready');
      expect(host.latestState.value).toBe('foo');
    });

    it('should support directly providing a state object rather than observable', () => {
      host.state = {mode: 'ready', value: 'direct!'};
      fixture.detectChanges();
      expect(host.latestState.mode).toBe('ready');
      expect(host.latestState.value).toBe('direct!');
    });

    describe('If the `state` observable errors', () => {
      let prevState: LoaderState;
      beforeEach(() => {
        prevState = host.latestState;
        stateSubject.error(new Error('oops'));
      });

      it('should stay on the last state', () => {
        expect(host.latestState).toBe(prevState);
      });

      it('should work if a new observable is provided', () => {
        host.state = of({mode: 'ready', value: 'fixed'});
        fixture.detectChanges();
        expect(host.latestState.value).toBe('fixed');
      });

      it('should work if a new state value is provided', () => {
        host.state = {mode: 'ready', value: 'fixed'};
        fixture.detectChanges();
        expect(host.latestState.value).toBe('fixed');
      })
    })
  });
});
