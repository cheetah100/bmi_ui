import { Component, Input } from '@angular/core';


@Component({
  selector: 'iv-loader, ui-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent {
  @Input() text: string = null;

  @Input() messageIcon: string = undefined;
  @Input() message: string = null;

  /** @deprecated use the `state` and `message` inputs */
  @Input() errorMessage: string = null;

  /** @deprecated use the `state` and `message` inputs */
  @Input() hasError = false;


  @Input() state: 'loading' | 'error' | 'message' = undefined;

  get currentState() {
    if (this.state) {
      return this.state;
    } else if (this.hasError) {
      return 'error';
    } else {
      return 'loading';
    }
  }

  get icon() {
    return this.messageIcon || (this.currentState === 'error' ? 'cui-warning' : 'cui-info-outline');
  }

  get messageText() {
    return this.message ?? this.errorMessage;
  }

  /** @deprecated Only spinner is supported. */
  @Input() type: 'waves' | 'spinner' = 'spinner';
}
