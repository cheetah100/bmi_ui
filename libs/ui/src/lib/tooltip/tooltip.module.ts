import { ModuleWithProviders, NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TooltipDirective } from './tooltip.directive';
import { TooltipComponent } from './tooltip.component';

@NgModule({
    imports: [CommonModule],
    exports: [TooltipDirective, TooltipComponent ],
    declarations: [TooltipDirective, TooltipComponent ],
    entryComponents: [TooltipComponent],
})
export class TooltipModule {
    static forRoot(): ModuleWithProviders<TooltipModule> {
        return {
            ngModule: TooltipModule
        };
    }
}