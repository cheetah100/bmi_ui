import { Component, ElementRef, ChangeDetectionStrategy, ChangeDetectorRef, AfterViewChecked, } from '@angular/core';
@Component({
  selector: 'idp-tooltip',
  template: `
    <div class="tooltip" *ngIf="!!message">
      <div class="tooltip-arrow"> </div>
      <div class="tooltip-inner">{{message}}</div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./tooltip.component.scss'],
})
export class TooltipComponent implements AfterViewChecked {

  private recalculateSize: boolean = false;
  public _open: boolean = false;
  public get open(): boolean {
    return this._open;
  }

  private _message: string;
  get message(): string {
    return this._message;
  }
  set message(value: string) {
    if (this._message !== value) {
      this._message = value;
      this.recalculateSize = true;
      this.tooltipElement.style.visibility = 'hidden';
      this.changeDetectorRef.markForCheck();
    }
  }

  private _targetElement: any;
  get targetElement(): any {
    return this._targetElement;
  }
  set targetElement(value: any) {
    this._targetElement = value;
  }
  private tooltipSize: any;
  private tooltipOffset: any;

  constructor(
    private elementRef: ElementRef,
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  private get tooltipElement(): any {
    return this.elementRef.nativeElement;
  }

  ngAfterViewChecked() {
    if (this.message !== undefined && this.recalculateSize) {
      this.tooltipElement.style.left = 0;
      this.tooltipElement.style.top = 0;
      const tooltipRect = this.tooltipElement.getBoundingClientRect();
      this.tooltipSize = {
        width: tooltipRect.width,
        height: tooltipRect.height
      };
      this.tooltipOffset = {
        x: tooltipRect.left,
        y: tooltipRect.top
      };
      if (this.tooltipSize.width > 0) {
        this.recalculateSize = false;
        this.update();
      }
    }
  }

  private hasMessage() {
    return this.message !== undefined && this.message !== null && this.message !== '';
  }

  public show() {
    this.recalculateSize = true;
    this._open = true;
    this.update();
  }

  public hide() {
    this._open = false;
    this.update();
  }

  public update() {

    if (!this.open || this.recalculateSize || !this.hasMessage()) {
      this.tooltipElement.style.visibility = 'hidden';
      return;
    }

    const gap = 5; // in px
    const targetRect = this.targetElement.getBoundingClientRect();
    const centerX = targetRect.left + (targetRect.width / 2);
    const centerY = targetRect.top + (targetRect.height / 2);
    const viewSize = {
      width: document.documentElement.clientWidth,
      height: document.documentElement.clientHeight
    };
    const element = this.tooltipElement;
    const ttElement = element.querySelector('.tooltip');

    if (targetRect.right + gap + this.tooltipSize.width < viewSize.width &&
      centerY - this.tooltipSize.height / 2 > 0 &&
      centerY + this.tooltipSize.height / 2 < viewSize.height) {

      // right is ok
      element.style.left = targetRect.right + gap - this.tooltipOffset.x + 'px';
      element.style.top = centerY - this.tooltipSize.height / 2 - this.tooltipOffset.y + 'px';

      ttElement.className = 'tooltip right';
    } else if (targetRect.left - gap - this.tooltipSize.width > 0 &&
      centerY - this.tooltipSize.height / 2 > 0 &&
      centerY + this.tooltipSize.height / 2 < viewSize.height) {

      // left is ok
      element.style.left = targetRect.left - gap - this.tooltipSize.width - this.tooltipOffset.x + 'px';
      element.style.top = centerY - this.tooltipSize.height / 2 - this.tooltipOffset.y + 'px';

      ttElement.className = 'tooltip left';
    } else if (targetRect.top - gap - this.tooltipSize.height > 0 &&
      centerX - this.tooltipSize.width / 2 > 0 &&
      centerX + this.tooltipSize.width / 2 < viewSize.width) {

      // top is ok
      element.style.top = targetRect.top - gap - this.tooltipSize.height - this.tooltipOffset.y + 'px';
      element.style.left = centerX - this.tooltipSize.width / 2 - this.tooltipOffset.x + 'px';

      ttElement.className = 'tooltip top';
    } else if (targetRect.bottom + gap + this.tooltipSize.height < viewSize.height &&
      centerX - this.tooltipSize.width / 2 > 0 &&
      centerX + this.tooltipSize.width / 2 < viewSize.width) {

      // bottom is ok
      element.style.top = targetRect.bottom + gap - this.tooltipOffset.y + 'px';
      element.style.left = centerX - this.tooltipSize.width - this.tooltipOffset.x / 2 + 'px';

      ttElement.className = 'tooltip bottom';
    } else {
      // hide tooltip. parent element spans whole page.
      ttElement.className = 'tooltip hidden';
    }

    if (this.open) {
      element.style.visibility = 'visible';
    }
  }
}
