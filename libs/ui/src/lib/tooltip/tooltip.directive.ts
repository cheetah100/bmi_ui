import {
    Directive,
    Input,
    HostListener,
    ElementRef, ComponentFactoryResolver, ViewContainerRef, ComponentRef,
} from '@angular/core';
import { TooltipComponent } from './tooltip.component';

@Directive({
    selector: '[tooltip]',
})
export class TooltipDirective {

    private componentRef: ComponentRef<TooltipComponent>;

    @Input('tooltipTrigger') trigger: TooltipTrigger = 'hover';

    private _message: string;
    @Input('tooltip') get message() {
        return this._message;
    }
    set message(value: string) {
        this._message = value;
        if (this.componentRef) {
            this.componentRef.instance.message = value;
        }
    }

    constructor(private viewContainerRef: ViewContainerRef,
                private resolver: ComponentFactoryResolver,
                private elementRef: ElementRef) {
    }

    ngOnInit() {
        this.createComponent();
    }

    private createComponent() {
        let componentFactory = this.resolver.resolveComponentFactory(TooltipComponent);
        this.componentRef = this.viewContainerRef.createComponent(componentFactory);
        this.componentRef.instance.targetElement = this.elementRef.nativeElement;
        this.componentRef.instance.message = this.message;
    }

    show() {
        if (this.componentRef) {
            this.componentRef.instance.show();
        }
    }

    hide(): void {
        if (this.componentRef) {
            this.componentRef.instance.hide();
        }
    }

    @HostListener('mouseenter', ['$event'])
    onMouseEnter(event) {
        if (this.trigger === 'hover') {
            this.show();
        }
    }

    @HostListener('mouseleave', ['$event'])
    onMouseLeave(event) {
        if (this.trigger === 'hover') {
            this.hide();
        }
    }

    @HostListener('click', ['$event'])
    onMouseClick(event: MouseEvent) {
        if (this.trigger === 'click') {
            this.show();
        }
    }

    @HostListener('window:scroll') scrollHide(){
      this.hide();
    }

    @HostListener('document:click', ['$event'])
    onMouseClickOut(event: MouseEvent) {

        if (!this.componentRef || this.trigger !== 'click' || !this.componentRef.instance.open) return;
        let ev: Event = event || window.event;
        let elementClicked = (ev.srcElement || ev.target) as Element;
        let close: boolean = false;
        let elementChecked: Element = elementClicked;
        while(elementChecked !== this.elementRef.nativeElement) {
            elementChecked = elementChecked.parentElement;
            if (elementChecked === null) {
                break;
            }
            if (elementChecked.nodeName === 'HTML') {
                this.hide();
                break;
            }
        }
    }
}

export type TooltipTrigger = 'hover' | 'click';
