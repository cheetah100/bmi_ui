import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

import { isHexColor } from '../colors';


@Component({
  selector: 'ui-status-indicator',
  template: `
    <div
      class="ui-status-indicator ui-status-indicator--{{colorClass}}"
      [style.background-color]="hexColor">
      <ng-content></ng-content>
    </div>
  `,
  styleUrls: ['./status-indicator.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StatusIndicatorComponent {
  @Input() color: string = null;


  get colorClass() {
    return isHexColor(this.color) ? 'none' : this.color || 'none';
  }

  get hexColor() {
    return isHexColor(this.color) ? this.color : null;
  }
}
