import { Icon, IconLibrary, CssClassIconLibrary } from './icon-library';


describe('Shared UI Icon Library', () => {
  let iconLibrary: IconLibrary;
  beforeEach(() => {
    iconLibrary = new CssClassIconLibrary({
      name: 'Test Icon Library',
      libraryId: 'shared-ui',
      generateCssClass: iconName => `ui-icon ui-icon-${iconName}`
    });
  });

  describe('.get()', () => {
    it('should return a falsy value if id is null', () => expect(iconLibrary.get(null)).toBeFalsy());

    it('should return falsy if the icon does not belong in this library', () => {
      expect(iconLibrary.get('cui-cloud')).toBeFalsy();
    });

    describe('creating plausible icons if they start with our prefix', () => {
      let plausibleIcon: Icon;
      beforeEach(() => {
        plausibleIcon = iconLibrary.get('shared-ui-plausible-but-missing');
      });

      it('should create the icon', () => expect(plausibleIcon).toBeDefined());
      it('should have the right icon class', () => expect(plausibleIcon.cssClassString).toBe('ui-icon ui-icon-plausible-but-missing'));
      it('should reuse the icon object once created', () => {
        const samePlausibleIcon = iconLibrary.get('shared-ui-plausible-but-missing');
        expect(samePlausibleIcon).toBe(plausibleIcon);
      });
    });
  });
});
