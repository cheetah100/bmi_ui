import { Injectable } from '@angular/core';
import { CssClassIconLibrary } from './icon-library';


@Injectable({providedIn: 'root'})
export class BrandIconLibrary extends CssClassIconLibrary {
  constructor() {
    super({
      name: 'Custom UI Icons',
      libraryId: 'cui',
      // generateCssClass: iconName => `icon-${iconName}`
      // we're replacing all branded icons with this from fa. 
      // we'll need to find similar icons to replace all of the branded ones
      generateCssClass: iconName => 'fa fa-puzzle-piece',
    });
  }
}
