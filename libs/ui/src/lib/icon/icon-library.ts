import { InjectionToken } from '@angular/core';


// Icon Libraries
// ==============
//
// There are multiple CSS icon libraries at play, e.g. FontAwesome4 and
// other icons. Ideally we can mix and match these, so this introduces the
// idea of icon libraries as a way of enumerating known icons, and providing the
// CSS class details needed to display these icons.
//
// The idea is that IconLibrary objects can be provided via dependency injection
// and can be registered in an app using a multi provider (ICON_LIBRARY_TOKEN)
// so that a component that needs to show icons can locate the suitable
// libraries to use in the app.
//
//
// Design discussion:  two concepts here??
// ----------------------------------------
//
// Having done the first implementation of this, it feels like there are
// actually two concerns here -- the rule for creating the icon, and the list of
// icons. This "IconLibrary" concept could be split up into an IconType (e.g.
// this is how you generate fontawesome4 icons) and separate palettes of known
// icons that could be used for pickers (avoiding storing a list of icon
// metadata)


export interface Icon {
  /**
   * The "id" for this icon. This should uniquely identify the icon, including
   * some kind of prefix.
   */
  id: string;

  /** The icon name, to use in an icon picker (etc.) */
  title: string;

  /**
   * The CSS classes needed for this icon, e.g. `fa fa-spock-o`
   */
  cssClassString: string;
}


export abstract class IconLibrary {
  /**
   * The id of this icon library. This might be useful for picking libraries
   * from a list, and should probably be used for prefixing icon IDs
   */
  abstract libraryId: string;

  /**
   * A display name for this library, in case it's needed (e.g. supporting
   * different palettes of icons)
   */
  abstract name: string;

  /**
   * Look up an icon by ID
   *
   * At its simplest, this could just do a Array.find() on the Icon array.
   *
   * If the icon doesn't exist in the library, this should return null. If there
   * are multiple icon libraries in play, a search may then move onto the next
   * library.
   *
   * The icons returned by this method don't need to be explicitly listed in the
   * `icons` array. This means that an icon library could pattern match the name
   * to handle unknown icons that start with the right prefix.
   */
  abstract get(iconId: string): Icon;
}


export class CssClassIconLibrary extends IconLibrary {
  public readonly libraryId: string;
  public readonly name: string;
  private readonly generateCssClass: (iconName: string) => string;
  private iconsById = new Map<string, Icon>();
  private iconRegex: RegExp;

  constructor(options: {
    libraryId: string,
    name: string;
    generateCssClass: (iconName: string) => string
  }) {
    super();
    this.libraryId = options.libraryId;
    this.generateCssClass = options.generateCssClass;
    this.iconRegex = new RegExp(`^${this.libraryId}-(.+)$`)
  }

  get(iconId: string): Icon {
    return this.iconsById.get(iconId) || this.tryGuessIcon(iconId);
  }

  private tryGuessIcon(iconId: string): Icon {
    const match = !!iconId && iconId.match(this.iconRegex);
    if (match) {
      const icon = this.makeIcon(match[1]);
      this.iconsById.set(iconId, icon);
      return icon;
    }
    return null;
  }

  private makeIcon(iconName: string): Icon {
    return {
      id: `${this.libraryId}-${iconName}`,
      title: iconName,
      cssClassString: this.generateCssClass(iconName)
    }
  }
}


export const ICON_LIBRARY_TOKEN = new InjectionToken<IconLibrary>('bmi-shared-ui icon libraries');
