import { Component, Input, Inject, Optional } from '@angular/core';

import { Icon, IconLibrary, ICON_LIBRARY_TOKEN } from './icon-library';

@Component({
  selector: 'ui-icon',
  template: `<span [class]="cssString"></span>`,
  styles: [`
    :host { display: inline-block; }
  `]
})
export class IconComponent {
  @Input() icon: string | Icon;
  @Input() fixedWidth = false;

  iconDef: Icon = null;

  constructor(
    @Inject(ICON_LIBRARY_TOKEN) @Optional() private defaultLibraries: IconLibrary[]
  ) {}

  get cssString () {
    const iconDef = this.iconDef;
    return `${iconDef ? iconDef.cssClassString : ''} ${this.fixedWidth ? 'ui-icon-fw' : ''}`;
  }

  ngOnChanges() {
    if (!this.icon) {
      this.iconDef = null;
    } else if (typeof this.icon === 'string') {
      this.iconDef = null;
      const librariesToSearch = this.defaultLibraries || [];
      for (const library of librariesToSearch) {
        const foundIcon = library.get(this.icon);
        if (foundIcon) {
          this.iconDef = foundIcon;
          break;
        }
      }
    } else {
      this.iconDef = this.icon;
    }
  }
}
