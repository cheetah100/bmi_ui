import { Injectable } from '@angular/core';
import { CssClassIconLibrary } from './icon-library';

@Injectable({providedIn: 'root'})
export class FontAwesome4IconLibrary extends CssClassIconLibrary {
  constructor() {
    super({
      libraryId: 'fa4',
      name: 'Font Awesome 4 Icons',
      generateCssClass: iconName => `fa fa-${iconName}`
    });
  }
}