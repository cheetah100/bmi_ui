import { Component, Input, Output, EventEmitter, OnChanges, SimpleChanges, ContentChild, TemplateRef, HostBinding } from '@angular/core';


/**
 * Menu items for a dropdown
 *
 * This type intentionally overlaps with Option a bit for ease of use: you can
 * use Option[] here instead, then manually check the value in the itemClicked
 * handler.
 */
export interface DropdownMenuItem {
  text: string;
  icon?: string;
  isActive?: boolean;
  onClick?(): void;
}


/**
 * Dropdown menu
 *
 * Examples of use:
 *
 * With a list of options (like a ui-select control) :
 *
 *     <ui-dropdown-menu [options]="listOfOptions" (optionClicked)="doSomething(event)">
 *        <button>This triggers the menu</button>
 *     </ui-dropdown-menu>
 *
 *
 * With an #items template:
 *
 *     <ui-dropdown-menu>
 *       <button>Triggers the menu</button>
 *       <ng-template #items>
 *         <ui-dropdown-item icon="shared-ui-trash" (click)="deleteIt()">
 *           Delete Something
 *         </ui-dropdown-item>
 *       </ng-template>
 *     </ui-dropdown-menu>
 *
 *
 * You can also combine these two approaches; the manually defined options come
 * first.
 *
 * When creating a dropdown item, you can set [isActive]="true", which will set
 * the .active class on the ui-dropdown-item element. You can achieve the same
 * result by setting the class manually, or using a directive like
 * `routerLinkActive`.
 */
@Component({
  selector: 'ui-dropdown-menu',
  templateUrl: './dropdown-menu.component.html',
  styleUrls: ['./dropdown-menu.component.scss']
})
export class DropdownMenuComponent implements OnChanges {
  @Input() options: DropdownMenuItem[] = [];
  @Input() positionPolicy: 'left' | 'right' | 'auto' | 'noRelative' = 'auto';
  @Output() optionClicked = new EventEmitter<DropdownMenuItem>();

  @ContentChild('items') dropdownItemsTemplate: TemplateRef<any>;

  @HostBinding('class.ui-dropdown-menu--open') public isOpen: boolean = false;
  public hasAnyIcons = false;

  public templateContext: DropdownMenuTemplateContext = {
    '$implicit': this,
    dropdown: this
  };

  ngOnChanges(changes: SimpleChanges) {
    if (changes.options) {
      this.hasAnyIcons = this.options.some(item => !!item.icon);
    }
  }

  open() {
    this.isOpen = true;
  }

  close() {
    this.isOpen = false;
  }

  toggle() {
    this.isOpen = !this.isOpen;
  }

  itemSelected(option: DropdownMenuItem) {
    this.isOpen = false;
    this.optionClicked.emit(option);
    if (option && typeof option.onClick === 'function') {
      option.onClick();
    }
  }
}


interface DropdownMenuTemplateContext {
  $implicit: DropdownMenuComponent;
  dropdown: DropdownMenuComponent;
}
