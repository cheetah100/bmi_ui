import { Component, Input, Optional, HostBinding, HostListener } from '@angular/core';

import { DropdownMenuComponent } from './dropdown-menu.component';

@Component({
  selector: 'ui-dropdown-item',
  template: `
    <ui-icon *ngIf="leaveIconSpace || icon" [icon]="icon" [fixedWidth]="true"></ui-icon>
    <ng-content></ng-content>
  `,
  styleUrls: ['./dropdown-menu-item.component.scss']
})
export class DropdownMenuItemComponent {
  @Input() icon?: string;
  @HostBinding('class.active') @Input() isActive?: boolean;
  @Input() leaveIconSpace = true;

  constructor(
    @Optional() private dropdownMenu: DropdownMenuComponent
  ) {}

  @HostListener('click')
  itemClicked() {
    if (this.dropdownMenu) {
      this.dropdownMenu.close();
    }
  }
}
