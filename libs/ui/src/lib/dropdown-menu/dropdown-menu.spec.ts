import { SimpleChange } from '@angular/core';
import { DropdownMenuItem, DropdownMenuComponent } from './dropdown-menu.component';


function initializeMenu(props: Partial<DropdownMenuComponent> = {}) {
  const menu = new DropdownMenuComponent();
  Object.assign(menu, props);

  // Fake the change detection events
  menu.ngOnChanges({
    options: new SimpleChange(undefined, menu.options, true)
  });
  return menu;
}


describe('DropdownMenuComponent Controller', () => {
  let menu: DropdownMenuComponent;

  describe('With no initial values provided', () => {
    beforeEach(() => {
      menu = initializeMenu();
    });

    it('should default to an empty array of items', () => {
      expect(menu.options).toEqual([]);
    });

    it('should not be open', () => {
      expect(menu.isOpen).toEqual(false);
    });
  });

  describe('With some menu items', () => {
    let itemHandlersCalled: DropdownMenuItem[];
    let optionClickedEvents: DropdownMenuItem[]

    const items: DropdownMenuItem[] = [
      { text: 'Item 1' },
      { text: 'Item with handler', onClick() { itemHandlersCalled.push(this); } },
      { icon: 'shared-ui-cloud', text: 'Item with icon' }
    ];

    beforeEach(() => {
      itemHandlersCalled = [];
      optionClickedEvents = [];
      menu = initializeMenu({
        options: items
      });

      menu.optionClicked.subscribe(item => optionClickedEvents.push(item));
    });

    it('should have detected that some items have icons', () => {
      expect(menu.hasAnyIcons).toBeTruthy();
    });

    describe('When an item is clicked in the menu', () => {
      beforeEach(() => {
        menu.isOpen = true;
        menu.itemSelected(menu.options[1]);
      });

      it('should emit optionClicked', () => {
        expect(optionClickedEvents).toEqual([menu.options[1]]);
      });

      it('should call the handler on the item', () => {
        expect(itemHandlersCalled).toEqual([menu.options[1]]);
      });
    });

    it('should work when an item with no handler is clicked', () => {
      menu.isOpen = true;
      menu.itemSelected(menu.options[0]);
      expect(optionClickedEvents).toEqual([menu.options[0]]);
    });
  });
});
