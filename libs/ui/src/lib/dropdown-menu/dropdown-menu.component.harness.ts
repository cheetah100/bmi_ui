import { ComponentHarness } from '@angular/cdk/testing';


export class DropdownMenuItemHarness extends ComponentHarness {
  static hostSelector = 'ui-dropdown-item';

  text = () => this.host().then(host => host.text({exclude: 'ui-icon'}));

  async click() {
    const el = await this.host();
    await el.click();
  }
}


export class DropdownMenuHarness extends ComponentHarness {
  static hostSelector = 'ui-dropdown-menu';

  items = this.locatorForAll(DropdownMenuItemHarness);

  async isOpen() {
    return (await this.host()).matchesSelector('.ui-dropdown-menu--open');
  }

  async toggle() {
    const dropdownToggle = await this.locatorFor('.ui-dropdown-control')();
    return await dropdownToggle.click();
  }
}

