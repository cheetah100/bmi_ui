// Would prefer interface, but using class so that this gets into the compilation
export class Breadcrumb {
  title: string;
  uri?: string | any[];
  active?: boolean;
}