import { Component, Input } from '@angular/core';
import { Breadcrumb } from './breadcrumb';
import { Observable } from 'rxjs';

@Component({
  selector: 'ui-breadcrumbs',
  styleUrls: ['./breadcrumbs.component.scss'],
  template: `
  <ng-container *ngIf="breadcrumbs | async as currentBreadcrumbs">
    <section *ngIf="currentBreadcrumbs.length > 1" class="type-{{ breadcrumbsType }}">
      <ng-container *ngFor="let breadcrumb of currentBreadcrumbs; let i = index">
        <i *ngIf="i" class="fa fa-chevron-right"></i>
        <span *ngIf="breadcrumb.active || !breadcrumb.uri" class="color-dk-gray-2"> {{ breadcrumb.title }} </span>
        <a *ngIf="!breadcrumb.active && breadcrumb.uri" [routerLink]="breadcrumb.uri" class="color-brand-blue">
          <span> {{ breadcrumb.title }} </span>
        </a>
      </ng-container>
    </section>
  </ng-container>
  `,
})
export class BreadcrumbsComponent {
  // TODO: remove this. It only affects styling.
  // 'history' gives history breadcrumbs.
  @Input() breadcrumbsType = 'breadcrumbs';
  @Input() public breadcrumbs: Observable<Breadcrumb[]>;

}
