import { Component, EventEmitter, Inject, Input, OnChanges, OnInit, Optional, Output, SimpleChanges } from '@angular/core';
import { ACTIVE_DYNAMIC_EDITOR_SERVICE, DynamicEditorDefinition, DynamicEditorDefinitionService } from '../dynamic-editor/dynamic-editor-definition';

@Component({
  selector: 'ui-dynamic-editor-add-menu',
  templateUrl: './dynamic-editor-add-menu.component.html',
})
export class DynamicEditorAddMenuComponent<TConfig = unknown> implements OnInit, OnChanges {
  constructor(
    @Optional() @Inject(ACTIVE_DYNAMIC_EDITOR_SERVICE) private injectedDefService: DynamicEditorDefinitionService<TConfig>
  ) {}

  // tslint:disable-next-line: no-input-rename
  @Input('defService') defServiceInput: DynamicEditorDefinitionService<TConfig>;

  @Output() configCreated = new EventEmitter<TConfig>();

  preparedMenuItems: DynamicEditorDefinition[] = [];

  get defService() {
    return this.defServiceInput || this.injectedDefService;
  }


  ngOnInit() {
    this.updateMenuItems();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.updateMenuItems();
  }

  addItem(def: DynamicEditorDefinition) {
    const config = this.defService.createConfig && this.defService.createConfig(def);
    if (config) {
      this.configCreated.emit(config);
    } else {
      throw new Error('Definition Service does not implement the createConfig method');
    }
  }

  private updateMenuItems() {
    if (this.defService && this.defService.getAvailableDefinitions) {
      this.preparedMenuItems = this.defService.getAvailableDefinitions();
    } else {
      this.preparedMenuItems = [];
    }
  }
}
