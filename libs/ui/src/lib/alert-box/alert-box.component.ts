import { Component, Input } from '@angular/core';

@Component({
  selector: 'ui-alert-box, idp-alert-box',
  styleUrls: ['./alert-box.component.scss'],
  templateUrl: './alert-box.component.html'
})
export class AlertBoxComponent {
  @Input() type: string = 'error';
  @Input() title: string = null;

  /**
   * type maps to default icons per type.
   * override with a specific ui-icon if you like
   */
  @Input() icon: string = null;
  readonly iconMap: Map<string, string> = new Map([
    ['warning', 'warning'],
    ['error', 'exclamation-circle'],
    ['default', 'info-circle'],
    ['success', 'check-circle'],
  ]);

  constructor() {
  }

  get iconName(): string {
    return this.icon || this.iconMap.get(this.type) || this.iconMap.get('error');
  }
}
