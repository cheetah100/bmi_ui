import { Component, Input, OnDestroy, ContentChild, TemplateRef } from '@angular/core';
import { Subscription, TeardownLogic } from 'rxjs';


export type ButtonToolbarItem = ButtonToolbarAction | ButtonToolbarMenu | ButtonToolbarSeparator;


interface ButtonLike {
  text: string;
  tooltip?: string;
  icon?: string;
}

export interface ButtonToolbarSeparator {
  type: 'separator';
}

export interface ButtonToolbarAction extends ButtonLike {
  type: 'action';
  invoke?: (button: ButtonToolbarAction) => void | TeardownLogic;
}

export interface ButtonToolbarMenu extends ButtonLike {
  type: 'menu';
  items: ButtonToolbarItem[];
}


@Component({
  selector: 'ui-button-toolbar',
  templateUrl: './button-toolbar.component.html',
  styleUrls: ['./button-toolbar.component.scss']
})
export class ButtonToolbarComponent implements OnDestroy {
  @Input() items: ButtonToolbarItem[] = [];
  @Input() showButtonText = true;
  @ContentChild('buttonTemplate') buttonTemplate: TemplateRef<any>;

  private subscription = new Subscription();

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  invokeAction(action: ButtonToolbarAction) {
    this.subscription.add(action.invoke?.(action));
  }
}
