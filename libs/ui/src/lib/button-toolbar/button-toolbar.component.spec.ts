import { Component } from '@angular/core';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { TestElement } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';

import { Subscription } from 'rxjs';

import { ButtonToolbarComponent, ButtonToolbarItem, ButtonToolbarAction, ButtonToolbarMenu } from './button-toolbar.component';
import { ButtonToolbarHarness, ButtonToolbarItemHarness } from './button-toolbar.component.harness';
import { DropdownMenuHarness, DropdownMenuItemHarness } from '../dropdown-menu/dropdown-menu.component.harness';

import { SharedUiModule } from '../shared-ui.module';


@Component({
  template: `
    <ui-button-toolbar [items]="items"></ui-button-toolbar>
  `
})
class TestHostComponent {
  items: ButtonToolbarItem[];
}


// A helper for creating handler functions that will track when they are clicked
// and allow the subscriptions to be inspected, to make sure cleanup has happened.
class ActionInvocationTracker {
  clickedActions: {action: ButtonToolbarAction, subscription: Subscription}[] = [];

  createHandler(): (action: ButtonToolbarAction) => Subscription {
    return action => {
      const subscription = new Subscription();
      this.clickedActions.push({action, subscription});
      return subscription;
    }
  }
}


describe('Button Toolbar Component', async () => {
  let fixture: ComponentFixture<TestHostComponent>;
  let harness: ButtonToolbarHarness;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        SharedUiModule,
      ],
      declarations: [
        TestHostComponent
      ]
    })

    fixture = TestBed.createComponent(TestHostComponent);
    harness = await TestbedHarnessEnvironment.harnessForFixture(fixture, ButtonToolbarHarness);
  });


  describe('With no items', async () => {
    it('should have no items', async() => expect(await(harness.itemElements).length).toBe(0));
  });

  describe('With some buttons configured', async () => {
    let actionTracker: ActionInvocationTracker;
    let itemsConfig: ButtonToolbarItem[];

    let itemElements: TestElement[];
    let itemHarnesses: ButtonToolbarItemHarness[];

    beforeEach(async () => {
      actionTracker = new ActionInvocationTracker();
      fixture.componentInstance.items = itemsConfig = [
        {type: 'action', text: 'Foo', icon: 'cui-edit', invoke: actionTracker.createHandler()},
        {type: 'action', text: 'Edit', icon: 'cui-edit', invoke: actionTracker.createHandler()},
        {type: 'separator'},
        {type: 'action', text: undefined, icon: 'cui-info', invoke: actionTracker.createHandler()},
        {
          type: 'menu',
          text: 'Options',
          items: [
            {type: 'action', text: 'Action 1'},
            {type: 'separator'},
            {type: 'action', text: 'Action 2', invoke: actionTracker.createHandler()}
          ]
        }
      ];

      fixture.detectChanges();

      itemElements = await harness.itemElements();
      itemHarnesses = await harness.items();
    });

    it('should have the right number of items', () => expect(itemElements.length).toBe(itemsConfig.length));

    describe('When an action is clicked', async () => {

      beforeEach(async () => await itemElements[1].click());

      it('should call the "invoke" action when clicked', () => {
        expect(actionTracker.clickedActions.length).toBe(1);
      })

      it('should pass the action item to the handler verbatim', () => {
        expect(actionTracker.clickedActions[0].action).toBe(itemsConfig[1] as ButtonToolbarAction);
      });

      it('should run the cleanup logic if the toolbar component is disposed', async () => {
        fixture.destroy();
        expect(actionTracker.clickedActions[0].subscription.closed).toBeTruthy();
      });
    });

    describe('Menus', () => {
      let menuIndex: number;
      let menuConfig: ButtonToolbarMenu;
      let menuElement: TestElement;
      let menuHarness: DropdownMenuHarness;
      let itemHarness: ButtonToolbarItemHarness;

      beforeEach(async () => {
        menuIndex = itemsConfig.findIndex(c => c.type === 'menu' && c.text === 'Options');
        menuConfig = itemsConfig[menuIndex] as ButtonToolbarMenu;
        menuElement = itemElements[menuIndex];
        itemHarness = itemHarnesses[menuIndex];
        menuHarness = await itemHarness.getMenu();
      });


      it('should be a menu element', async () => expect(await itemHarness.isMenu()).toBeTruthy());
      it('should be closed initially', async () => expect(await menuHarness.isOpen()).toBeFalsy());

      describe('Opening a menu', async () => {
        beforeEach(async () => {
          await menuHarness.toggle();
          fixture.detectChanges();

        });

        it('should be open', async () => expect(await menuHarness.isOpen()).toBeTruthy());

        it('should close on a subsequent click', async () => {
          await menuHarness.toggle();
          expect(await itemHarness.isMenuOpen()).toBeFalsy();
        });

        describe('Menu items', async () => {
          let menuItems: DropdownMenuItemHarness[];
          beforeEach(async () => menuItems = await menuHarness.items());

          it('should have 2 items (because menu separators are not counted)', () =>
            expect(menuItems.length).toBe(2));

          it('should invoke the action when clicked', async () => {
            await menuItems[1].click();
            expect(actionTracker.clickedActions[0].action).toBe(menuConfig.items[2] as ButtonToolbarAction);
          });
        });

      })
    })
  });
})
