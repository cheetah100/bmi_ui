import { ComponentHarness } from '@angular/cdk/testing';

import { DropdownMenuHarness } from '../dropdown-menu/dropdown-menu.component.harness';


export class ButtonToolbarItemHarness extends ComponentHarness {
  static hostSelector = '.button-toolbar__item';

  async click() {
    return (await this.host()).click();
  }

  async isMenu() {
    const host = await this.host();
    return host.matchesSelector('.button-toolbar__item--menu')
  }

  async isMenuOpen() {
    return (await this.host()).matchesSelector('.button-toolbar__item--menu-open');
  }

  async getMenu() {
    return new DropdownMenuHarness(this.locatorFactory);
  }
}


export class ButtonToolbarHarness extends ComponentHarness {
  static hostSelector = 'ui-button-toolbar';

  itemElements = this.locatorForAll('.button-toolbar__item');
  items = this.locatorForAll(ButtonToolbarItemHarness);
}
