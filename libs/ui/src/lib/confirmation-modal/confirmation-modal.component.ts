import { Component, EventEmitter, Input, OnInit, Inject, Optional } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';
import { ModalComponent } from '../modal/modal.component';
import { Modal } from '../modal/modal';
import { SafeHtml } from '@angular/platform-browser';
import { ConfirmationResult } from './confirmation-result';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import * as marked from 'marked';


export interface ConfirmationModalOptions {
  content: string | SafeHtml;
  title?: string;
  contentFormat?: 'raw' | 'markdown',
  buttonYesText?: string;
  buttonCancelText?: string;

  /** Should a confirmation result be emitted when the modal is cancelled? */
  emitCancelEvent?: boolean;
}


export const DEFAULT_CONFIRMATION_MODAL_OPTIONS: ConfirmationModalOptions = {
  content: '',
  buttonYesText: 'Yes',
  buttonCancelText: 'Cancel',
  contentFormat: 'markdown',
  emitCancelEvent: false
};



@Component({
  template: `
    <h1 *ngIf="options.title">{{options.title}}</h1>
    <mat-dialog-content><div [innerHtml]="rawHtmlContent"></div></mat-dialog-content>
    <mat-dialog-actions align="center">
      <button
      data-cy="confirmation-modal-confirm-button"
      class="btn btn--primary"
      (click)="confirmAction()">
        {{ options.buttonYesText }}
      </button>
      <button
      *ngIf="options.buttonCancelText" 
      data-cy="confirmation-modal-cancel-button"
      class="btn btn--ghost"
      (click)="cancelModal()">
        {{ options.buttonCancelText }}
      </button>
    </mat-dialog-actions>
  `,
})
export class ConfirmationModalComponent implements OnInit {

  options: ConfirmationModalOptions = {
    ...DEFAULT_CONFIRMATION_MODAL_OPTIONS,
    ...this.injectedOptions || {}
  };

  rawHtmlContent: SafeHtml;

  constructor(
    private dialogRef: MatDialogRef<ConfirmationModalComponent, ConfirmationResult>,
    @Optional() @Inject(MAT_DIALOG_DATA) private injectedOptions: ConfirmationModalOptions
  ) {}

  ngOnInit() {
    if (this.options.contentFormat === 'markdown') {
      this.rawHtmlContent = marked(this.options.content, {
        headerIds: false,
        smartypants: true
      });
    } else {
      this.rawHtmlContent = this.options.content;
    }
  }

  cancelModal() {
    this.dialogRef.close(new ConfirmationResult('cancel'));
  }

  confirmAction() {
    this.dialogRef.close(new ConfirmationResult('yes'));
  }
}
