/**
 * The result of a confirmation dialog box.
 */
export class ConfirmationResult<T = any> {
  constructor(
    public readonly action: 'yes' | 'cancel',
    public readonly data?: T
  ) {}
}