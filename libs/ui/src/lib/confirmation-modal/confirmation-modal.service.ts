import { Injectable, Injector, ComponentFactoryResolver } from '@angular/core';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ModalService } from '../modal/modal.service';
import { SafeHtml } from '@angular/platform-browser';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import * as showdown from 'showdown';
import { ConfirmationResult } from './confirmation-result';
import { ConfirmationModalComponent, ConfirmationModalOptions } from './confirmation-modal.component';


@Injectable()
export class ConfirmationModalService {
  constructor(
    private modalService: ModalService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private matDialogService: MatDialog,
    private injector: Injector,
  ) {}

  /**
   * Shows a confirmation modal.
   *
   * By default, the content of the modal is formatted using Markdown. This can
   * be disabled by setting the contentFormat to 'raw' for raw HTML. This will
   * still be passed through the DOM Sanitizer, so to disable this, you need to
   * manually bypass the sanitizer and provide a SafeHtml value instead.
   *
   * The observable that this returns will emit a ConfirmationResult object when
   * Yes is clicked, or the modal is cancelled or closed. At the moment these
   * are the only two supported actions, but this could be expanded in future to
   * differentiate between 'no' and 'cancel'.
   *
   * In most cases, the cancel event should cause no further action to be taken,
   * so by default this will be filtered out of the result stream.
   *
   * @param options
   */
  public show<T>(options: ConfirmationModalOptions): Observable<ConfirmationResult<T>> {
    if (!options.content) {
      throw new Error('Modal content must be supplied');
    }

    const dialogRef = this.matDialogService.open<ConfirmationModalComponent, ConfirmationModalOptions, ConfirmationResult>(ConfirmationModalComponent, {
      data: options,
      maxWidth: '570px',
      width: '570px',
    });

    return dialogRef.afterClosed().pipe(
      map(result => result || new ConfirmationResult('cancel')),
      filter(result => options.emitCancelEvent || result.action !== 'cancel')
    );
  }
}
