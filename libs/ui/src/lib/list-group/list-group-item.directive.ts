import { Directive, Input, HostBinding } from '@angular/core';

@Directive({
  selector: 'ui-list-group-item, [uiListGroupItem]',
  host: {
    '[class.ui-list-group__item]': 'true'
  }
})
export class ListGroupItemDirective {
  @HostBinding('class.ui-list-group__item--active') @Input() isActive = false;
  @HostBinding('class.ui-list-group__item--actionable') @Input() isActionable = false;
}
