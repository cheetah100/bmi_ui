import { Component, Input } from '@angular/core';

@Component({
  selector: 'ui-list-group',
  template: `<ng-content></ng-content>`,
  host: {
    '[class.ui-list-group]': 'true',
  },
  styleUrls: ['./list-group.component.scss']
})
export class ListGroupComponent {
}
