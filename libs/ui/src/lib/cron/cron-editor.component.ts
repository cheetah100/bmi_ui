import { Component, Input, forwardRef, OnInit, OnDestroy } from '@angular/core';
import { Option } from '@bmi/utils';
import { BaseControlValueAccessor } from '../utilities/base-control-value-accessor';
import {
  TypedFormControl,
  TypedFormGroup
} from '../utilities/typed-form-controls';
import { provideAsControlValueAccessor } from '../utilities/form-control-macros';
import { Subscription } from 'rxjs';

/**
 * Initial requirements, not full cron options:
 * - Allow either All weekdays, or a single weekday.
 * - Allow one specific hour.
 * - Allow one specific minute.
 */
interface CronValues {
  second?: number;
  minute: number;
  hour: number;
  monthDay?: '?';
  month?: '*';
  weekDay: string;
}

@Component({
  selector: 'bmi-ui-cron-editor',
  templateUrl: './cron-editor.component.html',
  styleUrls: ['./cron-editor.component.scss'],
  providers: [
    provideAsControlValueAccessor(forwardRef(() => CronEditorComponent))
  ]
})
export class CronEditorComponent extends BaseControlValueAccessor<string>
  implements OnInit, OnDestroy {
  // advanced mode not yet implemented
  // should allow users to enter a string for any cron expression
  @Input() advanced = false;
  defaultCron = '0 0 0 ? * SUN';
  enabled = false;
  weekDayOptions: Option[] = [
    { text: 'Every day', value: '?' },
    { text: 'Sunday', value: 'SUN' },
    { text: 'Monday', value: 'MON' },
    { text: 'Tuesday', value: 'TUE' },
    { text: 'Wednesday', value: 'WED' },
    { text: 'Thursday', value: 'THU' },
    { text: 'Friday', value: 'FRI' },
    { text: 'Saturday', value: 'SAT' }
  ];

  readonly form = new TypedFormGroup<CronValues>({
    minute: new TypedFormControl<number>(0),
    hour: new TypedFormControl<number>(0),
    weekDay: new TypedFormControl<string>('SUN')
  });

  private readonly subscription = new Subscription();

  ngOnInit() {
    this.subscription.add(
      this.form.valueChanges.subscribe((value: CronValues) => {
        this.change();
      })
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  syncControls(name: string, max: number) {
    const value: number = this.form.get(name).value;
    const limited: number = this.rangeLimited(value, max);
    this.form.get(name).setValue(limited, { emitEvent: false });
  }

  dayName(): string {
    const id = this.form.value.weekDay;
    if (id === '?') {
      return 'day';
    }
    return this.weekDayOptions.find(option => option.value === id).text;
  }

  rangeLimited(value: number, max: number): number {
    return Math.min(max, Math.max(0, value));
  }

  description(): string {
    if (this.enabled) {
      const hour = this.rangeLimited(this.form.value.hour, 23);
      const minute = this.rangeLimited(this.form.value.minute, 59);
      const displayHour = hour > 12 ? hour - 12 : hour ? hour : 12;
      return `Schedule: every ${this.dayName()}, at ${displayHour}:${
        minute < 10 ? '0' : ''
      }${minute}${hour < 12 ? 'a' : 'p'}m.`;
    }
    return 'No schedule';
  }

  cronToValues(cron: string = this.defaultCron): CronValues {
    if (!cron) {
      cron = this.defaultCron;
    }
    const values = cron.split(' ');
    if (!values.length || values.length !== 6) {
      console.warn('Cron expression not recognised', cron);
      return {} as CronValues;
    }
    const [, minute, hour, , , weekDay] = values;
    return {
      minute: parseInt(minute, 10) || 0,
      hour: parseInt(hour, 10) || 0,
      weekDay: weekDay || 'SUN'
    };
  }

  valuesToCron(values: CronValues): string {
    if (!!values) {
      // month day has to be '*' when weekDay is not specified, else '?'
      const monthDay = values.weekDay === '?' ? '*' : '?';
      return `0 ${values.minute} ${values.hour} ${monthDay} * ${values.weekDay}`;
    }
    return this.defaultCron;
  }

  updateComponentValue(value: string): void {
    this.enabled = !!value;
    this.form.setValue(this.cronToValues(value), { emitEvent: false });
  }

  change() {
    if (this.enabled) {
      this.onChanged(this.valuesToCron(this.form.value));
    } else {
      this.onChanged(null);
    }
  }
}
