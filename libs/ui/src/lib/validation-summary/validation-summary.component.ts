import { FormGroup } from '@angular/forms';
import { Input, Component } from '@angular/core';
import { formValidationSummary } from '../utilities/form-validation-summary';

@Component({
  selector: 'ui-validation-summary',
  template: `
<ui-alert-box class="error" *ngIf="validationSummary.length" title="{{ title }}">
  <ul>
    <li *ngFor="let validationMessage of validationSummary">
      {{ validationMessage }}
    </li>
  </ul>
</ui-alert-box>
  `,
  styles: [`
ul {
  padding: 0;
  margin: .5rem 0;
  list-style-position: inside;
}
  ` ]
})
export class ValidationSummaryComponent {

  @Input() formGroup: FormGroup;
  @Input() nameMap: Map<string, string> = null;
  @Input() title = 'Validation error';

  get validationSummary(): string[] {
    return formValidationSummary(this.formGroup, this.nameMap);
  }

}
