import { DataTableComponent, DataTableAction } from './data-table.component';
import { DataTableComponentHarness, DataTableCellHarness } from './data-table.component.harness';
import { TableModel, TableColumn } from './table-model';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { Component, ViewChild, TemplateRef, ViewContainerRef } from '@angular/core';
import { SharedUiModule } from '../shared-ui.module';
import { SimpleTableModel } from './simple-table-model';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { PortalTableCell, ActionButtonsTableCell } from './table-cell';
import { TemplatePortal } from '@angular/cdk/portal';
import range from 'lodash-es/range';
import { TestElement } from '@angular/cdk/testing';
import { ButtonToolbarHarness } from '../button-toolbar/button-toolbar.component.harness';

@Component({
  template: `
    <ui-data-table [data]="model" noDataMessage="Nothing to see here" (tableAction)="onTableAction($event)"></ui-data-table>

    <ng-template #exampleCustomCell let-value="value">
      <b>{{value}}</b>
    </ng-template>
  `
})
class TableHostComponent {
  model: TableModel;
  emittedTableActions: DataTableAction[] = [];
  @ViewChild('exampleCustomCell') portalTemplate: TemplateRef<any>;

  constructor(
    public viewContainerRef: ViewContainerRef
  ) {}

  onTableAction(action: DataTableAction) {
    this.emittedTableActions.push(action);
  }
}

describe('Data Table Component', () => {
  let fixture: ComponentFixture<TableHostComponent>;
  let host: TableHostComponent;
  let harness: DataTableComponentHarness;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [SharedUiModule, NoopAnimationsModule],
      declarations: [TableHostComponent]
    });

    fixture = TestBed.createComponent(TableHostComponent);
    host = fixture.componentInstance;
    harness = await TestbedHarnessEnvironment.harnessForFixture(fixture, DataTableComponentHarness);
  });

  afterEach(() => fixture.destroy());

  it('should show a message if no table model is set', async () => {
    host.model = undefined;
    fixture.detectChanges();
    const noDataMessage = await harness.noDataMessage();
    expect(await noDataMessage.text()).toEqual('Nothing to see here');
  });

  describe('Basic text/numbers in table', () => {
    let model: SimpleTableModel;
    beforeEach(async () => {
      host.model = model = new SimpleTableModel([
        new TableColumn('Region'),
        new TableColumn('Type'),
        new TableColumn('Capital'),
        new TableColumn('Area (km^2)')
      ]);

      [
        ['Auvergne-Rhône-Alpes', 'Metropolitan', 'Lyon', 69711],
        ['Bourgogne-Franche-Comté', 'Metropolitan', 'Dijon', 47784	],
        ['Bretagne', 'Metropolitan', 'Rennes', 27208	],
        ['Centre-Val de Loire', 'Metropolitan', 'Orléans', 39151	],
        ['Corse', 'Metropolitan', 'Ajaccio', 8680],
      ].map(row => model.addRow(row));

      model.update();
      fixture.detectChanges();
    });

    it('should not show a no data message', async () => {
      expect(await harness.noDataMessage()).toBeFalsy();
    });

    it('should have 4 columns', async () => {
      const columns = await harness.columnHeaderElements();
      expect(columns.length).toBe(4);
    })

    it('should have the correct column headers', async () => {
      const columns = await harness.columnHeaderElements();
      const columnTitles = await Promise.all(columns.map(c => c.text()));
      expect(columnTitles).toEqual(['Region', 'Type', 'Capital', 'Area (km^2)']);
    });

    it('should sort alphabetically when you sort a text column', async () => {
      const columns = await harness.columnHeaderElements();
      await columns[2].click();
      expect(await harness.getColumnText(2)).toEqual([
        'Ajaccio',
        'Dijon',
        'Lyon',
        'Orléans',
        'Rennes'
      ]);
    });

    it('should sort in reverse if you click a column twice', async () => {
      const columns = await harness.columnHeaderElements();
      await columns[2].click();
      await columns[2].click();

      expect(await harness.getColumnText(2)).toEqual([
        'Ajaccio',
        'Dijon',
        'Lyon',
        'Orléans',
        'Rennes'
      ].reverse());
    });

    it('should have the right data in the cells', async () => {
      expect(await harness.getColumnText(2)).toEqual([
        'Lyon',
        'Dijon',
        'Rennes',
        'Orléans',
        'Ajaccio',
      ])
    })
  });

  describe('Legacy action buttons', () => {
    let model: SimpleTableModel;
    let buttonCellElement: TestElement;

    let cellHarness: DataTableCellHarness;
    let buttonToolbar: ButtonToolbarHarness;

    beforeEach(async () => {
      model = new SimpleTableModel([
        new TableColumn('Buttons'),
        new TableColumn('Text')
      ]);

      model.addRow([
        <ActionButtonsTableCell>{
          type: 'actions',
          buttons: [
            {action: 'foo', title: 'Foo', icon: 'spock-o'},
            {action: 'bar', title: 'Bar', icon: 'spock-o'}
          ]
        },
        'Row 1'
      ])
      model.update();
      host.model = model;
      fixture.detectChanges();

      buttonCellElement = await harness.getCellElement(0, 0);
      cellHarness = await harness.getCell(0, 0);
      buttonToolbar = await cellHarness.buttonToolbar();
    });

    it('should have buttons', async () => {
      expect(buttonToolbar).toBeDefined();
    });

    it('should have the right number of buttons', async () => {
      expect((await buttonToolbar.itemElements()).length).toBe(2);
    });

    it('should emit the button config when we click it', async () => {
      const items = await buttonToolbar.items();
      await items[0].click();

      expect(host.emittedTableActions.length).toBe(1);
      expect(host.emittedTableActions[0].action).toBe('foo');
      expect(host.emittedTableActions[0].row.formattedData[1]).toBe('Row 1');
    });
  });

  describe('Thinking with portals: ', () => {
    it('should support portal cell values', async () => {
      // We want to do this first so that we can grab the TemplateRef from the
      // host component that we will use for this portal.
      fixture.detectChanges();

      const tableModel = new SimpleTableModel([new TableColumn('Values')]);
      tableModel.addRow([
          <PortalTableCell>{
            type: 'portal',
            value: new TemplatePortal(host.portalTemplate, host.viewContainerRef, {value: 'foobar'})
          }
      ]);

      host.model = tableModel;
      tableModel.update();
      fixture.detectChanges();

      const cellContent = await harness.getCellElement(0, 0);
      expect(cellContent).toBeDefined();
    });
  });
});
