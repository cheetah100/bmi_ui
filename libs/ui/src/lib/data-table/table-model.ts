import { Subject, Observable } from 'rxjs';
import { ValueFormatter, Formatters, TableDisplayValue } from './value-formatter';
import { ObjectMap } from '@bmi/utils';

import { ColumnRenderer } from './column-renderer';

/**
 * The configuration for a data table column.
 *
 * This specifies configuration options for the table such as whether to sort on
 * this column, whether to merge, and the formatter that will prepare the value
 * for display or sorting.
 */
export class TableColumn {

  /**
   * A custom column renderer.
   *
   * This is independent of the value formatter and can provide Portal instances
   * to use for individual table cells, or for the column header.
   */
  renderer?: ColumnRenderer;

  constructor(
    public name: string,
    public merge: boolean = false,
    public width: string = '1fr',
    public formatter: ValueFormatter = Formatters.defaultFormatter,
    public sortDirection: number = 0,
    public rightAlign = false,
  ) { }
}

/**
 * A row in the table.
 *
 * This stores the "raw data" but also prepares a formatted version of the row
 * using the configured formatter for each column.
 *
 * Preparing the formatted data happens once, when the row is constructed.
 *
 * In addition to preparing the formatted value, this can also provide a value
 * to use when sorting the data. Obviously, this won't be used when data is
 * sorted on the backend, but the InMemoryTableController can use this value to
 * correctly sort special types of data.
 */
export class TableRow {
  public readonly formattedData: TableDisplayValue[];

  constructor(
    private readonly table: TableModel,
    public readonly rawData: any[],
    public readonly route: string | any[] = null,
    public readonly dataIds: ObjectMap<string> = {},
  ) {
    this.formattedData = this.formatRow();
  }

  private formatRow() {
    const columns = this.table.columns;
    return this.rawData.map((value, index) => columns[index].formatter.formatValue(value));
  }

  getSortValue(index: number) {
    const formatter = this.table.columns[index].formatter;
    return formatter.sortValue(this.rawData[index]);
  }

  getDataId(key: string): string {
    return this.dataIds[key] || null;
  }
}


/**
 * A group of table rows, used for implementing the merged rows feature of the
 * table widget.
 */
export class TableRowGroup {
  constructor(
    public readonly table: TableModel,
    public readonly rows: TableRow[]
  ) { }
}


export class TablePagination {
  private _currentPage = 1;

  /**
   * Should we be using pagination for this table?
   *
   * If enabled, the table component should display a pagination component and
   * the controller should look at the pagination state when preparing the
   * results.
   */
  enabled = false;

  /**
   * The total number of pages in the results.
   */
  totalPages = 1;

  /**
   * The number of rows to display per page.
   */
  rowsPerPage = 50;

  /**
   * Gets or sets the current page.
   *
   * This is in the range [1 .. totalPages].
   */
  get currentPage() {
    return this._currentPage;
  }

  set currentPage(value: number) {
    // I don't think clamping the current page is actually a good idea because
    // it requires us to know how many pages are in the results before we can
    // request a page from the table. This would work fine if we always started
    // on page 1, but would prevent a backend-paginated controller from being
    // able to request a subsequnt page first.
    this._currentPage = Math.max(1, Math.min(value, this.totalPages));
  }

  /**
   * Goes to the next page of results.
   */
  nextPage() {
    this.currentPage += 1;
  }

  /**
   * Goes to the previous page of results.
   */
  previousPage() {
    this.currentPage -= 1;
  }
}


/**
 * A view model for sharing state between a table component and the controller
 * backend.
 *
 * This class doesn't have any logic in it; rather, it represents the data to
 * display on the table, the user-input state of the filters and sorting.
 *
 * Actually performing the filtering, sorting, pagination etc. is handled by the
 * TableController, which can be responsible for sourcing the data, and
 * presenting the result.
 *
 * This separation of responsibility allows us to completely decouple the view
 * from the data source; which permits server-side pagination using a custom
 * controller implementation, without having to change the table components.
 *
 * Further, it better allows the view implementations to be swapped out.
 *
 * For simple use cases with paging/filtering/sorting performed in JS, you can
 * use the SimpleTableModel class instead. This has an integrated controller
 * which will update whenever update() is called.
 *
 * Due to the way this was originally carried over from the TableWidget, this
 * stores an array of all of the table rows (used for sorting and filtering),
 * and an array of TableRowGroups, which are used to implement the merged row
 * feature.
 *
 * The groups determine which rows are actually visible on the table, and it's
 * the responsibility of the table controller to put the rows into groups.
 *
 * The idea of "groups" in this manner might disappear anyway, in favour of just
 * storing a rowspan value for each column. This would more closely match the
 * needs of the view for use with CSS Grid, and it makes rendering the table
 * more efficient and flexible (e.g. multiple levels of groupings).
 */
export class TableModel {
  private _updateTableEvent = new Subject<void>();
  private _onChangeEvent = new Subject<void>();


  /**
   * @deprecated Use `updateRequested` event instead.
   */
  public get onTableUpdate(): Observable<void> {
    return this._updateTableEvent.asObservable();
  }

  /**
   * Triggered when the table settings have changed and a
   */
  public get updateRequested(): Observable<void> {
    return this._updateTableEvent.asObservable();
  }

  /**
   * Triggered when the table has changed and the view needs to update.
   *
   * This allows OnPush components to monitor the table model for changes and
   * trigger a change detection pass,
   */
  public get onChange(): Observable<void> {
    return this._onChangeEvent.asObservable();
  }

  /**
   * All of the rows on this table.
   */
  public rows: TableRow[] = [];

  /**
   * The currently visible table rows, in groups.
   */
  public groups: TableRowGroup[] = [];

  /**
   * Should the current table data be displayed as grouped rows?
   */
  public showAsGroupedRows = false;

  public readonly pagination = new TablePagination();

  public filters: ObjectMap<string> = {
    search: '',
  };

  constructor(
    public readonly columns: TableColumn[]
  ) { }

  /**
   * Gets the column indexes for grouping rows by
   */
  getMergeColumnIndices(): number[] {
    const mergeIndices: number[] = [];
    for (let i = 0; i < this.columns.length; i++) {
      if (this.columns[i].merge) {
        mergeIndices.push(i);
      }
    }
    return mergeIndices;
  }

  /**
   * Gets the column indexes for sorting rows by.
   */
  getSortColumnIndices(): number[] {
    const sortIndices: number[] = [];
    for (let i = 0; i < this.columns.length; i++) {
      if (this.columns[i].sortDirection) {
        sortIndices.push(i);
      }
    }
    return sortIndices;
  }

  /**
   * Adds a row to the table.
   *
   * @param row array of raw data values. These need to correspond to the
   *     table columns.
   * @param rowLink a URL or array of router commands
   * @param rowIds an object with data identifiers. Currently for table-edit-widget to obtain the related card id for card actions
   */
  addRow(
    row: TableRow | any[],
    rowLinkRoute: string | any[] = null,
    dataIds: ObjectMap<string> = {},
  ) {
    if (!row) {
      throw new Error('Must provide table row data');
    } else {
      const tableRow = row instanceof TableRow ? row : new TableRow(this, row, rowLinkRoute, dataIds);
      if (tableRow.rawData.length !== this.columns.length) {
        throw new Error('Table row length must match the number of columns');
      }
      this.rows.push(tableRow);
    }
  }

  /**
   * Removes all data from the table.
   */
  clear() {
    this.rows.splice(0);
  }

  sortByColumn(column: TableColumn, direction: number = 1) {
    this.columns.forEach(col => col.sortDirection = (col === column ? direction : 0));
  }

  /**
   * Refreshes the table groups.
   *
   * This will call the TableController's refresh method, causing it to update
   * the data in the table.
   *
   * If a custom table controller hasn't been provided, then the default
   * in-memory table controller will be used.
   */
  update() {
    this._updateTableEvent.next();
    this.triggerOnChange();
  }

  triggerOnChange() {
    this._onChangeEvent.next();
  }
}
