import { Portal } from '@angular/cdk/portal';
import { TableRow } from './table-model';
import { TableDisplayValue } from './value-formatter';


export interface ColumnRenderer {
  createHeaderPortal?(): Portal<any>;
  createCellPortal?(value: TableDisplayValue, rawValue: any): Portal<any>;

  noHeaderPadding?: boolean;
  noCellPadding?: boolean;
}
