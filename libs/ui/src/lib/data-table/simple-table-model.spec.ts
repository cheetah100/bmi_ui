import { SimpleTableModel } from './simple-table-model';
import { TableColumn, TableRow } from './table-model';
import { ValueFormatter, Formatters } from './value-formatter';

// tslint:disable:no-big-function
describe('SimpleTableModel', () => {
  let stoplights: SimpleTableModel;
  beforeEach(() => {
    stoplights = new SimpleTableModel([
      new TableColumn('Name'),
      new TableColumn('Order'),
      new TableColumn('Color')
    ]);

    stoplights.addRow(['Red', 1, 'status-red']);
    stoplights.addRow(['Orange', 4, 'status-orange']);
    stoplights.addRow(['Yellow', 2, 'status-yellow']);
    stoplights.addRow(['Green', 3, 'deep-green']);

    stoplights.update();
  });

  const testData = [
    ['v1', 'Supply Chain', 'Rob', 14000, 'commit'],
    ['v2', 'Supply Chain', 'Jon', 300000, 'commit'],
    ['v3', 'OPS', 'Arya', 21000, 'actual'],
    ['v4', 'IT', 'Sansa', 199222, 'commit'],
    ['v5', 'IT', 'Sansa', 10000, 'actual'],
    ['v6', 'OPS', 'Arya', 1000, 'commit'],
    ['v7', 'IT', 'Bran', 3000, 'actual']
  ];

  let investments: SimpleTableModel;
  beforeEach(() => {
    investments = new SimpleTableModel([
      new TableColumn('id'),
      new TableColumn('Function'),
      new TableColumn('Subfunction'),
      new TableColumn('Value', false, null, Formatters.currency()),
      new TableColumn('Phase')
    ]);

    testData.forEach(r => investments.addRow(r));
    investments.update();
  });

  function getIds(rows: TableRow[]) {
    return rows.map(r => r.rawData[0] as string);
  }

  function getGroupValues(table: SimpleTableModel = investments) {
    const groupColumnIndices = table.getMergeColumnIndices();
    return table.groups.map(g => groupColumnIndices.map(col => g.rows[0].formattedData[col]));
  }

  function totalVisibleRows(table: SimpleTableModel) {
    return table.groups.reduce((sum, g) => sum + g.rows.length, 0);
  }

  function getAllIds(table: SimpleTableModel) {
    return table.groups.reduce((ids, group) => {
      ids.push(...getIds(group.rows));
      return ids;
    }, []);
  }

  it('should emit event when update is called', () => {
    let notificationCount = 0;
    stoplights.onTableUpdate.subscribe(() => notificationCount += 1);
    stoplights.update();
    expect(notificationCount).toBe(1);
  });

  describe('Adding rows', () => {
    it('should throw an error if no row data is provided', () => {
      expect(() => {
        stoplights.addRow(null);
      }).toThrowError();
    });

    it('should throw an error if too few columns are specified', () => {
      expect(() => {
        stoplights.addRow([]);
      }).toThrowError();
    });

    it('should throw an error if too many columns are specified', () => {
      expect(() => {
        stoplights.addRow(['Pink', 66, 'hot-pink', 'OMGPONIES']);
      }).toThrowError();
    });
  });

  describe('Clearing rows', () => {
    beforeEach(() => {
      stoplights.clear();
      stoplights.update();
    });

    it('should remove the rows', () => {
      expect(stoplights.rows.length).toBe(0);
    });

    it('should leave the first group empty', () => {
      expect(stoplights.groups.length).toBe(1);
      expect(stoplights.groups[0].rows.length).toBe(0);
    });
  });

  describe('Sorting', () => {
    function getNames() {
      return stoplights.groups[0].rows.map(r => r.rawData[0] as string);
    }

    it('should have one row group', () => {
      expect(stoplights.groups.length).toBe(1);
    });

    it('should have the initial ordering', () => {
      expect(getNames()).toEqual(['Red', 'Orange', 'Yellow', 'Green']);
    });

    it('should sort by a designated column', () => {
      stoplights.sortByColumn(stoplights.columns[0]);
      stoplights.update();
      expect(getNames()).toEqual(['Green', 'Orange', 'Red', 'Yellow']);
    });

    it('should sort descending too', () => {
      stoplights.sortByColumn(stoplights.columns[0], -1);
      stoplights.update();
      expect(getNames()).toEqual(['Yellow', 'Red', 'Orange', 'Green']);
    });

    it('should sort numeric values', () => {
      stoplights.sortByColumn(stoplights.columns[1]);
      stoplights.update();
      expect(getNames()).toEqual(['Red', 'Yellow', 'Green', 'Orange']);
    });
  });

  describe('Formatting', () => {
    // This value formatter will uppercase the string values, and will sort
    // by the length of the string
    class TestFormatter extends ValueFormatter {
      formatValue(value: any): string {
        return ('' + value).toUpperCase();
      }

      sortValue(value: any): number {
        return this.formatValue(value).length;
      }
    }

    let table: SimpleTableModel;

    beforeEach(() => {
      table = new SimpleTableModel([
        new TableColumn('Board Names', false, null, new TestFormatter())
      ]);

      table.addRow(['Projects']);
      table.addRow(['Business Outcomes']);
      table.addRow(['Ops Sponsor']);
      table.update();
    });

    it('should use the formatted values in the table rows', () => {
      const values = table.groups[0].rows.map(r => r.formattedData[0]);
      expect(values).toEqual(['PROJECTS', 'BUSINESS OUTCOMES', 'OPS SPONSOR']);
    });

    it('should use the sortValue from the formatter', () => {
      table.sortByColumn(table.columns[0]);
      table.update();
      const values = table.groups[0].rows.map(r => r.formattedData[0]);
      expect(values).toEqual(['PROJECTS', 'OPS SPONSOR', 'BUSINESS OUTCOMES']);
    });
  });

  describe('Row Merging', () => {
    describe('grouping by a single column', () => {
      beforeEach(() => {
        investments.columns[1].merge = true;
        investments.update();
      });

      it('.showAsGroupedRows should be true', () => {
        expect(investments.showAsGroupedRows).toBe(true);
      })

      it('should have the correct number of groups', () => {
        expect(investments.groups.length).toBe(3);
      });

      it('should order the groups ascending by default', () => {
        const groupNames = investments.groups.map(g => g.rows[0].formattedData[1]);
        expect(groupNames).toEqual(['IT', 'OPS', 'Supply Chain']);
      });
    });

    describe('grouping by multiple columns', () => {
      beforeEach(() => {
        investments.columns[1].merge = true;
        investments.columns[2].merge = true;
        investments.update();
      });

      it('should show as groups', () => {
        expect(investments.showAsGroupedRows).toBe(true);
      });

      it('should order groups ascending, left to right', () => {
        const groupValues = getGroupValues();
        expect(groupValues).toEqual([
          ['IT', 'Bran'],
          ['IT', 'Sansa'],
          ['OPS', 'Arya'],
          ['Supply Chain', 'Jon'],
          ['Supply Chain', 'Rob']
        ]);
      });

      it('should allow the first merge column to be sorted descending', () => {
        investments.columns[1].sortDirection = -1;
        investments.update();
        expect(getGroupValues()).toEqual([
          ['Supply Chain', 'Rob'],
          ['Supply Chain', 'Jon'],
          ['OPS', 'Arya'],
          ['IT', 'Sansa'],
          ['IT', 'Bran']
        ]);
      });

      it('should support sorting by subsequent merge columns', () => {
        investments.columns[2].sortDirection = -1;
        investments.update();
        console.log(getGroupValues(investments));
        expect(getGroupValues()).toEqual([
          ['IT', 'Sansa'],
          ['Supply Chain', 'Rob'],
          ['Supply Chain', 'Jon'],
          ['IT', 'Bran'],
          ['OPS', 'Arya'],
        ]);
      });
    });

    describe('sorting by a non-grouped column', () => {
      beforeEach(() => {
        investments.columns[1].merge = true;
        investments.columns[3].sortDirection = 1;
        investments.update();
      });

      it('should not show as groups', () => {
        expect(investments.showAsGroupedRows).toBe(false);
      });

      it('should have a single group', () => {
        expect(investments.groups.length).toBe(1);
      });

      it('should be ordered by the sort column', () => {
        expect(getIds(investments.groups[0].rows)).toEqual([
          'v6', 'v7', 'v5', 'v1', 'v3', 'v4', 'v2'
        ]);
      });
    });
  });

  describe('pagination', () => {
    beforeEach(() => {
      investments.pagination.enabled = true;
      investments.pagination.rowsPerPage = 3;
      investments.update();
    });

    it('should default to the first page', () => {
      expect(investments.pagination.currentPage).toBe(1);
    });

    it('should set the total number of pages', () => {
      expect(investments.pagination.totalPages).toBe(3);
    });

    it('should limit the number of visible rows when paginating', () => {
      expect(investments.groups.length).toBe(1);
      expect(investments.groups[0].rows.length).toBe(3);
    });

    it('should show the second page', () => {
      investments.pagination.currentPage = 2;
      investments.update();
      expect(investments.groups.length).toBe(1);
      expect(getIds(investments.groups[0].rows)).toEqual([
        'v4', 'v5', 'v6'
      ]);
    });

    describe('changing pages', () => {
      it('should move to the next page', () => {
        investments.pagination.nextPage();
        expect(investments.pagination.currentPage).toBe(2);
      });

      it('should not go past the last page', () => {
        investments.pagination.nextPage();
        investments.pagination.nextPage();
        investments.pagination.nextPage();
        expect(investments.pagination.currentPage).toBe(3);
      });

      it('should not go past the first page', () => {
        investments.pagination.previousPage();
        expect(investments.pagination.currentPage).toBe(1);
      });
    });

    describe('paginating with merged rows', () => {
      beforeEach(() => {
        investments.columns[1].merge = true;
        investments.columns[2].merge = true;
        investments.update();
      });

      it('should show the correct results on the first page', () => {
        expect(totalVisibleRows(investments)).toBe(3);
        expect(getGroupValues(investments)).toEqual([
          ['IT', 'Bran'],
          ['IT', 'Sansa'],
        ]);
      });

      it('should show the correct results on the second page', () => {
        investments.pagination.currentPage = 2;
        investments.update();
        expect(totalVisibleRows(investments)).toBe(3);
        expect(getGroupValues(investments)).toEqual([
          ['OPS', 'Arya'],
          ['Supply Chain', 'Jon'],
        ]);
      });

      it('should show the correct results on the third page', () => {
        investments.pagination.currentPage = 3;
        investments.update();
        expect(totalVisibleRows(investments)).toBe(1);
        expect(getGroupValues(investments)).toEqual([
          ['Supply Chain', 'Rob'],
        ]);
      });

      describe('splitting groups across page boundaries', () => {
        beforeEach(() => {
          investments.pagination.rowsPerPage = 2;
          investments.update();
        });

        it('should have two groups on the first page, with only 2 rows', () => {
          expect(investments.groups[0].rows.length).toBe(1);
          expect(investments.groups[1].rows.length).toBe(1);
          expect(getGroupValues(investments)).toEqual([
            ['IT', 'Bran'],
            ['IT', 'Sansa']
          ]);

          expect(getAllIds(investments)).toEqual([
            'v7', 'v4'
          ]);
        });
      });
    });
  });
});
