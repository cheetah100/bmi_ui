import { Component, Input, ChangeDetectionStrategy, Output, EventEmitter, OnInit, OnChanges, OnDestroy, ChangeDetectorRef, SimpleChanges, isDevMode } from '@angular/core';
import { Portal } from '@angular/cdk/portal';

import { EMPTY, BehaviorSubject, Subscription } from 'rxjs';
import { switchMap, startWith, distinctUntilChanged, throttle, debounceTime } from 'rxjs/operators';

import { TableModel, TableColumn, TableRow } from './table-model';
import { TableCellObject, TableDisplayValue, DataTableButton, ActionButtonsTableCell, convertActionCellToButtonToolbar, PortalTableCell, TextTableCell } from './table-cell';
import { ColumnRenderer } from './column-renderer';
import { ButtonToolbarItem } from '../button-toolbar/button-toolbar.component';


export interface DataTableAction {
  action: string;
  row: TableRow;
  button: DataTableButton;
}



interface TableColumnState {
  column: TableColumn;
  columnRenderer: ColumnRenderer;
  isPortalCell: boolean;
  headerPortal: Portal<any>;
  noHeaderPadding: boolean;
  noCellPadding: boolean;
}


interface TableCellViewModel {
  row: TableRow;
  rowIndex: number;
  columnIndex: number;
  value: TableCellObject;
  isRowLink: boolean;
  isEvenRow: boolean;
  isMergeColumn: boolean;
  rowSpan: number;
  isHiddenCell: boolean;
  styleNoCellPadding: boolean;
  styleRightAlign: boolean;
  routerLink: any;
}


@Component({
  selector: 'idp-data-table, ui-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss'],
})
export class DataTableComponent implements OnInit, OnChanges, OnDestroy {
  @Input() data: TableModel = null;
  @Input() noDataMessage = 'No data to display';
  @Input() search = '';
  @Output() tableAction = new EventEmitter<DataTableAction>();

  private tableModelSubject = new BehaviorSubject<TableModel>(undefined);
  private subscription = new Subscription();

  columnStates: TableColumnState[] = [];
  cells: TableCellViewModel[];

  gridTemplateColumnsCss = '';
  autoWidthColumns = false;

  constructor(private changeDetectorRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.subscription.add(this.tableModelSubject.pipe(
      distinctUntilChanged(),
      switchMap(tableModel => (tableModel ? tableModel.onChange : EMPTY).pipe(startWith(null))),
    ).subscribe(() => {
      this.updateViewState();
      this.changeDetectorRef.markForCheck();
    }));

    this.tableModelSubject.next(this.data);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.data) {
      this.tableModelSubject.next(this.data);
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private updateViewState() {
    // If we don't have a model yet, we should do nothing.
    if (!this.data) {
      this.columnStates = [];
      this.cells = undefined;
      return;
    }

    this.gridTemplateColumnsCss = this.makeColumnsCss();
    this.autoWidthColumns = this.data && this.data.columns.every(c => !c.width);

    const columnStates = this.columnStates = this.data.columns.map(column => ({
      column: column,
      columnRenderer: column.renderer,
      noHeaderPadding: !!column.renderer && column.renderer.noHeaderPadding,
      noCellPadding: !!column.renderer && column.renderer.noCellPadding,
      headerPortal: column.renderer?.createHeaderPortal?.(),
      isPortalCell: !!column.renderer?.createCellPortal,
    }));

    const cells = this.cells = new Array<TableCellViewModel>();
    const tableModel = this.data;

    const showAsGroupedRows = tableModel.showAsGroupedRows;
    for (let groupIndex = 0; groupIndex < tableModel.groups.length; groupIndex += 1) {
      const group = tableModel.groups[groupIndex];
      const rows = group.rows;
      for (let rowIndex = 0; rowIndex < rows.length; rowIndex += 1) {
        const row = rows[rowIndex];
        const rowData = row.formattedData;
        const rowlength = rowData.length;
        const isEvenRow = showAsGroupedRows
          ? groupIndex % 2 === 0
          : rowIndex % 2 === 0;
        for (let colIndex = 0; colIndex < rowlength; colIndex += 1) {
          const columnState = columnStates[colIndex];
          const isMergeColumn = showAsGroupedRows && (columnState.column.merge ?? false);
          cells.push({
            row: row,
            rowIndex: rowIndex,
            columnIndex: colIndex,
            isEvenRow: isEvenRow,
            isRowLink: !!row.route,
            routerLink: row.route,
            isMergeColumn: isMergeColumn,
            isHiddenCell: isMergeColumn && rowIndex > 0,
            rowSpan: isMergeColumn && rowIndex === 0 ? group.rows.length : 1,
            styleNoCellPadding: columnState.noCellPadding,
            styleRightAlign: columnState.column.rightAlign,
            value: this.prepareCellValue(rowData[colIndex], row.rawData[colIndex], columnState, row)
          });
        }
      }
    }

    this.changeDetectorRef.markForCheck();
  }

  private prepareCellValue(value: TableDisplayValue, rawValue: any, column: TableColumnState, row: TableRow): TableCellObject {
    if (column.isPortalCell) {
      return <PortalTableCell>{
        type: 'portal',
        value: this.createCellRendererPortal(column, value, rawValue)
      };
    } else if (typeof value === 'string' || typeof value === 'number') {
      return <TextTableCell>{type: 'text', value: value};
    } else {
      const cellType = value.type;
      if (isDevMode() && cellType === 'percentage' || cellType === 'date') {
        // These cell types don't strictly 'need' to be migrated because they
        // are just text by another name. These are handled by the default case,
        // but we'll take this opportunity to yell at devs about using it.
        console.warn(`DataTableComponent: using a deprecated cell type. Just use type: 'text'!`, value);
        return value;
      } else if (cellType === 'actions') {
        return convertActionCellToButtonToolbar(
          value as ActionButtonsTableCell,
          button => this.emitAction(button, row)
        );
      } else {
        return value;
      }
    }
  }

  private makeColumnsCss(): string {
    if (this.data) {
      return this.data.columns.map((c, i) => {
        const width = c.width ?? 'max-content';
        if (typeof width === 'number') {
          // TODO: the inferred type here is never, because the type info is
          // wrong. Does this case ever happen? it shouldn't because that would
          // require lying to the type checker... it's possible that this value
          // is coming directly out of a widget config without validation.
          console.warn('DataTableComponent: encountered a number in the column width value. This should not happen.', c);
          return `${width}fr`;
        } else {
          return width;
        }
      }).join(' ');
    } else {
      return '';
    }
  }

  trackByCellValue(index: number, cellValue: TableCellObject) {
    return cellValue ?? index;
  }

  sortColumn(column: TableColumn) {
    // If this column is already being sorted, then toggle the direction,
    // otherwise default to ascending.
    const direction = column.sortDirection !== 0 ? column.sortDirection * -1 : 1;
    this.data.sortByColumn(column, direction);
    this.data.update();
  }

  emitAction(button, row: TableRow) {
    this.tableAction.emit({
      action: button.action,
      button,
      row
    });
  }

  private createCellRendererPortal(columnState: TableColumnState, formattedValue: TableDisplayValue, rawValue: any) {
    return columnState.columnRenderer?.createCellPortal?.(formattedValue, rawValue);
  }
}
