import { ComponentHarness, LocatorFactory } from '@angular/cdk/testing';
import { ButtonToolbarHarness } from '../button-toolbar/button-toolbar.component.harness';


export class DataTableCellHarness extends ComponentHarness {
  static hostSelector = '.body-cell';

  buttonToolbar = this.locatorForOptional(ButtonToolbarHarness);
}


export class DataTableComponentHarness extends ComponentHarness {
  static hostSelector = 'ui-data-table';

  columnHeaderElements = this.locatorForAll('.table-content > .head-cell');
  bodyCells = this.locatorForAll('.table-content > .body-cell');
  noDataMessage = this.locatorForOptional('.no-data-message');

  getCellElement(rowIndex: number, colIndex: number) {
    return this.locatorFor(this.getCellSelector(rowIndex, colIndex))();
  }

  async getCell(rowIndex: number, colIndex: number) {
    const loader = await this.locatorFactory.harnessLoaderFor(
      this.getCellSelector(rowIndex, colIndex)
    );

    // This is nasty, but behind the scenes both LocatorFactory and
    // HarnessLoader are the same object (a HarnessEnvironment) in both TestBed
    // and Protractor implementations. This might be nicer if we refactor the
    // table cell into a separate component.
    return new DataTableCellHarness((loader as any) as LocatorFactory);
  }

  getCellsForColumn(colIndex: number) {
    return this.locatorForAll(`.table-content > .body-cell[data-table-col-index="${colIndex}"]`)();
  }

  async getColumnText(colIndex: number) {
    const columnCells = await this.getCellsForColumn(colIndex);
    return await Promise.all(columnCells.map(c => c.text()));
  }


  private getCellSelector(rowIndex: number, colIndex: number) {
    return `.table-content > .body-cell[data-table-row-index="${rowIndex}"][data-table-col-index="${colIndex}"]`;
  }
}


