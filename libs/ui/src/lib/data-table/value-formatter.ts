import { formatDate } from '@bmi/utils';

import { TableCellObject, TableDisplayValue } from './table-cell';
export { TableCellObject, TableDisplayValue } from './table-cell';

export class ValueFormatter {
  formatValue(value: any): TableDisplayValue {
    if (Array.isArray(value) && value.length) {
      return JSON.stringify(value);
    } else if (value === null || value === undefined) {
      return '';
    } else if (typeof value !== 'object') {
      return String(value);
    } else {
      return value;
    }
  }

  sortValue(value: any): any {
    return value;
  }
}


export class CurrencyFormatter extends ValueFormatter {
  constructor(public rollup: boolean = false) {
    super();
  }

  formatValue(value: any) {
    if (typeof (value) === 'number') {
      let currencyValue = value;
      if (this.rollup) {
        currencyValue = Math.round(currencyValue / 1000);
      }
      return `$${currencyValue.toLocaleString()}`;
    } else {
      return super.formatValue(value);
    }
  }
}


export class TitleCaseFormatter extends ValueFormatter {
  formatValue(value: any) {
    if (typeof (value) === 'string') {
      // Capitalize the first letter of every word
      return value.replace(/\b(\w)/g, (match: string, p1: string) => p1.toUpperCase());
    } else {
      super.formatValue(value);
    }
  }
}

export class DateFormatter extends ValueFormatter {
  formatValue(value: any) {
    if (typeof (value) === 'number' || value instanceof Date) {
      return formatDate(value, 'date');
    } else {
      return super.formatValue(value);
    }
  }
}

export class MapFormatter extends ValueFormatter {
  formatValue(value: any): TableDisplayValue {
    let valueToBeFormatted:string | null = value !== null ? JSON.stringify(value) : null;
    return super.formatValue(valueToBeFormatted);
  }
}

const SECOND_UNITS =  ['sec', 'secs', 'second', 'seconds'];
const MILLI_SECOND_UNITS = ['msec', 'msecs', 'millisecond', 'milliseconds'];

export class TimestampToDateFormatter extends DateFormatter {
  constructor(private unit:string = 'sec') {
    super();

    if (![...SECOND_UNITS, ...MILLI_SECOND_UNITS].includes(unit)) {
      throw new Error(`TimestampToDateFormatter: Incorrect unit: ${unit}`)
    }
  }

  formatValue(value: number): TableDisplayValue {
    if (MILLI_SECOND_UNITS.includes(this.unit)) {
      value = value / 1000
    }
    return super.formatValue(value);
  }
}

type LinkGeneratorFunction<T> = (value: T) => string | any[];

export class LinkFormatter<T> extends ValueFormatter {
  constructor(
    private linkGenerator: LinkGeneratorFunction<T>,
    private formatter: ValueFormatter = null
  ) {
    super();
  }

  formatValue(value: T): TableCellObject {
    const link = this.linkGenerator(value);
    return {
      type: 'link',
      value: this.formatter ? this.formatter.formatValue(value) : super.formatValue(value),
      routerLink: link
    };
  }

  sortValue(value: any) {
    return this.formatter ? this.formatter.sortValue(value) : super.sortValue(value);
  }
}

export class Formatters {
  static readonly defaultFormatter = new ValueFormatter();
  static readonly titleCase = new TitleCaseFormatter();
  static readonly date = new DateFormatter();
  static readonly timestamp = new TimestampToDateFormatter('sec');
  static readonly msecTimestamp = new TimestampToDateFormatter('msec');
  static readonly map = new MapFormatter();

  static currency(rollup = false) {
    return new CurrencyFormatter(rollup);
  }

  static link<T>(linkGenerator: LinkGeneratorFunction<T>, formatter?: ValueFormatter) {
    return new LinkFormatter<T>(linkGenerator, formatter);
  }
}
