import { InMemoryTableController } from './in-memory-table-controller';
import { TableModel, TableColumn } from './table-model';


const STOPLIGHT_TEST_DATA = [
  ['Red', 1, 'status-red', false],
  ['Orange', 4, 'status-orange', false],
  ['Yellow', 2, 'status-yellow', false],
  ['Green', 3, 'deep-green', false],
  ['Blue', 4, 'status-blue', false],
  ['Indigo', 5, 'indigo', false],
  ['Violet', 6, 'violet', false],
  ['Near ultraviolet', 7, 'near-uv', false],
  ['Extreme ultraviolet', 8, 'extreme-uv', true],
  ['X-rays', 9, 'x-ray', true],
  ['Gamma Rays', 10, 'gamma-rays', true],
];

describe('InMemoryTableController', () => {
  // Most of the functionality is currently covered by the table model tests, as
  // this controller is used as the default controller if a custom one isn't
  // supplied.
  let stoplights: TableModel;
  let tableController: InMemoryTableController;

  beforeEach(() => {
    stoplights = new TableModel([
      new TableColumn('Name'),
      new TableColumn('Order'),
      new TableColumn('Color'),
      new TableColumn('Is Ionizing?')
    ]);

    tableController = new InMemoryTableController(stoplights, true);
  });

  describe('Using controller as data source', () => {
    beforeEach(() => {
      STOPLIGHT_TEST_DATA.forEach(r => tableController.addRow(r));
      tableController.refresh();
    });

    it('should add the correct number of rows to the table', () => {
      expect(stoplights.rows.length).toBe(STOPLIGHT_TEST_DATA.length);
    });

    it('should "refill" the table model after clearing the view model', () => {
      stoplights.clear();
      expect(stoplights.rows.length).toBe(0);
      tableController.refresh();
      expect(stoplights.rows.length).toBe(STOPLIGHT_TEST_DATA.length);
    });
  });

  describe('Using the viewmodel as a data source', () => {
    beforeEach(() => {
      tableController.updateTableRows = false;
      STOPLIGHT_TEST_DATA.forEach(r => stoplights.addRow(r));
      tableController.refresh();
    });

    it('should leave the existing data intact', () => {
      tableController.refresh();
      expect(stoplights.rows.length).toBe(STOPLIGHT_TEST_DATA.length);
    });
  });
});
