import { flatMap } from 'rxjs/operators';

import { TableModel, TableColumn } from './table-model';
import { InMemoryTableController } from './in-memory-table-controller';

/**
 * An implementation of TableModel with an integrated controller.
 *
 * This handles the simple case where we just want client-side data handling.
 * By default, the table controller will just work with the data present in the
 * table model, so calling addRow/clear etc. on the table model is fine for
 * adding data.
 *
 * Splitting this into a separate class help avoid a circular dependency between
 * the TableModel and TableController classes.
 */
export class SimpleTableModel extends TableModel {
  public controller: InMemoryTableController = new InMemoryTableController(this, false);

  constructor(columns: TableColumn[]) {
    super(columns);
    this.updateRequested.pipe(
      flatMap(() => this.controller.refresh())
    ).subscribe();
  }
}
