import { of, Observable } from 'rxjs';

import { TableRow, TableRowGroup, TableModel } from './table-model';
import { TableController } from './table-controller';

import { ObjectMap, sortBy } from '@bmi/utils';
import { TableDisplayValue } from './value-formatter';

import isEqual from 'lodash-es/isEqual';

/**
 * Provides basic client-side filtering, sorting and grouping for the TableModel
 *
 * This is the logic for updating the data being displayed in a table model when
 * the data is being processed client-side.
 *
 * This table controller can be used in two modes: by default, it will
 *
 * This is the default controller for the table model,
 */
export class InMemoryTableController implements TableController {
  /**
   * Table
   */
  private tableRows: TableRow[] = [];

  constructor(public tableModel: TableModel, public updateTableRows = true) { }

  /**
   * Adds a data row to this table controller.
   *
   * This table controller supports two modes of operation: by default, it will
   * just use the row data that is present in the table model (for compatibility
   * with existing code).
   *
   * The other mode, is for the table controller to be the source of truth for
   * data, and to update the model whenever refresh is called.
   *
   * @param tableRow a table row instance, or array of data that matches the
   *    table model columns.
   */
  addRow(tableRow: TableRow | any[]) {
    if (tableRow instanceof TableRow) {
      this.tableRows.push(tableRow);
    } else {
      this.tableRows.push(new TableRow(this.tableModel, tableRow));
    }
  }

  /**
   * Clears the rows stored in the table controller.
   */
  clear() {
    this.tableRows.splice(0);
  }

  getDisplayString(display: any): string {
    let value: any = display;
    if (typeof display === 'object' && display.value) {
      value = display.value;
    }
    return (value || '').toString();
  }

  filterRows(): TableRow[] {
    const searchString = this.tableModel.filters.search.trim();
    if (!searchString) {
      // No search: don't filter, avoid all work
      return this.tableRows;
    }
    const search = searchString.toLocaleLowerCase();
    return this.tableRows.filter((row: TableRow) => {
      return row.formattedData.some((display: TableDisplayValue) => {
        if (!display) {
          // No cell value: avoid some work
          return false;
        }
        const cellValue: any = this.getDisplayString(display);
        return cellValue.toLocaleLowerCase().includes(search);
      });
    });
  }

  /**
   * Synchronously updates the table model
   *
   * This implementation of the table controller is guaranteed to be synchronous
   * The observable returned will emit a single void value then complete, but
   * this is soley for compatibility with the general TableController contract.
   *
   * It is not necessary to subscribe to the result on this controller to have
   * the refresh take place.
   */
  refresh(): Observable<void> {
    const tableModel = this.tableModel;
    if (this.updateTableRows) {
      this.tableModel.clear();
      const rows = this.filterRows();
      for (let i = 0; i < rows.length; i++) {
        this.tableModel.addRow(rows[i]);
      }
    } else if (this.tableRows.length > 0) {
      console.warn(`This TableController has rows defined, but updateTableRows is false, so these won't get synced with the table model.`);
    }

    const sortColumnIndices = tableModel.getSortColumnIndices();
    const mergeIndices = tableModel.getMergeColumnIndices();

    // If we're sorting by a column that's not a merge column, then we'll split
    // the merge rows.
    let allSortColumnsAreMerge = true;
    for (let i = 0; i < sortColumnIndices.length; i++) {
      const sortColumnIndex = sortColumnIndices[i];
      if (!mergeIndices.includes(sortColumnIndex)) {
        allSortColumnsAreMerge = false;
        break;
      }
    }

    // Technically we should support sorting by multiple columns, but it was
    // complicating the API for sortBy so I dropped it for simplicity. We'll
    // just use the first sort column as the direction.
    const firstSortColumn = tableModel.columns.find(c => c.sortDirection !== 0);
    const sortDirection = firstSortColumn ? firstSortColumn.sortDirection : 1;

    tableModel.showAsGroupedRows = allSortColumnsAreMerge && mergeIndices.length > 0;
    const sortColumns = tableModel.showAsGroupedRows ? [...sortColumnIndices, ...mergeIndices] : sortColumnIndices;
    const sortedData = sortBy(tableModel.rows, row => sortColumns.map(c => row.getSortValue(c)), sortDirection);

    let groups: TableRowGroup[];
    if (tableModel.showAsGroupedRows) {
      groups = this.groupData(tableModel, sortedData, mergeIndices);
    } else {
      groups = [new TableRowGroup(tableModel, sortedData)];
    }

    tableModel.groups = this.paginateResults(groups);
    tableModel.triggerOnChange();

    return of();
  }

  removeRowByDataIds(dataIds:ObjectMap<string>) {
    // Iterate over the table rows to find a match
    const rowsToDelete: number[] = this.tableRows.reduce<number[]>( (rows, row, index) => {
      if (isEqual(row.dataIds, dataIds)) rows.push(index);
      return rows;
    }, []);
    console.log('removeRowByDataIds =>  rowsToDelete:', rowsToDelete);
    // sort descending
    rowsToDelete.sort((a,b) => b-a);
    console.log('removeRowByDataIds =>  rowsToDelete:', rowsToDelete);
    rowsToDelete.forEach(index => this.tableRows.splice(index,1));
    this.refresh();
  }

  private groupData(tableModel: TableModel, sortedRows: TableRow[], groupIndices: number[]): TableRowGroup[] {
    const groups: TableRowGroup[] = [];
    let currentGroupRows = [];

    const finishGroup = () => {
      groups.push(new TableRowGroup(tableModel, currentGroupRows));
      currentGroupRows = [];
    };

    for (let i = 0; i < sortedRows.length; i++) {
      const row = sortedRows[i];
      if (i > 0) {
        const prevRow = sortedRows[i - 1];
        for (let j = 0; j < groupIndices.length; j++) {
          const colIndex = groupIndices[j];
          if (!isEqual(row.formattedData[colIndex], prevRow.formattedData[colIndex])) {
            finishGroup();
            break;
          }
        }
      }
      currentGroupRows.push(row);
    }

    if (currentGroupRows.length > 0) {
      finishGroup();
    }
    return groups;
  }

  private paginateResults(groups: TableRowGroup[]): TableRowGroup[] {
    const totalRows = groups.reduce((sum, g) => sum + g.rows.length, 0);
    if (!this.tableModel.pagination.enabled || totalRows === 0) {
      return groups;
    }

    // In order to paginate the data, we need to split up the groups so that
    // each page still has the correct number of rows.
    const rowsPerPage = this.tableModel.pagination.rowsPerPage;
    const rowsToSkip = rowsPerPage * (this.tableModel.pagination.currentPage - 1);

    let rowsSkipped = 0;
    let rowsOnCurrentPage = 0;
    const resultGroups: TableRowGroup[] = [];
    this.tableModel.pagination.totalPages = Math.ceil(totalRows / rowsPerPage);

    for (const group of groups) {
      if (rowsSkipped + group.rows.length <= rowsToSkip) {
        // Whole group is going to be skipped. Gobble it up and move on.
        rowsSkipped += group.rows.length;
        continue;
      }

      const sliceStart = rowsToSkip - rowsSkipped;
      const sliceEnd = Math.min(group.rows.length, sliceStart + rowsPerPage - rowsOnCurrentPage);
      const slicedGroupRows = group.rows.slice(sliceStart, sliceEnd);
      resultGroups.push(new TableRowGroup(this.tableModel, slicedGroupRows));

      rowsOnCurrentPage += slicedGroupRows.length;

      if (rowsOnCurrentPage >= rowsPerPage) {
        break;
      }
    }

    return resultGroups;
  }
}
