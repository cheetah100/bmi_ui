import { Observable } from 'rxjs';

export interface TableController {
  /**
   * Trigger a refresh of the table model.
   *
   * This should look at the current table model to get the filter and sort
   * state, then it should update the rows and any other data in the table
   * as necessary.
   *
   * This can happen asynchronously, or be deferred, but it should return an
   * observable that fires when the update is finished.
   */
  refresh(): Observable<void>;
}
