import { ButtonToolbarItem } from '../button-toolbar/button-toolbar.component';
import { Portal } from '@angular/cdk/portal';

export type TableDisplayValue = string | number | TableCellObject;


export interface TableCellObject<TCellType = string, T = any> {
  type: TCellType;

  /**
   * This should be the formatted value for the table cell. If the view doesn't
   * know how to display this type of object, it should just display the value.
   *
   * This should probably be made mandatory for all table cells, but there are
   * some 'legacy' cell types which need to be fixed first. The code isn't typesafe
   * which makes this refactoring difficult.
   */
  value?: T;

  // Like, dunno eh. A smarter solution for formatted values would be to include a
  // sortValue here and drop the concept of table formatters altogether.
  order?: number;
  routerLink?: string | any[] | null;
  [key: string]: any;
}



export type ButtonToolbarTableCell = TableCellObject<'button-toolbar', ButtonToolbarItem[]>;

export type TextTableCell = TableCellObject<'text', string>;

export interface DataTableButton {
  action: string;
  title: string;
  icon: string;
  [key: string]: any;
}


/** @deprecated use the ButtonToolbarTableCell instead */
export interface ActionButtonsTableCell extends TableCellObject<'actions', never> {
  buttons: DataTableButton[];
}


export function convertActionCellToButtonToolbar(actionCell: ActionButtonsTableCell, invokeFn: (button: DataTableButton) => void): ButtonToolbarTableCell {
  // This is to replace the old style of action buttons with the Button Toolbar component.
  // This codepath should be deprecated in favour of using the {type: 'button-toolbar'} table cell directly,
  // as this allows much more control.
  return {
    type: 'button-toolbar',
    value: actionCell.buttons.map(b => <ButtonToolbarItem>{
      type: 'action',
      // The old icons predate the standardized icon library names, so map it to the shared-ui icons:
      icon: `shared-ui-${b.icon}`,
      text: null,
      tooltip: b.text,
      invoke: () => { invokeFn(b); }
    })
  };
}


export interface StatusColorTableCell extends TableCellObject<'color', never> {
  color: string;
  title?: string;
  /**
   * An icon to display inside the status "light". This doesn't support icon libraries.
   */
  icon?: string;
}

export interface LinkTableCell extends TableCellObject<'link', string> {
  routerLink: string | any[];
}


export type BulletListTableCell = TableCellObject<'list', string[]>;

export type CheckmarkBooleanTableCell = TableCellObject<'boolean', boolean>;

export type PortalTableCell = TableCellObject<'portal', Portal<any>>;

