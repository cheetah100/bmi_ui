import { Component, EventEmitter, Input, Output } from '@angular/core';

import { TableModel } from '../data-table/table-model';

@Component({
  selector: 'ui-data-table-pagination',
  templateUrl: './data-table-pagination.component.html',
  styleUrls: ['./data-table-pagination.component.scss']
})
export class DataTablePaginationComponent {
  @Input() data: TableModel;
  @Output() pageChanged = new EventEmitter<void>();

  private triggerPageChanged() {
    this.data.update();
    this.pageChanged.emit();
  }

  get currentPage() {
    return this.data.pagination.currentPage;
  }

  get totalPages() {
    return this.data.pagination.totalPages;
  }

  get hasPreviousPage() {
    return this.currentPage > 1;
  }

  get hasNextPage() {
    return this.currentPage < this.totalPages;
  }

  previousPage() {
    this.data.pagination.previousPage();
    this.triggerPageChanged();
  }

  nextPage() {
    this.data.pagination.nextPage();
    this.triggerPageChanged();
  }

  firstPage() {
    this.goToPage(1);
  }

  lastPage() {
    this.goToPage(this.data.pagination.totalPages);
  }

  goToPage(pageNumber: number) {
    this.data.pagination.currentPage = pageNumber;
    this.triggerPageChanged();
  }
}
