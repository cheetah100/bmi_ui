import { Injectable, Optional, Inject } from '@angular/core';
import { BehaviorSubject, ReplaySubject } from 'rxjs';
import { ignoreElements, materialize, map } from 'rxjs/operators';

import { BmiModalOptions, BMI_MODAL_OPTIONS, ModalResult } from './bmi-modal-options';


@Injectable()
export class ModalContext<TResult = any> {

  constructor(@Inject(BMI_MODAL_OPTIONS) modalOptions: BmiModalOptions) {
    this._modalOptions.next(modalOptions);
  }

  // Using a replay subject here so that all messages emitted by the modal will
  // get repeated on resubscription.
  private _resultSubject = new ReplaySubject<ModalResult<TResult>>();

  /**
   * The result of this modal. This should emit a single value if the modal has
   * a result, and then this observable will complete. A modal instance is
   * single-use.
   */
  public readonly result = this._resultSubject.asObservable();

  private _modalOptions = new BehaviorSubject<BmiModalOptions>(undefined);
  public readonly options = this._modalOptions.asObservable();

  get data() {
    return this._modalOptions.value.data;
  }

  /**
   * Emits an event when the dialog result has completed
   */
  public readonly hasCompleted = this.result.pipe(
    ignoreElements(),
    materialize(),
    map(notification => true)
  );


  complete(result: ModalResult<TResult>) {
    if (!this._resultSubject.isStopped) {
      this._resultSubject.next(result);
      this._resultSubject.complete();
    }
  }

  cancel(data?: TResult) {
    this.complete({action: 'cancel', data});
  }

  success(data?: TResult) {
    this.complete({action: 'yes', data});
  }

  updateOptions(options: Partial<BmiModalOptions>) {
    this._modalOptions.next({
      ...this._modalOptions.value,
      ...options,
    });
  }
}
