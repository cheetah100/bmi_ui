import { Injectable, ViewContainerRef, ComponentFactoryResolver, Type, ComponentRef, Injector } from "@angular/core";
import { MatDialog } from '@angular/material/dialog';

import { defer } from 'rxjs';

import { MaterialDialogModalComponent } from './material-dialog-modal.component';

import { BmiModalOptions, ModalResult } from './bmi-modal-options';
import { Modal } from './modal';

@Injectable()
export class ModalService {
  private viewContainerRef: ViewContainerRef = null;
  private openModals: ComponentRef<Modal>[] = [];

  constructor(
    private matDialog: MatDialog
  ) {}

  public setViewContainer(viewContainerRef: ViewContainerRef) {
    this.viewContainerRef = viewContainerRef;
  }

  closeModal(modalComponent: ComponentRef<Modal>) {
    modalComponent.destroy();

    const index = this.openModals.findIndex(m => m === modalComponent);
    if (index >= 0) {
      this.openModals.splice(index, 1);
    }
  }

  public open(options: BmiModalOptions) {
    return defer(() => {
      const modalRef = this.matDialog.open<MaterialDialogModalComponent, BmiModalOptions, ModalResult<any>>(
        MaterialDialogModalComponent, {
        data: options,

        // Style rules in  @bmi/ui/styles/material-dialog-customization handle
        // the positioning and sizing of the dialog. This gives Bootstrap-style
        // long modal scrolling -- long dialogs scroll off the bottom of the
        // screen and the entire panel slides up, rather than having a scrolled
        // region inside.
        //
        // The default Material behaviour is a fixed size box with scrolling
        // content inside. This is okay if you've got tight control over the
        // dialog content design, but is much harder to get responsive layouts
        // to work, especially with variable length content.
        //
        // A known bug with this hack is that clicks on the "blank" area
        // around the modal don't hit the backdrop, as we have to resize the
        // overlay panel to get the desired scroll behaviour.
        panelClass: [
          'bmi-matdialog-scrollable',
          `bmi-matdialog-scrollable--${options.size || 'small'}`
        ]
      });

      return modalRef.afterClosed();
    });
  }

  /**
   * Eh, probably don't use this eh. Not ready for prime time
   * @param componentClass
   * @param componentFactoryResolver
   * @param injector
   */
  public createModal<T extends Modal>(componentClass: Type<T>, componentFactoryResolver: ComponentFactoryResolver, injector?: Injector): ComponentRef<T> {
    if (!this.viewContainerRef) {
      throw new Error('ModalService: Cannot create modal, ViewContainerRef not initialized!');
    }
    const componentFactory = componentFactoryResolver.resolveComponentFactory(componentClass);
    const componentRef = this.viewContainerRef.createComponent(componentFactory, undefined, injector);
    const instance = componentRef.instance;
    if (instance.modalClosed) {
      // If the dialog provides an observable for watching for close events,
      // destroy the component once an event is received, then remove this
      // subscription when the component is destroyed.
      const subscription = instance.modalClosed.subscribe(() => {
        componentRef.destroy();
      });

      componentRef.onDestroy(() => subscription.unsubscribe());
    }

    return componentRef;
  }
}
