import { InjectionToken, Type, Injector, TemplateRef } from '@angular/core';
import { Portal, ComponentType } from '@angular/cdk/portal';
import { Observable } from 'rxjs';


export type BmiModalContent<T = any> = Portal<T> | Type<T> | TemplateRef<T>;


export interface BmiModalOptions<TData = any> {
  title: string;

  /**
   * The body content of the modal, as a Portal, or a component type that will be
   * turned into a Portal
   *
   * You can create a ComponentPortal or TemplatePortal and put it here. If you
   * create a
   */
  content: BmiModalContent;

  /**
   * The size for the dialog. This will default to small (~560px), large is
   * (~1020px)
   */
  size?: 'small' | 'large';

  /**
   * Optional action buttons for the modal. These can be overriden by the
   * modal content, so a complex dialog might want to register its own actions.
   *
   * This abstraction allows the modal implementation to keep the buttons
   * visible, and styled in a separate panel on the modal.
   *
   * For simple modals like message boxes, the confirmation actions don't depend
   * on the content, so could be set up when the modal is opened.
   */
  actions?: BmiModalActionConfig[];

  /**
   * If provided, this is the injector will be used as the parent when creating
   * an injector for the Content view. Providing your own here allows you to
   * give the content access to a particular scope within the app, but when the
   * ComponentPortal is created, this injector will get extended to give access
   * to the ModalContext and related tokens.
   */
  injector?: Injector;

  /**
   * Optional, data to be passed through so that it can be accessed by the modal
   * content.
   */
  data?: TData;
}


/**
 * The configuration for an action button on a modal.
 *
 *
 */
export interface BmiModalActionConfig {
  action: string;
  buttonText: string;
  icon?: string;
  buttonStyle?: string;
  invoke?: (action: BmiModalActionConfig) => ModalResult | Observable<ModalResult>;

  /**
   * Optional, a signal that this input should be enabled.
   *
   * If omitted, the action is always valid, otherwise it's only valid once this
   * has emitted a true value.
   */
  isEnabled?: Observable<boolean>;
}


// This should be compatible with the type used by the ConfirmationModal in
// shared-ui, but that class is not exported in the public api
export interface ModalResult<TResult = any> {
  readonly action: string;
  readonly data?: TResult;
}


export const BMI_MODAL_OPTIONS = new InjectionToken<BmiModalOptions>('BMI Modal Options');

