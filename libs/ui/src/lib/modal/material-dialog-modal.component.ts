import { Component, Inject, OnInit, Type, TemplateRef, ViewContainerRef, Injector } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Portal, ComponentPortal, TemplatePortal } from '@angular/cdk/portal';
import { isObservable, of } from 'rxjs';
import { map, distinctUntilChanged, take } from 'rxjs/operators';
import pick from 'lodash-es/pick';
import { distinctUntilNotEqual } from '@bmi/utils';
import { BmiModalOptions, BMI_MODAL_OPTIONS, ModalResult, BmiModalActionConfig, BmiModalContent } from './bmi-modal-options';
import { ModalContext } from './modal-context';


@Component({
  selector: 'ui-mat-dialog-modal-implementation',
  templateUrl: './material-dialog-modal.component.html',
  styleUrls: ['./material-dialog-modal.component.scss'],

  providers: [
    ModalContext,

    // Re-provide the dialog options using a different token so that
    // ModalContext doesn't have a dependency on Material
    {provide: BMI_MODAL_OPTIONS, useExisting: MAT_DIALOG_DATA}
  ]
})
export class MaterialDialogModalComponent implements OnInit {

  constructor(
    public modalContext: ModalContext,
    private matDialogRef: MatDialogRef<MaterialDialogModalComponent, ModalResult<any>>,
    private viewContainerRef: ViewContainerRef,
    private injector: Injector,
  ) {}

  readonly options = this.modalContext.options;
  readonly title = this.options.pipe(map(opts => opts.title), distinctUntilChanged());
  readonly portal = this.options.pipe(
    map(opts => pick(opts, 'content', 'injector')),
    distinctUntilNotEqual(),
    map(({content, injector}) => this.makePortal(content, injector))
  );

  readonly actions = this.options.pipe(map(opts => opts.actions || []), distinctUntilNotEqual());
  readonly hasActions = this.actions.pipe(map(actions => actions.length > 0));


  ngOnInit() {
    this.modalContext.result.subscribe(result => this.matDialogRef.close(result));
  }

  triggerAction(action: BmiModalActionConfig) {
    // An action can have an invoke function. If this is present then we'll use
    // this to drive the outcome.
    if (action.invoke) {
      // Invoking the action might result in an observable, or the result might
      // be immediate.
      const invokeResult = action.invoke(action);
      const resultAsObservable = isObservable(invokeResult) ? invokeResult : of(invokeResult);

      // Take the first emitted value as the result. If it produces nothing,
      // then the dialog won't close. This is intentional, as the handler may
      // have had a side-effect.
      resultAsObservable.pipe(take(1)).subscribe(result => {
        if (result) {
          this.modalContext.complete(result);
        }
      });
    } else {
      this.modalContext.complete({action: action.action});
    }
  }

  closeModal() {
    this.modalContext.cancel();
  }

  private makePortal(content: BmiModalContent, injector?: Injector) {
    if (content instanceof Portal) {
      return content;
    } else if (content instanceof TemplateRef) {
      return new TemplatePortal(content, this.viewContainerRef, {
        modalContext: this.modalContext,
        $implicit: this.modalContext
      });
    } else {
      return new ComponentPortal(content, undefined, this.makePortalInjector(injector));
    }
  }

  private makePortalInjector(parentInjector?: Injector) {
    return Injector.create({
      name: 'BMI ModalContext Injector',
      parent: parentInjector || this.injector,
      providers: [
        {provide: ModalContext, useValue: this.modalContext}
      ]
    });
  }
}
