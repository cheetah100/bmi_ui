import {
  Component,
  HostListener,
  Input,
  Output,
  EventEmitter,
  ChangeDetectorRef,
  OnDestroy
} from '@angular/core';


/** @deprecated Use the new modal system instead! */
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ui-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnDestroy {
  @Input()
  styles: {[key: string]: any} = {};

  open = false;
  private closeAction = (() => {});

  @Output() modalScrollTop = new EventEmitter();
  @Output() modalClosed = new EventEmitter();
  constructor(
    private changeDetectorRef: ChangeDetectorRef
  ) {}

  ngOnDestroy() {
    // TODO: should we trigger the close logic here instead?
    this.setDialogVisibilty(false);
  }

  @HostListener('document:keypress', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    if (!this.open) {
      return;
    }
    if (event.keyCode === 27) {
      this.close();
    }
  }

  public setCloseFunction(func: () => void) {
    this.closeAction = func;
  }

  public close() {
    this.open = false;
    this.setDialogVisibilty(false);
    this.modalClosed.emit();
    this.closeAction();
  }

  public show() {
    this.setDialogVisibilty(true);
    window.setTimeout(() => {
      this.open = true;
      this.changeDetectorRef.markForCheck();
    }, 10);
  }

  private setDialogVisibilty(isVisible) {
    // TODO: this should be managed by a dialog service. Modifying the body
    // element style directly from this component is gross.
    document.getElementsByTagName('body')[0].style.overflow = isVisible ? 'hidden' : 'auto';
  }

  public scrollModal(evt) {
    this.modalScrollTop.emit((evt.srcElement || evt.target).scrollTop);
  }

  getModalStyles() {
    return this.styles || {};
  }

}
