import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'highlightTerm',
  pure: true
})
export class HighlightTermPipe implements PipeTransform {
  transform(value: string, terms: string | string[]): string {
    value = value ? value.toString() : '';
    if (!Array.isArray(terms)) {
      terms = [terms];
    }
    terms = terms.map(t => (t || '').trim());
    if (terms.filter(t => t).length === 0) {
      return value;
    }
    if (!terms || !terms.length || !value) {
      return value;
    }
    const hits = this.hitsArray(value, terms);
    return this.placeMarks(value, hits);
  }

  placeMarks(value, hits) {
    let result = '';
    for (let n = 0, nl = value.length; n < nl; n++) {
      let change = 0;
      if (n === 0) {
        change = hits[n];
      } else {
        change = hits[n] - hits[n - 1];
      }
      for (let c = 0, cl = Math.abs(change); c < cl; c++) {
        result += '<' + (change > 0 ? '' : '/') + 'mark>';
      }
      result += value[n];
      if (n === nl - 1) {
        for (let c = 0, cl = hits[n]; c < cl; c++) {
          result += '</mark>';
        }
      }
    }
    return result;
  }

  hitsArray(value, terms) {
    const markers = Array(value.length).fill(0);
    terms
      .filter(term => !!term)
      .forEach(term => {
        let exists = false;
        let start = 0;
        do {
          exists = false;
          const loc = value.toLowerCase().indexOf(term.toLowerCase(), start);
          if (loc > -1) {
            exists = true;
            for (let n = loc, nl = loc + term.length; n < nl; n++) {
              markers[n] += 1;
            }
            start = Math.min(loc + 1, value.length);
          }
        } while (exists);
      });
    return markers;
  }

}
