import { TestBed } from '@angular/core/testing';
import { HighlightTermPipe } from './highlight-term.pipe';

describe('[HighlightTermPipe]', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ HighlightTermPipe ]
    });
  });

  it('should create a hits array for counting number of hits per string character', () => {
    const pipe = new HighlightTermPipe();
    expect(pipe.hitsArray('Lethargic squirrel', ['letharg', 'argi'])).toEqual([1,1,1,1,2,2,2,1,0,0,0,0,0,0,0,0,0,0]);
  });

  it('should place mark containers correctly for a single matching search', () => {
    const pipe = new HighlightTermPipe();
    expect(pipe.transform('Lethargic squirrel charger', 'harg')).toBe('Let<mark>harg</mark>ic squirrel c<mark>harg</mark>er', 'Output does not match');
  });

  it('should place mark containers correctly for multiple search terms', () => {
    const pipe = new HighlightTermPipe();
    expect(pipe.transform('Lethargic squirrel charger', ['harg', 'quir'])).toBe('Let<mark>harg</mark>ic s<mark>quir</mark>rel c<mark>harg</mark>er', 'Output does not match');
  });

  it('should place mark containers correctly for overlapping search terms', () => {
    const pipe = new HighlightTermPipe();
    expect(pipe.transform('Lethargic squirrel charger', ['letharg', 'argi'])).toBe('<mark>Leth<mark>arg</mark>i</mark>c squirrel charger', 'Output does not match');
  });

  it('should not change the input if there is no match', () => {
    const pipe = new HighlightTermPipe();
    expect(pipe.transform('Lethargic squirrel', 'over9000')).toBe('Lethargic squirrel', 'String was changed')
  })

});