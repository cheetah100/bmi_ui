import { Component, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import { Point } from './point';


@Component({
  selector: 'ui-stacker-box, idp-stacker-box',
  templateUrl: './stacker-box.component.html',
  styleUrls: ['./stacker-box.component.scss']
})
export class StackerBoxComponent {
  @Input() heading: string = null;
  @Input() dragHandle = false;
  @Input() hideDelete = false;
  @Input() customStyles = false;
  @Output() deleteClicked = new EventEmitter<void>();
  @Output() dragStart = new EventEmitter<StackerDragEvent>();

  constructor(
    private elementRef: ElementRef
  ) { }

  get boundingRect() {
    return this.elementRef.nativeElement ? (this.elementRef.nativeElement as Element).getBoundingClientRect() : null;
  }

  dragStarted(event: MouseEvent) {
    event.preventDefault();
    const element = this.elementRef.nativeElement as Element;
    const rect = this.boundingRect;
    let left: number = rect.left;
    let top: number = rect.top;
    // check whether we are in modal context.
    // hard-coding .iv-modal for modal parent for now.
    // if we find this is required in other contexts, this selector could be an input
    const modalParent: Element = element.closest('.iv-modal');
    if (modalParent) {
      // we need to find the modal offset and reduce x and y values
      const modalRect = modalParent.getBoundingClientRect();
      // also account for modal vertical scroll
      const modalScroll = modalParent.scrollTop;
      left -= modalRect.left;
      top -= modalRect.top - modalScroll;
    }
    this.dragStart.emit({
      component: this,
      mouseEvent: event,
      rect: rect,
      mouseOffset: {
        x: event.clientX - left,
        y: event.clientY - top
      }
    });
  }
}


export interface StackerDragEvent {
  component: StackerBoxComponent;
  mouseEvent: MouseEvent;
  mouseOffset: Point;
  rect: ClientRect;
}
