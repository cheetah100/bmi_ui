import { Component, Input, Output, EventEmitter, TemplateRef, ContentChild, HostListener, ViewChildren, QueryList } from '@angular/core';
import { FormArray, AbstractControl } from '@angular/forms';
import { Point } from './point';
import { StackerBoxComponent, StackerDragEvent } from './stacker-box.component';

// TODO: import this from a shared lib
//import { moveFormArrayItem } from '@shared/utilities/form-array-helpers';
/**
 * Moves an item in a form array at sourceIndex to targetIndex
 *
 * @param formArray - the form array to mutate
 * @param sourceIndex - the index of the item to move in the form array
 * @param targetIndex - the destination for this item. Must be within the bounds
 *     of the formArray.
 */
export function moveFormArrayItem(formArray: FormArray, sourceIndex: number, targetIndex: number) {
  if (sourceIndex < 0 || sourceIndex >= formArray.length) {
    throw new Error(`Source index out of range: ${sourceIndex}`);
  }

  if (targetIndex < 0 || targetIndex >= formArray.length) {
    throw new Error(`Target index out of range: ${targetIndex}`);
  }

  const item = formArray.at(sourceIndex);
  formArray.controls.splice(sourceIndex, 1);
  formArray.controls.splice(targetIndex, 0, item);
  formArray.updateValueAndValidity();
}



@Component({
  selector: 'ui-stacker-list, idp-stacker-list',
  templateUrl: './stacker-list.component.html',
  styleUrls: ['./stacker-list.component.scss']
})
export class StackerListComponent {
  @Input() items: FormArray;
  @Input() propogateDeleteAction = false;
  @Input() hideDelete = false;
  @Input() showDrag = true;
  @Input() customStyles = false;
  @Output() orderChanged = new EventEmitter();
  @Output() propogateDeleteEvent = new EventEmitter();
  @Output() itemDeleted = new EventEmitter();

  @ContentChild(TemplateRef) contentTemplate: TemplateRef<StackerItemTemplateContext<AbstractControl>>;
  @ViewChildren(StackerBoxComponent) stackerBoxes: QueryList<StackerBoxComponent>;

  draggedItem: AbstractControl = null;
  dragOffset: Point = null;
  draggedItemRect: ClientRect = null;
  mousePosition: Point = null;
  hasOrderChanged = false;

  constructor() { }

  removeField(index: number) {
    if (this.propogateDeleteAction) {
      console.log('Propogate delete event to parent component');
      this.propogateDeleteEvent.emit({
        item: this.items.at(index)
      });
    } else {
      this.items.removeAt(index);
    }
    /**
     * Common use case: Parent component needs to be notified when an item is deleted,
     * but does not need to manage item deletion. That should still happen here.
     * propagateDeleteEvent only emits when delegating delete.
     */
    this.itemDeleted.emit({ item: this.items.at(index), index, });
  }

  private triggerOrderUpdated() {
    this.orderChanged.emit();
  }

  get isDragging() {
    return !!this.draggedItem;
  }

  dragStarted(item: AbstractControl, event: StackerDragEvent) {
    this.draggedItem = item;
    this.dragOffset = event.mouseOffset;
    this.draggedItemRect = event.rect;
    this.updateMousePosition(event.mouseEvent);
    this.hasOrderChanged = false;
  }

  updateMousePosition(event: MouseEvent) {
    this.mousePosition = {
      x: event.clientX,
      y: event.clientY
    };

    if (this.stackerBoxes) {
      const stackerBoxRects = this.stackerBoxes.map(box => box.boundingRect);
      const draggedItemIndex = this.items.controls.findIndex(c => c === this.draggedItem);

      const mouseY = event.clientY;
      for (let i = 0; i < stackerBoxRects.length; i++) {
        if (i !== draggedItemIndex && mouseY >= stackerBoxRects[i].top && mouseY <= stackerBoxRects[i].bottom) {
          moveFormArrayItem(this.items, draggedItemIndex, i);
          this.hasOrderChanged = true;
          break;
        }
      }
    }
  }

  @HostListener('document:mouseup', ['$event'])
  mouseUp(event: MouseEvent) {
    if (this.isDragging) {
      this.draggedItem = null;
      this.dragOffset = null;

      if (this.hasOrderChanged) {
        this.triggerOrderUpdated();
        this.hasOrderChanged = false;
      }
    }
  }

  @HostListener('document:mousemove', ['$event'])
  mouseMove(event: MouseEvent) {
    if (this.isDragging) {
      this.updateMousePosition(event);
    }
  }
}


interface StackerItemTemplateContext<T> {
  $implicit: T;
  index: number;
}
