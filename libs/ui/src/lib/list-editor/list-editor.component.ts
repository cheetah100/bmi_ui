import {
  Component,
  forwardRef,
  Input,
  TemplateRef,
  OnInit,
  OnDestroy
} from '@angular/core';
import { provideAsControlValueAccessor } from '../utilities/form-control-macros';
import { TemplatedFormArray } from '../utilities/templated-form-array';
import { TypedFormControl } from '../utilities/typed-form-controls';
import { Subscription } from 'rxjs';
import { BaseControlValueAccessor } from '../utilities/base-control-value-accessor';
import { OptionGroup } from '../select-controls/option';

@Component({
  selector: 'bmi-list-editor',
  template: `
    <ui-stacker-list [items]="formArray">
      <ng-template let-control>
        <bmi-ui-select-input
          [formControl]="control"
          [optionGroups]="optionGroups"
        ></bmi-ui-select-input>
      </ng-template>
    </ui-stacker-list>
    <button
      type="button"
      (click)="addItem()"
      class="btn btn--small btn--link"
      data-cy="add-list-editor-item">
      <span class="icon-add"></span> Add
    </button>
  `,
  styles: [
    `
      :host {
        max-width: 640px;
        display: block;
      }
      :host ::ng-deep .stacker-box-content {
        padding: 0;
      }
      :host ::ng-deep ui-stacker-box.default-style {
        margin-bottom: 0.2rem;
      }
      bmi-ui-select-input {
        display: block;
        width: calc(100% - 0.6rem);
      }
    `
  ],
  providers: [
    provideAsControlValueAccessor(forwardRef(() => ListEditorComponent))
  ]
})
export class ListEditorComponent extends BaseControlValueAccessor<string[]>
  implements OnInit, OnDestroy {
  @Input() controlTemplate: TemplateRef<any> = null;
  @Input() optionGroups: OptionGroup[];
  readonly formArray: TemplatedFormArray<string> = new TemplatedFormArray(
    () => new TypedFormControl(null)
  );
  private readonly subscription = new Subscription();

  ngOnInit() {
    this.subscription.add(
      this.formArray.valueChanges.subscribe((value: string[]) => {
        this.onChanged(value);
      })
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  updateComponentValue(value: string[]): void {
    this.formArray.patchValue(value || [], { emitEvent: false });
  }

  addItem() {
    this.formArray.pushValue('');
  }
}
