import { Component, ContentChild, TemplateRef } from '@angular/core';

import { DropdownMenuComponent } from '../dropdown-menu/dropdown-menu.component';

@Component({
  selector: 'ui-row-with-menu',
  templateUrl: './row-with-menu.component.html',
  styleUrls: ['./row-with-menu.component.scss']
})
export class RowWithMenuComponent {
  @ContentChild(DropdownMenuComponent) dropdownMenu: DropdownMenuComponent;
}
