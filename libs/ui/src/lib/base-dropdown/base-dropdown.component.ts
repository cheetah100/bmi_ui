import {
  Component,
  Input,
  Output,
  ElementRef,
  EventEmitter,
  ContentChild,
  ViewChild,
  TemplateRef,
  HostListener,
  OnInit,
  OnDestroy,
  Inject,
  Optional,
} from '@angular/core';

import { trigger, state, style, animate, transition, group, keyframes } from '@angular/animations';


import { Subscription } from 'rxjs';


import { CloseDialogService, CLOSE_DIALOG_SERVICE_TOKEN } from '../close-dialog.service';

/**
 * This implements the generic scaffolding for a dropdown component.
 *
 * This is intended to handle the functionality of opening and closing the
 * dropdown, accepting events for closing the dropdown and all of the event
 * wrangling.
 *
 * Currently, we have the single-select, multi-select, path-picker controls in
 * several variations and these all impelement this logic separately. Further,
 * the functionality isn't consistent between them -- some of these controls
 * support both floating and "expanding" variants, but this isn't consistent.
 *
 * This control expects two templates as content: #dropdownControl and
 * #dropdownContent.
 *
 * The control is what gets displayed all the time, like the input control or
 * button to trigger the dropdown.
 *
 * The content is displayed in a dropped down area that floats over the page
 * below. It's not rendered unless the dropdown is open.
 *
 * There are multiple ways to control the dropdown:
 *
 * 1. Let it do it itself. By default, clicking on the control area will toggle
 *    the dropdown.
 * 2. Call the open() and close() methods on the dropdown
 * 3. Set the isOpen Input property -- supports two-way binding
 *
 * To make controlling the dropdown easier, a reference to this dropdown
 * component is provided to both the control and content templates as the
 * $implicit variable, and explicitly under the name 'dropdown'.
 *
 *     <ui-base-dropdown [toggleOpenOnClick]="false">
 *       <ng-template #dropdownControl let-dropdown>
 *         <strong>Is Open:</strong> {{dropdown.isOpen}}
 *         <button (click)="dropdown.toggle()">Toggle</button>
 *       </ng-template>
 *       <ng-template>
 *         <p>This is the dropdown content</p>
 *       </ng-template>
 *     </ui-base-dropdown>
 */
@Component({
  selector: 'ui-base-dropdown',
  templateUrl: './base-dropdown.component.html',
  styleUrls: ['./base-dropdown.component.scss'],
  animations: [
    trigger('toggleOpen', [
      transition(':enter', [
        style({
          opacity: 0,
          'transform': 'translateY(-0.75rem)'
        }),
        animate('0.25s ease-in-out', style({
          opacity: 1,
          'transform': 'translateY(0rem)'
        }))
      ]),
      transition(':leave', [
        animate('0.25s ease-in-out', style({
          opacity: 0,
          'transform': 'translateY(-0.75rem)'
        }))
      ])
    ])
  ]
})
export class BaseDropdownComponent implements OnInit, OnDestroy {
  /**
   * [INPUT] Should the dropdown automatically toggle when the "control"
   * template is clicked?
   *
   * If you want to avoid this behaviour, then your control template should stop
   * its event propagation so that the click handler in this base control never
   * gets hit.
   */
  @Input() toggleOpenOnClick = true;
  @Input() rightAlign = false;
  @Input() positionPolicy: null | 'left' | 'right' | 'noRelative' | 'auto' = 'auto';
  /**
   * If there's less than this amount of space to the right of the control, it
   * will switch into right align mode (if sizing policy is auto and rightAlign
   * is not set)
   */
  @Input() autoPositionAssumedWidth = 20;

  // If true, the auto positioning thinks we should right align this dropdown.
  // This is considered whenever the dropdown is opened based on the current DOM
  // rects. If the layout changes while the dropdown is open, the position won't
  // be recalculated.
  private autoPositionRightAlign = false;

  get shouldBeRightAligned() {
    return this.rightAlign || this.autoPositionRightAlign || this.positionPolicy === 'right';
  }

  @ContentChild('dropdownControl') controlTemplate: TemplateRef<DropdownTemplateContext>;
  @ContentChild('dropdownContent') dropdownTemplate: TemplateRef<DropdownTemplateContext>;

  @ViewChild('dropdownContentElement') dropdownContentElement: ElementRef;

  private _isOpen = false;
  get isOpen(): boolean {
    return this._isOpen;
  }

  @Input()
  set isOpen(value: boolean) {
    if (value) {
      this.open();
    } else {
      this.close();
    }
  }

  /**
   * This will get triggered when the dropdown is opened or closed, and supports
   * the banana-in-box binding on the isOpen property.
   */
  @Output() isOpenChange = new EventEmitter<boolean>();

  templateContext: DropdownTemplateContext = {
    $implicit: this,
    dropdown: this
  };

  // This flag is used to prevent the dropdown from closing just as it first
  // opens, if the click to open it also triggers a dialog close event.
  private hasJustOpened = false;

  private subscription: Subscription = null;

  constructor(
    @Optional() @Inject(CLOSE_DIALOG_SERVICE_TOKEN) private closeDialogService: CloseDialogService,
    @Optional() private hostElement: ElementRef
  ) { }

  ngOnInit() {
    if (!!this.closeDialogService) {
      this.subscription = this.closeDialogService.onClose.subscribe(() => {
        if (!this.hasJustOpened) {
          this.close();
        }
      });
    }
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  open() {
    if (!this._isOpen) {
      this.considerAutoPositioning();
      this._isOpen = true;
      this.hasJustOpened = true;
      setTimeout(() => this.hasJustOpened = false, 50);
      this.isOpenChange.emit(this._isOpen);
    }
  }


  close() {
    if (this._isOpen) {
      this._isOpen = false;
      this.isOpenChange.emit(this._isOpen);
    }
  }

  toggle() {
    if (!this.isOpen) {
      this.open();
    } else {
      this.close();
    }
  }

  onControlAreaClicked(event: MouseEvent) {
    event.preventDefault();
    this.toggle();
  }

  private considerAutoPositioning() {
    this.autoPositionRightAlign = false;
    if (this.hostElement && this.positionPolicy === 'auto') {
      // Make a snap decision based on the information we currently have.
      this.decideOnAutoPositioning();

      // If we've decided to right align, commit to it. If not, the content may
      // still be quite big (unusual case), but we won't know until it's
      // rendered. Instead, we'll wait a bit for it to render, then reconsider.
      // This means it might snap to right-align if its estimates were wrong and
      // the content would go offscreen.
      if (!this.autoPositionRightAlign && !this.dropdownContentElement) {
        setTimeout(() => this.decideOnAutoPositioning(), 0);
      }
    }
  }

  private decideOnAutoPositioning(): void {
    const element = this.hostElement.nativeElement as HTMLElement;
    const hostRect = element.getBoundingClientRect();
    const viewportWidth = window.innerWidth;
    let rightEdgePosition = Math.max(hostRect.right, hostRect.left + this.autoPositionAssumedWidth);
    if (this.dropdownContentElement) {
      // If we know the size of the contents, we can make a better guess
      // about whether we need to right align. This isn't known when the
      // dropdown is closed though, because the contents hasn't been
      // rendered yet.
      const contentEl = this.dropdownContentElement.nativeElement as HTMLElement;
      const contentRightPosition = hostRect.left + contentEl.offsetWidth;
      rightEdgePosition = Math.max(rightEdgePosition, contentRightPosition);
    }

    this.autoPositionRightAlign = (rightEdgePosition + 10) > viewportWidth;
  }

  @HostListener('click', ['$event'])
  stopClickPropagation($event: MouseEvent) {
    // We want to stop all click on this component (including the dropped down
    // portion) from bubbling up and triggering a global "close all dialogs"
    // event... but we want the opening click to propagate through and close any
    // other dialogs.
    //
    // This is a really hacky awful way of doing it and is responsible for a
    // fair number of bugs, and overall complexity when implementing these
    // controls, as you've got to be super cogniscent of event order ... or just
    // break it 'til it's fixed.
    //
    // TL;DR, allow the first clicks immediately after opening the dropdown to
    // propagate through.
    if (!this.hasJustOpened) {
      $event.stopPropagation();
    }
  }
}


export class DropdownTemplateContext {
  $implicit: BaseDropdownComponent;
  dropdown: BaseDropdownComponent;
}
