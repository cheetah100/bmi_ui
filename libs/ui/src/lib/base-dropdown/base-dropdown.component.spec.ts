import { TestBed, fakeAsync, tick } from '@angular/core/testing';

import { BaseDropdownComponent } from './base-dropdown.component';
import { SimpleCloseDialogService, CLOSE_DIALOG_SERVICE_TOKEN } from '../close-dialog.service';

describe('BaseDropdownComponent', () => {
  let dropdown: BaseDropdownComponent;
  let dialogService: SimpleCloseDialogService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        BaseDropdownComponent,
        SimpleCloseDialogService,
        {provide: CLOSE_DIALOG_SERVICE_TOKEN, useExisting: SimpleCloseDialogService}
      ]
    });

    dropdown = TestBed.get(BaseDropdownComponent) as BaseDropdownComponent;
    dialogService = TestBed.get(SimpleCloseDialogService) as SimpleCloseDialogService;
    dropdown.ngOnInit();
  });

  it('should initially be closed', () => {
    expect(dropdown.isOpen).toBe(false);
  });

  it('should remain closed if close() is called', () => {
    dropdown.close();
    expect(dropdown.isOpen).toBe(false);
  });

  it('should open()', () => {
    dropdown.open();
    expect(dropdown.isOpen).toBe(true);
  });

  it('should remain open if open() is called again', () => {
    dropdown.open();
    dropdown.open();
    expect(dropdown.isOpen).toBe(true);
  });

  describe('Dialog service onClose callback', () => {
    it('should not close if it just opened', fakeAsync(() => {
      dropdown.open();

      // Advance a trivially small amount of time
      tick(5);

      dialogService.triggerClose();
      tick(500);

      expect(dropdown.isOpen).toBe(true);
    }));

    it('should close if the dialog has been open a while', fakeAsync(() => {
      dropdown.open();
      tick(500);

      dialogService.triggerClose();
      tick(20);

      expect(dropdown.isOpen).toBe(false);
    }));
  })


  describe('isOpenChange', () => {
    let eventCount: number;
    let lastEvent: boolean;
    beforeEach(() => {
      eventCount = 0;
      lastEvent = undefined;
      dropdown.isOpenChange.asObservable().subscribe(state => {
        eventCount += 1;
        lastEvent = state;
      });
    });

    it('should trigger the isOpenChange event when open() is called', () => {
      dropdown.open();
      expect(eventCount).toBe(1);
      expect(lastEvent).toBe(true);
    });

    it('should not trigger event if the value does not change', () => {
      dropdown.open();
      dropdown.open();
      expect(eventCount).toBe(1);
    });

    it('should trigger the event when close() is called', () => {
      dropdown.open();
      dropdown.close();
      expect(eventCount).toBe(2);
      expect(lastEvent).toBe(false);
    });

    it('should trigger the event when the isOpen property is set', () => {
      dropdown.isOpen = true;
      expect(eventCount).toBe(1);
      expect(lastEvent).toBe(true);
    });
  });

  describe('click propagation prevention', () => {
    let eventSpy: any;
    beforeEach(() => {
      eventSpy = jasmine.createSpyObj('EventSpy', ['stopPropagation']);
    });

    it('should stop clicks on the component from propagating', () => {
      dropdown.stopClickPropagation(eventSpy);
      expect(eventSpy.stopPropagation.calls.count()).toBe(1);
    });

    it('should not block the opening click', () => {
      dropdown.open();
      dropdown.stopClickPropagation(eventSpy);
      expect(eventSpy.stopPropagation.calls.count()).toBe(0);
    });

    it('should continue blocking ticks after a short period of time', fakeAsync(() => {
      dropdown.open();
      tick(100);
      dropdown.stopClickPropagation(eventSpy);
      expect(eventSpy.stopPropagation.calls.count()).toBe(1);
    }));
  });
});
