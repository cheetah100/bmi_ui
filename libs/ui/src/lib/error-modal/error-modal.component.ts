import { Component } from '@angular/core';
import { ModalContext } from '../modal/modal-context';
import { ErrorSummary } from './error-summary';

@Component({
  template: `
    <p>{{ error.message }}</p>

    <ul *ngIf="error.reasons" class="error-modal__reasons">
      <li *ngFor="let reason of error.reasons">
        <strong>{{reason.name}}</strong>
        {{reason.detail}}
      </li>
    </ul>

    <ng-container *ngIf="error.extraDetail">
      <hr>
      <ui-collapsible-section *ngIf="error.extraDetail" label="Extra Details">
        <code class="error-modal__extra-detail-text">
          <pre>{{error.extraDetail}}</pre>
        </code>
      </ui-collapsible-section>
    </ng-container>

  `,

  styles: [`
    .error-modal__extra-detail-text {
      overflow-x: auto;
      overflow-y: visible;
      font-size: 10px;
    }
  `]
})
export class ErrorModalComponent {
  constructor(private modalContext: ModalContext) {}

  get error() {
    return this.modalContext.data as ErrorSummary;
  }
}
