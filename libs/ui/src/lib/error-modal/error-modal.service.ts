import { Injectable } from '@angular/core';
import { Observable, MonoTypeOperatorFunction, throwError } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ModalService } from '../modal/modal.service';
import { ErrorSummary } from './error-summary';
import { summarizeError } from './summarize-error';

import { ErrorModalComponent } from './error-modal.component';


export interface ErrorModalOptions<T> {
  retryable?: boolean;
  onError?: (err: any, source: Observable<T>) => Observable<T>;
}


@Injectable({providedIn: 'root'})
export class ErrorModalService {
  constructor(private modalService: ModalService) {}

  open(errorSummary: ErrorSummary, retryable = false) {
    return this.modalService.open({
      content: ErrorModalComponent,
      title: errorSummary.title || 'Error',
      data: errorSummary,
      actions: [
        {
          action: 'cancel',
          buttonText: 'OK',
          buttonStyle: 'primary'
        },
        retryable ? {
          action: 'retry',
          buttonStyle: 'ghost',
          buttonText: 'Retry'
        } : null
      ].filter(x => !!x)
    })
  }

  catchError<T>(options?: ErrorModalOptions<T>): MonoTypeOperatorFunction<T> {
    const opts = options || {};
    const onError = options.onError || ((err) => throwError(err));
    return source => source.pipe(
      catchError((error, origin) => {
        return this.open(summarizeError(error), opts.retryable).pipe(
          switchMap(result => result.action === 'retry' ? origin : onError(error, origin))
        );
      })
    );
  }
}


