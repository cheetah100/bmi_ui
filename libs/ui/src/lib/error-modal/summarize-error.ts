import { HttpErrorResponse } from '@angular/common/http';
import { ErrorSummary, ErrorReason } from './error-summary';


export function summarizeError(error: unknown): ErrorSummary {
  if (isHttpErrorResponse(error)) {
    return summarizeHttpError(error);
  } else if (error instanceof Error) {
    return summarizeError(error);
  } else {
    return {
      message: 'An error occurred',
      error: error,
      extraDetail: error ? String(error) : undefined
    };
  }
}


function isHttpErrorResponse(error: unknown): error is HttpErrorResponse {
  return (
    error &&
    typeof error === 'object' &&
    error['status'] &&
    error['statusText'] &&
    error['name'] === 'HttpErrorResponse'
  );
}


function summarizeHttpError(error: HttpErrorResponse): ErrorSummary {
  return {
    message: `HTTP Error (${error.status} ${error.statusText})`,
    reasons: getHttpErrorReasons(error),
    error: error,
    extraDetail: JSON.stringify(error, undefined, 2),
  }
}

function getHttpErrorReasons(error: HttpErrorResponse): ErrorReason[] {
  if (typeof error.error === 'object') {
    return Object.entries(error.error).map(([key, value]) => ({
      name: key,
      detail: String(value)
    }));
  } else {
    return undefined;
  }
}


function summarizeException(error: Error): ErrorSummary {
  return {
    message: error.message,
    extraDetail: String(error),
  }
}
