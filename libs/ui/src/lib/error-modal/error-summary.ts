export interface ErrorSummary {
  title?: string;
  message: string;
  reasons?: ErrorReason[];
  extraDetail?: string;
  error?: any;
}


export interface ErrorReason {
  name: string;
  detail: string;
}
