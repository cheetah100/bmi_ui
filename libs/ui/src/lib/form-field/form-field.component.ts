import { Component, Input, ContentChild } from '@angular/core';
import { AbstractControl, FormControlName, FormControlDirective } from '@angular/forms';


/**
 * Wraps around an <input> element to show a label and validation feedback.
 *
 * The content for this control should be an input element, or similar form
 * control. This component should handle displaying the label, along with any
 * validation feedback.
 *
 * If the control is required, you can set [required]="true" on this component
 * which will show a "*" next to the label indicating a required field. This is
 * just a visual indicator though, you still need to specify a required
 * validator on your Reactive Forms control -- unfortunately this can't be
 * easily inferred from the control itself.
 */
@Component({
  selector: 'ui-form-field, idp-form-group',
  templateUrl: './form-field.component.html',
  styleUrls: ['./form-field.component.scss']
})
export class FormFieldComponent {
  // This will attempt to infer tho form control binding from the form group
  // contents, by looking for the Angular Forms control directives. This will
  // use the first one that it finds in the contents, which could be a problem
  // if multiple bound controls were included in the form group. That seems
  // unlikely though
  @ContentChild(FormControlName) formControlNameDirective: FormControlName;
  @ContentChild(FormControlDirective) formControlDirective: FormControlDirective;

  @Input() control: AbstractControl;
  @Input() label: string;
  @Input() required: boolean = false;
  @Input() hideLabelIfEmpty = false;

  /**
   * If defined, this will display a help icon in the form group with some
   * additional info.
   */
  @Input() infoText: string = null;

  /**
   * If defined, this will appear beneath the control and provides a place for
   * some additional instructions.
   */
  @Input() helpText: string = null;

  /**
   * Gets the control associated with this form group.
   *
   * This can be explicitly defined, by binding the [control] property, but it
   * can also be inferred from a formControl or formControlName directive on
   * a child input.
   */
  get formControl(): AbstractControl {
    if (this.control) {
      return this.control;
    } else {
      const controlDirective = this.formControlNameDirective || this.formControlDirective;
      if (controlDirective) {
        return controlDirective.control;
      } else {
        return null;
      }
    }
  }

  // TODO: Remove this in favour of using label.
  get title() {
    return this.label;
  }

  @Input()
  set title(value: string) {
    this.label = value;
  }


  get isInvalid() {
    const control = this.formControl;
    return control && control.invalid && control.touched;
  }

  get shouldShowLabel() {
    return !this.hideLabelIfEmpty || (this.label || this.infoText);
  }

  get formFeedbackText() {
    if (this.isInvalid) {
      return this.getErrorText();
    } else {
      return this.helpText;
    }
  }

  get isRequired() {
    if (this.required) {
      return true;
    } else if (this.formControl && this.formControl.validator) {
      const validator = this.formControl.validator(<AbstractControl>{});
      if (validator && validator.required) {
        return true;
      }
    }
    return false;
  }

  getErrorText() {
    const errors = this.formControl.errors;
    if (errors['required']) {
      return `Value is required`;
    } else {
      return Object.values(errors).filter(err => typeof err === 'string')[0] || 'Validation Error';
    }
  }
}
