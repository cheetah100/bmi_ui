import {
  Component,
  forwardRef, OnInit
} from '@angular/core';
import { DateAdapter } from '@angular/material/core';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { BaseControlValueAccessor } from '../utilities/base-control-value-accessor';
import { provideAsControlValueAccessor } from '../utilities/form-control-macros';


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ui-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: [ './datepicker.component.scss' ],
  providers: [
    provideAsControlValueAccessor(forwardRef(() => DatepickerComponent))
  ]
})
export class DatepickerComponent extends BaseControlValueAccessor<number> implements OnInit {
  date;

  constructor(
    private adapter: DateAdapter<any>
  ) {
    super();
  }

  ngOnInit() {
    const loc = this.getUsersLocale();
    this.adapter.setLocale(loc);
  }

  /**
   * Called by datepicker when the date changes
   * Converts Date to Unix timestamp and propagates the changed value
   * @param {string} type update type
   * @param {Event} event generated event with a value of type Date
   */
  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.onChanged(event.value.getTime())
  }

  /* ControlValueAccessor methods */

  updateComponentValue(value: number): void {
    this.date = new Date(value)
  }

  /**
   * Get the current locale from the browser
   * @param defaultValue default locale, set to en-US
   */
  private getUsersLocale(defaultValue: string='en-US'): string {
    if (typeof window === 'undefined' || typeof window.navigator === 'undefined') {
      return defaultValue;
    }
    const wn = window.navigator as any;
    let lang = wn.languages ? wn.languages[0] : defaultValue;
    lang = lang || wn.language || wn.browserLanguage || wn.userLanguage;
    return lang;
  }
}
