import { FormGroup, AbstractControl, ValidationErrors } from '@angular/forms';

const mappedOrDefault = (key: string, map: Map<string, string>): string => {
  let defaultName = key.replace(/[-_]/g, ' ').trim();
  try {
    defaultName = defaultName.charAt(0).toUpperCase() + defaultName.slice(1);
  } catch (e) {
    // Unlikely, we're using a control id
  }
  if (!map) {
    return defaultName;
  }
  const value: string = map.get(key);
  return value || defaultName;
};

const errorString = (control: AbstractControl): string => {
  if (!control.errors) {
    return 'is not valid';
  } else if (control.errors.required) {
    return 'is required';
  } else {
    return 'control.errors';
  }
}

/**
 * 
 * Generate a readable validation message summary.
 * Intended for use with the submit/save button on larger forms,
 * Where the cause of the validation issues might not be obvious.
 * 
 * For now, using just the direct child controls of the form group.
 * We could change this in future, maybe an option to give more detail.
 * 
 * i.e. in board editor, for now we'll say
 * 'Phases is not valid'
 * 
 * but maybe there would be a reason to have something like
 * `Phases is not valid.
 *   In item 3, name is required but has been left empty.
 *   In item 5, order must be greater than 0 but is set to -4
 * `
 * which is probably too much in most cases
 * 
 * @param formGroup
 * @param map optional. Use custom titles for control names. Keys should be form control ids.
 * Omitted controls will use prettified id as name.
 */

const formValidationSummary = (formGroup: FormGroup, map: Map<string, string> = null): string[] => {
  if (!formGroup || formGroup.valid) {
    return [];
  }
  return Object.entries(formGroup.controls)
    .filter((entry: [string, AbstractControl]) => {
      const [, control] = entry;
      return (!control.disabled && !control.valid);
    })
    .map((entry: [string, AbstractControl]) => {
      const [id, control] = entry;
      const name = mappedOrDefault(id, map);
      const error = errorString(control);
      return `${name} ${error}`;
    });
};

export { formValidationSummary };
