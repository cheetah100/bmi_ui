import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

// This matches basic decimal notation numbers, e.g:
//
//  '37', '37.8', '+37.8', '-12.12355676', '0.4', '.4'
//
// This supports a subset of the Java Double notation -- i.e. this filter should
// only pass numbers that will be valid doubles in Java, according to the
// Double.valueOf() documentation and Java Language Specification.
//
// We don't accept hexadecimal notations, exponent notation, or special IEEE754
// concepts like NaN and Infinity.
//
// See the corresponding spec file for more comprehensive tests.
const NUMBER_REGEX = /^\s*[+-]?((\d+(\.\d+)?)|(\.(\d+)))([eE][+-]?\d+)?\s*$/;

/**
 * Checks if a value is a valid number.
 *
 * If the value is a string, then this must be a valid decimal. If the value is
 * a number, then this must be finite -- neither NaN nor infinity.
 * @param value
 */
export function isValidNumber(value: any): boolean {
  switch (typeof value) {
    case 'string':
      return NUMBER_REGEX.test(value);
    case 'number':
      return isFinite(value);
    default:
      return false;
  }
}

/**
 * Checks if the value should be considered empty for validation purposes
 *
 * It's useful for reusable validators to pass blank values so that they can
 * be combined with the required validator.
 *
 * Angular's `Validators.required` only checks for empty strings -- whitespace
 * still counts as content, so we must do the same here otherwise a required
 * numeric string would accept '  ' which isn't valid.
 * @param value
 */
function isNullOrEmpty(value: any): boolean {
  if (value === null || value === undefined) {
    return true;
  } else if (typeof value === 'string' && value.length === 0) {
    return true;
  } else {
    return false;
  }
}


export function mustBeNumber(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const value = control.value;
    if (isNullOrEmpty(value) || isValidNumber(value)) {
      return null;
    } else {
      return {
        'mustBeNumber': 'Must be a number'
      };
    }
  }
}
