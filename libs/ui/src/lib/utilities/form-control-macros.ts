import { Type, Provider, forwardRef, ForwardRefFn } from '@angular/core';
import { ControlValueAccessor, Validator, NG_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';

// These macro functions should make it easier/less error prone to register a
// component as a CVA or Validator.
//
// These are evaluated by the AOT metadata compiler by 'folding' -- essentially
// the AOT compiler will substitute and reduce expressions symbolically, rather
// than executing them as Typescript. Unfortunately limits what we can do with
// them and still have it compile successfully.
//
// It's necessary to use these in conjunction with a forwardRef() to reference
// the component class. forwardRef() is handled specially by ngtsc when
// compiling metadata, so we can't just accept a lambda argument and have that
// folded in.

/**
 * Provides a component as a ControlValueAccessor.
 */
export function provideAsControlValueAccessor<T extends ControlValueAccessor>(type: Type<T>): Provider {
  return {
    provide: NG_VALUE_ACCESSOR,
    useExisting: type,
    multi: true,
  };
}


/**
 * Provides a component as a validator.
 */
export function provideAsValidator<T extends Validator>(type: Type<T>): Provider {
  return {
    provide: NG_VALIDATORS,
    useExisting: type,
    multi: true,
  };
}

/**
 * A helper macro to provide a component as a self-validating CVA
 */
export function provideAsValidatingCVA<T extends Validator & ControlValueAccessor>(type: Type<T>): Provider[] {
  return [
    provideAsControlValueAccessor(type),
    provideAsValidator(type)
  ];
}
