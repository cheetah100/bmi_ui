import { FormArray, FormGroup } from '@angular/forms';

/**
 * Deletes all items from a form array.
 */
export function clearFormArray(formArray: FormArray) {
  while (formArray.length > 0) {
    formArray.removeAt(0);
  }
}


/**
 * Moves an item in a form array at sourceIndex to targetIndex
 *
 * @param formArray - the form array to mutate
 * @param sourceIndex - the index of the item to move in the form array
 * @param targetIndex - the destination for this item. Must be within the bounds
 *     of the formArray.
 */
export function moveFormArrayItem(formArray: FormArray, sourceIndex: number, targetIndex: number) {
  if (sourceIndex < 0 || sourceIndex >= formArray.length) {
    throw new Error(`Source index out of range: ${sourceIndex}`);
  }

  if (targetIndex < 0 || targetIndex >= formArray.length) {
    throw new Error(`Target index out of range: ${targetIndex}`);
  }

  const item = formArray.at(sourceIndex);
  formArray.controls.splice(sourceIndex, 1);
  formArray.controls.splice(targetIndex, 0, item);
  formArray.updateValueAndValidity();
}


/**
 * Moves an item in the form array up, if possible.
 */
 export function moveFormArrayItemUp(formArray: FormArray, sourceIndex: number) {
   if (sourceIndex !== 0) {
     moveFormArrayItem(formArray, sourceIndex, sourceIndex - 1);
   }
 }


/**
 * Moves an item in the form array down, if possible
 */
export function moveFormArrayItemDown(formArray: FormArray, sourceIndex: number) {
  if (sourceIndex + 1 < formArray.length) {
    moveFormArrayItem(formArray, sourceIndex, sourceIndex + 1);
  }
}


/**
 * Gets a value from a form array or group, returning a default if the value
 * doesn't exist.
 */
export function getFormValue<T>(control: FormGroup | FormArray, formPath: string | (string | number)[], defaultValue: T = null): T {
  const c = control && control.get(formPath);
  return c ? c.value : defaultValue;
}
