import { AbstractControl, FormControl, FormGroup, FormArray, AbstractControlOptions, ValidatorFn, AsyncValidatorFn } from '@angular/forms';
import { Observable } from 'rxjs';

// These classes and interfaces provide a typed facade for form groups. This
// doesn't provide 'strong' typesafety guarantees, but improves developer
// ergonomics and should help to catch errors, or avoid casts.

// These types shouldn't really change the semantics of the built-in Angular
// form control models, rather just redeclare them with stricter type signatures


export interface FormControlUpdateOptions {
  emitEvent?: boolean;
  onlySelf?: boolean;
}


export interface TypedAbstractControl<T> extends AbstractControl {
  value: T;
  valueChanges: Observable<T>;
  setValue(value: T, options?: FormControlUpdateOptions);
  patchValue(value: T | Partial<T>, options?: FormControlUpdateOptions);
}


type TypedFormGroupControls<T extends {}> = {[P in keyof T]: TypedAbstractControl<T[P]>};


export class TypedFormGroup<T> extends FormGroup implements TypedAbstractControl<T> {
  value: T;
  valueChanges: Observable<T>;
  controls: TypedFormGroupControls<T>;

  constructor(controls: TypedFormGroupControls<T>,
    validatorOrOpts?: ValidatorFn | ValidatorFn[] | AbstractControlOptions | null, asyncValidator?: AsyncValidatorFn | AsyncValidatorFn[] | null) {
    super(controls, validatorOrOpts);
  }


  setValue(value: T, options?: FormControlUpdateOptions) {
    super.setValue(value, options);
  }

  patchValue(value: T | Partial<T>, options?: FormControlUpdateOptions) {
    super.patchValue(value, options);
  }

  getRawValue(): T {
    return super.getRawValue();
  }
}

export class TypedFormArray<T> extends FormArray implements TypedAbstractControl<T[]> {
  value: T[];
  valueChanges: Observable<T[]>;
  controls: TypedAbstractControl<T>[];


  constructor(controls: TypedAbstractControl<T>[], ...args) {
    super(controls, ...args);
  }

  setValue(value: T[], options?: FormControlUpdateOptions) {
    super.setValue(value, options);
  }

  patchValue(value: T[] | Partial<T>[], options?: FormControlUpdateOptions) {
    super.patchValue(value, options);
  }

  getRawValue(): T[] {
    return super.getRawValue();
  }
}


export class TypedFormControl<T> extends FormControl implements TypedAbstractControl<T> {
  value: T;
  valueChanges: Observable<T>;

  constructor(value?: T, validator_or_options?: ValidatorFn | ValidatorFn[] | AbstractControlOptions) {
    super(value, validator_or_options);
  }

  setValue(value: T, options?: FormControlUpdateOptions) {
    super.setValue(value, options);
  }

  patchValue(value: T | Partial<T>, options?: FormControlUpdateOptions) {
    super.patchValue(value, options);
  }
}
