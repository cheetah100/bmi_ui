import { AbstractControl } from '@angular/forms';
import { TypedFormArray, TypedAbstractControl, FormControlUpdateOptions } from './typed-form-controls';

type TemplateFunction<T> = (value: T) => TypedAbstractControl<T>;


/**
 * A variant of a FormArray that uses a template to build its content.
 *
 * This is intended to automate a common pattern, where you have a variable
 * length array of items that all share the same structure. Regular Angular
 * FormArrays won't grow or shrink the array when setting the value -- the
 * structure needs to match.
 *
 * This class allows you to specify a function that will create the contents of
 * the form array element -- for example, given a value, create a form group or
 * control.
 *
 * When the form array's value is updated, it will rebuild the array if the
 * length doesn't match.
 *
 * Currently, the rebuild process may nuke any form state like pristine, but
 * this logic will only be invoked when the size changes.
 *
 */
export class TemplatedFormArray<T> extends TypedFormArray<T> {
  constructor(private template: TemplateFunction<T>, ...args) {
    // TODO: better typing on the validators...
    super([], ...args);
  }

  setValue(value: T[], options?: FormControlUpdateOptions) {
    if (this.needsRebuild(value)) {
      this.rebuildArray(value);
    }
    super.setValue(value, options);
  }

  patchValue(value: T[], options?: FormControlUpdateOptions) {
    if (this.needsRebuild(value)) {
      this.rebuildArray(value);
    }
    super.patchValue(value, options);
  }

  reset(value: T[], options?: FormControlUpdateOptions) {
    if (this.needsRebuild(value)) {
      this.rebuildArray(value);
    }
    super.reset(value, options);
  }


  /**
   * Pushes a value into the form array, applying the template.
   *
   * This might be preferable to using .push() directly, as it ensure the
   * formarray's templates is applied.
   */
  pushValue(item: T, options?: FormControlUpdateOptions) {
    const control = this.template(item);
    this.push(control, {emitEvent: false});

    control.patchValue(item, options);
  }

  private needsRebuild(newValue: T[]) {
    return !newValue || newValue.length !== this.length;
  }

  private rebuildArray(value: T[]) {
    while (this.length > 0) {
      this.removeAt(0, {emitEvent: false});
    }

    if (value) {
      value.forEach(v => this.pushValue(v, {emitEvent: false}));
    }
  }

  // MASSIVE BODGE ALERT! Explanation needed!
  //
  // Adding {emitEvent: false} support to these Form Array methods!
  //
  // The Angular Forms form array doesn't give us a way to suppress change
  // events when adding/removing items from forms. On complex form this can lead
  // to a spam of change events which can cause a wide range of other bugs.
  //
  // The solution to this is actually fairly simple: the FormArray update
  // operations should pass options through to updateValueAndValidity.
  //
  // Unfortunately they don't, and worse, due to the massive software
  // engineering fail that is the @angular/forms implementation, we can't just
  // fork our own form array here altogether (I wish this were an option!)
  //
  // Instead, we can replace these methods. They are quite simple, but because
  // they rely on private implementation details I've had to copy them here and
  // add casts.
  //
  // This is a future maintenance hazard. If the Angular Forms implementation
  // changes drastically this may break; if that happens find the latest code
  // and copy it in here and adapt to pass through options, or reconsider this
  // approach

  push(control: TypedAbstractControl<T>, options?: FormControlUpdateOptions) {
    // MASSIVE BODGE ALERT: see comment above.
    // Dear future developer: if this is broken, sorry. Blame the Angular developers.

    // Sam: casting `this as any` so that I can call these private methods. Yuck.
    this.controls.push(control);
    (this as any)._registerControl(control);

    // Sam: passing options here --v
    this.updateValueAndValidity(options);
    (this as any)._onCollectionChange();
  }


  removeAt(index: number, options?: FormControlUpdateOptions) {
    // MASSIVE BODGE ALERT: see comment above.
    // Dear future developer: if this is broken, sorry. Blame the Angular developers.

    // Sam: added cast to any here due to accessing private members
    if (this.controls[index]) (this.controls[index] as any)._registerOnCollectionChange(() => {});
    this.controls.splice(index, 1);

    // Sam: passing options here
    this.updateValueAndValidity(options);
  }
}
