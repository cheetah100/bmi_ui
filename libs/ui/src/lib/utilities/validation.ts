import { FormGroup, AbstractControl, FormArray, Validators, ValidatorFn, AsyncValidatorFn, ValidationErrors } from '@angular/forms';

import { Observable, of } from 'rxjs';
import { map, take } from 'rxjs/operators';
import isNil from 'lodash-es/isNil';
import { mustBeNumber } from './number-validator';

type PredicateFunction<T> = (value: T) => boolean;


/**
 * Traverses a reactive form tree, 'visiting' all of the controls.
 *
 * @param control the form control, array or form group to visit.
 */
export function visitFormControls(control: AbstractControl, visitorFunction: (control: AbstractControl) => void) {
  if (control instanceof FormGroup) {
    for (const cc of Object.values(control.controls)) {
      visitFormControls(cc, visitorFunction);
    }
  } else if (control instanceof FormArray) {
    for (const cc of control.controls) {
      visitFormControls(cc, visitorFunction);
    }
  }

  visitorFunction(control);
}


/**
 * Force the validation on an entire form group/control/array
 *
 * If you've got stateful validation logic you might need to FORCE revalidation
 * of an entire complex form. Weirdly Angular doesn't provide a way to do this,
 * so this code will recursively explore an entire form tree and call
 * updateValueAndValidity on each control.
 *
 * This does a depth-first traversal and will explore form groups and form
 * arrays.
 *
 * By default, the emitEvent option of updateValueAndValidity will be false, to
 * avoid spamming events or getting into an infinite loop if triggered from a
 * valueChange event.
 *
 * @param control - the form control, form array or formgroup to explore
 * @param emitEvent - whether to emit events during the validation.
 */
export function forceValidation(control: AbstractControl, emitEvent: boolean = false): void {
  visitFormControls(control, c => c.updateValueAndValidity({emitEvent: emitEvent, onlySelf: true}));
}


export function markFormAsTouched(control: AbstractControl) {
  visitFormControls(control, c => c.markAsTouched({onlySelf: true}));
}


/**
 * A collection of validator utilities. This extends the Angular validators
 * object, so all of the standard validators are avaliable here too.
 *
 * This class is @dynamic so that ngc doesn't complain about the static methods
 * here having lambdas due to strict metadata checking being enabled by default
 * for libraries. The static methods here will never be used in Angular metadata
 * so there's nothing to fear.
 */
export class IdpValidators extends Validators {
  /**
   * Conditionally applies a validator.
   *
   * This will only apply the validator function (or functions) if the predicate
   * function returns true.
   *
   * A use case for this is to enable validation on form controls if a setting
   * is enabled.
   *
   * @param predicate - a function that returns true if the validators should be
   *     applied.
   * @param ...validators - one or more validator functions to apply to this
   *     control if the predicate returns a truthy value.
   */
  static if(predicate: PredicateFunction<AbstractControl>, ...validators: ValidatorFn[]): ValidatorFn {
    const validator = Validators.compose(validators);
    return (control: AbstractControl) => {
      if (!predicate(control)) {
        return null;
      } else {
        return validator(control);
      }
    };
  }

  /**
   * Validates that a form array has a minimum number of elements.
   */
  static minFormArrayLength(minLength: number): ValidatorFn {
    return (control: AbstractControl) => {
      const formArray = <FormArray>control;
      if (formArray.length >= minLength) {
        return null;
      } else {
        return {'minLength': `${minLength} items required`};
      }
    };
  }

  /**
   * Returns a validator that will ensure the control contains at least one
   * 'word' character
   */
  static mustBeNonBlank() {
    return IdpValidators.withErrorMessage(Validators.pattern(/.*\w+.*/), 'Cannot be blank');
  }

  /**
   * Validates that a form field contains a valid number, or is empty.
   *
   * This will also accept a completely empty form field, so that this can be
   * used in conjunction with Validators.required() where necessary.
   */
  static mustBeNumber() {
    return mustBeNumber();
  }

  static satisfies<T>(predicate: (value: T) => boolean | ValidationErrors, message = 'Invalid value'): ValidatorFn {
    return control => {
      const value = control.value as T;
      const result = predicate(value);

      if (result === true || isNil(result)) {
        return null;
      } else if (result) {
        return result;
      } else {
        return {
          'satisfies': message
        };
      }
    }
  }

  static asyncMustNotContainValue<T = string>(valueFn: () => (T[] | Observable<T[]>), message = 'This value already exists'): AsyncValidatorFn {
    const source = () => {
      const values_or_observable = valueFn();
      return Array.isArray(values_or_observable) ? of(values_or_observable) : values_or_observable;
    };

    return control => source().pipe(
      map(values => values.includes(control.value)),
      map(isFound => !isFound ? null : {mustNotContainValue: message}),
      take(1)
    );
  }

  static withErrorMessage(validator: ValidatorFn, message: string): ValidatorFn {
    return control => {
      const result = validator(control);
      if (result) {
        return { withErrorMessage: message };
      } else {
        return null;
      }
    };
  }
}
