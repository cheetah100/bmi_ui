import { OnDestroy, isDevMode, Directive } from '@angular/core';

import { ControlValueAccessor, Validator, ValidationErrors, AbstractControl } from '@angular/forms';


type ChangeCallback<T> = (value: T) => void;
type Callback = () => void;


/**
 * A helper base class for form controls.
 *
 * This implements the ControlValueAccessor interface with typed methods for
 * convenience, and handles the callbacks for you.
 *
 * The only method that needs to be provided is the writeValue() method. This is
 * called when the form model is setting the value on the control.
 *
 * When this control has changed, it can call the onChanged(newvalue) method to
 * pass the value back to the change callback (if registered).
 *
 * To support building complex nested form components, it's useful to have a
 * component register itself as a validator. If validation is desired, a
 * callback can be provided to the constructor which will perform custom
 * validation behaviour and return null, or a ValidationErrors object like a
 * regular Angular validator function.
 *
 * In order for validation to actually be performed, the component must be
 * provided as an NG_VALIDATORS, but the provideAsValidatingCVA() macro  will
 * do this for you.
 *
 * This base class helps work around a bug in Angular when providing a component
 * as its own validator: if the component is destroyed, then a new instance
 * attached, the old validator will still be active on the FormControl model.
 * These zombie validators are attached to a previous life of the form
 * component, and will not reflect the current state. If they were previously
 * invalid, they will continue to return errors and make the control invalid.
 *
 * The workaround for this is to set a flag when the component is destroyed and
 * not validate after that (the Angular Forms design doesn't permit easily
 * removing a specific validator). This base class implements OnDestroy, and so
 * subclasses should call super.ngOnDestroy(), or preventFurtherValidation() when
 * overriding ngOnDestroy().
 */

// Adding a directive here because this is used as a base class. Needed for Ng10
@Directive()
export abstract class BaseControlValueAccessor<T> implements ControlValueAccessor, Validator, OnDestroy {
  private _onChangeCallback: ChangeCallback<T> = null;
  private _onTouchCallback: Callback = null;
  private _onValidatorChanged: Callback = null;
  private _hasBeenDestroyed = false;
  private _isWriteValueInProgress = false;
  private __deferredValidationTimeout: any = null;

  /**
   * Returns true if the writeValue() method is in progress.
   *
   * This can be used to mask emitted events during writeValue to prevent
   * unwanted side effects. If a complex nested form can't be adapted to prevent
   * value change events as a result of writing value, this could be used as a
   * filter before propagating the changes.
   */
  protected get isWriteValueInProgress() {
    return this._isWriteValueInProgress;
  }

  /**
   * Reflects whether the control should be disabled.
   */
  public isDisabled = false;

  public writeValue(value: T): void {
    if (this._hasBeenDestroyed) {
      return;
    }
    this._warnIfWriteValueCalledRecursively();
    this._isWriteValueInProgress = true;
    try {
      this.updateComponentValue(value);
    } finally {
      this._isWriteValueInProgress = false;
    }
  }

  abstract updateComponentValue(value: T): void;

  private _warnIfCalledDuringWriteValue(methodName: string, additionDebugInfo = {}) {
    if (isDevMode() && this._isWriteValueInProgress) {
      console.warn(`BaseControlValueAccessor: ${methodName} called during write value.\n\n`
        + `This can trigger Angular Forms bugs (as of 7.2.x) when re-binding a control/component, `
        + `because Angular calls writeValue() before setting up the change/touch callbacks. \n\n`
        + `The cleanUpControl() code will set these callbacks to throw errors if called, but `
        + `most of the time these conditions will not be met and the issue can go unnoticed.\n\n`
        + `Calling change callbacks during writeValue means that setting the value of a form control `
        + `has side effects -- setting the value also changes the value, which is a dangerous `
        + `idea anyway. If you set the value of a textbox, you don't expect that to suddenly generate `
        + `a change event with a different value.\n\n\n`

        + `It's best to add {emitEvent: false} when updating sub-form controls during writeValue to `
        + `control the unwanted propagation of these events.`,
        {
          methodName: methodName,
          valueAccessor: this,
          ...additionDebugInfo
        }
      );
    }
  }

  private _warnIfWriteValueCalledRecursively() {
    if (isDevMode() && this._isWriteValueInProgress) {
      console.error('BaseControlValueAccessor: writeValue() called during writeValue() call. This is likely a bug.')
    }
  }

  preventFurtherValidation() {
    if (isDevMode()) {
      console.warn('preventFurtherValidation() is deprecated. Please use cleanup() instead')
    }
  }

  /**
   * Performs some cleanup and ensures that the callbacks will no longer be
   * called.
   */
  protected cleanup() {
    this._hasBeenDestroyed = true;
    if (this.__deferredValidationTimeout) {
      clearTimeout(this.__deferredValidationTimeout);
      this.__deferredValidationTimeout = null;
    }
  }

  ngOnDestroy() {
    this.cleanup();
  }




  /**
   * A template method to check this component instance for validity.
   *
   * Using this is preferable to overriding validate() as the base class'
   * implementation of validate() will prevent validation after the component
   * has been destroyed. Allowing further validation can cause confusion when
   * the validation is based on internal state, and there are zombie validators
   * due to form components being re-created.
   *
   * There are two ways to perform validation:
   *
   * 1)  you can look at the value in the `control` argument passed to this
   *     method. This is the external form model that this control is bound to
   *     -- this is the traditional validation approach.
   *
   * 2) If this component encapsulates a more complex form to produce a value,
   *    you may wish to use that form's validity instead. This generally seems
   *    to work fine and prevents repetition of validation logic, but this is a
   *    somewhat experimental use case -- care should be taken with component
   *    lifetimes as the self-validators in this class will prevent validation
   *    after the component has been destroyed.
   *
   * @param control the external form control model bound to this component.
   */
  validateSelf?(control?: AbstractControl): ValidationErrors;

  validate(control: AbstractControl): ValidationErrors {
    if (!this.validateSelf || this._hasBeenDestroyed) {
      return null;
    } else {
      return this.validateSelf(control);
    }
  }

  /**
   * Triggers revalidation on this form control.
   *
   * This relies on this value accessor also being registered as a Validator for
   * the control, and a validation callback being set.
   *
   * Currents this calls the validation function synchronously, but it
   */
  protected needsRevalidation() {
    if (this._hasBeenDestroyed) {
      return;
    }

    this._warnIfCalledDuringWriteValue('needsRevalidation()');
    if (this._onValidatorChanged) {
      this._onValidatorChanged();
    }
  }

  /**
   * Call to propagate a value change to the form model.
   */
  protected onChanged(value: T) {
    if (this._hasBeenDestroyed) {
      return;
    }

    this._warnIfCalledDuringWriteValue('onChanged()', {value});
    if (this._onChangeCallback) {
      this._onChangeCallback(value);
    }
  }

  /**
   * Call to propagate a touch up to the parent control.
   */
  protected onTouched() {
    if (this._hasBeenDestroyed) {
      return;
    }

    this._warnIfCalledDuringWriteValue('onTouched()');
    if (this._onTouchCallback) {
      this._onTouchCallback();
    }
  }

  registerOnChange(cb: ChangeCallback<T>) {
    this._onChangeCallback = cb;
  }

  registerOnTouched(cb: Callback) {
    this._onTouchCallback = cb;
  }

  registerOnValidatorChange(cb: Callback) {
    if (cb && !this.validateSelf) {
      throw new Error(
        `BaseControlValueAccessor: component is registered as a validator, yet doesn't do any self-validation\n\n`

        + `Most form controls should not perform any validation. In Reactive Forms, these validators should be placed `
        + `on the FormControl in the parent component.\n\n`

        + `If self-validation is not necessary for this control, adjust the providers in the Component to provide it`
        + `as an NG_VALUE_ACCESSOR, but not an NG_VALIDATORS (i.e. use the 'provideAsControlValueAccessor' helper)\n\n`

        + `The BaseControlValueAccessor has some workarounds for Angular Forms bugs regarding validator lifecycle and `
        + `uses the registerOnValidatorChange() hook as a signal that the CVA has been bound to a FormControl object. `
        + `This hook will schedule a task to revalidate the control, but a side effect of this is the revalidation will `
        + `cause the valueChange to re-emit, which can have unintended consequences.\n\n`

        + `Apologies if this broke your code, but tracking down a bug related to this took most of a day.`
      );
    }

    this._onValidatorChanged = cb;

    if (!this._hasBeenDestroyed && !this.__deferredValidationTimeout) {
      this.__deferredValidationTimeout = setTimeout(() => {
        this.__deferredValidationTimeout = null;
        this.needsRevalidation();
      });
    }
  }

  setDisabledState(isDisabled: boolean) {
      this.isDisabled = isDisabled;
  }
}
