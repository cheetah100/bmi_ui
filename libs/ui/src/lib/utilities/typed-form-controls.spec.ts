import { FormControl, FormArray, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';

import { TemplatedFormArray } from './templated-form-array';
import { TypedFormGroup, TypedFormArray, TypedFormControl } from './typed-form-controls';


interface TestValueType {
  array: string[];
  test: string;
  nested: {
    name: string;
    id: number;
  }[];
}

// Fully testing this is a little tricky because a lot of it relies on
// Typescript type inference. Instead, this should test that the types mostly
// work in positive cases, and that the behaviours of the Angular form controls
// haven't been accidentally broken.

describe('TypedFormControls', () => {
  describe('TypedFormGroup', () => {
    let form: TypedFormGroup<TestValueType>;

    beforeEach(() => {
      form = new TypedFormGroup<TestValueType>({
        test: new TypedFormControl('foobar'),
        array: new FormArray([new FormControl('x')]),
        nested: new TemplatedFormArray(x => new FormGroup({
          id: new TypedFormControl(x.id),
          name: new TypedFormControl(x.name)
        }))
      });
    });

    it('should produce a value that is assignable to the group type', () => {
      const value: TestValueType = form.value;
      expect(value.test).toBe('foobar');
    });

    it('should have a well-typed valueChanges observable', () => {
      const observable: Observable<TestValueType> = form.valueChanges;
      observable.subscribe(value => {
        const formValue: TestValueType = value;
        expect(formValue).toBeTruthy();
      });
    });

    it('should set the value when setValue is called', () => {
      form.setValue({
        test: 'bob',
        array: ['a'],
        nested: [
          {name: 'John', id: 123}
        ]
      }, {emitEvent: false});

      expect(form.value.test).toBe('bob');
      expect(form.value.array).toEqual(['a']);
      expect(form.value.nested[0].name).toBe('John');
    });

    it('should accept a subset of the type when patchValue is called', () => {
      form.patchValue({
        array: ['abc']
      });

      expect(form.value.array).toEqual(['abc']);
    });
  });
});
