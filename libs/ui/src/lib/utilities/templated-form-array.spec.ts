import { TemplatedFormArray } from './templated-form-array';
import { FormControl } from '@angular/forms';


describe('TemplatedFormArray', () => {
  describe('Monkey Patched @angular/forms behaviour', () => {
    // These cases mostly cover the 'monkey patched' behaviours added to the
    // push() and removeAt() methods.
    //
    // These methods had to be replaced because they always caused a value to be
    // emitted when the form control is updated, but this lead to event spam and
    // broken behaviours when doing programmatic updates.
    //
    // This fix was a last-ditch effort to fix these stupid behaviours. It's a
    // trivial change and it should be made upstream but Angular's forms are not
    // open for extension and the bodge relies on private implementation details
    // that could change in later versions.

    let formArray: TemplatedFormArray<string>;
    let emittedValues: string[][];

    beforeEach(() => {
      formArray = new TemplatedFormArray<string>(v => new FormControl(v));
      emittedValues = [];
      formArray.valueChanges.subscribe(value => emittedValues.push(value));
    });

    it('should not emit immediately', () =>  expect(emittedValues).toEqual([]));

    describe('patchValue(..., {emitEvent: false})', () => {
      beforeEach(() => formArray.patchValue(['foo', 'bar'], {emitEvent: false}));

      it('should update the value of the form array', () => {
        expect(formArray.value).toEqual(['foo', 'bar']);
      });

      it('should not emit any change events', () => {
        expect(emittedValues).toEqual([]);
      });
    });

    describe('patchValue(...)', () => {
      beforeEach(() => formArray.patchValue(['foo', 'bar']));

      it('should update the value', () => expect(formArray.value).toEqual(['foo', 'bar']));
      it('should emit a single change event', () => expect(emittedValues).toEqual([['foo', 'bar']]));
    });


    describe('push()', () => {
      it('should add a control to the form array', () => {
        const controlA = new FormControl('foo');
        const controlB = new FormControl('bar');
        formArray.push(controlA);
        formArray.push(controlB);
        expect(formArray.controls).toEqual([controlA, controlB]);
      });

      it('should emit an event when adding the control', () => {
        formArray.push(new FormControl('foo'));
        expect(emittedValues).toEqual([['foo']]);
      });

      it('should not emit an event if {emitEvent: false} is passed', () => {
        formArray.push(new FormControl('foo'), {emitEvent: false});
        expect(emittedValues).toEqual([]);
      });
    });

    describe('pushValue()', () => {
      it('should add a control', () => {
        formArray.pushValue('foo');
        expect(formArray.value).toEqual(['foo']);
      });

      it('should emit an event', () => {
        formArray.pushValue('foo');
        formArray.pushValue('bar');
        expect(emittedValues).toEqual([['foo'], ['foo', 'bar']]);
      });

      it('should not emit an event if {emitEvent: false} is passed', () => {
        formArray.pushValue('foo', {emitEvent: false});
        formArray.pushValue('bar', {emitEvent: false});
        expect(emittedValues).toEqual([]);
      });
    });
  });
});
