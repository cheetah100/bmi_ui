import { isValidNumber } from './number-validator';

describe('isValidNumber', () => {
  // This is the core functionality of the number validator. The actual form
  // control validator itself does a further test to pass 'blank' values so that
  // the validator can be used for optional numeric values too; combine with a
  // must be non-blank or required validator.

  // Doing some parameterized testing here. The first value here is whether the
  // case should be accepted or rejected, and it can accept an array of values
  // to test in a single case.
  const TEST_CASES: [boolean, string, any | any[]][] = [
    [true, 'natural numbers', ['37', '133332299', '0']],
    [true, 'negative integers', ['-34', '-12', '-1000']],
    [true, 'positive signs in front of integers', '+3'],
    [true, 'decimal numbers', ['3.14159', '0.123456789']],
    [true, 'decimal numbers with signs', ['+3.14', '-12.000000000001']],
    [true, 'decimal numbers with nothing before decimal point', ['.1', '.944423']],
    [true, 'signed decimal numbers with nothing before decimal point', ['+.5', '-.1']],
    [true, 'leading or trailing whitespace', [' 3.4', '100 ', '  .34']],
    [true, 'exponent notation', ['3e14', '3E14', '3.5e+20', '-2e-28', '2.44e-122', '.1e-10']],

    [false, 'decimal numbers with nothing after decimal point', ['13.', '.', '+3.', '-.']],
    [false, 'multiple decimal points in number', ['13.1333.433', '.333.21', '..4']],
    [false, 'non-digit characters in number', ['12.a4', 'A13', 'aseuth']],

    [false, 'NaN as a number', NaN],
    [false, 'NaN as a string', 'NaN'],
    [false, 'Infinity as a number', [Infinity, -Infinity]],
    [false, 'Infinity as a string', ['Infinity', '-Infinity']],

    [false, 'hexadecimal values', ['0xaf34', '0x3.22893', '-0x2', '0x3.3p2', '0x3.2p-1']],

    [false, 'comma separators in numbers', ['12,034,944', '1,2']],
    [false, 'whitespace in string', ['10 000', '1. 299',]],
    [false, 'empty strings', '',],
    [false, 'whitespace strings', ['   ', '\t', '\n']],
    [false, 'signs without numbers', ['+', '-']],
    [false, 'multiple signs', ['--3', '++', '12++']],
    [false, 'null values', null],
    [false, 'undefined values', undefined],
    [false, 'array values', [ [1, 2]]],
    [false, 'objects', [{a: 'foobar'}]],
    [false, 'functions', () => 4],
    [false, 'booleans', [true, false]],

  ]

  for (const [expected, specDescription, testValue] of TEST_CASES) {
    it(`should ${expected ? 'accept' : 'reject'} ${specDescription}`, () => {
      const testValues = Array.isArray(testValue) ? testValue : [testValue];
      for (const v of testValues) {
        expect(isValidNumber(v)).toBe(expected);
      }
    });
  }
});
