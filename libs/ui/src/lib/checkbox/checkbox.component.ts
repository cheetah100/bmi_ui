import {
  ControlValueAccessor,
  NG_VALUE_ACCESSOR
} from '@angular/forms';
import {
  Component,
  forwardRef,
  Output,
  Input,
  EventEmitter,
  Renderer2,
  ElementRef
} from '@angular/core';

@Component({
  selector: 'ui-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: [ './checkbox.component.scss' ],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CheckboxComponent),
      multi: true
    }
  ]
})
export class CheckboxComponent implements ControlValueAccessor {
  @Input() public checked: boolean = false;
  @Input('checkedColor') checkedColorClassName: string = null;
  @Input() disabled: boolean = false;
  @Input() label: string = null;
  @Output() clicked = new EventEmitter();

  constructor(
    private renderer: Renderer2,
    private elementRef: ElementRef
  ) {}

  toggleChecked() {
    if (!this.disabled) {
      this.checked = !this.checked;
      this.propagateChange(this.checked);
      this.clicked.emit(this.checked);
    }
  }

  writeValue(value: boolean) {
    this.checked = value;
  }

  propagateChange = (_: any) => {};

  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  propagateTouch = () => {};

  registerOnTouched(fn) {
    this.propagateTouch = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.renderer.setProperty(
      this.elementRef.nativeElement,
      'disabled',
      isDisabled
    );
  }

}
