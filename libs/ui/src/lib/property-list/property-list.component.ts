import { Component, Input, OnChanges } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ObjectMap, distinctUntilNotEqual } from '@bmi/utils';
import sortBy from 'lodash-es/sortBy';


export interface PropertyListItem {
  key: string;
  value: any;
}


interface PropertyListItemState extends PropertyListItem {
  lastChangedTimestamp?: number;
}


@Component({
  selector: 'ui-property-list',
  template: `
    <div *ngFor="let entry of entries | async; trackBy: trackItem" class="ui-property-list__entry">
      <div class="ui-property-list__key">{{entry.key}}</div>
      <div class="ui-property-list__value">{{entry.value}}</div>
    </div>
  `,

  styleUrls: ['./property-list.component.scss']
})
export class PropertyListComponent {
  @Input() data: PropertyListItem[] | ObjectMap<any> = undefined;
  @Input() sortKeys = false;

  itemsSubject = new BehaviorSubject<PropertyListItem[]>([]);

  entries = this.itemsSubject.asObservable();

  ngOnChanges() {
    if (Array.isArray(this.data)) {
      this.itemsSubject.next(this.data)
    } else if (this.data) {
      const items = Object.entries(this.data).map(kv => ({key: kv[0], value: kv[1]}));
      this.itemsSubject.next(this.sortKeys ? sortBy(items,[entry => entry.key]) : items);
    } else {
      this.itemsSubject.next([]);
    }
  }

  trackItem(index: number, item: PropertyListItem) {
    return item.key;
  }
}
