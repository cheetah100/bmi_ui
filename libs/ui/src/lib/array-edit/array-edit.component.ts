import { Component, Input, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';


type ChangeCallbackFn = (value: any) => void;
type CallbackFn = () => void;


@Component({
  selector: 'ui-array-edit',
  templateUrl: './array-edit.component.html',
  styleUrls: ['./array-edit.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ArrayEditComponent),
      multi: true
    }
  ]
})
export class ArrayEditComponent implements ControlValueAccessor {
  private onChanged: ChangeCallbackFn = null;
  private onTouched: CallbackFn = null;

  isDisabled = false;

  /**
   * The current array of values in the control.
   */
  values: string[] = [];

  valueToAdd = '';

  constructor() {}

  writeValue(value: any) {
    if (!value) {
      this.values = [];
    } else if (Array.isArray(value)) {
      // Ensure the values are all strings. This
      this.values = value.map(x => '' + x);
    } else {
      // We *could* add a split behaviour here (e.g. commas, pipes whatever) but
      // it might just end up being unexpected and confusing.
      throw new Error('ArrayEdit control: Tried to edit a non-array value');
    }
  }

  addCurrentValue() {
    this.addValue(this.valueToAdd);
    this.valueToAdd = '';
  }

  removeEntry(index: number) {
    this.values.splice(index, 1);
    this.onValueChanged();
  }

  addValue(value: string) {
    this.values.push(value);
    this.onValueChanged();
  }

  onValueChanged() {
    if (this.onChanged) {
      this.onChanged([...this.values]);
    }
  }

  dropdownToggled() {
    if (this.onTouched) {
      this.onTouched();
    }
  }

  textboxKeyDown($event: KeyboardEvent) {
    if ($event.key === 'Enter' && this.valueToAdd) {
      this.addCurrentValue();
    }
  }

  registerOnChange(callback: ChangeCallbackFn) {
    this.onChanged = callback;
  }

  registerOnTouched(callback: CallbackFn) {
    this.onTouched = callback;
  }

  setDisabledState(isDisabled: boolean) {
    this.isDisabled = isDisabled;
  }
}
