import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CollapsibleSectionComponent } from './collapsible-section.component';

@NgModule({
  imports: [CommonModule],
  declarations: [CollapsibleSectionComponent],
  exports: [CollapsibleSectionComponent]
})
export class BmiUiCollapsibleSectionModule {}
