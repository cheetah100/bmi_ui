import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ui-collapsible-section',
  templateUrl: './collapsible-section.component.html',
  styleUrls: ['./collapsible-section.component.scss']
})
export class CollapsibleSectionComponent {
  @Input() isOpen = false;
  @Input() initiallyOpen = false;
  @Input() type: 'fancy' | 'minimal' = 'minimal';
  @Output() isOpenChange = new EventEmitter<boolean>();

  @Input() label = '<Section Title>';

  ngOnInit() {
    if (this.initiallyOpen) {
      this.open();
    }
  }

  private setIsOpen(state: boolean) {
    if (this.isOpen !== state) {
      this.isOpen = state;
      this.isOpenChange.emit(state);
    }
  }

  open() {
    this.setIsOpen(true);
  }

  close() {
    this.setIsOpen(false);
  }

  toggle() {
    this.setIsOpen(!this.isOpen);
  }
}
