import { isHexColor } from './colors';

describe('isHexColor', () => {
  const TEST_CASES: [string, boolean, any[]][] = [
    ['empty strings', false, ['']],
    ['null values', false, [null]],
    ['undefined values', false, [undefined]],
    ['numbers', false, [44, -2, 0, 100]],

    ['#rgb', true, ['#eee', '#000', '#012', '#345', '#678', '#9AB', '#CDE', '#FFF', '#9ab', '#cde', '#fff']],
    ['#rgba', true, ['#0000', '#AAAF', '#beef', '#9999', '#0123', '#4567', '#89AB', '#CDEF', '#89ab', '#cdef']],
    ['#rrggbbaa', true, ['#01234567', '#89ABCDEF', '#89abcdef', '#00000000']],
    ['#rrggbb', true, ['#012345', '#6789AB', '#CDEFFF', '#6789ab', '#cdefff']],

    ['leading whitespace', false, [' #aaffee', ' #000']],
    ['trailing whitespace', false, ['#aaffee ', '#000 ']],
    ['hex notation with wrong numbers of digits', false, ['#12345', '#12', '#1234567', '#123456789']],
    ['hash sign by itself', false, ['#']],
    ['whitespace in hex numbers', false, ['#a bc', '#abc def']],
    ['hex numbers with no hash', false, ['abc', 'deadbeef', '123Abc']]
  ];

  for (const [description, expectedResult, testValues] of TEST_CASES) {
    it(`should return ${expectedResult} for ${description}`, () => {
      for (const value of testValues) {
        expect(isHexColor(value)).toBe(expectedResult, `Test value: '${value}'`);
      }
    });
  }
});
