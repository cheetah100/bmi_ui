import { Component, Input, OnInit, OnChanges, SimpleChanges, forwardRef, Optional, Inject } from '@angular/core';
import {  } from '@bmi/utils';
import { TypedFormControl } from '../utilities/typed-form-controls';
import { TemplatedFormArray } from '../utilities/templated-form-array';
import { BaseControlValueAccessor } from '../utilities/base-control-value-accessor';
import { provideAsControlValueAccessor } from '../utilities/form-control-macros';

import { DynamicEditorDefinitionService, ACTIVE_DYNAMIC_EDITOR_SERVICE, DynamicEditorDefinition } from '../dynamic-editor/dynamic-editor-definition';


@Component({
  selector: 'ui-dynamic-editor-list',
  templateUrl: './dynamic-editor-list.component.html',
  styles: [`
    ui-dropdown-menu > .btn--link {
      padding-left: 5px;
    }
  `],
  providers: [
    provideAsControlValueAccessor(forwardRef(() => DynamicEditorListComponent))
  ]
})
export class DynamicEditorListComponent<T = any> extends BaseControlValueAccessor<T[]> implements OnInit {
  constructor(
    @Optional() @Inject(ACTIVE_DYNAMIC_EDITOR_SERVICE) private injectedDefService: DynamicEditorDefinitionService<T>
  ){ super(); }

  /**
   * Optional: override the editor def service to use
   */
  // tslint:disable-next-line: no-input-rename
  @Input('defService') defServiceInput: DynamicEditorDefinitionService<T> = undefined;
  @Input() showAddMenu = true;
  @Input() addMenuLabel = 'Add';
  @Input() collapsible = false;

  formArray = new TemplatedFormArray<T>(() => new TypedFormControl<T>());

  // If we have collapsible items, when a new item is added, we want the section
  // to be open by default. To do this, we can keep track of the newly added
  // form control instances (which should be the 'key' for the ngFor anyway). As
  // this state is 'local' there could be issues in contexts where the form is
  // destroyed + recreated on changes, but that's going to cause broaded
  // usability issues than this.
  //
  // Using a weak set here to ensure it doesn't block GC, but that probably
  // doesn't matter over the lifetime of this component.
  private newlyAddedControls = new WeakSet<TypedFormControl<T>>();

  get defService() {
    return this.defServiceInput || this.injectedDefService;
  }

  ngOnInit() {
    this.formArray.valueChanges.subscribe(values => this.onChanged(values));
  }

  updateComponentValue(values: T[]) {
    this.formArray.patchValue(values || [], {emitEvent: false});
  }

  addItem(config: T) {
    this.formArray.pushValue(config);
    this.newlyAddedControls.add((this.formArray.controls[this.formArray.controls.length - 1]) as TypedFormControl<T>);
  }

  shouldBeInitiallyOpen(control: TypedFormControl<T>) {
    return this.newlyAddedControls.has(control);
  }
}
