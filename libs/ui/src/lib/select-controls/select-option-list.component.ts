import { Component, Input, Output, EventEmitter, ViewChild, ElementRef, AfterViewInit, OnDestroy, OnChanges } from '@angular/core';
import { Option, OptionGroup } from './option';
import { fromEvent, Subscription } from 'rxjs';
import { map, distinctUntilChanged, debounceTime } from 'rxjs/operators';


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ui-select-option-list',
  templateUrl: 'select-option-list.component.html',
  styleUrls: ['./select-option-list.component.scss'],
})
export class SelectOptionListComponent implements AfterViewInit, OnChanges, OnDestroy {
  @Input() selectedOption: Option;
  @Input() optionGroups: OptionGroup[];

  @Input() showSearch = true;

  @Output() optionSelected = new EventEmitter<Option>();
  @Output() newSearch = new EventEmitter<string>();

  groupCollapsedState = new Map<string,boolean>();

  @ViewChild('searchBox') searchBox: ElementRef;

  private subscription: Subscription;
  searchTerm = '';

  // This tracks the first group that a given value exists inside so that
  // selection will only be shown on the first occurence.
  private firstOptionGroupForValue = new Map<string, OptionGroup>();

  ngOnChanges() {
    this.updateFirstOptionGroupMap();
  }

  ngAfterViewInit() {
    this.newSearch.emit('');
    if (this.showSearch) {

      this.subscription = fromEvent(this.searchBox.nativeElement, 'keyup')
      .pipe(
        map((ev: KeyboardEvent) => {
          const input = ev.target as HTMLInputElement;
          return input.value;
        }),
        distinctUntilChanged(),
        debounceTime(250),
      )
      .subscribe((term: string) => {
        const searchTerm = term.trim();
        this.searchTerm = searchTerm;
        this.newSearch.emit(searchTerm);
      });
    }
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  focus() {
    if (this.searchBox && this.searchBox.nativeElement) {
      const searchBox = this.searchBox.nativeElement as HTMLInputElement;
      searchBox.focus();
    }
  }

  optionClicked(option: Option) {
    this.optionSelected.emit(option);
  }

  isGroupCollapsed(group: OptionGroup) {
    const groupKey = group.id || group.title;
    if (!group.collapsible || !groupKey) {
      return false;
    } else if (this.groupCollapsedState.has(groupKey)) {
      return this.groupCollapsedState.get(groupKey);
    } else {
      return !!group.collapsedByDefault;
    }
  }

  toggleGroupCollapsed(group: OptionGroup) {
    if (group.collapsible) {
      const key = group.id || group.title;
      this.groupCollapsedState.set(key, !this.isGroupCollapsed(group));
    }
  }

  trackOption(index: number, option: Option) {
    return option ? option.value || option.text : option;
  }

  trackOptionGroup(index: number, group: OptionGroup) {
    return group ? group.id || group.title || group : group;
  }

  isSelected(option: Option, optionGroup: OptionGroup): boolean {
    return this.selectedOption?.value === option.value
      && this.shouldSelectThisInstance(option.value, optionGroup);
  }

  private shouldSelectThisInstance(value: string, optionGroup: OptionGroup) {
    // It looks glitchy if the same option exists in a list multiple times, it
    // is selected, and both are visible at the same time. To address this, we
    // can just highlight the first instance of an option. There's an edge case
    // here where that group might be collapsed, so in that case, all instances
    // will get highlighted.
    const firstGroup = this.firstOptionGroupForValue.get(value);
    return !firstGroup || firstGroup === optionGroup || this.isGroupCollapsed(firstGroup);
  }

  private updateFirstOptionGroupMap() {
    this.firstOptionGroupForValue.clear();
    const firstOptionGroupForValue = this.firstOptionGroupForValue;
    for (const group of this.optionGroups ?? []) {
      for (const option of group.options) {
        if (!firstOptionGroupForValue.has(option.value)) {
          firstOptionGroupForValue.set(option.value, group);
        }
      }
    }
  }
}
