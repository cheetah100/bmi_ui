export interface Option {
  value: string;
  text: string;
  hidden?: boolean;
}

export interface OptionGroup {
  id?: string;
  title: string;
  options: Option[];

  collapsible?: boolean;
  collapsedByDefault?: boolean;
}