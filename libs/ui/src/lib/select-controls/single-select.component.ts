import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import {
  AfterContentInit,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  forwardRef,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Inject,
  Optional,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { CloseDialogService, CLOSE_DIALOG_SERVICE_TOKEN } from '../close-dialog.service';
import { provideAsControlValueAccessor } from '../utilities/form-control-macros';
import { Option } from './option';


@Component({
  templateUrl: './single-select.component.html',
  styleUrls: ['./select-shared.scss', './single-select.component.scss'],
  // tslint:disable-next-line:component-selector
  selector: 'ui-old-single-select, idp-single-select',
  providers: [provideAsControlValueAccessor(forwardRef(() => SingleSelectComponent))],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SingleSelectComponent implements ControlValueAccessor, OnInit, OnChanges, AfterContentInit, OnDestroy {

  // TODO: we should stop using the locked field and use the disabled property that is now supported
  // by angular 2 forms.

  public open = false;
  public search = '';

  /**
   * This is the 'source of truth' in the control.
   *
   * This is set by the writeValue() method of the CVA protocol, and is used to
   * look up (or generate) the selected option from.
   *
   * Previously, the selectedOption variable was the source of truth here but
   * that led to race conditions if the list of options was asynchronous -- if
   * the list was empty, the control would potentially just lose the input,
   * although there was a hack in place that would setTimeout() if the options
   * was null.
   *
   * To set this variable, use the updateSelectedValue() method within the
   * component, or rely on the control value accessor protocol, as this will
   * ensure the selectedOption is set correctly too.
   */
  private selectedValue: string;

  public selectedOption: Option;
  public disabled: boolean;
  private closeSubscription: Subscription = null;
  private opening = false;

  @Input() options: Option[];
  @Input() locked: boolean;

  /**
   *  changed from hideNone. Default behaviour is now to not show 'None' added.
   * you'll need to opt in by [showNone]="true"
   **/
  @Input() showNone = false;
  @Output() newValue: EventEmitter<any> = new EventEmitter<any>();
  @Input() theme: 'default' | 'blue' = 'default';
  @Input() defaultSelectedOption: any;

  /**
   * If true, this will be a "floating" select, dropping down over the top of
   * content below.
   */
  @Input() floating = false;

  private needsFilterUpdate = true;
  private _filteredData: Option[] = [];

  get filteredData() {
    if (this.needsFilterUpdate) {
      this._filteredData = this.applyFilters();
      this.needsFilterUpdate = false;
    }
    return this._filteredData;
  }


  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    @Optional() @Inject(CLOSE_DIALOG_SERVICE_TOKEN) private closeDialogService: CloseDialogService
  ) { }

  ngOnInit() {
    if (this.closeDialogService) {
      this.closeSubscription = this.closeDialogService.onClose.subscribe(() => {
        // Only close floating dropdowns. This behaviour doesn't suit the inline
        // style of dropdowns that grow their container.
        if (!this.opening && this.floating) {
          this.close();
          this.changeDetectorRef.markForCheck();
        }
      });
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.options) {
      this.needsFilterUpdate = true;
      this.updateSelectedOption(this.selectedValue);
    }
  }

  ngAfterContentInit() {
    if (this.defaultSelectedOption && this.defaultSelectedOption.value) {
      this.selectOption(this.defaultSelectedOption);
    }
  }

  ngOnDestroy() {
    if (this.closeSubscription) {
      this.closeSubscription.unsubscribe();
    }
  }

  applyFilters() {
    let options = this.options || [];
    if (this.search) {
      const searchString = this.search.toLowerCase();
      options = options.filter(opt => opt.text && opt.text.toLowerCase().indexOf(searchString) > -1);
    }
    return options;
  }

  triggerFilterUpdate() {
    this.needsFilterUpdate = true;
    this.changeDetectorRef.markForCheck();
  }

  selectOption(option: Option) {
    this.selectValue(option ? option.value : null);
  }

  selectValue(value: string) {
    this.close();
    this.updateSelectedOption(value);
    this.propagateChange(this.selectedValue);
    this.newValue.emit(this.selectedOption);
    this.triggerFilterUpdate();
  }

  toggleOpen() {
    this.touched();
    if (this.open) {
      this.close();
    } else {
      if (!this.locked && !this.disabled) {
        this.triggerFilterUpdate();
        this.open = true;
        this.opening = true;
        setTimeout(() => this.opening = false, 50);
      }
    }
  }

  openControl() {
    this.touched();
    this.open = true;
  }

  close() {
    this.open = false;
    this.opening = false;
    this.search = '';
  }

  writeValue(value: string) {
    this.updateSelectedOption(value);
    this.changeDetectorRef.markForCheck();
  }

  /**
   * Updates the selected option to match the specified value.
   * @param value
   */
  private updateSelectedOption(value: string) {
    this.selectedValue = value;
    // This behaviour of allowing options or strings must be deprecated and
    // removed! It adds pointless complexity to these components.
    this.selectedOption = this.findOptionByValue(value);
    this.changeDetectorRef.markForCheck();
  }

  /**
   * Finds the corresponding select option by value.
   *
   * If the select option isn't found, then this method may return a placeholder
   * one.
   *
   * Null or undefined values are handled specially here. If there is a matching
   * option, this will be returned, otherwise null will be returned for a null
   * or undefined option.
   *
   * If the value is not null or undefined, then a placeholder Option will be
   * returned. This is a new object created each time this function is called,
   * so object identity isn't preserved.
   *
   * @param value
   */
  findOptionByValue(value: string): Option | null {
    const option = this.options ? this.options.find(v => v.value === value || v.text === value) : null;
    if (option) {
      return option;
    } else if (value === null || value === undefined) {
      return null;
    } else {
      return {
        text: '' + value,
        value: value,
      };
    }
  }

  propagateChange = (_: any) => { };

  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  propagateTouch = () => { };

  registerOnTouched(fn) {
    this.propagateTouch = fn;
  }

  touched() {
    if (this.propagateTouch) {
      this.propagateTouch();
    }
  }

  blockClickEvent(event) {
    event.stopPropagation();
  }

  trackOptionBy(index: number, option: Option) {
    // Track the option by its value, so that even if the object identity
    // changes we can match it up with the DOM node. This was necessary, as some
    // components bind a list of options that are new objects each time change
    // detection is run. This lead to intermittent behaviour where items in the
    // list could not be selected.
    return option.value;
  }

  setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
    if (this.disabled) {
      this.open = false;
    }
  }

  clearSelection() {
    this.touched();
    this.close();
    this.selectValue(null);
  }
}
