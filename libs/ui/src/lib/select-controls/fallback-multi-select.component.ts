import { Component, forwardRef, Input, OnInit, OnDestroy, EventEmitter } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor, FormControl, ValidatorFn, AsyncValidatorFn } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Option } from './option';


/**
 * This little utility class helps us proxy through the touched status.
 */
class TouchedWatchFormControl extends FormControl {
  public onTouched = new EventEmitter<boolean>();

  constructor(
    formState: any = null,
    validator: ValidatorFn | ValidatorFn[] = null,
    asyncValidator: AsyncValidatorFn | AsyncValidatorFn[] = null
  ) {
    super(formState, validator, asyncValidator);
  }

  markAsTouched(options: { onlySelf?: boolean } = {}) {
    super.markAsTouched(options);
    this.onTouched.emit(true);
  }

  markAsUntouched(options: {onlySelf?: boolean}) {
    super.markAsUntouched(options);
    this.onTouched.emit(false);
  }
}



/**
 * Displays either a dropdown multiselect if options are available, or an array
 * select if not.
 *
 * Gravity conditions support multiple values by using | separated strings,
 * but this presents a UI problem. Ideally we'd like to show multi-selects if
 * possible, but there's a chance this data won't be available and we'd
 * still like the field to be editable.
 *
 * This little component will either display a regular text input, or a
 * multiselect if a list of options is available.
 */
@Component({
  selector: 'ui-fallback-multi-select',
  template: `
    <ui-array-edit *ngIf="!shouldShowAsMultiSelect" [formControl]="multiSelectControl"></ui-array-edit>
    <ui-multi-select *ngIf="shouldShowAsMultiSelect" [formControl]="multiSelectControl" [options]="options" theme="form-compact"></ui-multi-select>
  `,
  styleUrls: [ './fallback-multi-select.component.scss' ],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => FallbackMultiSelectComponent),
      multi: true
    }
  ],
})
export class FallbackMultiSelectComponent implements ControlValueAccessor, OnInit, OnDestroy {
  @Input() options: Option[];

  multiSelectControl = new TouchedWatchFormControl();
  subscriptions: Subscription[] = [];

  ngOnInit() {
    this.subscriptions = [
      this.multiSelectControl.valueChanges.subscribe(value => {
        if (this.propagateChange) {
          this.propagateChange(value);
        }
      }),

      this.multiSelectControl.onTouched.subscribe(value => {
        if (this.propagateTouch) {
          this.propagateTouch();
        }
      })
    ];
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  get shouldShowAsMultiSelect() {
    return !!this.options && this.options.length > 0;
  }

  writeValue(value: any) {
    console.log(value);
    this.multiSelectControl.setValue(value || [], {emitEvent: false});
  }

  propagateChange = (_: any) => {};

  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  propagateTouch = () => {};

  registerOnTouched(fn) {
    this.propagateTouch = fn;
  }

  setDisabledState(isDisabled: boolean) {
    if (isDisabled) {
      this.multiSelectControl.disable();
    } else {
      this.multiSelectControl.enable();
    }
  }
}


