import { SimpleChange } from '@angular/core';
import { TestBed, ComponentFixture, tick, fakeAsync, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SingleSelectComponent } from './single-select.component';
import { CLOSE_DIALOG_SERVICE_TOKEN, SimpleCloseDialogService } from '../close-dialog.service';
import { HighlightTermPipe } from '../pipes/highlight-term.pipe';
import { Option } from './option';


describe('SingleSelectComponent', () => {
  let fixture: ComponentFixture<SingleSelectComponent>;
  let component: SingleSelectComponent;

  let dialogService: SimpleCloseDialogService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule],
      declarations: [
        HighlightTermPipe,
        SingleSelectComponent,
      ],
      providers: [
        SimpleCloseDialogService,
        {provide: CLOSE_DIALOG_SERVICE_TOKEN, useExisting: SimpleCloseDialogService}
      ]
    }).compileComponents();

    dialogService = TestBed.get(SimpleCloseDialogService) as SimpleCloseDialogService;
    fixture = TestBed.createComponent(SingleSelectComponent);
    component = fixture.componentInstance;
  });

  afterEach(() => fixture.destroy());

  const options: Option[] = [
    {text: 'Test', value: 'test'},
    {text: 'Foobar', value: 'foobar'},
    {text: 'Bats', value: 'bats'}
  ];

  describe('With options set and initialized', () => {
    beforeEach(() => {
      component.options = [...options];
      fixture.detectChanges();
    });

    it('should initially be closed', () => {
      expect(component.open).toBe(false);
    });

    it('should initially have a null selected option', () => {
      expect(component.selectedOption).toBeFalsy();
    });

    describe('.writeValue()  [Angular Forms API]', () => {
      it('should select an existing option by value', () => {
        component.writeValue('test');
        expect(component.selectedOption).toBe(options[0]);
      });

      describe('Missing options', () => {
        function setOptions(options: Option[]) {
          const existingOptions = component.options;
          component.options = options;

          // The component relies on change detection, but it won't detect that
          // the options have changed and call ngOnChanges, so we do that
          // manually
          component.ngOnChanges({
            options: new SimpleChange(existingOptions, options, false)
          });
          fixture.detectChanges();
        }

        it('should create a fake option entry if the value is not found', () => {
          component.writeValue('flibbertyjibberty');
          expect(component.selectedOption.text).toEqual('flibbertyjibberty')
          expect(component.selectedOption.value).toEqual('flibbertyjibberty');
        });

        it('should update to the real entry when the options are updated', () => {
          component.writeValue('foo');
          const newOptions = [
            {text: 'Foo', value: 'foo'},
            ...options,
          ];

          setOptions(newOptions);

          expect(component.selectedOption.text).toEqual('Foo');
          expect(component.selectedOption.value).toEqual('foo');
        });

        it('should handle a null options array', () => {
          setOptions(null);
          component.writeValue('foo');
          expect(component.selectedOption.value).toBe('foo');
        })
      })


      it('should select an existing option by its text (even though this behaviour is stupid)', () => {
        component.writeValue('Test');
        expect(component.selectedOption).toBe(options[0]);
      });

      it('should not trigger a change propagation', () => {
        let changeCount = 0;
        component.registerOnChange(value => {
          changeCount += 1;
        });

        component.writeValue('bats');
        expect(changeCount).toBe(0);
      });
    });

    describe('selectOption()', () => {
      it('should close the select after an item is chosen', fakeAsync(() => {
        component.openControl();
        tick(500);
        component.selectOption(options[2]);
        expect(component.open).toBe(false);
      }));

      it('should set the option as the selected option', () => {
        component.selectOption(options[2]);
        expect(component.selectedOption).toBe(options[2]);
      });

      it('should call the onChange callback (part of Forms API)', () => {
        let changeCount = 0;
        let writtenValue: any;
        component.registerOnChange(value => {
          changeCount += 1;
          writtenValue = value;
        });

        component.selectOption(options[2]);

        expect(changeCount).toBe(1);
        expect(writtenValue).toBe(options[2].value);
      });
    });

    describe('search', () => {
      function doSearch(term: string) {
        // The component should expose a nicer API for doing this...
        component.search = term;
        component.triggerFilterUpdate();
        fixture.detectChanges();
      }

      it('should default to a blank search string', () => {
        expect(component.search).toBe('');
      });

      it('should show all items by default', () => {
        expect(component.filteredData).toEqual(options);
      });

      it('it should search the item Text for a substring', () => {
        doSearch('Test');
        expect(component.filteredData).toEqual([options[0]]);
      });

      it('should be case insensitive', () => {
        doSearch('ba');
        expect(component.filteredData).toEqual([options[1], options[2]]);
      });

      it('should reset to showing the full list after clearing the search string', () => {
        doSearch('bats');
        expect(component.filteredData).toEqual([options[2]]);
        doSearch('');
        expect(component.filteredData).toEqual(options);
      });
    });
  });

  describe('defaultSelectedOption', () => {
    it('should select the given option if set before the component initializes', waitForAsync(() => {
      component.options = [...options];
      component.defaultSelectedOption = options[2];
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(component.selectedOption).toBe(options[2]);
      });
    }));
  });
});
