import { SimpleChange } from '@angular/core';
import { TestBed, ComponentFixture, tick, fakeAsync, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TabbedSelectComponent, OptionsTab } from './tabbed-select.component';
import { SelectOptionListComponent } from './select-option-list.component';
import { SelectTogglerComponent } from './select-toggler.component';
import { BaseDropdownComponent } from '../base-dropdown/base-dropdown.component';
import { CLOSE_DIALOG_SERVICE_TOKEN, SimpleCloseDialogService } from '../close-dialog.service';
import { HighlightTermPipe } from '../pipes/highlight-term.pipe';
import { Option } from './option';
import { SelectComponent } from './select/select.component';
import { IconComponent } from '../icon/icon.component';


describe('TabbedSelectComponent', () => {
  let fixture: ComponentFixture<TabbedSelectComponent>;
  let component: TabbedSelectComponent;

  let dialogService: SimpleCloseDialogService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule],
      declarations: [
        HighlightTermPipe,
        TabbedSelectComponent,
        SelectComponent,
        SelectOptionListComponent,
        IconComponent,
        BaseDropdownComponent,
        SelectTogglerComponent,
      ],
      providers: [
        SimpleCloseDialogService,
        {provide: CLOSE_DIALOG_SERVICE_TOKEN, useExisting: SimpleCloseDialogService}
      ]
    }).compileComponents();

    dialogService = TestBed.get(SimpleCloseDialogService) as SimpleCloseDialogService;
    fixture = TestBed.createComponent(TabbedSelectComponent);
    component = fixture.componentInstance;
  });

  afterEach(() => fixture.destroy());

  const options: OptionsTab[] = [
    {
      name: 'Metasyntactic Variables',
      options: [
        {value: 'foo', text: 'Foo'},
        {value: 'bar', text: 'Bar'},
        {value: 'blargh', text: 'Blargh'},
      ]
    },
    {
      name: 'Tab 2',
      options: [
        {value: 'huey', text: 'Huey'},
        {value: 'dewey', text: 'Dewey'},
        {value: 'louie', text: 'Louie'},
      ]
    }
  ];

  describe('With options set', () => {
    beforeEach(() => {
      component.tabs = options;
      fixture.detectChanges();
    });

    it('should initially have a null selected option', () => {
      expect(component.selectedOption).toBeFalsy();
    });

    it('should pick the first tab as active', () => {
      expect(component.activeTab).toBe(options[0]);
    });

    describe('writeValue()', () => {
      it('should change tabs when selecting a value', () => {
        component.writeValue('dewey');
        expect(component.activeTab).toBe(options[1]);
      });

      it('should set the selectedValue', () => {
        component.writeValue('dewey');
        expect(component.selectedValue).toBe('dewey');
      });

      it('should set the correct selectedOption', () => {
        component.writeValue('dewey');
        expect(component.selectedOption).toBe(options[1].options.find(o => o.value === 'dewey'));
      });

      describe('selectOption()', () => {
        it('should select the corresponding value', () => {
          component.selectOption(options[0].options[2]);
          expect(component.selectedOption).toBe(options[0].options[2]);
        });

        it('should treat a null option as if it is a value of null', () => {
          component.writeValue('louie');
          component.selectOption(null);
          expect(component.selectedOption).toBe(null);
          expect(component.selectedValue).toBe(null);
        });
      });

      describe('Missing options', () => {

         it('should still set the selected value', () => {
           component.writeValue('daffy');
           expect(component.selectedValue).toBe('daffy');
         });

         it('should create a fake option object', () => {
           component.writeValue('daffy');
           expect(component.selectedOption).toBeDefined();
           expect(component.selectedOption.value).toBe('daffy');
           expect(component.selectedOption.text).toBe('daffy');
         });
      });
    });

  });
});
