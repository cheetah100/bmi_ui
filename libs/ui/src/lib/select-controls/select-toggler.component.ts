import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';


@Component({
  selector: 'ui-select-toggler',
  templateUrl: './select-toggler.component.html',
  styleUrls: ['./select-toggler.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectTogglerComponent {
  @Input() isOpen = false;
  @Output() toggleOpen = new EventEmitter();

  constructor() {}
}
