import {
  Component,
  Input,
  forwardRef,
  OnChanges,
  ViewChild,
  Output,
  EventEmitter
} from '@angular/core';
import { Option } from '@bmi/utils';
import { BaseControlValueAccessor } from '../../utilities/base-control-value-accessor';
import { provideAsControlValueAccessor } from '../../utilities/form-control-macros';
import {
  FuzzySearcher,
  getPreference,
  setUsedItemPreference,
  PreferenceItem,
  UsedValueItem
} from '@bmi/utils';
import { OptionGroup } from '../option';
import flatten from 'lodash-es/flatten';
import { SelectOptionListComponent } from '../select-option-list.component';
import ms from 'ms';
import sortBy from 'lodash-es/sortBy';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ui-select, ui-single-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
  providers: [provideAsControlValueAccessor(forwardRef(() => SelectComponent))]
})
export class SelectComponent extends BaseControlValueAccessor<string>
  implements OnChanges {
  @Input() options: Option[] = [];
  @Input() optionGroups: OptionGroup[] = undefined;

  @Input() placeholder = '-';
  @Input() displayLimit = 500;
  @Input() simpleModeLimit = 20;

  /**
   * Setting a value here will enable persistence of user choices for this id.
   * Selections will be saved to local storage.
   * And a list of most recently used items, if saved in local storage, will show
   * in the suggested options section of the dropdown.
   * ID can be specific to this select instance, or to the data that it holds.
   * Which might be more useful if the user is likely to have preferred options
   * across different views. Like always choosing 'Gravity Workflow Automation'
   * from applications.
   * 'home-page-user-form-select-4' vs 'board-application'
   */
  @Input() preferenceId: string = null;

  /** @deprecated  Added for compatibility with existing select controls */
  @Input() floating = true;

  /** @deprecated  Added for compatibility with existing select controls */
  @Input() defaultSelectedOption = null;

  @Output() newValue = new EventEmitter();
  @ViewChild(SelectOptionListComponent)
  optionsComponent: SelectOptionListComponent;

  selectedValue: string = null;
  selectedOption: Option = null;
  isOpen = false;
  loadingOptions = true;
  displaySuggestedOptions = [];

  allOptions: Option[];
  allOptionGroups: OptionGroup[] = [];
  visibleOptionGroups: OptionGroup[] = [];

  suggestedOptions: OptionGroup = null;

  searchTerm = '';

  private suggestedItemsLimit = 6;
  private fuzzySearcher = new FuzzySearcher<Option>();

  get isSimpleMode() {
    return (
      !this.optionGroups &&
      (!this.options || this.options.length < this.simpleModeLimit)
    );
  }

  get groupedSearchResults() {
    return !!this.optionGroups && this.optionGroups.length > 1;
  }

  ngOnChanges(changes) {
    if (changes.options || changes.optionGroups) {
      this.updateOptionGroups();
      this.updateSelectedOption(this.selectedValue);
      this.loadingOptions = false;
    }
  }

  onDropdownToggled(isOpen: boolean) {
    if (isOpen) {
      setTimeout(() => {
        if (this.optionsComponent) {
          this.optionsComponent.focus();
        }
      }, 50);
    }
  }

  updateOptionGroups() {
    const simpleMode = this.isSimpleMode;
    const groups = this.optionGroups || [
      {
        title: simpleMode ? '' : 'All Items',
        options: this.options || [],
        collapsible: !simpleMode && this.options.length > this.displayLimit,
        collapsedByDefault: true
      }
    ];

    this.allOptions = flatten(groups.map(g => g.options));
    this.allOptionGroups = groups.filter(g => g.options && g.options.length);
    this.fuzzySearcher.items = this.allOptions;

    const recentlyUsed = this.getSuggestedOptions();
    if (!simpleMode && recentlyUsed.length > 0) {
      this.suggestedOptions = {
        title: 'Frequently Used',
        options: recentlyUsed
      };
    } else {
      this.suggestedOptions = null;
    }

    this.applySearchFilter();
  }

  getSuggestedOptions() {
    if (!!this.preferenceId) {
      const userPref: PreferenceItem = getPreference(this.preferenceId);

      // There are two ways that an item will be suggested here -- if it's been
      // used very recently (e.g. in the last hour), OR if you've used it a few
      // times in the past. The historical use cutoff will prune out stuff
      // that's no longer likely to be interesting.
      const now = new Date().getTime();
      const veryRecentCutoff = now - ms('1 hour');
      const historicalUseCutoff = now - ms('90 days');
      const minHistoricalUses = 3;

      const filteredEntries = Object.entries(userPref)
        .filter(([key, value]) => value.lastUsed > veryRecentCutoff
          || value.counter >= minHistoricalUses && value.lastUsed > historicalUseCutoff);

      const topOptions = filteredEntries.sort((a, b) => a[1].lastUsed < b[1].lastUsed ? 1 : -1)
        .map(([key]) => this.allOptions.find((o: Option) => o.value === key))
        .filter(Boolean)
        .slice(0, this.suggestedItemsLimit);

      return sortBy(topOptions, o => o.text);
    } else {
      return [];
    }
  }

  applySearchFilter() {
    if (this.searchTerm) {
      const results = this.fuzzySearcher.search(this.searchTerm, 'text');
      if (this.groupedSearchResults) {
        this.visibleOptionGroups = this.optionGroups
          .map(group => ({
            ...group,
            collapsedByDefault: false,
            options: results.filter(r => group.options.includes(r))
          }))
          .filter(g => g.options.length > 0);
      } else {
        this.visibleOptionGroups = [
          {
            title: 'Search Results',
            options: results
          }
        ];
      }
    } else {
      this.visibleOptionGroups = [
        this.suggestedOptions,
        ...this.allOptionGroups
      ].filter(g => !!g);
    }
  }

  doSearch(searchTerm: string): void {
    this.searchTerm = searchTerm.trim();
    this.applySearchFilter();
  }

  selectOption(option: Option): void {
    this.selectValue(option ? option.value : null);
  }

  updateComponentValue(value: string): void {
    this.selectedValue = value;
    this.updateSelectedOption(value);
  }

  private selectValue(value: string): void {
    this.selectedValue = value;
    this.onChanged(value);
    this.updateSelectedOption(value);
    if (this.preferenceId) {
      setUsedItemPreference(this.preferenceId, value);
      this.updateOptionGroups();
    }
    this.newValue.emit(value);
  }

  private updateSelectedOption(value: string): void {
    this.selectedOption = this.allOptions?.find(option => option.value === value) ?? null;
  }
}
