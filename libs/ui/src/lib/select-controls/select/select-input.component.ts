import {
  Component,
  Input,
  forwardRef,
  OnChanges,
  ViewChild, OnInit
} from '@angular/core';
import { Option } from '@bmi/utils';
import { BaseControlValueAccessor } from '../../utilities/base-control-value-accessor';
import { provideAsControlValueAccessor } from '../../utilities/form-control-macros';
import {
  FuzzySearcher,
  getPreference,
  setUsedItemPreference,
  PreferenceItem,
  UsedValueItem
} from '@bmi/utils';
import { OptionGroup } from '../option';
import flatten from 'lodash-es/flatten';
import { SelectOptionListComponent } from '../select-option-list.component';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'bmi-ui-select-input',
  templateUrl: './select-input.component.html',
  styleUrls: ['./select.component.scss', './select-input.component.scss'],
  providers: [
    provideAsControlValueAccessor(forwardRef(() => SelectInputComponent))
  ]
})
export class SelectInputComponent extends BaseControlValueAccessor<string>
  implements OnChanges, OnInit {
  @Input() options: Option[] = [];
  @Input() optionGroups: OptionGroup[] = undefined;
  @Input() displayLimit = 500;
  @Input() simpleModeLimit = 20;
  @Input() preferenceId: string = null;
  @Input() placeholder = '';

  @ViewChild(SelectOptionListComponent)
  optionsComponent: SelectOptionListComponent;

  selectedValue: string = null;
  selectedOption: Option = null;
  isOpen = false;
  loadingOptions = true;
  displaySuggestedOptions = [];

  allOptions: Option[];
  allOptionGroups: OptionGroup[] = [];
  visibleOptionGroups: OptionGroup[] = [];

  suggestedOptions: OptionGroup = null;

  searchTerm = '';
  inputValue: FormControl = new FormControl('');
  hasOptions = false;

  private suggestedItemsLimit = 6;
  private fuzzySearcher = new FuzzySearcher<Option>();

  get isSimpleMode() {
    return (
      !this.optionGroups &&
      (!this.options || this.options.length < this.simpleModeLimit)
    );
  }

  get groupedSearchResults() {
    return !!this.optionGroups && this.optionGroups.length > 1;
  }

  ngOnChanges(changes) {
    if (changes.options || changes.optionGroups) {
      this.updateOptionGroups();
      this.updateSelectedOption(this.selectedValue);
      if (this.allOptionGroups) {
        this.loadingOptions = false;
      }
    }
  }

  ngOnInit() {
    this.inputValue.valueChanges.subscribe( val => {
      this.onChanged(val);
    })
  }

  onDropdownToggled(isOpen: boolean) {
    if (isOpen) {
      setTimeout(() => {
        if (this.optionsComponent) {
          this.optionsComponent.focus();
        }
      }, 50);
    }
  }

  updateOptionGroups() {
    const simpleMode = this.isSimpleMode;
    const groups = this.optionGroups || [
      {
        title: simpleMode ? '' : 'All Items',
        options: this.options || [],
        collapsible: !simpleMode && this.options.length > this.displayLimit,
        collapsedByDefault: true
      }
    ];

    this.allOptions = flatten(groups.map(g => g.options));
    this.allOptionGroups = groups.filter(g => g.options && g.options.length);
    this.fuzzySearcher.items = this.allOptions;
    this.hasOptions = !!this.allOptions.length;

    const recentlyUsed = this.getSuggestedOptions();
    if (!simpleMode && recentlyUsed.length > 0) {
      this.suggestedOptions = {
        title: 'Recently Used',
        options: recentlyUsed
      };
    } else {
      this.suggestedOptions = null;
    }

    this.applySearchFilter();
  }

  getSuggestedOptions() {
    if (!!this.preferenceId) {
      const userPref: PreferenceItem = getPreference(this.preferenceId);
      return Object.entries(userPref)
        .filter(([key, value]: [string, UsedValueItem]) => value.lastUsed)
        .sort((a: [string, UsedValueItem], b: [string, UsedValueItem]) =>
          a[1].lastUsed < b[1].lastUsed ? 1 : -1
        )
        .map(([key]) => this.allOptions.find((o: Option) => o.value === key))
        .filter(Boolean)
        .slice(0, this.suggestedItemsLimit);
    } else {
      return [];
    }
  }

  applySearchFilter() {
    if (this.searchTerm) {
      const results = this.fuzzySearcher.search(this.searchTerm, 'text');
      if (this.groupedSearchResults) {
        this.visibleOptionGroups = this.optionGroups
          .map(group => ({
            ...group,
            collapsedByDefault: false,
            options: results.filter(r => group.options.includes(r))
          }))
          .filter(g => g.options.length > 0);
      } else {
        this.visibleOptionGroups = [
          {
            title: 'Search Results',
            options: results
          }
        ];
      }
    } else {
      this.visibleOptionGroups = [
        this.suggestedOptions,
        ...this.allOptionGroups
      ].filter(g => !!g);
    }
  }

  doSearch(searchTerm: string): void {
    this.searchTerm = searchTerm.trim();
    this.applySearchFilter();
  }

  selectInputValue(): void {
    this.selectValue(this.inputValue.value);
  }

  selectOption(option: Option): void {
    this.selectValue(option ? option.value : null);
  }

  updateComponentValue(value: string): void {
    this.selectedValue = value;
    this.updateSelectedOption(value);
  }

  private selectValue(value: string): void {
    this.selectedValue = value;
    this.onChanged(value);
    this.updateSelectedOption(value);
    if (this.preferenceId) {
      setUsedItemPreference(this.preferenceId, value);
      this.updateOptionGroups();
    }
  }

  private updateSelectedOption(value: string): void {
    this.selectedOption = this.findOptionByValue(value);
    if (!this.selectedOption) {
      this.inputValue.setValue(value);
    }
  }

  private findOptionByValue(value: string): Option {
    if (this.allOptions) {
      return (
        this.allOptions.find((option: Option) => option.value === value) || null
      );
    }
    return null;
  }
}
