import { Component, Input, forwardRef, OnChanges, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild } from '@angular/core';
import { Option } from '@bmi/utils';
import { BaseControlValueAccessor } from '../../utilities/base-control-value-accessor';
import { provideAsControlValueAccessor } from '../../utilities/form-control-macros';
import {
  FuzzySearcher,
  getPreference,
  setUsedItemPreference,
  PreferenceItem,
  UsedValueItem,
} from '@bmi/utils';
import { OptionGroup } from '../option';
import flatten from 'lodash-es/flatten';
import { SelectOptionListComponent } from '../select-option-list.component';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ui-multi-select, idp-multi-select',
  templateUrl: './multi-select.component.html',
  styleUrls: ['./multi-select.component.scss'],
  providers: [
    provideAsControlValueAccessor(forwardRef(() => MultiSelectComponent)),
  ],
})
export class MultiSelectComponent extends BaseControlValueAccessor<string[]> implements OnChanges {

  @Input() options: Option[] = [];
  @Input() optionGroups: OptionGroup[] = undefined;

  @Input() placeholder = '-';
  @Input() displayLimit = 500;
  @Input() simpleModeLimit = 20;

  /**
   * Setting a value here will enable persistence of user choices for this id.
   * Selections will be saved to local storage.
   * And a list of most recently used items, if saved in local storage, will show
   * in the suggested options section of the dropdown.
   * ID can be specific to this select instance, or to the data that it holds.
   * Which might be more useful if the user is likely to have preferred options
   * across different views. Like always choosing 'Gravity Workflow Automation'
   * from applications.
   * 'home-page-user-form-select-4' vs 'board-application'
   */
  @Input() preferenceId: string = null;

  /** @deprecated  Added for compatibility with existing select controls */
  @Input() floating = true;

  /** @deprecated  Added for compatibility with existing select controls */
  @Input() defaultSelectedOption = null;

  @ViewChild(SelectOptionListComponent) optionsComponent: SelectOptionListComponent;

  selectedValues: string[] = [];
  selectedOptions: Option[] = [];
  isOpen = false;
  loadingOptions = true;
  displaySuggestedOptions = [];

  allOptions: Option[];
  allOptionGroups: OptionGroup[] = [];
  visibleOptionGroups: OptionGroup[] = [];

  suggestedOptions: OptionGroup = null;

  searchTerm = '';

  private suggestedItemsLimit = 6;
  private fuzzySearcher = new FuzzySearcher<Option>();

  get isSimpleMode() {
    return !this.optionGroups && (!this.options || this.options.length < this.simpleModeLimit);
  }

  get groupedSearchResults() {
    return !!this.optionGroups && this.optionGroups.length > 1;
  }

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
  ) {
    super();
  }

  ngOnChanges(changes) {
    if (changes.options || changes.optionGroups) {
      this.updateOptionGroups();
      this.updateSelectedOptions(this.selectedValues);
      this.loadingOptions = false;
    }
  }

  onDropdownToggled(isOpen: boolean) {
    if (isOpen) {
      setTimeout(() => {
        if (this.optionsComponent) {
          this.optionsComponent.focus();
        }
      }, 50);
    }
  }

  toggle() {
    if (!this.isDisabled) {
      this.isOpen = !this.isOpen;
    }
  }

  updateOptionGroups() {
    const simpleMode = this.isSimpleMode;
    const groups = this.optionGroups || [
      {
        title: simpleMode ? '' : 'All Items',
        options: this.options || [],
        collapsible: !simpleMode && this.options.length > this.displayLimit,
        collapsedByDefault: true,
      }
    ];

    this.allOptions = flatten(groups.map(g => g.options));
    this.allOptionGroups = groups.filter(g => g.options && g.options.length);
    this.fuzzySearcher.items = this.allOptions;

    const recentlyUsed = this.getSuggestedOptions();
    if (!simpleMode && recentlyUsed.length > 0) {
      this.suggestedOptions = {
        title: 'Recently Used',
        options: recentlyUsed
      };
    } else {
      this.suggestedOptions = null;
    }

    this.applySearchFilter();
  }

  getSuggestedOptions() {
    if (!!this.preferenceId) {
      const userPref: PreferenceItem = getPreference(this.preferenceId);
      return Object.entries(userPref)
        .filter(([key, value]: [string, UsedValueItem]) => value.lastUsed)
        .sort((a: [string, UsedValueItem], b: [string, UsedValueItem]) => a[1].lastUsed < b[1].lastUsed ? 1 : -1)
        .map(([key,]) => this.allOptions.find((o: Option) => o.value === key))
        .filter(Boolean)
        .slice(0, this.suggestedItemsLimit);
    } else {
      return [];
    }
  }


  applySearchFilter() {
    if (this.searchTerm) {
      const results = this.fuzzySearcher.search(this.searchTerm, 'text');
      if (this.groupedSearchResults) {
        this.visibleOptionGroups = this.optionGroups.map(group => ({
          ...group,
          collapsedByDefault: false,
          options: results.filter(r => group.options.includes(r))
        })).filter(g => g.options.length > 0);
      } else {
        this.visibleOptionGroups = [
          {
            title: 'Search Results',
            options: results
          }
        ];
      }
    } else {
      this.visibleOptionGroups = [
        this.suggestedOptions,
        ...this.allOptionGroups
      ].filter(g => !!g);
    }
  }

  doSearch(searchTerm: string): void {
    this.searchTerm = searchTerm.trim();
    this.applySearchFilter();
  }

  selectOptions(options: Option[]): void {
    const values: string[] = options ? options.map(option => option.value) : [];
    this.selectValue(values);
  }

  updateComponentValue(values: string[]): void {
    this.selectedValues = values || [];
    this.updateSelectedOptions(this.selectedValues);
  }

  private selectValue(values: string[]): void {
    this.selectedValues = values;
    this.onChanged(values);
    this.updateSelectedOptions(values);
    if (this.preferenceId) {
      values.forEach(value => setUsedItemPreference(this.preferenceId, value));
      this.updateOptionGroups();
    }
  }

  private updateSelectedOptions(values: string[]): void {
    this.selectedOptions = [];
    values.forEach(value => {

      const option = this.findOptionByValue(value);
      if (option) {
        this.selectedOptions.push(option);
      }
    })
  }

  private findOptionByValue(value: string): Option {
    if (this.allOptions) {
      return this.allOptions.find((option: Option) => option.value === value) || null;
    }
    return null;
  }

}
