import { SimpleChange } from '@angular/core';
import { TestBed, ComponentFixture, tick, fakeAsync, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HighlightTermPipe } from '../../pipes/highlight-term.pipe';

import { Option } from '../option';
import { MultiSelectOptionListComponent } from './multi-select-option-list.component';

describe('MultiSelectOptionListComponent', () => {
  let fixture: ComponentFixture<MultiSelectOptionListComponent>;
  let component: MultiSelectOptionListComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule],
      declarations: [
        HighlightTermPipe,
        MultiSelectOptionListComponent,
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(MultiSelectOptionListComponent);
    component = fixture.componentInstance;
  });

  afterEach(() => fixture.destroy());

  beforeEach(() => {
    component.optionGroups = [{
      title: 'All Items',
      options: [
        { text: 'Test', value: 'test' },
        { text: 'Foobar', value: 'foobar' },
        { text: 'Bats', value: 'flyingmammal' }
      ]
    }];

    fixture.detectChanges();
  });

  // search functionality: moved to select.component
  // I'll need to add something like this there

  // describe('Search', () => {
  //   it('should not filter the results if the search query is empty', () => {
  //     component.search = '';
  //     component.applyFilters();
  //     expect(component.filteredData).toEqual(options);
  //   });

  //   it('should apply the search filter to the visible text', () => {
  //     component.search = 'a';
  //     component.applyFilters();
  //     expect(component.filteredData).toContain(options[1]);
  //     expect(component.filteredData).toContain(options[2]);
  //   });

  //   it('should be case insensitive', () => {
  //     component.search = 'bats';
  //     component.applyFilters();
  //     expect(component.filteredData).toEqual([
  //       options[2]
  //     ]);
  //   });
  // });

});
