import { Component, Input, Output, ChangeDetectionStrategy, EventEmitter, ViewChild, ElementRef, AfterViewInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { Option, OptionGroup } from '../option';
import { fromEvent, Subscription } from 'rxjs';
import { map, distinctUntilChanged, debounceTime } from 'rxjs/operators';


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ui-multi-select-option-list',
  templateUrl: 'multi-select-option-list.component.html',
  styleUrls: ['./multi-select-option-list.component.scss'],
  //changeDetection: ChangeDetectionStrategy.OnPush
})
export class MultiSelectOptionListComponent implements AfterViewInit, OnDestroy {
  @Input() selectedOptions: Option[];
  @Input() optionGroups: OptionGroup[];

  @Input() showSearch = true;

  @Output() optionsSelected = new EventEmitter<Option[]>();
  @Output() newSearch = new EventEmitter<string>();

  groupCollapsedState = new Map<string,boolean>();

  // public selectedOptions: Option[] = [];
  public fixedOptions: any = [];

  @ViewChild('searchBox') searchBox: ElementRef;
  public allSelected = false;
  private subscription: Subscription;
  searchTerm = '';

  constructor(
    private changeDetector: ChangeDetectorRef
  ) { }

  ngAfterViewInit() {
    this.newSearch.emit('');
    if (this.showSearch) {

      this.subscription = fromEvent(this.searchBox.nativeElement, 'keyup')
      .pipe(
        map((ev: KeyboardEvent) => {
          const input = ev.target as HTMLInputElement;
          return input.value;
        }),
        distinctUntilChanged(),
        debounceTime(250),
      )
      .subscribe((term: string) => {
        const searchTerm = term.trim();
        this.searchTerm = searchTerm;
        this.newSearch.emit(searchTerm);
        this.changeDetector.markForCheck();
      });
    }
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  focus() {
    if (this.searchBox && this.searchBox.nativeElement) {
      const searchBox = this.searchBox.nativeElement as HTMLInputElement;
      searchBox.focus();
      this.changeDetector.markForCheck();
    }
  }

  isSelected(option: Option): boolean {
    return (this.selectedOptions === null || !!(this.selectedOptions.find(
      (selected) => {
        return Object.is(selected, option) ||
          (selected.value && selected.value === option.value);
      }
    )));
  }

  isGroupCollapsed(group: OptionGroup) {
    const groupKey = group.id || group.title;
    if (!group.collapsible || !groupKey) {
      return false;
    } else if (this.groupCollapsedState.has(groupKey)) {
      return this.groupCollapsedState.get(groupKey);
    } else {
      return !!group.collapsedByDefault;
    }
  }

  toggleGroupCollapsed(group: OptionGroup) {
    if (group.collapsible) {
      const key = group.id || group.title;
      this.groupCollapsedState.set(key, !this.isGroupCollapsed(group));
      this.changeDetector.markForCheck();
    }
  }

  trackOption(index: number, option: Option) {
    return option ? option.value || option.text : option;
  }

  trackOptionGroup(index: number, group: OptionGroup) {
    return group ? group.id || group.title || group : group;
  }

  selectOption(option: Option) {
    if (!this.isSelectedOption(option)) {
      this.selectedOptions.push(option);
      // if (this.config.defaultAll && this.selectedOptions.length === this.fixedOptions.length) {
      //   this.selectedOptions = null;
      // }
    } else {
      this.removeOne(option);
    }
    this.optionsSelected.emit(this.selectedOptions);
  }

  isSelectedOption(option: Option) {
    return (this. selectedOptions === null || !!(this.selectedOptions.find(
      (selected) => {
        return Object.is(selected, option) ||
          (selected.value && selected.value === option.value);
      }
    )));
  }


  removeOne(option: Option) {
    const startOptions = this.selectedOptions === null ? [...this.fixedOptions] : [...this.selectedOptions];
    for (let s = 0, sl = startOptions.length; s < sl; s++) {
      if (Object.is(startOptions[s], option) || (startOptions[s].value && startOptions[s].value === option.value)) {
        startOptions.splice(s, 1);
        break;
      }
    }
    this.selectedOptions = startOptions;
    // this.makeChanges();
    // this.filter();
  }

}
