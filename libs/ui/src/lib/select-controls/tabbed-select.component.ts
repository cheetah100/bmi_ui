import { Component, EventEmitter, Input, forwardRef, Inject, Output, OnInit, OnChanges } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { Option, OptionGroup } from './option';
import { TypedFormControl } from '../utilities/typed-form-controls';


export interface OptionsTab {
  name: string;
  options: Option[];
}


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ui-tabbed-select',
  templateUrl: './tabbed-select.component.html',
  styleUrls: ['./tabbed-select.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => TabbedSelectComponent),
    multi: true
  }]
})
export class TabbedSelectComponent implements ControlValueAccessor, OnInit, OnChanges {
  @Input() tabs: OptionsTab[] = [];
  @Input() placeholder = 'Please select...';
  @Output() tabChanged = new EventEmitter<OptionsTab>();

  activeTab: OptionsTab = null;

  formControl = new TypedFormControl<string>(null);

  get selectedValue() {
    return this.formControl.value;
  }

  selectedOption: Option = null;
  optionGroups: OptionGroup[] = [];

  ngOnInit() {
    this.changeTab(this.activeTab);
    this.updateSelectedOption(this.selectedValue);
    this.formControl.valueChanges.subscribe(value => this.propagateChange(value));
  }

  ngOnChanges(changes) {
    if (changes.tabs) {
      this.optionGroups = (this.tabs || []).map((tab, tabIndex) => (<OptionGroup>{
        title: tab.name,
        options: tab.options,
        collapsedByDefault: tabIndex !== 0 || tab === this.activeTab,
        collapsible: true
      }));

      this.changeTab(this.activeTab);
      this.updateSelectedOption(this.selectedValue);
    }
  }

  changeTab(tab: OptionsTab) {
    this.activeTab = tab || this.tabs[0];

    if (this.activeTab) {
      this.tabChanged.emit(this.activeTab);
    }
  }

  selectOption(option: Option) {
    this.writeValue(option ? option.value : null);
  }

  writeValue(value: string) {
    this.formControl.setValue(value, {emitEvent: false});
    this.updateSelectedOption(value);
  }

  private updateSelectedOption(value: string) {
    const [option, tab] = this.findOptionByValue(value);
    this.selectedOption = option;
    this.changeTab(tab);
  }

  findOptionByValue(value: string): [Option, OptionsTab] {
    let option: Option = null;
    let tabToActivate: OptionsTab = null;
    if (this.tabs) {
      for (const tab of this.tabs) {
        const foundOption = tab.options.find(v => v.value === value);
        if (foundOption) {
          option = foundOption;
          tabToActivate = tab;
          break;
        }
      }
    }

    if (option) {
      return [option, tabToActivate];
    } else if (value === null || value === undefined) {
      return [null, null];
    } else {
      return [{text: '' + value, value: value}, tabToActivate];
    }
  }

  propagateChange = (value: any) => {};
  propagateTouch = () => {};

  registerOnChange(fn: (value: any) => void) {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: () => void) {
    this.propagateTouch = fn;
  }

  setDisabledState(isDisabled: boolean) {
    // TODO.
  }
}
