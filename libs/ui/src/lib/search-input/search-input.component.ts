import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';

@Component({
  selector: 'idp-search-input, ui-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.scss'],
})
export class SearchInputComponent implements OnChanges {

  @Input() search = '';
  @Input() selection = false;
  @Input() icon = 'fa-search';
  @Input() placeholder = 'New search';
  @Output() newSearch: EventEmitter<string> = new EventEmitter<string>();
  @ViewChild('searchbox') searchElement: ElementRef;

  ngOnChanges(changes: SimpleChanges) {
    if (changes.selection && this.searchElement?.nativeElement) {
      if (this.selection) {
        this.searchElement.nativeElement.focus();
      } else {
        this.searchElement.nativeElement.blur();
      }
    }
  }

  searchChanged() {
    this.newSearch.emit(this.search);
  }

}
