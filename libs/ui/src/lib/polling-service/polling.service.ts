import { Injectable, NgZone } from '@angular/core';
import { Observable, BehaviorSubject, merge, timer, defer, fromEvent, asyncScheduler } from 'rxjs';
import { filter, map, throttle, share, observeOn, tap } from 'rxjs/operators';

import ms from 'ms';


function now() {
  return new Date().getTime();
}


/**
 * Provides some timing utilities for polled operations.
 *
 * When doing polled operations, we want to ease back on the frequency if the
 * user isn't present at the computer. This service offers a method to create an
 * interval timer with a dynamic frequency -- when the user is present it will
 * run at the requested rate, but it will be throttled significantly if there is
 * no sign of activity on the document.
 */
@Injectable({providedIn: 'root'})
export class PollingService {

  /**
   * A 'graph' of minimum interval vs. idle time.
   *
   * This is a list of [idle time, interval] tuples. As the user has been idling
   * longer the interval between polling should increase. The idea here is that
   * we're okay with polling while the user is actively engaging with the app,
   * and that it should keep doing so if they had the window open, but were not
   * actively using it (for a while) -- e.g. if they were monitoring something
   * while working.
   *
   * This sets the 'minimum' polling frequency we want to see. The list should
   * be sorted, as it will be iterated with an Array.find to locate the first
   * entry where the idle time is greater than the time since recorded activity.
   */
  MINIMUM_POLLING_INTERVAL_VS_IDLE_TIME: [number, number][] = [
    [ms('1 minute'),      undefined],
    [ms('20 minutes'),    ms('30 seconds')],
    [ms('1 hour'),        ms('1 minute')],
    [ms('4 hours'),       ms('1 hour')],
    [ms('12 hours'),      ms('1 day')]
  ]

  /**
   * If the window isn't focused, this penalty will be added to the last
   * activity time when considering timer throttling
   */
  UNFOCUSED_DOCUMENT_PENALTY = ms('1 hour');

  /**
   * Timers younger than this will be allowed to run freely
   */
  public YOUNG_TIMER_AGE = ms('10 seconds');


  /**
   * The last time we have sign of user input. This could come from manually
   * calling poke() or from an automated subscription to a number of browser
   * events
   */
  private lastUserInput = new BehaviorSubject<number>(now());

  /**
   * This timer runs periodically to force reconsideration of any polling timers
   * that might be running.
   */
  private idleTimer = timer(0, ms('10 seconds')).pipe(share());


  /**
   * We want to monitor a bunch of browser events for signs of life. This ought
   * to be done outside the Angular zone to prevent change detection issues.
   * If we subscribed to these events with a HostListener or similar it would
   * cause a noisy level of change detection.
   */
  private userInputEvents = merge(
    fromEvent(window, 'click'),
    fromEvent(window, 'scroll'),
    fromEvent(window, 'mousemove'),
    fromEvent(window, 'keypress'),
    fromEvent(window.document, 'visibilitychange'),
  ).pipe(
    throttle(() => timer(ms('1 second'))),
    observeOn(asyncScheduler),
    // We poke the user input subject as a side-effect here. This observable is
    // multicast using share, so this means that subscriptions will get
    // automatically cleaned up.
    tap(() => this.poke()),
    share(),
  );


  intervalTimer(intervalMs: number): Observable<void> {
    return defer(() => {
      const startTime = now();
      let lastFiredTime: number = undefined;
      return merge(
        timer(0, intervalMs),
        this.idleTimer,
        this.lastUserInput,
        this.userInputEvents,
      ).pipe(
        // The input to this pipeline is a handful of events that might require
        // the timer to fire. We apply a filter to determine if the conditions
        // are right based on two main criteria:
        //
        //  1. Is the timer young? This means newly created timers get a
        //     chance to fire, regardless of user input status.
        //
        //  2. Has enough time passed since the timer last fired?
        filter(() => {
          const timerAge = now() - startTime;
          const timeSinceFired = now() - lastFiredTime;
          const minimumInterval = this.getMinimumIntervalBasedOnUserActivity();

          // Timers which have not fired should be given a chance, and 'young'
          // timers get a certain window where they can emit as often as needed.
          if (lastFiredTime === undefined || minimumInterval === undefined || timerAge < this.YOUNG_TIMER_AGE) {
            return true;
          } else {
            return timeSinceFired >= minimumInterval;
          }
        }),

        // We rely on this throttle to ensure events don't happen more often
        // than was intended.
        throttle(() => timer(intervalMs), {leading: false, trailing: true}),
        tap(() => lastFiredTime = now()),
        map(() => undefined)
      );
    });
  }

  /** 
   * Provides a one-shot timer of at least the desired timeout. This could be longer 
   * if the user is idle when the timer is started.
   */
  oneshotTimer(intervalMs: number): Observable<void> {
    return defer(() => timer(Math.max(intervalMs, this.getMinimumIntervalBasedOnUserActivity())).pipe(map(() => undefined)));
  }

  private getMinimumIntervalBasedOnUserActivity(): number {
    let timeSinceActivity = now() - this.lastUserInput.value;
    if (!this.isAppFocused) {
      // If the document isn't focused we won't completely block polling, but
      // will add a time penalty.
      timeSinceActivity += this.UNFOCUSED_DOCUMENT_PENALTY;
    }

    return graphLookup(timeSinceActivity, this.MINIMUM_POLLING_INTERVAL_VS_IDLE_TIME);
  }

  get isAppFocused() {
    return !window.document.visibilityState || window.document.visibilityState === 'visible';
  }

  poke() {
    this.lastUserInput.next(now());
  }
}


function graphLookup(x: number, graph: [number, number][]): number {
  return (graph.find(entry => x < entry[0]) || graph[graph.length - 1])[1];
}
