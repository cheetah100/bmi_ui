import {
  Component,
  forwardRef,
  Input,
  TemplateRef,
  OnInit,
  OnDestroy
} from '@angular/core';
import { provideAsControlValueAccessor } from '../utilities/form-control-macros';
import { TemplatedFormArray } from '../utilities/templated-form-array';
import {
  TypedFormControl,
  TypedFormGroup
} from '../utilities/typed-form-controls';
import { Subscription } from 'rxjs';
import { BaseControlValueAccessor } from '../utilities/base-control-value-accessor';
import { ObjectMap } from '@bmi/utils';
import { OptionGroup } from '../select-controls/option';

export interface MapItem {
  key: string;
  value: string;
}

@Component({
  selector: 'bmi-map-editor',
  template: `
    <ui-stacker-list [items]="formArray">
      <ng-template let-group>
        <div class="map-editor__grid">
          <bmi-ui-select-input
            [formControl]="group.controls.key"
            [optionGroups]="optionGroups"
          ></bmi-ui-select-input>
          <span class="icon-arrow-right-tail"></span>
          <input data-cy="map-editor-value-input" [formControl]="group.controls.value" />
        </div>
      </ng-template>
    </ui-stacker-list>
    <button type="button" (click)="addItem()" class="btn btn--small btn--link">
      <span class="icon-add"></span> Add
    </button>
  `,
  styleUrls: ['./map-editor.component.scss'],
  providers: [
    provideAsControlValueAccessor(forwardRef(() => MapEditorComponent))
  ]
})
export class MapEditorComponent
  extends BaseControlValueAccessor<ObjectMap<string>>
  implements OnInit, OnDestroy {
  @Input() controlTemplate: TemplateRef<any> = null;
  @Input() optionGroups: OptionGroup[];
  readonly formArray: TemplatedFormArray<MapItem> = new TemplatedFormArray<
    MapItem
  >(
    () =>
      new TypedFormGroup<MapItem>({
        key: new TypedFormControl<string>(null),
        value: new TypedFormControl<string>(null)
      })
  );
  private readonly subscription = new Subscription();

  ngOnInit() {
    this.subscription.add(
      this.formArray.valueChanges.subscribe((value: MapItem[]) => {
        this.onChanged(this.arrayToMap(value));
      })
    );
  }

  mapToArray(objMap: ObjectMap<string>): MapItem[] {
    if (!objMap) {
      return [];
    }
    return Object.entries(objMap).map(([key, value]: [string, string]) => ({
      key,
      value
    }));
  }

  arrayToMap(value: MapItem[]): ObjectMap<string> {
    const objMap: ObjectMap<string> = {};
    value
      .filter((item: MapItem) => !!item && item.key)
      .forEach((item: MapItem) => (objMap[item.key] = item.value));
    return objMap;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  updateComponentValue(value: ObjectMap<string>): void {
    this.formArray.patchValue(this.mapToArray(value) || [], {
      emitEvent: false
    });
  }

  addItem() {
    this.formArray.pushValue({ key: null, value: null });
  }
}
