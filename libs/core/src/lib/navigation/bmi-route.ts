import { ParameterBinding } from '../page-data-context/parameter-binding';

/**
 * Represents a route in the BMI app.
 *
 * This is a superset of the Angular routing behaviour (mostly for compatibility
 * with the existing SPC menu), but with additional object types for configuring
 * links within the BMI application.
 *
 *
 */
export type BmiRoute = string | any[] | BmiPageRoute | BmiBuiltinRoute | BmiResolvedRoute;

/**
 * A navigable route. This has had any dynamic data resolved out of it, so it's
 * ready for link generation.
 */
export type BmiResolvedRoute = string | any[] | BmiResolvedPageRoute | BmiBuiltinRoute;

/**
 * Represents a route to a page within BMI
 *
 * The filters can be data-bound values. In ordor to actually navigate, it is
 * necessary to "resolve" this into a BmiResolvedPageRoute (get rid of any
 * bindings)
 */
export interface BmiPageRoute {
  type: 'page';
  moduleId: string;
  pageId: string;
  filters?: { [key: string]: ParameterBinding };
}


/**
 * A resolved version of the page route. This has had any data bindings looked
 * up already, so the filters should be directly ready for URL generation.
 *
 * If available, the urlId will be the 'nice' page ID for the route. This can be
 * used in place of the page ID when generating links for the client app.
 */
export interface BmiResolvedPageRoute extends BmiPageRoute {
  urlId?: string;
  filters?: {[key: string]: string}
}

export interface BmiBuiltinRoute {
  type: 'builtin';

  // TODO: open to ideas for handling this nicely?
  routeId: string;
  moduleId?: string;
}


/**
 * Is this route a BmiPageRoute?
 *
 * This checks if the route is a BmiPageRoute object, NOT whether the URL refers
 * to a page through another means (e.g. a URL string)
 */
export function isPageRoute(route: BmiRoute): route is (BmiPageRoute | BmiResolvedPageRoute) {
  return route && typeof (route) === 'object' && !Array.isArray(route) && route.type === 'page';
}


export function isRawRouteCommands(route: BmiRoute): route is string | any[] {
  return route && (typeof route === 'string' || Array.isArray(route));
}
