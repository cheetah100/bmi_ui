import { Observable } from 'rxjs';
import { BmiRoute } from './bmi-route';
import { Binder } from '../page-data-context/binder';


export interface BmiRouteOptions {
  binder: Binder;
  moduleId?: string;
}


/**
 * A class interface for handling navigations and URL generation for a BmiRoute
 *
 * This service can have different implementations/subclasses to allow the
 * behaviour to be customized a bit.
 */
export abstract class BmiRouteHandler {

  /**
   * Returns a *cold* observable that will navigate to this page when subscribed.
   */
  abstract navigate(route: BmiRoute, options: BmiRouteOptions): Observable<boolean>;

  /**
   * Generates Angular router commands.
   *
   * This API is provided to help the router link directives, but it might make
   * more sense to just generate an href here, and rely on "navigate" to
   * actually perform the navigation. That would allow external links to be
   * handled properly -- something that the Angular Router cannot handle.
   */
  abstract generateRouteCommands(route: BmiRoute, options: BmiRouteOptions): Observable<any[]>;
}
