import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { BmiRoute, BmiPageRoute, isPageRoute, BmiResolvedRoute, BmiResolvedPageRoute } from '../navigation/bmi-route';
import { ParameterBinding } from '../page-data-context/parameter-binding';
import { Binder } from '../page-data-context/binder';
import { combineLatestObject, ObjectMap } from '@bmi/utils';

import mapValues from 'lodash-es/mapValues';


export function resolveRouteBindings<T extends BmiRoute>(route: T, dataContext?: Binder): Observable<BmiResolvedRoute> {
  if (!isPageRoute(route) || noFiltersNeedBinding(route)) {
    return of(route as BmiResolvedRoute);
  } else if (typeof route === 'object' && dataContext) {
    return resolveFilterBindings(route.filters, dataContext).pipe(
      map(filters => ({
        ...route as BmiPageRoute,
        filters: filters
      }))
    );
  } else {
    console.warn('Cannot resolve route bindings: has bindings but no data context is available', route);
    return of({
      ...route as BmiPageRoute,
      filters: {},
    });
  }
}


function noFiltersNeedBinding(route: BmiPageRoute): boolean {
  return !route || !route.filters || Object.values(route.filters).every(binding => !binding || typeof (binding) === 'string');
}


export function getRouteBindings(route: BmiRoute): ParameterBinding[] {
  if (!isPageRoute(route) || noFiltersNeedBinding(route)) {
    return [];
  } else {
    return Object.values(route.filters || {});
  }
}


export function resolveFilterBindings(filters: ObjectMap<ParameterBinding>, binder: Binder): Observable<ObjectMap<string>> {
  return combineLatestObject(
    mapValues(filters, binding => !!binder ? binder.bind(binding) : of(null))
  ).pipe(
    map(filters => mapValues(filters, x => String(x)))
  );
}
