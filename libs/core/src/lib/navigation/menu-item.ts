import { BmiLink } from './bmi-link';
import { BmiRoute } from './bmi-route';


export interface MenuLink extends BmiLink {
  title: string;
  icon?: string;
  route: BmiRoute;
}


export interface MenuGroup {
  title: string;
  icon?: string;
  views: MenuLink[];
}


export type MenuItem = MenuGroup | MenuLink;
