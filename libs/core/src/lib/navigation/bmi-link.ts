import { BmiRoute } from './bmi-route';


export interface BmiLink {
  title: string;
  icon?: string;
  route?: BmiRoute;
}
