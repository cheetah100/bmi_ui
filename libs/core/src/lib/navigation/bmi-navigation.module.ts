import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { BmiRouteLinkDirective } from './bmi-route-link/bmi-route-link.directive';
import { BmiRouteLinkWithHrefDirective } from './bmi-route-link/bmi-route-link-with-href.directive';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
  ],

  declarations: [
    BmiRouteLinkDirective,
    BmiRouteLinkWithHrefDirective,
  ],

  exports: [
    BmiRouteLinkDirective,
    BmiRouteLinkWithHrefDirective,
  ]
})
export class BmiNavigationModule {}
