import { Injectable, SkipSelf } from '@angular/core';
import { defer, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { ConfirmationModalService } from '@bmi/ui';
import { BmiRoute } from './bmi-route';
import { BmiRouteHandler, BmiRouteOptions } from './bmi-route-handler';
import { escapeMarkdown } from '@bmi/utils';

import dedent from 'ts-dedent';

@Injectable({providedIn: 'root'})
export class ConfirmBeforeNavigateRouteHandler extends BmiRouteHandler {
  constructor(
    private confirmationService: ConfirmationModalService,
    @SkipSelf() private parentHandler: BmiRouteHandler
  ) {
    super();
  }

  navigate(route: BmiRoute, options: BmiRouteOptions) {
    return defer(() =>
      this.confirmationService.show({
        content: dedent`
          ## Confirm Navigate?

          You're being navigated to a different page, but because you're
          currently in the Page Designer, this might not be desirable.

          <code><pre>${escapeMarkdown(JSON.stringify(route, undefined, 2))}</pre></code>
        `,
        buttonYesText: 'Navigate',
        buttonCancelText: 'Stay on this page',
        emitCancelEvent: true,
      }).pipe(
        switchMap(result => result && result.action === 'yes'
          ? this.parentHandler.navigate(route, options)
          : of(false))
      )
    );
  }

  generateRouteCommands(route: BmiRoute, options: BmiRouteOptions) {
    return this.parentHandler.generateRouteCommands(route, options);
  }
}
