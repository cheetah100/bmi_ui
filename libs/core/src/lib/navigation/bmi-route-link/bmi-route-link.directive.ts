import { Directive, Input, HostListener, Optional, OnInit, OnDestroy, OnChanges } from '@angular/core';
import { Router } from '@angular/router';

import { BehaviorSubject, Subscription, combineLatest, of } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';

import { BmiRoute } from '../bmi-route';
import { BmiRouteHandler } from '../bmi-route-handler';
import { RoutesService } from '../../services/routes.service';
import { PageInfoService } from '../../page/page-info.service';

import { WidgetContext } from '../../widget-context';
import { PageDataContext } from '../../page-data-context/page-data-context';


@Directive({
  selector: '[bmiRouteLink]:not(a)'
})
export class BmiRouteLinkDirective implements OnChanges, OnDestroy {
  // tslint:disable:no-input-rename
  @Input('bmiRouteLink') route: BmiRoute;
  @Input('bmiRouteEnableInEditMode') enableInEditMode: boolean = false;

  private routeSubject = new BehaviorSubject<BmiRoute>(null);
  private subscription = new Subscription();

  constructor (
    private router: Router,
    private routeHandler: BmiRouteHandler,
    @Optional() private pageInfoService: PageInfoService,
    @Optional() private pageDataContext: PageDataContext,
    @Optional() private widgetContext: WidgetContext,
  ) {}

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  ngOnChanges() {
    this.routeSubject.next(this.route);
  }

  get currentModuleId() {
    return this.pageInfoService ? this.pageInfoService.currentModuleId : null;
  }

  get shouldFollowLink() {
    return !this.isEditMode || this.enableInEditMode;
  }

  get isEditMode() {
    return this.widgetContext && this.widgetContext.isEditModeNow;
  }

  // tslint:disable-next-line:no-unsafe-any
  @HostListener('click', ['$event'])
  public onClick(event: MouseEvent) {
    if (this.route && this.shouldFollowLink) {
      event.preventDefault();
      this.subscription.add(
        this.routeHandler.navigate(this.route, {
          binder: this.pageDataContext,
          moduleId: this.currentModuleId,
        }).subscribe()
      );
    }
  }
}
