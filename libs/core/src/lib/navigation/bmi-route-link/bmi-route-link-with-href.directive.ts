import { Directive, Input, HostListener, HostBinding, Optional, OnInit, OnChanges, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { BehaviorSubject, Subscription, combineLatest, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { BmiRoute } from '../bmi-route';
import { BmiRouteHandler } from '../bmi-route-handler';
import { PageInfoService } from '../../page/page-info.service';
import { WidgetContext } from '../../widget-context';
import { PageDataContext } from '../../page-data-context/page-data-context';

/**
 * This directive is almost identical to the regular bmiRouteLink directive, but
 * targets <a> elements directly so that it can set the href attribute
 */
@Directive({
  selector: 'a[bmiRouteLink]'
})
export class BmiRouteLinkWithHrefDirective implements OnChanges {
  // tslint:disable-next-line:no-input-rename
  @Input('bmiRouteLink') route: BmiRoute;

  // tslint:disable-next-line:no-input-rename
  @Input('bmiRouteEnableInEditMode') enableInEditMode: boolean = false;

  @HostBinding('href') url: string = '#';
  @Input() @HostBinding('attr.target') target: string;

  private routeSubject = new BehaviorSubject<BmiRoute>(null);
  private subscription = new Subscription();
  private routeCommands: any[] = null;
  private isEditMode = false;

  constructor (
    private router: Router,
    private routeHandler: BmiRouteHandler,
    @Optional() private pageInfoService: PageInfoService,
    @Optional() private pageDataContext: PageDataContext,
    @Optional() private widgetContext: WidgetContext,
  ) {}

  ngOnInit() {
    this.subscription.add(combineLatest([
      this.routeSubject.pipe(
        switchMap(route => this.routeHandler.generateRouteCommands(route, {
          binder: this.pageDataContext,
          moduleId: this.currentModuleId,
        }))
      ),
      this.widgetContext ? this.widgetContext.isViewMode : of(true)
    ]).subscribe(([routeCommands, isViewMode]) => {
      this.routeCommands = routeCommands
      this.isEditMode = !isViewMode;
      this.url = this.generateUrl();
    }));
  }

  ngOnChanges() {
    this.routeSubject.next(this.route);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  get currentModuleId() {
    return this.pageInfoService ? this.pageInfoService.currentModuleId : null;
  }

  get shouldFollowLink() {
    return !this.isEditMode || this.enableInEditMode;
  }

  private generateUrl() {
    if (this.routeCommands) {
      try {
        return this.router.createUrlTree(this.routeCommands).toString();
      } catch (exception) {
        console.error('Error generating URL', exception);
        return '#';
      }
    } else {
      return '#';
    }
  }

  // tslint:disable-next-line:no-unsafe-any
  @HostListener('click', ['$event.button', '$event.ctrlKey', '$event.metaKey', '$event.shiftKey'])
  onClick(button: number, ctrlKey: boolean, metaKey: boolean, shiftKey: boolean): boolean {
    if (button !== 0 || ctrlKey || metaKey || shiftKey) {
      return true;
    }

    if (typeof this.target === 'string' && this.target !== '_self') {
      return true;
    }

    if (this.routeCommands && this.shouldFollowLink) {
      this.subscription.add(this.routeHandler.navigate(this.route, {
        binder: this.pageDataContext,
        moduleId: this.currentModuleId,
      }).subscribe());
    }
    return false;
  }
}
