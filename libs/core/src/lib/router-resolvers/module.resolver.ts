import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';

import { Module, ModuleService } from '../services/module.service';

@Injectable()
export class ModuleResolver implements Resolve<Module> {
  constructor (
    private moduleService: ModuleService
  ) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Module> {
    const moduleId = route.paramMap.get('moduleId');
    return this.moduleService.getModule(moduleId).pipe(
      take(1)
    );
  }
}
