import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { take } from 'rxjs/operators';

import { Module, ModuleService } from '../services/module.service';
import { PageConfig } from '../page-config'

@Injectable()
export class ModulePageResolver implements Resolve<PageConfig> {

  resolve(route: ActivatedRouteSnapshot): Observable<PageConfig> {
    const module = route.parent.data['module'] as Module;
    const pageId = route.paramMap.get('pageId');

    if (!module) {
      return throwError(new Error(`ModulePageResolver: 'module' not found in parent router state! Check your resolvers`));
    } else if (!pageId) {
      return throwError(new Error(`ModulePageResolver: pageId param is missing/falsy!`));
    } else {
      return module.getPage(pageId).pipe(
        take(1)
      );
    }
  }
}
