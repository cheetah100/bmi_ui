import { GravityApiAppVisualisation } from '@bmi/gravity-services';
import { WidgetConfig } from './widget-config';

import { PageDataConfig } from './page-data-context/page-data-config';



export interface PageConfig<TRootWidget extends WidgetConfig = WidgetConfig> extends GravityApiAppVisualisation {
  id: string;
  name: string;
  type: string;
  applicationId: string;
  permissions: {[key: string]: any};

  /**
   * Optional 'nice' ID to use in URLs.
   */
  urlId?: string;

  ui: {
    rootWidget: TRootWidget;
    dataContext?: PageDataConfig;
  };
}


/**
 * Metadata about a page
 *
 * This isn't the exact format we get from Gravity, but it's used internally
 * where we need to provide this metadata (e.g. page listings, looking up
 * routes)
 */
export interface PageInfo {
  id: string;
  applicationId: string;
  title: string;

  /**
   * These are the additional URL IDs that this page can be found by.
   *
   * This facilitates using "pretty" names in URLs (e.g. pages/cost-summary)
   * rather than complete gibberish.
   *
   * There might be multiple aliases for a page (e.g. to facilitate redirects)
   * so this has been made an array to handle that case. At the moment the page
   * metadata only stores a single (optional) urlId value, so this list might be
   * empty.
   */
  urlIds: string[];

  filters: PageFilterInfo[];
}


/**
 * Metadata record about page filters
 */
export interface PageFilterInfo {
  /** The ID of the filter */
  id: string;

  /**
   * The optionlist board (if applicable) for this filter value. This is
   * provided in case it help with offering a picker
   */
  optionlist?: string;
}



export function isValidPageConfig(config: PageConfig): boolean {
  function isObject(x: unknown): boolean {
    return !!x && typeof(x) === 'object';
  }

  return (
    isObject(config) &&
    config.type === 'page-v2' &&
    isObject(config.ui) &&
    isObject(config.ui.rootWidget)
  );
}
