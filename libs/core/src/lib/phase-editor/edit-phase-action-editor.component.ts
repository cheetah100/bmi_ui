import { Component } from '@angular/core';
import { DynamicEditorContext, TypedFormControl } from '@bmi/ui';
import { EditPhaseActionConfig } from './edit-phase-event-action';

@Component({
  template: `
    <bmi-phase-editor-config [formControl]="form"></bmi-phase-editor-config>
  `
})
export class EditPhaseActionEditorComponent {
  constructor(
    private context: DynamicEditorContext<EditPhaseActionConfig>,
  ){}

  form = new TypedFormControl<EditPhaseActionConfig>();

  ngOnInit() {
    this.context.config.subscribe(config =>this.form.patchValue(config || {}, {emitEvent: false}));
    this.form.valueChanges.subscribe(fields => this.context.setConfig(fields));
  }
}