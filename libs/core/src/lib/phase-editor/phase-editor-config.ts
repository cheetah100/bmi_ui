
import { EventActionConfig } from "../events/event-action-config";
import { ParameterBinding } from "../formatter";

export interface PhaseEditorConfig {
  boardId: ParameterBinding;
  cardId: ParameterBinding;
  afterSaveEvent?: EventActionConfig[];
}
