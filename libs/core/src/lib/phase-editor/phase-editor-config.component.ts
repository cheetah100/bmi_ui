import { Component, forwardRef, OnInit } from '@angular/core';
import { BaseControlValueAccessor, provideAsControlValueAccessor, TypedFormControl, TypedFormGroup } from '@bmi/ui';
import { Option } from '@bmi/utils';
import { Observable, of } from 'rxjs';
import { PhaseEditorConfig } from './phase-editor-config';
import { OptionlistService } from '../services/optionlist.service';

@Component({
  selector: 'bmi-phase-editor-config',
  template: `
<ng-container [formGroup]="form">

  <ui-form-field label="Board">
    <bmi-binding-picker formControlName="boardId"></bmi-binding-picker>
  </ui-form-field>

  <ui-form-field label="Card ID">
    <bmi-binding-picker formControlName="cardId"></bmi-binding-picker>
  </ui-form-field>

  <ui-form-field label="Event: After Save">
    <ui-dynamic-editor-list
      bmiUsingDynamicEditorsFor="event-action"
      formControlName="afterSaveEvent"
      addMenuLabel="Add event action">
    </ui-dynamic-editor-list>
  </ui-form-field>

</ng-container>
  `,
  providers: [
    provideAsControlValueAccessor(forwardRef(() => PhaseEditorConfigComponent))
  ]
})
export class PhaseEditorConfigComponent extends BaseControlValueAccessor<PhaseEditorConfig> implements OnInit {

  constructor(
    private optionListService: OptionlistService
  ) {
    super();
  }

  form = new TypedFormGroup<PhaseEditorConfig>({
    boardId: new TypedFormControl(null),
    cardId: new TypedFormControl(null),
    afterSaveEvent: new TypedFormControl(undefined),
  });

  phaseOptions: Observable<Option[]> = of([]);

  ngOnInit() {
    this.form.valueChanges.subscribe(formValue => {
      const updatedConfig = {...formValue};

      if (!updatedConfig.afterSaveEvent || updatedConfig.afterSaveEvent.length === 0) {
        updatedConfig.afterSaveEvent = undefined;
      }

      this.onTouched();
      this.onChanged(updatedConfig);
    });
  }

  updateComponentValue(config: PhaseEditorConfig) {
    if (!config) {
      this.form.reset({}, {emitEvent: false});
    } else {
      this.form.patchValue(config, {emitEvent: false});
    }
  }

}