import { Injectable } from '@angular/core';
import { combineLatest, Observable } from 'rxjs';
import { switchMap, take } from 'rxjs/operators';
import { ActionResult, EventActionFunction } from '../events/event-action';
import { EventActionConfig } from '../events/event-action-config';
import { EventActionFactory, EventActionFactoryParams } from '../events/event-action-factory.service';
import { ParameterBinding } from '../page-data-context/parameter-binding';
import { PhaseEditorModalService } from './phase-editor-modal.service';

export interface EditPhaseActionConfig {
  boardId: ParameterBinding;
  cardId: ParameterBinding;
  afterSaveEvent?: EventActionConfig[];
}


@Injectable()
export class EditPhaseEventAction implements EventActionFactory<EditPhaseActionConfig> {
  constructor(
    private phaseEditorModalService: PhaseEditorModalService,
  ){}

  create(config: EditPhaseActionConfig, params: EventActionFactoryParams): EventActionFunction {
    return ctx => {
      const dataContext = ctx.dataContext || params.dataContext;
      return combineLatest([
        dataContext.bind(config.cardId),
        dataContext.bind(config.boardId)
      ]).pipe(
        take(1),
        switchMap(([cardId, boardId]) => this.phaseEditorModalService.show(
          {...config, cardId, boardId },
          { dataContext: dataContext, size: 'small' }
        )),
        switchMap(result => result && result.action !== 'cancel' ? ActionResult.CONTINUE : ActionResult.STOP)
      ) as Observable<ActionResult>;
    };
  }
}