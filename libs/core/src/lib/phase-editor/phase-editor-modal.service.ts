import { Injectable } from "@angular/core";
import { uniqueId } from "@bmi/utils";
import { ObjectMap } from "libs/utils/src/collection-utils";
import { PageConfig } from "../page-config";
import { ModalPageService, ShowPageOptions } from "../page/modal-page.service";
import { WidgetConfig } from "../widget-config";
import { PhaseEditorConfig } from "./phase-editor-config";


interface CreatePageOptions<T extends WidgetConfig> {
  title: string;
  dataContext?: ObjectMap<any>;
  rootWidget: T;
}

const createPageConfig = <T extends WidgetConfig>(options: CreatePageOptions<T>): PageConfig<T> => {
  return {
    applicationId: '__bmi',
    type: 'page-v2',
    id: uniqueId(),
    name: options.title,
    permissions: {},
    ui: {
      dataContext: options.dataContext || {},
      rootWidget: options.rootWidget
    }
  }
}

@Injectable({providedIn: 'root'})
export class PhaseEditorModalService {
  constructor(
    private modalPageService: ModalPageService,
  ) {}

  show(config: PhaseEditorConfig, options?: Partial<ShowPageOptions>) {
    const pageConfig = this.createPhaseEditorPage(config);
    return this.modalPageService.show({
      ...options ?? {},
      pageConfig,
    });
  }

  private createPhaseEditorPage(config: PhaseEditorConfig): PageConfig {
    return createPageConfig({
      title: 'Edit Phase',
      rootWidget: <WidgetConfig>{
        widgetType: 'phase-edit-form',
        fields: {
          ...config,
          afterSaveEvent: [
            {type: 'trigger-close-modal'}
          ]
        }
      }
    })
  }

}
