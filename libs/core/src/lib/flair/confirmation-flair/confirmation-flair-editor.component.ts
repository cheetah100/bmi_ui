import { Component, OnDestroy, OnInit } from '@angular/core';

import { Subscription } from 'rxjs';

import { TypedFormGroup, TypedFormControl, DynamicEditorContext } from '@bmi/ui';
import { ObjectMap } from '@bmi/utils';
import { PathResolver } from '@bmi/gravity-services';

import { ParameterBinding } from '../../page-data-context/parameter-binding';
import { PageDataContext } from '../../page-data-context/page-data-context';

import { ConfirmationFlairConfig } from './confirmation-flair';


@Component({
  selector: 'bmi-confirmation-flair-editor',
  template: `
    <ng-container [formGroup]="form">
      <ui-form-field label="Message Template">
        <textarea rows="10" formControlName="messageTemplate"></textarea>
      </ui-form-field>

      <ui-form-field>
        <bmi-data-map-editor formControlName="fields" [resolver]="resolver"></bmi-data-map-editor>
      </ui-form-field>
    </ng-container>
  `
})
export class ConfirmationFlairEditorComponent implements OnInit, OnDestroy {
  private subscription = new Subscription();

  readonly form = new TypedFormGroup<ConfirmationFlairConfig>({
    flairType: new TypedFormControl('test-confirmation-flair'),
    fields: new TypedFormControl<ObjectMap<ParameterBinding>>({}),
    messageTemplate: new TypedFormControl('')
  });

  resolver: PathResolver = null;

  constructor(
    private flairEditorContext: DynamicEditorContext<ConfirmationFlairConfig>,
    private pageDataContext: PageDataContext
  ) {}

  ngOnInit() {
    this.subscription.add(
      this.flairEditorContext.config.subscribe(
        config => this.form.patchValue(config, {emitEvent: false})
      )
    );

    this.subscription.add(
      this.form.valueChanges.subscribe(value => this.flairEditorContext.setConfig(value))
    );

    this.resolver = this.pageDataContext.getPathResolverNow();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
