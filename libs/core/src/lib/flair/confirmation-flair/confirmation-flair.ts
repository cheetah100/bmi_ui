import { Injectable, Injector } from '@angular/core';

import { of, Observable } from 'rxjs';
import { switchMap, take } from 'rxjs/operators';

import { ConfirmationModalService } from '@bmi/ui';
import { ObjectMap, combineLatestObject } from '@bmi/utils';

import mapValues from 'lodash-es/mapValues';

import { ParameterBinding } from '../../page-data-context/parameter-binding';
import { PageDataContext } from '../../page-data-context/page-data-context';

import { mustacheDown } from '../../util/mustachedown';

import { Flair, FlairFactory, FlairIcon } from '../flair';
import { FlairConfig } from '../flair-config';

export interface ConfirmationFlairConfig extends FlairConfig {
  messageTemplate: string;
  fields: ObjectMap<ParameterBinding>;
}


@Injectable({providedIn: 'root'})
export class ConfirmationFlairFactory implements FlairFactory {
  flairType = 'test-confirmation-flair';

  constructor (
    private confirmationService: ConfirmationModalService
  ) {}

  createFlair(config: ConfirmationFlairConfig, widgetInjector: Injector): Flair {
    // The flair factory itself is injected, but it's global. Any dependencies
    // from its injector come from the global module, so we can't access
    // page-specific stuff like the data context.
    //
    // For that reason, when creating the flair, the factory is given a 'local'
    // injector that will grab dependencies from the widget's scope.
    const dataContext = widgetInjector.get(PageDataContext);
    return new ConfirmationFlair(
      config,
      dataContext,
      this.confirmationService
    );
  }
}


export class ConfirmationFlair implements Flair {

  icons: Observable<FlairIcon[]> = of([
    {
      icon: 'cui-comment',
      label: 'Show Confirmation',
      invoke: () => this.showConfirmation()
    }
  ]);

  constructor (
    public config: ConfirmationFlairConfig,
    private binder: PageDataContext,
    private confirmationService: ConfirmationModalService
  ) {}

  showConfirmation() {
    combineLatestObject(mapValues(this.config.fields, binding => this.binder.bind(binding))).pipe(
      take(1),
      switchMap(data => {
        return this.confirmationService.show({
          content: mustacheDown(this.config.messageTemplate, {
            formatMarkdown: true,
            context: data,
            handleError: (error, text) => {
              console.warn('ConfirmationFlair: ', error);
              return text;
            }
          }),
          buttonYesText: 'Rad!',
          buttonCancelText: 'Also Rad!'
        })
      })
    ).subscribe();
  }
}
