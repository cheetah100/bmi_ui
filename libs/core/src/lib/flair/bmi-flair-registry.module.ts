import { NgModule, Provider, Type } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedUiModule } from '@bmi/ui';

import { BmiCoreComponentsModule } from '../components/bmi-core-components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FLAIR_FACTORY_TOKEN, FlairFactory } from './flair';

import { TestAlertFlairFactory } from './test-alert-flair';
import { ConfirmationFlairFactory } from './confirmation-flair/confirmation-flair';
import { WidgetTitleFlairFactory } from './title-flair/title-flair';
import { WidgetRouteFlairFactory } from './route-flair/route-flair';
import { NotesFlairFactory } from './notes-flair/notes-flair';

import { WidgetInfoFlairFactory } from './info-flair/info-flair';
import { KpiInfoFlairFactory } from './info-flair/kpi-info-flair';
import { InfoModalComponent } from './info-flair/info-modal.component';
import { KpiModalComponent } from './info-flair/kpi-modal.component';
import { MetricInfoModalComponent } from './metric-info-flair/metric-info-modal.component';
import { HideFlairFactory } from './hide-flair/hide-flair';
import { ScrollFlairFactory } from './scroll-flair/scroll-flair';
import { AlignmentFlairFactory } from './alignment-flair/alignment-flair';
import { ActionButtonModifierFactory } from './action-buttons-widget-modifier';
import { MetricInfoFlairFactory } from './metric-info-flair/metric-info-flair';

export function registerFlairFactory(type: Type<FlairFactory>): Provider[] {
  return [
    type,
    { provide: FLAIR_FACTORY_TOKEN, multi: true, useExisting: type }
  ];
}

@NgModule({
  imports: [
    CommonModule,
    SharedUiModule,
    BmiCoreComponentsModule,
    ReactiveFormsModule,
    FormsModule
  ],

  declarations: [
    InfoModalComponent,
    KpiModalComponent,
    MetricInfoModalComponent,
  ],

  entryComponents: [
    InfoModalComponent,
    KpiModalComponent,
    MetricInfoModalComponent,
  ],

  providers: [
    registerFlairFactory(ConfirmationFlairFactory),
    registerFlairFactory(TestAlertFlairFactory),
    registerFlairFactory(WidgetRouteFlairFactory),
    registerFlairFactory(WidgetTitleFlairFactory),
    registerFlairFactory(NotesFlairFactory),
    registerFlairFactory(WidgetInfoFlairFactory),
    registerFlairFactory(KpiInfoFlairFactory),
    registerFlairFactory(HideFlairFactory),
    registerFlairFactory(ScrollFlairFactory),
    registerFlairFactory(AlignmentFlairFactory),
    registerFlairFactory(ActionButtonModifierFactory),
    registerFlairFactory(MetricInfoFlairFactory),
  ]
})
export class BmiFlairRegistryModule {}
