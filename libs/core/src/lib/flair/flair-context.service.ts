import { Injectable, Optional, InjectionToken, Inject, OnDestroy, Injector } from '@angular/core';

import { Observable, BehaviorSubject, of } from 'rxjs';
import { distinctUntilChanged, switchMap, shareReplay } from 'rxjs/operators';

import { combineLatestDeferred, flattenLatest } from '@bmi/utils';
import flatten from 'lodash-es/flatten';

import { FlairConfig } from './flair-config';
import { Flair, FlairIcon, FlairFactory, FLAIR_FACTORY_TOKEN } from './flair';



function isArrayEqual<T>(a: T[], b: T[]): boolean {
  return a && b && a.length === b.length && a.every((x, i) => x === b[i]);
}


@Injectable()
export class FlairContextService implements OnDestroy {
  private flairFactories = new Map<string, FlairFactory>();

  private configFlairs = new BehaviorSubject<Flair[]>([]);
  private dynamicFlairs = new BehaviorSubject<Flair[]>([]);

  private currentFlairsSubject = new BehaviorSubject<Flair[]>([]);


  public readonly flairs = flattenLatest([this.dynamicFlairs, this.configFlairs]).pipe(
    distinctUntilChanged((a, b) => isArrayEqual(a, b)),
    shareReplay(1)
  );

  /**
   * Gets all of the flair icons for flairs active on this service instance.
   *
   * This combines and flattens the icons observable from all flairs, stripping
   * out the ones that don't support icons.
   */
  public readonly icons = this.flairs.pipe(
    switchMap(flairs => flattenLatest(flairs.map(f => f.icons).filter(x => x))),
    shareReplay(1)
  );

  get currentFlairs(): Flair[] {
    return this.currentFlairsSubject.value;
  }

  constructor (
    @Optional() @Inject(FLAIR_FACTORY_TOKEN) flairFactories: FlairFactory[],
    private injector: Injector
  ) {
    if (flairFactories) {
      flairFactories.forEach(factory => this.flairFactories.set(factory.flairType, factory));
    }

    this.flairs.subscribe(this.currentFlairsSubject);
  }

  /**
   * Initialize the config flairs.
   *
   * This will replace any existing config flairs in the instance. (in future
   * this behaviour may change to reuse flairs with unchanged configs!)
   *
   * Config flairs are separate from 'dynamic' flairs which are added using the
   * addFlair and deleteFlair methods.
   */
  initialize(flairConfig: FlairConfig[]) {
    // TODO: reuse the flairs if the configs haven't changed.
    this.destroyFlairs(this.configFlairs.value);

    const flairs = [];
    if (flairConfig) {
      for (const config of flairConfig) {
        try {
          flairs.push(this.createFlair(config));
        } catch (error) {
          console.error(`Flair creation error`, String(error));
        }
      }
    }
    this.configFlairs.next(flairs);
  }


  /**
   * Add a 'dynamic' flair instance.
   *
   * These are managed separately from the 'config' flairs, and allow widget
   * instances to dynamically create flair instances without needing to modify
   * the configs.
   *
   * These flairs aren't cleaned up when the service is reinitialized, so it's
   * up to the widget instance to manage the flairs that are created.
   */
  addFlair(flairConfig: FlairConfig): Flair {
    const flair = this.createFlair(flairConfig);
    this.dynamicFlairs.next([...this.dynamicFlairs.value, flair]);
    return flair;
  }

  /**
   * Removes a dynamic flair, destroying it.
   *
   * The flair will be removed from this instance (if present), and the destroy
   * method will be called on the flair if defined.
   *
   * This cannot be used to remove config-defined flairs.
   */
  removeFlair(flair: Flair): void {
    if (flair && flair.destroy) {
      flair.destroy();
    }
    this.dynamicFlairs.next(this.dynamicFlairs.value.filter(f => f !== flair));
  }


  /**
   * Clears all dynamic flairs from this instance.
   */
  clearDynamicFlairs() {
    this.destroyFlairs(this.dynamicFlairs.value);
    this.dynamicFlairs.next([]);
  }

  /**
   * Destroys the flair context and any flairs.
   */
  destroy() {
    this.destroyFlairs(this.configFlairs.value);
    this.destroyFlairs(this.dynamicFlairs.value);

    for (const subject of [this.configFlairs, this.dynamicFlairs]) {
      subject.next([]);
      subject.complete();
    }
  }

  ngOnDestroy() {
    // Implementing ngOnDestroy means that view-level injectors will
    // automatically clean up the service instance when the associated view is
    // destroyed
    this.destroy();
  }

  /**
   * Constructs a flair object from the config using the available flair
   * factories.
   *
   * If there is a flair factory available, it will be used to create a flair
   * from the config. Otherwise this will throw an error, as the flair isn't
   * supported.
   */
  private createFlair(config: FlairConfig): Flair {
    const factory = this.flairFactories.get(config.flairType);
    if (factory) {
      return factory.createFlair(config, this.injector);
    } else {
      throw new Error(`Unsupported flair type: '${config.flairType}'`);
    }
  }

  private destroyFlairs(flairs: Flair[]) {
    flairs.forEach(flair => flair.destroy && flair.destroy());
  }
}
