import { Injectable, Injector } from '@angular/core';
import { Subscription } from 'rxjs';

import { BaseWidgetModifierFlairFactory } from './base-widget-modifier-flair';
import { FlairConfig } from './flair-config';
import { WidgetWrapperContext } from '../components/widget-wrapper/widget-wrap';
import { ConfigurableToolbarItem } from '../components/configurable-action-buttons/config';

export interface ActionButtonModifierConfig extends FlairConfig {
  buttons: ConfigurableToolbarItem[];
}

@Injectable({ providedIn: 'root' })
export class ActionButtonModifierFactory extends BaseWidgetModifierFlairFactory<ActionButtonModifierConfig> {
  flairType = 'action-buttons';

  addModifiers(config: ActionButtonModifierConfig, wrapperContext: WidgetWrapperContext) {
    return wrapperContext.modify({
      actionButtons: config.buttons ?? undefined
    })
  }
}
