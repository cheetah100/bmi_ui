import { Injectable, Injector } from '@angular/core';
import { Subscription, TeardownLogic } from 'rxjs';

import { Flair, FlairFactory } from './flair';
import { FlairConfig } from './flair-config';
import { WidgetWrapperContext } from '../components/widget-wrapper/widget-wrap';


/**
 * A handy base class for widget modifiers that only need to alter the styling
 * context.
 *
 * In this case, you need to provide the flairType, and a addModifiers()
 * function. The addModifiers function will only be called if a wrap context can
 * be located, and the return result is teardown logic that will be disposed of
 * when the flair is destroyed.
 */
@Injectable()
export abstract class BaseWidgetModifierFlairFactory<TConfig extends FlairConfig> implements FlairFactory {
  abstract get flairType(): string;

  createFlair(config: TConfig, injector: Injector): Flair {
    const wrapperContext = injector.get(WidgetWrapperContext, null);
    const subscription = new Subscription();

    if (wrapperContext) {
      const teardown = this.addModifiers(config, wrapperContext, injector);
      if (Array.isArray(teardown)) {
        teardown.forEach(s => subscription.add(s))
      } else {
        subscription.add(teardown);
      }
    }

    return {
      config,
      destroy: () => subscription.unsubscribe()
    }
  }

  abstract addModifiers(config: TConfig, wrapContext: WidgetWrapperContext, injector: Injector): TeardownLogic | TeardownLogic[];
}
