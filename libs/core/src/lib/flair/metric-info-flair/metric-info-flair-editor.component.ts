import { Component, OnDestroy, OnInit } from '@angular/core';
import { DynamicEditorContext, OptionGroup, TemplatedFormArray, TypedFormControl, TypedFormGroup } from '@bmi/ui';
import { Subscription } from 'rxjs';
import { ParameterBinding } from '../../page-data-context/parameter-binding';
import { PersonWidgetFields } from '../../widgets/person-widget/person-widget-editor.component';
import { MetricInfoFlairConfig } from './metric-info-flair';


@Component({
  selector: 'bmi-metric-flair-editor',
  templateUrl: './metric-info-flair-editor.component.html',
  styles: [`
    ui-form-field + ui-form-field {
      margin-top: var(--gra-spacing-qtr);
    }
  `]
})
export class MetricInfoFlairEditorComponent implements OnInit, OnDestroy {

  private subscription = new Subscription();

  readonly form = new TypedFormGroup<MetricInfoFlairConfig>({
    flairType: new TypedFormControl<string>('metric-info'),
    name: new TypedFormControl<ParameterBinding>(null),
    description: new TypedFormControl<ParameterBinding>(null),
    people: new TemplatedFormArray<PersonWidgetFields>(() =>
      new TypedFormGroup({
        userId: new TypedFormControl<ParameterBinding>(null),
        name: new TypedFormControl<ParameterBinding>(null),
        subHeading: new TypedFormControl<ParameterBinding>(null),
      })
    ),
    template: new TypedFormControl<ParameterBinding>(null),
  });

  kpiOptions: OptionGroup[];

  get peopleControl() {
    return this.form.controls.people as TemplatedFormArray<PersonWidgetFields>;
  }

  constructor(
    private flairEditorContext: DynamicEditorContext<MetricInfoFlairConfig>,
  ) { }

  ngOnInit() {
    this.subscription.add(this.flairEditorContext.config.subscribe(
      config => this.form.patchValue(config, { emitEvent: false })
    ));
    this.subscription.add(this.form.valueChanges.subscribe(
      value => this.flairEditorContext.setConfig(value)
    ));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  addPerson() {
    this.peopleControl.pushValue({
      userId: null,
      name: null,
    });
  }

}
