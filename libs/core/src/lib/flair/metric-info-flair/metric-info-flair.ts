import { ComponentFactoryResolver, Injectable, Injector } from '@angular/core';
import { SafeHtml } from '@angular/platform-browser';
import { ModalService } from '@bmi/ui';
import { combineLatestObject } from '@bmi/utils';
import mapValues from 'lodash-es/mapValues';
import { combineLatest, Observable, of } from 'rxjs';
import { map, take } from 'rxjs/operators';
import * as showdown from 'showdown';
import { ActionResult, EventActionConfig, EventActionFunction } from '../../events';
import { PageDataContext } from '../../page-data-context/page-data-context';
import { ParameterBinding } from '../../page-data-context/parameter-binding';
import { mustacheDown } from '../../util/mustachedown';
import { PersonWidgetFields } from '../../widgets/person-widget/person-widget-editor.component';
import { Flair, FlairFactory, FlairIcon } from '../flair';
import { FlairConfig } from '../flair-config';
import { MetricInfoModalComponent } from './metric-info-modal.component';


export interface MetricInfoFlairConfig extends FlairConfig {
  name: ParameterBinding,
  description: ParameterBinding,
  people: PersonWidgetFields[],
  template: ParameterBinding,
}


@Injectable({ providedIn: 'root' })
export class MetricInfoFlairFactory implements FlairFactory {
  flairType = 'metric-info';

  constructor(
    private modalService: ModalService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private injector: Injector,
  ) { }

  createFlair(config: MetricInfoFlairConfig, widgetInjector: Injector): MetricInfoFlair {
    const dataContext = widgetInjector.get(PageDataContext);
    return new MetricInfoFlair(
      config,
      dataContext,
      this.modalService,
      this.componentFactoryResolver,
      this.injector,
    );
  }

  create(config: MetricInfoFlairConfig & EventActionConfig, foo: any): EventActionFunction {
    const flair = this.createFlair(config, null);
    return ctx => {
      flair.showOverlay();
      return ActionResult.CONTINUE;
    };
  }
}


export class MetricInfoFlair implements Flair {

  icons: Observable<FlairIcon[]> = of([
    {
      icon: 'shared-ui-info-circle',
      label: 'Show Widget Info',
      invoke: () => this.showOverlay()
    }
  ]);
  thisContext = null;

  constructor(
    public config: MetricInfoFlairConfig,
    private binder: PageDataContext,
    private modalService: ModalService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private injector: Injector,
  ) { }

  showOverlay() {
    let peopleObservables = [];
    if (this.config.people && this.config.people.length) {
      peopleObservables = this.config.people.map(
        person => combineLatestObject(
          mapValues(person, b => this.binder.bind(b))
        )
      ) as Observable<{userId: string, name: string, subHeading?: string}>[];
    }
    combineLatestObject({
      name: this.binder.bind(this.config.name),
      description: this.binder.bind(this.config.description),
      people: peopleObservables.length ? combineLatest(peopleObservables) : of([]),
      template: this.binder.bind(this.config.template).pipe(
        take(1),
        map(data => {
          const converter = new showdown.Converter({
            noHeaderId: true
          });
          return converter.makeHtml(
            mustacheDown(data, {
              formatMarkdown: true,
              context: data,
              handleError: (error, text) => {
                console.warn('MetricInfoFlair: ', error);
                return text;
              }
            }),
          );
        }),
      )
    }).subscribe((resolved: {
      name: string;
      description: string;
      people: {userId: string, name: string, subHeading?: string}[];
      template: SafeHtml;
    }) => {
      const modalRef = this.modalService.createModal(
        MetricInfoModalComponent,
        this.componentFactoryResolver,
        this.injector
      );
      const modal = modalRef.instance;
      modal.data = resolved;
      modal.show();
    });
  }

}
