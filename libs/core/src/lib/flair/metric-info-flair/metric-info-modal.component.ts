import { Component, EventEmitter, Input, ViewChild } from '@angular/core';
import { SafeHtml } from '@angular/platform-browser';
import { ModalComponent } from '@bmi/ui';
import { Observable } from 'rxjs';


interface Modal {
  modalClosed?: Observable<void>;
  show(): Observable<any>;
}

@Component({
  template: `
    <ui-modal (modalClosed)="onModalClosed()">
      <ui-loading-container [isLoading]="isLoading">
        <ng-template>
          <h1>{{ data.name }}</h1>
          <blockquote><p>{{ data.description }}</p></blockquote>
          <section class="metric-info__people">
            <bmi-person
              *ngFor="let person of data.people"
              [userId]="person.userId"
              [name]="person.name"
              [subHeading]="person.subHeading">
            </bmi-person>
          </section>
          <div class="no-margin" *ngIf="data.template" [innerHtml]="data.template"></div>
        </ng-template>
      </ui-loading-container>
    </ui-modal>
  `,
  styles: [`
    .metric-info__people {
      display: grid;
      grid-template-columns: 1fr 1fr;
      margin-bottom: var(--gra-spacing);
    }
    ui-loading-container {
      min-height: 180px;
    }
  `]
})
export class MetricInfoModalComponent implements Modal {
  @ViewChild(ModalComponent, { static: true }) private modalComponent: ModalComponent;

  @Input() public data: {
    name: string,
    description: string,
    people: { userId: string, name: string, subHeading?: string }[],
    template: SafeHtml
  } = null;

  modalClosed = new EventEmitter<void>();
  isLoading = false;

  show<T>(): Observable<any> {
    this.modalComponent.styles = {
      width: 'auto',
      minWidth: '64rem',
      maxWidth: '80vw',
    };
    this.modalComponent.show();
    return null;
  }

  onModalClosed() {
    this.modalClosed.emit();
  }

  close() {
    this.modalComponent.close();
  }

}