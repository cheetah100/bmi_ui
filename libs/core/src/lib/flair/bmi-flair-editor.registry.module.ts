import { NgModule, Provider } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PortalModule } from '@angular/cdk/portal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedUiModule } from '@bmi/ui';
import { BmiCoreModule } from '../bmi-core.module';
import { BmiCoreEditorComponentsModule } from '../editor-components/bmi-core-editor-components.module';
import { EditorWidgets, MappedFormSchemaForType } from '../forms/generic-editor-schema';


import { GenericEventActionEditorComponent } from '../event-actions/generic-event-action-editor/generic-event-action-editor.component';



import {
  FlairEditorDefinition,
  FLAIR_EDITOR_TOKEN
} from './flair-editor-definition';

import { ConfirmationFlairEditorComponent } from './confirmation-flair/confirmation-flair-editor.component';
import { TitleFlairEditorComponent } from './title-flair/title-flair-editor.component';
import { RouteFlairEditorComponent } from './route-flair/route-flair-editor.component';
import { NotesFlairEditorComponent } from './notes-flair/notes-flair-editor.component';
import { InfoFlairEditorComponent } from './info-flair/info-flair-editor.component';
import { KpiFlairEditorComponent } from './info-flair/kpi-flair-editor.component';
import { HideFlairEditorComponent } from './hide-flair/hide-flair-editor.component';
import { ScrollFlairEditorComponent } from './scroll-flair/scroll-flair-editor.component';
import { AlignmentFlairEditorComponent } from './alignment-flair/alignment-flair-editor.component';
import { MetricInfoFlairEditorComponent } from './metric-info-flair/metric-info-flair-editor.component';
import { ActionButtonModifierConfig } from './action-buttons-widget-modifier';
import { FlairConfig } from './flair-config';

export function provideFlairEditor(
  flairEditorDefinition: FlairEditorDefinition
): Provider {
  return {
    provide: FLAIR_EDITOR_TOKEN,
    multi: true,
    useValue: flairEditorDefinition
  };
}


interface GenericFlairEditorDefiniton<TConfig> {
  type: string;
  name: string;
  icon?: string;
  schema: MappedFormSchemaForType<Omit<TConfig, 'flairType'>>;
  defaultConfig: Partial<TConfig>;
}


function defineGenericFlairEditor<TConfig extends FlairConfig>(options: GenericFlairEditorDefiniton<TConfig>) {
  return provideFlairEditor({
    type: options.type,
    name: options.name,
    icon: options.icon,

    // TODO: we can generalize this component, there isn't really anything
    // specific about event actions there
    component: GenericEventActionEditorComponent,
    options: {
      schema: options.schema
    },
    defaultConfig: options.defaultConfig
  });
}

@NgModule({
  imports: [
    CommonModule,
    SharedUiModule,
    PortalModule,
    FormsModule,
    ReactiveFormsModule,
    BmiCoreModule,
    BmiCoreEditorComponentsModule
  ],

  declarations: [
    ConfirmationFlairEditorComponent,
    HideFlairEditorComponent,
    InfoFlairEditorComponent,
    KpiFlairEditorComponent,
    NotesFlairEditorComponent,
    RouteFlairEditorComponent,
    TitleFlairEditorComponent,
    ScrollFlairEditorComponent,
    AlignmentFlairEditorComponent,
    MetricInfoFlairEditorComponent,
  ],


  providers: [
    // Don't forget to add the flair editor to the declarations

    provideFlairEditor({
      type: 'widget-route',
      name: 'Widget Link',
      addPhraseText: 'Make Link',
      icon: 'cui-link',
      component: RouteFlairEditorComponent,
      noUserSorting: true,
      sortWeight: -5,
      defaultConfig: {
        route: null,
        fields: {}
      }
    }),

    provideFlairEditor({
      type: 'widget-title',
      name: 'Widget Title',
      addPhraseText: 'Add Title',
      icon: 'cui-default-app',
      component: TitleFlairEditorComponent,
      noUserSorting: true,
      sortWeight: -10,
      defaultConfig: {
        title: '<Add widget Title Here>',
        fields: {}
      }
    }),

    provideFlairEditor({
      type: 'notes-flair',
      name: 'Notes Flair',
      component: NotesFlairEditorComponent,
      icon: 'cui-notes',
      defaultConfig: {
        fields: {}
      }
    }),
    provideFlairEditor({
      type: 'widget-info',
      name: 'Custom Info Popup',
      icon: 'shared-ui-info-circle',
      component: InfoFlairEditorComponent,
      defaultConfig: {
        template: '## Widget Information',
        fields: {}
      }
    }),

    provideFlairEditor({
      type: 'kpi-info',
      name: 'Kpi Info Popup',
      icon: 'shared-ui-info-circle',
      component: KpiFlairEditorComponent,
      defaultConfig: {
        kpiId: null,
        fields: {}
      }
    }),

    provideFlairEditor({
      type: 'metric-info',
      name: 'Metric Info Popup',
      icon: 'shared-ui-info-circle',
      component: MetricInfoFlairEditorComponent,
      defaultConfig: {}
    }),

    provideFlairEditor({
      type: 'hide',
      name: 'Hide Widget',
      icon: 'cui-eye-closed',
      component: HideFlairEditorComponent,
      defaultConfig: {
        hide: true
      }
    }),

    provideFlairEditor({
      type: 'scroll',
      name: 'Scrollable Widget (limit height)',
      icon: 'shared-ui-arrows-v',
      component: ScrollFlairEditorComponent,
      defaultConfig: {
        height: 400,
        fixed: true
      }
    }),

    provideFlairEditor({
      type: 'alignment',
      name: 'Horizontal Alignment',
      icon: 'fa4-align-left',
      component: AlignmentFlairEditorComponent,
      defaultConfig: {
        height: 400,
        fixed: true
      }
    }),

    defineGenericFlairEditor<ActionButtonModifierConfig>({
      type: 'action-buttons',
      name: 'Action Buttons',
      schema: {
        buttons: EditorWidgets.actionButtons({label: 'Actions'}),
      },
      defaultConfig: {
        buttons: [],
      }
    })
  ]
})
export class BmiFlairEditorRegistryModule {}
