import { Component, OnInit, OnDestroy } from '@angular/core';

import { Subscription } from 'rxjs';

import {
  TypedFormGroup,
  TypedFormControl,
  DynamicEditorContext
} from '@bmi/ui';
import { ObjectMap } from '@bmi/utils';
import { PathResolver } from '@bmi/gravity-services';

import { ParameterBinding } from '../../page-data-context/parameter-binding';
import { PageDataContext } from '../../page-data-context/page-data-context';

import { WidgetTitleFlairConfig } from './title-flair';

@Component({
  selector: 'bmi-title-flair-editor',
  template: `
    <ng-container [formGroup]="form">
      <div class="title-flair-editor__title-grid">
        <ui-form-field label="Widget Title">
          <input formControlName="title" />
        </ui-form-field>
        <ui-form-field label="Underline">
          <ui-checkbox [formControl]="underlineControl
          "></ui-checkbox>
        </ui-form-field>
      </div>

      <ui-form-field label="Title Template Values">
        <bmi-data-map-editor
          formControlName="fields"
          [resolver]="resolver"
        ></bmi-data-map-editor>
      </ui-form-field>
    </ng-container>
  `,
  styles: [
    `
      .title-flair-editor__title-grid {
        display: grid;
        grid-gap: var(--gra-spacing-half);
        grid-template-columns: 1fr 60px;
      }
    `
  ]
})
export class TitleFlairEditorComponent implements OnInit, OnDestroy {
  private subscription = new Subscription();

  readonly form = new TypedFormGroup<WidgetTitleFlairConfig>({
    flairType: new TypedFormControl('widget-title-flair'),
    fields: new TypedFormControl<ObjectMap<ParameterBinding>>({}),
    title: new TypedFormControl<string>(''),
    options: new TypedFormGroup({
      noUnderline: new TypedFormControl<boolean>(false)
    })
  });
  // We want users to check for underline.
  // We'll ! that for the actual config for default underline compatibility
  readonly underlineControl = new TypedFormControl<boolean>(true)

  resolver: PathResolver = null;

  get noUnderlineControl() {
    return this.form.get('options.underline');
  }

  constructor(
    private flairEditorContext: DynamicEditorContext<WidgetTitleFlairConfig>,
    private pageDataContext: PageDataContext
  ) {}

  ngOnInit() {
    this.subscription
      .add(
        this.flairEditorContext.config.subscribe(config => {
          const options = config.options || {};
          this.form.patchValue(config, { emitEvent: false });
          this.underlineControl.setValue(!options.noUnderline, { emitEvent: false });
        })
      )
      .add(
        this.underlineControl.valueChanges.subscribe(
          show => this.form.get('options.noUnderline').setValue(!show)
        )
      )
      .add(
      this.form.valueChanges.subscribe(value =>
        this.flairEditorContext.setConfig(value)
      )
    );

    this.resolver = this.pageDataContext.getPathResolverNow();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
