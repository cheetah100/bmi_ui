import { Injectable, Injector, InjectFlags } from '@angular/core';

import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

import { combineLatestObject, ObjectMap } from '@bmi/utils';

import { BaseWidgetModifierFlairFactory } from '../base-widget-modifier-flair';
import { FlairConfig } from '../flair-config';

import mapValues from 'lodash-es/mapValues';

import { ParameterBinding } from '../../page-data-context/parameter-binding';
import { PageDataContext } from '../../page-data-context/page-data-context';

import { WidgetWrapperContext, WidgetStyleOptions, WidgetTitleOptions } from '../../components/widget-wrapper/widget-wrap';

import { mustacheDown } from '../../util/mustachedown';


export interface WidgetTitleFlairConfig extends FlairConfig {
  title: string;
  options: WidgetTitleOptions;
  fields: ObjectMap<ParameterBinding>;
}

@Injectable({ providedIn: 'root' })
export class WidgetTitleFlairFactory extends BaseWidgetModifierFlairFactory<WidgetTitleFlairConfig> {
  flairType = 'widget-title';

  addModifiers(config: WidgetTitleFlairConfig, context: WidgetWrapperContext, injector: Injector) {
    const dataContext = injector.get(PageDataContext, undefined, InjectFlags.Optional);
    return context.modify(
      combineLatestObject(
        mapValues(config.fields, b => dataContext?.bind(b))
      ).pipe(
        map(values =>
          <WidgetStyleOptions>{
            title: mustacheDown(config.title, {
              context: values,
              formatMarkdown: false,
            }),
            titleOptions: config.options
          }
        ),
      )
    )
  }
}
