import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { TypedFormGroup, TypedFormControl, DynamicEditorContext } from '@bmi/ui';

import { HideFlairConfig } from './hide-flair';


@Component({
  selector: 'bmi-hide-flair-editor',
  template: `
    <ng-container [formGroup]="form">
      <ui-form-field label="Hide widget (still shows in edit mode)">
        <ui-checkbox formControlName="hide"></ui-checkbox>
      </ui-form-field>
    </ng-container>
  `
})
export class HideFlairEditorComponent implements OnInit, OnDestroy {
  private subscription = new Subscription();

  readonly form = new TypedFormGroup<HideFlairConfig>({
    flairType: new TypedFormControl<string>('hide-flair'),
    hide: new TypedFormControl<boolean>(true),
  });

  constructor(
    private flairEditorContext: DynamicEditorContext<HideFlairConfig>,
  ) { }

  ngOnInit() {
    this.subscription.add(
      this.flairEditorContext.config.subscribe(
        config => this.form.patchValue(config, { emitEvent: false })
      )
    );
    this.subscription.add(
      this.form.valueChanges.subscribe(value => this.flairEditorContext.setConfig(value))
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
