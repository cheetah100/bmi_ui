import { Injectable, Injector } from '@angular/core';
import { Subscription } from 'rxjs';

import { BaseWidgetModifierFlairFactory } from '../base-widget-modifier-flair';
import { FlairConfig } from '../flair-config';
import { WidgetWrapperContext } from '../../components/widget-wrapper/widget-wrap';


export interface HideFlairConfig extends FlairConfig {
  hide: boolean;
}


@Injectable({ providedIn: 'root' })
export class HideFlairFactory extends BaseWidgetModifierFlairFactory<HideFlairConfig> {
  flairType = 'hide';

  addModifiers(config: HideFlairConfig, context: WidgetWrapperContext) {
    return context.modify({
      hidden: config.hide
    });
  }
}
