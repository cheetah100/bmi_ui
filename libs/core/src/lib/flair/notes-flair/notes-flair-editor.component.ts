import { Component, OnInit, OnDestroy } from '@angular/core';

import { Subscription } from 'rxjs';

import { TypedFormGroup, TypedFormControl, DynamicEditorContext } from '@bmi/ui'

import { NotesFlairConfig } from './notes-flair';

import { BmiPageRoute } from '../../navigation/bmi-route';
import { PageEditorContext } from '../../page-editor/page-editor-context';


@Component({
  selector: 'bmi-notes-flair-editor',
  styles: [`
ui-form-field {
  margin-bottom: 1rem;
}
  `],
  template: `
<ng-container [formGroup]="form">
  <ui-form-field label="Notes page">
    <bmi-core-route-selector
      [moduleId]="moduleId"
      formControlName="notesPageRoute">
    </bmi-core-route-selector>
  </ui-form-field>
</ng-container>
  `
})

export class NotesFlairEditorComponent implements OnInit, OnDestroy {
  private subscription = new Subscription();

  // Map of boardId and optionlists.
  public optionlistMap = new Map();

  readonly form = new TypedFormGroup<NotesFlairConfig>({
    flairType: new TypedFormControl('notes-flair'),
    notesPageRoute: new TypedFormControl<BmiPageRoute>(null),
  });

  moduleId: string = null;

  constructor(
    private flairEditorContext: DynamicEditorContext<NotesFlairConfig>,
    private pageEditorContext: PageEditorContext,
  ) { }

  ngOnInit() {
    this.subscription.add(
      this.flairEditorContext.config.subscribe(
        (config: NotesFlairConfig) => this.form.patchValue(config, { emitEvent: false })
      )
    );

    this.subscription.add(
      this.form.valueChanges.subscribe(value => this.flairEditorContext.setConfig(value))
    );

    this.subscription.add(
      this.pageEditorContext.appId.subscribe((appId: string) => this.moduleId = appId)
    );

  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
