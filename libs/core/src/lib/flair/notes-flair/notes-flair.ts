import { Injectable, Injector } from '@angular/core';
import { of, Observable } from 'rxjs';

import { Flair, FlairFactory, FlairIcon } from '../flair';
import { FlairConfig } from '../flair-config';
import { ModalPageService } from '../../page/modal-page.service';
import { BmiPageRoute } from '../../navigation/bmi-route';
import { PageDataContext } from '../../page-data-context/page-data-context';

export interface NotesFlairConfig extends FlairConfig {
  notesPageRoute: BmiPageRoute;
}

@Injectable({ providedIn: 'root' })
export class NotesFlairFactory implements FlairFactory {

  flairType = 'notes-flair';

  constructor(
    private modalPageService: ModalPageService,
  ) { }

  createFlair(config: NotesFlairConfig, widgetInjector: Injector): Flair {
    const binder = widgetInjector.get(PageDataContext);
    return new NotesPageFlair(
      config,
      this.modalPageService,
      binder
    );
  }
}

/**
 * Candidate for renaming.
 * This flair was created for notes.
 * It opens another page in a modal window.
 * Now that filter params come with the page route, there isn't anything tying
 * this to notes. You could open any page with any filters.
 * So, ModalPageFlair?
 */
export class NotesPageFlair implements Flair {

  icons: Observable<FlairIcon[]> = of([
    {
      icon: 'cui-notes',
      label: 'Show Notes',
      invoke: () => this.showNotesModal()
    }
  ]);

  constructor(
    public config: NotesFlairConfig,
    private modalPageService: ModalPageService,
    private binder: PageDataContext,
  ) { }

  showNotesModal() {
    this.modalPageService.showOverlay(
      this.config,
      this.binder,
    );
  }

}
