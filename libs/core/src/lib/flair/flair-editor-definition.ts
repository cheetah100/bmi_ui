import { InjectionToken, Type } from '@angular/core';
import { ObjectMap } from '@bmi/utils';

import { FlairConfig } from './flair-config';


export interface FlairEditorDefinition {
  type: string;
  name: string;

  /**
   * The text to use to add this flair. This will be used on buttons and menus
   * if defined; otherwise the name will be used. (e.g. 'Add Link')
   */
  addPhraseText?: string;

  /**
   * The icon to use for this flair in the editor.
   *
   * This is independent from the icon that might get displayed in the client
   * app, but this icon is intended to make it easier to recognize the flair.
   */
  icon?: string;

  /**
   * The editor component to use. If unspecified, there will be no configurable
   * options for this flair.
   */
  component: Type<{}>;

  options?: ObjectMap<any>;

  /**
   * Whether the flair editor should allow a user to add multiple instances of
   * this flair type to a widget.
   *
   * This constraint is a usability enhancement, and is not a guarantee that
   * there will only be one instance of this flair.
   */
  allowDuplicates?: boolean;

  /**
   * If defined/true, this should prevent the user sorting this flair.
   *
   * By setting this to true, it means the flair editor should be displayed as
   * a 'static' form control, rather than reorderable.
   */
  noUserSorting?: boolean;

  /**
   * A sorting weight for flairs that don't allow user sorting. This allows us
   * to enforce the title always being displayed first etc.
   */
  sortWeight?: number;

  /**
   * Optional. Default config values. When creating an instance of this flair
   * the flairType will be set automatically so it's not necessary to include in
   * the config.
   */
  defaultConfig: Partial<FlairConfig>;
}


export const FLAIR_EDITOR_TOKEN = new InjectionToken<FlairEditorDefinition>('BMI Flair Editor Definitions');


