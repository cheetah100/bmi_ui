import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { FlairIcon } from './flair';

@Injectable()
export abstract class FlairIconContainer {
  abstract setFlairIcons(iconSource: Observable<FlairIcon[]>);
}
