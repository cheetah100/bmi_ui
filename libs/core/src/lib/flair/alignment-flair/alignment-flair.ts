import { Injectable, Injector } from '@angular/core';
import { Subscription } from 'rxjs';

import { BaseWidgetModifierFlairFactory } from '../base-widget-modifier-flair';
import { FlairConfig } from '../flair-config';
import { WidgetWrapperContext } from '../../components/widget-wrapper/widget-wrap';

export interface AlignmentFlairConfig extends FlairConfig {
  horizontalAlignment?: 'left' | 'center' | 'right';
}

@Injectable({ providedIn: 'root' })
export class AlignmentFlairFactory extends BaseWidgetModifierFlairFactory<AlignmentFlairConfig> {
  flairType = 'alignment';

  addModifiers(config: AlignmentFlairConfig, wrapperContext: WidgetWrapperContext) {
    return wrapperContext.modify({
      horizontalAlignment: config.horizontalAlignment ?? undefined
    })
  }
}
