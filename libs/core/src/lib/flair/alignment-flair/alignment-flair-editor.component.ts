import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { TypedFormGroup, TypedFormControl, DynamicEditorContext, Option } from '@bmi/ui';

import { AlignmentFlairConfig } from './alignment-flair';

@Component({
  template: `
    <ng-container [formGroup]="form">
      <ui-form-field label="Horizontal Alignment">
        <ui-select formControlName="horizontalAlignment" [options]="alignmentOptions"></ui-select>
      </ui-form-field>
    </ng-container>
  `
})
export class AlignmentFlairEditorComponent implements OnInit, OnDestroy {
  private subscription = new Subscription();

  readonly form = new TypedFormGroup<AlignmentFlairConfig>({
    flairType: new TypedFormControl('alignment'),
    horizontalAlignment: new TypedFormControl('left'),
  });

  alignmentOptions: Option[] = [
    {value: 'left', text: 'Left Align (stretched)'},
    {value: 'center', text: 'Center align'},
    {value: 'right', text: 'Right align'},
  ]

  constructor(private flairEditorContext: DynamicEditorContext<AlignmentFlairConfig>) {}

  ngOnInit() {
    this.subscription.add(
      this.flairEditorContext.config.subscribe(config =>
        this.form.patchValue(config, { emitEvent: false })
      )
    );
    this.subscription.add(
      this.form.valueChanges.subscribe(value =>
        this.flairEditorContext.setConfig(value)
      )
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
