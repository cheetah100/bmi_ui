import { Injectable } from '@angular/core';
import { of } from 'rxjs';

import { Flair, FlairFactory, FlairIcon } from './flair';
import { FlairConfig } from './flair-config';


const QUOTES = [
  'Well, I thought I remembered you saying that you wanted to express yourself.',
  'Well, okay. Fifteen is the minimum, okay?',
  `They come to Chotchkie's for the atmosphere and the attitude. Okay? That's what the flair's about. It's about fun.`,
  `Look, we want you to express yourself, okay? Now if you feel that the bare minimum is enough, then okay. But some people choose to wear more and we encourage that, okay? You do want to express yourself, don't you?`,
];


function getQuote() {
  return QUOTES[Math.floor(Math.random() * QUOTES.length)];
}


@Injectable({providedIn: 'root'})
export class TestAlertFlairFactory implements FlairFactory {
  readonly flairType = 'test-alert-flair';

  createFlair(config: FlairConfig): Flair {
    // This is a trivial flair implementation. A more sophisticated flair might
    // have a class for the Flair which gets created here.
    return {
      config: config,
      icons: of<FlairIcon[]>([{
        icon: 'shared-ui-briefcase',
        label: 'Show an alert',
        invoke: () => alert(getQuote())
      }])
    };
  }
}

