import { OnInit, Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { ObjectMap } from '@bmi/utils';
import { PathResolver } from '@bmi/gravity-services';
import { TypedFormGroup, TypedFormControl, DynamicEditorContext } from '@bmi/ui';

import { PageDataContext } from '../../page-data-context/page-data-context';
import { ParameterBinding } from '../../page-data-context/parameter-binding';
import { WidgetRouteFlairConfig } from './route-flair';
import { PageInfoService } from '../../page/page-info.service';
import { PageData } from '../../page-data-context/page-data-context';

@Component({
  selector: 'bmi-route-flair-editor',
  template: `
    <ng-container [formGroup]="form">

      <ui-form-field label="Widget Route">
        <bmi-core-route-selector
          [resolver]="resolver"
          formControlName="route"
          [moduleId]="moduleId">
        </bmi-core-route-selector>
      </ui-form-field>

    </ng-container>
  `
})
export class RouteFlairEditorComponent implements OnInit, OnDestroy {

  private subscription = new Subscription();
  resolver: PathResolver = null;
  moduleId: string;
  readonly form = new TypedFormGroup<WidgetRouteFlairConfig>({
    flairType: new TypedFormControl('widget-route-flair'),
    route: new TypedFormControl(null),
  });

  constructor(
    private flairEditorContext: DynamicEditorContext<WidgetRouteFlairConfig>,
    private pageInfoService: PageInfoService,
    private pageDataContext: PageDataContext,
  ) { }

  ngOnInit() {
    this.moduleId = this.pageInfoService.currentModuleId;
    this.resolver = this.pageDataContext.getPathResolverNow();
    this.subscription.add(
      this.flairEditorContext.config.subscribe(
        config => this.form.patchValue(config, { emitEvent: false })
      )
    );
    this.subscription.add(
      this.form.valueChanges.subscribe(
        (value: WidgetRouteFlairConfig) => this.flairEditorContext.setConfig(value)
      )
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
