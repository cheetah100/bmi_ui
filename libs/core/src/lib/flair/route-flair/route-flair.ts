import { Injectable, Injector } from '@angular/core';

import { BmiRoute } from '../../navigation/bmi-route';
import { FlairConfig } from '../flair-config';
import { FlairFactory, Flair } from '../flair';
import { BaseWidgetModifierFlairFactory } from '../base-widget-modifier-flair';
import { WidgetWrapperContext } from '../../components/widget-wrapper/widget-wrap';

export interface WidgetRouteFlairConfig extends FlairConfig {
  route: BmiRoute;
}


@Injectable({ providedIn: 'root' })
export class WidgetRouteFlairFactory extends BaseWidgetModifierFlairFactory<WidgetRouteFlairConfig> {
  flairType = 'widget-route';

  addModifiers(config: WidgetRouteFlairConfig, context: WidgetWrapperContext) {
    return context.modify({
      route: config.route
    });
  }
}
