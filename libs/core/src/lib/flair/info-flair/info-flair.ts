import { Injectable, Injector, ComponentFactoryResolver } from '@angular/core';
import { of, Observable } from 'rxjs';
import { take, map } from 'rxjs/operators';
import mapValues from 'lodash-es/mapValues';
import * as showdown from 'showdown';

import { ObjectMap, combineLatestObject } from '@bmi/utils';

import { ParameterBinding } from '../../page-data-context/parameter-binding';
import { PageDataContext } from '../../page-data-context/page-data-context';
import { mustacheDown } from '../../util/mustachedown';
import { Flair, FlairFactory, FlairIcon } from '../flair';
import { FlairConfig } from '../flair-config';
import { ModalService } from '@bmi/ui';
import { InfoModalComponent } from './info-modal.component';
import { SafeHtml } from '@angular/platform-browser';

import { EventActionFunction, EventActionFactory, EventActionConfig, ActionResult } from '../../events';


export interface WidgetInfoFlairConfig extends FlairConfig {
  template: string;
  fields: ObjectMap<ParameterBinding>;
}


@Injectable({ providedIn: 'root' })
export class WidgetInfoFlairFactory implements FlairFactory {
  flairType = 'widget-info';

  constructor(
    private modalService: ModalService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private injector: Injector,
  ) { }

  createFlair(config: WidgetInfoFlairConfig, widgetInjector: Injector): WidgetInfoFlair {
    const dataContext = widgetInjector.get(PageDataContext);
    return new WidgetInfoFlair(
      config,
      dataContext,
      this.modalService,
      this.componentFactoryResolver,
      this.injector,
    );
  }

  create(config: WidgetInfoFlairConfig & EventActionConfig, foo: any): EventActionFunction {
    const flair = this.createFlair(config, null);
    return ctx => {
      flair.showOverlay();
      return ActionResult.CONTINUE;
    };
  }
}


export class WidgetInfoFlair implements Flair {

  icons: Observable<FlairIcon[]> = of([
    {
      icon: 'shared-ui-info-circle',
      label: 'Show Widget Info',
      invoke: () => this.showOverlay()
    }
  ]);

  constructor(
    public config: WidgetInfoFlairConfig,
    private binder: PageDataContext,
    private modalService: ModalService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private injector: Injector,
  ) { }

  showOverlay() {
    combineLatestObject(mapValues(this.config.fields, binding => this.binder.bind(binding))).pipe(
      take(1),
      map(data => {
        const converter = new showdown.Converter({
          noHeaderId: true
        });
        return converter.makeHtml(
          mustacheDown(this.config.template, {
            formatMarkdown: true,
            context: data,
            handleError: (error, text) => {
              console.warn('WidgetInfoFlair: ', error);
              return text;
            }
          }),
        );
      })
    ).subscribe((content: SafeHtml) => {
      const modalRef = this.modalService.createModal(
        InfoModalComponent,
        this.componentFactoryResolver,
        this.injector
      );
      const modal = modalRef.instance;
      modal.rawHtmlContent = content;
      modal.show();
    });
  }

}
