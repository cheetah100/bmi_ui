import { Injectable, ComponentFactoryResolver, Injector } from '@angular/core';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { FlairFactory, Flair, FlairIcon } from '../flair';
import { ModalService } from '@bmi/ui';
import { PageDataContext } from '../../page-data-context/page-data-context';
import { ObjectMap } from '@bmi/utils';
import { ParameterBinding } from '../../page-data-context/parameter-binding';
import { FlairConfig } from '../flair-config';
import { CardCacheService, CardData } from '@bmi/gravity-services';
import { KpiModalComponent, KpiCardFields } from './kpi-modal.component';


export interface KpiInfoFlairConfig extends FlairConfig {
  kpiId: ParameterBinding;
}

@Injectable({ providedIn: 'root' })
export class KpiInfoFlairFactory implements FlairFactory {
  flairType = 'kpi-info';

  constructor(
    private modalService: ModalService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private cardCacheService: CardCacheService,
  ) { }

  createFlair(config: KpiInfoFlairConfig, widgetInjector: Injector): Flair {
    const dataContext = widgetInjector.get(PageDataContext);
    return new KpiInfoFlair(
      config,
      dataContext,
      this.modalService,
      this.componentFactoryResolver,
      widgetInjector,
      this.cardCacheService,
    );
  }
}

export class KpiInfoFlair implements Flair {

  icons: Observable<FlairIcon[]> = of([
    {
      icon: 'shared-ui-info-circle',
      label: 'Show KPI details',
      invoke: () => this.showOverlay()
    }
  ]);

  constructor(
    public config: KpiInfoFlairConfig,
    private binder: PageDataContext,
    private modalService: ModalService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private injector: Injector,
    private cardCacheService: CardCacheService,
  ) { }

  showOverlay() {
    const modalRef = this.modalService.createModal(
      KpiModalComponent,
      this.componentFactoryResolver,
      this.injector
    );
    const modal = modalRef.instance;
    modal.show();
    modal.kpiData = this.binder.bind(this.config.kpiId).pipe(
      switchMap(kpi => this.cardCacheService.get('kpi', kpi as string) as Observable<CardData<KpiCardFields>>)
    );
  }

}
