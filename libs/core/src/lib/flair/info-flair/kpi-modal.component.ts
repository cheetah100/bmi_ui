import { Component, EventEmitter, Input, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';

import { CardData, CardFields } from '@bmi/gravity-services';
import { ModalComponent } from '@bmi/ui';
import { Modal } from './info-modal.component';
export interface KpiCardFields extends CardFields {
  name: string;
  description: string;
  pocs: string[];
  additional_links: string[];
  source_of_record_links: string[];
}

interface PageLink {
  title: string;
  url: string;
}

@Component({
  templateUrl: './kpi-modal.component.html',
  styleUrls: ['./kpi-modal.component.scss'],
})
export class KpiModalComponent implements Modal, OnInit, OnDestroy {
  @ViewChild(ModalComponent, { static: true }) private modalComponent: ModalComponent;

  @Input() kpiData: Observable<CardData<KpiCardFields>> = null;

  modalClosed = new EventEmitter<void>();
  pocs: { name: string, id: string }[] = [];
  sorLinks: PageLink[] = [];
  additionalLinks: PageLink[] = [];
  isLoading: boolean = true;
  kpiFields: KpiCardFields = null;
  subscription = new Subscription();

  ngOnInit() {
    this.subscription.add(this.kpiData.subscribe((kpiCard: CardData<KpiCardFields>) => {
      this.kpiFields = kpiCard.fields;
      this.pocs = this.kpiFields.pocs.map((poc: string) => {
        const [id, name] = poc.split('|');
        if (name || id) {
          return { name, id };
        }
      }).filter(Boolean);
      this.sorLinks = this.mapLinks(this.kpiFields.source_of_record_links);
      this.additionalLinks = this.mapLinks(this.kpiFields.additional_links);
      this.isLoading = false;
    }));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  mapLinks(joinedLinks: string[]): { title: string, url: string }[] {
    return joinedLinks.map((link: string) => {
      const [title, url] = link.split('|');
      if (url) {
        return { title, url };
      }
    }).filter(Boolean);
  }

  show<T>(): Observable<any> {
    this.modalComponent.styles = {
      width: 'auto',
      minWidth: '48rem',
      maxWidth: '64rem',
    };
    this.modalComponent.show();
    return null;
  }

  onModalClosed() {
    this.modalClosed.emit();
  }

  close() {
    this.modalComponent.close();
  }

}
