import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { SafeHtml } from '@angular/platform-browser';

import { ModalComponent } from '@bmi/ui';
import { Observable } from 'rxjs';

/**
 * Badness alert
 * This should really be imported from shared-ui.
 * As far as I can tell though, interfaces can't be exported unless they are with something angular.
 * The only way I know to do this is add a dummy service in the same file, and export that in the 
 * library module.
 * Which is why I'm repeating the definition here. It doesn't seem any worse than that solution
 *
 * Well, the interface could be moved to a different file in shared-ui. But I'm hoping
 * somebody knows how to do this properly.
 */
export interface Modal {
  modalClosed?: Observable<void>;
  show(): Observable<any>;
}

@Component({
  template: `
    <ui-modal (modalClosed)="onModalClosed()">
      <div *ngIf="rawHtmlContent" [innerHtml]="rawHtmlContent"></div>
      <ng-content *ngIf="!rawHtmlContent"></ng-content>
    </ui-modal>
  `,
})
export class InfoModalComponent implements Modal {
  @ViewChild(ModalComponent, { static: true }) private modalComponent: ModalComponent;

  @Input() public rawHtmlContent: SafeHtml = null;

  modalClosed = new EventEmitter<void>();

  show<T>(): Observable<any> {
    this.modalComponent.styles = {
      width: 'auto',
      minWidth: '64rem',
      maxWidth: '80vw',
    };
    this.modalComponent.show();
    return null;
  }

  onModalClosed() {
    this.modalClosed.emit();
  }

  close() {
    this.modalComponent.close();
  }

}
