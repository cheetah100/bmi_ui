import { Component, OnInit, OnDestroy } from '@angular/core';

import { Subscription } from 'rxjs';

import { TypedFormGroup, TypedFormControl, OptionGroup, DynamicEditorContext } from '@bmi/ui';
import { ObjectMap } from '@bmi/utils';
import { OptionlistService } from '../../services/optionlist.service';

import { ParameterBinding } from '../../page-data-context/parameter-binding';

import { KpiInfoFlairConfig } from './kpi-info-flair';


@Component({
  selector: 'bmi-kpi-flair-editor',
  template: `
    <ng-container [formGroup]="form">

      <ui-form-field label="KPI">
        <bmi-formatter-picker formControlName="kpiId" preferredType="optionlist" [hints]="{optionGroups: kpiOptions}"></bmi-formatter-picker>
      </ui-form-field>

    </ng-container>
  `
})
export class KpiFlairEditorComponent implements OnInit, OnDestroy {

  private subscription = new Subscription();

  readonly form = new TypedFormGroup<KpiInfoFlairConfig>({
    flairType: new TypedFormControl('kpi-flair'),
    kpiId: new TypedFormControl(null),
  });

  kpiOptions: OptionGroup[];

  constructor(
    private flairEditorContext: DynamicEditorContext<KpiInfoFlairConfig>,
    private optionlistService: OptionlistService,
  ) { }

  ngOnInit() {
    this.subscription.add(
      this.optionlistService.getOptionGroups('kpi').subscribe(options => this.kpiOptions = options)
    );

    this.subscription.add(this.flairEditorContext.config.subscribe(
      config => this.form.patchValue(config, { emitEvent: false })
    ));
    this.subscription.add(this.form.valueChanges.subscribe(
      value => this.flairEditorContext.setConfig(value)
    ));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
