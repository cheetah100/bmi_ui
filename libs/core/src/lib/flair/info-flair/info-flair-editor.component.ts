import { Component, OnInit, OnDestroy } from '@angular/core';

import { Subscription } from 'rxjs';

import { TypedFormGroup, TypedFormControl, DynamicEditorContext } from '@bmi/ui';
import { ObjectMap } from '@bmi/utils';
import { PathResolver } from '@bmi/gravity-services';

import { ParameterBinding } from '../../page-data-context/parameter-binding';
import { PageDataContext } from '../../page-data-context/page-data-context';

import { WidgetInfoFlairConfig } from './info-flair';


@Component({
  selector: 'bmi-info-flair-editor',
  template: `
    <ng-container [formGroup]="form">

      <ui-form-field label="Template">
        <textarea formControlName="template"></textarea>
      </ui-form-field>

      <ui-form-field>
        <bmi-data-map-editor formControlName="fields" [resolver]="resolver"></bmi-data-map-editor>
      </ui-form-field>

    </ng-container>
  `,
  styles: [`
textarea {
  resize: vertical;
  border: 1px solid var(--gra-color--gray-400);
  height: 5em;
}
  `],
})
export class InfoFlairEditorComponent implements OnInit, OnDestroy {
  private subscription = new Subscription();

  readonly form = new TypedFormGroup<WidgetInfoFlairConfig>({
    flairType: new TypedFormControl('info-flair'),
    fields: new TypedFormControl<ObjectMap<ParameterBinding>>({}),
    template: new TypedFormControl('')
  });

  resolver: PathResolver = null;

  constructor(
    private flairEditorContext: DynamicEditorContext<WidgetInfoFlairConfig>,
    private pageDataContext: PageDataContext
  ) { }

  ngOnInit() {
    this.subscription.add(
      this.flairEditorContext.config.subscribe(
        config => this.form.patchValue(config, { emitEvent: false })
      )
    );

    this.subscription.add(
      this.form.valueChanges.subscribe(value => this.flairEditorContext.setConfig(value))
    );

    this.resolver = this.pageDataContext.getPathResolverNow();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
