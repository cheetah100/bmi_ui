import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { TypedFormGroup, TypedFormControl, DynamicEditorContext } from '@bmi/ui';

import { ScrollFlairConfig } from './scroll-flair';

@Component({
  selector: 'bmi-scroll-flair-editor',
  template: `
    <ng-container [formGroup]="form">
      <ui-form-field label="Widget maximum height">
        <input formControlName="height" type="number" />
      </ui-form-field>
      <ui-form-field label="Fixed (maintain height when empty)">
        <ui-checkbox formControlName="fixed"></ui-checkbox>
      </ui-form-field>
    </ng-container>
  `
})
export class ScrollFlairEditorComponent implements OnInit, OnDestroy {
  private subscription = new Subscription();

  readonly form = new TypedFormGroup<ScrollFlairConfig>({
    flairType: new TypedFormControl<string>('scroll-flair'),
    height: new TypedFormControl<number>(400),
    fixed: new TypedFormControl<boolean>(true)
  });

  constructor(private flairEditorContext: DynamicEditorContext<ScrollFlairConfig>) {}

  ngOnInit() {
    this.subscription.add(
      this.flairEditorContext.config.subscribe(config =>
        this.form.patchValue(config, { emitEvent: false })
      )
    );
    this.subscription.add(
      this.form.valueChanges.subscribe(value =>
        this.flairEditorContext.setConfig(value)
      )
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
