import { Injectable, Injector } from '@angular/core';
import { Subscription } from 'rxjs';

import { BaseWidgetModifierFlairFactory } from '../base-widget-modifier-flair';
import { FlairConfig } from '../flair-config';
import { WidgetWrapperContext } from '../../components/widget-wrapper/widget-wrap';

export interface ScrollFlairConfig extends FlairConfig {
  height: number;
  fixed: boolean;
}

@Injectable({ providedIn: 'root' })
export class ScrollFlairFactory extends BaseWidgetModifierFlairFactory<ScrollFlairConfig> {
  flairType = 'scroll';

  addModifiers(config: ScrollFlairConfig, context: WidgetWrapperContext) {
    return context.modify({
      height: {
        min: config.fixed ? `${config.height}px` : 'auto',
        max: `${config.height}px`
      }
    });
  }
}
