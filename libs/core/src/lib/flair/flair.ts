import { InjectionToken, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { FlairConfig } from './flair-config';


export interface Flair<TConfig extends FlairConfig = FlairConfig> {
  readonly config: TConfig;
  icons?: Observable<FlairIcon[]>;

  /**
   * An optional cleanup function for this flair.
   */
  destroy?(): void;
}



export interface FlairIcon {
  /** The icon id to display for this flair */
  icon: string;

  /** Label text. This might be a menu item, or a tooltip */
  label: string;

  /**
   * An optional action to perform when the flair is clicked.
   *
   * If this method exists, it indicates there is some action that can be
   * performed when this piece of flair is activated.
   */
  invoke?(): void;
}


export const FLAIR_FACTORY_TOKEN = new InjectionToken<FlairFactory>('@bmi/core Flair Factory');


export interface FlairFactory {
  flairType: string;
  createFlair(config: FlairConfig, localInjector: Injector): Flair;
}

