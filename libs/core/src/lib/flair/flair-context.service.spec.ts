import { Injector } from '@angular/core'

import { Observable, of, Subject, EMPTY, Notification } from 'rxjs';
import { map, materialize } from 'rxjs/operators';

import { Flair, FlairIcon, FlairFactory } from './flair';
import { FlairConfig } from './flair-config';
import { FlairContextService,  } from './flair-context.service';


interface TestFlairConfig extends FlairConfig {
  icons?: Subject<FlairIcon[]>;
  id?: string;
  onDestroy?();
}


class TestFlair implements Flair<TestFlairConfig> {
  icons = this.config.icons || EMPTY;
  constructor (public config: TestFlairConfig) {}

  destroy() {
    if (this.config.onDestroy) {
      this.config.onDestroy();
    }
  }
}


// The flair service may have some slight asynchrony so this helper lets us
// defer doing some assertions until the next tick. A more robust strategy would
// probably be using fakeAsync for this...
function wait(): Promise<void> {
  return new Promise((resolve, reject) => {
    setTimeout(() => resolve(), 0);
  });
}


const testFlairFactory: FlairFactory = {
  flairType: 'test-flair',
  createFlair: config => new TestFlair(config as TestFlairConfig)
};


function makeIcon(label: string): FlairIcon {
  return { icon: 'test', label: label };
}


describe('FlairContextService', () => {
  let flairContext: FlairContextService;
  let localInjector: Injector;

  beforeEach(() => {
    localInjector = Injector.create({providers: []})
    flairContext = new FlairContextService([testFlairFactory], localInjector);
  });

  describe('Initializing', () => {
    describe('Empty/null values', () => {
      afterEach(() => expect(flairContext.currentFlairs).toEqual([]));
      it('should handle an empty array of configs', () => flairContext.initialize([]));
      it('should handle a null list of configs (as if empty)', () => flairContext.initialize(null));
      it('should handle undefined configs array (as if empty array)', () => flairContext.initialize(undefined));
    });

    it('should handle a supported type of flair config', () => {
      const testConfig = {flairType: 'test-flair'};
      flairContext.initialize([testConfig]);
      expect(flairContext.currentFlairs.length).toEqual(1);
      expect(flairContext.currentFlairs[0].config).toEqual(testConfig);
    });


    describe('With an existing config', () => {
      beforeEach(() => flairContext.initialize([{flairType: 'test-flair'}]));

      it('should be cleared if initialized with []', () => {
        flairContext.initialize([]);
        expect(flairContext.currentFlairs).toEqual([]);
      });

      it('should be cleared if initialized with null', () => {
        flairContext.initialize(null);
        expect(flairContext.currentFlairs).toEqual([]);
      })
    });

    it('should skip over unsupported flair types', () => {
      flairContext.initialize([{flairType: 'unsupported'}, {flairType: 'test-flair'}]);
      expect(flairContext.currentFlairs.map(f => f.config.flairType)).toEqual(['test-flair']);
    });
  });

  describe('Flair icons', () => {
    let emittedFlairIcons: FlairIcon[][] = [];
    let iconEvents: Notification<FlairIcon[]>[] = [];

    beforeEach(() => {
      emittedFlairIcons = [];
      iconEvents = [];
      flairContext.icons.subscribe(icons => emittedFlairIcons.push(icons));
      flairContext.icons.pipe(materialize()).subscribe(event => iconEvents.push(event));
    });

    function last<T>(items: T[]): T {
      return items[items.length - 1];
    }

    function latestIcons() {
      return last(emittedFlairIcons);
    }

    it('should initially have no flair icons', () => {
      expect(emittedFlairIcons[0]).toEqual([]);
    });

    it('should not emit more flair icons until initialized', done => {
      wait()
        .then(() => expect(emittedFlairIcons.length).toBe(1))
        .finally(done);
    });

    describe('With a flair that produces icons', () => {
      let iconSubject: Subject<FlairIcon[]>;

      beforeEach(() => {
        iconSubject = new Subject<FlairIcon[]>();
        flairContext.initialize([{flairType: 'test-flair', icons: iconSubject}]);
      });

      it('should emit an empty array for the latest icons', done => {
        wait()
          .then(() => expect(latestIcons()).toEqual([]))
          .finally(done);
      });

      it('should have emitted icons again after the flair was initialized', done => {
        wait()
          .then(() => expect(emittedFlairIcons.length).toEqual(2))
          .finally(done)
      });

      it('should emit icons when the flair produces them', done => {
        const testIcon: FlairIcon = makeIcon('Test Icon');
        iconSubject.next([testIcon]);
        wait().then(() => expect(latestIcons()).toEqual([testIcon])).finally(done);
      });

      it('should re-emit the icons if they change again', done => {
        const testIcon = makeIcon('Test Icon');
        const testIcon2 = makeIcon('Test Icon 2');
        iconSubject.next([testIcon]);
        wait()
          .then(() => iconSubject.next([testIcon, testIcon2]))
          .then(wait)
          .then(() => expect(latestIcons()).toEqual([testIcon, testIcon2]))
          .finally(done);
      });
    });

    describe('With multiple icon-producing flairs', () => {
      let iconsA: Subject<FlairIcon[]>;
      let iconsB: Subject<FlairIcon[]>;

      beforeEach(() => {
        iconsA = new Subject<FlairIcon[]>();
        iconsB = new Subject<FlairIcon[]>();
        flairContext.initialize([
          {flairType: 'test-flair', icons: iconsA},
          {flairType: 'test-flair', icons: iconsB}
        ]);
      });

      it('should emit icons from flairs as they become available', done => {
        const icons = [makeIcon('1'), makeIcon('2')];
        iconsB.next(icons);

        wait()
          .then(() => expect(latestIcons()).toEqual(icons))
          .finally(done);
      });

      it('should emit icons in the order the flairs are defined', done => {
        iconsB.next([makeIcon('B1'), makeIcon('B2')]);
        wait()
          .then(() => iconsA.next([makeIcon('A1'), makeIcon('A2')]))
          .then(wait)
          .then(() => expect(latestIcons().map(icon => icon.label)).toEqual(['A1', 'A2', 'B1', 'B2']))
          .finally(done);
      });
    });

    it('should handle flairs that do not define any icons', done => {
      flairContext.initialize([{flairType: 'test-flair', icons: null}]);
      wait()
        .then(() => expect(emittedFlairIcons).toEqual([[], []]))  // Initial, then after creating the flair
        .finally(done);
    });

    describe('When the service instance is destroyed', () => {
      beforeEach(() => {
        flairContext.initialize([{flairType: 'test-flair', icons: of([makeIcon('Test')])}]);
        flairContext.destroy();
      });

      it('should emit an empty array of icons', done => {
        wait()
          .then(() => expect(latestIcons()).toEqual([]))
          .finally(done);
      });

      it('should complete the stream of icons', done => {
        wait()
          .then(() => expect(last(iconEvents).kind).toBe('C'))
          .finally(done);
      });
    });
  });

  describe('Dynamically created flairs', () => {
    let emittedFlairs: Flair[][];

    beforeEach(() => {
      emittedFlairs = [];
      flairContext.flairs.subscribe(flairs => emittedFlairs.push(flairs));
    });

    it('should support adding a flair', () => {
      const flair = flairContext.addFlair({flairType: 'test-flair'});
      expect(flairContext.currentFlairs).toContain(flair);
    });

    it('should emit a new flairs array containing the new flair', () => {
      const flair = flairContext.addFlair({flairType: 'test-flair'});
      expect(emittedFlairs).toEqual([[], [flair]]);
    });

    it('should allow created flairs to be removed', () => {
      const flair = flairContext.addFlair({flairType: 'test-flair'});
      flairContext.removeFlair(flair);
      expect(emittedFlairs).toEqual([[], [flair], []])
    });

    it('should call destroy on the removed flairs', () => {
      let destroyCount = 0;
      const flair = flairContext.addFlair({flairType: 'test-flair', onDestroy: () => destroyCount += 1});
      flairContext.removeFlair(flair);
      expect(destroyCount).toBe(1);
    });

    describe('Dynamic flairs and config flairs', () => {
      let configFlairs: Flair[];

      beforeEach(() => {
        flairContext.initialize([
          {flairType: 'test-flair', id: 'A'},
          {flairType: 'test-flair', id: 'B'}
        ]);

        configFlairs = [...flairContext.currentFlairs];
      });

      it('should put newly added dynamic flairs before any config ones', () => {
        const flair = flairContext.addFlair({flairType: 'test-flair', id: 'C'});
        expect(flairContext.currentFlairs).toEqual([flair, ...configFlairs]);
      });

      describe('clearDynamicFlairs()', () => {
        let flair: Flair;
        let destroyCount: number;

        beforeEach(() => {
          destroyCount = 0;
          flair = flairContext.addFlair({flairType: 'test-flair', onDestroy: () => destroyCount += 1});
          flairContext.clearDynamicFlairs();
        });

        it('should remove just the dynamic flairs', () => {
          expect(flairContext.currentFlairs).toEqual(configFlairs);
        });

        it('should call destroy() on the dynamic flairs', () => {
          expect(destroyCount).toEqual(1);
        });
      });
    });
  });

});
