import { Component, Input, OnInit, OnChanges, SimpleChanges, SimpleChange, Optional, EventEmitter, Output, Injector } from '@angular/core';

import { BehaviorSubject, Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

import { ObjectMap } from '@bmi/utils';
import { distinctUntilNotEqual } from '../util/distinct-until-not-equal';

import { PageConfig } from '../page-config';
import { WidgetConfig } from '../widget-config';
import { WidgetMode } from '../widget-context';
import { PageEditorContext } from '../page-editor/page-editor-context';

import { FilterService } from '../filters/filter.service';
import { PageInfoService } from './page-info.service';
import { PageDataContext } from '../page-data-context/page-data-context';
import { provideDataContextServices } from '../page-data-context/helpers';
import { Binder } from '../page-data-context/binder';

@Component({
  selector: 'bmi-page',
  templateUrl: './page.component.html',
  styleUrls: ['./base-fixes.scss'],
  providers: [
    PageInfoService,
    provideDataContextServices()
  ]
})
export class PageComponent implements OnInit, OnChanges {
  @Input() config: PageConfig;
  @Input() filters: ObjectMap<string>;
  @Input() mode: WidgetMode = 'view';

  @Output() filtersChange = new EventEmitter<ObjectMap<string>>(true);

  private filterValuesSubject = new BehaviorSubject<ObjectMap<string>>({});
  private resizeEventSubject = new Subject<null>();

  get rootWidgetConfig(): WidgetConfig {
    return this.config && this.config.ui && this.config.ui.rootWidget;
  }

  /**
   * Provides access to this component's injector instance.
   *
   * This should allow external access to the page's filter service and data
   * context. The intended use for this is passing the injector across to the
   * sidebar when editing page properties.
   */
  public get injector(): Injector {
    return this._injector;
  }

  public get binder(): Binder {
    return this.pageDataContext;
  }

  constructor(
    private pageInfoService: PageInfoService,
    private pageDataContext: PageDataContext,
    private filterService: FilterService,
    @Optional() private pageEditorContext: PageEditorContext,
    private _injector: Injector,
  ) { }

  ngOnInit() {
    this.filterValuesSubject.subscribe(filters => {
      for (const [filterId, value] of Object.entries(filters)) {
        this.filterService.set(filterId, value);
      }
    });

    this.filterService.rawFilterValues.pipe(
      debounceTime(15),
      distinctUntilNotEqual(),
    ).subscribe(filters => {
      this.filtersChange.next(filters);
    });

    this.resizeEventSubject.pipe(
      debounceTime(15),
    ).subscribe(() => this.resizeWidgets());

  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.config) {
      this.pageInfoService.setPageConfig(this.config);
      this.pageDataContext.setConfig(this.config.ui.dataContext || {});
    }

    if (changes.filters || changes.config) {
      this.filterValuesSubject.next(this.filters || {});
    }
  }

  widgetConfigUpdated(widgetConfig: WidgetConfig) {
    if (this.pageEditorContext) {
      this.pageEditorContext.updatePageConfig(c => c.ui.rootWidget = widgetConfig);
    }
  }

  requestWidgetsResize(): void {
    this.resizeEventSubject.next(null);
  }

  /**
   * Simple, but a bit hacky.
   * Triggering window resize means that highcharts instances redraw to fill their space.
   * This could be changed to maybe asking each widget to Chart.resize()
   */
  resizeWidgets(): void {
    window.dispatchEvent(new Event('resize'));
  }

}
