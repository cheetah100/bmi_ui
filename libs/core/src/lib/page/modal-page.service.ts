import { Injectable, Injector } from '@angular/core';
import { ModalService } from '@bmi/ui';
import { ObjectMap } from '@bmi/utils';
import { isObservable, Observable, of } from 'rxjs';
import { filter } from 'rxjs/operators';
import { NotesFlairConfig } from '../flair/notes-flair/notes-flair';
import { BmiPageRoute } from '../navigation/bmi-route';
import { resolveFilterBindings } from '../navigation/bmi-route-utils';
import { PageConfig } from '../page-config';
import { Binder } from '../page-data-context/binder';
import { PageDataContext } from '../page-data-context/page-data-context';
import { PageRepositoryService } from '../services/page-repository.service';
import { ModalPageComponent } from './modal-page.component';


export interface ShowPageOptions {
  pageConfig: PageConfig | Observable<PageConfig>,
  filters?: ObjectMap<string> | Observable<ObjectMap<string>>,
  dataContext?: PageDataContext,
  injector?: Injector,
  size?: 'large' | 'small',
}


@Injectable({ providedIn: 'root' })
export class ModalPageService {
  constructor(
    private modalService: ModalService,
    private injector: Injector,
    private pageRepositoryService: PageRepositoryService,
  ) { }

  pageConfig(pageRoute: BmiPageRoute): Observable<PageConfig> {
    return this.pageRepositoryService.getPage(
      pageRoute.moduleId,
      pageRoute.pageId
    ).pipe(
      filter((config: PageConfig) => !!config)
    );
  }

  pageFilters(pageRoute: BmiPageRoute, binder: Binder): Observable<ObjectMap<string>> {
    return resolveFilterBindings(pageRoute.filters, binder);
  }

  /** @deprecated use showPage instead, move this to the flair implementation  */
  showOverlay(config: NotesFlairConfig, binder: Binder) {
    return this.showPage(
      this.pageConfig(config.notesPageRoute),
      this.pageFilters(config.notesPageRoute, binder),
    );
  }

  show(options: ShowPageOptions) {
    const pageInjector = Injector.create({
      name: 'BMI Page Modal Injector',
      parent: options.injector ?? this.injector,
      providers: [
        options.dataContext ? {provide: PageDataContext, useValue: options.dataContext } : []
      ],
    });

    return this.modalService.open({
      title: null,
      content: ModalPageComponent,
      size: options.size ?? 'large',
      injector: pageInjector,
      data: {
        pageConfig: isObservable(options.pageConfig) ? options.pageConfig : of(options.pageConfig),
        filters: isObservable(options.filters) ? options.filters : of(options.filters || {})
      }
    });
  }

  showPage(
    pageConfig: PageConfig | Observable<PageConfig>,
    filters: ObjectMap<string> | Observable<ObjectMap<string>>,
  ) {
    return this.show({pageConfig, filters});
  }
}
