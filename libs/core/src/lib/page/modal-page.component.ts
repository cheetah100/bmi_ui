import { Component, ViewChild, Input, EventEmitter, OnInit, Inject } from '@angular/core';
import { ModalComponent, ModalContext, ModalResult } from '@bmi/ui';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';
import { BmiPageRoute } from '../navigation/bmi-route';
import { PageConfig } from '../page-config';
import { ObjectMap } from '@bmi/utils';


interface ModalPageOptions {
  pageConfig: Observable<PageConfig>;
  filters: Observable<ObjectMap<string>>;
}


@Component({
  selector: 'bmi-modal-page',
  template: `
    <ui-observable-loader [source]="pageConfig">
      <ng-template>
        <bmi-page
          [config]="pageConfig | async"
          [filters]="filters | async">
        </bmi-page>
      </ng-template>
    </ui-observable-loader>
  `,
})
export class ModalPageComponent {
  constructor(
    private modalContext: ModalContext<any>,
  ) {}

  readonly options = this.modalContext.data;

  get pageConfig() {
    return this.options.pageConfig;
  }

  get filters() {
    return this.options.filters;
  }
}
