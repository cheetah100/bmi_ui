import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, distinctUntilChanged, publishReplay, refCount } from 'rxjs/operators';
import { PageConfig, PageInfo } from '../page-config';
import { makePageInfo } from '../util/make-page-info';

import lodashIsEqual from 'lodash-es/isEqual';


@Injectable()
export class PageInfoService {
  private pageConfigSubject = new BehaviorSubject<PageConfig>(null);

  /**
   * Observe the metadata about the current page instance.
   */
  get pageInfo(): Observable<PageInfo> {
    return this.pageConfigSubject.pipe(
      map(config => makePageInfo(config)),
      distinctUntilChanged((a, b) => lodashIsEqual(a, b)),
      publishReplay(1),
      refCount()
    );
  }

  get pageConfig(): Observable<PageConfig> {
    return this.pageConfigSubject.asObservable();
  }

  get currentModuleId(): string {
    return this.pageConfigSubject.value ? this.pageConfigSubject.value.applicationId : null;
  }

  setPageConfig(pageConfig: PageConfig) {
    this.pageConfigSubject.next(pageConfig);
  }
}
