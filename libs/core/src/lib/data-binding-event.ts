
export interface DataBindingValueEvent<T = unknown> {
  type: 'value';
  value: T;
}


export interface DataBindingLoadingEvent {
  type: 'loading';
}


export type DataBindingStatusEvent = DataBindingLoadingEvent;

export type DataBindingEvent<T = unknown> = DataBindingValueEvent<T> | DataBindingStatusEvent;



export function makeValueEvent<T>(value: T): DataBindingValueEvent<T> {
  return {type: 'value', value};
}


export function makeLoadingEvent(): DataBindingLoadingEvent {
  return {type: 'loading'};
}
