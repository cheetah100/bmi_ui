import { ObjectMap } from '@bmi/utils';
import { ParameterBinding } from '../page-data-context/parameter-binding';
import { EventActionConfig } from '../events';

/**
 * Common configuration for a card editor
 *
 * This was refactored out of the CardEditForm so that it can be reused by
 * similar widgets or components -- such as when configuring a card editor
 * popup.
 */
export interface CardEditorConfig {
  board: string;
  cardId: ParameterBinding;
  defaults?: ObjectMap<ParameterBinding>;

  afterSaveEvent?: EventActionConfig[];
}
