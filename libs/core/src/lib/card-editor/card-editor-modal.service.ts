import { Injectable } from '@angular/core';
import { switchMap, mapTo } from 'rxjs/operators';
import { ConfirmationModalService, ErrorModalService } from '@bmi/ui';
import { BoardService } from '@bmi/gravity-services';
import { uniqueId, ObjectMap } from '@bmi/utils';
import { PageConfig } from '../page-config';
import { WidgetConfig } from '../widget-config';
import { ModalPageService, ShowPageOptions } from '../page/modal-page.service'
import { CardEditorConfig } from '../card-editor/card-editor-config';
import { CardEditFormGeneratorWidgetFields } from '../widgets/card-edit-form-generator/card-edit-form-generator.service';
import dedent from 'ts-dedent';


@Injectable({providedIn: 'root'})
export class CardEditorModalService {
  constructor(
    private modalPageService: ModalPageService,
    private confirmationModalService: ConfirmationModalService,
    private errorModal: ErrorModalService,
    private boardService: BoardService,
  ) {}

  editCard(boardId: string, cardId: string) {
    return this.show({board: boardId, cardId: cardId});
  }

  createCard(boardId: string) {
    return this.editCard(boardId, null);
  }

  /**
   * Shows a confirmation to delete a card, and then delete it.
   */
  deleteCard(boardId: string, cardId: string) {
    return this.confirmationModalService.show({
      content: dedent`
          ## Confirm deletion of ${boardId} card ##

          Are you sure you want to delete this *${boardId}* card with id ${cardId}?
        `,
      buttonYesText: 'Delete Card',
      emitCancelEvent: false,
    }).pipe(
      switchMap(result => this.boardService.deleteCard(boardId, cardId).pipe(
        this.errorModal.catchError({retryable: true}),
        mapTo(result)
      ))
    );
  }

  /**
   * Shows the card editor modal
   *
   * This is the fully featured method which allows data binding to take place,
   * as you can use the options parameter to pass through some settings to the
   * page modal, such as the injector/data context to use.
   *
   * @param config The card editor config -- specifies the board, card, defaults
   *     etc. These values can have data bindings which will be resolved against
   *     the data context provided. If no data context is provided, then this
   *     will probably be null, as the root injector doesn't have a data context
   * @param options to pass along to the ModalPageService
   */
  show(config: CardEditorConfig, options?: Partial<ShowPageOptions>) {
    return this.modalPageService.show({
      ...options ?? {},
      pageConfig: this.createCardEditorPage(config)
    });
  }

  private createCardEditorPage(config: CardEditorConfig): PageConfig {
    return createPageConfig({
      title: 'Edit Card',
      rootWidget: <WidgetConfig<CardEditFormGeneratorWidgetFields>>{
        widgetType: 'card-edit-form-generator',
        fields: {
          ...config,
          afterSaveEvent: [
            {type: 'trigger-close-modal'}
          ]
        }
      }
    })
  }
}


interface CreatePageOptions<T extends WidgetConfig> {
  title: string;
  dataContext?: ObjectMap<any>;
  rootWidget: T;
}


function createPageConfig<T extends WidgetConfig>(options: CreatePageOptions<T>): PageConfig<T> {
  return {
    applicationId: '__bmi',
    type: 'page-v2',
    id: uniqueId(),
    name: options.title,
    permissions: {},
    ui: {
      dataContext: options.dataContext || {},
      rootWidget: options.rootWidget
    }
  }
}
