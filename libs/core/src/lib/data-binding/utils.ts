import { ParameterBinding } from '../page-data-context/parameter-binding';

// Where possible these functions should stick to the ngc-compatible subset of
// Typescript, allowing them to be used in Angular Metadata (e.g. decorators for
// modules and components.)

// Avoid object and array spread syntax (...), lambdas and basically anything
// dynamic. It gets evaluated by substitution.


/**
 * A macro function for defining a path binding.
 * @param path - the dotted path to bind, e.g. `this.id`
 */
export function pathBinding(path: string): ParameterBinding {
  return {bindingType: 'data-path', path: path};
}


/**
 * A macro function for defining a filter binding
 * @param filterId - the id of the filter to bind
 */
export function filterBinding(filterId: string): ParameterBinding {
  return {bindingType: 'filter', filterId: filterId};
}
