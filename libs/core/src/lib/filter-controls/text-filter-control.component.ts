import { Component, Input, OnInit } from '@angular/core';
import { EMPTY, ReplaySubject } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { TypedFormControl } from '@bmi/ui';
import { Filter } from '../filters/filter';

@Component({
  selector: 'bmi-core-text-filter-control',
  template: `
    <ui-form-field [label]="filter.name">
      <input type="text"  [formControl]="control" />
    </ui-form-field>
  `
})
export class TextFilterControlComponent implements OnInit {
  @Input() filter: Filter<string>;
  readonly control = new TypedFormControl<string>('');

  private onFilterChanged = new ReplaySubject<Filter<string>>(1);

  ngOnInit() {
    this.onFilterChanged.pipe(
      switchMap(filter => filter ? filter.state : EMPTY )
    ).subscribe(state => {
      this.control.patchValue(state.rawValue, {emitEvent: false});
    });

    this.control.valueChanges.subscribe(value => this.filter.set(value));
  }

  ngOnChanges(changes) {
    if (changes.filter) {
      this.onFilterChanged.next(this.filter);
    }
  }
}
