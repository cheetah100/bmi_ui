import { Component, Input } from '@angular/core';
import { Filter } from '../filters/filter';

@Component({
  selector: 'bmi-core-filter-control',
  template: `
    <ng-container *ngIf="filter" [ngSwitch]="type">
      <bmi-core-text-filter-control *ngSwitchCase="'text'" [filter]="filter"></bmi-core-text-filter-control>
      <bmi-core-single-select-filter-control *ngSwitchCase="'single-select'" [filter]="filter"></bmi-core-single-select-filter-control>
    </ng-container>
  `
})
export class FilterControlComponent {
  @Input() type: string = 'text';
  @Input() filter: Filter;
}
