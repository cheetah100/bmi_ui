import { Component, Input, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { EMPTY, Observable, of, ReplaySubject } from 'rxjs';
import { publishReplay, refCount, switchMap } from 'rxjs/operators';
import { TypedFormControl, Option } from '@bmi/ui';
import { Filter } from '../filters/filter';

@Component({
  selector: 'bmi-core-single-select-filter-control',
  template: `
    <ui-form-field [label]="filter.name">
      <ui-single-select
        [floating]="true"
        [formControl]="control"
        [options]="options | async"
        [optionGroups]="optionGroups | async"
        preferenceId="filter_{{filter.id}}">
      </ui-single-select>
    </ui-form-field>
  `
})
export class SingleSelectFilterControlComponent implements OnInit, OnChanges {
  @Input() filter: Filter<string>;
  readonly control = new TypedFormControl<string>('');

  private onFilterChanged = new ReplaySubject<Filter<string>>(1);

  readonly options = this.onFilterChanged.pipe(
    switchMap(filter => filter.options && !filter.optionGroups ? filter.options : of(undefined)),
    publishReplay(1),
    refCount()
  );

  readonly optionGroups = this.onFilterChanged.pipe(
    switchMap(filter => filter.optionGroups || of(undefined)),
    publishReplay(1),
    refCount()
  )

  ngOnInit() {
    this.onFilterChanged.pipe(
      switchMap(filter => filter ? filter.state : EMPTY )
    ).subscribe(state => {
      this.control.patchValue(state.rawValue, {emitEvent: false});
    });

    this.control.valueChanges.subscribe(value => this.filter.set(value));
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.filter) {
      this.onFilterChanged.next(this.filter);
    }
  }
}
