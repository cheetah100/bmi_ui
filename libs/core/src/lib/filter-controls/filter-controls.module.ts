import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedUiModule } from '@bmi/ui';

import { FilterControlComponent } from './filter-control.component';
import { TextFilterControlComponent } from './text-filter-control.component';
import { SingleSelectFilterControlComponent } from './single-select-filter-control.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedUiModule
  ],

  declarations: [
    FilterControlComponent,
    TextFilterControlComponent,
    SingleSelectFilterControlComponent
  ],

  exports: [
    FilterControlComponent
  ]
})
export class FilterControlsModule {}
