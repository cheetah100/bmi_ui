import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { SharedUiModule } from '@bmi/ui';
import { BmiNavigationModule } from '../navigation/bmi-navigation.module';

import { FlairIconsComponent } from './flair-icons/flair-icons.component';
import { ProfileImageComponent } from './profile-image/profile-image.component';
import { ScoreMatrixComponent } from './score-matrix/score-matrix.component';
import { ScoreMatrixRowComponent } from './score-matrix/score-matrix-row.component';
import { WidgetWrapperComponent } from './widget-wrapper/widget-wrapper.component';
import { WidgetWrapperContextDirective } from './widget-wrapper/widget-wrapper-context.directive';
import { UiCardComponent } from './ui-card/ui-card.component';
import { AddWidgetDropdownComponent } from './add-widget-dropdown/add-widget-dropdown.component';
import { HelpBoxComponent } from './help-box/help-box.component';
import { PersonComponent } from './profile-image/person.component';

import { DataContextDirective } from '../page-data-context/data-context.directive';
import { ThisPathResolverDirective } from '../page-data-context/this-path-resolver.directive';

import { EditModeDefaultPipe } from './edit-mode-default.pipe';
import { ViewableImageComponent } from './help-box/viewable-image.component';

import { ConfigurableActionButtonsComponent } from './configurable-action-buttons/configurable-action-buttons.component';

@NgModule({
  imports: [
    CommonModule,
    SharedUiModule,
    RouterModule,
    BmiNavigationModule,
  ],

  declarations: [
    FlairIconsComponent,
    ProfileImageComponent,
    ScoreMatrixComponent,
    ScoreMatrixRowComponent,
    WidgetWrapperComponent,
    WidgetWrapperContextDirective,
    UiCardComponent,
    AddWidgetDropdownComponent,
    DataContextDirective,
    ThisPathResolverDirective,
    EditModeDefaultPipe,
    HelpBoxComponent,
    ViewableImageComponent,
    ConfigurableActionButtonsComponent,
    PersonComponent,
  ],

  entryComponents: [],

  exports: [
    FlairIconsComponent,
    ProfileImageComponent,
    ScoreMatrixComponent,
    ScoreMatrixRowComponent,
    WidgetWrapperComponent,
    WidgetWrapperContextDirective,
    UiCardComponent,
    AddWidgetDropdownComponent,
    DataContextDirective,
    ThisPathResolverDirective,
    EditModeDefaultPipe,
    HelpBoxComponent,
    ViewableImageComponent,
    ConfigurableActionButtonsComponent,
    PersonComponent,

    // TODO: this should be imported elsewhere when it's needed.
    BmiNavigationModule,
  ],
})
export class BmiCoreComponentsModule {}
