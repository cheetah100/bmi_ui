import { Component, Input } from '@angular/core';

@Component({
  selector: 'bmi-profile-image',
  templateUrl: './profile-image.component.html',
  styleUrls: ['./profile-image.component.scss']
})
export class ProfileImageComponent {
  @Input() userId: string;

  get imageUrl(): string {
    if (this.userId) {
      /** assets could be local or http. used as img src */
      return `image-assets/people/${this.userId}.jpg`
    } else {
      return null;
    }
  }

  get cssImageUrl() {
    const url = this.imageUrl;
    return url ? `url(${url})` : null;
  }
}
