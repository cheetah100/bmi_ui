import { Component, Input } from "@angular/core";


@Component({
  template: `
<bmi-profile-image [userId]="userId"></bmi-profile-image>
<div class="profile-image__info">
  <h4>{{ name | editModeDefault:'[name]' }}</h4>
  <p class="profile-image__info__sub-heading" *ngIf="!!subHeading">{{ subHeading }}</p>
</div>
  `,
  styles: [`
bmi-profile-image {
  width: 48px;
  height: 48px;
  font-size: 48px;
  flex: 0 0 auto;
}
:host {
  display: flex;
  align-items: center;
}
.profile-image__info {
  margin-left: 1rem;
}
h4 {
  font-size: 1.667rem;
  margin-bottom: 0;
}
.profile-image__info__sub-heading {
  font-size: 1.25rem;
  color: var(--gra-color--gray-600);
  margin: 0;
}
  `],
  selector: 'bmi-person',
})
export class PersonComponent {

  @Input() userId: string;
  @Input() name: string;
  @Input() subHeading: string = null;

}