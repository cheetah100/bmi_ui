import { Component, Input, Optional, forwardRef, SkipSelf, OnInit } from '@angular/core';

@Component({
  selector: 'bmi-core-card',
  template: `
<ng-container *ngIf="hasCard else content">
  <div
    class="widget-card"
    [class.widget-card--edit-mode]="editing"
    [class.widget-card--show-card]="hasCard">
    <ng-container *ngTemplateOutlet="content"></ng-container>
  </div>
</ng-container>
<ng-template #content>
  <ng-content></ng-content>
</ng-template>
  `,
  styleUrls: ['./ui-card.component.scss'],
})
export class UiCardComponent {

  @Input() editing = false;
  @Input() showCard = true;

  get nested(): boolean {
    return (!!this.closestCard && this.closestCard.cardExists);
  }

  get cardExists(): boolean {
    return (this.nested || this.showCard);
  }

  get hasCard(): boolean {
    return (!this.nested && this.showCard);
  }

  constructor(
    @SkipSelf() @Optional() private closestCard?: UiCardComponent
  ) { }

}
