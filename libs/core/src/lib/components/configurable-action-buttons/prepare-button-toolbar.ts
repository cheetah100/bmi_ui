
import { TeardownLogic } from 'rxjs';
import { ButtonToolbarAction, ButtonToolbarSeparator, ButtonToolbarMenu, ButtonToolbarItem } from '@bmi/ui';
import { ConfigurableToolbarItem, ConfigurableToolbarAction } from './config';

/**
 * Converts a toolbar config with events and stuff into something that the
 * toolbar component can handle.
 *
 * What to actually do when an item is clicked isn't handled here -- that work
 * is delegated to an `invokeAction` function passed as a parameter, as the
 * caller has the context necessary to plug that logic together, this just
 * converts the configs.
 *
 * @param config the config to convert into button toolbar items
 * @param invokeAction the callback that will happen when the item is invoked,
 *     and should actually handle the work of triggering the events. The return
 *     value of the callback is a TeardownLogic which should be tied to the
 *     lifetime of the button toolbar. If this behaviour isn't desired, return
 *     void/undefined.
 */
export function prepareButtonToolbar(
  config: ConfigurableToolbarItem[],
  invokeAction: (actionConfig: ConfigurableToolbarAction) => TeardownLogic
): ButtonToolbarItem[] {
  function prepareAction(config: ConfigurableToolbarAction): ButtonToolbarAction {
    return { ...config, invoke: () => invokeAction(config) };
  }

  return config.map(item => {
    switch (item.type) {
      case 'action':
        return prepareAction(item);
      case 'menu':
        return {
          ...item,
          items: prepareButtonToolbar(item.items ?? [], invokeAction)
        }
      default:
        return item;
    }
  })
}

