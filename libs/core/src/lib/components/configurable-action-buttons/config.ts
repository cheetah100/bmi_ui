import { ButtonToolbarAction, ButtonToolbarSeparator, ButtonToolbarMenu, ButtonToolbarItem } from '@bmi/ui';
import { EventActionConfig } from '../../events';


export type ConfigurableToolbarItem = ConfigurableToolbarAction | ButtonToolbarSeparator | ConfigurableToolbarMenu;

export interface ConfigurableToolbarAction extends Omit<ButtonToolbarAction, 'invoke'> {
  onInvoke: EventActionConfig[];
}

export interface ConfigurableToolbarMenu extends Omit<ButtonToolbarMenu, 'items'> {
  items: ConfigurableToolbarItem[];
}


