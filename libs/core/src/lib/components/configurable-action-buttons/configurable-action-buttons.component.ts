import { Component, Injector, Input, OnChanges } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { distinctUntilNotEqual } from '@bmi/utils';

import { PageDataContext } from '../../page-data-context/page-data-context';
import { EventActionFactoryService } from '../../events';
import { ConfigurableToolbarItem, ConfigurableToolbarAction } from './config';
import { prepareButtonToolbar } from './prepare-button-toolbar';


@Component({
  selector:'bmi-configurable-action-buttons',
  template: `<ui-button-toolbar [items]="preparedButtons | async"></ui-button-toolbar>`
})
export class ConfigurableActionButtonsComponent implements OnChanges {
  constructor(
    private actionFactory: EventActionFactoryService,
    private dataContext: PageDataContext,
    private injector: Injector,
  ) {}

  @Input() buttons: ConfigurableToolbarItem[] = [];
  private buttonsSubject = new BehaviorSubject(this.buttons);

  preparedButtons = this.buttonsSubject.pipe(
    distinctUntilNotEqual(),
    map(buttons => prepareButtonToolbar(buttons, action => this.invokeAction(action)))
  );

  ngOnChanges(changes) {
    this.buttonsSubject.next(this.buttons ?? []);
  }

  invokeAction(action: ConfigurableToolbarAction) {
    return this.actionFactory.runActionChain(action.onInvoke, {
      dataContext: this.dataContext,
      injector: this.injector,
    }).subscribe();
  }
}
