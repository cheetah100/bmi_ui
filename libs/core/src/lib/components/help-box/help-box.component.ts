import { Component } from '@angular/core';

@Component({
  selector: 'bmi-help-box',
  template: `
    <blockquote class="blockquote--primary">
      <ng-content></ng-content>
    </blockquote>
  `
})
export class HelpBoxComponent {}
