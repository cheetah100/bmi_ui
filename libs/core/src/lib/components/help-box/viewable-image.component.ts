import {
  Component,
  HostListener,
  ViewChild,
  TemplateRef,
  Input
} from '@angular/core';
import { BmiModalOptions, ModalService } from '@bmi/ui';

@Component({
  selector: 'bmi-viewable-image',
  template: `
    <ng-template #content>
      <img src="{{ src }}" />
    </ng-template>
    <img *ngIf="!!src" src="{{ src }}" title="View image" />
  `,
  styles: [
    `
      :host {
        cursor: pointer;
      }
    `
  ]
})
export class ViewableImageComponent {
  @ViewChild('content') public templateRef: TemplateRef<any>;

  constructor(private modalService: ModalService) {}

  @Input() src: string;

  @HostListener('click')
  onClick() {
    if (!this.src) {
      return;
    }
    const options: BmiModalOptions = {
      title: '',
      content: this.templateRef,
      size: 'large',
      actions: [
        {
          action: 'cancel',
          buttonText: 'Close',
          buttonStyle: 'primary'
        }
      ]
    };
    this.modalService.open(options).subscribe(() => {});
  }
}
