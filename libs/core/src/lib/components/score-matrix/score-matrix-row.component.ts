import { Component, ElementRef, Input } from '@angular/core';
import { ScoreMatrixCell } from './score-matrix-types';
import { BmiColorService } from '../../services/bmi-color.service';

@Component({
  selector: 'bmi-core-score-matrix-row',
  template: `
<div
  *ngFor="let cell of cells"
  class="score-matrix__cell"
  [style.background-color]="getColor(cell)"
  [class.score-matrix__cell--inactive]="!!cell.inactive">
  {{ cell.text }}
</div>
<div *ngIf="label" class="score-matrix__label">{{ label }}</div>
  `,
  styles: [`
/**
 * parent component can set the height and width on <bmi-core-score-matrix-row>
 * by default, width will be container width.
 * default height will depend on whether text is used
 */
:host {
  display: flex;
  min-height: 10px;
  align-items: stretch;
  justify-content: center;
}
.score-matrix__cell,
.score-matrix__label {
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
  display: flex;
  align-items: center;
}
.score-matrix__cell {
  margin-top: 3px;
  background-color: var(--gra-color--gray-600);
  flex-basis: 100%;
  border-radius: 2.5px;
  padding: 2px;
  justify-content: center;
  font-size: .67vw;
  font-weight: 400;
  line-height: 1.75;
}
.score-matrix__cell + .score-matrix__cell {
  margin-left: 3px;
}
.score-matrix__cell--inactive {
  opacity: 0.6;
}
.score-matrix__label {
  flex-basis: 150%;
  padding: 2px 6px;
  font-weight: 700;
}
  `],
})
export class ScoreMatrixRowComponent {

  @Input() cells: ScoreMatrixCell[] = [];
  @Input() label: string = null;

  constructor(
    private colorService: BmiColorService,
  ) { }

  getColor(cell: ScoreMatrixCell): string {
    return this.colorService.getCssColor(cell.color, cell.color);
  }

}
