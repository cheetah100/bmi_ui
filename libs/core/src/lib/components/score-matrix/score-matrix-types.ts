export interface ScoreMatrixCell {
  // Making this an object for future expansion. This could have properties for
  // things like text (to display in cell), tooltip, maybe a link for each cell?

  /**
   * The color for the cell. This could be a well-known color like "red" or
   * "green" which will be translated into a suitable color. It should also
   * support a #RRGGBB(AA)? color string.
   */
  color: string;

  /**
   * If set, this cell should be shaded as "inactive" -- slightly
   * muted/transparent.
   */
  inactive?: boolean;
  text?: string;
}


export interface ScoreMatrixData {
  rows: ScoreMatrixCell[][];
  columnLabels?: string[];
  rowLabels?: string[];
  title?: string;
}
