import { Component, Input, OnInit } from '@angular/core';

import { BmiColorService } from '../../services/bmi-color.service';
import { ScoreMatrixData, ScoreMatrixCell } from './score-matrix-types';

@Component({
  selector: 'bmi-score-matrix',
  templateUrl: './score-matrix.component.html',
  styleUrls: ['./score-matrix.component.scss']
})
export class ScoreMatrixComponent implements OnInit {
  @Input() data: ScoreMatrixData;
  headerCells: ScoreMatrixCell[] = null;

  constructor(
    private colorService: BmiColorService
  ) { }

  ngOnInit() {
    if (this.data.columnLabels && this.data.columnLabels.length) {
      this.headerCells = this.data.columnLabels.map((label: string) => ({
        text: label,
        color: 'white'
      }));
    }
  }

  getColor(cell: ScoreMatrixCell): string {
    return this.colorService.getCssColor(cell.color, cell.color);
  }
}
