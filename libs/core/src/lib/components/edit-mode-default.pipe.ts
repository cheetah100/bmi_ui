import { Pipe, PipeTransform, Optional } from '@angular/core';
import { WidgetContext } from '../widget-context';


@Pipe({
  name: 'editModeDefault'
})
export class EditModeDefaultPipe implements PipeTransform {
  constructor(
    @Optional() private widgetContext: WidgetContext
  ) {}

  get isInEditMode() {
    return !!this.widgetContext && this.widgetContext.isEditModeNow;
  }

  transform(value: any, editModeValue: any) {
    if ((value === undefined || value === null) && this.isInEditMode) {
      return editModeValue;
    } else {
      return value;
    }
  }
}
