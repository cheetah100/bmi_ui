import { Component, Input } from '@angular/core';
import { FlairIcon } from '../../flair/flair';

@Component({
  selector: 'bmi-flair-icons',
  templateUrl: './flair-icons.component.html',
  styleUrls: ['./flair-icons.component.scss']
})
export class FlairIconsComponent {
  @Input() flairIcons: FlairIcon[];

  invokeFlair(flairIcon: FlairIcon) {
    if (flairIcon && typeof flairIcon.invoke === 'function') {
      flairIcon.invoke();
    }
  }
}
