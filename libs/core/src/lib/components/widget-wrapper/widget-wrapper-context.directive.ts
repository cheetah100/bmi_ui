import { Directive } from '@angular/core';

import { WidgetWrap, WidgetWrapperContext } from './widget-wrap';

@Directive({
  selector: '[bmiWidgetWrapperContext]',
  providers: [
    WidgetWrapperContext,
    {provide: WidgetWrap, useExisting: WidgetWrapperContext}
  ]
})
export class WidgetWrapperContextDirective {}
