import { Injectable, OnDestroy, Provider } from '@angular/core';
import { ObjectMap, StateContainer } from '@bmi/utils';
import { Observable, of, Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { ConfigurableToolbarItem } from '../../components/configurable-action-buttons/config';
import { FlairIcon } from '../../flair/flair';
import { BmiRoute } from '../../navigation/bmi-route';

/**
 * Using this as an interface for widget wrapper
 * This is so that we can use multiple wrappers,
 * depending upon requirements of theme or layout
 * @deprecated use WidgetWrapperContext instead.
 */
export abstract class WidgetWrap {
  abstract setTitle(title: string, options: WidgetTitleOptions): void;
  abstract setRoute(route: BmiRoute): void;
  abstract setIcon(icon: Observable<FlairIcon[]>): void;
  abstract setHidden(hide: boolean): void;
  abstract setHeight(height: ObjectMap<string>): void;
}


export interface WidgetStyleOptions {
  title?: string;
  route?: BmiRoute;
  icons?: Observable<FlairIcon[]>;
  hidden?: boolean;
  height?: ObjectMap<string>;  // huh?
  horizontalAlignment?: 'left' | 'right' | 'center';
  titleOptions?: WidgetTitleOptions;
  actionButtons?: ConfigurableToolbarItem[];
}


export interface WidgetTitleOptions {
  noUnderline?: boolean;
}


@Injectable()
export class WidgetWrapperContext implements WidgetWrap, OnDestroy {
  private styleState = new StateContainer<WidgetStyleOptions>({
    title: undefined,
    route: undefined,
    icons: null,
    hidden: false,
    height: null,
    horizontalAlignment: 'left',
    titleOptions: {
      noUnderline: false,
    },
  });

  readonly state = this.styleState.state;

  get currentState() {
    return this.styleState.currentState;
  }

  public readonly title = this.styleState.select(s => s.title);
  public readonly route = this.styleState.select(s => s.route);
  public readonly icons = this.styleState.select(s => s.icons).pipe(
    switchMap(iconSource => (iconSource ? iconSource : of<FlairIcon[]>(null)))
  );
  public readonly hidden = this.styleState.select(s => s.hidden);
  public readonly height = this.styleState.select(s => s.height);
  public readonly titleOptions = this.styleState.select(s => s.titleOptions);

  ngOnDestroy() {
    this.styleState.unsubscribe();
  }

  /**
   * Adds a modifier to the current widget styling.
   *
   * A modifier is a controlled way of altering the styling, without actually
   * 'mutating' the base state. These modifiers stack up and get merged, but are
   * tied to a subscription: when you unsubscribe, the modifier will be removed
   * along with any effect.
   *
   * To avoid complicating subscription management, a modifier can be an
   * observable stream of updates -- it could be watching a config and updating.
   */
  modify(state: Partial<WidgetStyleOptions> | Observable<Partial<WidgetStyleOptions>>): Subscription {
    return this.styleState.addModifier(state);
  }

  select<R>(fn: (state: WidgetStyleOptions) => R): Observable<R> {
    return this.styleState.select(fn);
  }

  setTitle(title: string, options: Partial<WidgetTitleOptions>) {
    this.updateState({
      title,
      titleOptions: options
    });
  }

  setTitleOptions(options: Partial<WidgetTitleOptions>) {
    this.updateState({titleOptions: options});
  }

  setIcon(icons: Observable<FlairIcon[]>) {
    this.updateState({icons});
  }

  setRoute(route: BmiRoute) {
    this.updateState({route});
  }

  setHidden(hide: boolean) {
    this.updateState({hidden: !!hide});
  }

  setHeight(height: ObjectMap<string>) {
    this.updateState({height});
  }

  private updateState(valuesToUpdate: Partial<WidgetStyleOptions>) {
    this.styleState.set(valuesToUpdate);
  }
}

/**
 * Helper for providing a widget wrapper service
 *
 * This has static deps (i.e. nothing at the moment) for the context class so
 * that it can be used with dynamically created injectors as well as static
 * metadata
 */
export function provideWidgetWrapperContext(): Provider[] {
  return [
    { provide: WidgetWrapperContext, useClass: WidgetWrapperContext, deps: [] },
    { provide: WidgetWrap, useExisting: WidgetWrapperContext }
  ];
}

