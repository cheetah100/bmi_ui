import { Component, Input, Optional } from '@angular/core';
import isNil from 'lodash-es/isNil';
import { combineLatest, Observable, of, Subscription } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { WidgetContext } from '../../widget-context';
import { FlairIcon } from '../../flair/flair';
import { BmiRoute } from '../../navigation/bmi-route';
import { WidgetWrapperContext, WidgetStyleOptions } from './widget-wrap';

@Component({
  template: `
    <ng-container *ngIf="!(hideWidget | async)">
      <ng-container *ngIf="(showHeader | async)">
        <header class="widget-wrapper__header widget-wrapper--{{ size }}">
          <h2 class="widget-wrapper__title" data-cy="widget-wrapper-title">{{ title | async }}</h2>
          <bmi-configurable-action-buttons *ngIf="toolbarButtons | async as buttons" [buttons]="buttons"></bmi-configurable-action-buttons>
          <bmi-flair-icons
            class="widget-wrapper__icons"
            [flairIcons]="icons | async"
          ></bmi-flair-icons>
        </header>
        <hr class="widget-wrapper__hr" *ngIf="(underline | async)" />
      </ng-container>
      <div
        [class.widget-wrapper__scroll-pane]="allowScroll"
        [class.widget-wrapper--align-right]="isRightAligned | async"
        [class.widget-wrapper--align-center]="isCenterAligned | async"
        [style.min-height]="minHeight"
        [style.max-height]="maxHeight"
      >
        <ng-content></ng-content>
      </div>
      <a
        *ngIf="!!(route | async)"
        class="widget-wrapper__link-cover {{
          (isEditMode | async) ? 'widget-wrapper__link-cover--edit-mode' : ''
        }}"
        [bmiRouteLink]="route | async"
      >
      </a>
    </ng-container>
  `,
  selector: 'bmi-core-widget-wrapper',
  styleUrls: ['./widget-wrapper.component.scss']
})
export class WidgetWrapperComponent {
  constructor(
    @Optional() private widgetContext: WidgetContext,
    @Optional() private injectedwrapperContext: WidgetWrapperContext,
  ) {}

  @Input() size: 'normal' | 'huge' = 'normal';

  // It's possible that there is no style context available here (although maybe
  // we should just provide one?). We can create a new one here to ensure that
  // the further observable props in this component can access the style state.
  readonly styleOptions = this.injectedwrapperContext ?? new WidgetWrapperContext();

  title = this.styleOptions.title;
  underline = this.styleOptions.select(s => !s.titleOptions?.noUnderline);
  route = this.styleOptions.route;
  icons = this.styleOptions.icons;
  toolbarButtons = this.styleOptions.select(s => s.actionButtons?.length ? s.actionButtons : null);
  hidden = this.styleOptions.hidden;

  isRightAligned = this.styleOptions.select(s => s.horizontalAlignment === 'right');
  isCenterAligned = this.styleOptions.select(s => s.horizontalAlignment === 'center');

  showHeader = combineLatest([this.title, this.icons, this.toolbarButtons]).pipe(
    map(([title, icons, buttons]) => !isNil(title) || icons?.length > 0 || buttons?.length > 0)
  );

  isEditMode = this.widgetContext ? this.widgetContext.isEditMode : of(false);
  hideWidget = combineLatest([
    this.isEditMode,
    this.hidden
  ]).pipe(
    map(([isEditMode, isHidden]) => !isEditMode && isHidden)
  );


  // TODO: make these declarative observables to remove side-effects
  minHeight: string = 'auto';
  maxHeight: string = 'none';
  allowScroll: boolean = false;

  private subscription = new Subscription();

  ngOnInit() {
    this.subscription.add(
      this.styleOptions.select(s => s.height).subscribe(height => {
        const max = height && height.max ? height.max : 'none';
        this.minHeight = height && height.min ? height.min : 'auto';
        this.maxHeight = max;
        this.allowScroll = max !== 'none';
      })
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
