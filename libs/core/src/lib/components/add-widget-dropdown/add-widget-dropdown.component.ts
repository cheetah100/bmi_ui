import { Component, OnInit, Input, Output, EventEmitter, ViewChild, Optional, Injector } from '@angular/core';
import { Observable, of } from 'rxjs';
import { filter, map, startWith, last, switchMap } from 'rxjs/operators';
import { DropdownMenuComponent } from '@bmi/ui';

import { WidgetConfig, WidgetConfigTemplateDef } from '../../widget-config';
import { WidgetTemplateService } from '../../widget-config-template.service';
import { FormContext } from '../../forms/form-context';
import { makeFormControlWidget } from '../../forms/make-form-control-widget';
import { distinctUntilNotEqual } from '../../util/distinct-until-not-equal';
import { pasteWidgetConfig } from '../../util/copy-paste-widget-config';
import { WidgetEditorModalService } from '../../widget-editor-modal/widget-editor-modal.service';

import flatten from 'lodash-es/flatten';
import sortBy from 'lodash-es/sortBy';
import uniq from 'lodash-es/uniq';


const WIDGET_GROUPS = [
  'Common',
  'Layouts',
  'Graphs',
  'Navigation',
  'Forms',
  'Form Controls',
  'Other',
];


export const SPECIAL_OPTIONS: WidgetTemplateOption[] = [
  {
    id: '__blank-widget',
    name: 'Blank Widget (placeholder)',
    groups: ['Other', 'Layouts'],
    icon: 'fa4-square-o',
    widgetConfig: null,
    sortWeight: Infinity
  },
  {
    id: '__paste-widget',
    name: 'Paste Widget from Clipboard',
    groups: ['Common'],
    widgetConfig: null,
    sortWeight: Infinity,
    icon: 'fa4-paste',
    generateWidgetConfig: () => pasteWidgetConfig()
  }
];


@Component({
  selector: 'bmi-add-widget-dropdown',
  templateUrl: './add-widget-dropdown.component.html',
  styleUrls: ['./add-widget-dropdown.component.scss']
})
export class AddWidgetDropdownComponent implements OnInit {
  @Input() type = 'widget';
  @Input() alwaysShowModal = false;
  @Input() showSpecialOptions = true;
  @Output() widgetCreated = new EventEmitter<WidgetConfig>();
  @ViewChild(DropdownMenuComponent, { static: true }) menu: DropdownMenuComponent;

  get isOpen () {
    return this.menu && this.menu.isOpen;
  }

  get isInsideForm() {
    return !!this.formContext;
  }

  groups: WidgetTemplateGroup[];

  constructor (
    private templateService: WidgetTemplateService,
    private widgetEditorModal: WidgetEditorModalService,
    private injector: Injector,
    @Optional() private formContext: FormContext,
  ) {}


  ngOnInit() {
    this.makeFormFieldOptions().pipe(
      startWith(null),
      distinctUntilNotEqual(),
    ).subscribe(formFieldGroup => {
       const templates = [
         ...this.templateService.getTemplates(this.type),
         ...this.showSpecialOptions ? SPECIAL_OPTIONS : [],
       ];

       const sorted = sortBy(templates, t => [t.sortWeight || 0, t.name]);

       function itemsForGroup(group: string) {
         return sorted.filter(t => t.groups && t.groups.includes(group));
       }

       const groupsFromTemplates = flatten(templates.map(t => t.groups || []));
       const groupsInPreferredOrder = uniq([
         ...WIDGET_GROUPS,
         ...sortBy(groupsFromTemplates),
       ]);

       let groups = groupsInPreferredOrder.map(groupName => {
         return <WidgetTemplateGroup>{
           name: groupName,
           items: itemsForGroup(groupName)
         }
       });

       const itemsInGroups = new Set(flatten(groups.map(g => g.items)));
       const itemsWithoutGroup = sorted.filter(item => !itemsInGroups.has(item));

       const otherGroup = groups.find(g => g.name === 'Other');
       otherGroup.items.push(...itemsWithoutGroup);

       // This is a bit hacky, but if we're inside a form we want to move the
       // "Form Controls" groups to the top of the list.
       if (this.isInsideForm) {
         const formControlsGroup = groups.find(g => g.name === 'Form Controls');
         groups = [
           formControlsGroup,
           ...groups.filter(g => g !== formControlsGroup)
         ];
       }

       if (formFieldGroup) {
         groups.unshift(formFieldGroup);
       }

       this.groups = groups.filter(g => g.items.length > 0);
    });
  }

  itemClicked(item: WidgetTemplateOption) {
    const source = typeof item.generateWidgetConfig === 'function'
        ? item.generateWidgetConfig()
        : of(this.templateService.createTemplate(item));

    source.pipe(
      last(),
      switchMap(config => {
        if (this.alwaysShowModal) {
          return this.widgetEditorModal.create(config, 'Add Widget', this.injector).pipe(
            filter(result => result.action === 'save'),
            map(result => result.data)
          );
        } else {
          return of(config);
        }
      })
    ).subscribe({
      next: config => this.widgetCreated.emit(config),
      error: err => alert('Widget creation error:' + err)
    });
  }

  makeFormFieldOptions(): Observable<WidgetTemplateGroup> {
    if (this.isInsideForm) {
      return this.formContext.fields.pipe(
        map(fields => fields.map(f => (<WidgetTemplateOption>{
          id: `__form-field__${f.id}`,
          name: f.label,
          widgetConfig: makeFormControlWidget(f)
        }))),
        map(widgetTemplates => (<WidgetTemplateGroup>{
          id: '__form-fields',
          name: 'Available Form Fields',
          items: widgetTemplates
        }))
      );
    } else {
      return of(null);
    }
  }
}


interface WidgetTemplateOption extends WidgetConfigTemplateDef {
  generateWidgetConfig?: () => Observable<WidgetConfig>;
}


interface WidgetTemplateGroup {
  id: string;
  name: string;
  items: WidgetTemplateOption[]
}
