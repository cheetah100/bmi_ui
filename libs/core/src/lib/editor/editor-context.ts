import { Injectable } from '@angular/core';

import { BehaviorSubject, Observable } from 'rxjs';
import { distinctUntilChanged, map, throttleTime, publishBehavior, refCount, tap } from 'rxjs/operators';

import { EditableAspect } from './editable-aspect';

/**
 * An injectable service for communicating between widgets and a page editor.
 *
 * This is not intended to be injected globally; rather, it would only be
 * provided during the context of page editing.
 *
 * A widget can optionally inject this service to find out if it's currently
 * being edited and use this to communicate with the editor --
 * for example, to set the selection to an aspect of itself.
 */
@Injectable()
export class EditorContext {
  private _selectedSubject = new BehaviorSubject<EditableAspect>(null);

  private _selected = this._selectedSubject.pipe(
    throttleTime(100, undefined, {leading: true, trailing: false}),
    distinctUntilChanged(),
    publishBehavior(null),
    refCount()
  );

  /**
   * Request to make this editable thing the active one.
   */
  select(aspect: EditableAspect): void {
    this._selectedSubject.next(aspect);
  }

  get selected(): Observable<EditableAspect> {
    return this._selected;
  }

  /**
   * Watches the selected aspect and focuses only on your own selection events.
   *
   * If the selection changes to a different owner, this will emit a `null`
   * value.
   */
  watchForOwnSelection(owner: any): Observable<EditableAspect> {
    return this.selected.pipe(
      map(aspect => (aspect && aspect.owner === owner) ? aspect : null),
      distinctUntilChanged()
    );
  }
}
