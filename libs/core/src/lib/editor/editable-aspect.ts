import { Injector } from '@angular/core';

/**
 * Represents the selection of an editable aspect of the page.
 *
 * Rather than being limited to selecting whole widgets, the page editor can
 * select an 'editable aspect' of a widget; for example, a cell in a grid
 * layout.
 *
 * This abstraction throws all of the responsibility for selection over to the
 * widget implementation.
 */
export abstract class EditableAspect {
  /**
   * @deprecated
   * The 'type ID' of the editor component to load.
   *
   * The initial plan for the editor is to avoid the complexities of dynamic
   * loading and just ng-switch the editable components, so this ID lets us pick
   * which editor to show.
   *
   * This is a temporary measure: it would be MUCH better to use the 'portal'
   * technique from CDK. The naming of the field reflects this.
   */
  tempEditorTypeId: string;

  /**
   * Gets the 'owner' of this editable aspect. This should be the widget
   * that instigated the selection. This value can be used as a key to know if
   * you've lost selection.
   */
  owner: any;

  /**
   * The dependency injector to use for the sidebar.
   *
   * The intended use is to allow a sidebar to DI from the widget's point of
   * view in the page -- e.g. to see which filters are affecting it etc.
   *
   * If not specified, it will use a sensible default -- either the sidebar, or
   * the page's injector node.
   */
  injector?: Injector;
}
