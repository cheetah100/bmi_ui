import { Component, OnInit, OnDestroy, EventEmitter } from '@angular/core';
import { ValidationErrors, AbstractControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter } from 'rxjs/operators';
import { PageEditorContext } from '../page-editor/page-editor-context';
import { PageInfoService } from '../page/page-info.service';
import { PageConfig } from '../page-config';
import { WidgetConfig } from '../widget-config';

import { Permissions } from '@bmi/gravity-services';
import { TypedFormGroup, TypedFormControl, TypedFormArray, IdpValidators } from '@bmi/ui';

import lodashIsEqual from 'lodash-es/isEqual';

const NICE_URL_PATTERN = /^\w(-?\w+)+$/;


function mustBeValidJson(control: AbstractControl): ValidationErrors {
  const value = control.value;
  if (!value) {
    return null;
  }

  try {
      JSON.parse(value);
      return null;
  } catch (e) {
    return {
      'json': 'Must be valid JSON'
    };
  }
}


@Component({
  selector: 'bmi-core-sidebar-page-properties',
  templateUrl: './page-properties-sidebar.component.html',
  styleUrls: ['../editor-sidebar/editor-sidebar.scss'],
  styles: [`
    .suggested-url-link { margin-top: 5px; }
  `
  ]
})
export class PagePropertiesSidebarComponent implements OnInit {
  readonly editorForm = new TypedFormGroup({
    name: new TypedFormControl('', [IdpValidators.required, IdpValidators.mustBeNonBlank()]),
    urlId: new TypedFormControl('', [IdpValidators.pattern(NICE_URL_PATTERN)]),
    permissions: new TypedFormControl<Permissions>({}),
    rootWidget: new TypedFormControl<WidgetConfig>(null),
    dataContextJson: new TypedFormControl<string>('null', {updateOn: 'blur', validators: [mustBeValidJson]})
  });

  suggestedUrlId: string = null;
  private updateUrlSuggestion = new EventEmitter<void>();

  private subscriptions: Subscription[] = [];

  constructor(
    private pageEditorContext: PageEditorContext
  ) {}

  ngOnInit() {
    this.subscriptions.push(this.updateUrlSuggestion.pipe(
      debounceTime(500)
    ).subscribe(() => {
      this.suggestedUrlId = this.makeSuggestedUrlId();
    }));

    this.subscriptions.push(this.pageEditorContext.pageConfig.pipe(
      // The Permissions component is bad and it was causing issues when
      // rebinding to a different form control (Angular 'feature' we discovered
      // during the BMI Charts project). It's got a number of problems that
      // don't cause any apparent issue in simple forms, but break in this world
      // of two-way binding.
      //
      // The problems are n-fold:
      //
      //  1. The Permissions component emits during writeValue
      //
      //  2. The permissions component emits partial, invalid updates as it's
      //     refreshing its internal form. This is due to the way the FormArray
      //     is rebuilt, and we should probably mask those update events where
      //     possible. The right place for that fix might be in @bmi/ui
      //
      //  3. The Permissions component emits regardless of whether the state has
      //     actually changed.
      //
      //  4. The Permissions component update propagates up the page
      //     synchronously, eventually causing the PageConfig to change, then the
      //     form gets updated, before the previous writeValue() has even
      //     finished.
      //
      // The interim solution here is to break the loop. We introduce a deep
      // comparing distinctUntilChanged() op when the page updates, AND we put a
      // debounce on the valueChange propagation. This makes the updates feel a
      // little less immediate, but removes the possibility of an infinite loop.
      distinctUntilChanged((a, b) => lodashIsEqual(a, b))
    ).subscribe((pageConfig: PageConfig) => {
      if (!pageConfig) {
        this.editorForm.reset({emitEvent: false});
      } else {
        this.editorForm.patchValue({
          name: pageConfig.name,
          urlId: pageConfig.urlId || '',
          permissions: pageConfig.permissions || {},
          rootWidget: pageConfig.ui.rootWidget,
          dataContextJson: JSON.stringify(pageConfig.ui.dataContext, null, 2),
        }, {emitEvent: false});

        this.updateUrlSuggestion.emit();
      }
    }));

    this.subscriptions.push(this.editorForm.valueChanges.pipe(
      // Hack part 2: debouncing.
      //
      // This is the second part of the infinite loop fix. The permissions
      // component update is pretty buggy and likes to spam out updates. We
      // should fix that, and make sure some of our utility functions don't emit
      // spurious partial updates (like for clearing a form array).
      //
      // Unfortunately these events are processed synchronously and it can cause
      // infinite recursion if we let these partial events propagate up.
      debounceTime(0),
      filter(() => this.editorForm.valid)
    ).subscribe(formValue => {
      this.pageEditorContext.updatePageConfig(c => {
        c.name = formValue.name;
        c.permissions = formValue.permissions;
        c.urlId = formValue.urlId ? formValue.urlId : null;
        c.ui.rootWidget = formValue.rootWidget;
        c.ui.dataContext = formValue.dataContextJson ? JSON.parse(formValue.dataContextJson) : null;
      });
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  updateRootWidget(config: WidgetConfig) {
    this.editorForm.controls.rootWidget.setValue(config);
  }

  get hasUsefulUrlSuggestion() {
    return this.suggestedUrlId && !this.editorForm.value.urlId;
  }

  useSuggestedUrl() {
    this.editorForm.controls.urlId.setValue(this.suggestedUrlId);
  }

  makeSuggestedUrlId() {
    const suggestedUrl = this.editorForm.value.name
      .toLowerCase()
      .replace(/([^\w]|_)+/g, '-') // Replace - in place of non-word and underscore chars
      .replace(/(^-)|(-$)/g, '');  // Trim leading/trailing -'s

    return suggestedUrl.match(NICE_URL_PATTERN) ? suggestedUrl : null;
  }
}
