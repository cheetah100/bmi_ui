import {Directive, forwardRef, Inject, Optional } from '@angular/core';

import {
  DynamicEditorDefinition,
  DynamicEditorDefinitionService,
  DynamicEditorContext,
  provideAsDynamicEditorService,
} from '@bmi/ui';

import { createMapFrom } from '@bmi/utils';
import sortBy from 'lodash-es/sortBy';

import { FlairConfig } from '../flair/flair-config';
import { FlairEditorDefinition, FLAIR_EDITOR_TOKEN } from '../flair/flair-editor-definition';


/**
 * A directive which provides dynamic editor defs for flairs
 */
@Directive({
  selector: '[bmiFlairsDynamicEditorDefs]',
  providers: [
    provideAsDynamicEditorService(forwardRef(() => FlairsDynamicEditorDirective))
  ]
})
export class FlairsDynamicEditorDirective
  implements DynamicEditorDefinitionService<FlairConfig, FlairEditorDefinition>
{
  constructor(
    @Inject(FLAIR_EDITOR_TOKEN) @Optional() private flairEditorDefs: FlairEditorDefinition[]
  ) {
    this.flairEditorDefs = flairEditorDefs || [];
  }

  private defsByType = createMapFrom(
    this.flairEditorDefs,
    ft => ft.type,
  );

  getDefinition(config: FlairConfig) {
    return config && this.defsByType.get(config.flairType);
  }

  onConfigUpdated(config: FlairConfig, def: FlairEditorDefinition) {
    return {
      ...config,
      flairType: def.type
    };
  }

  getAvailableDefinitions() {
    return sortBy(this.flairEditorDefs, f => f.sortWeight, f => f.name);
  }

  createConfig(def: FlairEditorDefinition) {
    return {
      ...def.defaultConfig || {},
      flairType: def.type,
    };
  }
}
