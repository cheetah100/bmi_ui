import { Component, forwardRef, Inject, Optional} from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  BaseControlValueAccessor,
  provideAsControlValueAccessor,
  TemplatedFormArray,
  TypedFormControl,
  TypedFormGroup,
  Option,
  DropdownMenuItem,
} from '@bmi/ui';
import { createMapFrom } from '@bmi/utils';

import { FlairConfig } from '../flair/flair-config';
import { FlairEditorDefinition, FLAIR_EDITOR_TOKEN } from '../flair/flair-editor-definition';
import { distinctUntilNotEqual } from '../util/distinct-until-not-equal';

import sortBy from 'lodash-es/sortBy';

interface FlairMenuItem extends DropdownMenuItem {
  flairType: string;
}


@Component({
  selector: 'bmi-flair-editor-list',
  templateUrl: './flair-editor-list.component.html',
  styleUrls: ['./flair-editor-list.component.scss'],
  providers: [
    provideAsControlValueAccessor(forwardRef(() => FlairEditorListComponent))
  ]
})
export class FlairEditorListComponent extends BaseControlValueAccessor<FlairConfig[]> {
  flairs = new TypedFormGroup({
    fixedFlairs: new TemplatedFormArray(() => new TypedFormControl<FlairConfig>(null)),
    sortableFlairs: new TypedFormControl<FlairConfig[]>(null)
  });


  flairDefsByType = createMapFrom(this.flairEditorDefinitions, def => def.type);
  allFlairOptions = sortBy(this.flairEditorDefinitions, def => [def.sortWeight, def.name])
    .map(def => (<FlairMenuItem>{
      flairType: def.type,
      text: def.addPhraseText || def.name,
      icon: def.icon || null
    }));

  menuFlairOptions: FlairMenuItem[] = [];
  buttonFlairOptions: FlairMenuItem[] = [];

  // These flair types won't be added to the dropdown because they get buttons.
  SPECIAL_FLAIR_TYPES = ['widget-title', 'widget-route'];

  get currentFlairConfigs() {
    return [
      ...this.flairs.value.fixedFlairs || [],
      ...this.flairs.value.sortableFlairs || [],
    ];
  }

  get hasTitle(): boolean {
    return this.currentFlairConfigs.some(fc => fc.flairType === 'widget-title');
  }

  get hasRoute(): boolean {
    return this.currentFlairConfigs.some(fc => fc.flairType === 'widget-route');
  }

  constructor(
    @Inject(FLAIR_EDITOR_TOKEN) @Optional() private flairEditorDefinitions: FlairEditorDefinition[],
  ) {
    super();
    if (!flairEditorDefinitions) {
      this.flairEditorDefinitions = [];
    }
  }

  ngOnInit() {
    this.flairs.valueChanges.pipe(
      map(() => this.currentFlairConfigs),
      distinctUntilNotEqual(),
    ).subscribe(configs => this.onChanged(configs));

    this.updateAvailableFlairTypes();
  }

  updateComponentValue(value: FlairConfig[]) {
    this.updateFlairs(value || [], {emitEvent: false});
  }

  addFlair(flairType: string) {
    const def = flairType && this.flairDefsByType.get(flairType);
    if (def) {
      const newFlairConfig = {
        ...(def.defaultConfig || {}),
        flairType: def.type
      };

      this.updateFlairs([...this.currentFlairConfigs, newFlairConfig], {emitEvent: true});
    }
  }

  private updateFlairs(flairConfigs: FlairConfig[], options: {emitEvent: boolean}): void {
    const fixedFlairTuples = flairConfigs
      .map(f => ({config: f, def: this.flairDefsByType.get(f.flairType)}))
      .filter(({def}) => def && def.noUserSorting);

    const fixedFlairs = sortBy(fixedFlairTuples, x => (x.def && x.def.sortWeight) || 0)
      .map(x => x.config);

    this.flairs.patchValue({
      fixedFlairs: fixedFlairs,
      sortableFlairs: flairConfigs.filter(f => !fixedFlairs.includes(f))
    }, options);

    this.updateAvailableFlairTypes();
  }

  private updateAvailableFlairTypes(flairConfigs: FlairConfig[] = this.currentFlairConfigs) {
    // Update the available flair options. This will remove any flair
    // suggestions that already exist (unless the config allows duplicates)
    const flairsToHide = flairConfigs
      .map(f => this.flairDefsByType.get(f.flairType))
      .filter(def => def && !def.allowDuplicates)
      .map(def => def.type);

    const allAvailableFlairs = this.allFlairOptions.filter(opt => !flairsToHide.includes(opt.flairType));
    this.menuFlairOptions = allAvailableFlairs.filter(opt => !this.SPECIAL_FLAIR_TYPES.includes(opt.flairType));
    this.buttonFlairOptions = allAvailableFlairs.filter(opt => this.SPECIAL_FLAIR_TYPES.includes(opt.flairType));
  }
}
