import * as marked from 'marked';
import * as Mustache from 'mustache';
import lodashDefaults from 'lodash-es/defaults';

export interface MustacheDownOptions {
  formatMarkdown?: boolean;
  applyTemplate?: boolean;
  context?: { [key: string]: any };
  handleError?: (
    error: any,
    text: string,
    options: MustacheDownOptions
  ) => string;
}

const DEFAULT_OPTIONS: Partial<MustacheDownOptions> = {
  formatMarkdown: true,
  applyTemplate: true,
  context: {},
  handleError: (error, text) => {
    console.warn('Markdown/Mustache renderer: ', error);
    return text;
  },
};

/**
 * A combination Mustache/Mardkown utility
 *
 * This will apply a Mustache template, then format the results as markdown.
 * By default it will throw errors that happen during Mustache or Markdown
 * templating, but this can be overriden in the options.
 *
 * In some cases, you may wish to use this without formatting as Markdown. You
 * can disable markdown formatting by setting `formatMarkdown` to false in the
 * options
 */
export function mustacheDown(
  text: string = '',
  options?: MustacheDownOptions
): string {
  options = lodashDefaults({}, options, DEFAULT_OPTIONS);
  try {
    const templated = options.applyTemplate ? Mustache.render(text, options.context) : text;
    if (options.formatMarkdown) {
      return marked(templated, {
        headerIds: false,
        smartypants: true
      });
    } else {
      return templated;
    }
  } catch (error) {
    if (options.handleError) {
      return options.handleError(error, text, options);
    } else {
      return text;
    }
  }
}
