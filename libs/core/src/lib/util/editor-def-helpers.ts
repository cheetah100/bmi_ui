import { Provider, Type } from '@angular/core';
import { ObjectMap } from '@bmi/utils';
import { WidgetConfig, WidgetConfigTemplateDef, BMI_WIDGET_CONFIG_TEMPLATE } from '../widget-config';
import { BMI_WIDGET_EDITORS, WidgetComponentClass, WidgetDefinition } from '../widget-definition';

import { WIDGET_CONVERTER_TOKEN, WidgetConverter } from '../widget-converter/widget-converter';


import { EditorWidgets, MappedFormSchemaForType } from '../forms/generic-editor-schema';
import { GenericWidgetEditorComponent } from '../widgets/generic-editor/generic-widget-editor.component';


export function defineConfigTemplate(configTemplate: WidgetConfigTemplateDef): Provider {
  return {
    provide: BMI_WIDGET_CONFIG_TEMPLATE,
    multi: true,
    useValue: configTemplate
  }
}

export function defineEditor(widgetType: string, type: WidgetComponentClass, configTemplate?: WidgetConfigTemplateDef, options?: ObjectMap<any>): Provider[] {
  return [
    {
      provide: BMI_WIDGET_EDITORS,
      multi: true,
      useValue: <WidgetDefinition>{
        widgetType: widgetType,
        component: type,
        options: options,
      }
    },

    configTemplate ? defineConfigTemplate(configTemplate) : []
  ];
}


export function defineWidgetConverter(converter: Type<WidgetConverter>): Provider[] {
  return [
    {
      provide: WIDGET_CONVERTER_TOKEN,
      multi: true,
      useClass: converter
    }
  ];
}


interface DefineGenericWidgetEditorParams<T extends WidgetConfig> extends WidgetConfigTemplateDef {
  widgetType: string;
  name: string;
  groups: string[];
  schema: MappedFormSchemaForType<T['fields']>;
  widgetConfig: T;
}


export function defineGenericWidgetEditor<T extends WidgetConfig>(params: DefineGenericWidgetEditorParams<T>): Provider[] {
  return [
    {
      provide: BMI_WIDGET_EDITORS,
      multi: true,
      useValue: <WidgetDefinition>{
        widgetType: params.widgetType,
        component: GenericWidgetEditorComponent,
        options: {
          schema: params.schema,
        },
      }
    },

    defineConfigTemplate({
      name: params.name,
      groups: params.groups,
      widgetConfig: params.widgetConfig
    })
  ];
}
