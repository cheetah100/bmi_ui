import { Injectable, Injector } from '@angular/core';
import { TestBed } from "@angular/core/testing";
import { AbstractConfigFactory, ConfigFactory, ConfigWithType, createDynamicServiceLocator, createServiceLocator, ServiceDef } from './config-factory';


interface TestConfig {
  type: string;
  value?: string;
}


interface TestFactoryParams {
  extra?: string;
}

interface TestService extends ConfigFactory<TestConfig, string, any> {}

@Injectable()
class TestServiceA implements TestService {
  create(config: TestConfig, params: TestFactoryParams) {
    return config.value?.toUpperCase();
  }
}

@Injectable()
class TestServiceB implements TestService {
  create(config: TestConfig, params: TestFactoryParams) {
    return `${config.value} (${params.extra ?? 'n/a'})`
  }
}


// This is for testing the behaviour of the dynamic injector if the serivce was
// never registered as a provider. It should throw an appropriate error message.
// Don't add this one to the testbed.
@Injectable()
class TestServiceWhichIsNotProvided implements TestService {
  create() {
    return 'C'
  }
}


const serviceDefs: ServiceDef<TestService>[] = [
  { type: 'type-A', service: TestServiceA },
  { type: 'type-B', service: TestServiceB },
  { type: 'type-B2', service: TestServiceB },
  { type: 'not-provided-service', service: TestServiceWhichIsNotProvided }
];


describe('Service Locators', () => {
  let serviceInstances: TestService[];
  let injector: Injector;
  let locator: (name: string) => TestService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        TestServiceA,
        TestServiceB
      ]
    });

    serviceInstances = [
      TestBed.inject(TestServiceA),
      TestBed.inject(TestServiceB)
    ];

    injector = TestBed.inject(Injector);
    locator = undefined;
  });

  describe('createServiceLocator', () => {
    beforeEach(() => locator = createServiceLocator('Test Service Locator', serviceDefs, serviceInstances));

    it('should return a function', () =>
      expect(typeof locator).toBe('function'));

    it('should return the correct service instance when given a valid type', () =>
      expect(locator('type-B')).toBe(TestBed.inject(TestServiceB)));

    it('should handle the same service registered under multiple types', () =>
      expect(locator('type-B2')).toBe(TestBed.inject(TestServiceB)));

    it('should throw an exception if the type does not exist', () => {
      expect(() => locator('does-not-exist')).toThrowError(/Test Service Locator/g);
    })
  });

  describe('createDynamicServiceLocator', () => {
    beforeEach(() => locator = createDynamicServiceLocator('Test Dynamic Service Locator', serviceDefs, injector));

    it('should return a function', () =>
      expect(typeof locator).toBe('function'));

    it('should return the correct instance for a valid type', () =>
      expect(locator('type-A')).toBe(TestBed.inject(TestServiceA)));

    it('should support services that are registered multiple times', () =>
      expect(locator('type-B2')).toBe(TestBed.inject(TestServiceB)));

    it('should throw an error if the type is not registered', () => {
      expect(() => locator('does-not-exist')).toThrowError(/Test Dynamic Service Locator.*no definition/g);
    });

    it('should throw an error specifically about injection', () => {
      expect(() => locator('not-provided-service')).toThrowError(/Test Dynamic Service Locator.*could not inject/g);
    })
  });

  describe('Config Factory', () => {
    class TestConfigFactory extends AbstractConfigFactory<TestConfig, string, TestFactoryParams> {
      constructor() {
        super('TestConfigFactory', serviceDefs, serviceInstances)
      }
    }

    let factory: TestConfigFactory;

    beforeEach(() => factory = new TestConfigFactory());

    it('should produce the expected result based on the config type', () => {
      expect(factory.create({type: 'type-A', value: 'test'}, {})).toBe('TEST');
    });

    it('should pass along the params so the factory implementation can use it', () => {
      expect(factory.create({type: 'type-B', value: 'test'}, {extra: 'Foobar'})).toBe('test (Foobar)');
    })

    it('should throw if the type does not exist', () => {
      expect(() => factory.create({type: 'does not exist'}, null)).toThrowError();
    });

    it('should throw if the config is null', () => {
      expect(() => factory.create(null, null)).toThrowError();
    })
  })
})
