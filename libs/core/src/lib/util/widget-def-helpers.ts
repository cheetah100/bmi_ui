import { Type, Provider } from '@angular/core';
import { WidgetDefinition, BMI_WIDGETS, BMI_WIDGET_EDITORS } from '../widget-definition';
import { GeneratorWidgetOptions, GeneratorService } from '../widgets/generator-widget/generator-service';
import { GeneratorWidgetComponent } from '../widgets/generator-widget/generator-widget.component';


export function defineWidget(widgetDef: WidgetDefinition): Provider {
  return {
    provide: BMI_WIDGETS,
    multi: true,
    useValue: widgetDef
  };
}


/**
 * Defines a generator widget
 *
 * A generator widget dynamically generates the widget config to display using a
 * generator service. This allows you to define widgets that are implemented
 * using other existing widgets.
 *
 * The generator service takes the config and produces the widget config to
 * display. All generator widgets use the same Widget component that invokes the
 * generator service and may implement some basic loading and error handling
 * logic.
 */
export function defineGeneratorWidget(widgetType: string, generator: Type<GeneratorService>): Provider[] {
  return [
    generator,
    defineWidget({
      widgetType: widgetType,
      component: GeneratorWidgetComponent,
      options: <GeneratorWidgetOptions>{
        generator: generator
      }
    })
  ];
}


