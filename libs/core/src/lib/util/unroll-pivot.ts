import { PivotData } from '@bmi/gravity-services';

export interface PivotRecord {
  x: string;
  y: string;
  value: any;
}


export function unrollPivot(pivotData: PivotData): PivotRecord[] {
  const {data, xAxis, yAxis } = pivotData;
  const xLength = xAxis.length;
  const yLength = yAxis.length;
  const results: PivotRecord[] = [];
  for (let j = 0; j < yLength; j++) {
    const y = yAxis[j];
    for (let i = 0; i < xLength; i++) {
      results.push({
        x: xAxis[i],
        y: y,
        value: data[j][i]
      })
    }
  }

  return results;
}
