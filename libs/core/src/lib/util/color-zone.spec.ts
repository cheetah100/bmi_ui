import { ColorZone, evaluateColorZone, makeRedYellowGreenColorZones } from './color-zone';

describe('evaluateColorZone', () => {
  const ZONES: ColorZone[] =  makeRedYellowGreenColorZones(25, 75);

  describe('With some simple Red (< 25) yellow (< 75) green levels', () => {
    const DEFAULT_COLOR = 'the default color';
    const TEST_CASES: [string, number, string][] = [
      ['handle values below zero', -4, 'red'],
      ['handle zero', 0, 'red'],
      ['in the red zone', 12, 'red'],
      ['boundary cases should be strictly less-than', 25, 'yellow'],
      ['in the yellow', 66, 'yellow'],
      ['in the final zone', 83, 'green'],
      ['infinity', Infinity, 'green'],
      ['NaN', NaN, DEFAULT_COLOR],
      ['null', null, DEFAULT_COLOR],
      ['undefined', undefined, DEFAULT_COLOR],
      ['string values even though types do not allow it', (<unknown>'55') as number, DEFAULT_COLOR]
    ];

    for (const [spec, value, expected] of TEST_CASES) {
      it(`${spec} (${JSON.stringify(value)} => ${expected})`, () => {
        expect(evaluateColorZone(ZONES, value, DEFAULT_COLOR))
      });
    }
  });

  it('should return the default color if there are no zones', () => {
    expect(evaluateColorZone([], 44, 'default')).toBe('default');
  });

  it('should return the default color if there is no final catch-all zone', () => {
    expect(evaluateColorZone(ZONES.slice(0, -1), 85, 'default')).toBe('default');
  });

  it('should return the default colour if zones array is falsy', () => {
    expect(evaluateColorZone(null, 84, 'default')).toEqual('default');
  });
});


describe('makeRedYellowGreenColorZones', () => {
  it('should create the right zones when given the expected params', () => {
    expect(makeRedYellowGreenColorZones(25, 75)).toEqual([
      {value: 25, color: 'red'},
      {value: 75, color: 'yellow'},
      {color: 'green'}
    ]);
  });

  it('should omit the red zone if the red value is null', () => {
    expect(makeRedYellowGreenColorZones(null, 75)).toEqual([
      {value: 75, color: 'yellow'},
      {color: 'green'}
    ]);
  });

  it('should not omit the red zone if the threshold is 0', () => {
    expect(makeRedYellowGreenColorZones(0, 75)).toEqual([
      {value: 0, color: 'red'},
      {value: 75, color: 'yellow'},
      {color: 'green'}
    ]);
  });

  it('should omit the yellow zone if the threshold is null', () => {
    expect(makeRedYellowGreenColorZones(25, null)).toEqual([
      {value: 25, color: 'red'},
      {color: 'green'}
    ]);
  });

  it('should not omit the yellow zone if the threshold is 0', () => {
    // Note, I've adjusted the red value here so that the value is monotonically
    // increasing. This helper might not enforce that requirement, but it's part
    // of the contract of color zones
    expect(makeRedYellowGreenColorZones(-10, 0)).toEqual([
      {value: -10, color: 'red'},
      {value: 0, color: 'yellow'},
      {color: 'green'}
    ]);
  });
});
