export interface ClipboardApi {
  readText(): Promise<string>;
  writeText(value: string): Promise<void>;
}


export function getClipboard(): Promise<ClipboardApi> {
  const clipboardApi = window.navigator['clipboard'] as ClipboardApi;
  if (clipboardApi && clipboardApi.readText && clipboardApi.writeText) {
    return Promise.resolve(clipboardApi);
  } else {
    return Promise.reject(new Error('Clipboard API not supported by your web browser.'));
  }
}



export function clipboardCopyText(text: string): Promise<void> {
  return getClipboard().then(clipboard => clipboard.writeText(text));
}


export function clipboardPasteText(): Promise<string> {
  return getClipboard().then(clipboard => clipboard.readText());
}
