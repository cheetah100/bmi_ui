import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { ignoreElements, materialize, map } from 'rxjs/operators';


@Injectable()
export class ModalContext<TResult = any> {
  // Using a replay subject here so that all messages emitted by the modal will
  // get repeated on resubscription.
  private _resultSubject = new ReplaySubject<ModalResult<TResult>>();

  /**
   * The result of this modal. This should emit a single value if the modal has
   * a result, and then this observable will complete. A modal instance is
   * single-use.
   */
  public readonly result = this._resultSubject.asObservable();

  /**
   * Emits an event when the dialog result has completed
   */
  public readonly hasCompleted = this.result.pipe(
    ignoreElements(),
    materialize(),
    map(notification => true)
  );

  complete(result: ModalResult<TResult>) {
    if (!this._resultSubject.isStopped) {
      this._resultSubject.next(result);
      this._resultSubject.complete();
    }
  }

  cancel(data?: TResult) {
    this.complete({action: 'cancel', data});
  }

  success(data?: TResult) {
    this.complete({action: 'yes', data});
  }
}


// This should be compatible with the type used by the ConfirmationModal in
// shared-ui, but that class is not exported in the public api
export interface ModalResult<TResult = any> {
  readonly action: string;
  readonly data?: TResult;
}
