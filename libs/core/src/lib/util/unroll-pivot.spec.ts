import { PivotData } from '@bmi/gravity-services';

import { unrollPivot, PivotRecord } from './unroll-pivot';
import flatten from 'lodash-es/flatten';

describe('Pivot Unroller', () => {
  const testData: PivotData = {
    data: [
      [ 1,  2,  3,  4],
      [ 5,  6,  7,  8],
      [ 9, 10, 11, 12],
      [13, 14, 15, 16]
    ],
    xAxis: ['A', 'B', 'C', 'D'],
    yAxis: ['P', 'Q', 'R', 'S'],
    datasources: undefined
  };

  let records: PivotRecord[];

  beforeEach(() => {
    records = unrollPivot(testData);
  });

  it('should produce the same number of records as cells in the pivot', () => {
    expect(records.length).toBe(16);
  });

  it('should emit the values in row-major order (i.e. along X axis, then y)', () => {
    const flattenedPivotValues = flatten(testData.data);
    expect(records.map(v => v.value)).toEqual(flattenedPivotValues);
  });

  it('should emit the right X and Ys to identify a data value', () => {
    const record = records.find(r => r.x === 'C' && r.y === 'Q');
    expect(record.value).toBe(7);
  });

});

