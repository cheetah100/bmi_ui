
/**
 * Object.fromEntries() implementation. The opposite of Object.entries
 *
 * This takes an iterable of [key, value] pairs and turns it back into an
 * object.
 *
 * This allows you to split an object out into an array of entries, process it,
 * then reconstruct.
 *
 * This is available on `Object` in modern browsers (part of ES2018 or
 * something) but we don't have it available/polyfilled so this util will do it
 */
export function objectFromEntries<TVal>(entries: Iterable<[string, TVal]>) {
  const result: {[key: string]: TVal} = {};
  for ( const [key, val] of entries) {
    result[key] = val;
  }
  return result;
}
