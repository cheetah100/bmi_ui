import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { WidgetConfig, validateWidgetSelection } from '../widget-config';

import { clipboardCopyText, clipboardPasteText } from './clipboard';


/**
 * Attempts to paste and validate a widget config from the clipboard.
 *
 * It will emit an error if the value is not a valid widget config, or if
 * clipboard access is unavailable.
 */
export function pasteWidgetConfig(): Observable<WidgetConfig> {
  return from(clipboardPasteText()).pipe(
    map(pastedText => validateWidgetSelection(JSON.parse(pastedText)))
  )
}


export function copyWidgetConfig(widgetConfig: WidgetConfig): Observable<void> {
  return from(clipboardCopyText(JSON.stringify(widgetConfig, undefined, 2)));
}

