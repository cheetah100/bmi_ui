import { pipe } from 'rxjs';
import { createMapFrom } from '@bmi/utils';


export interface FormatStyleDefinition {
  id: string;
  name: string;
  format: (value: number) => string;
}


function makeIntlFormatter(numberFormatOptions: Intl.NumberFormatOptions & {notation?: string}): (value: number) => string {
  const numberFormat = new Intl.NumberFormat('en-US', numberFormatOptions);
  return value => numberFormat.format(value);
}


export const NUMBER_FORMAT_STYLES: FormatStyleDefinition[] = [
  {
    id: 'default',
    name: 'Decimal (2 decimal places)',
    format: makeIntlFormatter({style: 'decimal', maximumFractionDigits: 2}),
  },

  {
    id: 'percentage-0-to-1',
    name: 'Percentage (0 to 1)',
    format: makeIntlFormatter({style: 'percent'})
  },

  {
    id: 'percentage-0-to-100',
    name: 'Percentage (0 to 100)',
    format: pipe(
      value => (value / 100),
      makeIntlFormatter({style: 'percent'})
    )
  },

  {
    id: 'currency',
    name: 'Currency',
    format: makeIntlFormatter({style: 'currency', currency: 'USD'})
  }
];


const formatStyleMap = createMapFrom(NUMBER_FORMAT_STYLES, fs => fs.id);


/**
 * Formats a number using a predefined format style
 *
 * If the value is not a number, it will return the default value instead (this
 * defaults to '-').
 */
export function formatNumber(value: number, styleId = 'default', defaultValue = '-'): string {
  return getNumberFormatFunction(styleId, defaultValue)(value);
}


/**
 * Get a format function for the number format style
 *
 * This does the same thing as formatNumber, but bakes in the style and default
 * value. This could be more efficient as it avoids the map lookup each time.
 */
export function getNumberFormatFunction(styleId: string, defaultValue = '-'): (value: number) => string {
  const formatStyle = formatStyleMap.get(styleId) || formatStyleMap.get('default');
  return value => Number.isFinite(value) ? formatStyle.format(value) : defaultValue;
}
