import { EventEmitter } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';

/**
 * A simple state-based multi-level undo system.
 */
export class UndoController<T> {
  private undoStack: T[] = [];
  private redoStack: T[] = [];

  // We want to batch up closely spaced edits into a single undo entry, and we
  // can do this by replacing the top item on the undo stack.
  private newConfigApplied = new EventEmitter<void>();
  private shouldReplaceTopItem = false;

  private stateChangeSubject: BehaviorSubject<T>;

  constructor(initialState: T, combineCloseTogetherUpdates = true) {
    this.stateChangeSubject = new BehaviorSubject<T>(initialState);
    this.undoStack.push(initialState);

    if (combineCloseTogetherUpdates) {
      this.newConfigApplied.pipe(
        tap(() => this.shouldReplaceTopItem = true),
        debounceTime(500)
      ).subscribe(config => {
        this.shouldReplaceTopItem = false;
      });
    }
  }

  get state(): Observable<T> {
    return this.stateChangeSubject.asObservable();
  }

  get currentState(): T {
    return this.undoStack[this.undoStack.length - 1];
  }

  get canUndo() {
    // There should always be a current state on the undo stack, so we'll only
    // permit an undo operation if
    return this.undoStack.length > 1;
  }

  get canRedo() {
    return this.redoStack.length > 0;
  }

  private emitStateChangeEvent() {
    this.stateChangeSubject.next(this.currentState);
  }

  initialize(initialState: T) {
    this.undoStack.splice(0);
    this.redoStack.splice(0);
    this.shouldReplaceTopItem = false;
    this.undoStack.push(initialState);
    this.emitStateChangeEvent();
  }

  undo(): T {
    this.shouldReplaceTopItem = false;
    if (this.canUndo) {
      this.redoStack.push(this.undoStack.pop());
      this.emitStateChangeEvent();
    }

    return this.currentState;
  }

  redo(): T {
    this.shouldReplaceTopItem = false;
    if (this.canRedo) {
      this.undoStack.push(this.redoStack.pop());
      this.emitStateChangeEvent();
    }

    return this.currentState;
  }

  apply(config: T): void {
    this.redoStack.splice(0);

    if (this.shouldReplaceTopItem) {
      this.undoStack.pop();
    }

    this.undoStack.push(config);
    this.newConfigApplied.emit();
    this.emitStateChangeEvent();
  }
}

