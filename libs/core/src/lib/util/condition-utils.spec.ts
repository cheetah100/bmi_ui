import { GravityApiCondition } from '@bmi/gravity-services';
import { makeEqualToCondition, objectToConditions } from './condition-utils';

describe('makeEqualToCondition', () => {
  it('should return falsy if the value is undefined', () => {
    expect(makeEqualToCondition('test', undefined)).toBeFalsy();
  });

  describe('null values (ISNULL condition ops)', () => {
    let condition: GravityApiCondition;
    beforeEach(() => condition = makeEqualToCondition('test', null));

    it('should set the condition t', () => {
      expect(condition.operation).toBe('ISNULL');
    });

    it('should set the fieldname', () => {
      expect(condition.fieldName).toBe('test');
    })
  });

  describe('String values', () => {
    let condition: GravityApiCondition;
    beforeEach(() => condition = makeEqualToCondition('testField', 'foo'));
    it('should set operation', () => expect(condition.operation).toBe('EQUALTO'));
    it('should set the fieldName', () => expect(condition.fieldName).toBe('testField'));
    it('should set the value', () => expect(condition.value).toBe('foo'));
  });
});


describe('objectToConditions', () => {
  it('should return [] for an empty object', () => {
    expect(objectToConditions({})).toEqual([]);
  });

  it('should return [] for a null object', () => {
    expect(objectToConditions(null)).toEqual([]);
  });

  it('should return [] for an undefined object', () => {
    expect(objectToConditions(undefined)).toEqual([]);
  });

  it('should return an EQUALTO condition for a key with a defined value', () => {
    expect(objectToConditions({'foo': 'bar'})).toEqual([makeEqualToCondition('foo', 'bar')]);
  });

  it('should omit entries with `undefined` values', () => {
    expect(objectToConditions({
      'foo': 'bar',
      'blah': undefined
    })).toEqual([makeEqualToCondition('foo', 'bar')]);
  });
});
