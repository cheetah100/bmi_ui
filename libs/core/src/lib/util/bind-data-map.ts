import { Binder } from '../page-data-context/binder';
import { ParameterBinding } from '../page-data-context/parameter-binding';
import { ObjectMap, combineLatestObject } from '@bmi/utils';
import mapValues from 'lodash-es/mapValues';

export function bindDataMap(binder: Binder, fields: ObjectMap<ParameterBinding>) {
  return combineLatestObject(mapValues(fields, x => binder.bind(x)));
}
