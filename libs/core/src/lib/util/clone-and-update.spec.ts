import { cloneAndUpdate } from './clone-and-update';

import isEqual from 'lodash-es/isEqual';
import cloneDeep from 'lodash-es/cloneDeep';

interface TestNestedObject {
  value: string;
  array: {n: number}[];
  nestedObject: {
    deeper: {
      x: number,
      y: number,
      recursive?: TestNestedObject
    }
  }
};


/**
 *  Are A and B separate object graphs?.
 *
 * This recursively explores arrays and objects in A and B, testing if they
 * share any objects in common. This only supports regular objects, like you'd
 * find in JSON. Weird stuff like maps isn't supported, but the use case for
 * this is configs that need to be JSON serializable.
 */
function isThoroughlyCloned(a: any, b: any): boolean {
  function exploreObject(obj: any, valueSet = new Set<any>()) {
    if (obj && (typeof (obj) === 'object' || Array.isArray(obj)) && !valueSet.has(obj)) {
      valueSet.add(obj);
      Object.values(obj).forEach(x => exploreObject(x, valueSet));
    }
    return valueSet;
  }

  const valuesInA = exploreObject(a);
  const valuesInB = exploreObject(b);

  return Array.from(valuesInA).every(x => !valuesInB.has(x));
}


describe('cloneAndUpdate()', () => {

  describe('On primitive values', () => {
    const coreTypes: [string, any[]][] = [
      ['null', [null]],
      ['undefined', [undefined]],
      ['true', [true]],
      ['false', [false]],
      ['number', [0, -1, 2, 4, Infinity]],
      ['string', ['foo', 'bar', '']]
    ];

    for (const [type, values] of coreTypes) {
      it(`should handle ${type} values`, () => {
        values.forEach(x => expect(cloneAndUpdate(x, () => {})).toBe(x));
      });
    }
  });

  it('should clone objects', () => {
    const input = {test: 'hello'};
    const cloned = cloneAndUpdate(input, () => {});
    expect(input).toEqual(cloned);
    expect(input).not.toBe(cloned);
  });

  describe('Nested objects', () => {
    let testObject: TestNestedObject;

    beforeEach(() => {
      testObject = {
        value: 'test',
        array: [{n: 1}],
        nestedObject: {
          deeper: {
            x: 1,
            y: 2
          }
        }
      };
    });

    // The validity of our tests depends on whether we can detect if two object
    // graphs share any references, hence why we are testing this 'oracle'
    // first.
    describe('testing our test oracle', () => {
      it('should detect identical, uncloned objects', () => {
        expect(isThoroughlyCloned(testObject, testObject)).toBeFalsy();
      });

      it('should detect shallow cloned objects', () => {
        expect(isThoroughlyCloned(testObject, {...testObject})).toBeFalsy();
      });

      it('should detect shallow cloned objects with changed fields', () => {
        expect(isThoroughlyCloned(testObject, {...testObject, value: 'foo'})).toBeFalsy();
      });

      it('should not mind completely different values', () => {
        expect(isThoroughlyCloned(testObject, null)).toBeTruthy();
        expect(isThoroughlyCloned(testObject, {
          value: 'foo',
          array: [],
          nestedObject: {
            deeper: null
          }
        })).toBeTruthy();
      });

      it('should detect shared references in arrays too', () => {
        const cloned = cloneDeep(testObject);
        cloned.array[0] = testObject.array[0];
        expect(isThoroughlyCloned(testObject, cloned)).toBeFalsy();
      });

      it('should not get caught out by circular references', () => {
        testObject.nestedObject.deeper.recursive = testObject;
        const cloned = cloneDeep(testObject);
        expect(cloned.nestedObject.deeper.recursive).toBe(cloned);
        expect(isThoroughlyCloned(testObject, cloned)).toBeTruthy();
      });
    });


    it('should properly clone the object', () => {
      expect(isThoroughlyCloned(cloneAndUpdate(testObject, () => {}), testObject)).toBeTruthy();
    });

    it('should be a different object reference', () => {
      const cloned = cloneAndUpdate(testObject, () => {});
      expect(cloned).not.toBe(testObject);
    });

    it('should retain equality', () => {
      const cloned = cloneAndUpdate(testObject, () => {});
      expect(isEqual(cloned, testObject)).toBeTruthy();
    });

    it('should allow modification of the object', () => {
      const cloned = cloneAndUpdate(testObject, x => x.nestedObject.deeper.x = 7);
      expect(cloned.nestedObject.deeper.x).toBe(7);
      expect(isThoroughlyCloned(testObject, cloned)).toBeTruthy();
    });

    it('should clone away any references introduced during an update', () => {
      const cloned = cloneAndUpdate(testObject, x => x.nestedObject = testObject.nestedObject);
      expect(isThoroughlyCloned(testObject, cloned)).toBeTruthy();
    });
  });

});
