import { PageConfig, PageFilterInfo, PageInfo } from '../page-config';

import { FilterWidgetConfig } from '../widgets/filter-controls/filter-controls-config';
import { FilterDefinition } from '../filters/filter-definition';
import { findDeep } from './find-deep';

import get from 'lodash-es/get';
import matches from 'lodash-es/matches';
import flatten from 'lodash-es/flatten';



/**
 * Extract the pageInfo from a page config
 */
export function makePageInfo(config: PageConfig): PageInfo {
  return {
    id: config.id,
    applicationId: config.applicationId,
    title: config.name,
    urlIds: config.urlId ? [config.urlId] : [],
    filters: grossHackToIntrospectFiltersWhichShouldAbsolutelyBeReplacedWithSomethingBetterIAmSorry(config)
  };
}

function grossHackToIntrospectFiltersWhichShouldAbsolutelyBeReplacedWithSomethingBetterIAmSorry(config: PageConfig): PageFilterInfo[] {
  // This is a total hack. It doesn't belong here, or anywhere really. It exists
  // to search the page config, find any filter widgets and extract that
  // metadata. We can then use this when setting up links. This should be a more
  // formal part of a page configuration!!
  const filterWidgets = findDeep(config, matches({ widgetType: 'filter-controls' })) as FilterWidgetConfig[];
  const filters = flatten(filterWidgets.map(widget => widget.fields.filters || []));
  return filters.map(fd => <PageFilterInfo>{
    id: fd.id,
    optionlist: fd.config && fd.config.optionlist
  });
}
