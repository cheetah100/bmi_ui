import { Observable, combineLatest, OperatorFunction, MonoTypeOperatorFunction, ObservableInput, of, merge } from 'rxjs';
import { filter, map, distinctUntilChanged, share, switchMap } from 'rxjs/operators';

import { DataBindingEvent, makeLoadingEvent, makeValueEvent, DataBindingValueEvent, DataBindingStatusEvent } from '../data-binding-event';


export function combineBindingEvents<T>(sources: Observable<DataBindingEvent<T>>[]): Observable<DataBindingEvent<T[]>> {
  return combineLatest(sources).pipe(
    map(result => allEventsHaveValue(result) ? makeValueEvent(result.map(r => r.value)) : makeLoadingEvent()),

    // combineLatest can be spammy, so we'll use a distinct operator here to
    // avoid redundant loading events.
    distinctStatusEvents()
  );
}


export function bindingStatusPassthrough<T, R>(
  valuePipeline: (obs: Observable<DataBindingValueEvent<T>>) => ObservableInput<DataBindingEvent<R>>
): OperatorFunction<DataBindingEvent<T>, DataBindingEvent<R>> {
  return source => {
    const sharedUpstream = source.pipe(share());
    return merge(
      sharedUpstream.pipe(filterOnlyStatusEvents()),
      valuePipeline(sharedUpstream.pipe(filterOnlyValueEvents()))
    );
  };
}


export function distinctStatusEvents<T>(): MonoTypeOperatorFunction<DataBindingEvent<T>> {
  return source => source.pipe(
    distinctUntilChanged((a, b) => a.type === 'loading' && b.type === 'loading')
  );
}


export function extractValuesOnly<T>(): OperatorFunction<DataBindingEvent<T>, T> {
  return source => source.pipe(
    filterOnlyValueEvents(),
    map(event => event.value),
  );
}

export function filterOnlyStatusEvents<T>(): OperatorFunction<DataBindingEvent<T>, DataBindingStatusEvent> {
  return source => source.pipe(
    filter(event => event.type !== 'value'),
    map(event => event as DataBindingStatusEvent)
  );
}

export function filterOnlyValueEvents<T>(): OperatorFunction<DataBindingEvent<T>, DataBindingValueEvent<T>> {
  return source => source.pipe(
    filter(event => event.type === 'value'),
  ) as Observable<DataBindingValueEvent<T>>;
}


function allEventsHaveValue<T>(events: DataBindingEvent<T>[]): events is DataBindingValueEvent<T>[] {
  return events && events.every(e => e.type === 'value');
}
