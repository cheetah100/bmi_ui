import { deepFreezeObject } from './deep-freeze-object';

interface TestObject {
  value: string;
  nestedObject: {
    nestedArray: string[];
  },
  plainArray: string[],
  objectArray: {x: number}[];
}


describe('deepFreezeObject', () => {
  let value: TestObject, proxy: TestObject;

  beforeEach(() => {
    value = {
      value: 'foo',
      nestedObject: {
        nestedArray: ['foo', 'bar']
      },
      plainArray: ['a', 'b', 'c'],
      objectArray: [
        {x: 3}, {x: 4}
      ]
    };

    proxy = deepFreezeObject(value, 'MODIFIED', 'value');
  })

  it('should act as a clone', () => {
    expect(value).toEqual(proxy);
  })

  it('should catch modifications to fields', () => {
    expect(() => {
      proxy.value = 'bar';
    }).toThrowError()
  });

  it('should catch attempts to add new fields', () => {
    expect(() => {
      proxy['blah'] = 5;
    }).toThrowError();
  });

  it('should catch modifications to nested objects', () => {
    expect(() => {
      proxy.nestedObject.nestedArray = null;
    }).toThrowError();
  });

  it('should catch modifications to nested arrays', () => {
    expect(() => {
      proxy.plainArray.push('z');
    }).toThrowError();
  });

  it('should catch nested objects inside arrays', () => {
    expect(() => {
      proxy.objectArray[1].x = 8;
    }).toThrowError();
  });

  it('should preserve object identity across getter calls', () => {
    expect(proxy.nestedObject).toBe(proxy.nestedObject);
  });
});
