import { ObjectMap } from '@bmi/utils';


export function mapObject<TEntry, TResult>(
  object: ObjectMap<TEntry>,
  valueFunction: (value: TEntry) => TResult,
  keyFunction: (key: string) => string = x => x
): ObjectMap<TResult> {
  const result: ObjectMap<TResult> = {};
  for (const [key, value] of Object.entries(object)) {
    result[keyFunction(key)] = valueFunction(value);
  }
  return result;
}
