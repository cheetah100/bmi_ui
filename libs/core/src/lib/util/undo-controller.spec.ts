import { UndoController } from './undo-controller';

describe('Page Designer Undo Controller: ',  () => {

  describe('Synchronous test cases', () => {
    let undoController: UndoController<string>;
    let stateChanges: string[];

    beforeEach(() => {
      // To simplify these tests, we turn off the feature to combine close
      // together updates into the same undo state.
      undoController = new UndoController<string>('', false);
      stateChanges = [];
      undoController.state.subscribe(newState => {
        stateChanges.push(newState);
      });
    });

    describe('With an empty undo/redo stack:', () => {
      it('should not be able to undo', () => {
        expect(undoController.canUndo).toBeFalsy();
      });

      it('should not be able to redo', () => {
        expect(undoController.canRedo).toBeFalsy();
      });

      it('should return the initial state when undoing', () => {
        expect(undoController.undo()).toEqual('');
      });

      it('should return the initial state when redoing', () => {
        expect(undoController.redo()).toEqual('');
      });

      it('should emit the initial state immediately (i.e. BehaviorSubject semantics)', () => {
        expect(stateChanges).toEqual(['']);
      });

      it('should not emit an event when undoing with nothing to do', () => {
        undoController.undo();
        expect(stateChanges.length).toBe(1);
      });

      it('should not emit an event when redoing with nothing to do', () => {
        undoController.redo();
        expect(stateChanges.length).toBe(1);
      })
    });

    describe('With an state on the undo stack', () => {
      beforeEach(() => {
        undoController.apply('Test');
      });
      it('should show this value as the current state', () => expect(undoController.currentState).toEqual('Test'));

      it('should be able to undo', () => expect(undoController.canUndo).toBeTruthy());
      it('should not be able to redo', () => expect(undoController.canRedo).toBeFalsy());
      it('should return to the initial state, after undoing', () => expect(undoController.undo()).toEqual(''));

      describe('After undoing that state', () => {
        beforeEach(() => undoController.undo());
        it('should not be able to undo further', () => expect(undoController.canUndo).toBeFalsy());
        it('should be able to redo', () => expect(undoController.canRedo).toBeTruthy());
        it('should return the state when redoing', () => expect(undoController.redo()).toEqual('Test'));
      });

      describe('undo() then redo()',() => {
        beforeEach(() => { undoController.undo(); undoController.redo(); });
        it('should not be able to redo again', () => expect(undoController.canRedo).toBeFalsy());
        it('should be able to undo', () => expect(undoController.canUndo).toBeTruthy());
        it('should be in the applied state', () => expect(undoController.currentState).toEqual('Test'));
        it('should return the initial state when undoing', () => expect(undoController.undo()).toEqual(''));
      });
    });

    describe('With multiple items on the undo stack,', () => {
      beforeEach(() => {
        undoController.apply('Hello');
        undoController.apply('Hello World');
        undoController.apply('Hello World!');
      });

      it('should undo in reverse order', () => {
        expect(undoController.undo()).toEqual('Hello World');
        expect(undoController.undo()).toEqual('Hello');
        expect(undoController.undo()).toEqual('');
      });

      it('should redo in the correct order too', () => {
        for (let i = 0; i < 3; i++) {
          undoController.undo();
        }

        expect(undoController.redo()).toBe('Hello');
        expect(undoController.redo()).toBe('Hello World');
        expect(undoController.redo()).toBe('Hello World!');
      });

      describe('if undo() then apply() is called, ', () => {
        beforeEach(() => {
          undoController.undo();
          undoController.apply('Hello, World!');
        });

        it('should clear the redo stack', () => {
          expect(undoController.canRedo).toBeFalsy();
        });

        it('should undo to the correct state', () => {
          expect(undoController.undo()).toEqual('Hello World');
        });
      });

      it('should emit state change events when the changes were applied', () => {
        expect(stateChanges).toEqual(['', 'Hello', 'Hello World', 'Hello World!']);
      });

      it('should emit a state change after undoing', () => {
        undoController.undo();
        expect(stateChanges).toEqual(['', 'Hello', 'Hello World', 'Hello World!', 'Hello World']);
      });

      it('should emit a state change after redoing', () => {
        undoController.undo();
        undoController.redo();
        expect(stateChanges).toEqual(['', 'Hello', 'Hello World', 'Hello World!', 'Hello World', 'Hello World!']);
      });

      describe('After re-initializing the UndoController', () => {
        beforeEach(() => {
          undoController.initialize('Test');
        });

        it('should not be able to undo', () => expect(undoController.canUndo).toBeFalsy());
        it('should not be able to redo', () => expect(undoController.canRedo).toBeFalsy());
        it('should return the current state', () => expect(undoController.currentState).toBe('Test'));

        it('should emit a change event', () => {
          expect(stateChanges).toEqual(['', 'Hello', 'Hello World', 'Hello World!', 'Test']);
        })
      });
    });
  });
});
