/**
 * Recursively deep freezes an object, attempting to prevent any modification.
 *
 * This relies on Proxy to trap setter operations on an object. It works
 * recursively, by also overriding the getter and returning a proxied version
 * of any values.
 *
 * This recursive behaviour is lazy, and memoized. Only properties that are
 * accessed get proxied, and the proxy object is cached to preserve object
 * identity semantics.
 *
 * If the Proxy object is unsupported, this will just return the input without
 * freezing. This is intended to just be used in devMode on evergreen browsers
 * to catch bad behaviour that might cause bugs.
 *
 * @param object the object to proxy
 * @param errorMessage base error message to show. This will be suffixed with :
 *     and a more specific error
 * @param basePath the first path segment to use when generating paths for error
 *     messages. This can be anything.
 * @returns a proxied version of the input object, if supported
 */
export function deepFreezeObject<T extends object>(object: T, errorMessage: string, basePath: string = ''): T {
  if (shouldProxyValue(object)) {
    // When getting a value from this object, we keep track of the proxy
    // instances in order to preserve object identity across calls, and for
    // performance.
    const proxiedValues = new Map();
    return new Proxy(object, {
      get: (target, prop) => {
        const value = target[prop];
        if (shouldProxyValue(value)) {
          let proxy = proxiedValues.get(value);
          if (!proxy) {
            proxy = deepFreezeObject(value, errorMessage, `${basePath}.${String(prop)}`);
            proxiedValues.set(value, proxy);
          }
          return proxy;
        } else {
          return value;
        }
      },

      set: (target, prop) => {
        throw new Error(`${errorMessage}: Attempted to set ${basePath}.${String(prop)}`);
      }
    });
  } else {
    return object;
  }
}


function shouldProxyValue(value) {
  return Proxy && value && typeof (value) === 'object';
}
