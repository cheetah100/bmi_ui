export function findDeep(
  input: unknown,
  predicate: (x: unknown) => boolean,
  results: unknown[] = []
): unknown[] {
  if (predicate(input)) {
    results.push(input);
  }
  if (input && (Array.isArray(input) || typeof input === 'object')) {
    Object.values(input).forEach(x => findDeep(x as unknown, predicate, results));
  }
  return results;
}
