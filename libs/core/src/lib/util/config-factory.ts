import { Injector, Type } from '@angular/core';
import { createMapFrom } from '@bmi/utils';


export interface ConfigWithType {
  type: string;
}


export interface ServiceDef<TService> {
  type: string;
  service: Type<TService>;
}



export interface ConfigFactory<TConfig, TProduct, TParams> {
  create(config: TConfig, params: TParams): TProduct;
}


/**
 * Creates a function for looking up service instances based on some metadata.
 * This is useful for dynamically locating factories/services based on some
 * dynamic configs.
 *
 * It was refactored into a separate function to make it reusable for more than
 * just factories.
 */
export function createServiceLocator<TService>(
  helpfulLocatorName: string,
  serviceDefs: ServiceDef<TService>[],
  serviceInstances: TService[]
): (type: string) => TService {
  const defs = serviceDefs ?? [];
  const instances = serviceInstances ?? [];
  const instancesByType = createMapFrom(
    defs,
    fd => fd.type,
    fd => instances.find(f => f instanceof fd.service)
  );

  return type => {
    const service = instancesByType.get(type);
    if (!service) {
      throw new Error(`${helpfulLocatorName}: instance not found for type: ${type}`);
    } else {
      return service;
    }
  };
}


/**
 * Creates a function for dynamically injecting a service based on some type
 * mapping metadata.
 */
export function createDynamicServiceLocator<TService>(
  locatorName: string,
  defs: ServiceDef<TService>[],
  injector: Injector
): (type: string) => TService {
  const serviceClassesByType = createMapFrom(defs ?? [], d => d.type, d => d.service);
  return type => {
    const serviceToken = serviceClassesByType.get(type);
    if (!serviceToken) {
      throw new Error(`${locatorName}: no definition for '${type}'`);
    }

    try {
      return injector.get(serviceToken);
    } catch (error) {
      throw new Error(`${locatorName}: could not inject the service for '${type}\n\n${error}'`)
    }
  }
}

/**
 * Base class for multi-factories which produce a different type of product
 * based on the type property of the config.
 *
 * This is a common pattern needed for "plugin-y" things (data sources, actions,
 * flairs, etc.) where each type can have a dedicated factory service  (the
 * dependency injected thing), but produces something based on configs (e.g. the
 * bit that does the work.)
 *
 * This base class doesn't care what's being produced, it's job is to outsource
 * the production to the relevant factory based on the type.
 *
 * There's also a generic "params" that can be passed through to the factory.
 * This is an arbitrary type, so it could be something simple like a data
 * context to use, but you could use an object to pass through multiple values.
 */
export abstract class AbstractConfigFactory<
    TConfig extends ConfigWithType,
    TProduct = any,
    TParams = any>
  implements ConfigFactory<TConfig, TProduct, TParams>
{
  constructor(
    private factoryName: string,
    private factoryDefs: ServiceDef<ConfigFactory<TConfig, TProduct, TParams>>[],
    private factoryInstances: ConfigFactory<TConfig, TProduct, TParams>[]
  ) {}

  private locator = createServiceLocator(this.factoryName, this.factoryDefs, this.factoryInstances);

  create(config: TConfig, params: TParams): TProduct {
    const factory = this.locator(config?.type);
    return factory.create(config, params);
  }
}
