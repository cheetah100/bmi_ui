import { DataBindingEvent, makeValueEvent, makeLoadingEvent, DataBindingValueEvent } from '../data-binding-event';
import { filterOnlyStatusEvents, filterOnlyValueEvents, bindingStatusPassthrough } from './data-binding-operators';

import { Observable, of, Subject } from 'rxjs';
import { tap } from 'rxjs/operators';

function setupPipeline<T, R>(pipelineFn: (source: Observable<T>) => Observable<R>): [Subject<T>, R[]] {
  const subject = new Subject<T>();
  const results: R[] = [];
  pipelineFn(subject).subscribe(value => results.push(value));
  return [subject, results];
}

function onSubscribe<T>(fn: () => void) {
  return source => {
    fn();
    return source;
  }
}


describe('Data Binding Event RxJS Operators', () => {

  describe('filterOnlyStatusEvents()', () => {
    let input: Subject<DataBindingEvent>;
    let output: DataBindingEvent[];

    beforeEach(() => {
      [input, output] = setupPipeline(source => source.pipe(filterOnlyStatusEvents()));
    });

    it('should pass through loading events', () => {
      input.next(makeLoadingEvent());
      expect(output).toEqual([makeLoadingEvent()]);
    });

    it('should not pass through value events', () => {
      input.next(makeValueEvent('test'));
      expect(output).toEqual([]);
    });
  });


  describe('bindingStatusPassthrough()', () => {
    let input: Subject<DataBindingEvent>;
    let output: DataBindingEvent[];
    let valuePipelineEvents: DataBindingValueEvent[];
    let inputSubscriptionCount: number;

    beforeEach(() => {
      inputSubscriptionCount = 0;
      valuePipelineEvents = [];
      [input, output] = setupPipeline(source => source.pipe(
        onSubscribe(() => inputSubscriptionCount += 1),
        bindingStatusPassthrough(valueSource => valueSource.pipe(
          tap<DataBindingValueEvent>(event => valuePipelineEvents.push(event))
        ))
      ));
    });


    describe('loading status events', () => {
      beforeEach(() => {
        input.next(makeLoadingEvent());
      });

      it('should output the event verbatim', () => expect(output).toEqual([makeLoadingEvent()]));
      it('should not pass the event to the value pipeline', () => expect(valuePipelineEvents).toEqual([]));
    });

    describe('value events', () => {
      let event: DataBindingValueEvent;
      beforeEach(() => {
        event = makeValueEvent('test event');
        input.next(event);
      });

      it('should output the event once', () =>  expect(output).toEqual([event]));
      it('should pass the event to the value pipeline', () => expect(valuePipelineEvents).toEqual([event]));
    });
  });
});
