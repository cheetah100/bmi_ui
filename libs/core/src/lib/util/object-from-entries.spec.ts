import { objectFromEntries } from './object-from-entries';

describe('objectFromEntries', () => {
  it('should return an empty object if passed an empty array', () => {
    expect(objectFromEntries([])).toEqual({});
  });

  it('should be the dual of Object.entries', () => {
    const original = {a: 4, b: 11, c: 36};
    expect(objectFromEntries(Object.entries(original))).toEqual(original);
  });

  it('should support iterable of pairs (e.g. map)', () => {
    const map = new Map<string, number>([
        ['a', 4],
        ['b', 33],
        ['c', 78]
      ]);

    expect(objectFromEntries(map)).toEqual({
      a: 4,
      b: 33,
      c: 78
    });

  });
});
