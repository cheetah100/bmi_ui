import { COLORS } from '@bmi/ui';


/**
 * Color zones let you define a range of values which should be shown a
 * particular color, such as Red - Yellow - Green based on a metric score.
 *
 * This structure is derived from the Highcharts feature, but it's a common
 * enough pattern for this kind of thing. To define a range of colour zones, you
 * would use an array of these objects:
 *
 *     [
 *       {value: 25, color: 'red'},
 *       {value: 75, color: 'yellow'},
 *       {color: 'green'}
 *     ]
 *
 * The zones must be in ascending order of values
 */
export interface ColorZone {
  value?: number;
  color: string;
}


export function evaluateColorZone(zones: ColorZone[], value: number, defaultValue = '#000000'): string {
  if (!zones || !isFinite(value)) {
    return defaultValue;
  }

  for (let i = 0; i < zones.length; i++) {
    const zoneValue = zones[i].value;
    const hasNoValue = zoneValue === null || zoneValue === undefined;
    if (hasNoValue || value < zones[i].value) {
      return zones[i].color;
    }
  }

  return defaultValue;
}


/**
 * Construct some simple Red - Yellow - Green color zones.
 *
 * This takes the red and yellow thresholds (green is implicit), and an optional
 * map of color strings to use (by default these are 'red', 'yellow' 'green')
 * and returns an array of zone definitions.
 *
 * If either threshold is omitted, the zone will not be included -- e.g. if the
 * yellowThreshold is not a valid number, then only a red zone and green zone
 * will be defined. (If neither are provided, everything will be green, which
 * might not be desirable)
 */
export function makeRedYellowGreenColorZones(
  redThreshold: number | null | undefined,
  yellowThreshold: number | null | undefined,
  colors = { red: 'red', yellow: 'yellow', green: 'green'}
): ColorZone[] {
  return [
    Number.isFinite(redThreshold) ? {value: redThreshold, color: colors.red} : null,
    Number.isFinite(yellowThreshold) ? {value: yellowThreshold, color: colors.yellow} : null,
    {color: colors.green}
  ].filter(zone => !!zone);
}
