import { formatNumber } from './format-number';

function parametricTests<T, U>(f: (input: T) => U, cases: [string, T, U][]) {
  for (const [spec, input, expected] of cases) {
    it(spec, () => expect(f(input)).toEqual(expected));
  }
}


describe('formatNumber', () => {

  const DEFAULT_VALUE = '-';
  describe('default (2dp rounding)', () => {
    parametricTests(value => formatNumber(value, 'default', DEFAULT_VALUE), [
      ['whole numbers should have no fractional', 37, '37'],
      ['trailing 0s are not included', 37.5, '37.5'],
      ['numbers should round down if below .x5', 37.773, '37.77'],
      ['big numbers get grouped with commas in the US style', 1337131, '1,337,131'],
      ['NaN gets formatted as default value', NaN, DEFAULT_VALUE],
      ['null gets formatted as default value', null, DEFAULT_VALUE],
      ['undefined gets formatted as default value', undefined, DEFAULT_VALUE]
    ]);
  });

  describe('percentage-0-to-1', () => {
    parametricTests(x => formatNumber(x, 'percentage-0-to-1', DEFAULT_VALUE), [
      ['it gets a percentage sign', 0.45, '45%'],
      ['it gets rounded to 0dp', 0.145, '15%'],
    ]);
  })
});
