import cloneDeep from 'lodash-es/cloneDeep';

/**
 * A paranoid immutable update function. Clones, then updates, then clones.
 *
 * This is designed for updating an existing data object where you want to make
 * sure that you're not introducing mutations into the original structure.
 *
 * The way this works:
 *
 *  1. Deep-clone the original
 *  2. Run the callback, passing the cloned object for mutation.
 *  3. Deep clone the result and return.
 *
 * Yes, that's a lot of cloning, but computers are fast, memory is cheap and
 * the input configs are probably small.
 *
 * It's a small price to pay for peace of mind.
 *
 * @param original the original object
 * @param callback a function to call which will be passed a cloned copy that is safe to mutate
 * @returns returns the updated object, cloned once more for safety.
 */
export function cloneAndUpdate<T>(original: T, callback: (config: T) => void): T {
  const clonedConfig = cloneDeep(original);
  callback(clonedConfig);
  return cloneDeep(clonedConfig);
}
