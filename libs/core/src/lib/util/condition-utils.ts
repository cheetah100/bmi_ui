import { GravityApiCondition } from '@bmi/gravity-services';
import { ObjectMap } from '@bmi/utils';


export function makeEqualToCondition(fieldName: string, value: any): GravityApiCondition {
  if (value === undefined) {
    return null
  } else if (value === null) {
    // Value must be set or Gravity will give a 500 error. The actual value
    // seems to be ignored when doing the filtering though?
    return { fieldName, operation: 'ISNULL', value: 'true' }
  } else {
    return { fieldName, operation: 'EQUALTO', value: String(value) };
  }
}


export function objectToConditions(values: ObjectMap<any>): GravityApiCondition[] {
  return Object.entries(values || {})
    .map(([key, value]) => makeEqualToCondition(key, value))
    .filter(condition => !!condition);
}
