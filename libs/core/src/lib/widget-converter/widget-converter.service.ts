import { Injectable, Optional, Inject } from '@angular/core';

import { WidgetConfig } from '../widget-config';
import { WIDGET_CONVERTER_TOKEN, WidgetConverter } from './widget-converter';

import sortBy from 'lodash-es/sortBy';


@Injectable({providedIn: 'root'})
export class WidgetConverterService {
  private converters: WidgetConverter[];

  constructor(
    @Inject(WIDGET_CONVERTER_TOKEN) @Optional() widgetConverters: WidgetConverter[]
  ) {
    this.converters = sortBy(widgetConverters || [], c => c.name);
  }


  doesConverterSupport(converter: WidgetConverter, config: WidgetConfig): boolean {
    return !converter.shouldShowFor || converter.shouldShowFor(config)
  }

  getConverters(widgetConfig: WidgetConfig): WidgetConverter[] {
    return this.converters.filter(c => this.doesConverterSupport(c, widgetConfig));
  }
}
