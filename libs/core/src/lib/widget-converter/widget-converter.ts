import { InjectionToken } from '@angular/core';
import { WidgetConfig } from '../widget-config';


/**
 * An interface for widget converter objects. The type parameters here are for
 * convenience when developing, and of course have no influence at runtime!
 */
export interface WidgetConverter<T extends WidgetConfig = WidgetConfig, U extends WidgetConfig = WidgetConfig> {
  /** The name of this widget converter */
  readonly name: string;

  /** Optional: should the converter show for this widget. Default is true */
  shouldShowFor?(widgetConfig: WidgetConfig): boolean;

  /**
   * Converts a widget config into a new config.
   */
  convert(widgetConfig: T): U;
}


export const WIDGET_CONVERTER_TOKEN = new InjectionToken<WidgetConverter>('@bmi/core Widget Converter');
