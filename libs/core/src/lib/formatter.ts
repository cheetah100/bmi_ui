import { Injectable, InjectionToken, Inject, Optional, Type } from '@angular/core';
import { Observable } from 'rxjs';
import { createMapFrom, ObjectMap } from '@bmi/utils';

import { Binder } from './page-data-context/binder';

import { ParameterBinding, FormatterConfig } from './page-data-context/parameter-binding';


// Re-export these things for compatibility
export { ParameterBinding, FormatterConfig } from './page-data-context/parameter-binding';



export interface FormatterBinder {
  bind(binding: ParameterBinding): Observable<any>;
}



export interface FormatterMetadata {
  formatterType: string;
  name: string;
  groups?: string[];
  sortWeight?: number;
  defaultConfig?: ParameterBinding | Partial<FormatterConfig>;
  component?: Type<{}>;
  shouldShowFormatter?: (x: ObjectMap<any>) => boolean;

  supportsText?: boolean;
  supportsNull?: boolean;
}


export interface FormatterTypeMapping {
  formatterType: string;
  formatterClass: Type<Formatter>;
}


export interface Formatter<TConfig extends FormatterConfig = FormatterConfig> {
  format(config: TConfig, binder: Binder): Observable<any>;
}


export abstract class FormatterEditorContext<
  T = ParameterBinding,
  THints extends ObjectMap<any> = ObjectMap<any>
  > {
  abstract hints: Observable<THints>;
  abstract formatterConfig: Observable<T>;
  abstract setFormatterConfig(config: T): void;
}


export function isFormatterConfig(binding: unknown): binding is FormatterConfig {
  return binding && typeof binding === 'object' && binding.hasOwnProperty('formatterType') && !!binding['formatterType'];
}


export const FORMATTER_SERVICES = new InjectionToken<Formatter>('@bmi/core Formatter services');
export const FORMATTER_TYPE_MAPPING = new InjectionToken<FormatterTypeMapping>('@bmi/core Formatter type mapping');
export const FORMATTER_METADATA = new InjectionToken<FormatterMetadata>('@bmi/core Formatter metadata');


@Injectable({providedIn: 'root'})
export class FormatterService {
  private formatters: Map<string, Formatter>;

  constructor(
    @Optional() @Inject(FORMATTER_TYPE_MAPPING) formatterTypeMapping: FormatterTypeMapping[],
    @Optional() @Inject(FORMATTER_SERVICES) formatterInstances: Formatter[]
  ) {
    const formatters = formatterInstances || [];
    this.formatters = createMapFrom(
      formatterTypeMapping || [],
      ft => ft.formatterType,
      ft => formatters.find(f => f instanceof ft.formatterClass)
    );
  }

  private getFormatter(config: FormatterConfig) {
    const formatter = this.formatters.get(config.formatterType);
    if (!formatter) {
      throw new Error(`Unknown formatter type: ${config.formatterType}`);
    } else {
      return formatter;
    }
  }

  bindFormatter(config: FormatterConfig, binder: Binder): Observable<any> {
    return this.getFormatter(config).format(config, binder);
  }

  /** @deprecated Just use the PageDataContext directly */
  makeFormatterBinder(binder: Binder): FormatterBinder {
    return binder;
  }
}
