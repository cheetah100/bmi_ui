  import { Injectable, Inject, Optional } from '@angular/core';
import { WidgetConfig, WidgetConfigTemplateDef, BMI_WIDGET_CONFIG_TEMPLATE } from './widget-config';

import cloneDeep from 'lodash-es/cloneDeep';

import { uniqueId } from '@bmi/utils';



@Injectable({providedIn: 'root'})
export class WidgetTemplateService {
  private templates: WidgetConfigTemplateDef[];

  constructor(@Optional() @Inject(BMI_WIDGET_CONFIG_TEMPLATE) widgetTemplateDefs: WidgetConfigTemplateDef[]) {
    this.templates = (widgetTemplateDefs || []).map(t => ({
      ...t,
      id: t.id || uniqueId('wt', 8),
      type: t.type || 'widget',
      description: t.description || ''
    }));
  }

  getTemplates(type = 'widget'): WidgetConfigTemplateDef[] {
    return this.templates.filter(t => t.type === type);
  }

  createTemplate(id_or_template: string | WidgetConfigTemplateDef): WidgetConfig {
    const template = typeof id_or_template === 'string' ?
        this.templates.find(x => x.id === id_or_template) :
        id_or_template;

    if (!template) {
      throw new Error(`Widget Config Template not valid: ${JSON.stringify(id_or_template, null, 2)}`);
    }

    return cloneDeep(template.widgetConfig);
  }

  createTemplateByName(name:string):WidgetConfig {
    const template = this.templates.find(x => x.name === name);

    if (!template) {
      throw new Error(`Widget Config Template name not valid: ${name}`);
    }

    return cloneDeep(template.widgetConfig);
  }
}
