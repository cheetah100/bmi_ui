import { Observable } from 'rxjs';

export interface Modal {
  modalClosed?: Observable<void>;
  show(): Observable<any>;
}
