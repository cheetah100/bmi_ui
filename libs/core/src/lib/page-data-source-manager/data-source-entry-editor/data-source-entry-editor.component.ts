import { Component, OnInit, forwardRef, Output, EventEmitter } from '@angular/core';

import { BaseControlValueAccessor, provideAsControlValueAccessor, TypedFormGroup, TypedFormControl } from '@bmi/ui';
import { FetcherConfig } from '../../page-data-context/page-data-config';
import { distinctUntilNotEqual } from '../../util/distinct-until-not-equal';

export interface DataSourceEntry {
  id: string;
  config: FetcherConfig;
}


@Component({
  selector: 'bmi-data-source-entry-editor',
  templateUrl: './data-source-entry-editor.component.html',
  styleUrls: ['./data-source-entry-editor.component.scss'],
  providers: [provideAsControlValueAccessor(forwardRef(() => DataSourceEntryEditorComponent))],
})
export class DataSourceEntryEditorComponent extends BaseControlValueAccessor<DataSourceEntry> implements OnInit {
  @Output() onCloneClicked = new EventEmitter<void>();
  @Output() onDeleteClicked = new EventEmitter<void>();

  readonly form = new TypedFormGroup<DataSourceEntry>({
    id: new TypedFormControl<string>(''),
    config: new TypedFormControl<FetcherConfig>(null)
  });


  get dataSourceType() {
    return this.config && this.config.type;
  }

  get config() {
    return this.form.value.config;
  }

  ngOnInit() {
    this.form.valueChanges.pipe(
      distinctUntilNotEqual(),
    ).subscribe(value => {
      this.onChanged(value);
    });
  }

  updateComponentValue(entry: DataSourceEntry) {
    if (entry) {
      this.form.patchValue(entry, {emitEvent: false});
    } else {
      this.form.reset({
        id: '',
        config: null
      }, {emitEvent: false});
    }
  }

  configUpdated(config: FetcherConfig) {
    this.form.controls.config.setValue(config, {emitEvent: true});
  }
}
