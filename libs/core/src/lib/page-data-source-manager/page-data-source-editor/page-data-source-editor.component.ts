import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Validators } from '@angular/forms';

import { PageDataContext } from '../../page-data-context/page-data-context';

import { TemplatedFormArray, TypedFormGroup, TypedFormControl, DropdownMenuItem, ConfirmationModalService } from '@bmi/ui';
import { createMapFrom } from '@bmi/utils';
import cloneDeep from 'lodash-es/cloneDeep';

import { PageDataConfig, FetcherConfig } from '../../page-data-context/page-data-config';
import { DataSourceEntry } from '../data-source-entry-editor/data-source-entry-editor.component';
import { objectFromEntries } from '../../util/object-from-entries';
import { ValidateUniqueId } from '../data-context-editors/unique-id.validator';

interface DataSourceType<T extends FetcherConfig = FetcherConfig> {
  type: string;
  name: string;
  icon?: string;
  config: T;
}


const DATA_SOURCE_TYPES: DataSourceType[] = [
  {
    type: 'card',
    name: 'Single Card',
    icon: 'shared-ui-search',
    config: {
      type: 'card',
      board: null,
      conditions: []
    }
  },

  {
    type: 'card-list',
    name: 'Card List',
    icon: 'shared-ui-list',
    config: {
      type: 'card-list',
      board: null,
      conditions: []
    }
  },

  {
    type: 'notes',
    name: 'Notes Source',
    icon: 'cui-comment',
    config: {
      type: 'notes',
      board: null,
      keys: {}
    }
  },
  {
    type: 'pivot-transform',
    name: 'Transform - Pivot',
    icon: 'shared-ui-area-chart',
    config: {
      type: 'pivot-transform',
      transformId: null,
      conditions: []
    }
  }
];


function capitalize(value: string) {
  return value.substring(0, 1).toUpperCase() + value.substring(1);
}

function camelCase(text: string) {
  return text.split(/[_\-\s\b]+/g)
    .map((x, index) => index > 0 ? capitalize(x) : x)
    .join('');
}


@Component({
  selector: 'bmi-page-data-source-editor',
  templateUrl: './page-data-source-editor.component.html',
  styleUrls: ['./page-data-source-editor.component.scss'],
  providers: [
    PageDataContext
  ]
})
export class PageDataSourceEditorComponent implements OnInit {

  @Input() config: PageDataConfig;
  @Output() configChange = new EventEmitter<PageDataConfig>();

  readonly form = new TemplatedFormArray<DataSourceEntry>(() =>
    new TypedFormControl({id: '', config: null})
  );

  public selectedIndex: number = undefined;

  dropdownItems = DATA_SOURCE_TYPES.map(type => ({
      type: type.type,
      text: type.name,
      icon: type.icon
    }));

  sourceTypesByType = createMapFrom(DATA_SOURCE_TYPES, t => t.type);

  constructor(
    private localDataContext: PageDataContext,
    private confirmationService: ConfirmationModalService
  ) { }

  get selectedDataSourceId(): string {
    return this.selectedDataSource ? this.selectedDataSource.id : null;
  }

  ngOnInit() {
    this.localDataContext.setConfig(this.config);
    const dataSourceConfigs = Object.entries(this.config)
      .map(([id, config]) => <DataSourceEntry>{ id, config });

    this.form.patchValue(dataSourceConfigs, {emitEvent: false});

    // Pick the first data source
    if (this.form.length > 0) {
      this.selectedIndex = 0;
    }

    this.form.valueChanges.subscribe(value => {
      const dataSourceConfig = objectFromEntries(value.map<[string, FetcherConfig]>(({id, config}) => [id, config]));
      this.localDataContext.setConfig(dataSourceConfig);
      this.configChange.emit(dataSourceConfig);
    });
  }

  get selectedDataSource(): DataSourceEntry {
    return this.form.value[this.selectedIndex];
  }

  get dataSourceEntries(): DataSourceEntry[] {
    return this.form.value;
  }

  get selectedControl() {
    return this.form.get([this.selectedIndex]);
  }

  get hasSelectedIndex(): boolean {
    return typeof this.selectedIndex === 'number' && this.selectedIndex >= 0 && this.selectedIndex < this.form.length;
  }

  selectDataSource(dataSourceIndex: number) {
    this.selectedIndex = dataSourceIndex;
  }

  addSource(type: string = 'card') {
    const sourceType = DATA_SOURCE_TYPES.find(t => t.type === type);
    const config = cloneDeep(sourceType.config);

    this.form.pushValue({
      id: this.generateUniqueId(camelCase(`new ${type} source`)),
      config: config
    });

    this.selectLastSource();
  }

  getTypeName(sourceType: string): string {
    const type = this.sourceTypesByType.get(sourceType);
    return (type ? type.name : null) || sourceType;
  }

  getTypeIcon(sourceType: string) {
    const type = this.sourceTypesByType.get(sourceType);
    return type ? type.icon : null;
  }

  deleteSelectedSource() {
    if (this.hasSelectedIndex) {
      const selectedIndex = this.selectedIndex;
      this.confirmationService.show({
        content: `# Remove data source ${this.selectedDataSourceId}\n\n` +
          `Are you sure you want to remove this data source? This can be undone ` +
          `if you don't save the changes to the data context.`,
        buttonYesText: 'Remove'
      }).subscribe(() => {
        this.form.removeAt(selectedIndex);
        this.selectLastSource();
      });
    }
  }

  cloneSelectedSource() {
    const selectedEntry = this.selectedDataSource;
    if (selectedEntry) {
      this.form.pushValue({
        id: this.generateUniqueId(selectedEntry.id),
        config: cloneDeep(selectedEntry.config)
      });
      this.selectLastSource();
    }
  }

  private selectLastSource() {
    this.selectedIndex = this.form.length - 1;
  }

  private generateUniqueId(id: string): string {
    const existingIds = new Set(this.dataSourceEntries.map(e => e.id));
    let numericSuffix = 1;
    while (true) {
      const generatedId = `${id}${numericSuffix}`;
      if (!existingIds.has(generatedId)) {
        return generatedId;
      } else {
        numericSuffix += 1;
      }
    }
  }
}
