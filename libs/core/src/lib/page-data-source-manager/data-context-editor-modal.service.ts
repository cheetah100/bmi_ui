import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import { ModalService, ModalResult } from '@bmi/ui';
import cloneDeep from 'lodash-es/cloneDeep';

import { PageDataConfig } from '../page-data-context/page-data-config';
import { PageDataSourceEditorModalComponent } from './page-data-source-editor-modal.component';


interface DataContextModalOptions {
  config: PageDataConfig;
  injector: Injector;
  title?: string;
}


@Injectable({providedIn: 'root'})
export class DataContextEditorModalService {
  constructor(
    private modalService: ModalService
  ) {}

  show(options: DataContextModalOptions): Observable<PageDataConfig> {
    return this.modalService.open({
      title: options.title ?? 'Page Data Editor',
      content: PageDataSourceEditorModalComponent,
      injector: options.injector,
      size: 'large',
      data: {
        dataSourceConfig: cloneDeep(options.config || {})
      }
    }).pipe(
      filter(result => result?.action === 'save'),
      map(result => result.data)
    );
  }
}

