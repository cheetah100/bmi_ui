import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { PivotTransformSourceConfig } from '../../../data-sources/pivot-transform-source';
import { GravityConfigService, GravityApiConditionOperation, GravityTransformService } from '@bmi/gravity-services';
import { TypedFormGroup, TypedFormControl, TemplatedFormArray, Option } from '@bmi/ui';
import { ParameterBinding } from '../../../page-data-context/parameter-binding';
import { CardFetcherConfigCondition } from '../../../page-data-context/card-fetcher-impl';
import { map, distinctUntilChanged, switchMap } from 'rxjs/operators';

@Component({
  selector: 'bmi-pivot-transform-source-editor',
  templateUrl: './pivot-transform-source-editor.component.html',
  styleUrls: ['./pivot-transform-source-editor.component.scss']
})
export class PivotTransformSourceEditorComponent implements OnInit, OnChanges {

  @Input() config: PivotTransformSourceConfig;
  @Output() updatedConfig = new EventEmitter<any>();

  transformOptions = this.transformService.getTransformsByType('pivot').pipe(
    map(transforms =>  transforms.map(transform => <Option>{ text: transform.title, value: transform.id}))
  );
  transformBoard = null;

  readonly form = new TypedFormGroup<PivotTransformSourceConfig>({
    type: new TypedFormControl('pivot-transform'),
    transformId: new TypedFormControl<string>(null),
    conditions: new TemplatedFormArray<CardFetcherConfigCondition>(() => new TypedFormGroup({
      fieldName: new TypedFormControl(null),
      value: new TypedFormControl(null),

      // TODO; add components to set these. Optional means that the query can
      // still run if this parameter is not defined (the condition will be
      // omitted).
      optional: new TypedFormControl(false),
      operation: new TypedFormControl<GravityApiConditionOperation>('EQUALTO'),
    }))
  });


  constructor(
    private transformService: GravityTransformService
  ) { }

  ngOnInit() {
    this.form.get('transformId').valueChanges.subscribe(
      (transformId: string) => this.updateTransformBoard(transformId)
    );
    this.form.valueChanges.subscribe(() => this.updatedConfig.emit(this.form.value));
  }

  ngOnChanges(changes) {
    if (changes.config) {
      this.form.patchValue(this.config, {emitEvent: false});
      this.updateTransformBoard(this.config.transformId);
    }
  }

  get conditions() {
    return this.form.get('conditions') as TemplatedFormArray<CardFetcherConfigCondition>;
  }

  updateTransformBoard(transformId: string) {
    if (transformId) {
      this.transformService.get(transformId).subscribe(transformConfig => {
        this.transformBoard = transformConfig.transforms.a.configuration.board;
      });
    }

  }

  addCondition() {
    this.conditions.pushValue({
      fieldName: null,
      operation: 'EQUALTO',
      optional: false,
      value: null
    });
  }
}
