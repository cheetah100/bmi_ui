import { Component, Input, Output, EventEmitter, OnInit, OnDestroy, OnChanges } from '@angular/core';

import { Observable, Subscription } from 'rxjs';
import { distinctUntilChanged, map, shareReplay, startWith, switchMap } from 'rxjs/operators';

import { GravityConfigService } from '@bmi/gravity-services';
import { TypedFormGroup, TypedFormControl, Option, IdpValidators, TemplatedFormArray } from '@bmi/ui';
import { FormArray, FormControl } from '@angular/forms';
import { ObjectMap } from '@bmi/utils';

import { ParameterBinding } from '../../../page-data-context/parameter-binding';
import { NotesDataSourceConfig } from '../../../notes/notes-data-source';
import { NotesBoardService } from '../../../notes/notes-board.service';

import { objectFromEntries } from '../../../util/object-from-entries';
import { OptionlistService } from '../../../services/optionlist.service';


@Component({
  selector: 'bmi-notes-source-editor',
  templateUrl: './notes-source-editor.component.html'
})
export class NotesSourceEditorComponent implements OnInit, OnDestroy, OnChanges {
  @Input() config: NotesDataSourceConfig;
  @Output() updatedConfig = new EventEmitter<NotesDataSourceConfig>();

  readonly form = new TypedFormGroup<NotesDataSourceConfig>({
    type: new TypedFormControl('notes'),
    board: new TypedFormControl<string>(null, [IdpValidators.required]),
    keys: new TypedFormControl({}),
    visibilityKeys: new TemplatedFormArray(() => new TypedFormControl(null, [IdpValidators.required])),
  });

  get visibilityKeys() {
    return this.form.controls.visibilityKeys as TemplatedFormArray<any>;
  }

  boardOptions = this.optionListService.getAllBoards().pipe(
    map(boards => boards.map(b => <Option>{value: b.id, text: b.name})),
    shareReplay(1),
  );

  private subscription = new Subscription();

  constructor(
    private gravityConfigService: GravityConfigService,
    private notesBoardService: NotesBoardService,
    private optionListService: OptionlistService
  ) {}


  ngOnInit() {
    this.subscription.add(
      this.form.controls.board.valueChanges.pipe(
        distinctUntilChanged(),
        switchMap(boardId => this.getUpdatedKeyFields(boardId))
      ).subscribe(updatedKeys => {
        this.form.controls.keys.setValue(updatedKeys);
      })
    );

    this.subscription.add(this.form.valueChanges.subscribe(value => {
      this.updatedConfig.emit(this.form.value);
    }));
  }

  ngOnChanges(changes) {
    if (changes.config) {
      this.form.patchValue(this.config, {emitEvent: false});
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private getUpdatedKeyFields(boardId: string): Observable<ObjectMap<ParameterBinding>> {
    return this.notesBoardService.getSuggestedKeyFields(boardId).pipe(
      map(suggestedFields => {
        // We want to keep any existing entries which have bindings set up, or
        // that are in the suggested fields for the board.
        const existingEntries = Object.entries(this.form.controls.keys.value)
          .filter(([field, binding]) => suggestedFields.includes(field) || !!binding);
        const updatedKeyFields = objectFromEntries(existingEntries);
        // Any other suggested fields should be added as null
        for (const field of suggestedFields) {
          if (!updatedKeyFields.hasOwnProperty(field)) {
            updatedKeyFields[field] = null;
          }
        }

        return updatedKeyFields;
      })
    );
  }

  addVisibilityKey() {
    this.visibilityKeys.pushValue('');
  }

}
