import { AbstractControl } from '@angular/forms';

export function ValidateUniqueId(existingIds: string[]) {
  return (control: AbstractControl): {[key: string]: any} | null => {
    let validationError;
    if (control.value) {
      validationError = existingIds.includes(control.value);
    }
    return validationError ? {'invalidId': {value: control.value}} : null;
  };
}

