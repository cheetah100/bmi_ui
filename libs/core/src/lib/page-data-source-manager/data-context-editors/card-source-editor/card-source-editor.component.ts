import { Component, EventEmitter, OnChanges, Input, Output } from '@angular/core';
import { Validators, FormControl } from '@angular/forms';
import { GravityConfigService, GravityApiConditionOperation } from '@bmi/gravity-services';
import { Option, TypedFormGroup, TemplatedFormArray, TypedFormControl } from '@bmi/ui';
import { map, shareReplay } from 'rxjs/operators';
import { ValidateUniqueId } from '../unique-id.validator';

import { DataSourceConfig } from '../../../data-sources/types';
import { FilterConditionWithBinding } from '../../../data-sources/filter-condition-with-binding';
import { CardDataSourceConfig } from '../../../page-data-context/card-fetcher-impl';
import { ParameterBinding } from '../../../page-data-context/parameter-binding';

import { BoardPathResolver } from '@bmi/gravity-services';
import { OptionlistService } from '../../../services/optionlist.service';


@Component({
  selector: 'bmi-card-source-editor',
  templateUrl: './card-source-editor.component.html',
  styleUrls: ['./card-source-editor.component.scss']
})
export class CardSourceEditorComponent implements OnChanges {

  @Input() config: CardDataSourceConfig;
  @Output() updatedConfig = new EventEmitter();

  readonly sourceEditorForm = new TypedFormGroup<CardDataSourceConfig>({
    type: new FormControl(),
    board: new FormControl(),
    orderBy: new FormControl(),
    descending: new FormControl(),
    conditions: new TemplatedFormArray<FilterConditionWithBinding>(() => new TypedFormGroup({
      fieldName: new TypedFormControl(null),
      value: new TypedFormControl(null),

      // TODO; add components to set these. Optional means that the query can
      // still run if this parameter is not defined (the condition will be
      // omitted).
      optional: new TypedFormControl(false),
      operation: new TypedFormControl<GravityApiConditionOperation>('EQUALTO'),
    }))
  });

  cardDataSourceOptions = [{
    text: 'Single Card [card]',
    value: 'card'
  }, {
    text: 'Multiple Cards [card-list]',
    value: 'card-list'
  }];

  operationOptions = [{
    text: 'Equal To',
    value: 'EQUALTO'
  }, {
    text: 'Not Equal To',
    value: 'NOTEQUALTO'
  }, {
    text: 'Number / Boolean Equal To',
    value: 'NUMBEREQUALTO'
  }, {
    text: 'Number Not Equal To',
    value: 'NUMBERNOTEQUALTO'
  }];

  sourceIds = [];

  boardOptions = this.optionListService.getAllBoards().pipe(
    map(boards => boards.map(b => <Option>{value: b.id, text: b.name})),
    shareReplay(1),
  );

  get selectedBoard() {
    return this.sourceEditorForm.value.board || undefined;
  }

  constructor(
    private configService: GravityConfigService,
    private optionListService: OptionlistService
  ) { }

  get conditions() {
    return this.sourceEditorForm.get('conditions') as TemplatedFormArray<FilterConditionWithBinding>;
  }

  ngOnInit() {
    this.sourceEditorForm.valueChanges.subscribe(() => this.saveDataSource());
  }

  ngOnChanges(changes) {
    if (changes.config) {
      this.sourceEditorForm.reset(this.config, {emitEvent: false});
    }
  }

  getPreferredFormatterType(condition: FilterConditionWithBinding): string {
    if ((!condition.value || typeof condition.value === 'string') && this.getOptionlistBoard(condition.fieldName)) {
      return 'optionlist';
    } else if (typeof condition.value === 'string') {
      return 'text';
    } else {
      return 'data-binding';
    }
  }

  getOptionlistBoard(path: string) {
    const board = this.selectedBoard;
    if (path && board && this.configService.boardExists(board)) {
      const fp = new BoardPathResolver(board, this.configService).resolve(path);
      return fp.targetBoard;
    } else {
      return undefined;
    }
  }

  addCondition() {
    this.conditions.pushValue({
      fieldName: null,
      operation: 'EQUALTO',
      optional: false,
      value: null
    });
  }

  saveDataSource() {
    const config = {...this.sourceEditorForm.value };
    if (!config.descending) {
      delete config.descending;
    }

    if (!config.orderBy) {
      delete config.orderBy;
    }

    this.updatedConfig.emit(config);
  }
}
