import { Component, OnInit, EventEmitter, Inject } from '@angular/core';
import { Subject } from 'rxjs';
import { PageDataConfig } from '../page-data-context/page-data-config';
import { ModalContext } from '@bmi/ui';


@Component({
  selector: 'bmi-page-data-source-editor-modal',
  styleUrls: ['./page-data-source-editor-modal.component.scss'],
  template: `
    <div class="data-sources-modal__layout">
      <bmi-page-data-source-editor
        [config]="config"
        (configChange)="onConfigUpdated($event)">
      </bmi-page-data-source-editor>
    </div>
  `
})
export class PageDataSourceEditorModalComponent {
  constructor(
    private modalContext: ModalContext
  ) {}

  config = this.modalContext.data.dataSourceConfig || {};
  updatedConfig = this.config;

  ngOnInit() {
    this.modalContext.updateOptions({
      actions: [
        {
          action: 'save',
          buttonStyle: 'primary',
          buttonText: 'Save',
          invoke: () => ({action: 'save', data: this.updatedConfig}),
        },
        {
          action: 'cancel',
          buttonStyle: 'ghost',
          buttonText: 'Cancel',
        }
      ]
    });
  }

  onConfigUpdated(dataSources: PageDataConfig) {
    this.updatedConfig = dataSources;
  }
}
