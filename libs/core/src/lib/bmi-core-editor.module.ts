import { NgModule, Provider } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { PortalModule } from '@angular/cdk/portal';

import { GravityModule, GravityUiModule } from '@bmi/gravity-services';
import { SharedUiModule } from '@bmi/ui';
import { BmiChartsEditorModule } from '@bmi/legacy-charts';

import { BmiWidgetEditorCommonModule } from './bmi-core-widget-editor.module';
import { BmiCoreEditorComponentsModule } from './editor-components/bmi-core-editor-components.module';

import { BmiCoreModule } from './bmi-core.module';
import { BmiFlairEditorRegistryModule } from './flair/bmi-flair-editor.registry.module';

import { FormatterEditorRegistryModule } from './formatters/formatter-editor-registry.module';

import { BmiWidgetsEditorModule } from './widgets/bmi-widgets-editor.module';

import { PageEditorComponent } from './page-editor/page-editor.component';
import { EditorSidebarComponent } from './editor-sidebar/editor-sidebar.component';

import { PageDataSourceEditorComponent } from './page-data-source-manager/page-data-source-editor/page-data-source-editor.component';
import { PageDataSourceEditorModalComponent } from './page-data-source-manager/page-data-source-editor-modal.component';
import { DataSourceEntryEditorComponent } from './page-data-source-manager/data-source-entry-editor/data-source-entry-editor.component';
import { CardSourceEditorComponent } from './page-data-source-manager/data-context-editors/card-source-editor/card-source-editor.component';
import { NotesSourceEditorComponent } from './page-data-source-manager/data-context-editors/notes-source-editor/notes-source-editor.component';
import { PivotTransformSourceEditorComponent } from './page-data-source-manager/data-context-editors/pivot-transform-source-editor/pivot-transform-source-editor.component';

import { PagePropertiesSidebarComponent } from './page-properties-sidebar/page-properties-sidebar.component';
import { GenericEditWidgetSidebarComponent } from './editor-sidebar/generic-edit-widget-sidebar.component';
import { WidgetEditorModalComponent } from './widget-editor-modal/widget-editor-modal.component';


import { EventActionsEditorRegistry } from './event-actions/event-actions-editor.registry';


@NgModule({
  declarations: [
    PageEditorComponent,
    PageDataSourceEditorComponent,
    PageDataSourceEditorModalComponent,
    CardSourceEditorComponent,
    NotesSourceEditorComponent,
    PivotTransformSourceEditorComponent,
    DataSourceEntryEditorComponent,

    EditorSidebarComponent,
    GenericEditWidgetSidebarComponent,

    PagePropertiesSidebarComponent,
    WidgetEditorModalComponent,
  ],

  // Add components to the entryComponents list ONLY if they're being dynamically
  // loaded. i.e. Widget editors and sidebars.
  entryComponents: [
    PageDataSourceEditorModalComponent,
    WidgetEditorModalComponent,
  ],

  exports: [
    PageEditorComponent,
    BmiCoreEditorComponentsModule,

  ],

  imports: [
    CommonModule,
    BmiCoreModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    PortalModule,
    SharedUiModule,
    GravityUiModule,
    GravityModule.forRoot(),
    BmiChartsEditorModule,
    BmiCoreEditorComponentsModule,
    BmiFlairEditorRegistryModule,
    EventActionsEditorRegistry,
    FormatterEditorRegistryModule,
    BmiWidgetEditorCommonModule,
    BmiWidgetsEditorModule,
  ],
})
export class BmiCoreEditorModule { }
