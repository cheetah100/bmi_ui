import { Observable, of } from 'rxjs';
import { CardData, BoardService, GravityApiCondition, CardCacheService, BoardQueryParams } from '@bmi/gravity-services';
import { Binder } from './binder';
import { DataBindingEvent } from '../data-binding-event';
import { DataSourceConfig } from '../data-sources/types';
import { FilterConditionWithBinding, bindConditions, bindConditionsEvents } from '../data-sources/filter-condition-with-binding';

export type CardFetcherConfigCondition = FilterConditionWithBinding;


export interface CardDataSourceConfig extends DataSourceConfig {
  board: string;
  conditions: CardFetcherConfigCondition[];
  orderBy?: string;
  descending?: boolean;
  view?: string;
}


export function makeCardSearchQuery(config: CardDataSourceConfig, conditions: GravityApiCondition[]): BoardQueryParams {
  return {
    boardId: config.board,
    conditions: conditions,
    viewId: config.view || undefined,
    order: config.orderBy || '_id',
    descending: !!config.descending,
  };
}



export class CardFetcherImpl {
  constructor(
    private board: string,
    private binder: Binder,
    private boardService: BoardService,
    private cardCacheService?: CardCacheService
  ) { }

  fetch(conditions: GravityApiCondition[]): Observable<CardData[]> {
    if (!conditions) {
      return of(null);
    } else {
      const query = this.boardService.query(this.board)
        .filterBy(conditions)
        .orderBy('_id')
        .query;
      return this.cardCacheService ? this.cardCacheService.search(query) : this.boardService.search(query);
    }
  }
}
