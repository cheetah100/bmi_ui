import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { ParameterBinding } from './parameter-binding';
import { DataBindingEvent, makeValueEvent } from '../data-binding-event';
import { Binder } from './binder';

import isEqual from 'lodash-es/isEqual';


/**
 * A data-bound list is a table of bound data values, generated from 'mapping'
 * data bindings over a list (e.g. a card search, or transform result).
 *
 * This produces a 2D table of results (like a SQL query would). These results
 * are available synchronously, but each row of the results also functions as a
 * binder, for interoperability with code that needs to evaluate bindings
 * asynchronously.
 *
 * These data-bound lists are immutable; if new results are returned, a new
 * instance should be created with the updated data in it.
 *
 * This interface allows direct access to the raw data arrays for bulk data
 * processing: the `bindings` array correlates directly to the raw data in each
 * row, allowing efficient indexed lookups if necessary.
 */
export interface BoundListResult {
  bindings: ParameterBinding[];
  rows: BoundListRow[];
}


/**
 * A row in a data-bound list result.
 *
 * This stores the raw data, but also provides methods to synchronously (`get`)
 * and asynchronously (`bind`) resolve bindings on the row.
 *
 * Note: the current implementation will thorw an error if you attempt to get()
 * or bind() any parameters that are not explicitly included in the row.
 */
export class BoundListRow implements Binder {
  constructor (
    public readonly bindings: ParameterBinding[],
    public readonly data: any[],
  ) {}


  /**
   * Gets the value of a binding synchronously.
   *
   * Like bind(), but synchronous.
   */
  get(binding: ParameterBinding): any {
    if (!binding || typeof binding === 'string') {
      return binding;
    } else {
      const index = this.getIndex(binding);
      if (index >= 0) {
        return this.data[index];
      } else {
        throw new Error(`Unknown parameter binding: ${JSON.stringify(binding)}`);
      }
    }
  }

  /**
   * Gets a binding from this row as an observable
   *
   * Like get() but an observable.
   *
   * This is really just `of(get(x))` so that the list rows can be used as
   * binders.
   *
   * This will synchronously throw an error if a parameter is requested but it's
   * not part of the row -- this is a programming error, as a data-bound list
   * should specify the bindings it needs ahead of time.
   */
  bind(binding: ParameterBinding): Observable<any> {
    // TODO: should we defer unknown bindings off to a global binder instead
    // here?
    return of(this.get(binding));
  }

  bindEvents(binding: ParameterBinding): Observable<DataBindingEvent<any>> {
    return this.bind(binding).pipe(
      map(value => makeValueEvent(value))
    );
  }

  /**
   * Looks up the index for a binding in this row. If not found, this will
   * return -1.
   */
  getIndex(binding: ParameterBinding): number {
    return this.bindings.findIndex(x => isEqual(x, binding));
  }
}


export function makeBoundList(options: {
  listBindings: ParameterBinding[],
  listData: any[][],
  otherBindings?: ParameterBinding[],
  otherData?: any[]
}): BoundListResult {
  const bindings = [...options.listBindings, ...(options.otherBindings || [])];
  const rows = options.listData.map(row => [...row, ...(options.otherData || [])]);
  return {
    bindings: bindings,
    rows: rows.map(row => new BoundListRow(bindings, row))
  }
}
