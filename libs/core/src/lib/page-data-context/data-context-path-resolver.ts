import { PathResolver, FieldPath } from '@bmi/gravity-services';
import { ObjectMap } from '@bmi/utils';
import { Fetcher } from './fetcher';

import { splitPath } from './split-path';
import mapValues from 'lodash-es/mapValues';


interface PathSegment extends Partial<FieldPath> {
  fieldName: string;
  gravityType?: string;
  targetBoard?: string;
  isMetadata?: boolean;
  invalidityReason?: string;
}


export function makeFieldPath(pathSegments: PathSegment[]): FieldPath {
  function segmentToFieldPath(segment: PathSegment, parent: FieldPath = null): FieldPath {
    const path = <FieldPath>{
      fieldName: segment.fieldName,
      isListField: segment.isListField,
      isMultiValue: segment.isMultiValue,
      gravityType: segment.gravityType,
      targetBoard: segment.targetBoard,
      isMetadata: segment.isMetadata,
      isReferenceField: segment.isReferenceField,
      parent: parent,
      path: parent ? `${parent.path}.${segment.fieldName}` : segment.fieldName,
      isValid: !segment.invalidityReason && (!(parent && typeof (parent.isValid) === 'boolean') || parent.isValid),
      toString() {
        return this.path;
      }
    };

    path.segments = parent ? [...parent.segments, path] : [path];
    return path;
  }

  const head = segmentToFieldPath(pathSegments[0], null);
  const remaining = pathSegments.slice(1);
  return remaining.reduce<FieldPath>((parent, segment) => segmentToFieldPath(segment, parent), head);
}

function makeInvalidPath(path: string, reason: string) {
  return makeFieldPath(
    path.split('.').map(segment => (<PathSegment>{
      fieldName: segment,
      invalidityReason: reason
    }))
  );
}


export class DataContextPathResolver implements PathResolver {
  readonly dataSourceName = 'Page Data Context';

  constructor(private resolvers: ObjectMap<PathResolver>) {}

  resolve(path: string): FieldPath {
    if (!path) {
      return null;
    } else {
      const [sourceName, restOfPath] = splitPath(path);
      const resolver = this.resolvers[sourceName];
      if (!resolver) {
        return makeInvalidPath(path, `Unknown data source '${sourceName}'`);
      } else {
        const resolvedPath = resolver.resolve(restOfPath);
        const resolvedSegments = resolvedPath ? resolvedPath.segments : [];
        return makeFieldPath([{fieldName: sourceName}, ...resolvedSegments]);
      }
    }
  }

  getChildPaths(fieldPath: FieldPath): FieldPath[] {
    if (!fieldPath) {
      return this.getRootPaths();
    } else {
      const rootSegment = fieldPath.segments[0];
      const remainder = fieldPath.segments.slice(1);
      const resolver = this.resolvers[rootSegment.fieldName];
      if (!resolver) {
        return [];
      } else {
        return resolver.getChildPaths(remainder.length ? makeFieldPath(remainder) : null)
          .map(path => makeFieldPath([{fieldName: rootSegment.fieldName}, ...path.segments]));
      }
    }
  }

  private getRootPaths(): FieldPath[] {
    return Object.keys(this.resolvers)
    .map(key => makeFieldPath([{ fieldName: key }]));
  }
}
