import { Provider } from '@angular/core';

import { PageDataContext } from './page-data-context';
import { PathResolverContext } from './path-resolver-context';
import { FilterService } from '../filters/filter.service';


export function provideDataContextServices(): Provider {
  return [
    FilterService,
    PageDataContext,
    { provide: PathResolverContext, useExisting: PageDataContext }
  ]
}
