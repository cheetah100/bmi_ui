import { Directive, Input, OnChanges, SimpleChanges, Provider } from '@angular/core';
import { ObjectMap } from '@bmi/utils';
import { PageDataConfig } from './page-data-config';
import { PageDataContext } from './page-data-context';
import { DataSource } from '../data-sources/types';
import { provideDataContextServices } from './helpers';
import { PathResolverContext } from './path-resolver-context';

@Directive({
  selector: '[bmiDataContext], [bmiDataContextSources]',

  providers: [
    provideDataContextServices()
  ]
})
export class DataContextDirective {
  @Input('bmiDataContext') config: PageDataConfig;
  @Input('bmiDataContextSources') sources: ObjectMap<DataSource>;

  constructor(
    private dataContext: PageDataContext,
  ) {}

  ngOnChanges(changes: SimpleChanges) {
    if (changes.config) {
      this.dataContext.setConfig(this.config);
    }

    if (changes.sources) {
      this.dataContext.setDataSources(this.sources);
    }
  }

}


