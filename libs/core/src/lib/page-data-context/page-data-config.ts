export interface FetcherConfig {
  name?: string;
  type: string;
  [propName: string]: any;
}


export interface PageDataConfig {
  [id: string]: FetcherConfig;
}

