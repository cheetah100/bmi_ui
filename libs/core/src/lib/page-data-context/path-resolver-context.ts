import { Observable } from 'rxjs';
import { PathResolver } from '@bmi/gravity-services';


export abstract class PathResolverContext {
  abstract getPathResolver(): Observable<PathResolver>;
}
