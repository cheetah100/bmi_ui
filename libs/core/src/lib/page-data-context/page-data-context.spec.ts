import { TestBed, fakeAsync, tick, flush, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { of } from 'rxjs';

import { PageDataContext } from './page-data-context';
import { ParameterBinding } from './parameter-binding';
import { DATA_SOURCE_FACTORY_TOKEN, DATA_SOURCE_TYPE_TOKEN, defineDataSourceType } from '../data-sources/data-source-factory';

import { FilterService } from '../filters/filter.service';
import { StringFilter } from '../filters/string-filter';
import { FormatterService } from '../formatter';

import { BoardService, CardData, CardFields, GravityConfigService, gravityTestbed, GravityTransformService } from '@bmi/gravity-services';
import { CardDataSourceFactory } from '../data-sources/card-data-source';

import { TestBinding, TestBoardService, BoardQueryParams, makeCondition, testCardQueryHandler } from './test-utils';


describe('PageDataContext', () => {
  let filterService: FilterService;
  let boardService: TestBoardService;
  let pdc: PageDataContext;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],

      providers: [
        FilterService,
        { provide: BoardService, useClass: TestBoardService },
        gravityTestbed({}),
        PageDataContext,
        FormatterService,

        // This isn't being provided in the Gravity testbed, nor is it a
        // tree-shakable provider so it's not getting loaded on demand.
        {provide: GravityTransformService },

        defineDataSourceType({
          type: 'card',
          name: 'Card',
          service: CardDataSourceFactory,
        }),
      ]
    });

    filterService = TestBed.get(FilterService) as FilterService;
    boardService = TestBed.get(BoardService) as TestBoardService;
    pdc = TestBed.get(PageDataContext) as PageDataContext;
  }));

  function makeCard(id: string, board: string, fields: CardFields = {}): CardData {
    return { id, board, fields, phase: 'current' }
  }

  const TEST_CARD_DATA: CardData[] = [
    makeCard('foo', 'test_board', { name: 'Foo Card' }),
    makeCard('test', 'test_board', {
      'name': 'Test Card',
      'list': ['a', 'b'],
      'number': 3,
      'otherCardId': 'foo'
    })
  ];

  function makeCardFetcherConfig(filterId: string) {
    return {
      type: 'card',
      board: 'test_board',
      conditions: [{fieldName: 'id', value: {bindingType: 'filter', filterId: filterId}}]
    };
  }


  describe('Parameter binding', () => {
    describe('Literal string binding', () => {
      let hasEmitted: boolean;
      let isComplete: boolean;
      let boundValue: any;

      beforeEach(() => {
        hasEmitted = false;
        isComplete = false;
        boundValue = undefined;
        pdc.bindParameter('foobar').subscribe({
          next: bv => {
            hasEmitted = true;
            boundValue = bv;
          },
          complete: () => isComplete = true
        });
      });

      it('should emit the string synchronously', () => expect(hasEmitted).toBeTruthy());
      it('should emit the literal value',  () => expect(boundValue).toBe('foobar'));
      it('should complete once done', () => expect(isComplete).toBeTruthy());
    });

    describe('Filter binding', () => {
      let emittedValues: any[];
      let isComplete = false;

      beforeEach(() => {
        filterService.addFilter(new StringFilter('test', 'Test Filter'));

        emittedValues = [];
        isComplete = false;

        pdc.bindParameter({bindingType: 'filter', filterId: 'test'}).subscribe({
          next: value => emittedValues.push(value),
          complete: () => isComplete = true
        });
      });

      it('should initially return an null value', () => {
        expect(emittedValues).toEqual([null]);
      });

      it('should emit the filter value once the filter is set', () => {
        filterService.set('test', 'foobar');
        expect(emittedValues).toEqual([null, 'foobar']);
      });
    });
  });



/*
 * Commented out: this doesn't belong here and has broken due to changing the
 * implementation of path lookups.
 *
 * Can't be bothered fixing this as this test suite is already bloated because
 *  the current PDC implementation has too many responsibilities.
 *
 * describe('lookupPath()', () => {
 *   it('should return null if the fetcher portion of the path is unknown', () => {
 *     const values = [];
 *     pdc.lookupPath('foobar.name').subscribe(value => values.push(value))
 *     expect(values).toEqual([null]);
 *   });
 *
 *   describe('With a card fetcher', () => {
 *     beforeEach(() => {
 *       boardService.handleQuery = testCardQueryHandler(TEST_CARD_DATA);
 *       filterService.addFilter(new StringFilter('filterA', 'Test Filter'));
 *       filterService.addFilter(new StringFilter('filterB', 'Test Filter'));
 *       pdc.setConfig({
 *         test: makeCardFetcherConfig('filterA')
 *       });
 *     });
 *
 *     it('should return fields from the card', fakeAsync(() => {
 *       filterService.set('filterA', 'test');
 *       tick();
 *
 *       const results = [];
 *       pdc.lookupPath('test.name').subscribe(x => results.push(x));
 *       tick();
 *
 *       expect(results).toEqual(['Test Card']);
 *     }));
 *
 *     it('should look up the value after the data config is changed', fakeAsync(() => {
 *       filterService.set('filterB', 'test');
 *       const results = [];
 *       pdc.lookupPath('newFetcher.name').subscribe(x => results.push(x));
 *       tick();
 *
 *       expect(results).toEqual([null]);
 *
 *       pdc.setConfig({
 *         ...pdc.config,
 *         newFetcher: makeCardFetcherConfig('filterB')
 *       });
 *
 *       tick();
 *
 *       expect(results).toEqual([null, 'Test Card'])
 *     }));
 *
 *     it('should emit null if a required filter is cleared', fakeAsync(() => {
 *       const results = [];
 *       pdc.lookupPath('test.name').subscribe(x => results.push(x));
 *
 *       filterService.set('filterA', 'test');
 *       tick();
 *
 *       filterService.set('filterA', null);
 *       tick();
 *
 *       // The CardFetcher doesn't emit a null value first when doing a path
 *       // lookup
 *       expect(results).toEqual(['Test Card', null]);
 *     }));
 *   });
 * });
 */

  describe('Data context parameter bindings between fetchers', () => {
    beforeEach(() => {
      boardService.handleQuery = testCardQueryHandler(TEST_CARD_DATA);
      filterService.addFilter(new StringFilter('filterA', 'Test Filter'));

      pdc.setConfig({
        test: makeCardFetcherConfig('filterA'),
        otherCard: {
          type: 'card',
          board: 'test_board',
          conditions: [{fieldName: 'id', value: { bindingType: 'data-path', path: 'test.otherCardId'}}]
        }
      });
    });

    it('should have a null state if the path has not emitted', fakeAsync(() => {
      tick();
      expect(pdc.snapshot['otherCard']).toBe(null);
    }));

    it('should resolve the card once the data is available', fakeAsync(() => {
      filterService.set('filterA', 'test');
      tick();
      expect(pdc.snapshot['otherCard']).toBeDefined();
    }));

    // Commented out: PathLookupService needs to be mocked for testing. This belongs elsewhere.
    //
    // it('should make this resolved card available for path lookups', fakeAsync(() => {
    //   const values = [];
    //   pdc.lookupPath('otherCard.name').subscribe(x => values.push(x));
    //   filterService.set('filterA', 'test');
    //   tick();

    //   expect(values).toEqual(['Foo Card']);
    // }));
  });
});
