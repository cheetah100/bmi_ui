import { Injectable, Optional, SkipSelf, OnDestroy } from '@angular/core';

import { BehaviorSubject, Observable, of, throwError, combineLatest, Subscription } from 'rxjs';
import { distinctUntilChanged, map, switchMap, startWith, tap, publishReplay, refCount } from 'rxjs/operators';

import { BoardService, PathLookupService, GravityConfigService, PathResolver, CardCacheService } from '@bmi/gravity-services';
import { ObjectMap, combineLatestObject } from '@bmi/utils';
import { distinctUntilNotEqual } from '../util/distinct-until-not-equal'

import { PageDataConfig, FetcherConfig } from './page-data-config';
import { Fetcher } from './fetcher';
import { Binder } from './binder';
import { BoundListResult } from './bound-list-result';
import { ParameterBinding, migrateBinding, isPathBinding } from './parameter-binding';

import { CardDataSourceConfig } from './card-fetcher-impl';

import { FilterService } from '../filters/filter.service';

import { DataContextPathResolver } from './data-context-path-resolver';

import { splitPath } from './split-path';

import { DataBindingEvent, makeValueEvent } from '../data-binding-event';
import { extractValuesOnly } from '../util/data-binding-operators';

import mapValues from 'lodash-es/mapValues';
import pickBy from 'lodash-es/pickBy';

import { DataSourceService } from '../data-sources/data-source.service';
import { DataSourceWithListApi, DataSourceWithBoard } from '../data-sources/types';

import { FormatterService, isFormatterConfig } from '../formatter';


export type PageData = ObjectMap<any>;

interface CreateDataContextOptions {
  dataSources?: ObjectMap<Fetcher>;
  config?: PageDataConfig;
}

@Injectable()
export class PageDataContext implements Binder, OnDestroy {
  private _config = new BehaviorSubject<PageDataConfig>({});
  private _fetchers = new BehaviorSubject<ObjectMap<Fetcher>>({});
  private _addedDataSources = new BehaviorSubject<ObjectMap<Fetcher>>({});
  private subscription = new Subscription();

  private _state = new BehaviorSubject<PageData>({});

  public readonly dataSources: Observable<ObjectMap<Fetcher>> = combineLatest([
    this._fetchers,
    this.parentBinder ? this.parentBinder.dataSources : of(<ObjectMap<Fetcher>>{})
  ]).pipe(
    map(([dataSources, parentSources]) => ({
      ...parentSources,
      ...dataSources
    }))
  );

  private _dataSourcesSnapshot: ObjectMap<Fetcher> = {};

  constructor(
    @Optional() @SkipSelf() private parentBinder: PageDataContext,
    private dataSourceService: DataSourceService,
    private formatterService: FormatterService,
    private filterService: FilterService,
  ) {
    this.subscription.add(combineLatest([
      this._config.pipe(
        distinctUntilNotEqual(),
        map(config => mapValues(config, c => this.createFetcher(c)))
      ),
      this._addedDataSources
    ]).pipe(
      map(([sourcesFromConfig, addedDataSources]) => ({
        ...sourcesFromConfig,
        ...addedDataSources,
      }))
    ).subscribe(this._fetchers));

    this.subscription.add(this._fetchers.pipe(
      switchMap(fetchers => combineLatestObject(
        mapValues(fetchers, f => {
          return f && f.state ? f.state.pipe(startWith(null)) : of(null);
        })
      )),
    ).subscribe(this._state));

    this.subscription.add(
      this.dataSources.subscribe(dataSources => this._dataSourcesSnapshot = {...dataSources})
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  /**
   * Creates a child data context from this one.
   */
  create(options?: CreateDataContextOptions): PageDataContext {
    const newContext = new PageDataContext(
      this,
      this.dataSourceService,
      this.formatterService,
      this.filterService
    );

    if (options?.config) {
      newContext.setConfig(options.config);
    }

    if (options?.dataSources) {
      newContext.setDataSources(options.dataSources);
    }

    return newContext;
  }

  /**
   * @deprecated use data binding instead
   */
  get state(): Observable<PageData> {
    return this._state.asObservable();
  }

  /**
   * @deprecated use data binding instead
   */
  get snapshot(): PageData {
    return this._state.value;
  }

  get config() {
    return this._config.value;
  }

  get dataSourcesSnapshot() {
    return this._dataSourcesSnapshot;
  }

  /**
   * @deprecated exposing a data context state like this was a bad idea that
   * exposes internal details of how the fetchers work. This limits the options
   * for optimization in future, and makes testing harder.
   *
   * Use the proper binding APIs instead.
   */
  watch<T = any>(id: string): Observable<T> {
    return this.state.pipe(
      map(state => state[id] as T),
      distinctUntilChanged(),
      publishReplay(1),
      refCount()
    );
  }

  setConfig(config: PageDataConfig): void {
    this._config.next(config);
  }

  setDataSources(sources: ObjectMap<Fetcher>) {
    this._addedDataSources.next(sources || {});
  }

  getFetcher<T extends Fetcher = Fetcher>(id: string): Observable<T> {
    return this.dataSources.pipe(
      map(fetchers => fetchers[id] as T),
      distinctUntilChanged(),
      publishReplay(1),
      refCount()
    );
  }

  private lookupPath<T = any>(path: string): Observable<T> {
    const [fetcherId, restOfPath] = splitPath(path);
    return this.getFetcher(fetcherId).pipe(
      switchMap(fetcher => {
        if (!fetcher || !fetcher.lookupPath) {
          return of(null);
        } else {
          return fetcher.lookupPath(restOfPath);
        }
      }),
      distinctUntilChanged()
    );
  }

  bindThis(sourceId: string, bindings: ParameterBinding[]): Observable<BoundListResult> {
    if (!sourceId) {
      return throwError(new Error('Data source not provided'));
    } else {
      return this.getFetcher(sourceId).pipe(
        switchMap(fetcher => {
          if (!fetcher || !fetcher.bindThis) {
            return throwError(new Error(`Data source '${sourceId}' does not exist`));
          } else if (!fetcher.bindThis) {
            return throwError(new Error(`Data source '${sourceId}' does not support list operations`));
          } else {
            return fetcher.bindThis(bindings);
          }
        }),
        publishReplay(1),
        refCount()
      );
    }
  }

  getBoard(sourceId:string): Observable<string>{
    if (!sourceId) {
      return throwError(new Error('Data source not provided'));
    } else {
      return this.getFetcher(sourceId).pipe(
        map((fetcher: DataSourceWithBoard) => fetcher.board)
      );
    }
  }

  getPathResolver(options: {
    thisDataSourceId?: string,
    filterPredicate?: (fetcher: Fetcher) => boolean
  } = {}): Observable<PathResolver> {
    return this.dataSources.pipe(
      map(dataSources => this.getPathResolverNow(options))
    );
  }

  /**
   * Get a path resolver for the data context
   *
   * This combines together all of the data sources that implement the
   * getPathResolver() operation into a single PathResolver that can be used to
   * power a path-picker UI control.
   *
   * If picking a value where a 'this' binding is valid, you can specify the
   * name of another data source to use as the special 'this' data source name.
   * If that source supports path-picking, it will be provided as 'this'
   *
   * @deprecated Use the observable version.
   */
  getPathResolverNow(options: {
    thisDataSourceId?: string,
    filterPredicate?: (fetcher: Fetcher) => boolean
  } = {}): PathResolver {
    options = options || {};

    const resolvers = mapValues(
      pickBy(
        this.dataSourcesSnapshot,
        fetcher => !!fetcher.getPathResolver &&
            (!options.filterPredicate || options.filterPredicate(fetcher))),
      value => value.getPathResolver()
    );

    if (options.thisDataSourceId && resolvers[options.thisDataSourceId]) {
      resolvers['this'] = resolvers[options.thisDataSourceId];
    }

    return new DataContextPathResolver(resolvers);
  }


  private createFetcher(fetcherConfig: FetcherConfig): Fetcher {
    return this.dataSourceService.create(fetcherConfig, this);
  }

  /**
   * @deprecated use bind() instead
   */
  bindParameter(parameter: ParameterBinding): Observable<any> {
    return this.bind(parameter);
  }

  bind<T = any>(binding: ParameterBinding): Observable<T> {
    return this.bindEvents(binding).pipe(extractValuesOnly()) as Observable<T>;
  }

  bindEvents(parameter: ParameterBinding): Observable<DataBindingEvent> {
    if (!parameter || typeof parameter === 'string') {
      return of(makeValueEvent(parameter));
    } else if (parameter.bindingType === 'filter') {
      return this.filterService.watchFilterState(parameter.filterId).pipe(
        map(filterState => makeValueEvent(filterState && filterState.rawValue))
      );
    } else if (parameter.bindingType === 'data-path') {
      return this.lookupPath(parameter.path).pipe(map(value => makeValueEvent(value)));
    } else if (parameter.bindingType === 'formatter' || isFormatterConfig(parameter)) {
      return this.formatterService.bindFormatter(parameter, this).pipe(map(v => makeValueEvent(v)));
    } else {
      console.error(`Unsupported binding: ${JSON.stringify(parameter)}`);
      return of(makeValueEvent(null));
    }
  }
}

