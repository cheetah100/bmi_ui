import { Observable } from 'rxjs';
import { BoundListResult, BoundListRow, makeBoundList } from './bound-list-result';
import { ParameterBinding } from './parameter-binding';
import { makeValueEvent } from '../data-binding-event';

import isEqual from 'lodash-es/isEqual';

function makeRow(dataTuples: [ParameterBinding, unknown][]) {
  return new BoundListRow(dataTuples.map(x => x[0]), dataTuples.map(x => x[1]));
}


function expectUnknownParamError(f: Function) {
  expect(f).toThrowError(/^Unknown parameter.*$/);
}

function expectEmittedValue<T>(obs: Observable<T>) {
  let value: T;
  let hasEmitted = false;
  let hasCompleted = false;

  obs.subscribe({
    next: x => {
      value = x;
      hasEmitted = true;
    },
    complete: () => hasCompleted = true
  });

  if (!hasEmitted) {
    throw new Error('Observable did not emit a value')
  }

  if (!hasCompleted) {
    throw new Error('Observable did not complete');
  }

  return expect(value);
}


function thisBinding(path: string): ParameterBinding {
  return { bindingType: 'data-path', path: `this.${path}`};
}

function filterBinding(filterId: string): ParameterBinding {
  return { bindingType: 'filter', filterId: filterId};
}


describe('BoundListRow', () => {
  describe('With an empty list of bindings', () => {
    let row : BoundListRow;

    beforeEach(() => {
      row = makeRow([]);
    });

    it('should have no bindings', () => expect(row.bindings).toEqual([]));
    it('should have no data', () => expect(row.data).toEqual([]));

    it('should accept null bindings and return a falsy value', () => {
      expect(row.get(null)).toBeFalsy();
    });

    it('should accept undefined bindings and return a falsy value', () => {
      expect(row.get(undefined)).toBeFalsy();
    });

    it('should return string values verbatim', () => {
      expect(row.get('foobar')).toBe('foobar');
    });

    it('should throw an error if you attempt to get a non-const binding', () => {
      expectUnknownParamError(() => {
        row.get(thisBinding('foobar'));
      });
    });

    it('should throw an error if you attempt to bind a non-const binding', () => {
      expectUnknownParamError(() => {
        row.bind(thisBinding('foobar'));
      });
    });

    it('should return -1 from getIndex for any non-const bindings', () => {
      expect(row.getIndex(thisBinding('anything'))).toBe(-1);
    });

    it('should return -1 from getIndex for string bindings too', () => {
      expect(row.getIndex('blah')).toBe(-1);
    })
  });

  describe('With some data', () => {
    let row: BoundListRow;

    beforeEach(() => {
      row = makeRow([
        [thisBinding('foo'), 45],
        [thisBinding('bar'), 'testing'],
        [thisBinding('name'), 'Test Row Name'],
        [thisBinding('value'), 'Test Row Value'],
        [filterBinding('testFilter'), 'Test Filter Value'],
        [thisBinding('bar'), 'duplicated value']
      ]);
    });

    describe('get()', () => {
      it('should throw an error if the binding is missing', () => {
        expectUnknownParamError(() => {
          row.get(thisBinding('unknownParameter'));
        });
      });

      it('should accept null bindings and return a falsy value', () => {
        expect(row.get(null)).toBeFalsy();
      });

      it('should accept undefined bindings and return a falsy value', () => {
        expect(row.get(undefined)).toBeFalsy();
      });

      it('should return string values verbatim', () => {
        expect(row.get('foobar')).toBe('foobar');
      });

      it('should find path bindings', () => {
        expect(row.get(thisBinding('name'))).toEqual('Test Row Name');
      })

      it('should find filter bindings', () => {
        expect(row.get(filterBinding('testFilter'))).toEqual('Test Filter Value');
      });

      it('should take the first binding in case of duplicates', () => {
        expect(row.get(thisBinding('bar'))).toBe('testing');
      });
    });

    describe('bind()', () => {
      it('should throw if the binding is missing', () => {
        expectUnknownParamError(() => { row.bind(thisBinding('not-here')) });
      });

      it('should emit the value synchronously if present', () => {
        expectEmittedValue(row.bind(thisBinding('foo'))).toEqual(45);
      });

      it('should emit strings verbatim', () => {
        expectEmittedValue(row.bind('foo')).toEqual('foo');
      });
    });

    describe('bindEvents()', () => {
      it('should emit values wrapped in value events', () => {
        expectEmittedValue(row.bindEvents(thisBinding('foo'))).toEqual(makeValueEvent(45));
      });
    });
  });
});




describe('makeBoundList', () => {
  describe('With data', () => {
    let list: BoundListResult;

    beforeEach(() => {
      list = makeBoundList({
        listBindings: [thisBinding('id'), thisBinding('animal'), thisBinding('isGoodPet')],
        listData: [
          [45, 'dog', true],
          [46, 'cat', true],
          [47, 'giraffe', false],
          [48, 'bobcat', false]
        ],

        // These are just extra bindings that will be the same for each row.
        otherBindings: [filterBinding('year'), 'someConstant'],
        otherData: ['2020', 'Yay']
      });
    });

    it('should combine the this and other bindings', () => {
      expect(list.bindings).toEqual([
        thisBinding('id'), thisBinding('animal'), thisBinding('isGoodPet'),
        filterBinding('year'), 'someConstant'
      ]);
    });

    it('should add the other values to the end of each row', () => {
      expect(list.rows.every(row => isEqual(row.data.slice(-2), ['2020', 'Yay']))).toBeTruthy();
    });

    it('should have the expected number of rows', () => {
      expect(list.rows.length).toBe(4);
    });
  });

  describe('Without any `other` bindings', () => {
    let list: BoundListResult;
    beforeEach(() => {
      list = makeBoundList({
        // It shouldn't care what type of bindings these are anyway.
        listBindings: [thisBinding('id'), thisBinding('animal'), thisBinding('isGoodPet')],
        listData: [
          [45, 'dog', true],
          [46, 'cat', true],
          [47, 'giraffe', false],
          [48, 'bobcat', false]
        ],
      });
    });

    it('should have the right number of bindings', () => expect(list.bindings.length).toBe(3));
    it('should have the correct column count in each row', () => {
      expect(list.rows.every(row => row.data.length === 3)).toBeTruthy();
    });
  });
});
