import { Observable } from 'rxjs';
import { ParameterBinding } from './parameter-binding';
import { DataBindingEvent } from '../data-binding-event';


export interface Binder {
  bind<T = any>(binding: ParameterBinding): Observable<T>;
  bindEvents(binding: ParameterBinding): Observable<DataBindingEvent>;
}
