import { Directive, Input, forwardRef, Optional } from '@angular/core';

import { BehaviorSubject, of } from 'rxjs';
import { distinctUntilChanged, switchMap } from 'rxjs/operators';
import { PathResolver } from '@bmi/gravity-services';
import { PageDataContext } from './page-data-context';
import { PathResolverContext } from './path-resolver-context';


@Directive({
  selector: '[bmiThisPathSource]',
  providers: [
    { provide: PathResolverContext, useExisting: forwardRef(() => ThisPathResolverDirective) }
  ]
})
export class ThisPathResolverDirective extends PathResolverContext {
  constructor(
    private dataContext: PageDataContext
  ) {
    super();
  }

  @Input('bmiThisPathSource') dataSourceId: string;
  private dataSourceIdSubject = new BehaviorSubject<string>(undefined);

  readonly pathResolver = this.dataSourceIdSubject.pipe(
    distinctUntilChanged(),
    switchMap(dataSourceId => this.dataContext.getPathResolver({thisDataSourceId: this.dataSourceId}))
  );

  ngOnChanges() {
    this.dataSourceIdSubject.next(this.dataSourceId);
  }

  getPathResolver() {
    return this.pathResolver;
  }
}
