import { Observable, Subject, NEVER, of } from 'rxjs';
import { map } from 'rxjs/operators';

import lodashIsEqual from 'lodash-es/isEqual';

import { Binder } from './binder';
import { makeValueEvent } from '../data-binding-event';
import { ParameterBinding } from './parameter-binding';
import { BoardService, GravityApiCondition, GravityApiConditionOperation, CardData } from '@bmi/gravity-services';
import { Injectable } from '@angular/core';


export class TestBinding {
  valueSubject = new Subject<any>();
  constructor (public readonly binding: ParameterBinding) {}

  emit(value: any) {
    this.valueSubject.next(value);
  }
}


 export class TestParameterBinder implements Binder {
  public bindings: TestBinding[] = [];

  getBinding(binding: ParameterBinding): TestBinding {
    const existing = this.findBinding(binding);
    if (existing) {
      return existing;
    } else {
      const testBinding = new TestBinding(binding);
      this.bindings.push(testBinding);
      return testBinding;
    }
  }

  private findBinding(binding: ParameterBinding): TestBinding {
    return this.bindings.find(x => lodashIsEqual(x.binding, binding));
  }

  bind(paramBinding: ParameterBinding): Observable<any> {
    return this.getBinding(paramBinding).valueSubject.asObservable();
  }

  bindEvents(binding: ParameterBinding) {
    return this.bind(binding).pipe(
      map(value => makeValueEvent(value))
    );
  }

  hasBinding(paramBinding: ParameterBinding): boolean {
    return !!this.findBinding(paramBinding);
  }

  emit(paramBinding: ParameterBinding, value: any) {
    this.getBinding(paramBinding).emit(value);
  }
}


export interface BoardQueryParams {
  boardId: string;
  conditions?: GravityApiCondition[];
}

@Injectable()
export class TestBoardService extends BoardService {
  queries: BoardQueryParams[] = [];

  handleQuery: (params: BoardQueryParams) => Observable<CardData[]>;

  search<T extends CardData = CardData>(boardQuery: BoardQueryParams): Observable<T[]> {
    this.queries.push(boardQuery);
    if (this.handleQuery) {
      return this.handleQuery(boardQuery) as Observable<T[]>;
    } else {
      return NEVER;
    }
  }
}


export function testCardQueryHandler(cards: CardData[]) {
  return (query: BoardQueryParams) => {
    const boardCards = cards.filter(c => c.board === query.boardId);
    if (query.conditions && query.conditions.length > 0) {
      const condField = query.conditions[0].fieldName;
      const condValue = query.conditions[0].value;
      if (query.conditions.length > 1 || (condField !== 'id' && condField !== '_id') || condValue.includes('|')) {
        throw new Error('Test Card Query Handler only supports a single ID condition');
      } else {
        return of(boardCards.filter(c => c.id === condValue));
      }
    } else {
      return of(boardCards);
    }
  }
}


export function makeCondition(fieldName: string, value: string, operation: GravityApiConditionOperation = 'EQUALTO'): GravityApiCondition {
  return { fieldName, value, operation: operation, conditionType: 'PROPERTY', };
}
