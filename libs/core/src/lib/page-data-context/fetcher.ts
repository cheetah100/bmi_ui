import { Observable } from 'rxjs';

import {
  DataSource,
  DataSourceWithBoard,
  DataSourceWithListApi,
  DataSourceWithPathLookup,
  DataSourceWithPathMetadata
} from '../data-sources/types';

import { DataSourceWithListGroupApi } from '../data-sources/data-group';

/**
 * A data source
 *
 * @deprecated use more specific types and especially don't use the `state` property.
 */
export interface Fetcher<T = any> extends
    Partial<DataSource>,
    Partial<DataSourceWithBoard>,
    Partial<DataSourceWithListApi>,
    Partial<DataSourceWithPathLookup>,
    Partial<DataSourceWithPathMetadata>,
    Partial<DataSourceWithListGroupApi>
{
  /**
  * Gets the state of the fetcher.
  * @deprecated
  */
  readonly state?: Observable<T>;

}
