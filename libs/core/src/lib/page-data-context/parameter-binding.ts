import { Observable } from 'rxjs';

import { splitPath } from './split-path';


export interface FilterBinding {
  bindingType: 'filter';
  filterId: string;
}


export interface DataPathBinding {
  bindingType: 'data-path' | 'this-path';
  path: string;
}

export type ParameterBinding = string | BindingObject;
export type BindingObject = FilterBinding | DataPathBinding | FormatterBinding;


export type FormatterBinding<T extends FormatterConfig = FormatterConfig> = {bindingType: 'formatter'} & T;

export interface FormatterConfig {
  formatterType: string;
  [field: string]: any;
}


export function isParameterBinding(x: unknown): x is ParameterBinding {
  return x && (typeof x === 'string' || isBindingObject(x));
}

export function isBindingObject(x: unknown): x is BindingObject {
  return x && typeof x === 'object' && !!x.hasOwnProperty('bindingType');
}

export function isPathBinding(x: unknown): x is DataPathBinding {
  return isBindingObject(x) && x.bindingType === 'data-path';
}


export function isFormatterBinding(x: unknown): x is FormatterBinding {
  return isBindingObject(x) && x.bindingType === 'formatter';
}


export function isFilterBinding(binding: ParameterBinding): binding is FilterBinding {
  return isBindingObject(binding) && binding.bindingType === 'filter';
}


/**
 * Gets the path portion of a 'this' binding.
 *
 * There are two supported binding types: 'this-path' and 'data-path' where the
 * first segment is 'this'.
 *
 * This function will return the path to look up on the 'this' data object (e.g.
 * the row for a table). e.g.:
 *
 *     { bindingType: 'this-path', path: 'foo.bar'} -> 'foo.bar'
 *     { bindingType: 'data-path', path: 'this.foo.bar'} -> 'foo.bar'
 *
 * Eventually the former will be dropped, but for now this should smooth over
 * the transition by allowing both.
 */
export function getThisPath(x: DataPathBinding): string {
  if (x.bindingType === 'this-path') {
    return x.path;
  } else {
    const [firstSeg, restOfPath] = splitPath(x.path);
    return firstSeg === 'this' ? restOfPath : x.path;
  }
}


export function isThisPathBinding(x: ParameterBinding): x is DataPathBinding {
  if (!x || typeof x !== 'object') {
    return false;
  } else if (x.bindingType === 'this-path' || (x.bindingType === 'data-path' && splitPath(x.path)[0] === 'this')) {
    return true;
  } else {
    return false;
  }
}



/**
 * We want to get rid of the 'this-path' style of binding, so this will
 * transform the binding to the data-path 'this.' style.
 *
 * We should remove this once configs are migrated to the new style.
 */
export function migrateBinding(binding: ParameterBinding): ParameterBinding {
  if (isThisPathBinding(binding) && binding.bindingType === 'this-path') {
    console.warn(`Using deprecated 'this-path' binding. Please update this config!`, binding);
    return {bindingType: 'data-path', path: `this.${binding.path}` };
  } else {
    return binding;
  }
}


export function describeBinding(binding: ParameterBinding): string {
  if (!binding) {
    return null;
  } else if (isPathBinding(binding)) {
    return `Path: ${binding.path}`
  } else if (isFilterBinding(binding)) {
    return `Filter: ${binding.filterId}`;
  } else {
    return 'Data Binding';
  }
}
