
export function splitPath(path: string): [string, string] {
  const firstPathSep = path.indexOf('.');
  if (firstPathSep >= 0) {
    const fetcherId = path.slice(0, firstPathSep);
    const restOfPath = path.slice(firstPathSep + 1);
    return [fetcherId, restOfPath]
  } else {
    return [path, ''];
  }
}
