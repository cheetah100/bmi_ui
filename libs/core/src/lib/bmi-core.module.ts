import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MatDialogModule } from '@angular/material/dialog';

import { SharedUiModule } from '@bmi/ui';

import { BmiCoreWidgetModule } from './bmi-core-widget.module';
import { FilterControlsModule } from './filter-controls/filter-controls.module';
import { BmiCoreComponentsModule } from './components/bmi-core-components.module';
import { BmiFlairRegistryModule } from './flair/bmi-flair-registry.module';
import { BmiCoreChartsModule } from './charts/bmi-core-charts.module';
import { FormatterRegistryModule } from './formatters/formatter-registry.module';

import { PageComponent } from './page/page.component';
import { AppHeaderComponent } from './app-layout/header/app-header.component';
import { AppSidebarComponent } from './app-layout/sidebar/app-sidebar.component';
import { ModalPageComponent } from './page/modal-page.component';
import { BmiFormsModule } from './forms/bmi-forms.module';

import { DataSourceRegistryModule } from './data-sources/data-source-registry.module';
import { EventActionsRegistry } from './event-actions/event-actions.registry';

import { BmiRouteHandler } from './navigation/bmi-route-handler';
import { RoutesService } from './services/routes.service';

@NgModule({
  imports: [
    CommonModule,
    FilterControlsModule,
    SharedUiModule,
    RouterModule,
    BmiCoreComponentsModule,
    BmiCoreWidgetModule,
    BmiCoreChartsModule,
    MatDialogModule,
    BmiFormsModule,
    BmiFlairRegistryModule,
    FormatterRegistryModule,
    DataSourceRegistryModule,
    EventActionsRegistry,
  ],

  entryComponents: [
    ModalPageComponent,
  ],

  declarations: [
    AppHeaderComponent,
    AppSidebarComponent,
    PageComponent,
    ModalPageComponent,
  ],

  exports: [
    AppHeaderComponent,
    AppSidebarComponent,
    PageComponent,
    ModalPageComponent,
    BmiCoreComponentsModule,
    BmiCoreWidgetModule,
    BmiFormsModule,
  ],

  providers: [
    {provide: BmiRouteHandler, useExisting: RoutesService}
  ],

})
export class BmiCoreModule { }
