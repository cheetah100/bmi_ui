import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { MenuItem } from '../../navigation/menu-item';
import { Observable, Subscription } from 'rxjs';


@Component({
  selector: 'bmi-app-sidebar',
  templateUrl: './app-sidebar.component.html',
  styleUrls: ['./app-sidebar.component.scss'],
})
export class AppSidebarComponent implements OnInit, OnDestroy {

  @Input() menuContent: MenuItem[] = [];
  @Input() contextTabs: MenuItem[] = null;
  @Input() closeEvents: Observable<unknown>;
  @Input() cssClass = 'sidebar--default';

  menuOpen = false;
  private openDrawers = new Map<number, boolean>();
  private subscription = new Subscription();

  ngOnInit() {
    if (this.closeEvents) {
      this.subscription.add(
        this.closeEvents.subscribe(() => this.closeDrawers())
      );
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  openDrawer(event: MouseEvent, i: number): void {
    event.stopPropagation();
    this.openDrawers.set(i, true);
  }

  closeDrawers(): void {
    this.openDrawers = new Map<number, boolean>();
  }

  drawerIsOpen(i: number): boolean {
    return this.openDrawers.get(i);
  }

}
