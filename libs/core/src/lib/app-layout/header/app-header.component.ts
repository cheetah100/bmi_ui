import { Component, Input, ViewChild, ContentChild, TemplateRef } from '@angular/core';
import { CurrentUserService } from '../../services/current-user.service';
import { SsoLogoutService } from '../../services/sso-logout.service';

@Component({
  selector: 'bmi-app-header',
  templateUrl: './app-header.component.html',
  styles: [`
::ng-deep body.gra .bmi-app-header {
  display: block;
  height: auto;
}
.bmi-app-header .header-panels {
  padding: 0 var(--gra-spacing);
}
.bmi-app-header .header-panel {
  min-height: 48px;
}
.confidential-banner {
  background-color: var(--gra-color--gray-200);
  height: 15px;
  background-size: 140px;
  background-repeat: repeat-x;
  background-position: var(--gra-spacing) 50%;
  border-bottom: 1px solid var(--gra-color--gray-400);
  width: 100vw;
}
  `]
})
export class AppHeaderComponent {

  @ContentChild('rightPanel') userRightPanel: TemplateRef<any>;
  @ViewChild('defaultRightPanel', { static: true }) defaultRightPanelTemplate: TemplateRef<any>;

  @Input() headerTitle = '';
  @Input() contextTabs = null;

  userName = this.currentUserService.fullName;

  constructor(
    private currentUserService: CurrentUserService,
    private ssoLogoutService: SsoLogoutService
  ) { }

  ssoLogout() {
    this.ssoLogoutService.ssoLogout();
  }

}
