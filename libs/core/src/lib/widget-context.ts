import { isDevMode } from '@angular/core';
import { BehaviorSubject, Observable, Subject, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { cloneAndUpdate } from './util/clone-and-update';

import { EditorContext } from './editor/editor-context';
import { WidgetConfig, WidgetSelection } from './widget-config';
import { WidgetDefinition } from './widget-definition';

import { deepFreezeObject } from './util/deep-freeze-object';


function checkForModificationsInDevMode<T extends object>(value: T): T {
  if (isDevMode()) {
    return deepFreezeObject(value, 'Widget Config Mutated', 'config');
  } else {
    return value;
  }
}


export type WidgetMode = 'view' | 'edit';


/**
 * A service for communications between a widget host and the widget instance.
 */
export class WidgetContext<T extends WidgetConfig = WidgetConfig> {

  private _widgetConfig = new BehaviorSubject<T>(null);
  private _widgetSelection = new BehaviorSubject<WidgetSelection>(null);
  private _mode = new BehaviorSubject<WidgetMode>(undefined);

  private _updatedWidgetConfigSubject = new Subject<WidgetSelection>();

  readonly isViewMode = this.mode.pipe(map(m => m === 'view'));
  readonly isEditMode = this.mode.pipe(map(m => m === 'edit'));
  public readonly widgetConfig = this._widgetConfig.asObservable();
  public readonly widgetSelection = this._widgetSelection.asObservable();

  constructor(
    public readonly widgetDefinition: WidgetDefinition,
    widgetConfig: Observable<T>,
    widgetSelection: Observable<WidgetSelection>,
    public readonly mode: Observable<WidgetMode>,
    public readonly editorContext?: EditorContext,
  ) {
    widgetConfig.subscribe(this._widgetConfig);
    widgetSelection.subscribe(this._widgetSelection);
    mode.subscribe(this._mode);
  }

  get widgetConfigSnapshot(): T {
    return this._widgetConfig.value;
  }

  get modeSnapshot() {
    return this._mode.value;
  }

  get isInsideEditor() {
    return !!this.editorContext;
  }

  get isViewModeNow() {
    return !this.isEditModeNow;
  }

  get isEditModeNow() {
    return this.modeSnapshot === 'edit';
  }

  /**
   * Watches for requests to update the widget config.
   *
   * This is used by the WidgetHost to propagate
   */
  get onWidgetConfigUpdateRequested(): Observable<WidgetSelection> {
    return this._updatedWidgetConfigSubject.asObservable();
  }

  /**
   * Requests a change to the  widget config.
   *
   * There are two ways this method can be used:
   *
   *  1. Passing in a config object
   *  2. Passing a callback which can mutate a deep-cloned config
   *
   * This is an action that flows up the view hierarchy. As widgets can be
   * nested inside other widgets, the widget doesn't necessarily 'own' its
   * config. Changes have to flow up the hierarchy, merging with the parent
   * configs.
   */
  updateWidgetConfig(callback: (config: T) => void) {
    if (!this._updatedWidgetConfigSubject.closed) {
      this._updatedWidgetConfigSubject.next(cloneAndUpdate(this.widgetConfigSnapshot, callback));
    }
  }

  /**
   * Requests to replace the widgets config.
   *
   * This is the "advanced mode" of the updateWidgetConfig function. Rather than
   * giving you an object to mutate, you're expected to return a new object
   * without mutating the existing state.
   *
   * You must treat the config as immutable
   */
  setWidgetConfig(callback: (config: T) => T | WidgetSelection) {
    const originalConfig = checkForModificationsInDevMode(this.widgetConfigSnapshot);
    const updated = callback(originalConfig);

    this._updatedWidgetConfigSubject.next(updated);
  }

  /**
   * Destroy this widget context, closing out any observable streams.
   */
  destroy() {
    this._widgetConfig.complete();
    this._updatedWidgetConfigSubject.complete();
  }
}



