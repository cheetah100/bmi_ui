import { Component, Input, Type, Injector } from '@angular/core';

import { ComponentPortal } from '@angular/cdk/portal';

import { PageEditorContext } from '../page-editor/page-editor-context';
import { EditableAspect } from '../editor/editable-aspect';

import { GenericEditWidgetSidebarComponent } from './generic-edit-widget-sidebar.component';
import { PagePropertiesSidebarComponent } from '../page-properties-sidebar/page-properties-sidebar.component';
import { GridLayoutSectionSidebarComponent } from '../widgets/grid-layout/grid-layout-section-sidebar/grid-layout-section-sidebar.component';
import { GridLayoutCellSidebarComponent } from '../widgets/grid-layout/grid-layout-cell-sidebar/grid-layout-cell-sidebar.component';



@Component({
  selector: 'bmi-editor-sidebar',
  templateUrl: './editor-sidebar.component.html',
  entryComponents: [
    GridLayoutSectionSidebarComponent,
    GridLayoutCellSidebarComponent,
    PagePropertiesSidebarComponent,
    GenericEditWidgetSidebarComponent
  ]
})
export class EditorSidebarComponent {
  @Input() aspect: EditableAspect;

  private sidebarComponents: {[sidebarType: string]: Type<any>} = {
    'grid-layout-section': GridLayoutSectionSidebarComponent,
    'grid-layout-cell': GridLayoutCellSidebarComponent,
    'page-properties': PagePropertiesSidebarComponent,
    'generic-edit-widget': GenericEditWidgetSidebarComponent
  };

  sidebarPortal: ComponentPortal<any> = null;

  constructor (
    private editorContext: PageEditorContext,
    private injector: Injector
  ) {}

  ngOnInit() {
    this.editorContext.selected.subscribe(aspect => {
      this.sidebarPortal = this.isSupportedSidebar(aspect) ? this.makeSidebarPortal(aspect) : null;
    });
  }

  private isSupportedSidebar(aspect: EditableAspect): boolean {
    return !!aspect && !!this.sidebarComponents[aspect.tempEditorTypeId];
  }

  private makeSidebarPortal(aspect: EditableAspect) {
    const componentType = this.sidebarComponents[aspect.tempEditorTypeId];
    const sidebarInjector = Injector.create({
      name: 'Page Editor Sidebar Injector',
      parent: aspect.injector || this.editorContext.pageInjector || this.injector,
      providers: [
        { provide: EditableAspect, useValue: aspect }
      ]
    });

    return new ComponentPortal(componentType, undefined, sidebarInjector);
  }
}
