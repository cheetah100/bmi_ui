import { Component, Input, Inject } from '@angular/core';

import { BehaviorSubject, of, Subscription } from 'rxjs';
import { filter, switchMap } from 'rxjs/operators';

import { WidgetSelection } from '../widget-config';
import { EditableAspect } from '../editor/editable-aspect';
import { EditWidgetAspect } from '../widget-host/edit-widget-aspect';


@Component({
  selector: 'bmi-core-sidebar-generic-edit-widget',
  template: `
    <h3>Widget Editor</h3>
    <bmi-widget-selector
      [widgetSelection]="widgetConfig | async"
      (updateWidgetSelection)="updateWidgetConfig($event)">
    </bmi-widget-selector>
  `,
  styleUrls: ['./editor-sidebar.scss']
})
export class GenericEditWidgetSidebarComponent {
  readonly widgetConfig = this.aspect.widgetSelection;

  constructor(
    @Inject(EditableAspect) private aspect: EditWidgetAspect
  ) { }

  updateWidgetConfig(config: WidgetSelection) {
    this.aspect.setWidgetSelection(config);
  }
}
