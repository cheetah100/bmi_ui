import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PortalModule } from '@angular/cdk/portal';

import { SharedUiModule } from '@bmi/ui';

import { BmiCoreWidgetModule } from './bmi-core-widget.module';
import { PageComponent } from './page/page.component';

import { BmiCoreComponentsModule } from './components/bmi-core-components.module';

import { WidgetConfigEditorComponent } from './widget-config-editor/widget-config-editor.component';
import { WidgetSelectorComponent } from './widget-selector/widget-selector.component';

import { FlairEditorListComponent } from './flair-editor-list/flair-editor-list.component';
import { FlairsDynamicEditorDirective } from './flair-editor-list/flair-dynamic-editor-defs.directive';


@NgModule({
  imports: [
    CommonModule,
    SharedUiModule,
    BmiCoreComponentsModule,
    BmiCoreWidgetModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    PortalModule,
  ],

  declarations: [
    WidgetConfigEditorComponent,
    WidgetSelectorComponent,
    FlairEditorListComponent,
    FlairsDynamicEditorDirective,
  ],

  exports: [
    WidgetSelectorComponent,
    WidgetConfigEditorComponent,
    FlairEditorListComponent,
  ],

})
export class BmiWidgetEditorCommonModule { }
