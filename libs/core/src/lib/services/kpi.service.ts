import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { CardCacheService, CardData } from '@bmi/gravity-services';
import { COLORS } from '@bmi/ui';
import { ObjectMap } from '@bmi/utils';

import { AxisRange } from '../charts/basic-chart-config';
import { ColorZone, makeRedYellowGreenColorZones } from '../util/color-zone';
import { getNumberFormatFunction } from '../util/format-number';


const KPI_STATUS_COLORS = {
  red: COLORS.statusRed.cssColor,
  yellow: COLORS.statusYellow.cssColor,
  green: COLORS.statusGreen.cssColor
};


export interface KpiDefaults {
  axisRange?: AxisRange;
  unit?: string;
  format?: (value: number) => string;
}


const KPI_DEFAULT_SETTINGS: ObjectMap<KpiDefaults> = {
  'decimal': {
    axisRange: {softMin: 0, softMax: 50},
    format: getNumberFormatFunction('default')
  },

  'percentage': {
    axisRange: {softMin: 0, softMax: 100, endOnTick: false},
    unit: '%',
    format: getNumberFormatFunction('percentage-0-to-100')
  }
}

const UNKNOWN_KPI_SETTINGS: KpiDefaults = {};


@Injectable({providedIn: 'root'})
export class KpiService {
  constructor(
    private cardCache: CardCacheService,
  ) {}

  get(id: string): Observable<Kpi> {
    return this.cardCache.get('kpi', id).pipe(
      map(card => new Kpi(card))
    );
  }
}


export interface KpiFields {
  name?: string;
  description?: string;
  is_manual?: boolean;
  is_standard?: boolean;
  metric_category?: string;
  is_visible?: boolean;
  text_format?: string;

  has_status_levels?: boolean;
  status_level_red?: number;
  status_level_yellow?: number;

  pocs?: any;
  pocs_id?: any;
  source_of_record_links?: any;
}


export class Kpi {
  constructor(readonly card: CardData<KpiFields>) {}

  get id() { return this.card.id; }
  get name() { return this.card.fields.name; }
  get title() { return this.card.title || this.name; }
  get description() { return this.card.fields.description; }
  get category() { return this.card.fields.metric_category; }
  get settings() {
    return KPI_DEFAULT_SETTINGS[this.card.fields.text_format] || UNKNOWN_KPI_SETTINGS;
  }

  get hasStatusLevels() {
    const redThreshold = this.card.fields.status_level_red;
    const yellowThreshold = this.card.fields.status_level_yellow;
    return !!this.card.fields.has_status_levels && (Number.isFinite(redThreshold) || Number.isFinite(yellowThreshold));
  }

  getStatusColorZones(): ColorZone[] {
    if (this.hasStatusLevels) {
      return makeRedYellowGreenColorZones(
        this.card.fields.status_level_red,
        this.card.fields.status_level_yellow,
        KPI_STATUS_COLORS
      );
    } else {
      return undefined;
    }
  }

  getSuggestedAxisRange(): AxisRange {
    return this.settings.axisRange || undefined;
  }
}
