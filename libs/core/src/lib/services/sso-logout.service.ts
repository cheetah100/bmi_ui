import { Inject, Injectable, isDevMode, Optional } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { BMI_APP_INIT_PARAMETERS, BmiAppInitParameters } from '@bmi/gravity-services';


@Injectable({providedIn: 'root'})
export class SsoLogoutService {
  private logoutUrl;

  constructor(
      @Inject(DOCUMENT) private document: Document,
      @Optional() @Inject(BMI_APP_INIT_PARAMETERS) private bmiAppInitParameters: BmiAppInitParameters) {
        // TODO Please specfiy
        this.logoutUrl = 'LOGOUT_URL'
      }

  ssoLogout() {
    if (isDevMode()) {
      alert(`Would go to SSO Logout URL: ${this.logoutUrl}`);
    }
    else {
      this.document.location.href = `${this.logoutUrl}`;
    }
  }
}
