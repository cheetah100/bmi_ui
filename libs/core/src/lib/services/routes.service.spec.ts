import { RoutesService, BmiRouteMetadata } from './routes.service';
import { of, Observable } from 'rxjs';
import { ModuleService } from './module.service';
import { fakeAsync, tick } from '@angular/core/testing';
import { BmiRoute } from '../navigation/bmi-route';

const moduleId = 'shtmgnt';
const mockModuleService = {
  getModule: (moduleId: string): Observable<any> => of({
    pageList: of([
      { title: 'Page A', applicationId: moduleId, id: 'pga' },
      { title: 'Page C', applicationId: moduleId, id: 'pgc' },
      { title: 'Page B', applicationId: moduleId, id: 'pgb' },
    ]),
  }),
} as unknown as ModuleService;


describe('RoutesService', () => {

  let routesService: RoutesService;

  beforeEach(() => {
    routesService = new RoutesService(mockModuleService, null, null);
  });

  describe('getRouteOptions()', () => {
    it('should give a list of correct sorted route options for the given module', fakeAsync(() => {
      let routeOptions: BmiRouteMetadata[];
      routesService.getRouteOptions(moduleId).subscribe(options => routeOptions = options);
      tick();
      expect(routeOptions.length).toBe(3);
      expect(routeOptions.map((option: BmiRouteMetadata) => option.title)).toEqual(['Page A', 'Page B', 'Page C']);
    }));
  });

  describe('getRouterLink()', () => {
    it('should return a route item consumable by routerLink directive for different route types', () => {
      expect(routesService.getRouterLink('test-route', moduleId)).toEqual([`/${moduleId}/test-route`]);
      expect(routesService.getRouterLink('test-route')).toEqual(['/test-route']);
      expect(routesService.getRouterLink(undefined, moduleId)).toEqual([`/${moduleId}/`]);
      expect(routesService.getRouterLink([moduleId, 'test-page'])).toEqual([moduleId, 'test-page']);
      expect(routesService.getRouterLink(
        { type: 'page', pageId: 'test-page', moduleId },
        moduleId
      )).toEqual(['/', moduleId, 'pages', 'test-page', {}]);
      expect(routesService.getRouterLink(
        { type: 'builtin', routeId: 'test-builtin', moduleId },
        moduleId
      )).toEqual([moduleId, 'test-builtin']);
    });
    it('should not add module segment to absolute route strings', () => {
      expect(routesService.getRouterLink('/abs-route', moduleId)).toEqual(['/abs-route']);
    });
    it('should throw an error if the route given is unusable', () => {
      expect(() => {
        routesService.getRouterLink({ type: null } as any, moduleId);
      }).toThrow();
    });
  });

});
