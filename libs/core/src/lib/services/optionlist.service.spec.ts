const some = makeBoard({
  config: new BoardConfigBuilder('some')
    .name('Some')
    .addField('name')
    .addField('value', (f) => f.type('NUMBER'))
    .addField('active', (f) => f.type('BOOLEAN')),
  cards: [
    {
      id: 'C1',
      title: 'Card 1',
      fields: { name: 'Berk', value: 9001, active: true },
    },
    {
      id: 'C2',
      title: 'Card 2',
      fields: { name: 'Drutt', value: 1337, active: true },
    },
    {
      id: 'C3',
      title: 'Card 3',
      fields: { name: 'Boni', value: 0, active: true },
    },
    {
      id: 'C4',
      title: 'Card 4',
      fields: { name: 'Bubo', value: -1, active: false },
    },
    {
      id: 'C5',
      title: 'Card 5',
      fields: { name: 'Splund', value: Math.PI, active: true },
    },
  ],
});
const others = makeBoard({
  config: new BoardConfigBuilder('others').name('Others').addField('name'),
  cards: [
    { id: 'C1', title: 'Card 1', fields: { name: 'Worm' } },
    { id: 'C2', title: 'Card 2', fields: { name: 'Rogg' } },
  ],
});
const kpi = makeBoard({
  config: new BoardConfigBuilder('kpi').name('KPI')
  .addField('name')
  .addField('is_standard')
  .addField('is_visible')
  .addField('metric_category_name'),
  cards: [
    { id: 'C1', title: 'Card 1', fields: {
      is_standard: true, is_visible: true, metric_category_name: 'enthusiasm'}
    },
    { id: 'C2', title: 'Card 2', fields: {
      is_standard: true, is_visible: true, metric_category_name: 'mindset'}
    },
  ]
})
const quarterConfig = {
  ...QUARTER_BOARD_CONFIG,
  id: 'quarter',
  name: 'Quarter',
};
const finalQuarter = {
  id: 'Q1FY20',
  fields: {
    name: 'Q1FY20',
    start_date: 1564210800001,
    end_date: 3000000000000,
    title: 'Q1FY20',
  },
  title: 'Q1FY20',
  phase: 'current',
};
const quarterCards = [...QUARTER_CARDS, finalQuarter].map((q) => ({
  ...q,
  name: q.id,
  fiscalYear: q.id.substr(2),
}));
const quarter = makeBoard({
  config: quarterConfig,
  cards: quarterCards,
});
const fiscal_months = makeBoard({
  config: FISCAL_MONTHS_BOARD_CONFIG,
  cards: FISCAL_MONTH_CARDS,
});
const boards = { some, others, quarter, fiscal_months, kpi };

const mockService = {
  // getTitles: (id: string) => null, //board
  // getConfig: (id: string) => null,
  search: (params) => of(boards[params.boardId].cards),
  getAllQuarters: () => of(quarterCards),
  getCurrentQuarter: () => of(finalQuarter),
  watchAllConfigs: () => of([some.config, others.config, quarter.config]),
  watchBoardConfig: (id) => of(boards[id].config),
  // list: () => null, //credential
  list: (id: string) =>
    of([
      {
        id: 'resource_1',
        name: 'A resource',
        type: 'script',
        boardId: id,
      },
    ]),
};

import { HttpClient } from '@angular/common/http';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import {
  BoardConfigBuilder,
  CardCacheService,
  FISCAL_MONTHS_BOARD_CONFIG,
  FISCAL_MONTH_CARDS,
  GravityConfigService,
  GravityResourceService,
  gravityTestbed2,
  GravityTimelineService,
  makeBoard,
  QUARTER_BOARD_CONFIG,
  QUARTER_CARDS,
} from '@bmi/gravity-services';
import { of } from 'rxjs';
import { OptionlistService } from './optionlist.service';

/**
 * Somewhere between useful tests and blindly chasing coverage
 */

describe('OptionlistService', () => {
  let optionlistService: OptionlistService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        gravityTestbed2({
          boards: [some, others, quarter, fiscal_months, kpi],
        }),
        { provide: GravityTimelineService, useValue: mockService },
        { provide: HttpClient, useValue: mockService },
        { provide: GravityResourceService, useValue: mockService },
        { provide: GravityConfigService, useValue: mockService },
        { provide: CardCacheService, useValue: mockService }
      ],
    });
    optionlistService = TestBed.inject(OptionlistService);
  });

  describe('getAllBoards', () => {
    it('should provide an observable of registered board configs', fakeAsync(() => {
      let boards;
      optionlistService.getAllBoards().subscribe((res) => (boards = res));
      tick();
      expect(boards.map((b) => b.id)).toEqual(['others', 'quarter', 'some']);
    }));
  });

  describe('getOptions', () => {
    it('should provide an observable of sorted Options for all cards on a board', fakeAsync(() => {
      let options;

      optionlistService.getOptions('some').subscribe((res) => (options = res));
      tick();
      expect(options.length).toBe(5);

      optionlistService.getOptions(null).subscribe((res) => (options = res));
      tick();
      expect(options).toEqual([]);

      optionlistService
        .getOptions('gravity-system-RESOURCE', 'others')
        .subscribe((res) => (options = res));
      tick();
      expect(options).toEqual([{ text: 'A resource', value: 'resource_1' }]);
    }));

    it('should provide sorted options for system types', fakeAsync(() => {
      let options;

      optionlistService
        .getOptions('gravity-system-BOARD')
        .subscribe((res) => (options = res));
      tick();
      expect(options).toEqual([
        { value: 'others', text: 'Others' },
        { value: 'quarter', text: 'Quarter' },
        { value: 'some', text: 'Some' },
      ]);

      optionlistService
        .getOptions('gravity-system-PHASE', 'quarter')
        .subscribe((res) => (options = res));
      tick();
      expect(options).toEqual([
        { value: 'archive', text: 'archive' },
        { value: 'current', text: 'current' },
      ]);

      optionlistService
        .getOptions('gravity-system-RANDOM')
        .subscribe((res) => (options = res));
      tick();
      expect(options).toEqual([]);
    }));
  });

  describe('getOptionGroups', () => {
    it('should provide an observable of OptionGroups for board cards', fakeAsync(() => {
      let options;

      optionlistService
        .getOptionGroups('others')
        .subscribe((res) => (options = res[0].options));
      tick();
      expect(options).toEqual([
        { value: 'C1', text: 'C1' },
        { value: 'C2', text: 'C2' },
      ]);

      optionlistService
        .getOptionGroups('quarter')
        .subscribe((res) => (options = res[0].options));
      tick();
      expect(options.length).toBe(9);

      optionlistService
        .getOptionGroups('fiscal_months')
        .subscribe(res => options = res[0].options);
      tick();
      expect(options.length).toBe(12);

      let optionGroups;
      optionlistService.getOptionGroups('kpi').subscribe(res => optionGroups = res);
      tick();
      expect(optionGroups.length).toBe(2);
      expect(optionGroups[0].title).toBe('enthusiasm');
      expect(optionGroups[0].options.length).toBe(1);
      expect(optionGroups[1].title).toBe('mindset');
      expect(optionGroups[1].options.length).toBe(1);

    }));
  });
});
