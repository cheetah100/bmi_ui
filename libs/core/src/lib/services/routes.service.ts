import { Injectable, InjectionToken, Optional, Inject } from '@angular/core';
import { Router } from '@angular/router';

import { Observable, defer, from, of } from 'rxjs';
import { take, map, switchMap } from 'rxjs/operators';

import { ObjectMap, combineLatestObject } from '@bmi/utils';

import { PageInfo, PageFilterInfo } from '../page-config';
import { Binder } from '../page-data-context/binder';
import { Module, ModuleService } from './module.service';

import { BmiRouteHandler, BmiRouteOptions } from '../navigation/bmi-route-handler';
import { BmiBuiltinRoute, BmiPageRoute, BmiRoute, BmiResolvedRoute, BmiResolvedPageRoute, isPageRoute } from '../navigation/bmi-route';
import { resolveFilterBindings } from '../navigation/bmi-route-utils';


export interface BmiRouteMetadata {
  title: string;
  route: BmiRoute;
  availableFilters?: PageFilterInfo[]
}


export type PageRouteCommandGenerator = (pageRoute: BmiResolvedPageRoute) => any[];

export const BMI_PAGE_ROUTE_GENERATOR = new InjectionToken<PageRouteCommandGenerator>('Generator for BMI page links');


function defaultPageRouteGenerator(route: BmiResolvedPageRoute) {
  return [`/`, route.moduleId, 'pages', route.urlId || route.pageId, route.filters || {}];
}


@Injectable({ providedIn: 'root' })
export class RoutesService extends BmiRouteHandler {
  constructor(
    private moduleService: ModuleService,
    private router: Router,
    @Optional() @Inject(BMI_PAGE_ROUTE_GENERATOR) private customPageRouteFn: PageRouteCommandGenerator,
  ) {
    super();
  }

  private routeMethodsConfig: ObjectMap<{
    id: string,
    route: (moduleId: string) => string[],
    canAccess: (moduleId: string) => boolean,
  }> = {};


  getModuleRoutes(moduleId: string): Observable<BmiRouteMetadata[]> {
    return this.getAvailablePageRoutes(moduleId);
  }

  getRouteOptions(moduleId: string): Observable<BmiRouteMetadata[]> {
    return this.getModuleRoutes(moduleId);
  }

  getAvailablePageRoutes(moduleId: string): Observable<BmiRouteMetadata[]> {
    return this.moduleService.getModule(moduleId).pipe(
      switchMap((module: Module) => module.pageList),
      map((pages: PageInfo[]) => pages
        .filter((page: PageInfo) => page.applicationId === moduleId)
        .sort((a: PageInfo, b: PageInfo) => a.title > b.title ? 1 : -1)
        .map((page: PageInfo) => ({
          title: page.title,
          availableFilters: page.filters || [],
          route: {
            type: 'page' as 'page',
            moduleId: page.applicationId,
            pageId: page.id,
          }
        }))
      )
    );
  }

  navigate(route: BmiRoute, options: BmiRouteOptions): Observable<boolean> {
    return this.generateRouteCommands(route, options).pipe(
      take(1),
      switchMap(commands => this.router.navigate(commands)),
    );
  }

  generateRouteCommands(route: BmiRoute, options: BmiRouteOptions) {
    return this.resolveRoute(route, options).pipe(
      map(resolved => this.getRouterLink(resolved))
    );
  }

  resolveRoute(route: BmiRoute, options: BmiRouteOptions): Observable<BmiResolvedRoute> {
    if (!isPageRoute(route)) {
      return of(route);
    } else {
      return this.resolvePageRoute(route, options);
    }
  }

  private resolvePageRoute(route: BmiPageRoute, options: BmiRouteOptions): Observable<BmiResolvedPageRoute> {
    const moduleId = route.moduleId || options.moduleId;
    return combineLatestObject({
      pageInfo: this.moduleService.getPageInfo(moduleId, route.pageId),
      filters: resolveFilterBindings(route.filters, options.binder),
    }).pipe(
      map(({pageInfo, filters}) => ({
        ...route,
        filters,
        urlId: pageInfo ? pageInfo.urlIds[0] : undefined
      }))
    )
  }

  getRouterLink(route: BmiResolvedRoute = '', moduleId = ''): any[] {
    if (typeof route === 'string') {
      return this.getStringLinkCommands(route, moduleId);
    } else if (Array.isArray(route)) {
      return route;
    } else if (route && route.type) {
      if (route.type === 'page') {
        return this.generatePageCommands(route);
      } else if (route.type === 'builtin') {
        return this.getBuiltinRouterLink(route);
      } else {
        throw new Error('Unknown BmiRoute route type: ${route.type}');
      }
    } else {
      throw new Error(`Unknown BmiRoute object: ${JSON.stringify(route)}`);
    }
  }

  generatePageCommands(route: BmiResolvedPageRoute) {
    const fn = this.customPageRouteFn || defaultPageRouteGenerator;
    return fn(route);
  }

  getStringLinkCommands(route: string, moduleId?: string): any[] {
    if (route.startsWith('/')) {
      // Treat absolute URLs absolutely.
      return [route];
    } else if (moduleId && !route.startsWith(`${moduleId}/`) && route !== moduleId) {
      return [`/${moduleId}/${route}`];
    } else {
      return [`/${route}`];
    }
  }

  getBuiltinRouterLink(route: BmiBuiltinRoute): string[] {
    return [route.moduleId, ...route.routeId.split('/')].filter(Boolean);
  }

}
