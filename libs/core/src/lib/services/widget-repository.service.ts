
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { map, publishReplay, refCount } from 'rxjs/operators';

import { GravityUrlService, GravityWidgetService } from '@bmi/gravity-services';
import { ObservableCache } from '@bmi/utils';

import { WidgetConfig } from '../widget-config';

@Injectable({providedIn: 'root'})
export class WidgetRepositoryService {
  private readonly MAX_AGE = 30 * 1000;

  private widgetCache = new ObservableCache({
    factory: appId => this.widgetService.getAppWidgets(appId)
  });

  constructor(private widgetService: GravityWidgetService) {}

  getAllWidgets(appId: string) {
    const staleness = this.widgetCache.checkFreshness(appId);
    if (staleness === null || staleness >= this.MAX_AGE) {
      this.widgetCache.refresh(appId);
    }

    return this.widgetCache.watch(appId);
  }

  refreshWidgets(appId: string) {
    this.widgetCache.refresh(appId);
  }

  getWidget(appId: string, widgetId: string): Observable<WidgetConfig> {
    return this.getAllWidgets(appId).pipe(
      map(widgets => widgets.find(w => w.id === widgetId)),
      publishReplay(1),
      refCount()
    );
  }
}
