import { Injectable } from '@angular/core';
import { Observable, combineLatest, iif, of, defer } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import groupBy from 'lodash-es/groupBy';
import sortBy from 'lodash-es/sortBy';
import orderBy from 'lodash-es/orderBy';
import partition from 'lodash-es/partition';

import { Option, OptionGroup } from '@bmi/ui';
import {
  BoardService,
  CardCacheService,
  CardData,
  GravityTimelineService,
  Quarter,
  GravityConfigService,
  BoardConfig,
  CredentialService,
  GravityResourceService,
  BoardResourceSummary,
  CredentialSummary
} from '@bmi/gravity-services';
import { ObservableCache, ObjectMap } from '@bmi/utils';


interface CardFields {
  name: string;
  [x: string]: any;
}

@Injectable({ providedIn: 'root' })
export class OptionlistService {
  private _cache = new ObservableCache<ObjectMap<string>>({
    factory: board => this.boardService.getTitles(board)
  });

  private REFRESH_AGE = 30 * 60 * 60 * 1000;
  private DISPLAY_LIMIT = 500;
  private readonly SYSTEM_PREFIX = 'gravity-system-';

  constructor(
    private boardService: BoardService,
    private cardCacheService: CardCacheService,
    private timelineService: GravityTimelineService,
    private gravityConfigService: GravityConfigService,
    private credentialService: CredentialService,
    private resourceService: GravityResourceService
  ) {}

  get(boardId: string): Observable<ObjectMap<string>> {
    const cacheAge = this._cache.checkFreshness(boardId);
    if (cacheAge === null || cacheAge > this.REFRESH_AGE) {
      this._cache.refresh(boardId);
    }
    return this._cache.watch(boardId);
  }

  /**
   * Get a list of all boards
   * @returns list of boards ordered by name
   */
  getAllBoards(): Observable<BoardConfig[]> {
    return this.gravityConfigService.watchAllConfigs().pipe(
      map(boards => sortBy(boards, b => b.name)),
      shareReplay(1)
    );
  }

  getOptions(optionsId: string, context: string = null): Observable<Option[]> {
    if (!optionsId) {
      return of([]);
    }
    return iif(
      () => optionsId.startsWith(this.SYSTEM_PREFIX),
      defer(() => this.getSystemOptions(optionsId, context)),
      defer(() => this.getCardOptions(optionsId))
    );
  }

  getSystemOptions(optionsId: string, context: string): Observable<Option[]> {
    const type = optionsId.replace(this.SYSTEM_PREFIX, '');
    switch (type) {
      case 'CREDENTIAL':
        return this.credentialService
          .list()
          .pipe(
            map((credentials: CredentialSummary[]) =>
              this.getObjectOptions(credentials)
            )
          );
      case 'BOARD':
        return this.getAllBoards().pipe(
          map(boards => this.getObjectOptions(boards))
        );
      case 'PHASE':
        return this.boardService
          .getConfig(context)
          .pipe(
            map(config => this.getObjectOptions(Object.values(config.phases)))
          );
      case 'RESOURCE':
        return this.resourceService
          .list(context)
          .pipe(
            map((resources: BoardResourceSummary[]) =>
              this.getObjectOptions(resources)
            )
          );
      case 'MAP':
      case 'LIST':
      case 'FIELD':
        if (context.startsWith('gravity-system')) {
          return of([]);
        }
        return this.boardService.getConfig(context).pipe(
          map(config => {
            const options = Object.values(config.fields).map(field => ({
              text: field.label,
              value: field.id
            }));
            return this.sortOptions(options);
          })
        );
      default:
        console.warn(`Unsupported options requested: ${optionsId}`);
        return of([]);
    }
  }

  getCardOptions(boardId: string): Observable<Option[]> {
    return this.get(boardId).pipe(map(titles => this.createOptions(titles)));
  }

  getObjectOptions(object: { id: string; name: string }[]): Option[] {
    const options = object.map(item => ({
      text: item.name,
      value: item.id
    }));
    return this.sortOptions(options);
  }

  sortOptions(options: Option[]): Option[] {
    return sortBy(options, o => o.text);
  }

  createOptions(objectMap: ObjectMap<string>): Option[] {
    const options = Object.entries(objectMap).map(([key, value]) => ({
      value: key,
      text: value || key
    }));
    return this.sortOptions(options);
  }

  getOptionGroups(
    boardId: string,
    context: string = null
  ): Observable<OptionGroup[]> {
    switch (boardId) {
      case 'kpi':
        return this.getKpiGroups();

      case 'quarter':
        return this.groupQuarters();

      case 'service':
        return this.groupByCategoryName(
          'service',
          'service_options_grouped_by_category'
        );

      case 'fiscal_months':
        return this.groupByCategoryName(
          'fiscal_months',
          'months_with_year_category',
          'quarter_fiscalYear',
          'start_date'
        );

      default:
        return this.getDefaultOptionlistGroups(boardId, context);
    }
  }

  private getDefaultOptionlistGroups(
    boardId: string,
    context: string = null
  ): Observable<OptionGroup[]> {
    return this.getOptions(boardId, context).pipe(
      map(options => optionsToOptionGroups(options))
    );
  }

  private getKpiGroups(): Observable<OptionGroup[]> {
    interface KpiCardFields {
      name: string;
      is_standard: boolean;
      is_visible: boolean;
      metric_category_name: string;
    }

    return this.cardCacheService
      .search({
        boardId: 'kpi',
        viewId: 'kpi_options_with_metric_category'
      })
      .pipe(
        map((kpiCards: CardData<KpiCardFields>[]) =>
          sortBy(kpiCards, k => k.fields.name)
        ),
        map(kpiCards => {
          const mkKpiOption = (k: CardData<KpiCardFields>) =>
            <Option>{ value: k.id, text: k.fields.name };

          const visibleKpis = kpiCards.filter(k => k.fields.is_visible);

          const standardKpisWithCategories = visibleKpis.filter(
            k =>
              k.fields.is_standard &&
              k.fields.metric_category_name &&
              k.fields.is_visible
          );

          const nonStandardKpis = visibleKpis.filter(
            k => !k.fields.is_standard
          );

          const otherKpis = kpiCards.filter(
            k =>
              !standardKpisWithCategories.includes(k) &&
              !nonStandardKpis.includes(k)
          );
          const groupedKpis = groupBy(
            standardKpisWithCategories,
            k => k.fields.metric_category_name
          );

          const categoryGroups = Object.entries(groupedKpis).map(
            ([categoryId, cards]: [string, CardData<KpiCardFields>[]]) =>
              <OptionGroup>{
                id: categoryId,
                title: categoryId,
                options: cards.map(k => mkKpiOption(k))
              }
          );

          return [
            ...sortBy(categoryGroups, g => g.title),
            {
              title: 'Non-standard',
              collapsible: true,
              collapsedByDefault: true,
              options: nonStandardKpis.map(k => mkKpiOption(k))
            },
            {
              title: 'Other',
              collapsible: true,
              collapsedByDefault: true,
              options: otherKpis.map(k => mkKpiOption(k))
            }
          ].filter(g => g.options.length > 0);
        })
      );
  }

  private groupByCategoryName(
    boardId: string,
    viewId = 'all',
    categoryField = 'category_name',
    sortField: string = null
  ) {
    return this.cardCacheService
      .search({
        boardId,
        viewId
      })
      .pipe(
        map((cards: CardData<CardFields>[]) =>
          sortBy(cards, c => c.fields.name)
        ),
        map(allCards => {
          const [cardsWithCategory, otherCards] = partition(
            allCards,
            c => !!c.fields[categoryField]
          );
          const grouped = groupBy(cardsWithCategory, c =>
            String(c.fields[categoryField])
          );
          const categoryGroups = Object.entries(grouped).map(
            ([category, cards]: [string, CardData<CardFields>[]]) => {
              if (sortField) {
                cards = sortBy(cards, (c: CardData<CardFields>) => [
                  c.fields[sortField]
                ]);
              }
              return <OptionGroup>{
                id: category,
                title: category,
                options: cards.map(
                  c => <Option>{ value: c.id, text: c.fields.name }
                )
              };
            }
          );

          return [
            ...sortBy(categoryGroups, g => g.title),
            {
              title: 'Other',
              options: otherCards.map(
                c => <Option>{ value: c.id, text: c.fields.name }
              ),
              collapsible: true,
              collapsedByDefault: otherCards.length > this.DISPLAY_LIMIT
            }
          ].filter(g => g.options.length > 0);
        })
      );
  }

  private groupQuarters() {
    return combineLatest([
      this.timelineService.getAllQuarters(),
      this.timelineService.getCurrentQuarter()
    ]).pipe(
      map(([allQuarters, currentQuarter]) => {
        function isUsefulQuarter(quarter: Quarter) {
          if (!quarter || !currentQuarter) {
            return false;
          } else {
            const yearDifference =
              quarter.fiscalYear - currentQuarter.fiscalYear;
            return yearDifference >= -2 && yearDifference <= 1;
          }
        }

        const sortedQuarters = orderBy(
          allQuarters,
          [q => q.fiscalYear, q => q.fiscalQuarter],
          ['desc', 'asc']
        );
        const usefulQuarters = sortedQuarters.filter(q => isUsefulQuarter(q));

        const groupedQuarters = groupBy(usefulQuarters, q => q.fiscalYear);
        return [
          ...orderBy(Object.entries(groupedQuarters), t => t[0], 'desc').map(
            ([year, quarters]) =>
              <OptionGroup>{
                title: `FY ${year}`,
                options: quarters.map(
                  q => <Option>{ value: q.id, text: q.name }
                ),
                collapsible: true,
                collapsedByDefault:
                  quarters[0].fiscalYear !== currentQuarter.fiscalYear
              }
          ),
          {
            title: 'All Quarters',
            options: sortedQuarters.map(
              q => <Option>{ value: q.id, text: q.name }
            ),
            collapsible: true,
            collapsedByDefault: true
          }
        ];
      })
    );
  }
}

export function optionsToOptionGroups(
  options: Option[],
  displayLimit = 1000
): OptionGroup[] {
  const isHuge = options.length >= displayLimit;
  return [
    {
      title: isHuge ? 'All Items' : null,
      options: options,
      collapsible: isHuge,
      collapsedByDefault: isHuge
    }
  ];
}
