import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, of, interval } from 'rxjs';
import { map, switchMap, publishLast, publishReplay, refCount, tap, share } from 'rxjs/operators';

import { GravityApiApplication, GravityUrlService } from '@bmi/gravity-services';
import { ObjectMap, ObservableMap, ObservableMapOptions, CacheEntry } from '@bmi/utils';

import { PageConfig, PageInfo } from '../page-config';
import { PageRepositoryService, lookupPage } from './page-repository.service';
import { MenuItem } from '../navigation/menu-item';
import { BmiRoute } from '../navigation/bmi-route';

import keyBy from 'lodash-es/keyBy';


export interface ModuleMenuConfig {
  data: MenuItem[];
  defaultRoute: BmiRoute;
}


export interface ModuleExtraSettings {
  /**
   * An optional group name, used for organizing/grouping modules in a list
   */
  group?: string;

  /**
   * When sorting modules, the sort weight can be used as a factor to influence
   * where it falls in the list. Bigger numbers come last.
   */
  sortWeight?: number;

  /**
   * A list of 'favourite' boards. This can be used to populate a list of boards
   * when working within the context of a module.
   */
  favoriteBoards?: string[];
}


const DEFAULT_MENU_CONFIG: ModuleMenuConfig = { data: [], defaultRoute: null };


const DEFAULT_EXTRA_SETTINGS: ModuleExtraSettings = {
  group: null,
  favoriteBoards: [],
  sortWeight: 0
};


export interface ModuleSummary {
  id: string;
  name: string;
  description: string;
}


export class Module {

  constructor(
    public readonly moduleId: string,
    private moduleService: ModuleService,
  ) {}

  readonly config = this.moduleService.getModuleConfig(this.moduleId);
  readonly menuConfig = this.getSettings('menu', DEFAULT_MENU_CONFIG);
  readonly extraSettings = this.getSettings('extraSettings', DEFAULT_EXTRA_SETTINGS);
  readonly menuItems = this.menuConfig.pipe(map(menu => menu.data || []));
  readonly defaultRoute = this.menuConfig.pipe(map(menu => menu.defaultRoute));
  readonly pageList = this.moduleService.getPageList(this.moduleId);
  readonly name = this.config.pipe(map(c => c.name));

  getPage(pageId: string): Observable<PageConfig> {
    return this.moduleService.lookupPage(this.moduleId, pageId);
  }

  private getSettings<T>(key: string, defaults: T): Observable<T> {
    return this.config.pipe(
      map(config => {
        const settings = config.modules[key];
        return {
          ...defaults,
          ...(settings && settings.ui as T) || {}
        }
      }),
      publishReplay(1),
      refCount(),
    );
  }
}


@Injectable({providedIn: 'root'})
export class ModuleService {

  private defaultOptions = {
    maxAge: 30000,
    emitStaleValues: false
  };

  private moduleObjects = new Map<string, Module>();

  private moduleConfigs = new ObservableMap<string, GravityApiApplication>({
    source: moduleId => this.fetchModuleConfig(moduleId),
    ...this.defaultOptions
  });

  private pageLists = new ObservableMap<string, PageInfo[]>({
    source: moduleId => this.pageRepositoryService.listPages(moduleId),
    ...this.defaultOptions
  });

  private pages = new ObservableMap<string, ObjectMap<PageConfig>>({
    source: moduleId => this.pageRepositoryService.getAllPages(moduleId).pipe(
      map(pages => keyBy(pages, p => p.id))
    ),
    ...this.defaultOptions
  });

  private moduleSummary = new CacheEntry({
    source: () => this.fetchModuleSummaries(),
    maxAge: 30000
  })


  constructor(
    private pageRepositoryService: PageRepositoryService,
    private httpClient: HttpClient,
    private urlService: GravityUrlService
  ) {}

  getModule(moduleId: string): Observable<Module> {
    if (!this.moduleObjects.has(moduleId)) {
      this.moduleObjects.set(moduleId, new Module(moduleId, this));
    }
    return of(this.moduleObjects.get(moduleId));
  }

  private fetchModuleConfig(moduleId: string): Observable<GravityApiApplication> {
    const url = this.urlService.app(moduleId).baseUrl;
    return this.httpClient.get<GravityApiApplication>(url).pipe(
      publishLast(),
      refCount()
    );
  }

  private fetchModuleSummaries(): Observable<ModuleSummary[]> {
    const url = this.urlService.url('/app/descriptions');
    return this.httpClient.get<ObjectMap<ModuleSummary>>(url).pipe(
      map(result => Object.entries(result).map(([key, summary]) => ({...summary, id: key})))
    );
  }

  getModuleConfig(moduleId: string): Observable<GravityApiApplication> {
    return this.moduleConfigs.watch(moduleId);
  }

  getModuleSummaries() {
    return this.moduleSummary.watch();
  }


  getPageList(moduleId: string): Observable<PageInfo[]> {
    this.pageLists.refresh(moduleId);
    return this.pageLists.watch(moduleId);
  }

  getPageInfo(moduleId: string, pageId: string) {
    return this.getPageList(moduleId).pipe(
      map(pages => pages.find(p => p.id === pageId))
    );
  }

  private getPages(moduleId: string): Observable<ObjectMap<PageConfig>> {
    return this.pages.watch(moduleId);
  }

  private getPage(moduleId: string, pageId: string) {
    return this.getPages(moduleId).pipe(
      map(pages => pages[pageId])
    );
  }

  invalidateModuleConfig(moduleId: string) {
    this.moduleConfigs.invalidate(moduleId);
  }

  refresh(moduleId: string) {
    this.moduleConfigs.refresh(moduleId);
    this.pageLists.refresh(moduleId);
    this.pages.refresh(moduleId);
  }

  lookupPage(moduleId: string, pageId: string): Observable<PageConfig> {
    return this.getPageList(moduleId).pipe(
      switchMap(pageList => {
        const foundPageInfo = lookupPage(pageList, pageId);
        if (foundPageInfo) {
          return this.getPage(moduleId, foundPageInfo.id);
        } else {
          throw new Error('Page not found: module: ${moduleId}, page: ${pageId}');
        }
      })
    )
  }

  deleteModule(moduleId: string): Observable<void> {
    return this.httpClient.delete(this.urlService.url(`/app/${moduleId}`)).pipe(
      tap(() => {
        this.moduleObjects.delete(moduleId);
        // TODO: remove pages and stuff. ObservableMap needs this feature to
        // ensure that all existing CacheEntries are destroyed properly and the
        // streams complete.
      }),
      map(() => undefined)
    );
  }
}
