import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, of, throwError } from 'rxjs';
import { catchError, map, publishLast, refCount } from 'rxjs/operators';

import { PageConfig, PageInfo, isValidPageConfig } from '../page-config';
import { makePageInfo } from '../util/make-page-info';

import { ObjectMap, uniqueId } from '@bmi/utils';

import { GravityUrlService } from '@bmi/gravity-services';


/**
 * Try to find a page by ID or by URL id.
 *
 * This takes a list of page info, and will attempt to find a match first by
 * looking at the page's id, and if no match is found it will search for the
 * first page with a matching urlId
 */
export function lookupPage(pages: PageInfo[], id: string): PageInfo {
  return pages.find(p => p.id === id) || pages.find(p => p.urlIds.includes(id));
}


@Injectable({ providedIn: 'root' })
export class PageRepositoryService {
  constructor(
    private urlService: GravityUrlService,
    private httpClient: HttpClient
  ) { }

  private pageUrl(appId: string, pageId: string) {
    return this.urlService.app(appId).url(`visualisation/${pageId}`);
  }

  private pageCollectionUrl(appId: string) {
    return this.urlService.app(appId).url('visualisation');
  }

  getPage(appId: string, pageId: string): Observable<PageConfig> {
    return this.httpClient.get<PageConfig>(this.pageUrl(appId, pageId)).pipe(
      map(config => {
        if (!isValidPageConfig(config)) {
          throw new Error(`Invalid page config for '${pageId}' (app: ${appId})`);
        } else {
          return config;
        }
      }),
      publishLast(),
      refCount()
    );
  }

  getAllPages(appId: string): Observable<PageConfig[]> {
    return this.httpClient.get<ObjectMap<PageConfig>>(this.pageCollectionUrl(appId)).pipe(
      map(pages => Object.values(pages).filter(p => isValidPageConfig(p))),
      publishLast(),
      refCount()
    );
  }

  listPages(appId: string): Observable<PageInfo[]> {
    return this.getAllPages(appId).pipe(
      catchError(error => of(<PageConfig[]>[])),
      map(pages => pages.map(p => makePageInfo(p))),
      publishLast(),
      refCount()
    );
  }

  createPage(appId: string, pageConfig: PageConfig): Observable<PageConfig> {
    return this.savePage(appId, pageConfig, true);
  }

  savePage(appId: string, pageConfig: PageConfig, create = false): Observable<PageConfig> {
    if (!isValidPageConfig(pageConfig)) {
      return throwError(new Error('Page config is not valid'));
    } else {
      const updatedConfig: PageConfig = {
        ...pageConfig,
        applicationId: appId,
      };

      if (create) {
        updatedConfig.id = updatedConfig.id || uniqueId('p', 8);
        return this.httpClient.post<PageConfig>(this.pageCollectionUrl(appId), updatedConfig).pipe(
          publishLast(),
          refCount()
        );
      } else {
        return this.httpClient.put<PageConfig>(this.pageUrl(appId, updatedConfig.id), updatedConfig).pipe(
          publishLast(),
          refCount()
        );
      }
    }
  }

  deletePage(appId: string, pageId: string): Observable<unknown> {
    return this.httpClient.delete(this.pageUrl(appId, pageId));
  }

}
