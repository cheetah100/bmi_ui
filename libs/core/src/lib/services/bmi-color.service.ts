import { Injectable } from '@angular/core';
import { COLORS } from '@bmi/ui';
import { ObjectMap } from '@bmi/utils';
import flatten from 'lodash-es/flatten';


@Injectable({providedIn: 'root'})
export class BmiColorService {
  private metricColors: ObjectMap<string> = {
    'red': COLORS.statusRed.cssColor,
    'yellow': COLORS.statusYellow.cssColor,
    'green': COLORS.statusGreen.cssColor
  };

  /**
   * Looks up a 'well known' color and gets a CSS-compatible color expression.
   *
   * The result of this function will probably be a #RRGGBB hex string, although
   * any representation supported by CSS should be supported (e.g. rgba(...)).
   *
   * The colour name is normalized by lowercasing it, but if it's not known then
   * the default color parameter will be returned.
   */
  getCssColor(colorName: string, defaultColor?: string): string {
    const normalizedName = colorName ? colorName.toLowerCase() : '';
    return this.metricColors[normalizedName] || defaultColor;
  }
}
