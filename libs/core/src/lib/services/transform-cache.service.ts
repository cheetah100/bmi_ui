import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GravityApiFilter, GravityTransformService, GravityApiCondition } from '@bmi/gravity-services';
import { ObjectMap, ObservableMap } from '@bmi/utils';

import stableStringify from 'fast-json-stable-stringify';


export interface TransformParameters {
  transformId: string;
  conditions?: GravityApiCondition[];
  params?: ObjectMap<any>;
}


@Injectable({providedIn: 'root'})
export class TransformCacheService {
  private transformCache = new ObservableMap<TransformParameters, ObjectMap<any>>({
    source: transformParameters => this.execTransform(transformParameters),
    keyFunction: transformParameters => stableStringify(transformParameters),
    maxAge: 5 * 60 * 1000,
  });

  constructor(
    private transformService: GravityTransformService
  ) {}

  get(transformParameters: TransformParameters) {
    return this.transformCache.watch(transformParameters);
  }

  private execTransform(parameters: TransformParameters): Observable<ObjectMap<any>> {
    if (parameters.params) {
      throw new Error('TODO: support for parameterized transform API is not implemented');
    } else {
      const filter = parameters.conditions ? makeFilterFromConditions(parameters.conditions) : undefined;
      return this.transformService.execute(parameters.transformId, filter);
    }
  }
}


function makeFilterFromConditions(conditions: GravityApiCondition[]): GravityApiFilter {
  const conditionObj: ObjectMap<GravityApiCondition> = {};
  conditions.forEach((cond, index) => conditionObj[`_${index}`] = cond);
  return {
    boardId: null,
    conditions: conditionObj
  };
}
