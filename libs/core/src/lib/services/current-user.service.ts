import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { distinctUntilChanged, map, shareReplay } from 'rxjs/operators';
import { GravityApiUser, GravityUrlService } from '@bmi/gravity-services';


@Injectable({providedIn: 'root'})
export class CurrentUserService {
  readonly currentUserInfo = this.http.get<GravityApiUser>(this.gravityUrlService.url('/user/current')).pipe(
    shareReplay(1)
  );

  readonly userid = this.currentUserInfo.pipe(
    map(user => user.id)
  );

  readonly fullName = this.currentUserInfo.pipe(
    map(user => user.fullName)
  );

  readonly teams = this.currentUserInfo.pipe(
    map(user => Object.keys(user.teams)),
    shareReplay(1),
  );

  constructor(
    private http: HttpClient,
    private gravityUrlService: GravityUrlService
  ) {}
}
