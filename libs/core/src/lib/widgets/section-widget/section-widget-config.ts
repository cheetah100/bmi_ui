import { WidgetConfig } from '../../widget-config';

export interface SectionWidgetFields {
  body: WidgetConfig;
}

export type SectionWidgetConfig = WidgetConfig<SectionWidgetFields>;
