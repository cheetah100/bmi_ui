import { Injectable } from '@angular/core';
import { WidgetConfig } from '../../widget-config';
import { WidgetConverter } from '../../widget-converter/widget-converter';
import { SectionWidgetConfig } from './section-widget-config';
import { uniqueId } from '@bmi/utils';

@Injectable({providedIn: 'root'})
export class WrapWithSectionConverter implements WidgetConverter<WidgetConfig, SectionWidgetConfig> {
  readonly name = 'Wrap with Section Widget';

  convert(widget: WidgetConfig): SectionWidgetConfig {
    return {
      widgetType: 'section',
      fields: {
        body: widget,
      },
      flairs: [
        {flairType: 'widget-title', title: 'New Section'}
      ]
    };
  }
}
