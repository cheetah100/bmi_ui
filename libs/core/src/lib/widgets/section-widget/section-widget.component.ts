import { Component } from '@angular/core';
import { map } from 'rxjs/operators';
import { SectionWidgetConfig } from './section-widget-config';
import { WidgetConfig } from '../../widget-config';
import { WidgetContext } from '../../widget-context';


@Component({
  template: `
    <bmi-core-widget-wrapper size="huge">
      <bmi-widget-host
          bmiWidgetWrapperContext
          [config]="body | async"
          [editable]="true"
          (updateWidgetConfig)="updateBodyConfig($event)">
      </bmi-widget-host>
    </bmi-core-widget-wrapper>
  `
})
export class SectionWidgetComponent {
  readonly body = this.widgetContext.widgetConfig.pipe(
    map(config => config.fields.body)
  );

  constructor (private widgetContext: WidgetContext<SectionWidgetConfig>) {}

  updateBodyConfig(bodyConfig: WidgetConfig) {
    this.widgetContext.updateWidgetConfig(c => c.fields.body = bodyConfig);
  }
}
