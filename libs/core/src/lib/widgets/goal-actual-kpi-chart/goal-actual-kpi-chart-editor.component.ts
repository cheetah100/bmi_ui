import { Component, OnInit } from '@angular/core';

import { TypedFormGroup, TypedFormControl } from '@bmi/ui';

import { WidgetConfig } from '../../widget-config';
import { WidgetContext } from '../../widget-context';
import { GoalActualMetricChartFields } from './goal-actual-chart-generator.service';

@Component({
  templateUrl: './goal-actual-kpi-chart-editor.component.html'
})
export class GoalActualKpiChartEditorComponent implements OnInit {
  form = new TypedFormGroup<GoalActualMetricChartFields>({
    actualDataSource: new TypedFormControl(null),
    goalDataSource: new TypedFormControl(null),
    kpi: new TypedFormControl(null),
  });

  constructor (
    private widgetContext: WidgetContext<WidgetConfig<GoalActualMetricChartFields>>
  ) {}

  ngOnInit() {
    this.widgetContext.widgetConfig.subscribe(config => this.form.patchValue(config.fields, {emitEvent: false}));
    this.form.valueChanges.subscribe(fields => this.widgetContext.updateWidgetConfig(c => c.fields = fields));
  }
}
