import { Injectable, Injector } from '@angular/core';
import { Observable, forkJoin, throwError, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { COLORS } from '@bmi/ui';

import { GeneratorService } from '../generator-widget/generator-service';
import { Kpi, KpiService } from '../../services/kpi.service';
import { WidgetConfig } from '../../widget-config';

import { BasicChartWidgetConfig, } from '../basic-chart-widget/basic-chart-widget-config';
import { BasicChartSettings } from '../../charts/basic-chart-config';
import { ParameterBinding } from '../../page-data-context/parameter-binding';
import { PageDataContext } from '../../page-data-context/page-data-context';

import { createPivotSeriesDef } from '../../data-series/create-pivot-source-def';

import { ColorZone } from '../../util/color-zone';


export interface GoalActualMetricChartFields {
  actualDataSource: string;
  goalDataSource: string;
  kpi: ParameterBinding;
  zones?: ColorZone[];
  defaultChartSettings?: Partial<BasicChartSettings>;
}


type GoalActualWidgetConfig = WidgetConfig<GoalActualMetricChartFields>;


@Injectable({providedIn: 'root'})
export class GoalActualChartGeneratorService
  implements GeneratorService<GoalActualWidgetConfig, BasicChartWidgetConfig> {

  constructor(
    private kpiService: KpiService
  ) {}

  public generate(config: GoalActualWidgetConfig, injector: Injector): Observable<BasicChartWidgetConfig> {
    const binder = injector.get(PageDataContext);
    return binder.bind(config.fields.kpi).pipe(
      switchMap(kpiId => typeof kpiId === 'string' ? this.kpiService.get(kpiId) : of(<Kpi>null)),
      map(kpi => {
        if (!kpi) {
          return null;
        }

        return <BasicChartWidgetConfig>{
          widgetType: 'basic-chart',
          fields: {
            chartSettings: {
              ...(config.fields.defaultChartSettings || {}),
              xAxisTitle: 'Quarter',
              yAxisTitle: kpi.title,
              polar: false,
              stacked: null,

              yAxisRange: kpi.getSuggestedAxisRange() || undefined,
              yAxisLabels: {
                formatFunction: kpi.settings.format || undefined,
              },

              seriesGroups: [
                {
                  dataSeries: createPivotSeriesDef(config.fields.actualDataSource),
                  selectedOnly: true,
                  seriesSettings: [
                    {
                      value: kpi.title,
                      color: null,
                      zones: config.fields.zones || kpi.getStatusColorZones(),
                      zoneAxis: 'y',
                      label: kpi.title,
                      type: 'column'
                    }
                  ]
                },

                config.fields.goalDataSource ? {
                  dataSeries: createPivotSeriesDef(config.fields.goalDataSource),
                  selectedOnly: true,
                  seriesSettings: [
                    {
                      value: kpi.title,
                      color: COLORS.dkGray2.cssColor,
                      connectNulls: true,
                      label: 'Goal',
                      type: 'line',
                    }
                  ]
                } : undefined
              ].filter(sg => !!sg),
            }
          }
        }
      })
    );
  }
}
