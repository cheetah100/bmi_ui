import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { SharedUiModule } from '@bmi/ui';
import { BmiChartsModule } from '@bmi/legacy-charts';

import { BmiCoreModule } from '../bmi-core.module';
import { FilterControlsModule } from '../filter-controls/filter-controls.module';
import { BmiCoreChartsModule } from '../charts/bmi-core-charts.module';
import { FormControlWidgetRegistryModule } from './form-controls/form-control-widget-registry.module';

import { GeneratorWidgetComponent } from './generator-widget/generator-widget.component';

import { GridLayoutWidgetComponent } from './grid-layout/grid-layout-widget.component';
import { MarkdownWidgetComponent } from './markdown/markdown-widget.component';
import { PageLayoutWidgetComponent } from './page-layout/page-layout-widget.component';
import { InfoboxWidgetComponent } from './infobox/infobox-widget.component';
import { InfoboxWidgetProgressBarComponent } from './infobox/infobox-progress-bar.component';
import { InfoboxWidgetGaugeComponent } from './infobox/infobox-gauge.component';

import { FilterControlsWidgetComponent } from './filter-controls/filter-controls-widget.component';

import { BmiChartsWidgetComponent } from './bmi-charts/bmi-charts-widget.component';
import { TableWidgetComponent } from './table/table-widget.component'

import { ActionWidgetComponent } from './action-widget/action-widget.component';
import { ActionItemComponent } from './action-widget/action-item.component';
import { ContentTabsWidgetComponent } from './content-tabs-widget/content-tabs-widget.component';
import { TimelineWidgetComponent } from './timeline-widget/timeline-widget.component';
import { TimelineDisplayComponent } from '../timeline-display/timeline-display.component';
import { MultiStatusWidgetComponent } from './multi-status-widget/multi-status-widget.component';
import { BasicChartWidgetComponent } from './basic-chart-widget/basic-chart-widget.component';
import { ReferenceWidgetComponent } from './reference-widget/reference-widget.component';
import { NavListWidgetComponent } from './nav-list-widget/nav-list-widget.component';

import { PlaceholderWidgetComponent } from '../widget-host/placeholder-widget.component';
import { PersonWidgetComponent } from './person-widget/person-widget.component';

import { ForeachWidgetComponent } from './foreach-widget/foreach-widget.component';

import { MetricChartGeneratorService } from './metric-chart-widget/metric-chart-generator.service';
import { GoalActualChartGeneratorService } from './goal-actual-kpi-chart/goal-actual-chart-generator.service';

import { SectionWidgetComponent } from './section-widget/section-widget.component';
import { DrawerWidgetComponent } from './drawer-widget/drawer-widget.component';

import { CardEditFormComponent } from './card-edit-form/card-edit-form.component';

import { GaugeChartToggleWidgetComponent } from './gauge-chart-toggle/gauge-chart-toggle-widget.component';
import { AlertWidgetComponent } from './alert-widget/alert-widget.component';
import { CardEditFormGeneratorService } from "./card-edit-form-generator/card-edit-form-generator.service";
import { defineGeneratorWidget, defineWidget } from '../util/widget-def-helpers';

import { ButtonToolbarWidgetComponent } from './button-toolbar/button-toolbar-widget.component';
import { ColumnTypeEditorRegistry } from './table/column-type-editor.registry';
import { PhaseEditFormComponent } from './phase-edit-form/phase-edit-form.component';
import { MetricGaugeWidgetComponent } from './metric-gauge-widget/metric-gauge-widget.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedUiModule,
    RouterModule,
    BmiCoreModule,
    FilterControlsModule,
    BmiChartsModule,
    BmiCoreChartsModule,
    FormControlWidgetRegistryModule,
    ColumnTypeEditorRegistry,
  ],

  declarations: [
    PlaceholderWidgetComponent,
    GridLayoutWidgetComponent,
    GeneratorWidgetComponent,
    MarkdownWidgetComponent,
    PageLayoutWidgetComponent,
    FilterControlsWidgetComponent,
    BmiChartsWidgetComponent,
    InfoboxWidgetComponent,
    InfoboxWidgetGaugeComponent,
    InfoboxWidgetProgressBarComponent,
    TableWidgetComponent,
    ActionWidgetComponent,
    ActionItemComponent,
    ContentTabsWidgetComponent,
    TimelineWidgetComponent,
    TimelineDisplayComponent,
    MultiStatusWidgetComponent,
    BasicChartWidgetComponent,
    BmiChartsWidgetComponent,
    ReferenceWidgetComponent,
    NavListWidgetComponent,
    PersonWidgetComponent,
    ForeachWidgetComponent,
    SectionWidgetComponent,
    DrawerWidgetComponent,
    CardEditFormComponent,
    PhaseEditFormComponent,
    GaugeChartToggleWidgetComponent,
    AlertWidgetComponent,
    ButtonToolbarWidgetComponent,
    MetricGaugeWidgetComponent,
  ],


  providers: [
    defineWidget({ widgetType: 'action-widget', component: ActionWidgetComponent }),
    defineWidget({ widgetType: 'alert', component: AlertWidgetComponent }),
    defineWidget({ widgetType: 'basic-chart', component: BasicChartWidgetComponent }),
    defineWidget({ widgetType: 'bmi_chart', component: BmiChartsWidgetComponent }),
    defineWidget({ widgetType: 'breadcrumbs', component: NavListWidgetComponent }),
    defineWidget({ widgetType: 'content-tabs', component: ContentTabsWidgetComponent }),
    defineWidget({ widgetType: 'filter-controls', component: FilterControlsWidgetComponent }),
    defineWidget({ widgetType: 'gantt', component: TimelineWidgetComponent }),
    defineWidget({ widgetType: 'grid-layout', component: GridLayoutWidgetComponent }),
    defineWidget({ widgetType: 'infobox', component: InfoboxWidgetComponent }),
    defineWidget({ widgetType: 'markdown', component: MarkdownWidgetComponent }),
    defineWidget({ widgetType: 'multi-status', component: MultiStatusWidgetComponent }),
    defineWidget({ widgetType: 'nav-list', component: NavListWidgetComponent }),
    defineWidget({ widgetType: 'page-layout', component: PageLayoutWidgetComponent }),
    defineWidget({ widgetType: 'person', component: PersonWidgetComponent }),
    defineWidget({ widgetType: 'reference', component: ReferenceWidgetComponent }),
    defineWidget({ widgetType: 'table-v2', component: TableWidgetComponent }),
    defineWidget({ widgetType: 'foreach', component: ForeachWidgetComponent }),
    defineWidget({ widgetType: 'section', component: SectionWidgetComponent, provideOwnWrapperContext: true }),
    defineWidget({ widgetType: 'drawer', component: DrawerWidgetComponent }),
    defineWidget({ widgetType: 'card-edit-form', component: CardEditFormComponent }),

    defineGeneratorWidget('metric-chart', MetricChartGeneratorService),
    defineGeneratorWidget('goal-actual-kpi-chart', GoalActualChartGeneratorService),
    defineGeneratorWidget('card-edit-form-generator', CardEditFormGeneratorService),

    defineWidget({ widgetType: 'gauge-chart-toggle', component: GaugeChartToggleWidgetComponent, provideOwnWrapperContext: true }),

    defineWidget({widgetType: 'button-toolbar', component: ButtonToolbarWidgetComponent}),
    defineWidget({ widgetType: 'phase-edit-form', component: PhaseEditFormComponent }),
    defineWidget({ widgetType: 'metric-gauge', component: MetricGaugeWidgetComponent, provideOwnWrapperContext: true }),
  ]
})
export class BmiWidgetsModule { }
