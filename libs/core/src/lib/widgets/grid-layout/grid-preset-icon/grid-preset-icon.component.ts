import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { GridLayoutPreset } from '../grid-layout-presets';

@Component({
  selector: 'bmi-core-grid-preset-icon',
  templateUrl: './grid-preset-icon.component.html',
  styleUrls: ['./grid-preset-icon.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GridPresetIconComponent {
  @Input() preset: GridLayoutPreset;
}
