import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { FormControl, ValidationErrors } from '@angular/forms';

import { BehaviorSubject, Subscription } from 'rxjs';
import { filter, switchMap } from 'rxjs/operators';

import { EditableAspect } from '../../../editor/editable-aspect';
import { GridSectionEditableAspect } from '../grid-layout-widget.component';
import { GridLayoutSection, fillUpSectionLayout, GridLayoutCell, createCell } from '../grid-layout-config';

import { TypedFormGroup, TypedFormControl, TemplatedFormArray } from '@bmi/ui';


@Component({
  selector: 'bmi-core-grid-layout-section-sidebar',
  templateUrl: './grid-layout-section-sidebar.component.html',
  styleUrls: ['../../../editor-sidebar/editor-sidebar.scss', './grid-layout-section-sidebar.component.scss']
})
export class GridLayoutSectionSidebarComponent implements OnInit, OnDestroy {
  sectionConfig: GridLayoutSection = null;
  private subscriptions: Subscription[];

  readonly form = new TypedFormGroup({
    layoutId: new TypedFormControl<string>(null),
    cells: new TemplatedFormArray<GridLayoutCell>(c => new TypedFormControl(c))
  });

  get sectionCells()  {
    return this.form.controls.cells as TemplatedFormArray<GridLayoutCell>;
  }

  constructor(
    @Inject(EditableAspect) private aspect: GridSectionEditableAspect
  ) {}

  ngOnInit() {
    this.subscriptions = [
      this.aspect.sectionConfig.subscribe(config => {
        this.sectionConfig = config;
        if (this.sectionConfig) {
          this.form.patchValue({
            layoutId: config.layoutId,
            cells: config.cells
          }, { emitEvent: false });
        }
      }),

      this.form.valueChanges.pipe(
        filter(x => this.sectionConfig && this.form.valid)
      ).subscribe(formValue => {
        const hasChangedLayout = this.sectionConfig.layoutId !== formValue.layoutId;
        const updatedSection: GridLayoutSection = {
          ...this.sectionConfig,
          layoutId: formValue.layoutId,
          cells: formValue.cells
        }

        this.aspect.updateConfig(hasChangedLayout ? fillUpSectionLayout(updatedSection) : updatedSection);
      })
    ];
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  describeCellForRearranging(cell: GridLayoutCell): string {
    if (!cell.widgetConfig) {
      return 'Empty Cell'
    } else if (typeof cell.widgetConfig === 'string') {
      return `Widget ${cell.widgetConfig}`
    } else if (cell.widgetConfig.title) {
      return `${cell.widgetConfig.title} (${cell.widgetConfig.widgetType})`;
    } else {
      return `${cell.widgetConfig.widgetType} widget`;
    }
  }

  addGridCell() {
    this.sectionCells.pushValue(createCell());
  }
}
