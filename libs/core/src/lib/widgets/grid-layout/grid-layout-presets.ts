
export interface GridLayoutPreset {
  id: string;
  name: string;
  spans: {col: number, row: number}[];

  /**
   * If specified, this is the default number of cells to use when filling up
   * the layout. If not falsy, the number of spans will be used instead.
   */
  defaultCells?: number;
}


export const GRID_LAYOUT_PRESETS: GridLayoutPreset[] = [
  {
    id: '1-column',
    name: 'Full Width',
    spans: [
      {col: 12, row: 1},
      {col: 12, row: 1},
      {col: 12, row: 1}
    ],
    defaultCells: 1
  },
  {
    id: '2-column',
    name: 'Two Column',
    spans: [
      {col: 6, row: 1},
      {col: 6, row: 1}
    ]
  },

  {
    id: '3-column',
    name: 'Three Column',
    spans: [
      {col: 4, row: 1},
      {col: 4, row: 1},
      {col: 4, row: 1}
    ]
  },

  {
    id: '4-column',
    name: 'Four Column',
    spans: [
      {col: 3, row: 1},
      {col: 3, row: 1},
      {col: 3, row: 1},
      {col: 3, row: 1}
    ]
  },

  {
    id: '1-third-2-thirds',
    name: 'One Third, Two Thirds',
    spans: [
      {col: 4, row: 1},
      {col: 8, row: 1},
    ]
  },

  {
    id: '2-thirds-1-third',
    name: 'Two Thirds, One Third',
    spans: [
      {col: 8, row: 1},
      {col: 4, row: 1},
    ]
  },

  {
    id: '2-stacked-left',
    name: 'Two Stacked (Left)',
    spans: [
      {col: 6, row: 1},
      {col: 6, row: 2},
      {col: 6, row: 1}
    ]
  },

  {
    id: '2-stacked-right',
    name: 'Two Stacked (Right)',
    spans: [
      {col: 6, row: 2},
      {col: 6, row: 1},
      {col: 6, row: 1}
    ]
  },

  {
    id: '3-stacked-left',
    name: 'Three Stacked (Left)',
    spans: [
      {col: 6, row: 1},
      {col: 6, row: 3},
      {col: 6, row: 1},
      {col: 6, row: 1}
    ]
  },

  {
    id: '3-stacked-right',
    name: 'Three Stacked (Right)',
    spans: [
      {col: 6, row: 3},
      {col: 6, row: 1},
      {col: 6, row: 1},
      {col: 6, row: 1}
    ]
  }
];
