import { Component, Input, Inject, OnInit, OnDestroy } from '@angular/core';
import { FormControl, ValidationErrors } from '@angular/forms';

import { BehaviorSubject, combineLatest, Subscription } from 'rxjs';
import { filter, switchMap } from 'rxjs/operators';

import { WidgetSelection } from '../../../widget-config';
import { GridCellEditableAspect } from '../grid-layout-widget.component';

import { TypedFormGroup, TypedFormControl, IdpValidators } from '@bmi/ui';

import { GridLayoutSection, GridLayoutCell } from '../grid-layout-config';
import { GRID_LAYOUT_PRESETS } from '../grid-layout-presets';

import { EditableAspect } from '../../../editor/editable-aspect';

@Component({
  selector: 'bmi-core-grid-layout-cell-sidebar',
  templateUrl: './grid-layout-cell-sidebar.component.html',
  styleUrls: ['../../../editor-sidebar/editor-sidebar.scss']
})
export class GridLayoutCellSidebarComponent implements OnInit, OnDestroy {

  sectionConfig: GridLayoutSection = null;
  cellConfig: GridLayoutCell = null;
  sectionPresetName: string = null;

  subscriptions: Subscription[] = [];

  readonly form = new TypedFormGroup({
    span: new TypedFormControl<number>(12, IdpValidators.if(() => this.allowSpanEditing,
      IdpValidators.required,
      IdpValidators.mustBeNumber(),
      IdpValidators.min(1),
      IdpValidators.max(12)
    )),
    rowSpan: new TypedFormControl<number>(1, IdpValidators.if(() => this.allowSpanEditing,
      IdpValidators.required,
      IdpValidators.mustBeNumber(),
      IdpValidators.min(1),
      IdpValidators.max(12)
    )),
    noCard: new TypedFormControl<boolean>(false),
    widgetSelection: new TypedFormControl<WidgetSelection>(null)
  });

  get allowSpanEditing() {
    return !this.sectionPresetName;
  }

  constructor(@Inject(EditableAspect) private aspect: GridCellEditableAspect) {}

  ngOnInit() {
    this.subscriptions = [
      combineLatest(
        this.aspect.cellConfig,
        this.aspect.sectionConfig
      ).subscribe(([cellConfig, sectionConfig]) => {
        this.sectionConfig = sectionConfig;
        this.cellConfig = cellConfig;
        this.sectionPresetName = this.getSectionPresetName(this.sectionConfig);

        if (this.cellConfig) {
          this.form.patchValue({
            span: this.form.controls.span.pristine ? cellConfig.span || 12 : cellConfig.span,
            rowSpan: this.form.controls.rowSpan.pristine ? cellConfig.rowSpan || 1 : cellConfig.rowSpan,
            noCard: cellConfig.noCard,
            widgetSelection: cellConfig.widgetConfig
          }, { emitEvent: false });
        }
      }),

      this.form.valueChanges.pipe(
        filter(x => this.cellConfig && this.form.valid)
      ).subscribe(formValue => {
        this.aspect.updateConfig({
          ...this.cellConfig,
          span: formValue.span || 12,
          rowSpan: formValue.rowSpan || 1,
          noCard: Boolean(formValue.noCard),
          widgetConfig: formValue.widgetSelection
        });
      })
    ];
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  private getSectionPresetName(config: GridLayoutSection): string {
    if (this.sectionConfig && this.sectionConfig.layoutId) {
      const preset = GRID_LAYOUT_PRESETS.find(p => p.id === this.sectionConfig.layoutId);
      return preset ? preset.name : 'Unknown preset';
    } else {
      return null;
    }
  }

  selectSection() {
    if (this.aspect) {
      this.aspect.selectSection();
    }
  }
}
