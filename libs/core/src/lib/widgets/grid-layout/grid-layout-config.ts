import { WidgetConfig, WidgetSelection } from '../../widget-config';
import { uniqueId } from '@bmi/utils';

import { GRID_LAYOUT_PRESETS } from './grid-layout-presets';


export type GridLayoutConfig = WidgetConfig<GridLayoutFields>;


export interface GridLayoutFields {
  /**
   * A grid is made up of sections.
   *
   * Each section is treated as a separate grid, as this simplifies preset
   * layouts.
   */
  sections: GridLayoutSection[];
  neverShowCards?: boolean;

  // This is here for compatibility while migrating to using sections.
  // New and updated pages won't have it; then we can get rid of it once it's
  // no longer used.
  widgets?: GridLayoutCell[];

}


/**
 * A grid section is made up of cells.
 *
 * A grid section is rendered using CSS Grid, and the cells flow across then
 * down, according to their column and rowspan properties.
 *
 * This allows manual control of the layout, but it's not very user friendly.
 * To simplify this, we can specify a layout preset to use for the section which
 * will determine the sizing for each cell, overriding the config.
 *
 * This means the user can pick options like "3 column" and easily switch
 * between them without having to grok the 12 column grid and the intricacies of
 * rowspan.
 */
export interface GridLayoutSection {
  /**
   * The layout preset ID for this grid section.
   *
   * If this is a valid layout Id, then it will override the col and row spans
   * in the cells.
   */
  layoutId?: string;
  cells: GridLayoutCell[];

  /** Optional ID used for tracking grid sections across modifications */
  id?: string;
}


export interface GridLayoutCell {
  span?: number;
  rowSpan?: number;
  noCard?: boolean;
  widgetConfig: WidgetSelection;

  /** Optional ID used for tracking grid cells across modifications */
  id?: string;
}



export function createCell(span: number = 12, config: WidgetConfig = null, id = uniqueId()): GridLayoutCell {
  return {
    id,
    span: span,
    rowSpan: 1,
    widgetConfig: config,
    noCard: false
  };
}


export function createSection(layoutId: string = '2-column', cells: GridLayoutCell[] = [], id = uniqueId()): GridLayoutSection {
  return fillUpSectionLayout({
    id,
    layoutId: layoutId,
    cells: cells
  });
}

export function createGrid(fields: Partial<GridLayoutFields> = {}): GridLayoutConfig {
  return {
    widgetType: 'grid-layout',
    fields: {
      ...fields,
      sections: fields.sections || [createSection()]
    }
  }
}

export function createStackedGrid(widgets: WidgetConfig[], deterministicIds = false, extraFields?: Partial<GridLayoutFields>) {
  return createGrid({
    ...extraFields || {},
    sections: [
      createSection(
        null,
        widgets.map((w, index) => createCell(12, w, deterministicIds ? `cell_${index}` : uniqueId())),
        deterministicIds ? 'section_0' : uniqueId()
      )
    ]
  });
}


export const GridLayoutUtils = {
  createCell,
  createSection,
  createGrid,
  createStackedGrid
};


function trimEmptyCellsFromEnd(cells: GridLayoutCell[], target: number = 0): GridLayoutCell[] {
  const result: GridLayoutCell[] = [...cells];
  for (let i = result.length - 1; i >= 0; i--) {
    if (result.length <= target || result[i].widgetConfig) {
      break;
    } else {
      result.splice(i, 1);
    }
  }
  return result;
}

/**
 * Adds/removes empty cells to fill up the section layout.
 */
export function fillUpSectionLayout(section: GridLayoutSection): GridLayoutSection {
  const preset = GRID_LAYOUT_PRESETS.find(p => p.id === section.layoutId);
  const minColumns = preset ? preset.defaultCells || preset.spans.length : 0;

  const cells = trimEmptyCellsFromEnd(section.cells, minColumns);

  const additionalCellsNeeded = minColumns - cells.length;
  if (additionalCellsNeeded > 0) {
    for (let i = 0; i < additionalCellsNeeded; i++) {
      cells.push(createCell());
    }
  }

  return {
    ...section,
    cells: cells
  };
}


/**
 * Config Migrations:
 *
 * These were a bit of a nuisance, so this is an attempt at a simple framework
 * for handling config migrations in future.
 *
 * The idea here is that we can define a series of migration tasks, and each
 * task consists of 2 functions: is the migration needed, and a transform
 * function to perform the migration.
 *
 * A migration must treat the config as immutable. It's a transformation and
 * thus should clone as needed.
 *
 * Migrations are applied in sequence; so you can assume earlier tasks were
 * applied if necessary.
 */
interface Migration<T> {
  isNeeded(config: T): boolean;
  migrate(config: T): T;
}


// The initial release of the page builder did not have sections, and that
// oversight caused a little grief because it's a structural change to the
// configs. It wasn't unforeseen, but lesson learned.
class InitialSectionMigration implements Migration<GridLayoutConfig> {
  isNeeded(config: GridLayoutConfig) {
    return !config.fields.sections || config.fields.hasOwnProperty('widgets');
  }

  migrate(config: GridLayoutConfig): GridLayoutConfig {
    const updated: GridLayoutConfig = {
      ...config,
      fields: {
        ...config.fields,
        sections: [{
          id: uniqueId(),
          layoutId: null,
          cells: config.fields.widgets
        }]
      }
    }

    delete updated.fields.widgets;
    return updated;
  }
}


// If a section or cell doesn't have an ID it might causes issues when
// rearranging (due to not having a suitable stable key in the config). Plus
// tracking selection by index causes some unnecessary headaches. It's much
// better if we can assume the ID exists.
class MissingIdMigration implements Migration<GridLayoutConfig> {
  isNeeded(config: GridLayoutConfig) {
    return config.fields.sections.some(s => !s.id || s.cells.some(c => !c.id));
  }

  migrate(config: GridLayoutConfig) {
    const cloned = cloneConfigForUpdate(config);
    for (const section of cloned.fields.sections) {
      section.id = section.id || uniqueId();
      for (const cell of section.cells) {
        cell.id = cell.id || uniqueId();
      }
    }
    return cloned;
  }
}


const MIGRATIONS = [
  new InitialSectionMigration(),
  new MissingIdMigration()
];


export function needsMigration(config: GridLayoutConfig): boolean {
  return MIGRATIONS.some(s => s.isNeeded(config));
}

export function migrateConfig(initialConfig: GridLayoutConfig): GridLayoutConfig {
  return MIGRATIONS.reduce((config, step) => step.isNeeded(config) ? step.migrate(config) : config, initialConfig);
}


/**
 * Helper for updating the grid without touching widget configs
 *
 * This will clone the sections and cells leaving those arrays and objects
 * ready for modification, but the widget configs will be carried over
 * verbatim (don't modify these!).
 */
export function cloneConfigForUpdate(original: GridLayoutConfig): GridLayoutConfig {
  return {
    ...original,
    fields: {
      ...original.fields,
      sections: (original.fields.sections || []).map(section => ({
        ...section,
        cells: section.cells.map(cell => ({...cell}))
      }))
    }
  };
}

