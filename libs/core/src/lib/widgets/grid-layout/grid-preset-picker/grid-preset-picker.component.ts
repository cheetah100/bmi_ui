import { Component, Input, forwardRef } from '@angular/core';
import { BaseControlValueAccessor, provideAsControlValueAccessor } from '@bmi/ui';

import { GRID_LAYOUT_PRESETS, GridLayoutPreset } from '../grid-layout-presets';


interface GridPresetOption {
  value: string;
  text: string;
  preset?: GridLayoutPreset;
}


@Component({
  selector: 'bmi-core-grid-preset-picker',
  templateUrl: './grid-preset-picker.component.html',
  styleUrls: ['./grid-preset-picker.component.scss'],
  providers: [ provideAsControlValueAccessor(forwardRef(() => GridPresetPickerComponent)) ]
})
export class GridPresetPickerComponent extends BaseControlValueAccessor<string>{
  options: GridPresetOption[] = [
    { value: null, text: 'None (manual layout)' },
    ...GRID_LAYOUT_PRESETS.map(p => ({ value: p.id, text: p.name, preset: p }))
  ];

  selectedValue: string = null;
  selectedOption: GridPresetOption = this.options[0];

  optionClicked(option: GridPresetOption) {
    this.updateComponentValue(option.value);
    this.onChanged(option.value);
  }

  updateComponentValue(layoutId: string) {
    this.selectedValue = layoutId;
    this.selectedOption = this.options.find(o => o.value === layoutId) || this.options[0];
  }
}
