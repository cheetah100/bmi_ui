import { Injectable } from '@angular/core';
import { WidgetConfig } from '../../widget-config';
import { WidgetConverter } from '../../widget-converter/widget-converter';
import { GridLayoutConfig, createSection, createCell } from './grid-layout-config';


@Injectable({providedIn: 'root'})
export class WrapWithGridConverter implements WidgetConverter<WidgetConfig, GridLayoutConfig>{
  readonly name = 'Wrap with Grid Layout';

  convert(widget: WidgetConfig): GridLayoutConfig {
    return {
      widgetType: 'grid-layout',
      fields: {
        sections: [
          createSection('1-column', [createCell(undefined, widget)])
        ]
      }
    };
  }
}
