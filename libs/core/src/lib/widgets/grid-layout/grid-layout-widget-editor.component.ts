import { Component, OnInit } from '@angular/core';

import { withLatestFrom } from 'rxjs/operators';
import { TypedFormGroup, TypedFormControl, TemplatedFormArray } from '@bmi/ui';

import { WidgetContext } from '../../widget-context';
import { GridLayoutConfig, GridLayoutFields, createSection, createGrid, GridLayoutSection } from './grid-layout-config';

import { copyWidgetConfig, pasteWidgetConfig } from '../../util/copy-paste-widget-config';


@Component({
  template: `
    <ng-container [formGroup]="form">
      <ui-form-field>
        <ui-checkbox formControlName="neverShowCards" label="Disable cards around grid cells"></ui-checkbox>
      </ui-form-field>

      <ui-form-field label="Sections">
        <ui-stacker-list [items]="sections">
          <ng-template let-control>
            Section
            <button type="button" class="btn btn--link btn--small" (click)="copySectionAsWidget(control.value)">
              <ui-icon icon="fa4-clone"></ui-icon> Copy as grid widget
            </button>
          </ng-template>
        </ui-stacker-list>
      </ui-form-field>
      <button type="button" class="btn btn--success btn--small" (click)="addSection()">Add Section</button>
      <button type="button" class="btn btn--link btn--small" (click)="pasteSectionsFromGrid()">
        <ui-icon icon="fa4-paste"></ui-icon> Paste Sections From grid
      </button>
    </ng-container>
  `
})
export class GridLayoutWidgetEditorComponent implements OnInit {
  form = new TypedFormGroup<Partial<GridLayoutFields>>({
    neverShowCards: new TypedFormControl(undefined),
    sections: new TemplatedFormArray(() => new TypedFormControl(undefined))
  });

  get sections() {
    return this.form.controls.sections as TemplatedFormArray<GridLayoutSection>;
  }

  constructor(private context: WidgetContext<GridLayoutConfig>) {}

  ngOnInit() {
    this.context.widgetConfig.subscribe(config => this.form.patchValue(config.fields, {emitEvent: false}));
    this.form.valueChanges.pipe(
    ).subscribe((formFields) => {
      this.context.updateWidgetConfig(config => {
        config.fields = {
          ...config.fields,
          ...formFields
        };
      })
    });
  }

  addSection() {
    this.sections.pushValue(createSection());
  }

  copySectionAsWidget(section: GridLayoutSection) {
    copyWidgetConfig(createGrid({
      sections: [section]
    })).subscribe({
      error: err => alert('Cannot copy widget: ' + String(err))
    });
  }

  pasteSectionsFromGrid() {
    pasteWidgetConfig().subscribe({
      next: widget => {
        if (widget.widgetType === 'grid-layout') {
          const otherGridWidget = widget as GridLayoutConfig;

          this.sections.setValue([
            ...this.sections.value,
            ...otherGridWidget.fields.sections || []
          ]);

        } else {
          alert('Pasted widget is not a grid layout');
        }
      },
      error: err => alert('Cannot paste widget: ' + String(err))
    })
  }
}
