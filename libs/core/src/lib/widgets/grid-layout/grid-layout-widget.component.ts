import { Component, Optional, OnInit, OnDestroy, HostBinding, Host, Injector } from '@angular/core';

import { combineLatest, Observable, BehaviorSubject } from 'rxjs';
import { map, distinctUntilChanged, publishBehavior, refCount } from 'rxjs/operators';

import { WidgetContext } from '../../widget-context';
import { WidgetConfig, WidgetSelection } from '../../widget-config';
import { EditorContext } from '../../editor/editor-context';
import { EditableAspect } from '../../editor/editable-aspect';

import {
  GridLayoutConfig, GridLayoutSection, GridLayoutCell,
  createCell, createSection, needsMigration, cloneConfigForUpdate, migrateConfig
} from './grid-layout-config';

import { GRID_LAYOUT_PRESETS, GridLayoutPreset } from './grid-layout-presets';

import { uniqueId } from '@bmi/utils';
import { PageEditorContext } from '../../page-editor/page-editor-context';
import { PageComponent } from '../../page/page.component';
import { UiCardComponent } from '../../components/ui-card/ui-card.component';



export interface GridEditableAspect extends EditableAspect {
  sectionIndex: number;
  cellIndex?: number;
}

export interface GridSectionEditableAspect extends GridEditableAspect {
  sectionIndex: number;
  sectionConfig: Observable<GridLayoutSection>;
  updateConfig(sectionConfig: GridLayoutSection);
}

export interface GridCellEditableAspect extends GridEditableAspect {
  sectionIndex: number;
  cellIndex: number;
  sectionConfig: Observable<GridLayoutSection>;
  cellConfig: Observable<GridLayoutCell>;
  updateConfig(cellConfig: GridLayoutCell);
  selectSection();
}



@Component({
  selector: 'bmi-grid-layout-widget',
  templateUrl: './grid-layout-widget.component.html',
  styleUrls: ['./grid-layout-widget.component.scss']
})
export class GridLayoutWidgetComponent implements OnInit, OnDestroy {
  widgetConfig: GridLayoutConfig = null;
  sections: GridLayoutSection[] = [];
  isInsideCard = false;
  disableCardEditControls = false;
  private viewMode: boolean = false;

  selectedAspectSubject = new BehaviorSubject<GridSectionEditableAspect | GridCellEditableAspect>(null);

  get selectedAspect() {
    return this.selectedAspectSubject.value;
  }

  get selectedSectionIndex() {
    return this.selectedAspect && this.selectedAspect.sectionIndex;
  }

  get selectedCellIndex() {
    return this.selectedAspect && this.selectedAspect.cellIndex;
  }

  get gridWidgets() {
    return this.widgetConfig && this.widgetConfig.fields.widgets;
  }

  get neverShowCards() {
    return this.widgetConfig && !!this.widgetConfig.fields.neverShowCards;
  }

  get selectedSectionConfig(): Observable<GridLayoutSection> {
    return combineLatest(
      this.widgetContext.widgetConfig,
      this.selectedAspectSubject
    ).pipe(
      map(([config, selected]) => {
        if (!selected || needsMigration(config)) {
          return null;
        } else {
          return config.fields.sections[selected.sectionIndex] || null;
        }
      }),
      distinctUntilChanged(),
      publishBehavior(null),
      refCount()
    );
  }

  get selectedCellConfig(): Observable<GridLayoutCell> {
    return combineLatest(
      this.selectedSectionConfig,
      this.selectedAspectSubject
    ).pipe(
      map(([section, selected]) => {
        if (!section || !selected || (selected.cellIndex === null || selected.cellIndex === undefined)) {
          return null;
        } else {
          return section.cells[selected.cellIndex]
        }
      }),
      distinctUntilChanged(),
      publishBehavior(null),
      refCount()
    );
  }

  // tslint:disable-next-line: no-unsafe-any
  @HostBinding('class.is-edit-mode')
  get isInEditMode() {
    return this.widgetContext.isInsideEditor && !this.viewMode;
  }

  constructor(
    private widgetContext: WidgetContext<GridLayoutConfig>,
    private injector: Injector,
    @Optional() private editorContext: EditorContext,
    @Optional() private pageComponent: PageComponent,
    @Optional() private closestCard: UiCardComponent,
  ) { }

  ngOnInit() {
    this.widgetContext.widgetConfig.subscribe(config => {
      if (this.pageComponent) {
        this.pageComponent.requestWidgetsResize();
      }

      this.widgetConfig = migrateConfig(config);

      // Prepare the grid, applying the section layouts as necessary.
      this.sections = this.widgetConfig.fields.sections.map(section => this.applySectionLayout(section));
      this.isInsideCard = !!this.closestCard && this.closestCard.cardExists;
      this.disableCardEditControls = this.neverShowCards || this.isInsideCard;
    });

    if (this.editorContext) {
      this.editorContext.watchForOwnSelection(this).subscribe(aspect => {
        this.selectedAspectSubject.next(aspect as GridCellEditableAspect);
      });
      this.widgetContext.isViewMode.subscribe((viewMode: boolean) => this.viewMode = viewMode);
    }
  }

  ngOnDestroy() {
    if (this.selectedAspect) {
      this.editorContext.select(null);
    }
  }


  isSelected(sectionIndex: number, cellIndex?: number): boolean {
    const selectedSectionIndex = this.selectedSectionIndex;
    const selectedCellIndex = this.selectedCellIndex;
    if (cellIndex !== null && cellIndex !== undefined) {
      // We're looking for a specific cell, so the cell index must be an exact
      // match
      return sectionIndex === selectedSectionIndex && cellIndex === this.selectedCellIndex;
    } else {
      // The mismatched section would have been weeded out by the first case
      return sectionIndex === selectedSectionIndex && (selectedCellIndex === null || selectedCellIndex === undefined);
    }
  }


  /**
   * If this section has a layout preset, we apply the col/row spans to each
   * cell. We don't actually mutate the config section here, but the widget
   * configs should be left intact.
   */
  private applySectionLayout(section: GridLayoutSection): GridLayoutSection {
    const layout = GRID_LAYOUT_PRESETS.find(p => p.id === section.layoutId);
    if (layout) {
      const cells = section.cells.map((widget, index) => {
        const span = layout.spans[index % layout.spans.length];
        return {
          ...widget,
          span: span.col,
          rowSpan: span.row || 1
        };
      });

      return {
        ...section,
        cells: cells
      };
    } else {
      return section;
    }
  }


  /**
   * A helper for mutating the grid config (but not the embedded widgetConfigs!)
   *
   * Like the widgetContext.updateWidgetConfig API, this gives you a config you
   * can mutate, but this won't clone the entire subtree, rather it will clone
   * the Grid config parts and leave the child widgets untouched (exact same
   * objects).
   *
   * This will also perform any migration of the config as needed.
   */
  private updateGridConfig(fn: (config: GridLayoutConfig) => void) {
    this.widgetContext.setWidgetConfig(original => {
      const cloned = cloneConfigForUpdate(migrateConfig(original));
      fn(cloned);
      return cloned;
    })
  }

  /**
   * Perform a config migration if necessary.
   *
   * This really has made a bigger mess than expected, because the section
   * migration complicates the indexing logic. Hopefully this can go away soon.
   */
  private migrateConfigIfNecessary() {
    if (needsMigration(this.widgetContext.widgetConfigSnapshot)) {
      this.updateGridConfig(c => c);
    }
  }

  trackGridSection(index: number, section: GridLayoutSection) {
    return section.id;
  }

  trackGridCell(index: number, cell: GridLayoutCell) {
    return cell.id || index;
  }

  gridCellClicked(event: MouseEvent, sectionIndex: number, index: number) {
    if (this.editorContext && event.button === 0) {
      this.migrateConfigIfNecessary();
      this.selectCell(sectionIndex, index);
    }
  }

  selectCell(sectionIndex: number, index: number) {
    if (this.isInEditMode) {
      this.editorContext.select(<GridCellEditableAspect>{
        owner: this,
        tempEditorTypeId: 'grid-layout-cell',
        injector: this.injector,
        sectionIndex: sectionIndex,
        cellIndex: index,
        sectionConfig: this.selectedSectionConfig,
        cellConfig: this.selectedCellConfig,
        selectSection: () => this.selectSection(sectionIndex),
        updateConfig: updatedCellConfig => this.updateGridConfig(c => {
          c.fields.sections[sectionIndex].cells[index] = updatedCellConfig;
        })
      });
    }
  }

  selectSection(sectionIndex: number) {
    if (this.isInEditMode) {
      this.migrateConfigIfNecessary();
      this.editorContext.select(<GridSectionEditableAspect>{
        owner: this,
        tempEditorTypeId: 'grid-layout-section',
        injector: this.injector,
        sectionIndex: sectionIndex,
        sectionConfig: this.selectedSectionConfig,
        updateConfig: sectionConfig => this.updateGridConfig(c => {
          c.fields.sections[sectionIndex] = sectionConfig;
        })
      });
    }
  }

  updateChildWidgetConfig(sectionIndex: number, index: number, widgetConfig: WidgetConfig) {
    this.updateGridConfig(c => c.fields.sections[sectionIndex].cells[index].widgetConfig = widgetConfig);
  }

  addWidget(sectionIndex: number, widgetConfig: WidgetConfig = null) {
    const cell = createCell();
    cell.widgetConfig = widgetConfig;

    this.updateGridConfig(config => {
      config.fields.sections[sectionIndex].cells.push(cell);
    });

    setTimeout(() => {
      const section = this.widgetConfig.fields.sections[sectionIndex];
      const cellIndex = section ? section.cells.findIndex(c => c.id === cell.id) : -1;
      if (cellIndex > 0) {
        this.selectCell(sectionIndex, cellIndex);
      }
    }, 30);
  }

  addSection() {
    this.insertSectionBefore(this.sections.length);
  }

  insertSectionBefore(index: number) {
    this.updateGridConfig(config => {
      config.fields.sections.splice(index, 0, createSection());
    });
  }

  deleteSection(sectionIndex: number) {
    this.updateGridConfig(config => {
      config.fields.sections.splice(sectionIndex, 1);
    });
  }

  deleteGridCell(sectionIndex: number, index: number) {
    event.preventDefault();
    this.updateGridConfig(config => {
      config.fields.sections[sectionIndex].cells.splice(index, 1);
    });
  }

  toggleCard(sectionIndex: number, index: number): void {
    this.updateGridConfig(config => {
      config.fields.sections[sectionIndex].cells[index].noCard = !config.fields.sections[sectionIndex].cells[index].noCard;
    });
  }

}
