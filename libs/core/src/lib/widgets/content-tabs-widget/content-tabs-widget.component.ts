import { Component, OnDestroy, OnInit } from '@angular/core';
import { FuzzySearcher, ObjectMap, switchMapOverItems } from '@bmi/utils';
import { combineLatest, Observable, of, Subscription } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { DataGroup } from '../../data-sources/data-group';
import { DataSource, DataSourceWithListApi } from '../../data-sources/types';
import { PageDataConfig } from '../../page-data-context/page-data-config';
import { PageDataContext } from '../../page-data-context/page-data-context';
import { describeBinding, ParameterBinding } from '../../page-data-context/parameter-binding';
import { bindDataMap } from '../../util/bind-data-map';
import { WidgetConfig } from '../../widget-config';
import { WidgetContext } from '../../widget-context';


export type ContentTabsWidgetConfig = WidgetConfig<ContentTabsFields>;

export interface TabConfig {
  id: string;
  title: string;
  widgetConfig: WidgetConfig;
}

export interface DynamicTabConfig {
  dataSourceId: string;
  id: ParameterBinding;
  title: ParameterBinding;
  widgetConfig: WidgetConfig;
  dataContext?: PageDataConfig;
}

export interface ContentTabsFields {
  tabGenerationMode?: 'static' | 'dynamic-grouped' | 'dynamic-list';
  navLocation: 'top' | 'left' | 'none';
  sections: TabConfig[];
  dynamicConfig?: DynamicTabConfig;
}


interface TabState extends TabConfig {
  dataSources?: ObjectMap<DataSource>;
  dataContext?: PageDataConfig;
}

@Component({
  templateUrl: './content-tabs-widget.component.html',
  styleUrls: ['./content-tabs-widget.component.scss'],
})
export class ContentTabsWidgetComponent implements OnInit, OnDestroy {

  subscription = new Subscription();
  config: ContentTabsWidgetConfig;
  currentTab = 0;
  tabs: TabState[] = [];
  filteredTabs: TabState[] = [];
  private readonly searcher = new FuzzySearcher<TabState>();

  constructor(
    private widgetContext: WidgetContext<ContentTabsWidgetConfig>,
    private dataContext: PageDataContext,
  ) { }

  ngOnInit() {
    this.subscription.add(this.widgetContext.widgetConfig.subscribe(
      config => this.config = config
    ));

    this.subscription.add(this.widgetContext.widgetConfig.pipe(
      switchMap(config => {
        switch (config.fields.tabGenerationMode) {
          case 'dynamic-grouped':
            return this.createGroupedTabs(config.fields.dynamicConfig);
          case 'dynamic-list':
            return this.createListTabs(config.fields.dynamicConfig);
          default:
            return of(config.fields.sections)
        }
      })
    ).subscribe(tabs => {
      this.tabs = [...tabs];
      this.searcher.items = [...tabs];
      this.filteredTabs = [...tabs];
      if (this.currentTab >= tabs.length) {
        this.currentTab = 0;
      }
    }));
  }

  filterTabs(term: string = ''): void {
    if (!!term.trim()) {
      this.filteredTabs = this.searcher.search(term, 'title');
    } else {
      this.filteredTabs = [...this.tabs];
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  selectTab(id: string): void {
    this.currentTab = this.tabs.findIndex(tab => tab.id === id) || 0;
  }

  getTabById(id: string, tabs: TabConfig[]): TabConfig {
    return tabs.find((tab: TabConfig) => tab.id === id) || null;
  }

  updateTabWidgetConfig(updated: WidgetConfig, id: string): void {
    this.widgetContext.updateWidgetConfig(newConfig => {
      const tabMode = newConfig.fields.tabGenerationMode || 'static';
      if (tabMode !== 'static') {
        newConfig.fields.dynamicConfig.widgetConfig = updated;
      } else {
        const target = this.getTabById(id, newConfig.fields.sections);
        target.widgetConfig = updated;
      }
    });
  }

  trackByTab(index: number, tab: TabConfig) {
    return tab.id;
  }

  private createListTabs(dynamicConfig: DynamicTabConfig): Observable<TabState[]> {
    return combineLatest([
      this.dataContext.getFetcher(dynamicConfig.dataSourceId),
      this.widgetContext.isEditMode,
    ]).pipe(
      switchMap(([dataSource, isEditMode]) => {
        if (isEditMode) {
          return of([{
            id: '__edit-mode__',
            title: `{{ ${describeBinding(dynamicConfig.title || dynamicConfig.id)} }}`,
            widgetConfig: dynamicConfig.widgetConfig,
            dataContext: dynamicConfig.dataContext || {},
            dataSources: {
              tabItem: dataSource?.getExampleItem(),
            }
          }]);
        } else if (dataSource && dataSource.bindList) {
          return this.makeTabsFromListSource(dynamicConfig, dataSource as DataSourceWithListApi);
        } else {
          return of([]);
        }
      })
    );
  }

  private makeTabsFromListSource(dynamicConfig: DynamicTabConfig, dataSource: DataSourceWithListApi) {
    return dataSource.bindList().pipe(
      switchMapOverItems(item =>
        combineLatest([
          of(item),
          bindDataMap(this.dataContext.create({dataSources: {this: item}}), {
            id: dynamicConfig.id,
            title: dynamicConfig.title
          })
        ]).pipe(
          map(([item, {id, title}]) => <TabState>{
            id: String(id),
            title: String(title || id),
            widgetConfig: dynamicConfig.widgetConfig,
            dataContext: dynamicConfig.dataContext,
            dataSources: {
              tabItem: item
            }
          })
        )
      )
    );
  }

  private createGroupedTabs(dynamicConfig: DynamicTabConfig): Observable<TabConfig[]> {
    return combineLatest([
      this.dataContext.getFetcher(dynamicConfig.dataSourceId),
      this.widgetContext.isEditMode,
    ]).pipe(
      switchMap(([dataSource, isEditMode]) => {
        if (isEditMode) {
          return of([<DataGroup>{
            id: '__edit-mode__',
            name: `{{ ${describeBinding(dynamicConfig.title || dynamicConfig.id)} }}`,
            items: dataSource as any
          }]);
        } else if (dataSource && dataSource.bindGroups) {
          return dataSource.bindGroups(dynamicConfig.id, dynamicConfig.title);
        } else {
          return of<DataGroup[]>([]);
        }
      }),
      map(groups => groups.map(g => <TabState>{
        id: g.id,
        title: g.name,
        widgetConfig: dynamicConfig.widgetConfig,
        dataContext: dynamicConfig.dataContext,
        dataSources: {
          tabItems: g.items
        }
      }))
    );
  }
}
