import { Component, Optional, OnInit, OnDestroy, Injector } from '@angular/core';
import { WidgetContext } from '../../widget-context';
import { Subscription } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { uniqueId, distinctUntilNotEqual } from '@bmi/utils';
import { TypedFormControl, TypedFormGroup, TemplatedFormArray, Option, DropdownMenuComponent } from '@bmi/ui';
import { ContentTabsWidgetConfig, ContentTabsFields, TabConfig, DynamicTabConfig } from './content-tabs-widget.component';
import { PageEditorContext } from '../../page-editor/page-editor-context';
import { PageDataContext } from '../../page-data-context/page-data-context';
import { FormControl } from '@angular/forms';
import { WidgetConfig } from '../../widget-config';
import { WidgetWrap } from '../../components/widget-wrapper/widget-wrap';
import { DataContextEditorModalService } from '../../page-data-source-manager/data-context-editor-modal.service';

@Component({
  templateUrl: './content-tabs-widget-editor.component.html',
  styles: [`
ui-form-field {
  margin-bottom: 20px;
}
input {
  width: calc(100% - 5px);
}
  `],
})
export class ContentTabsWidgetEditorComponent implements OnInit, OnDestroy {

  private subscription = new Subscription();
  public navOptions: Option[] = [
    { text: 'Top', value: 'top' },
    { text: 'Left', value: 'left' },
  ];

  tabGenerationOptions: Option[] = [
    { text: 'Static (default)', value: 'static' },
    { text: 'Dynamic (grouped from data)', value: 'dynamic-grouped'},
    { text: 'Dynamic (from list)', value: 'dynamic-list'}
  ];

  readonly form = new TypedFormGroup<ContentTabsFields>({
    navLocation: new FormControl('top'),

    sections: new TemplatedFormArray<TabConfig>(
      () => new TypedFormGroup({
        id: new FormControl(uniqueId()),
        title: new FormControl(' '),
        widgetConfig: new FormControl(null),
      })),

    tabGenerationMode: new TypedFormControl(),

    dynamicConfig: new TypedFormGroup<DynamicTabConfig>({
      dataSourceId: new TypedFormControl(),
      id: new TypedFormControl(),
      title: new TypedFormControl(),
      widgetConfig: new TypedFormControl(),
      dataContext: new TypedFormControl(),
    }),
  });

  get isDynamicTabs() {
    const generationMode = this.form.value.tabGenerationMode;
    return generationMode && generationMode !== 'static';
  }

  constructor(
    private widgetContext: WidgetContext<ContentTabsWidgetConfig>,
    private pageEditorContext: PageEditorContext,
    private dataContextEditor: DataContextEditorModalService,
    private injector: Injector,
  ) { }

  ngOnInit() {
    this.subscription.add(this.widgetContext.widgetConfig.subscribe(config => {
      this.form.patchValue({
        tabGenerationMode: 'static',
        ...config.fields,
      }, { emitEvent: false });
    }));

    this.subscription.add(this.form.valueChanges.subscribe(formValue => {
      this.widgetContext.updateWidgetConfig(config => {
        config.fields = {...config.fields, ...formValue};
      });
    }));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  addTab(): void {
    const sections = this.form.controls.sections as TemplatedFormArray<TabConfig>;
    sections.pushValue({
      title: ' ', id: uniqueId(),
      widgetConfig: { widgetType: null, fields: null }
    });
  }

  editDynamicTabDataContext() {
    this.subscription.add(
      this.dataContextEditor.show({
        title: 'Edit Tab Data Context',
        config: this.form.value.dynamicConfig?.dataContext ?? {},
        injector: this.injector,
      }).subscribe(config => {
        this.form.controls.dynamicConfig.patchValue({
          dataContext: config
        });
      })
    )
  }
}
