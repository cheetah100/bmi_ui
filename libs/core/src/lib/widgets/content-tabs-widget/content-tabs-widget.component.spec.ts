import { ComponentFixture } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { WidgetHarness } from '../testing/widget-harness';
import { WidgetHostHostComponent } from '../testing/widget-host-host.component';
import { WidgetTestEnvironment } from '../testing/widget-test-environment';
import { WidgetTestOptions } from "../testing/widget-test-options";
import { ContentTabsWidgetComponent, ContentTabsWidgetConfig } from './content-tabs-widget.component';


const childWidgetConfig = id => ({
  widgetType: 'markdown',
  fields: {
    markdown: `Tab content. (${id}).`,
    bindings: null
  }
});
const sectionConfig = id => ({
  id: `tab${id}`,
  title: `Tab ${id}`,
  widgetConfig: childWidgetConfig(id)
});
const widgetConfig = {
  widgetType: 'content-tabs',
  fields: {
    navLocation: 'top' as 'top',
    sections: [
      sectionConfig(1),
      sectionConfig(2),
      sectionConfig(3)
    ]
  },
}
const envOptions: WidgetTestOptions<ContentTabsWidgetConfig, ContentTabsWidgetComponent> = {
  config: widgetConfig,
  componentType: ContentTabsWidgetComponent,
}
let harness: WidgetHarness;
let fixture: ComponentFixture<WidgetHostHostComponent>;
let env: WidgetTestEnvironment<ContentTabsWidgetConfig, ContentTabsWidgetComponent>;
const headingSelector = '.tab__heading';
const contentSelector = '.tab-content';

describe('ContentTabsWidget', () => {

  let clickableTabButtons;
  beforeEach(async () => {
    env = new WidgetTestEnvironment<ContentTabsWidgetConfig, ContentTabsWidgetComponent>(envOptions);
    fixture = env.fixture;
    harness = await env.getHarness(WidgetHarness);
    clickableTabButtons = fixture.debugElement.queryAll(By.css(headingSelector));
    fixture.detectChanges();
  });

  afterEach(async () => {
    env.fixture.destroy();
  });

  describe('widget display, default mode', () => {
    it('should show the first tab\'s content by default', async () => {
      const tabContent = await harness.getDisplayText(contentSelector);
      expect(tabContent).toBe('Tab content. (1).');
    });
    it('should display tabs matching config sections', async () => {
      const tabTitles = await harness.getDisplayTexts(headingSelector);
      expect(tabTitles).toEqual(['Tab 1', 'Tab 2', 'Tab 3']);
    });
  });

  describe('on selecting a tab', () => {
    it('should display the associated content', async () => {
      let tabContent = await harness.getDisplayText(contentSelector);
      expect(tabContent).toBe('Tab content. (1).');
      clickableTabButtons[2].nativeElement.click();
      fixture.detectChanges();
      tabContent = await harness.getDisplayText(contentSelector);
      expect(tabContent).toBe('Tab content. (3).');
    });
  });

  describe('widget display, grouped mode', () => {
    beforeEach(() => {
      env.config = {
        widgetType: 'content-tabs',
        fields: {
          navLocation: 'top',
          sections: [],
          tabGenerationMode: "dynamic-grouped",
          dynamicConfig: {
            dataSourceId: "cards",
            id: {
              bindingType: "data-path",
              path: "this.value"
            },
            title: {
              bindingType: 'data-path',
              path: 'this.name'
            },
            widgetConfig: {
              widgetType: 'markdown',
              fields: {
                markdown: `Grouped tab content: {{id}}.`,
                bindings: {
                  id: {
                    bindingType: 'data-path',
                    path: 'tabItems.value'
                  }
                }
              }
            }
          },
        }
      };
    });
    it('should show tab content from the first generated tab', async () => {
      const tabContent = await harness.getDisplayText(contentSelector);
      expect(tabContent).toBe('Grouped tab content: 0.');
    })
    it('should show sorted tabs generated from the selected field in view mode', async () => {
      const tabTitles = await harness.getDisplayTexts(headingSelector);
      expect(tabTitles).toEqual(['Beelzebub', 'Dog', 'Wayne']);
    })
    it('should show a single tab containing the tab field config in edit mode', async () => {
      env.viewMode = 'edit';
      const tabTitles = await harness.getDisplayTexts(headingSelector);
      expect(tabTitles).toEqual(['{{ Path: this.name }}']);
    });
  });

  describe('widget display, list mode', () => {
    beforeEach(() => {
      env.config = {
        widgetType: 'content-tabs',
        fields: {
          navLocation: 'top',
          sections: [],
          tabGenerationMode: "dynamic-list",
          dynamicConfig: {
            dataSourceId: "cards",
            id: {
              bindingType: "data-path",
              path: "this.value"
            },
            title: {
              bindingType: 'data-path',
              path: 'this.name'
            },
            widgetConfig: {
              widgetType: 'markdown',
              fields: {
                markdown: `List tab content.`,
                bindings: {
                  id: {
                    bindingType: 'data-path',
                    path: 'this.value'
                  }
                }
              }
            }
          },
        }
      };
    });
    it('should show tabs generated from the selected field in view mode', async () => {
      const tabTitles = await harness.getDisplayTexts(headingSelector);
      expect(tabTitles).toEqual(['Dog', 'Wayne', 'Beelzebub']);
    })
    it('should show a single tab containing the tab field config in edit mode', async () => {
      env.viewMode = 'edit';
      const tabTitles = await harness.getDisplayTexts(headingSelector);
      expect(tabTitles).toEqual(['{{ Path: this.name }}']);
    });
  });

  /**
   * Just checking this in a more unit testy way, for coverage. Because doing
   * it through user interactions is hard.
   */
  describe('filterTabs()', () => {
    it('should filter tabs based on the search term', async () => {
      env.component.filterTabs('');
      expect(env.component.filteredTabs.map(tab => tab.title)).toEqual(['Tab 1', 'Tab 2', 'Tab 3']);
      env.component.filterTabs('2');
      expect(env.component.filteredTabs.map(tab => tab.title)).toEqual(['Tab 2']);
      env.component.filterTabs('ab ');
      expect(env.component.filteredTabs.map(tab => tab.title)).toEqual(['Tab 1', 'Tab 2', 'Tab 3']);
    });
  });

});

