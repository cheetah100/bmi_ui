import { Injectable } from '@angular/core';
import { WidgetConfig } from '../../widget-config';
import { WidgetConverter } from '../../widget-converter/widget-converter';
import { ContentTabsWidgetConfig } from './content-tabs-widget.component';
import { uniqueId } from '@bmi/utils';

@Injectable({providedIn: 'root'})
export class WrapWithTabsConverter implements WidgetConverter<WidgetConfig, ContentTabsWidgetConfig> {
  readonly name = 'Wrap with Tabs';

  convert(widget: WidgetConfig): ContentTabsWidgetConfig {
    return {
      widgetType: 'content-tabs',
      fields: {
        navLocation: 'top',
        sections: [
          {
            id: uniqueId(),
            title: 'Tab 1',
            widgetConfig: widget
          }
        ]
      }
    };
  }
}
