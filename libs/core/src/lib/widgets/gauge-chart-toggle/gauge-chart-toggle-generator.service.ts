import { Injectable } from '@angular/core';
import { COLORS } from '@bmi/ui';
import { Observable, of } from 'rxjs';
import { ParameterBinding } from '../../formatter';
import { ColorZone } from '../../util/color-zone';
import { WidgetConfig } from '../../widget-config';
import { ContentTabsWidgetConfig } from '../content-tabs-widget/content-tabs-widget.component';
import { GeneratorService } from '../generator-widget/generator-service';
import { InfoboxWidgetConfig } from '../infobox/infobox-config';


export interface GaugeChartToggleWidgetFields {
  metricCardSource: string;
  actualDataSource: string;
  goalDataSource: string;
  size: DisplaySize;
  showChart: boolean;
  useZones: boolean;
  zones: ColorZone[];
}

export type DisplaySize = 'small' | 'medium' | 'large';
export type GaugeChartToggleWidgetConfig = WidgetConfig<GaugeChartToggleWidgetFields>;

export const DEFAULT_ZONES: ColorZone[] = [{
  value: 25,
  color: COLORS.statusRed.cssColor
}, {
  value: 75,
  color: COLORS.statusYellow.cssColor
}, {
  color: COLORS.statusGreen.cssColor
}];

@Injectable({ providedIn: 'root' })
export class GaugeChartToggleGeneratorService implements GeneratorService<GaugeChartToggleWidgetConfig, ContentTabsWidgetConfig> {

  generate(config: GaugeChartToggleWidgetConfig): Observable<ContentTabsWidgetConfig> {
    return of({
      widgetType: 'content-tabs',
      fields: {
        navLocation: 'top' as 'top',
        filter: null,
        sections: [
          {
            id: 'tab-gauge',
            title: 'Gauge',
            widgetConfig: this.generateGauge(config),
          },
          {
            id: 'tab-chart',
            title: 'Chart',
            widgetConfig: this.generateChart(config),
          },
        ]
      },
      flairs: null
    });
  }

  private binding(path: string): ParameterBinding {
    return {
      bindingType: 'data-path',
      path,
    };
  }

  generateGauge(config: GaugeChartToggleWidgetConfig): InfoboxWidgetConfig {
    const source = config.fields.metricCardSource;
    const widgetConfig: InfoboxWidgetConfig = {
      widgetType: 'infobox',
      flairs: null,
      fields: {
        value: this.binding(`${source}.actual`),
        displayValue: {
          bindingType: 'formatter',
          formatterType: 'number-format',
          input: this.binding(`${source}.actual`),
          style: 'percentage-0-to-100'
        },
        styleOptions: {
          visualization: 'gauge',
          size: config.fields.size,
          color: {
            bindingType: 'formatter',
            formatterType: 'color-name',
            input: this.binding(`${source}.status`),
          },
        },
      }
    };
    if (config.fields.useZones) {
      widgetConfig.fields.styleOptions.color = {
        bindingType: 'formatter',
        formatterType: 'color-zone',
        input: this.binding(`${source}.actual`),
        zones: config.fields.zones,
      }
    }
    return widgetConfig;
  }

  generateChart(config: GaugeChartToggleWidgetConfig) {
    const widgetConfig: WidgetConfig = {
      widgetType: 'goal-actual-kpi-chart',
      flairs: null,
      fields: {
        actualDataSource: config.fields.actualDataSource,
        goalDataSource: config.fields.goalDataSource,
        kpi: this.binding(`${config.fields.metricCardSource}.kpi`),
        defaultChartSettings: {
          chartHeight: '50%',
          hideAxisTitles: true,
          hideLegend: true,
          xAxisLabels: {
            formatFunction: value => {
              return typeof value === 'string' ? value.split(' ')[0] : value;
            },
          },
        },
      }
    };
    if (config.fields.useZones) {
      widgetConfig.fields.zones = config.fields.zones;
    }
    return widgetConfig;
  }

}
