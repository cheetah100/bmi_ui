import { WidgetHarness } from '../testing/widget-harness';
import { WidgetTestOptions } from '../testing/widget-test-options';
import { WidgetTestEnvironment } from '../testing/widget-test-environment';
import { GaugeChartToggleWidgetConfig } from './gauge-chart-toggle-generator.service';
import { GaugeChartToggleWidgetComponent } from './gauge-chart-toggle-widget.component';
import { By } from '@angular/platform-browser';


const envOptions: WidgetTestOptions<GaugeChartToggleWidgetConfig, GaugeChartToggleWidgetComponent> = {
  config: {
    widgetType: "gauge-chart-toggle",
    fields: {
      size: "large",
      useZones: true,
      zones: [
        {
          value: 25,
          color: "#CF2030"
        },
        {
          value: 75,
          color: "#FFCC00"
        },
        {
          color: "#42A942"
        }
      ],
      showChart: true,
      metricCardSource: 'card',
      actualDataSource: 'cards',
      goalDataSource: 'cards'
    },
    flairs: [
    {
      flairType: "widget-title",
      title: 'Toggle widget title'
    }],
  },
  componentType: GaugeChartToggleWidgetComponent,
};
let harness: WidgetHarness;
let env: WidgetTestEnvironment<GaugeChartToggleWidgetConfig, GaugeChartToggleWidgetComponent>;
const viewChartSelector = '.btn--link';
const closeChartSelector = 'div.icon-exit';

describe('GaugeChartToggleWidget', () => {

  beforeEach(async () => {
    env = new WidgetTestEnvironment<GaugeChartToggleWidgetConfig, GaugeChartToggleWidgetComponent>(envOptions);
    harness = await env.getHarness(WidgetHarness);
  });

  afterEach(async () => {
    env.fixture.destroy();
  });

  describe('populate gauge title', () => {
    it('should show the title flair as widget heading', async () => {
      const title = await harness.getDisplayText('h2');
      expect(title).toBe('Toggle widget title');
    })
  });

  describe('toggle chart view', () => {
    it('should provide a button to show the chart, if chart is enabled', () => {
      const button = env.fixture.debugElement.query(By.css(viewChartSelector));
      expect(button).toBeTruthy();
    });
    it('should not provide a toggle button for no chart', () => {
      env.config = {
        widgetType: "gauge-chart-toggle",
        fields: {
          size: "large",
          useZones: false,
          zones: null,
          showChart: false,
          metricCardSource: 'card',
          actualDataSource: 'cards',
          goalDataSource: 'cards'
        },
      };
      env.fixture.detectChanges();
      const button = env.fixture.debugElement.query(By.css(viewChartSelector));
      expect(button).toBeFalsy();
    });
    it('should show the close chart button on chart view only', () => {
      let button = env.nativeElementbyCss(closeChartSelector);
      expect(button).toBeFalsy();
      env.component.toggleChart(true);
      env.fixture.detectChanges();
      button = env.nativeElementbyCss(closeChartSelector);
      expect(button).toBeTruthy();
    });
    it('should return to the gauge view on press of close chart button', () => {
      env.component.toggleChart(true);
      env.fixture.detectChanges();
      const button = env.nativeElementbyCss(closeChartSelector);
      button.click();
      env.fixture.detectChanges();
      const gauge = env.nativeElementbyCss('.gauge');
      expect(gauge).toBeTruthy();
    });
  });

});
