import { Injectable } from '@angular/core';
import { WidgetConfig } from '../../widget-config';
import { WidgetConverter } from '../../widget-converter/widget-converter';
import { GaugeChartToggleWidgetConfig, DEFAULT_ZONES } from './gauge-chart-toggle-generator.service';
import { ContentTabsWidgetConfig } from '../content-tabs-widget/content-tabs-widget.component';
import { InfoboxWidgetConfig } from '../infobox/infobox-config';
import { GoalActualMetricChartFields } from '../goal-actual-kpi-chart/goal-actual-chart-generator.service';
import { splitPath } from '../../page-data-context/split-path';
import { isPathBinding } from '../../page-data-context/parameter-binding';

import isMatch from 'lodash-es/isMatch';

@Injectable({ providedIn: 'root' })
export class ContentTabsToGaugeChartConverter implements WidgetConverter<WidgetConfig, GaugeChartToggleWidgetConfig> {
  readonly name = 'Convert Tabs to Gauge/Chart Widget';

  shouldShowFor(config: ContentTabsWidgetConfig) {
    return isMatch(config, {
      widgetType: 'content-tabs',
      fields: {
        sections: [
          {
            widgetConfig: { widgetType: 'infobox' }
          },
          {
            widgetConfig: { widgetType: 'goal-actual-kpi-chart' }
          }
        ]
      }
    });
  }

  convert(config: ContentTabsWidgetConfig): GaugeChartToggleWidgetConfig {
    const infoboxConfig = config.fields.sections[0].widgetConfig as InfoboxWidgetConfig;
    const chartConfig = config.fields.sections[1].widgetConfig as WidgetConfig<GoalActualMetricChartFields>;

    const gaugeValueBinding = infoboxConfig.fields.value;
    const cardSource = isPathBinding(gaugeValueBinding) ? splitPath(gaugeValueBinding.path)[0] : null;

    return {
      widgetType: 'gauge-chart-toggle',
      fields: {
        size: 'medium',
        useZones: false,
        zones: DEFAULT_ZONES,
        showChart: true,
        metricCardSource: cardSource,
        actualDataSource: chartConfig.fields.actualDataSource,
        goalDataSource: chartConfig.fields.goalDataSource
      },

      flairs: config.flairs || []
    };
  }
}
