import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { TypedFormGroup, TypedFormControl } from '@bmi/ui';
import { WidgetContext } from '../../widget-context';
import { GaugeChartToggleWidgetFields, GaugeChartToggleWidgetConfig, DisplaySize, DEFAULT_ZONES } from './gauge-chart-toggle-generator.service';
import { Option } from '@bmi/utils';
import { ColorZone } from '../../util/color-zone';

@Component({
  templateUrl: './gauge-chart-toggle-widget-editor.component.html',
  styles: [`
ui-form-field {
  margin-bottom: var(--gra-spacing-half);
}
  `],
})
export class GaugeChartToggleWidgetEditorComponent implements OnInit, OnDestroy {

  readonly sizeOptions: Option[] = [
    { text: 'Small', value: 'small' },
    { text: 'Medium', value: 'medium' },
    { text: 'Large', value: 'large' },
  ];
  private readonly subscription = new Subscription();
  readonly form = new TypedFormGroup<GaugeChartToggleWidgetFields>({
    size: new TypedFormControl<DisplaySize>('medium'),
    useZones: new TypedFormControl<boolean>(false),
    zones: new TypedFormControl<ColorZone[]>(DEFAULT_ZONES),
    showChart: new TypedFormControl<boolean>(false),
    metricCardSource: new TypedFormControl<string>(null),
    actualDataSource: new TypedFormControl<string>(null),
    goalDataSource: new TypedFormControl<string>(null),
  });


  constructor(
    private widgetContext: WidgetContext<GaugeChartToggleWidgetConfig>,
  ) { }

  ngOnInit() {
    this.subscription.add(this.widgetContext.widgetConfig.subscribe((config: GaugeChartToggleWidgetConfig) => {
      this.form.patchValue(config.fields, { emitEvent: false });
    }));

    this.subscription.add(this.form.valueChanges.subscribe(updatedValue =>
      this.widgetContext.updateWidgetConfig(c => c.fields = updatedValue)
    ));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
