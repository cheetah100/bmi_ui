import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { WidgetWrapperContext } from '../../components/widget-wrapper/widget-wrap';
import { FlairIcon } from '../../flair/flair';
import { FlairContextService } from '../../flair/flair-context.service';
import { PageDataContext } from '../../page-data-context/page-data-context';
import { WidgetConfig } from '../../widget-config';
import { WidgetContext } from '../../widget-context';
import { GaugeChartToggleGeneratorService, GaugeChartToggleWidgetConfig } from './gauge-chart-toggle-generator.service';


@Component({
  template: `

<section
  class="toggler__gauge-grid toggler__gauge-grid--{{ gaugeSize }}" *ngIf="!showingChart">
  <bmi-widget-host [config]="gaugeConfig"></bmi-widget-host>
  <div class="toggler__gauge-grid__info">
    <h2>{{ title | async }}</h2>
    <span>{{ quarter }}</span>
    <bmi-flair-icons class="toggler__gauge-grid__icons" [flairIcons]="icons | async"></bmi-flair-icons>
    <button
      *ngIf="hasChart"
      type="button"
      class="btn btn--link btn--small"
      (click)="toggleChart(true)">
      <span class="icon-analysis"></span>&nbsp;View chart
    </button>
  </div>
</section>

<section class="toggler__chart-section" *ngIf="showingChart">
  <h2>{{ title | async }}</h2>
  <bmi-widget-host [config]="chartConfig"></bmi-widget-host>
  <div (click)="toggleChart(false)" class="icon-exit" title="Switch back to gauge"></div>
</section>

  `,
  styleUrls: [
    './gauge-chart-toggle-widget.component.scss',
    '../infobox/cui-gauge-overrides.scss',
  ],
})
export class GaugeChartToggleWidgetComponent implements OnInit, OnDestroy {

  gaugeConfig: WidgetConfig;
  chartConfig: WidgetConfig;
  hasChart = false;
  showingChart = false;
  title: Observable<string>;
  icons: Observable<FlairIcon[]> = null;
  gaugeSize = 'large';
  quarter: string = null;
  private subscription = new Subscription();

  constructor(
    private widgetContext: WidgetContext,
    private generator: GaugeChartToggleGeneratorService,
    private binder: PageDataContext,
    private widgetWrapperContext: WidgetWrapperContext,
    private flairContextService: FlairContextService,
  ) { }

  ngOnInit() {

    this.subscription.add(
      this.widgetContext.widgetConfig.subscribe(
        (config: GaugeChartToggleWidgetConfig) => {
          this.gaugeConfig = this.generator.generateGauge(config);
          this.gaugeSize = config.fields.size;
          this.hasChart = config.fields.showChart;
          if (this.hasChart) {
            this.chartConfig = this.generator.generateChart(config);
          } else {
            this.showingChart = false;
          }
        }
      )
    );

    this.subscription.add(
      this.widgetContext.widgetConfig.pipe(
        switchMap((config: GaugeChartToggleWidgetConfig) => this.binder.bind({
          bindingType: 'data-path',
          path: `${config.fields.metricCardSource}.quarter.name`,
        }))
      ).subscribe((quarter: string) => this.quarter = quarter)
    );

    this.icons = this.flairContextService.icons;
    this.title = this.widgetWrapperContext.title;

  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  toggleChart(bool: boolean): void {
    this.showingChart = bool;
  }

}
