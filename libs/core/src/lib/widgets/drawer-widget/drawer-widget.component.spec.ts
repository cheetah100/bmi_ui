import { By } from '@angular/platform-browser';
import { WidgetHarness } from '../testing/widget-harness';
import { WidgetTestEnvironment } from '../testing/widget-test-environment';
import { WidgetTestOptions } from "../testing/widget-test-options";
import { DrawerWidgetConfig } from './drawer-widget-config';
import { DrawerWidgetComponent } from './drawer-widget.component';

const childWidgetConfig = {
  widgetType: 'markdown',
  fields: {
    markdown: 'Some markdown content for testing.',
    bindings: null
  }
}
const envOptions: WidgetTestOptions<DrawerWidgetConfig, DrawerWidgetComponent> = {
  config: {
    widgetType: 'drawer',
    fields: {
      body: childWidgetConfig
    },
  },
  componentType: DrawerWidgetComponent,
}
let harness: WidgetHarness;
let env: WidgetTestEnvironment<DrawerWidgetConfig, DrawerWidgetComponent>;
const buttonSelector = '.drawer-widget__toggle-button';
let clickableButton;

describe('DrawerWidget', () => {

  beforeEach(async () => {
    env = new WidgetTestEnvironment<DrawerWidgetConfig, DrawerWidgetComponent>(envOptions);
    harness = await env.getHarness(WidgetHarness);
    clickableButton = env.fixture.debugElement.query(By.css(buttonSelector));
  });

  afterEach(async () => {
    env.fixture.destroy();
  });

  describe('toggle drawer', () => {

    it('should display the correct button text for a closed drawer', async () => {
      const buttonText = await harness.getDisplayText(buttonSelector);
      expect(buttonText).toBe('Show more');     
    });

    it('should display the correct button text for an open drawer', async () => {
      clickableButton.nativeElement.click();
      env.fixture.detectChanges();
      const buttonText = await harness.getDisplayText(buttonSelector);
      expect(buttonText).toBe('Show less'); 
    });

  });

  describe('widget display', () => {

    it('should display the contained widget when open', async () => {
      clickableButton.nativeElement.click();
      env.fixture.detectChanges();
      const widgetText = await harness.getDisplayText('.markdown-content');
      expect(widgetText).toBe(childWidgetConfig.fields.markdown);
    });

    it('should not display the contained widget when closed', async () => {
      const widgetText = await harness.getDisplayText('.drawer-widget');
      expect(widgetText).toBe('Show more');
    });

    it('should update the contained widget as its config changes (open)', async () => {
      clickableButton.nativeElement.click();
      env.fixture.detectChanges();
      let widgetText = await harness.getDisplayText('.markdown-content');
      expect(widgetText).toBe(childWidgetConfig.fields.markdown);
      env.config = {
        widgetType: 'drawer',
        fields: {
          body: {widgetType: 'markdown', fields: {markdown: 'New widget display'}}
        },
      };
      widgetText = await harness.getDisplayText('.markdown-content');
      expect(widgetText).toBe('New widget display');
    });

  });

});