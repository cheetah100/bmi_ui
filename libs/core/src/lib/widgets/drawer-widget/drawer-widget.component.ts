import { animate, style, transition, trigger } from '@angular/animations';
import { Component } from '@angular/core';
import { BehaviorSubject, combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { WidgetConfig } from '../../widget-config';
import { WidgetContext } from '../../widget-context';
import { DrawerWidgetConfig } from './drawer-widget-config';


@Component({
  templateUrl: './drawer-widget.component.html',
  styleUrls: ['./drawer-widget.component.scss'],
  animations: [
    trigger('toggleOpen', [
      transition(':enter', [
        style({
          opacity: 0,
          'transform': 'translateY(-0.75rem)'
        }),
        animate('0.25s ease-in-out', style({
          opacity: 1,
          'transform': 'translateY(0rem)'
        }))
      ]),
      transition(':leave', [
        animate('0.25s ease-in-out', style({
          opacity: 0,
          'transform': 'translateY(-0.75rem)'
        }))
      ])
    ])
  ]
})
export class DrawerWidgetComponent {
  isOpen = new BehaviorSubject<boolean>(false);

  buttonText = combineLatest([
    this.widgetContext.widgetConfig,
    this.isOpen,
  ]).pipe(
    map(([config, isOpen]) => {
      if (isOpen) {
        return config.fields.labelWhenOpen || 'Show less'
      } else {
        return config.fields.labelWhenClosed || 'Show more';
      }
    })
  );

  readonly body = this.widgetContext.widgetConfig.pipe(
    map(config => config.fields.body)
  );

  constructor (private widgetContext: WidgetContext<DrawerWidgetConfig>) {}

  updateBodyConfig(bodyConfig: WidgetConfig) {
    this.widgetContext.updateWidgetConfig(c => c.fields.body = bodyConfig);
  }

  toggleDrawer() {
    this.isOpen.next(!this.isOpen.value);
  }
}
