import { WidgetConfig } from '../../widget-config';


export interface DrawerWidgetFields {
  body: WidgetConfig;
  labelWhenClosed?: string;
  labelWhenOpen?: string;
}


export type DrawerWidgetConfig = WidgetConfig<DrawerWidgetFields>;
