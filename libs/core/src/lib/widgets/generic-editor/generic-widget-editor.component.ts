import { Component } from '@angular/core';
import { TypedFormControl } from '@bmi/ui';
import { PluginMetadata, PluginMetadataField } from '@bmi/gravity-services'
import { ObjectMap } from '@bmi/utils';
import { WidgetConfig } from '../../widget-config';
import { WidgetContext } from '../../widget-context';


import { MappedFormSchemaForType, generatePluginMetadataFromSchema } from '../../forms/generic-editor-schema';


interface GenericWidgetEditorOptions<T extends WidgetConfig> {
  schema: MappedFormSchemaForType<T['fields']>;
}


@Component({
  template: `
    <bmi-plugin-form
      *ngIf="pluginMetadata"
      [pluginMetadata]="pluginMetadata"
      [formControl]="control"
      [defaultColumnWidth]="12">
    </bmi-plugin-form>
  `
})
export class GenericWidgetEditorComponent<TConfig extends WidgetConfig> {
  constructor(
    private context: WidgetContext
  ) {}

  control = new TypedFormControl<TConfig['fields']>(null);
  pluginMetadata: PluginMetadata;

  readonly options = (this.context.widgetDefinition.options || {}) as GenericWidgetEditorOptions<TConfig>
  readonly schema = this.options.schema;


  ngOnInit() {
    if (this.schema) {
      this.pluginMetadata = generatePluginMetadataFromSchema(this.schema);
    }

    this.context.widgetConfig.subscribe(config => this.control.setValue(config.fields, {emitEvent: false}));
    this.control.valueChanges.subscribe(value => this.context.updateWidgetConfig(c => c.fields = value));
  }
}

