import * as Highcharts from 'highcharts';
import { Component, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'bmi-core-gantt-widget',
  template: `
<div #chartContainer class="chart-container"></div>
  `,
})
export class TimelineWidgetComponent {

  @ViewChild('chartContainer', { static: true }) chartContainer: ElementRef;

  initHighcharts() {
    // *** requires gantt ***
    // Highcharts.chart(
    //   this.chartContainer.nativeElement as HTMLElement,
    //   Highcharts.ganttChart('container', {
    //     colors: ['#1e4471'],
    //     xAxis: [{
    //       labels: {
    //         format: '{value:%B}' // day of the week
    //       },
    //       grid: {
    //         enabled: true, // default setting
    //         tickInterval: 1000 * 60 * 60 * 24 * 365.25 / 12, // Q
    //       },
    //     }, {
    //       grid: {
    //         tickInterval: 1000 * 60 * 60 * 24 * 365.25, // FY
    //       },
    //       labels: {
    //         format: 'FY{value:%y}'
    //       },
    //     }],
    //     yAxis: [{
    //     }],
    //     series: [{
    //       name: 'D',
    //       data: [{
    //         id: 's',
    //         name: 'A',
    //         title: 'AA',
    //         start: Date.UTC(2019, 10, 18),
    //         end: Date.UTC(2019, 11, 4),
    //       }, {
    //         id: 'd',
    //         name: 'D',
    //         start: Date.UTC(2019, 11, 5),
    //         end: Date.UTC(2019, 11, 12),
    //       }, {
    //         id: 'a',
    //         name: 'B',
    //         start: Date.UTC(2019, 11, 13),
    //         end: Date.UTC(2020, 3, 26)
    //       }, {
    //         name: 'V',
    //         start: Date.UTC(2019, 11, 28),
    //         end: Date.UTC(2020, 4, 2),
    //       }, {
    //         name: 'F',
    //         start: Date.UTC(2020, 4, 3),
    //         end: Date.UTC(2020, 4, 15),
    //       }]
    //     }]
    //   }),
    // );
  }

}
