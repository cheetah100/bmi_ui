import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { WidgetContext } from '../../widget-context';
import { PageDataContext } from '../../page-data-context/page-data-context';
import { switchMap, tap } from 'rxjs/operators';
import { AlertWidgetConfig, OptionWithIcon, LEVELS } from './alert-widget-editor.component';


@Component({
  selector: 'bmi-alert-widget',
  template: `
<div class="alert alert--{{ level }} bmi-alert-widget">
  <div class="alert__icon {{ iconClass() }}"></div>
  <div class="alert__message">{{ content | async }}</div>
</div>
  `,
  styles: [`
div.bmi-alert-widget.alert {
  margin: 0;
}
  `],
})
export class AlertWidgetComponent implements OnInit {

  contentSubject = new BehaviorSubject('');
  content: Observable<string> = this.contentSubject.asObservable();
  private _level: string;
  get level(): string {
    return this._level;
  }

  constructor(
    private widgetContext: WidgetContext<AlertWidgetConfig>,
    private binder: PageDataContext,
  ) { }

  ngOnInit() {
    this.widgetContext.widgetConfig.pipe(
      tap(config => this._level = config.fields.level),
      switchMap(config => this.binder.bind(config.fields.template)),
    ).subscribe(x => this.contentSubject.next(x));
  }

  iconClass(): string {
    const option: OptionWithIcon = LEVELS.find(
      (level: OptionWithIcon) => level.value === this.level
    );
    if (!!option) {
      return option.icon;
    }
    return '';
  }

}
