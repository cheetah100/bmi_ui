import { Component, OnInit, OnDestroy } from '@angular/core';
import { Option } from '@bmi/utils';
import { WidgetConfig } from '../../widget-config';
import { ParameterBinding } from '../../formatter';
import { Subscription } from 'rxjs';
import { TypedFormGroup, TypedFormControl } from '@bmi/ui';
import { WidgetContext } from '../../widget-context';


export interface OptionWithIcon extends Option {
  icon: string;
}

export const LEVELS: OptionWithIcon[] = [
  { text: 'Info', value: 'info', icon: 'icon-info-outline' },
  { text: 'Warning', value: 'warning', icon: 'icon-warning-outline' },
  { text: 'Warning Alt', value: 'warning-alt', icon: 'icon-warning-outline' },
  { text: 'Success', value: 'success', icon: 'icon-check-outline' },
];

export type AlertWidgetConfig = WidgetConfig<AlertWidgetFields>;

export interface AlertWidgetFields {
  level: string;
  template: ParameterBinding;
}

@Component({
  template: `
<h2>Alert widget</h2>
<ng-container [formGroup]="form">
  <ui-form-field label="Alert level">
    <ui-select
      [options]="LEVELS"
      formControlName="level">
    </ui-select>
  </ui-form-field>
  <ui-form-field label="Content">
    <bmi-formatter-picker
      preferredType="template-string"
      formControlName="template">
    </bmi-formatter-picker>
  </ui-form-field>
</ng-container>
  `,
})
export class AlertWidgetEditorComponent implements OnInit, OnDestroy {

  private subscription = new Subscription();
  LEVELS = LEVELS;

  readonly form = new TypedFormGroup<AlertWidgetFields>({
    level: new TypedFormControl<string>('warning-alt'),
    template: new TypedFormControl<ParameterBinding>(null),
  });

  constructor(
    private widgetContext: WidgetContext,
  ) { }

  ngOnInit() {
    this.subscription.add(this.widgetContext.widgetConfig.subscribe(
      (config: WidgetConfig) => this.form.patchValue(config.fields, { emitEvent: false })
    ));
    this.subscription.add(this.form.valueChanges.subscribe(
      (updatedValue: AlertWidgetFields) => this.widgetContext.updateWidgetConfig(
        (config: AlertWidgetConfig) => config.fields = updatedValue
      )
    ));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
