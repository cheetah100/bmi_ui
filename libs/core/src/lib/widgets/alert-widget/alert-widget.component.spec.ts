import { TestElement } from '@angular/cdk/testing';
import { WidgetHarness } from '../testing/widget-harness';
import { AlertWidgetConfig } from './alert-widget-editor.component';
import { AlertWidgetComponent } from './alert-widget.component';
import { WidgetTestEnvironment } from '../testing/widget-test-environment';
import { WidgetTestOptions } from '../testing/widget-test-options';


class AlertWidgetHarness extends WidgetHarness {

  static hostSelector = 'bmi-alert-widget';

  async getAlertMessage(): Promise<string> {
    return this.getDisplayText('div.alert__message');
  }

  async getAlertElement(): Promise<TestElement> {
    const boxLocator = this.locatorFor('div.alert');
    return boxLocator();
  }

}

const testConfig = {
  widgetType: 'alert',
  fields: {
   level: 'warning-alt',
   template: 'Careful now'
  },
};
const envOptions: WidgetTestOptions<AlertWidgetConfig, AlertWidgetComponent> = {
  config: testConfig,
  componentType: AlertWidgetComponent,
}
let harness: AlertWidgetHarness;
let env: WidgetTestEnvironment<AlertWidgetConfig, AlertWidgetComponent>;

/**
 * This is a bit too much - I've been using this simple widget to develop the 
 * widget harness. That's why there are all these unimportant cases.
 */
describe('AlertWidget', () => {

  beforeEach(async () => {
    env = new WidgetTestEnvironment<AlertWidgetConfig, AlertWidgetComponent>(envOptions);
    harness = await env.getHarness(AlertWidgetHarness);
  });

  describe('alert color class', () => {
    it('should use the correct widget color class for each level', async () => {
      const alertElement = await harness.getAlertElement();
      expect(await alertElement.hasClass('alert--warning-alt')).toBe(true);
    });
    it('should update the widget color class with config changes', async () => {
      env.config = Object.assign({}, testConfig, { fields: { level: 'info' }})
      const alertElement = await harness.getAlertElement();
      expect(await alertElement.hasClass('alert--info')).toBe(true);
    })
  })

  describe('message display', () => {
    it('should display the correct message for plain text templates', async () => {
      const message: string = await harness.getAlertMessage();
      expect(message).toBe('Careful now');
    });
    it('should update message when config changes (plain text)', async () => {
      env.config = Object.assign({}, testConfig, {fields:{template: 'Never mind'}});
      const message: string = await harness.getAlertMessage();
      expect(message).toBe('Never mind');
    })
  });

  describe('message display with data binding', () => {

    it('should replace bound text variables in the message correctly', async () => {
      env.config = Object.assign({}, testConfig, {
        fields:{
          template: {
            bindingType: 'formatter',
            formatterType: "template-string",
            value: "Careful now {{a}}",
            data: {
              a: "then"
            }
          }
        }
      });
      const message: string = await harness.getAlertMessage();
      expect(message).toBe('Careful now then');
    });

    it('should replace bound text variables in the message correctly', async () => {
      env.config = Object.assign({}, testConfig, {
        fields: {
          template: {
            bindingType: 'formatter',
            formatterType: "template-string",
            value: "Careful now, {{a}}",
            data: {
              a: {
                bindingType: "filter",
                filterId: "someFilter"
              }
            }
          }
        }
      });
      env.setFilter({id: 'someFilter', rawValue: 'something is strange'});
      const message: string = await harness.getAlertMessage();
      expect(message).toBe('Careful now, something is strange');
    });
  });

  it('should replace variables from card sources in the display message', async () => {
    env.fixture.componentInstance.setContextConfig({
      mockCard: {
        type: "card",
        board: "test",
        orderBy: "id",
        descending: true,
        conditions: [
          {
            fieldName: "value",
            value: '7334',
            optional: false,
            operation: "EQUALTO"
          }
        ]
      }
    });
    env.config = Object.assign({}, testConfig, {
      fields: {
        template: {
          bindingType: 'formatter',
          formatterType: "template-string",
          value: "Careful now, {{a}}",
          data: {
            a: {
              bindingType: "data-path",
              path: "mockCard.name"
            }
          }
        }
      }
    });
    const message: string = await harness.getAlertMessage();
    expect(message).toBe('Careful now, Wayne');
  });

});
