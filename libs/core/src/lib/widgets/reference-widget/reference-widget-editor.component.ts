import { Component, OnInit, OnDestroy } from '@angular/core';
import { map, publishBehavior, refCount, debounceTime } from 'rxjs/operators';

import { Option } from '@bmi/utils';
import { TypedFormGroup, TypedFormControl } from '@bmi/ui';

import { PageEditorContext } from '../../page-editor/page-editor-context';
import { Subscription } from 'rxjs';
import { WidgetContext } from '../../widget-context';
import { WidgetConfig } from '../../widget-config';
import { ReferenceWidgetFields, ReferenceWidgetConfig } from './reference-widget.component';


@Component({
  template: `
<ui-form-field label="Select Widget" [formGroup]="form">
  <ui-single-select
    formControlName="widgetId"
    [options]="appWidgets | async"
    [floating]="true">
  </ui-single-select>
</ui-form-field>
  `
})
export class ReferenceWidgetEditorComponent implements OnInit, OnDestroy {

  private subscription = new Subscription();

  appWidgets = this.pageEditorContext.allWidgets.pipe(
    map(widgets => widgets.map(w => <Option>{ value: w.id, text: w.title || w.id })),
    publishBehavior<Option[]>([]),
    refCount()
  );

  readonly form = new TypedFormGroup({
    widgetId: new TypedFormControl<string>(''),
  });

  constructor(
    private pageEditorContext: PageEditorContext,
    private widgetContext: WidgetContext,
  ) { }

  ngOnInit() {
    this.subscription.add(this.widgetContext.widgetConfig.subscribe(
      (config: WidgetConfig) => this.form.patchValue(config.fields, { emitEvent: false })
    ));
    this.subscription.add(this.form.valueChanges.subscribe(
      (updatedValue: ReferenceWidgetFields) => this.widgetContext.updateWidgetConfig(
        (config: ReferenceWidgetConfig) => config.fields = updatedValue
      )
    ));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
