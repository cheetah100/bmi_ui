import { WidgetConfig } from '../../widget-config';
import { Component, OnInit } from '@angular/core';
import { WidgetRepositoryService } from '../../services/widget-repository.service';
import { PageInfoService } from '../../page/page-info.service';
import { WidgetContext } from '../../widget-context';
import { switchMap } from 'rxjs/operators';


export interface ReferenceWidgetFields {
  widgetId: string;
}

export type ReferenceWidgetConfig = WidgetConfig<ReferenceWidgetFields>;

@Component({
  template: `
<bmi-widget-host
  [config]="chartWidgetConfig"
  [updateFlairIcons]="false">
</bmi-widget-host>
  `,
})
export class ReferenceWidgetComponent implements OnInit {

  chartWidgetConfig: WidgetConfig;

  constructor(
    private pageInfoService: PageInfoService,
    private widgetContext: WidgetContext,
    private widgetRepositoryService: WidgetRepositoryService,
  ) { }

  ngOnInit() {
    this.widgetContext.widgetConfig.pipe(
      switchMap((config: ReferenceWidgetConfig) => {
        const widgetId = config.fields.widgetId;
        const moduleId = this.pageInfoService.currentModuleId;
        return this.widgetRepositoryService.getWidget(moduleId, widgetId);
      })
    ).subscribe(
      (config: WidgetConfig) => this.chartWidgetConfig = config
    );
  }

}
