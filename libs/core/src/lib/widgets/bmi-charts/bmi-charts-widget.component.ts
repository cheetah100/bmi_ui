import { Component, OnInit, OnDestroy } from '@angular/core';

import { combineLatest, Subscription, EMPTY } from 'rxjs';
import { catchError, distinctUntilChanged, finalize, map, switchMap, publishReplay, refCount, tap, filter } from 'rxjs/operators';

import lodashIsEqual from 'lodash-es/isEqual';

import { WidgetContext } from '../../widget-context';
import { WidgetConfig } from '../../widget-config';
import { FilterService } from '../../filters/filter.service'
import { FilterState } from '../../filters/filter';

import { ChartConfig, ChartData, ChartDataSourceService, ChartDataFilterCondition, ChartSettings } from '@bmi/legacy-charts';
import { ObjectMap } from '@bmi/utils';
import { cloneAndUpdate } from '../../util/clone-and-update';


type BmiChartWidgetConfig = WidgetConfig<ChartConfig>;

class BmiChartSettings implements ChartSettings {
  chartTitle?: string;
}

@Component({
  selector: 'bmi-core-bmi-charts-widget',
  templateUrl: './bmi-charts-widget.component.html',
  styleUrls: ['./bmi-charts-widget.component.scss']
})
export class BmiChartsWidgetComponent implements OnInit, OnDestroy {

  private subscription: Subscription;
  chartConfig: ChartConfig<BmiChartSettings>;
  chartData: ChartData[];

  isLoading = true;
  errorMessage: string = null;

  constructor(
    private widgetContext: WidgetContext<BmiChartWidgetConfig>,
    private filterService: FilterService,
    private chartDataSourceService: ChartDataSourceService,
  ) { }

  ngOnInit() {
    this.subscription = combineLatest([
      this.widgetContext.widgetConfig,
      this.filterService.state
    ]).pipe(
      filter(([config, filterState]) => !!config),
      map(([config, filterState]) => ({ config, chartFilters: this.prepareChartFilters(config, filterState) })),
      distinctUntilChanged((a, b) => lodashIsEqual(a, b)),
      switchMap(({ config, chartFilters }) => {
        this.isLoading = true;
        return this.chartDataSourceService.getChartData(config.fields.dataSources, chartFilters).pipe(
          catchError(() => {
            this.errorMessage = 'Error loading chart';
            return EMPTY
          }),
          map(chartData => ({ config, chartData }))
        );
      })
    ).subscribe(({ config, chartData }) => {
      this.isLoading = false;
      this.errorMessage = null;
      this.chartConfig = cloneAndUpdate(
        config.fields,
        (cfg: ChartConfig<BmiChartSettings>) => delete (cfg.chartSettings.chartTitle)
      );
      this.chartData = chartData;
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private prepareChartFilters(config: BmiChartWidgetConfig, filterState: ObjectMap<FilterState>): ChartDataFilterCondition[] {
    // This is looking at some details that probably aren't relevant to this
    // widget, but we also don't want to refresh the data when irrelevant
    // filters change. We should improve the APIs of our chart data services so
    // that we can change the filters afterwards (e.g. as inputs into an RxJS
    // pipeline) and get updated data only if necessary
    const relevantFilters = new Set<string>();
    config.fields.dataSources.forEach(ds => {
      if (ds.globalFilterFieldMap) {
        Object.keys(ds.globalFilterFieldMap).forEach(k => relevantFilters.add(k));
      }
    });

    return Array.from(relevantFilters.values())
      .map(filterId => filterState[filterId])

      // Don't include conditions for filters that aren't defined on this page,
      // or for filters with null values (that breaks things). This is still
      // very bodgy.
      .filter(f => !!f && f.rawValue)
      .map(f => <ChartDataFilterCondition>{
        fieldName: f.id,
        operation: 'EQUALTO',
        value: Array.isArray(f.value) ? f.value.join('|') : f.rawValue
      });
  }
}
