import { Component, OnInit, OnDestroy } from '@angular/core';
import { TemplatedFormArray, TypedFormGroup, TypedFormControl } from '@bmi/ui';

import { Subscription } from 'rxjs';

import { WidgetContext } from '../../widget-context';
import { FilterDefinition } from '../../filters/filter-definition';
import { FilterWidgetConfig, FilterControlFields } from './filter-controls-config';

@Component({
  selector: 'bmi-core-filter-controls-widget-editor',
  templateUrl: './filter-controls-widget-editor.component.html',
  styleUrls: ['./filter-controls-widget-editor.component.scss']
})
export class FilterControlsWidgetEditorComponent implements OnInit, OnDestroy {
  readonly form = new TypedFormGroup({
    columns: new TypedFormControl(4),
    filters: new TemplatedFormArray<FilterDefinition>(def => new TypedFormControl(def))
  });

  private subscriptions: Subscription[];

  get filtersArray() {
    return this.form.controls.filters as TemplatedFormArray<FilterDefinition>;
  }

  constructor(
    private widgetContext: WidgetContext<FilterWidgetConfig>
  ) {}

  ngOnInit() {
    this.subscriptions = [
      this.widgetContext.widgetConfig.subscribe(newConfig => {
        this.form.patchValue(newConfig.fields, {emitEvent: false});
      }),

      this.form.valueChanges.subscribe(updatedValue => {
        this.widgetContext.updateWidgetConfig(config => config.fields = updatedValue);
      })
    ];
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  addFilter() {
    this.filtersArray.pushValue({
      id: '',
      name: '',
      filterType: 'optionlist',
      controlType: 'single-select',
      config: {
        optionlist: null
      }
    });
  }
}
