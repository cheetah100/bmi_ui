import { WidgetConfig } from '../../widget-config';
import { FilterDefinition } from '../../filters/filter-definition';

export type FilterWidgetConfig = WidgetConfig<FilterControlFields>;


export interface FilterControlFields {
  columns?: number;
  filters: FilterDefinition[];
}

