import { Component, Input, OnInit, OnDestroy } from '@angular/core';

import { distinctUntilChanged, throttleTime, withLatestFrom } from 'rxjs/operators';
import lodashIsEqual from 'lodash-es/isEqual';

import { WidgetContext } from '../../widget-context';
import { Filter } from '../../filters/filter';
import { FilterService } from '../../filters/filter.service';
import { FilterWidgetConfig } from './filter-controls-config';
import { FilterDefinition } from '../../filters/filter-definition';
import { FilterFactoryService } from '../../filters/filter-factory.service';

import { StringFilter } from '../../filters/string-filter';

import { createMapFrom } from '@bmi/utils';


interface FilterItem {
  id: string;
  filter: Filter;
  definition: FilterDefinition;
}


@Component({
  selector: 'bmi-core-filter-controls-widget',
  templateUrl: './filter-controls-widget.component.html',
  styleUrls: ['./filter-controls-widget.component.scss']
})
export class FilterControlsWidgetComponent implements OnInit, OnDestroy {
  activeFilters: FilterItem[] = [];

  columns = 4;

  isInEditMode = this.widgetContext.isEditMode;
  isInsideEditor = this.widgetContext.isInsideEditor;

  constructor(
    private widgetContext: WidgetContext<FilterWidgetConfig>,
    private filterService: FilterService,
    private filterFactoryService: FilterFactoryService
  ) {}

  ngOnInit() {
    // This is the lazy strategy. When the configs change we'll delete and
    // recreate the filters. This widget creates the Filter object based on the
    // config and registers these with the FilterService instance.
    //
    // A better solution here would be to only replace the filter objects that
    // are needed, and to try and preserve the filter state.
    this.widgetContext.widgetConfig.pipe(
      throttleTime(500, undefined, {leading: true, trailing: true}),
      distinctUntilChanged((a, b) => lodashIsEqual(a, b)),
    ).subscribe((widgetConfig) => {
      // We'll make an effort to preserve filter state across instances here, by
      // capturing the raw values of the old filters.
      const existingFilters = createMapFrom(this.activeFilters, f => f.id);
      this.cleanupFilters();
      this.columns = widgetConfig.fields.columns || 4;
      widgetConfig.fields.filters.forEach(def => {
        const existing = existingFilters.get(def.id);
        const initialValue = existing ? existing.filter.snapshot.rawValue : def.defaultValue;
        const filter = this.makeFilter(def);

        if (!filter) {
          return;
        }

        if (initialValue) {
          filter.set(initialValue);
        }

        this.activeFilters.push({
          id: def.id,
          definition: def,
          filter: filter
        });
      });

      this.filterService.addFilter(this.activeFilters.map(f => f.filter));
    });
  }

  ngOnDestroy() {
    this.cleanupFilters();
  }

  cleanupFilters() {
    this.filterService.removeFilter(this.activeFilters.map(f => f.filter));
    this.activeFilters = [];
  }

  trackFilterControl(index: number, filterItem: FilterItem) {
    return filterItem.id || index;
  }

  makeFilter(def: FilterDefinition): Filter {
    try {
      return this.filterFactoryService.createFilter(def);
    } catch (error) {
      console.error('Filter creation error: ', error, def);

      // We can still create a placeholder filter though. If this behaviour
      // causes problems, returning a falsy value instead will prevent the
      // filter from being created.
      if (def.id) {
        return new StringFilter(def.id, def.name);
      }
    }
  }
}
