import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { TypedFormGroup, TypedFormControl } from '@bmi/ui';
import { WidgetContext } from '../../widget-context';
import { MetricGaugeWidgetFields, MetricGaugeWidgetConfig, DisplaySize } from './metric-gauge-widget.component';
import { Option } from '@bmi/utils';
import { ParameterBinding } from '../../page-data-context/parameter-binding';

@Component({
  templateUrl: './metric-gauge-widget-editor.component.html',
  styles: [`
ui-form-field {
  margin-bottom: var(--gra-spacing-half);
}
  `],
})
export class MetricGaugeWidgetEditorComponent implements OnInit, OnDestroy {

  readonly sizeOptions: Option[] = [
    { text: 'Small', value: 'small' },
    { text: 'Medium', value: 'medium' },
    { text: 'Large', value: 'large' },
  ];
  private readonly subscription = new Subscription();
  readonly form = new TypedFormGroup<MetricGaugeWidgetFields>({
    size: new TypedFormControl<DisplaySize>('medium'),
    color: new TypedFormControl<ParameterBinding>('#00bceb'),
    metricType: new TypedFormControl<ParameterBinding>(null),
    metricValue: new TypedFormControl<ParameterBinding>(null),
    metricDataCardSource: new TypedFormControl<string>(null),
    useLink: new TypedFormControl<boolean>(false),
    linkText: new TypedFormControl<ParameterBinding>(null),
    subHeading: new TypedFormControl<ParameterBinding>(null),
  });


  constructor(
    private widgetContext: WidgetContext<MetricGaugeWidgetConfig>,
  ) { }

  ngOnInit() {
    this.subscription.add(this.widgetContext.widgetConfig.subscribe((config: MetricGaugeWidgetConfig) => {
      this.form.patchValue(config.fields, { emitEvent: false });
    }));

    this.subscription.add(this.form.valueChanges.subscribe(updatedValue => {
      this.widgetContext.updateWidgetConfig(c => c.fields = updatedValue);
    }));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}