import { Component, OnDestroy, OnInit } from '@angular/core';
import { config, Observable, Subscription } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { WidgetWrapperContext } from '../../components/widget-wrapper/widget-wrap';
import { FlairIcon } from '../../flair/flair';
import { FlairContextService } from '../../flair/flair-context.service';
import { BmiRoute } from '../../navigation/bmi-route';
import { PageDataContext } from '../../page-data-context/page-data-context';
import { ParameterBinding } from '../../page-data-context/parameter-binding';
import { WidgetConfig } from '../../widget-config';
import { WidgetContext } from '../../widget-context';


export interface MetricGaugeWidgetFields {
  metricDataCardSource: string;
  metricValue: ParameterBinding;
  metricType: ParameterBinding;
  size: DisplaySize;
  subHeading: ParameterBinding;
  color: ParameterBinding;
  useLink: boolean;
  linkText: ParameterBinding;
}
export type DisplaySize = 'small' | 'medium' | 'large';
export type MetricGaugeWidgetConfig = WidgetConfig<MetricGaugeWidgetFields>;
export type UnitType = 'percentage_0_to_100' | 'decimal' | 'currency_USD';

export const displayBigNumber = (n: number): {display: string, unit: string } => {
  if ((!n || isNaN(n)) && n !== 0) {
    return ({ display: '', unit: '' })
  }
  if (Math.abs(n/1e12) >= 0.99995) {
    return ({ display: (n/1e12).toFixed(1), unit: 'T' });
  }
  if (Math.abs(n/1e9) >= 0.99995) {
    return ({ display: (n/1e9).toFixed(1), unit: 'B' });
  }
  if (Math.abs(n/1e6) >= 0.99995) {
    return ({ display: (n/1e6).toFixed(1), unit: 'M' });
  }
  if (Math.abs(n/1e3) >= 0.9995) {
    return ({ display: (n/1e3).toFixed(1), unit: 'K' });
  }
  return ({ display: n.toFixed(0), unit: ''});
}

@Component({
  templateUrl: './metric-gauge-widget.component.html',
  styleUrls: [
    './metric-gauge-widget.component.scss',
    '../infobox/cui-gauge-overrides.scss',
  ],
})
export class MetricGaugeWidgetComponent implements OnInit, OnDestroy {

  gaugeConfig: WidgetConfig;
  title: Observable<string>;
  icons: Observable<FlairIcon[]> = null;
  gaugeSize = 'medium';
  quarter: string = null;
  private subscription = new Subscription();
  color: Observable<string>;
  value: Observable<number>;
  // required for CUI styling - whole numbers only
  get roundedValue(): Observable<number> {
    return this.value.pipe(map(v => Math.round(v)));
  }
  type: Observable<string>;
  route: Observable<BmiRoute>;
  linkText: Observable<string>;
  subHeading: Observable<string>;

  constructor(
    private widgetContext: WidgetContext,
    private binder: PageDataContext,
    private widgetWrapperContext: WidgetWrapperContext,
    private flairContextService: FlairContextService,
  ) { }

  ngOnInit() {

    this.subscription.add(
      this.widgetContext.widgetConfig.subscribe(
        (config: MetricGaugeWidgetConfig) => {
          this.gaugeConfig = config;
          this.gaugeSize = config.fields.size;
          this.value = this.binder.bind(config.fields.metricValue);
          this.type = this.binder.bind(config.fields.metricType);
          this.linkText = this.binder.bind(config.fields.linkText);
          this.subHeading = this.binder.bind(config.fields.subHeading);
          this.color = this.binder.bind(config.fields.color);
        }
      )
    );

    this.subscription.add(
      this.widgetContext.widgetConfig.pipe(
        switchMap((config: MetricGaugeWidgetConfig) => this.binder.bind({
          bindingType: 'data-path',
          path: `${config.fields.metricDataCardSource}.quarter`,
        }))
      ).subscribe((quarter: string) => this.quarter = quarter)
    );

    this.icons = this.flairContextService.icons;
    this.title = this.widgetWrapperContext.title;
    this.route = this.widgetWrapperContext.route;

  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }


  displayNumber(n): { display: string, unit: string } {
    return displayBigNumber(n);
  }

}
