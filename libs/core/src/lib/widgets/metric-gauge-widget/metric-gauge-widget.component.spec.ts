import { displayBigNumber } from './metric-gauge-widget.component';

describe('displayBigNumber()', () => {
  
  let display: string;
  let unit: string;
  
  it('should return empty string values for falsy numbers', () => {

    ({ display, unit } = displayBigNumber(undefined));
    expect(display).toBe('');
    expect(unit).toBe('');

    ({ display, unit } = displayBigNumber(null));
    expect(display).toBe('');
    expect(unit).toBe('');

    ({ display, unit } = displayBigNumber(NaN));
    expect(display).toBe('');
    expect(unit).toBe('');

  });

  it('should return the original number with no units if less in magnitude than 1000 when rounded', () => {

    ({ display, unit } = displayBigNumber(-990));
    expect(display).toBe('-990');
    expect(unit).toBe('');

    ({ display, unit } = displayBigNumber(-1));
    expect(display).toBe('-1');
    expect(unit).toBe('');
  
    ({ display, unit } = displayBigNumber(0));
    expect(display).toBe('0');
    expect(unit).toBe('');

    ({ display, unit } = displayBigNumber(1));
    expect(display).toBe('1');
    expect(unit).toBe('');
    
    ({ display, unit } = displayBigNumber(999.4));
    expect(display).toBe('999');
    expect(unit).toBe('');

  });

  it('should return the modified number with correct units if at least magnitude 1000 when rounded', () => {

    ({ display, unit } = displayBigNumber(-301621111111111));
    expect(display).toBe('-301.6');
    expect(unit).toBe('T');

    ({ display, unit } = displayBigNumber(-22900000000));
    expect(display).toBe('-22.9');
    expect(unit).toBe('B');

    ({ display, unit } = displayBigNumber(-1000001));
    expect(display).toBe('-1.0');
    expect(unit).toBe('M');

    ({ display, unit } = displayBigNumber(-1000000));
    expect(display).toBe('-1.0');
    expect(unit).toBe('M');

    ({ display, unit } = displayBigNumber(-999999));
    expect(display).toBe('-1.0');
    expect(unit).toBe('M');

    ({ display, unit } = displayBigNumber(-1000));
    expect(display).toBe('-1.0');
    expect(unit).toBe('K');

    ({ display, unit } = displayBigNumber(999.5));
    expect(display).toBe('1.0');
    expect(unit).toBe('K');

    ({ display, unit } = displayBigNumber(1000));
    expect(display).toBe('1.0');
    expect(unit).toBe('K');

    ({ display, unit } = displayBigNumber(1361));
    expect(display).toBe('1.4');
    expect(unit).toBe('K');

    ({ display, unit } = displayBigNumber(998666));
    expect(display).toBe('998.7');
    expect(unit).toBe('K');

    ({ display, unit } = displayBigNumber(999940));
    expect(display).toBe('999.9');
    expect(unit).toBe('K');

    ({ display, unit } = displayBigNumber(999950));
    expect(display).toBe('1.0');
    expect(unit).toBe('M');

    ({ display, unit } = displayBigNumber(12345678));
    expect(display).toBe('12.3');
    expect(unit).toBe('M');

    ({ display, unit } = displayBigNumber(12345678910));
    expect(display).toBe('12.3');
    expect(unit).toBe('B');

    ({ display, unit } = displayBigNumber(98765678910999));
    expect(display).toBe('98.8');
    expect(unit).toBe('T');
    
  });

})