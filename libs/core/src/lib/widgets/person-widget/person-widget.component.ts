import { Component, OnDestroy, OnInit } from '@angular/core';
import { combineLatest, Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { PageDataContext } from '../../page-data-context/page-data-context';
import { WidgetContext } from '../../widget-context';
import { PersonWidgetConfig } from './person-widget-editor.component';

@Component({
  template: `
<bmi-person
  [userId]="userId"
  [name]="name"
  [subHeading]="subHeading">
</bmi-person>
  `,
})
export class PersonWidgetComponent implements OnInit, OnDestroy {

  userId: string;
  name: string;
  subHeading: string = null;
  private subscription = new Subscription();

  constructor(
    private widgetContext: WidgetContext,
    private binder: PageDataContext,
  ) { }

  ngOnInit() {
    this.subscription.add(this.widgetContext.widgetConfig.pipe(
      switchMap((config: PersonWidgetConfig) => combineLatest([
        this.binder.bind(config.fields.userId),
        this.binder.bind(config.fields.name),
        this.binder.bind(config.fields.subHeading),
      ])),
    ).subscribe(([id, name, sub]: string[]) => {
      this.userId = id;
      this.name = name;
      this.subHeading = sub;
    }));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
