import { WidgetHarness } from '../testing/widget-harness';
import { WidgetTestEnvironment } from '../testing/widget-test-environment';
import { WidgetTestOptions } from "../testing/widget-test-options";
import { PersonWidgetConfig } from './person-widget-editor.component';
import { PersonWidgetComponent } from './person-widget.component';

const envOptions: WidgetTestOptions<PersonWidgetConfig, PersonWidgetComponent> = {
  config: {
    widgetType: 'person',
    fields: {
      "cecId": {
        "bindingType": "data-path",
        "path": "card.name"
      },
      "name": {
        "bindingType": "data-path",
        "path": "card.name"
      },
      "subHeading": "Strange odour"
    },
  },
  componentType: PersonWidgetComponent,
  dataContextConfig: {
    card: {
      type: 'card',
      board: 'test',
      conditions: [{
        fieldName: "value",
        value: '7334',
        optional: false,
        operation: "EQUALTO"
      }]
    }
  }
}
let harness: WidgetHarness;
let env: WidgetTestEnvironment<PersonWidgetConfig, PersonWidgetComponent>;

describe('PersonWidget', () => {

  beforeEach(async () => {
    env = new WidgetTestEnvironment<PersonWidgetConfig, PersonWidgetComponent>(envOptions);
    harness = await env.getHarness(WidgetHarness);
  });

  afterEach(async () => {
    env.fixture.destroy();
  });

  describe('Widget display', () => {
    it('should populate heading and sub-heading correctly', async () => {
      const heading = await harness.getDisplayText('h4');
      const subHeading = await harness.getDisplayText('.profile-image__info__sub-heading');
      expect(heading).toBe('Wayne');
      expect(subHeading).toBe('Strange odour');
    });
    it('should populate the image cecId property correctly', () => {
      expect(env.component.cecId).toBe('Wayne');
    })
  });

});
