import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { TypedFormGroup, TypedFormControl } from '@bmi/ui';
import { WidgetContext } from '../../widget-context';
import { WidgetConfig } from '../../widget-config';
import { ParameterBinding } from '../../formatter';

export type PersonWidgetConfig = WidgetConfig<PersonWidgetFields>;

export interface PersonWidgetFields {
  userId: ParameterBinding;
  name: ParameterBinding;
  subHeading?: ParameterBinding;
}

@Component({
  template: `
<ng-container [formGroup]="form">
  <ui-form-field label="USERID">
    <bmi-formatter-picker
      preferredType="data-binding"
      formControlName="userId">
    </bmi-formatter-picker>
  </ui-form-field>
  <ui-form-field label="Name">
    <bmi-formatter-picker
      preferredType="data-binding"
      formControlName="name">
    </bmi-formatter-picker>
  </ui-form-field>
  <ui-form-field label="Sub heading">
    <bmi-formatter-picker
      preferredType="text"
      formControlName="subHeading">
    </bmi-formatter-picker>
  </ui-form-field>
</ng-container>
  `
})
export class PersonWidgetEditorComponent implements OnInit, OnDestroy {

  private subscription = new Subscription();

  readonly form = new TypedFormGroup<PersonWidgetFields>({
    userId: new TypedFormControl<ParameterBinding>(null),
    name: new TypedFormControl<ParameterBinding>(null),
    subHeading: new TypedFormControl<ParameterBinding>(null),
  });

  constructor(
    private widgetContext: WidgetContext,
  ) { }

  ngOnInit() {
    this.subscription.add(this.widgetContext.widgetConfig.subscribe(
      (config: WidgetConfig) => this.form.patchValue(config.fields, { emitEvent: false })
    ));
    this.subscription.add(this.form.valueChanges.subscribe(
      (updatedValue: PersonWidgetFields) => this.widgetContext.updateWidgetConfig(
        (config: PersonWidgetConfig) => config.fields = updatedValue
      )
    ));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
