import { ParameterBinding } from '../../page-data-context/parameter-binding';
import { WidgetConfig } from '../../widget-config';


export interface TableWidgetColumn<TContentType extends TableColumnOptions = TableColumnOptions> {
  title?: string;
  width?: string;
  mergeRows?: boolean;
  content: TContentType;
}


export interface TableColumnOptions {
  type: string;
  [key: string]: any;
}


export interface EditabilityOptions {
  add: boolean;
  edit: boolean;
  delete: boolean;

  actionsColumnTitle?: string;

  /** The board to use when editing items */
  boardId?: string;

  /** The card ID binding */
  cardId?: ParameterBinding;
}


export interface TableWidgetFields {
  /**
   * The data source to use for this table.
   */
  dataSourceId: string;

  hasPagination: boolean;
  rowsPerPage?: number;
  hasSearch: boolean;

  columns: TableWidgetColumn[];

  isEditable?: boolean;
  editOptions?: EditabilityOptions;
}


export type TableWidgetConfig = WidgetConfig<TableWidgetFields>;
