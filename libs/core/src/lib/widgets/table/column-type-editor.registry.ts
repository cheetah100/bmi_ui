import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DynamicEditorDefinition, registerDynamicEditor, SharedUiModule } from '@bmi/ui';
import { pathBinding } from '../../data-binding/utils';
import { BmiCoreEditorComponentsModule } from '../../editor-components/bmi-core-editor-components.module';
import { ActionButtonTableColumnOptions } from './action-button-column/action-button-column-config';
import { ActionButtonColumnEditorComponent } from './action-button-column/action-button-column-editor.component';
import { DefaultColumnOptions } from './default-table-column/default-column-config';
import { DefaultTableColumnEditorComponent } from './default-table-column/default-table-column-editor.component';
import { TableColumnOptions } from './table-config';
import { TimelineColumnOptions } from './timeline-column/timeline-column-config';
import { TimelineColumnEditorComponent } from './timeline-column/timeline-column-editor.component';


function registerTableColumnEditor<TConfig extends TableColumnOptions>(def: DynamicEditorDefinition<TConfig>) {
  return registerDynamicEditor('table-column', def);
}


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedUiModule,
    BmiCoreEditorComponentsModule,
  ],

  declarations: [
    TimelineColumnEditorComponent,
    DefaultTableColumnEditorComponent,
    ActionButtonColumnEditorComponent,
  ],

  providers: [
    registerTableColumnEditor<DefaultColumnOptions>({
      type: 'default',
      name: 'Regular',
      sortWeight: -1,
      component: DefaultTableColumnEditorComponent,
      defaultConfig: {
        value: pathBinding('this.id'),
        route: null,
      }
    }),

    registerTableColumnEditor<ActionButtonTableColumnOptions>({
      type: 'action-buttons',
      name: 'Action buttons',
      component: ActionButtonColumnEditorComponent,
      defaultConfig: {
        buttons: [
          {type: 'action', icon: 'cui-info', text: 'Example', onInvoke: []}
        ]
      }
    }),

    registerTableColumnEditor<TimelineColumnOptions>({
      type: 'timeline',
      name: 'Timeline',
      component: TimelineColumnEditorComponent,
      defaultConfig: {
        startQuarterId: '20211',
        endQuarterId: '20214',
      },
    })
  ]
})
export class ColumnTypeEditorRegistry {}
