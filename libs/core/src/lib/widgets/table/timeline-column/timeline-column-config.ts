import { ParameterBinding } from '../../../page-data-context/parameter-binding';
import { TableColumnOptions } from '../table-config';


export interface TimelineColumnOptions extends TableColumnOptions {
  startQuarterId?: string;
  endQuarterId?: string;

  itemStartDate?: ParameterBinding;
  itemEndDate?: ParameterBinding;
  itemName?: ParameterBinding;
}
