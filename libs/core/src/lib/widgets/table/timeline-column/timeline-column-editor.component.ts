import { Component, OnInit } from '@angular/core';
import { DynamicEditorContext, TypedFormControl, TypedFormGroup } from '@bmi/ui';
import { OptionlistService } from '../../../services/optionlist.service';
import { TimelineColumnOptions } from './timeline-column-config';


@Component({
  template: `
    <ng-container [formGroup]="form">
      <div class="table-column table-column__common">
        <ui-form-field label="Timeline Start Quarter">
          <ui-select [optionGroups]="quarters | async" formControlName="startQuarterId"></ui-select>
        </ui-form-field>

        <ui-form-field label="Timeline End Quarter">
        <ui-select [optionGroups]="quarters | async" formControlName="endQuarterId"></ui-select>
        </ui-form-field>

        <ui-form-field label="Item Start Date">
          <bmi-binding-picker formControlName="itemStartDate"></bmi-binding-picker>
        </ui-form-field>

        <ui-form-field label="Item End Date">
          <bmi-binding-picker formControlName="itemEndDate"></bmi-binding-picker>
        </ui-form-field>
      </div>
      <ui-form-field label="Item Name">
        <bmi-binding-picker formControlName="itemName"></bmi-binding-picker>
      </ui-form-field>
    </ng-container>
  `
})
export class TimelineColumnEditorComponent implements OnInit {
  constructor(
    private context: DynamicEditorContext<TimelineColumnOptions>,
    private optionlistService: OptionlistService,
  ) {}

  form = new TypedFormGroup<TimelineColumnOptions>({
    type: new TypedFormControl(),
    startQuarterId: new TypedFormControl(),
    endQuarterId: new TypedFormControl(),
    itemStartDate: new TypedFormControl(),
    itemEndDate: new TypedFormControl(),
    itemName: new TypedFormControl()
  });

  quarters = this.optionlistService.getOptionGroups('quarter');

  ngOnInit() {
    this.context.config.subscribe(config => this.form.patchValue(config, {emitEvent: false}));
    this.form.valueChanges.subscribe(config => this.context.setConfig(config));
  }
}
