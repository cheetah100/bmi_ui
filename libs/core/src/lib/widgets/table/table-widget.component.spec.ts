import {
  DATA_CONFIG_DEFAULT_CARDS,
  DEFAULT_TEST_BOARDS,
  WidgetTestOptions,
} from '../testing/widget-test-options';
import { TableWidgetColumn, TableWidgetConfig } from './table-config';
import { TableWidgetComponent } from './table-widget.component';
import { WidgetHarness } from '../testing/widget-harness';
import { WidgetTestEnvironment } from '../testing/widget-test-environment';
import { cloneDeep } from 'lodash-es';
import { CardEditorModalService, pathBinding } from '@bmi/core';
import { TestBed } from '@angular/core/testing';
import { BoardService, CardCacheService, GravityTimelineService, Quarter } from '@bmi/gravity-services';
import { ConfirmationModalService } from '@bmi/ui';
import { MatDialog } from '@angular/material/dialog';
import { Observable, of } from 'rxjs';
import { TimelineColumnOptions } from './timeline-column/timeline-column-config';


const defaultConfig: TableWidgetConfig = {
  widgetType: 'table-v2',
  fields: {
    hasPagination: true,
    rowsPerPage: 8,
    dataSourceId: undefined,
    hasSearch: false,
    isEditable: false,
    editOptions: {
      add: false,
      edit: false,
      delete: false,
    },
    columns: [
      {
        title: 'Pillar Title',
        width: '1fr',
        mergeRows: null,
        content: {
          type: 'default',
          value: pathBinding('this.value')
        }
      },
      {
        title: 'Name',
        width: '1fr',
        mergeRows: null,
        content: {
          type: 'default',
          value: pathBinding('this.name'),
        },
      },
    ],
  },
  // 'flairs': null
};

const TIMELINE_COLUMN_TEMPLATE: TableWidgetColumn<TimelineColumnOptions> = {
  title: 'Timeline',
  width: '4fr',
  mergeRows: false,
  content: {
    type: 'timeline',
    startQuarterId: '20202',
    endQuarterId: '20203',
    itemStartDate: pathBinding('this.planned_start_date'),
    itemEndDate: pathBinding('this.planned_end_date'),
    itemName: null
  }
};
const envOptions: WidgetTestOptions<TableWidgetConfig, TableWidgetComponent> = {
  config: defaultConfig,
  componentType: TableWidgetComponent,
  dataContextConfig: DATA_CONFIG_DEFAULT_CARDS,
};

export class DialogMock {
  afterClosedResponse: any;

  open() {
    return {
      afterClosed: () => {
        return of(this.afterClosedResponse ? this.afterClosedResponse : {});
      },
    };
  }
}
const dialogMock = new DialogMock();

export class GravityTimelineServiceMock {
  quarters: Observable<Quarter[]> = of([]);

  getAllQuarters(): Observable<Quarter[]> {
    return this.quarters;
  }
  getAllQuartersInThisRange(endQuarterId: String, startQuarterId: String): Observable<Quarter[]> {
    return of([]);
  }
}

const gravityTimelineServiceMock = new GravityTimelineServiceMock();

let harness: WidgetHarness;
let env: WidgetTestEnvironment<TableWidgetConfig, TableWidgetComponent>;
let widgetComponent;
const numberOfDataRows = DEFAULT_TEST_BOARDS[0].cards.length;
let numberOfColumns = defaultConfig.fields.columns.length;

describe('TableWidget', () => {
  beforeEach(async () => {
    // Add Mock Dialog Provider
    envOptions.override = (TestBed) => {
      TestBed.overrideProvider(MatDialog, { useValue: dialogMock });
      TestBed.overrideProvider(GravityTimelineService, { useValue: gravityTimelineServiceMock });

    };

    env = new WidgetTestEnvironment<TableWidgetConfig, TableWidgetComponent>(
      envOptions
    );
    harness = await env.getHarness(WidgetHarness);
    env.fixture.detectChanges();
    widgetComponent = await env.component;
  });

  afterEach(async () => {
    env.fixture.destroy();
    env = null;
  });

  describe('Display Empty Grid', () => {
    it('should show an empty table', async () => {
      env.fixture.detectChanges();
      env.viewMode = 'view';
      const rows = await harness.getDisplayTexts('.body-cell');
      expect(rows.length).toBe(0);
    });
  });

  describe('Show Data', () => {
    const expectedColumnData = [
      '9001',
      'Dog',
      '7334',
      'Wayne',
      '0',
      'Beelzebub',
    ];
    beforeEach(() => {
      const config = cloneDeep(defaultConfig);
      config.fields.dataSourceId = 'cards';
      env.config = config;
    });

    it('should show all rows of data ', async () => {
      const cells = await harness.getDisplayTexts('.body-cell');
      expect(cells.length).toBe(2 * numberOfDataRows);
      expect(cells).toEqual(expectedColumnData);
    });

    it('should show column headings', async () => {
      const headings = await harness.getDisplayTexts('.head-cell');
      expect(headings.length).toBe(numberOfColumns);
      expect(headings).toEqual(['Pillar Title', 'Name']);
    });
  });

  describe('Test search function', () => {
    beforeEach(async () => {
      const config = cloneDeep(defaultConfig);
      /*
        The search control will only become active when a datasource is specified
       */
      config.fields.hasSearch = true;
      config.fields.dataSourceId = 'cards';
      env.config = config;
      env.fixture.detectChanges();
      widgetComponent = await env.component;
    });

    it('should show search control when search is enabled', () => {
      expect(widgetComponent.searchEnabled).toBeTruthy();
      const nativeElement = env.fixture.nativeElement;
      const searcher = nativeElement.querySelector('ui-search-input');
      expect(searcher).toBeTruthy();
      const attributes = searcher.attributes as NamedNodeMap;
      expect(attributes).toBeTruthy();
      const placeHolder = attributes.getNamedItem('placeholder');
    });

    it('should filter rows when search criteria has been entered', async () => {
      widgetComponent.updateSearch('Wayne');
      env.fixture.detectChanges();

      const cells = await harness.getDisplayTexts('.body-cell');
      expect(cells.length).toBe(2);
      expect(cells).toEqual(['7334', 'Wayne']);
    });
  });

  describe('Test Add Action', () => {
    const addButtonSelector = '.table-control .table-control-item';
    const addButtonLabel = 'Add Card';
    beforeEach(async () => {
      const config = cloneDeep(defaultConfig);
      config.fields.isEditable = true;
      config.fields.editOptions.add = true;
      config.fields.dataSourceId = 'cards';
      env.config = config;
      env.fixture.detectChanges();
      widgetComponent = await env.component;
    });

    it('should Display the Add button when configured', async () => {
      const addButton = await harness.getDisplayTexts(addButtonSelector);
      expect(addButton).toEqual([addButtonLabel]);
    });

    it('should Open a modal when clicking the add button and close when cancel button clicked', async () => {
      dialogMock.afterClosedResponse = { action: 'cancel', data: undefined };
      const cardCacheService = TestBed.inject(CardCacheService);
      spyOn(widgetComponent, 'createCard').and.callThrough();
      spyOn(cardCacheService, 'refreshBoard');
      const addButton = env.nativeElementbyCss(addButtonSelector);
      addButton.click();
      env.fixture.detectChanges();
      expect(widgetComponent.createCard).toHaveBeenCalled();
      expect(cardCacheService.refreshBoard).not.toHaveBeenCalled();
    });

    it('should Open a modal when clicking the add button and close when save button clicked', async () => {
      dialogMock.afterClosedResponse = { action: 'yes' };
      const cardCacheService = TestBed.inject(CardCacheService);
      spyOn(widgetComponent, 'createCard').and.callThrough();
      spyOn(cardCacheService, 'refreshBoard');
      const addButton = env.nativeElementbyCss(addButtonSelector);
      addButton.click();
      env.fixture.detectChanges();
      expect(widgetComponent.createCard).toHaveBeenCalled();
      expect(cardCacheService.refreshBoard).toHaveBeenCalled();
    });
  });

  describe('Test Edit Action', () => {
    const editButtonsSelector = '.ui-icon.ui-icon-edit';
    beforeEach(async () => {
      const config = cloneDeep(defaultConfig);
      config.fields.isEditable = true;
      config.fields.editOptions.edit = true;
      config.fields.dataSourceId = 'cards';
      env.config = config;
      env.fixture.detectChanges();
      widgetComponent = await env.component;
    });
    it('should add a table column with an edit button', async () => {
      const headings = await harness.getDisplayTexts('.head-cell');
      expect(headings.length).toBe(numberOfColumns + 1);
      const editButtons = env.debugElementsbyCss(editButtonsSelector);
      expect(editButtons.length).toBe(numberOfDataRows);
      const editButton = editButtons[0].nativeElement;
      editButton.click();
      env.fixture.detectChanges();
    });

    it('should open a card edit form when clicking the edit button', () => {
      const service = TestBed.inject(CardEditorModalService);
      spyOn(service, 'editCard').and.callThrough();
      const editButtons = env.debugElementsbyCss(editButtonsSelector);
      expect(editButtons.length).toBe(numberOfDataRows);

      const editButton = editButtons[0].nativeElement;
      editButton.click();
      env.fixture.detectChanges();
      expect(service.editCard).toHaveBeenCalled();
    });
  });

  describe('Test Delete Action', () => {
    const deleteButtonsSelector = '.ui-icon.ui-icon-trash';
    let deleteButtons;
    let deleteButton;
    beforeEach(async () => {
      const config = cloneDeep(defaultConfig);
      config.fields.isEditable = true;
      config.fields.editOptions.delete = true;
      config.fields.dataSourceId = 'cards';
      env.config = config;
      env.fixture.detectChanges();
      widgetComponent = await env.component;
    });
    it('should Add a table column with a delete button', async () => {
      const headings = await harness.getDisplayTexts('.head-cell');
      expect(headings.length).toBe(numberOfColumns + 1);

      deleteButtons = env.debugElementsbyCss(deleteButtonsSelector);
      deleteButton = deleteButtons[0].nativeElement;
      expect(deleteButtons.length).toBe(numberOfDataRows);
    });

    it('should show a confirmation modal when the delete button is clicked', () => {
      const confirmationModalService = TestBed.inject(ConfirmationModalService);
      spyOn(confirmationModalService, 'show').and.callThrough();

      deleteButtons = env.debugElementsbyCss(deleteButtonsSelector);
      deleteButton = deleteButtons[0].nativeElement;
      deleteButton.click();
      env.fixture.detectChanges();
      expect(confirmationModalService.show).toHaveBeenCalled();
    });

    it('should delete a card when the Delete Card button is clicked on the confirmation modal', () => {
      const boardService = TestBed.inject(BoardService);
      spyOn(boardService, 'deleteCard').and.callThrough();

      deleteButtons = env.debugElementsbyCss(deleteButtonsSelector);
      deleteButton = deleteButtons[0].nativeElement;
      deleteButton.click();
      env.fixture.detectChanges();
      expect(boardService.deleteCard).toHaveBeenCalled();
    });
  });

  describe('Test Column Types', () => {
    const timelineHeaderSelector = '.head-cell .bmi-timeline__marker--triangle-cap';
    const timeLineRowSelector = '.body-cell .bmi-timeline__marker';
    beforeEach(async () => {
      const config = cloneDeep(defaultConfig);
      config.fields.dataSourceId = 'cards';
      config.fields.columns[numberOfColumns - 1] = TIMELINE_COLUMN_TEMPLATE;
      env.config = config;
      env.fixture.detectChanges();
      widgetComponent = await env.component;
    });
    it('should display Timeline', async () => {
      const nativeElement = env.fixture.nativeElement;
      const headings = await harness.getDisplayTexts(timelineHeaderSelector);
      expect(headings.length).toBe(1);
      const timeLineCells = env.debugElementsbyCss(timeLineRowSelector);
      expect(timeLineCells.length).toBe(numberOfDataRows);
    });
  });

  describe('Pagination', () => {
      beforeEach(async () => {
        const config = cloneDeep(defaultConfig);
        config.fields.dataSourceId = 'cards';
        config.fields.rowsPerPage = 1;
        env.config = config;
        env.fixture.detectChanges();
        widgetComponent = await env.component;
      });
    const paginationSelector = 'ui-data-table-pagination';
    const pageIndicatorSelector = '.page-indicator';

    it('should show pagination controls when there are more records than fit on a page', async () => {
      const nativeElement = env.fixture.nativeElement;
      const paginationControls = env.debugElementsbyCss(paginationSelector);
      expect(paginationControls.length).toBe(1);
      const pageIndicatorText = await harness.getDisplayText(pageIndicatorSelector);
      expect(pageIndicatorText).toBe(`1 of ${numberOfDataRows}`);
    });
  });
});
