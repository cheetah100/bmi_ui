import { Component, OnInit, OnDestroy } from '@angular/core';

import { WidgetContext } from '../../widget-context';
import {
  TableWidgetConfig,
  TableWidgetColumn,
  TableWidgetFields,
  EditabilityOptions,
  TableColumnOptions
} from './table-config';

import { TypedFormGroup, TemplatedFormArray, TypedFormControl } from '@bmi/ui';
import { ensureIdUniqueIn, Option } from '@bmi/utils';


@Component({
  selector: 'bmi-core-table-widget-editor',
  templateUrl: './table-widget-editor.component.html',
  styleUrls: ['./table-widget-editor.component.scss'],
})
export class TableWidgetEditorComponent implements OnInit {
  constructor(
    private widgetContext: WidgetContext<TableWidgetConfig>,
  ) {}

  widthOptions: Option[] = [
    { text: 'Fit', value: 'max-content' },
    { text: 'Small', value: '1fr' },
    { text: 'Medium', value: '2fr' },
    { text: 'Large', value: '4fr' },
  ];

  columnStyleOptions: Option[] = [
    { text: 'Default', value: 'default' },
    { text: 'Timeline', value: 'timeline' },
    { text: 'Status', value: 'status' },
    { text: 'Boolean', value: 'boolean' },
    { text: 'Percentage', value: 'percentage' },
    { text: 'Date', value: 'date' },
  ];

  readonly form = new TypedFormGroup<TableWidgetFields>({
    hasPagination: new TypedFormControl(true),
    rowsPerPage: new TypedFormControl(8),
    dataSourceId: new TypedFormControl(),
    hasSearch: new TypedFormControl(false),
    isEditable: new TypedFormControl(false),
    editOptions: new TypedFormGroup<EditabilityOptions>({
      add: new TypedFormControl(false),
      edit: new TypedFormControl(),
      delete: new TypedFormControl(),
      actionsColumnTitle: new TypedFormControl(),
    }),
    columns: new TemplatedFormArray<TableWidgetColumn>(() => new TypedFormGroup({
      title: new TypedFormControl(),
      width: new TypedFormControl(),
      mergeRows: new TypedFormControl(),
      content: new TypedFormControl()
    }))
  });

  get editable() {
    return !!this.form.value.isEditable;
  }

  get selectedDataSource() {
    return this.form.value.dataSourceId;
  }


  ngOnInit() {
    this.widgetContext.widgetConfig.subscribe(config => this.form.patchValue(config.fields, { emitEvent: false }));
    this.form.valueChanges.subscribe(updatedValue => {
      this.widgetContext.updateWidgetConfig(config => config.fields = updatedValue);
    });
  }

  addColumn(columnContent: TableColumnOptions) {
    const existingColumnTitles = this.form.value.columns.map(c => c.title);
    const uniqueTitle = ensureIdUniqueIn('New Column', existingColumnTitles, ' ');
    const columnsControl = this.form.controls.columns as TemplatedFormArray<TableWidgetColumn>;
    columnsControl.pushValue({
      width: '1fr',
      title: uniqueTitle,
      content: columnContent
    });
  }

}
