import { ParameterBinding } from '../../../formatter';
import { BmiPageRoute } from '../../../navigation/bmi-route';
import { TableColumnOptions } from '../table-config';

export interface DefaultColumnOptions extends TableColumnOptions {
  value: ParameterBinding;
  route?: BmiPageRoute;

  /**
   * [optional] allows you to select a special display mode for this cell, such
   * as boolean or status.
   */
  displayStyle?: string;
}
