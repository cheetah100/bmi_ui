import { Component, OnInit } from '@angular/core';
import { DynamicEditorContext, TypedFormControl, TypedFormGroup, Option } from '@bmi/ui';
import { DefaultColumnOptions } from './default-column-config';

@Component({
  templateUrl: './default-table-column-editor.component.html'
})
export class DefaultTableColumnEditorComponent implements OnInit {
  constructor (
    private context: DynamicEditorContext<DefaultColumnOptions>
  ) {}

  form = new TypedFormGroup<DefaultColumnOptions>({
    type: new TypedFormControl(),
    value: new TypedFormControl(),
    route: new TypedFormControl(),
    displayStyle: new TypedFormControl(),
  });

  displayStyleOptions: Option[] = [
    {value: null, text: 'Default'},
    {value: 'link', text: 'Link to another page'},
    {value: 'boolean', text: 'Boolean (tick/cross)'},
    {value: 'status', text: 'Colored Status Indicator (red/yellow/green)'}
  ];

  get hasAdvancedOptionsSet() {
    const value = this.form.value;
    return value.route || value.displayStyle;
  }

  get isLinkMode() {
    return this.form.value.displayStyle === 'link';
  }

  ngOnInit() {
    this.context.config.subscribe(config => this.form.patchValue(config, {emitEvent: false}));
    this.form.valueChanges.subscribe(config => this.context.setConfig(config));
  }
}
