import { Injectable, Injector } from '@angular/core';
import { ButtonToolbarTableCell, TableDisplayValue } from '@bmi/ui';
import { Observable, of } from 'rxjs';
import { prepareButtonToolbar } from '../../../components/configurable-action-buttons/prepare-button-toolbar';
import { EventActionFactoryService } from '../../../events';
import { PageDataContext } from '../../../page-data-context/page-data-context';
import { TableWidgetColumn } from '../table-config';
import { ActionButtonTableColumnOptions } from './action-button-column-config';


@Injectable({providedIn: 'root'})
export class ActionButtonTableColumnService {
  constructor(
    private actionFactory: EventActionFactoryService
  ) {}

  prepareTableValue(
    column: TableWidgetColumn<ActionButtonTableColumnOptions>,
    dataContext: PageDataContext,
    injector: Injector
  ): Observable<TableDisplayValue> {
    return of(<ButtonToolbarTableCell>{
      type: 'button-toolbar',
      value: prepareButtonToolbar(
        column.content.buttons ?? [],
        action => this.actionFactory.runActionChain(action.onInvoke, {dataContext, injector}).subscribe()
      )
    });
  }
}
