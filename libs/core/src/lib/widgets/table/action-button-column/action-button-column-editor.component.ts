import { Component, OnInit } from '@angular/core';
import { DynamicEditorContext, TypedFormControl, TypedFormGroup } from '@bmi/ui';
import { ActionButtonTableColumnOptions } from './action-button-column-config';

@Component({
  template: `
    <ng-container [formGroup]="form">
      <bmi-button-toolbar-editor formControlName="buttons"></bmi-button-toolbar-editor>
    </ng-container>
  `
})
export class ActionButtonColumnEditorComponent implements OnInit {
  constructor(
    private context: DynamicEditorContext<ActionButtonTableColumnOptions>
  ) {}

  form = new TypedFormGroup<ActionButtonTableColumnOptions>({
    type: new TypedFormControl(),
    buttons: new TypedFormControl()
  });

  ngOnInit() {
    this.context.config.subscribe(config => this.form.patchValue(config, {emitEvent: false}));
    this.form.valueChanges.subscribe(value => this.context.setConfig(value));
  }
}
