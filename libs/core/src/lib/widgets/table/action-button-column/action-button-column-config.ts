import { ConfigurableToolbarAction, ConfigurableToolbarItem } from '../../../components/configurable-action-buttons/config';
import { TableColumnOptions } from '../table-config';

export interface ActionButtonTableColumnOptions extends TableColumnOptions {
  buttons: ConfigurableToolbarItem[];
}
