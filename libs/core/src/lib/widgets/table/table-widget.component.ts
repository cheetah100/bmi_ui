import { TemplatePortal } from '@angular/cdk/portal';
import { Component, Injector, OnDestroy, OnInit, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core';
import { BoardService, CardCacheService, GravityTimelineService } from '@bmi/gravity-services';
import {
  ConfirmationModalService, DataTableAction, InMemoryTableController, TableColumn,
  TableModel, TableRow,
  ValueFormatter,
  PollingService,
  LoaderState
} from '@bmi/ui';
import { combineLatestDeferred, combineLatestObject, formatDate, ObjectMap } from '@bmi/utils';
import cloneDeep from 'lodash-es/cloneDeep';
import lodashMapValues from 'lodash-es/mapValues';
import { BehaviorSubject, combineLatest, Observable, of, Subscription, EMPTY, throwError } from 'rxjs';
import { catchError, map, switchMap, tap, switchMapTo } from 'rxjs/operators';
import dedent from 'ts-dedent';
import { CardEditorModalService } from '../../card-editor/card-editor-modal.service';
import { BmiResolvedRoute } from '../../navigation/bmi-route';
import { Binder } from '../../page-data-context/binder';
import { PageDataContext } from '../../page-data-context/page-data-context';
import { RoutesService } from '../../services/routes.service';
import { TimelineDataPoint, TimelineMarker } from '../../timeline-display/timeline-display';
import { distinctUntilNotEqual } from '../../util/distinct-until-not-equal';
import { WidgetContext } from '../../widget-context';
import { TableWidgetColumn, TableWidgetConfig } from './table-config';

import { DataSourceWithListApi } from '../../data-sources/types';
import { TimelineColumnOptions } from './timeline-column/timeline-column-config';
import { DefaultColumnOptions } from './default-table-column/default-column-config';
import { ActionButtonTableColumnService } from './action-button-column/action-button-column.service';
import { ActionButtonTableColumnOptions } from './action-button-column/action-button-column-config';
import { deepFreezeObject } from '../../util/deep-freeze-object';

function arrayify<T>(value: T | T[]): T[] {
  return Array.isArray(value) ? value : [value];
}

@Component({
  selector: 'bmi-core-table-widget',
  templateUrl: 'table-widget.component.html',
  styles: [`
      ui-data-table-pagination {
        margin-top: 1rem;
      }
      ui-search-input {
        margin-bottom: 1rem;
        display: block;
      }
      ui-data-table-pagination {
        display: block;
      }

      bmi-core-timeline-display {
        flex: 1;
        cursor: default;
      }

      div.table-control {
        display: flex;

      }
  `],
})
export class TableWidgetComponent implements OnInit, OnDestroy {
  private subscription: Subscription;
  private defaultPageRows = 8;
  tableModel: TableModel = null;
  paginated = false;
  searchEnabled = false;
  addEnabled = false;
  search = '';

  loaderState = new BehaviorSubject<LoaderState>({mode: 'loading'});

  private currentBoardId:string;

  tableModelSource = new BehaviorSubject<TableModel>(null);

  private triggerRefresh = new BehaviorSubject<void>(null);
  private readonly emptyColumn: TableWidgetColumn = {
    title: '',
    width: '1fr',
    content: null,
  };

  @ViewChild('timelineHeaderPortal', { static: true }) timelineHeaderPortal: TemplateRef<any>;
  @ViewChild('timelineCellPortal', { static: true }) timelineCellPortal: TemplateRef<any>;

  constructor(
    private context: WidgetContext<TableWidgetConfig>,
    private pageDataContext: PageDataContext,
    private routeService: RoutesService,
    private viewContainerRef: ViewContainerRef,
    private quartersService: GravityTimelineService,
    private cardCacheService: CardCacheService,
    private cardEditorModal: CardEditorModalService,
    private actionButtonColumnService: ActionButtonTableColumnService,
    private injector: Injector,
  ) {}

  ngOnInit() {

    this.subscription = this.context.widgetConfig.pipe(
        distinctUntilNotEqual(),
        map(config => this.injectFakeEditActionsColumn(config)),
        map(config => deepFreezeObject(config, 'Code attempted to modify table widget config')),
        switchMap(config =>
          combineLatest([
            of(config),
            this.createTableColumns(config),
            this.getTableData(config),
            this.getTableSource(config),
            this.triggerRefresh
          ]).pipe(
            catchError((err, source) => {
              console.error('Table widget error', err);
              this.loaderState.next({mode: 'error', message: String(err)});
              return EMPTY;
            })
          )
        )
      )
      .subscribe(([tableConfig, tableColumns, tableData, tableSource]) => {
        if (tableData.length > 0) {
          this.loaderState.next({mode: 'ready'});
        } else {
          this.loaderState.next({mode: 'message', message: 'No results'});
        }

        const fields = tableConfig.fields;
        this.searchEnabled = !!tableConfig.fields.hasSearch;
        this.addEnabled = fields.isEditable && fields.editOptions?.add;
        this.currentBoardId = fields.editOptions.boardId ?? tableSource;
        this.tableModel = new TableModel(tableColumns);
        const tableController = new InMemoryTableController(this.tableModel);
        tableData.forEach(row => {
          tableController.addRow(
            new TableRow(
              this.tableModel,
              tableConfig.fields.columns.map((column, index) => {
                return row[index];
              }),
              null,
            )
          );
        });

        this.tableModel.updateRequested.pipe(
          switchMap(() => tableController.refresh()),
          tap( () => this.tableModelSource.next(this.tableModel))
        ).subscribe();

        this.tableModel.filters.search = this.search;

        const pageRows: number = tableConfig.fields.rowsPerPage ?? this.defaultPageRows;
        this.paginated = tableConfig.fields.hasPagination && pageRows !== 0 && (tableData.length && tableData.length > pageRows);
        if (this.paginated) {
          this.tableModel.pagination.enabled = true;
          this.tableModel.pagination.rowsPerPage = pageRows;
        }

        this.tableModel.update();
        this.tableModelSource.next(this.tableModel)
      });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onTableAction(action: DataTableAction) {
    const boardId = this.currentBoardId;
    const cardId = action.button.cardId;
    if (boardId && cardId) {
      if (action.action === 'edit-card') {
        this.subscription.add(this.cardEditorModal.editCard(boardId, cardId).subscribe((result) => {
          if (result?.action === 'yes') {
            this.refreshBoard(boardId)
          }
        }));
      } else if(action.action === 'delete-card') {
        this.subscription.add(
          this.cardEditorModal.deleteCard(boardId, cardId).subscribe(() => this.refreshBoard(boardId))
        );
      }
    }
  }

  createCard() {
    this.subscription.add(this.cardEditorModal.createCard(this.currentBoardId).subscribe((result) => {
      if (result?.action === 'yes') {
        this.refreshBoard(this.currentBoardId)
      }
    }));
  }

  updateSearch(search: string): void {
    this.search = search;
    this.tableModel.filters.search = search;
    this.tableModel.update();
  }

  private injectFakeEditActionsColumn(config: TableWidgetConfig): TableWidgetConfig {
    // If this table is editable, we will augment the config with an edit actions column
    const fields = config.fields;
    const editConfig = fields.editOptions;
    const needsAnEditActionsColumn = fields.isEditable && editConfig && (editConfig?.edit || editConfig?.delete);
    const alreadyHasEditActionsColumn = config.fields.columns.some(c => c.content?.type === 'edit-actions');
    if (needsAnEditActionsColumn && !alreadyHasEditActionsColumn) {
      const actionCol: TableWidgetColumn = cloneDeep(this.emptyColumn);
      actionCol.title = editConfig.actionsColumnTitle ?? '';
      actionCol.content = {type: 'edit-actions'};
      return {
        ...config,
        fields: {
          ...config.fields,
          columns: [
            {
              ...this.emptyColumn,
              title: editConfig.actionsColumnTitle ?? '',
              content: {type: 'edit-actions'}
            },
            ...config.fields.columns,
          ]
        }
      }
    } else {
      return config;
    }
  }

  private createTableColumns(config: TableWidgetConfig): Observable<TableColumn[]> {
    return combineLatestDeferred(
      config.fields.columns.map(col => this.createColumn(col, config))
    );
  }

  private createColumn(
    columnConfig: TableWidgetColumn,
    config: TableWidgetConfig
  ): Observable<TableColumn> {
    const column = new TableColumn(columnConfig.title);
    column.width = columnConfig.width;
    column.formatter = new DefaultTableWidgetFormatter();
    column.merge = !!columnConfig.mergeRows;

    const contentType = columnConfig?.content?.type;
    if (contentType === 'default') {
      const content = columnConfig.content as DefaultColumnOptions;
      if (content.route) {
        column.formatter = new LinkCellFormatter(this.routeService);
      } else if (content.displayStyle === 'boolean') {
        column.formatter = new BooleanCellFormatter();
      } else if (content.displayStyle === 'status') {
        column.formatter = new StatusCellFormatter();
      }
    } else if (contentType === 'timeline') {
      const timelineConfig = columnConfig.content as TimelineColumnOptions;
      return this.prepareTimelineColumn(column, timelineConfig);
    } else if (contentType === 'edit-actions') {
      column.width = '80px';
    }

    return of(column);
  }

  private getTableData(config: TableWidgetConfig): Observable<any[][]> {
    const dataSourceId = config.fields.dataSourceId;

    if (!dataSourceId) {
      return throwError(new Error('No data source selected'));
    }

    return this.pageDataContext.getFetcher(dataSourceId).pipe(
      switchMap((dataSource: DataSourceWithListApi) => {
        if (!dataSource) {
          return throwError(new Error(`Data source '${dataSourceId} not found`));
        } else if (dataSource.bindList) {
          return dataSource.bindList().pipe(
            map(items => items ?? []),
            map(items => items.map(item => this.pageDataContext.create({
              dataSources: { this: item }
            }))),
            switchMap(itemContexts => combineLatestDeferred(itemContexts.map(dc => this.prepareRow(config, dc))))
          )
        } else {
          throw new Error('Data source not supported');
        }
      })
    );
  }

  private getTableSource(config: TableWidgetConfig): Observable<string> {
    const dataSourceId = config.fields.dataSourceId;
    return this.pageDataContext.getBoard(dataSourceId);
  }

  private prepareRow(config: TableWidgetConfig, binder: PageDataContext): Observable<any[]> {
    const columns = config.fields.columns;
    return combineLatestDeferred(columns.map(c => this.prepareCell(c, config, binder))).pipe(
      distinctUntilNotEqual()
    );
  }

  private prepareCell(column: TableWidgetColumn, config: TableWidgetConfig, binder: PageDataContext): Observable<any> {
    const contentType = column.content?.type;

    if (contentType === 'default') {
      const content = column.content as DefaultColumnOptions;
      if (content.route) {
        return combineLatestObject<LinkCellValue>({
          value: binder.bind(content.value),
          route: this.routeService.resolveRoute(content.route, {binder})
        });
      } else {
        return binder.bind(content.value);
      }
    } else if (contentType === 'edit-actions') {
      const editOptions = config.fields.editOptions;
      return binder.bind({bindingType: 'data-path', path: 'this.id'}).pipe(
        map(cardId => {
          return {
            type: 'actions',
            buttons: [
              editOptions.edit ? {title: 'Edit', icon: 'edit', action: 'edit-card', cardId} : undefined,
              editOptions.delete ? {title: 'Delete', icon: 'trash', action: 'delete-card', cardId} : undefined,
            ].filter(x => !!x)
          };
        })
      );
    } else if (contentType === 'action-buttons') {
      return this.actionButtonColumnService.prepareTableValue(column as TableWidgetColumn<ActionButtonTableColumnOptions>, binder, this.injector);
    } else if (contentType === 'timeline') {
      return this.prepareTimelineData(column.content as TimelineColumnOptions, binder);
    } else {
      return of(null);
    }
  }

  private refreshBoard(boardId: string) {
    this.cardCacheService.refreshBoard(boardId, true);
    this.triggerRefresh.next();
  }

  private getQuarter(quarterId: string) {
    return this.quartersService.getAllQuarters().pipe(
      map(quarters => quarters.find(q => q.id === quarterId))
    );
  }

  private prepareTimelineColumn(column: TableColumn, timelineConfig: TimelineColumnOptions) {
    return combineLatestObject({
      startQuarter: this.getQuarter(timelineConfig.startQuarterId),
      endQuarter: this.getQuarter(timelineConfig.endQuarterId),
      quarters: this.quartersService.getAllQuartersInThisRange(
        timelineConfig.startQuarterId,
        timelineConfig.endQuarterId
      ),
      markers: this.getTimelineGridlines(timelineConfig)
    }).pipe(
      map(({ quarters, startQuarter, endQuarter, markers }) => {
        const timelineHeaderContext = {
          timelineStart: startQuarter ? startQuarter.startDate : 0,
          timelineEnd: endQuarter ? endQuarter.endDate : 0,
          dataPoints: quarters.map(q => ({ name: q.name, start: q.startDate, end: q.endDate })),
          markers: markers
        };

        column.formatter = new TimelineCellFormatter();
        column.renderer = {
          noHeaderPadding: true,
          noCellPadding: true,
          createHeaderPortal: () => new TemplatePortal(
              this.timelineHeaderPortal,
              this.viewContainerRef,
              { value: timelineHeaderContext }
            ),
          createCellPortal: (value) => new TemplatePortal(this.timelineCellPortal, this.viewContainerRef, { value })
        };
        return column;
      })
    );
  }


  private getTimelineGridlines(timelineConfig: TimelineColumnOptions): Observable<TimelineMarker[]> {
    return this.quartersService.getAllQuartersInThisRange(
      timelineConfig.startQuarterId,
      timelineConfig.endQuarterId
    ).pipe(
      map(quarters => {
        const startDates = quarters.map(q => q.startDate);

        // The quarters service returns these backwards, but we'll assume
        // they're unordered and then drop the first one, as we don't want a
        // grid line at the start of the timeline
        startDates.sort((x, y) => x - y);
        return startDates.slice(1).map(date => (<TimelineMarker>{
          at: date,
          style: 'grid-line',
          color: 'gray'
        }));
      })
    );
  }

  private prepareTimelineData(timelineConfig: TimelineColumnOptions, binder: Binder): Observable<TimelineCellValue> {
    if (!timelineConfig) {
      return of(null);
    } else {
      return combineLatestObject({
        startQuarter: this.getQuarter(timelineConfig.startQuarterId),
        endQuarter: this.getQuarter(timelineConfig.endQuarterId),
        itemStartDate: binder.bind(timelineConfig.itemStartDate).pipe(map(x => arrayify(x))),
        itemEndDate: binder.bind(timelineConfig.itemEndDate).pipe(map(x => arrayify(x))),
        itemName: binder.bind(timelineConfig.itemName).pipe(map(x => arrayify(x))),
        markers: this.getTimelineGridlines(timelineConfig)
      }).pipe(
        map(({ startQuarter, endQuarter, itemStartDate, itemEndDate, itemName, markers }) => {
          // Prepare the data points.
          const maxLength = Math.max(itemName.length, itemStartDate.length, itemEndDate.length);
          const dataPoints: TimelineDataPoint[] = [];
          for (let i = 0; i < maxLength; i++) {
            dataPoints.push({
              name: itemName[i] ? String(itemName[i]) : '',
              start: Number(itemStartDate[i]),
              end: Number(itemEndDate[i])
            });
          }

          return {
            type: 'bmi-timeline-cell',
            timelineStart: startQuarter ? startQuarter.startDate : 0,
            timelineEnd: endQuarter ? endQuarter.endDate : 0,
            dataPoints: dataPoints,
            markers: markers
          };
        })
      );
    }
  }
}


interface LinkCellValue {
  value: any;
  route: BmiResolvedRoute;
}


class LinkCellFormatter extends ValueFormatter {
  constructor(private routeService: RoutesService) {
    super();
  }

  formatValue(tableValue: LinkCellValue) {
    return {
      type: 'link',
      value: tableValue.value,
      routerLink: this.routeService.getRouterLink(tableValue.route)
    }
  }

  sortValue(tableValue: LinkCellValue) {
    return tableValue.value;
  }
}

const COLOR_ORDER_MAP = {
  'green': 1,
  'yellow': 2,
  'red': 3,
};

interface StatusCellValue {
  type: 'color';
  color: string;
}

class StatusCellFormatter {
  // just doing what was used for idp.
  // Needs improvement.
  // I hope the tableValue is 'red', 'yellow', or 'green'!

  formatValue(tableValue: string) {
    return {
      type: 'color',
      color: COLOR_ORDER_MAP[tableValue] ? tableValue : 'white',
    };
  }

  sortValue(tableValue: string) {
    return COLOR_ORDER_MAP[tableValue] || -Infinity;
  }
}

class BooleanCellFormatter extends ValueFormatter {
  formatValue(tableValue: any) {
    if (tableValue === null || tableValue === undefined) {
      return 'N/A';
    } else if (typeof tableValue === 'boolean') {
      return {
        type: 'boolean',
        value: tableValue
      };
    } else {
      return super.formatValue(tableValue);
    }
  }

  sortValue(tableValue: string) {
    return tableValue;
  }
}


class DefaultTableWidgetFormatter extends ValueFormatter {
  formatValue(tableValue: any) {
    if (Array.isArray(tableValue)) {
      return {
        type: 'list',
        value: tableValue.filter(x => !!x)
      };
    } else {
      return super.formatValue(tableValue);
    }
  }
}

class TimelineCellFormatter extends ValueFormatter {
  sortValue(tableValue: TimelineCellValue) {
    return tableValue && Math.min(...tableValue.dataPoints.map(x => x.start));
  }
}


interface TimelineCellValue {
  type: string;
  timelineStart: number;
  timelineEnd: number;
  dataPoints: TimelineDataPoint[];
}
