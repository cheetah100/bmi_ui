import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { PathResolver } from '@bmi/gravity-services';
import { TypedFormGroup, TypedFormControl, TemplatedFormArray } from '@bmi/ui';
import { Option, ObjectMap } from '@bmi/utils';

import { WidgetContext } from '../../widget-context';
import {
  NavListWidgetFields,
  NavListWidgetConfig,
  typeOptions
} from './nav-list-widget.component';
import { WidgetConfig } from '../../widget-config';
import { BmiLink } from '../../navigation/bmi-link';
import { PageInfoService } from '../../page/page-info.service';
import { PageDataContext } from '../../page-data-context/page-data-context';
import { ParameterBinding } from '../../page-data-context/parameter-binding';
import { BmiRoute } from '../../navigation/bmi-route';

@Component({
  template: `
    <ui-form-field label="Display type" [formGroup]="form">
      <ui-single-select
        [options]="typeOptions"
        formControlName="type"
      ></ui-single-select>
    </ui-form-field>

    <ui-stacker-list [items]="form.controls.links">
      <ng-template let-item>
        <ng-container [formGroup]="item">
          <ui-form-field label="title">
            <input formControlName="title" />
          </ui-form-field>
          <ui-form-field label="Title variables">
            <bmi-data-map-editor
              formControlName="bindings"
              [resolver]="resolver"
            >
            </bmi-data-map-editor>
          </ui-form-field>
          <ui-form-field label="Page link">
            <bmi-core-route-selector
              [moduleId]="moduleId"
              formControlName="route"
              [resolver]="resolver"
            >
            </bmi-core-route-selector>
          </ui-form-field>
        </ng-container>
      </ng-template>
    </ui-stacker-list>
    <button
      type="button"
      class="btn btn--small btn--secondary"
      (click)="addLink()"
    >
      Add link
    </button>
  `
})
export class NavListWidgetEditorComponent implements OnInit, OnDestroy {
  readonly form = new TypedFormGroup<NavListWidgetFields>({
    type: new TypedFormControl<typeOptions>('horizontal'),
    links: new TemplatedFormArray<BmiLink>(
      () =>
        new TypedFormGroup({
          route: new TypedFormControl<BmiRoute>(null),
          title: new TypedFormControl<string>(null),
          bindings: new TypedFormControl<ObjectMap<ParameterBinding>>({})
        })
    )
  });
  moduleId: string;
  resolver: PathResolver;
  typeOptions: Option[] = [
    { text: 'Blocks', value: 'block' },
    { text: 'Horizontal', value: 'horizontal' },
    { text: 'Vertical', value: 'vertical' },
    { text: 'Tabs (horizontal)', value: 'tabs-h' },
    { text: 'Tabs (vertical)', value: 'tabs-v' }
  ];

  private subscription = new Subscription();

  get linksControl() {
    return this.form.controls.links as TemplatedFormArray<BmiLink>;
  }

  constructor(
    private widgetContext: WidgetContext,
    private pageInfoService: PageInfoService,
    private pageDataContext: PageDataContext
  ) {}

  ngOnInit() {
    this.moduleId = this.pageInfoService.currentModuleId;
    this.resolver = this.pageDataContext.getPathResolverNow();
    this.subscription.add(
      this.widgetContext.widgetConfig.subscribe((config: WidgetConfig) =>
        this.form.patchValue(config.fields, { emitEvent: false })
      )
    );
    this.subscription.add(
      this.form.valueChanges.subscribe((updatedValue: NavListWidgetFields) =>
        this.widgetContext.updateWidgetConfig(
          (config: NavListWidgetConfig) => (config.fields = updatedValue)
        )
      )
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  addLink() {
    this.linksControl.pushValue({
      route: null,
      title: ''
    });
  }
}
