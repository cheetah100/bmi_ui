import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, Observable, of, iif, combineLatest } from 'rxjs';

import { WidgetConfig } from '../../widget-config';
import { BmiLink } from '../../navigation/bmi-link';
import { WidgetContext } from '../../widget-context';
import { PageInfoService } from '../../page/page-info.service';
import { map, switchMap } from 'rxjs/operators';
import { PageInfo } from '../../page-config';
import { ObjectMap, combineLatestObject } from '@bmi/utils';
import { ParameterBinding } from '../../page-data-context/parameter-binding';
import { PageDataContext } from '../../page-data-context/page-data-context';
import { mustacheDown } from '../../util/mustachedown';
import { mapValues } from 'lodash-es';
import { BmiPageRoute } from '../../navigation/bmi-route';

export type typeOptions = 'horizontal' | 'vertical' | 'breadcrumbs' | 'tabs-h' | 'tabs-v';

export interface BreadcrumbsLink extends BmiLink {
  bindings?: ObjectMap<ParameterBinding>;
}

export interface NavListWidgetFields {
  type: typeOptions;
  links: BreadcrumbsLink[];
}

export type NavListWidgetConfig = WidgetConfig<NavListWidgetFields>;

@Component({
  template: `
<nav *ngIf="links.length">
  <ul class="navigation-list navigation-list--{{ type }}{{ cuiListClasses(type)}}">
    <ng-container *ngFor="let link of links; let i = index">
      <li class="navigation-list__li" *ngIf="!!link.title" [class.active]="linkIsActive(link)">
        <a *ngIf="link.route" [bmiRouteLink]="link.route">{{ link.title }}</a>
        <span class="navigation-list__routeless" *ngIf="!link.route">{{ link.title }}</span>
        <span class="ui-icon ui-icon-chevron-right" *ngIf="type === 'breadcrumbs' && i < links.length - 1"></span>
      </li>
    </ng-container>
  </ul>
</nav>
  `,
  styleUrls: ['./nav-list-widget.component.scss'],
})
export class NavListWidgetComponent implements OnInit, OnDestroy {

  private config: NavListWidgetConfig;
  type = 'horizontal';
  links: BreadcrumbsLink[] = [];
  private pageId: string;
  private subscription = new Subscription();

  constructor(
    private widgetContext: WidgetContext,
    private pageInfoService: PageInfoService,
    private binder: PageDataContext,
  ) { }

  ngOnInit() {

    this.subscription.add(this.widgetContext.widgetConfig.pipe(
      switchMap((config: NavListWidgetConfig) => {
        this.config = config;
        return this.renderLinksTitles(config.fields.links);
      }),
      switchMap((links: BreadcrumbsLink[]) => {
        return this.defaultBreadcrumbsFields(this.config.fields, links);
      }),
    ).subscribe(
      (fields: NavListWidgetFields) => {
        this.links = [...fields.links];
        this.type = fields.type;
      }
    ));

    this.subscription.add(
      this.pageInfoService.pageInfo.subscribe(
        (pageInfo: PageInfo) => this.pageId = pageInfo.id
      )
    );

  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private renderVars(
    template: string,
    bindings: ObjectMap<ParameterBinding>
  ): Observable<string> {

    return combineLatestObject(mapValues(
      bindings, b => this.binder.bind(b)
    )).pipe(
      map(values => mustacheDown(template, {
        formatMarkdown: false,
        context: { ...values },
        handleError: (error, text) => {
          console.warn('Markdown renderer: ', error);
          return text;
        }
      })),
    );

  }

  private renderLinksTitles(links: BreadcrumbsLink[]): Observable<BreadcrumbsLink[]> {
    return iif(

      () => !!links.length,

      combineLatest(
        links.map((bl: BreadcrumbsLink) => this.renderVars(bl.title, bl.bindings || {}))
      ).pipe(
        map((titles: string[]) => {
          const renderedLinks: BreadcrumbsLink[] = [];
          for (let l = 0, ll = links.length; l < ll; l++) {
            renderedLinks.push({ ...links[l], title: titles[l] });
          }
          return renderedLinks;
        })
      ),

      of([])

    );
  }

  private defaultBreadcrumbsFields(
    fields: NavListWidgetFields,
    links: BreadcrumbsLink[],
  ): Observable<NavListWidgetFields> {

    return iif(

      () => fields.type === 'breadcrumbs',

      this.pageInfoService.pageInfo.pipe(
        map((pageInfo: PageInfo) => {
          const breadcrumbed = [
            {
              title: 'Home',
              route: {
                type: 'builtin',
                routeId: '',
                moduleId: pageInfo.applicationId
              },
            },
            ...links,
            { title: pageInfo.title },
          ];
          return Object.assign({}, fields, { links: breadcrumbed });
        })
      ),

      of({ ...fields, links }),

    );
  }

  cuiListClasses(type: string): string {
    switch (type) {
      case 'tabs-h':
        return ' tabs';
      case 'tabs-v':
        return ' tabs tabs--vertical tabs--right';
      default:
        return '';
    }
  }

  linkIsActive(link: BreadcrumbsLink): boolean {
    if (link.route) {
      const route = link.route as BmiPageRoute;
      return (
        this.pageId
        && route.type
        && route.type === 'page'
        && route.pageId === this.pageId
      );
    }
    return false;
  }

}
