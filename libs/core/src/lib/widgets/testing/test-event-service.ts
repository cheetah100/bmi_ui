import { BehaviorSubject, Observable } from 'rxjs';
import { EventActionConfig } from '../../events';

/**
 * basic, just catering for button widget so far.
 * Will need to be fleshed out.
 */

export class TestEventService {
  private _config = null;
  private _configSubj = new BehaviorSubject<EventActionConfig>(this._config);
  private _emitted = 0;
  setConfig(config) {
    this._config = config;
  }
  emit(evt: string) {
    this._configSubj.next(this._config);
    this._emitted++;
  }
  get hasEmitted(): boolean {
    return !!this._emitted;
  }
  resetEmitted() {
    this._emitted = 0;
  }
}
