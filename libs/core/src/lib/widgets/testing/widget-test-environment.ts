import { HttpClient } from '@angular/common/http';
import { GravityTimelineService, GravityTransformService, gravityTestbed2 } from '@bmi/gravity-services';
import { ConfirmationModalService, SharedUiModule } from '@bmi/ui';
import { StateContainer } from '@bmi/utils';
import { Observable, of } from 'rxjs';
import { FilterState } from '../../filters/filter';
import { FilterService } from '../../filters/filter.service';
import { FlairContextService } from '../../flair/flair-context.service';
import { FormatterService } from '../../formatter';
import { PageInfo } from '../../page-config';
import { PageDataContext } from '../../page-data-context/page-data-context';
import { PageInfoService } from '../../page/page-info.service';
import { CurrentUserService } from '../../services/current-user.service';
import { WidgetRepositoryService } from '../../services/widget-repository.service';
import { WidgetConfig } from '../../widget-config';
import { WidgetMode } from '../../widget-context';
import { WidgetHostComponent } from '../../widget-host/widget-host.component';
import { BmiWidgetsModule } from '../bmi-widgets.module';
import { WidgetHarness } from './widget-harness';
import { WidgetHostHostComponent } from './widget-host-host.component';
import { DEFAULT_TEST_BOARDS, WidgetTestState, WidgetTestOptions, DATA_CONFIG_DEFAULT_CARDS } from './widget-test-options';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement, Type } from '@angular/core';
import { RoutesService } from '../../services/routes.service';
import { EventService } from '../../events';
import { TestEventService } from './test-event-service';
import { FormContext } from '../../forms/form-context';
import { deepFreezeObject } from '../../util/deep-freeze-object';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


const freeze = <TConfig extends WidgetConfig>(value: TConfig): TConfig => deepFreezeObject(value, 'Widget Config Mutated', 'config');

const getWidgetFixture = (
  widgetEnv,
  declarations = [],
  boards = DEFAULT_TEST_BOARDS,
  override
): ComponentFixture<WidgetHostHostComponent> => {
  TestBed.configureTestingModule({
    imports: [BmiWidgetsModule, SharedUiModule, BrowserAnimationsModule],
    declarations: [WidgetHostHostComponent, WidgetHostComponent, ...declarations],
    providers: [
      PageDataContext,
      FormatterService,
      {provide: WidgetRepositoryService, useValue: widgetEnv},
      {provide: ConfirmationModalService, useValue: {show: c => of(false)}},
      {provide: FlairContextService, useValue: null},
      {provide: HttpClient, useValue: null},
      {provide: GravityTimelineService, useValue: null},
      {provide: CurrentUserService, useValue: widgetEnv},
      {provide: FilterService, useValue: widgetEnv},
      {provide: GravityTransformService, useValue: null},
      {provide: PageInfoService, useValue: widgetEnv},
      {provide: RoutesService, useValue: null},
      {provide: EventService, useValue: widgetEnv.eventService},
      {provide: FormContext, useValue: null},
      gravityTestbed2({ boards }),
    ]
  });
  if (!!override) {
    override(TestBed);
  }
  const fixture = TestBed.createComponent(WidgetHostHostComponent);
  fixture.componentInstance.config = widgetEnv.getWidget();
  fixture.componentInstance.viewMode = widgetEnv.getViewModeObs();
  fixture.autoDetectChanges(true);
  return fixture;
}

export class WidgetTestEnvironment<TConfig extends WidgetConfig, TComponent = Type<any>> {

  private state = new StateContainer<WidgetTestState<TConfig>>({
    widgetConfig: undefined,
    viewMode: 'view',
    filters: {},
    pageInfo: { title: 'Test page for testing' },
  });
  private _fixture: ComponentFixture<WidgetHostHostComponent>;
  private _component: TComponent;
  eventService = new TestEventService();

  // these could be settable / handled properly - haven't seen a use case yet
  cecid: 'testuser';
  fullName: 'Test User';
  teams: ['readonly'];

  set config(config: TConfig) {
    this.state.addModifier({widgetConfig: freeze<TConfig>(config)});
  }

  widgetConfig():Observable<TConfig> {
    return this.state.select(s => s.widgetConfig);
  }

  set viewMode(mode: WidgetMode) {
    this.state.addModifier({viewMode: mode});
  }

  get pageInfo(): Observable<Partial<PageInfo>> {
    return this.state.select(s => s.pageInfo);
  }

  get fixture(): ComponentFixture<WidgetHostHostComponent> {
    return this._fixture;
  }

  get component(): TComponent {
    return this._component;
  }

  constructor(options: WidgetTestOptions<TConfig, TComponent>) {
    this.state.set({widgetConfig: freeze<TConfig>(options.config)});
    this._fixture = getWidgetFixture(
      this,
      options.declarations,
      options.boards,
      options.override
    );
    const debugElement: DebugElement = this._fixture.debugElement.query(
      By.directive(options.componentType)
    );
    this._component = debugElement?.componentInstance;
    this.setDataContext(options.dataContextConfig ?? DATA_CONFIG_DEFAULT_CARDS);
  }

  nativeElementbyCss(selector: string) {
    const query = this.fixture.debugElement.query(By.css(selector));
    return query?.nativeElement ?? null;
  }

  debugElementsbyCss(selector: string) {
    return this.fixture.debugElement.queryAll(By.css(selector));
  }

  setFilter(filter: Partial<FilterState>) {
    this.state.addModifier({filters: {[filter.id]: filter}});
  }

  getWidget() {
    return this.state.select(s => s.widgetConfig);
  }

  getViewModeObs() {
    return this.state.select(s => s.viewMode);
  }

  watchFilterState(id: string) {
    return this.state.select(s => s.filters[id]);
  }

  getHarness<V extends WidgetHarness>(type): Promise<V> {
    return TestbedHarnessEnvironment.loader(this.fixture).getHarness(type);
  }

  setDataContext(config) {
    this._fixture.componentInstance.setContextConfig(config);
  }

}
