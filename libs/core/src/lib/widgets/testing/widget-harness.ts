import { ComponentHarness, TestElement } from '@angular/cdk/testing';


export class WidgetHarness extends ComponentHarness {
  static hostSelector = 'bmi-widget-host';
  async getDisplayText(selector: string): Promise<string> {
    const boxLocator = this.locatorFor(selector);
    return (await boxLocator()).text();
  }
  async getDisplayTexts(selector: string): Promise<string[]> {
    return this.locatorForAll(selector)()
    .then(
      (testElements: TestElement[]) => Promise.all(testElements.map(
        (element: TestElement) => element.text()
      ))
    );
  }

}
