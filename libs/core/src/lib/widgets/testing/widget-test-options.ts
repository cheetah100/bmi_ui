import { FakeBoard, CardFields, makeBoard, BoardConfigBuilder } from '@bmi/gravity-services';
import { ObjectMap } from '@bmi/utils';
import { WidgetConfig } from '../../widget-config';
import { WidgetMode } from '../../widget-context';
import { FilterState } from '../../filters/filter';
import { PageInfo } from '../../page-config';
import { Type } from '@angular/core';
import { PageDataConfig } from '../../page-data-context/page-data-config';


/**
 * @param componentType widget component type. Used to get env.component for access to methods etc
 * @param boards set up board schema and card data
 * @param declarations anything additional to the widget component.
 * @param config config for the widget being tested
 * @param dataContextConfig config for data context
 * @param override override tested widget setup. Example: overrideComponent
 */
export interface WidgetTestOptions<TConfig extends WidgetConfig, TComponent> {
  config: TConfig;
  componentType: Type<TComponent>;
  dataContextConfig?: PageDataConfig;
  declarations?: Type<any>[];
  boards?: FakeBoard<CardFields>[];
  override?: (testBed: any) => any;
}

export interface WidgetTestState<TConfig extends WidgetConfig> {
  widgetConfig: TConfig;
  viewMode: WidgetMode;
  filters: ObjectMap<Partial<FilterState>>;
  pageInfo: Partial<PageInfo>;
}

export const DEFAULT_TEST_BOARDS = [
  makeBoard({
    config: new BoardConfigBuilder('test')
      .addField('name')
      .addField('cancelled', f => f.type('BOOLEAN'))
      .addField('value', f => f.type('NUMBER')),

    cards: [
      {id: '1', title: 'One', fields: {name: 'Dog', cancelled: false, value: 9001}},
      {id: '2', title: 'Two', fields: {name: 'Wayne', cancelled: false, value: 7334}},
      {id: '3', title: 'Three', fields: {name: 'Beelzebub', cancelled: false, value: 0}},
    ]
  }),
];

export const DATA_CONFIG_DEFAULT_CARDS = {
  cards: {
    type: 'card-list',
    board: 'test',
    conditions: []
  }
}
