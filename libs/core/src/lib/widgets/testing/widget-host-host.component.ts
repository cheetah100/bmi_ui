import { Component } from '@angular/core';
import { PageDataContext } from '../../page-data-context/page-data-context';

@Component({ template: `
<bmi-widget-host
  [config]="config | async"
  [mode]="viewMode | async"
>
</bmi-widget-host>
` })
export class WidgetHostHostComponent {
  config;
  viewMode;

  constructor(
    private pageDataContext: PageDataContext
  ) {}

  setContextConfig(config) {
    this.pageDataContext.setConfig(config);
  }

}
