
import { Component, OnChanges, Input } from '@angular/core';

@Component({
  selector: 'bmi-infobox-progress-bar',
  template: `
  <div
    [attr.data-percentage]="roundedValue"
    [ngClass]="{ 'progressbar--small': sizeMap.small, 'progressbar--large': sizeMap.large, 'progressbar': true }">
      <div class="progressbar__fill" [style.background-color]="color"></div>
      <div class="progressbar__label">{{ labelText }}</div>
  </div>
`
})
export class InfoboxWidgetProgressBarComponent implements OnChanges {

  @Input() value: number;
  @Input() displayValue?: string = undefined;
  @Input() size: string;
  @Input() color: string;
  roundedValue: number;

  sizeMap = {
    small: false,
    medium: true,
    large: false
  };

  get labelText() {
    const displayValue = this.displayValue;
    if (displayValue !== undefined && displayValue !== null) {
      return displayValue;
    } else {
      return this.value;
    }
  }

  ngOnChanges() {
    this.roundedValue = Math.round(this.value);
    Object.keys(this.sizeMap).forEach(key => this.sizeMap[key] = false);
    this.sizeMap[this.size] = true;
  }

}
