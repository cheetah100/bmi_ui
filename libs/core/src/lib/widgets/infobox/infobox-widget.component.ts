import { Component, OnInit } from '@angular/core';

import { of } from 'rxjs';
import { map, switchMap, publishReplay, refCount } from 'rxjs/operators';

import { WidgetContext } from '../../widget-context';
import { PageDataContext } from '../../page-data-context/page-data-context';

import { InfoboxWidgetConfig, DEFAULT_STYLE_OPTIONS } from './infobox-config';

@Component({
  selector: 'bmi-infobox-widget',
  templateUrl: './infobox-widget.component.html',
  styleUrls: ['./infobox-widget.component.scss']
})
export class InfoboxWidgetComponent implements OnInit {

  visualizationType: string;
  size: string;

  readonly value = this.context.widgetConfig.pipe(
    switchMap(config => this.binder.bind(config.fields.value)),
    publishReplay(1),
    refCount()
  );

  displayValue = this.context.widgetConfig.pipe(
    switchMap(config => config.fields.displayValue ? this.binder.bind(config.fields.displayValue) : of(undefined)),
    publishReplay(1),
    refCount()
  );

  readonly isArray = this.value.pipe(map(value => Array.isArray(value)));

  readonly styleOptions = this.context.widgetConfig.pipe(
    map(config => ({ ...DEFAULT_STYLE_OPTIONS, ...(config.fields.styleOptions || {}) })),
    publishReplay(1),
    refCount(),
  );

  readonly color = this.styleOptions.pipe(
    switchMap(styleOptions => this.binder.bind(styleOptions.color)),
    publishReplay(1),
    refCount()
  );

  constructor(
    private context: WidgetContext<InfoboxWidgetConfig>,
    private binder: PageDataContext,
  ) { }

  ngOnInit() {
    this.context.widgetConfig.subscribe((config: InfoboxWidgetConfig) => {
      this.visualizationType = (config.fields.styleOptions) ? config.fields.styleOptions.visualization : '';
      this.size = (config.fields.styleOptions) ? config.fields.styleOptions.size : '';
    });
  }
}
