import { WidgetConfig } from '../../widget-config';
import { ParameterBinding, FormatterBinding } from '../../page-data-context/parameter-binding';
import { ConstValueFormatterConfig } from '../../formatters/const-value-formatter.service';
import { ColorZone } from '../../util/color-zone';


export interface InfoboxWidgetFields {
  value: ParameterBinding;
  displayValue?: ParameterBinding;
  styleOptions?: InfoboxStyleOptions;
}


export interface InfoboxStyleOptions {
  visualization?: 'text' | 'bar' | 'gauge';
  size?: 'small' | 'medium' | 'large';
  zones?: ColorZone[];
  color?: ParameterBinding;
}


export type InfoboxWidgetConfig = WidgetConfig<InfoboxWidgetFields>;


export const DEFAULT_STYLE_OPTIONS: Partial<InfoboxStyleOptions> = {
  visualization: 'text',
  size: 'medium',
  color: <FormatterBinding<ConstValueFormatterConfig<string>>>{
    bindingType: 'formatter',
    formatterType: 'color-picker',
    value: '#FF7300'
  }
};

