
export const FormatOptions = [{
  text: 'Integer',
  value: 'integer'
}, {
  text: 'Percentage',
  value: 'percentage'
}, {
  text: 'Decimal',
  value: 'decimal'
}, {
  text: 'Metric Shorthand',
  value: 'metric_shorthand'
}];

export const DataVisualOptions = [{
  text: 'Text Only',
  value: 'text'
}, {
  text: 'Progress Bar',
  value: 'bar'
}, {
  text: 'Gauge',
  value: 'gauge'
}];

export const SizeOptions = [{
  text: 'Small',
  value: 'small'
}, {
  text: 'Medium',
  value: 'medium'
}, {
  text: 'Large',
  value: 'large'
}];
