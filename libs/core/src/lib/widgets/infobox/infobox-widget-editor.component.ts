import { Component, OnInit } from '@angular/core';
import { TypedFormControl, TypedFormGroup } from '@bmi/ui';
import { Validators } from '@angular/forms';

import { WidgetContext } from '../../widget-context';
import { PageInfoService } from '../../page/page-info.service';
import { SizeOptions, DataVisualOptions } from './infobox-editor-optionlists';
import { InfoboxWidgetConfig, InfoboxWidgetFields, DEFAULT_STYLE_OPTIONS } from './infobox-config';
import { distinctUntilNotEqual } from '../../util/distinct-until-not-equal';

@Component({
  selector: 'bmi-infobox-widget-editor',
  templateUrl: './infobox-widget-editor.component.html',
  styleUrls: ['../../editor-sidebar/editor-sidebar.scss']
})
export class InfoboxWidgetEditorComponent implements OnInit {

  dataVisualOptions = DataVisualOptions;
  sizeOptions = SizeOptions;
  moduleId: string = null;

  readonly infoboxForm = new TypedFormGroup<InfoboxWidgetFields>({
    value: new TypedFormControl(null, [Validators.required]),
    displayValue: new TypedFormControl(null),
    styleOptions: new TypedFormGroup({
      visualization: new TypedFormControl(null),
      size: new TypedFormControl(null),
      color: new TypedFormControl(null),
    })
  });

  constructor(
    private widgetContext: WidgetContext<InfoboxWidgetConfig>,
    private pageInfo: PageInfoService
  ) { }

  ngOnInit() {
    this.pageInfo.pageInfo.subscribe(page => {
      this.moduleId = page.applicationId;
    });

    this.widgetContext.widgetConfig.pipe(
      distinctUntilNotEqual()
    )
      .subscribe(config => {
        this.infoboxForm.patchValue({
          ...config.fields,
          styleOptions: { ...DEFAULT_STYLE_OPTIONS, ...(config.fields.styleOptions || {}) },
        }, { emitEvent: false });
      });

    this.infoboxForm.valueChanges.pipe(
      distinctUntilNotEqual()
    )
      .subscribe(update => {
        this.widgetContext.updateWidgetConfig(config => config.fields = update);
      });
  }

}
