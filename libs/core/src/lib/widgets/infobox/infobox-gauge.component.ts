
import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'bmi-infobox-gauge',
  template: `
  <section style="text-align: center">
    <div
      [ngClass]="{ 'gauge--small': sizeMap.small, 'gauge--medium': sizeMap.medium, 'gauge--large': sizeMap.large, 'gauge': true }"
      [attr.data-percentage]="roundedValue">
      <div class="gauge__circle">
          <div class="mask full">
              <div class="fill" [style.background-color]="color"></div>
          </div>
          <div class="mask half">
              <div class="fill" [style.background-color]="color"></div>
              <div class="fill fix" [style.background-color]="color"></div>
          </div>
      </div>
      <div class="gauge__inset">
          <div class="gauge__percentage">{{ labelText }}</div>
      </div>
    </div>
  </section>
  `,
  styleUrls: ['./cui-gauge-overrides.scss'],
})
export class InfoboxWidgetGaugeComponent implements OnChanges {

  @Input() value: number;
  @Input() displayValue?: string = undefined;
  @Input() size;
  @Input() color;
  roundedValue: number;

  sizeMap = {
    small: false,
    medium: true,
    large: false
  };


  get labelText() {
    const displayValue = this.displayValue;
    if (displayValue !== undefined && displayValue !== null) {
      return displayValue;
    } else {
      return Math.round(this.value);
    }
  }

  ngOnChanges() {
    this.roundedValue = Math.round(this.value);
    Object.keys(this.sizeMap).forEach(key => this.sizeMap[key] = false);
    this.sizeMap[this.size] = true;
  }

}
