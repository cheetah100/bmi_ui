import { NgModule, Provider } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { SharedUiModule } from '@bmi/ui';
import { BmiFormsModule } from '../../forms/bmi-forms.module';
import { BmiCoreEditorComponentsModule } from '../../editor-components/bmi-core-editor-components.module';

import { BmiCoreModule } from '../../bmi-core.module';
import { defineWidget } from '../../util/widget-def-helpers';


import { CombinedEditorFormControlWidgetComponent } from './combined-editor-form-control-widget.component';


/**
 * Defines an editor form control from the combined widget implementation.
 *
 * As many of these widgets are basically identical wrapping around a component
 * this helps to cut back on boilerplate.
 */
function defineEditorFromCombinedWidget(controlType: string) {
  return defineWidget({
    widgetType: `editor-form-control:${controlType}`,
    component: CombinedEditorFormControlWidgetComponent,
    options: {
      controlType: controlType
    }
  });
}


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedUiModule,
    RouterModule,
    BmiCoreModule,
    BmiFormsModule,
    BmiCoreEditorComponentsModule,
  ],

  declarations: [
    CombinedEditorFormControlWidgetComponent,
  ],


  providers: [
    defineEditorFromCombinedWidget('event-editor'),
    defineEditorFromCombinedWidget('binding-picker'),
    defineEditorFromCombinedWidget('formatter'),
    defineEditorFromCombinedWidget('route-selector'),
    defineEditorFromCombinedWidget('action-buttons'),
  ]
})
export class EditorFormControlRegistry {}
