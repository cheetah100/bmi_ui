import { Component, Optional } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { combineLatest, of } from 'rxjs';
import { distinctUntilChanged, map, switchMap } from 'rxjs/operators';

import { FormContext, FormField } from '../../forms/form-context';
import { FormControlWidgetConfig } from '../../forms/form-control-widget-config';
import { WidgetContext } from '../../widget-context';


/**
 * A widget that implements several useful editor widgets, but many of these
 * share a lot of boilerplate without any interesting configuration.
 */
@Component({
  template: `
    <ui-form-field [ngSwitch]="controlType" [label]="label | async" [required]="required | async">
      <bmi-binding-picker *ngSwitchCase="'binding-picker'" [formControl]="control | async"></bmi-binding-picker>
      <ui-dynamic-editor-list *ngSwitchCase="'event-editor'" bmiUsingDynamicEditorsFor="event-action" [formControl]="control | async" addMenuLabel="Add event action"></ui-dynamic-editor-list>
      <bmi-core-route-selector *ngSwitchCase="'route-selector'" [formControl]="control | async"></bmi-core-route-selector>
      <bmi-formatter-picker *ngSwitchCase="'formatter'" [formControl]="control | async"></bmi-formatter-picker>
      <bmi-button-toolbar-editor *ngSwitchCase="'action-buttons'" [formControl]="control | async"></bmi-button-toolbar-editor>
    </ui-form-field>
  `
})
export class CombinedEditorFormControlWidgetComponent {
  constructor (
    private widgetContext: WidgetContext<FormControlWidgetConfig>,
    @Optional() private formContext: FormContext,
  ) {}

  controlType = this.widgetContext.widgetDefinition?.options?.controlType as string;

  field = this.widgetContext.widgetConfig.pipe(
    map(config => config.fields.fieldId),
    distinctUntilChanged(),
    switchMap(fieldId => this.formContext ? this.formContext.get(fieldId) : of(<FormField>undefined))
  );

  required = this.widgetContext.widgetConfig.pipe(
    map( config => config.fields.required || false)
  );

  control = this.field.pipe(
    map(field => field ? field.control : new FormControl()),
  );

  label = combineLatest([
    this.field,
    this.widgetContext.widgetConfig
  ]).pipe(
    map(([field, config]) => (field && field.label) || 'No label')
  );

}
