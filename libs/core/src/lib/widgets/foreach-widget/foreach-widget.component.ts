import { Component } from '@angular/core';
import { combineLatestDeferred, ObjectMap } from '@bmi/utils';
import mapValues from 'lodash-es/mapValues';
import range from 'lodash-es/range';
import { Observable, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { ObjectDataSource } from '../../data-sources/object-data-source';
import { DataSource, DataSourceWithListApi } from '../../data-sources/types';
import { PageDataContext } from '../../page-data-context/page-data-context';
import { bindDataMap } from '../../util/bind-data-map';
import { distinctUntilNotEqual } from '../../util/distinct-until-not-equal';
import { WidgetConfig } from '../../widget-config';
import { WidgetContext } from '../../widget-context';
import { ForeachWidgetFields } from './foreach-widget-config';


interface LoopInstance {
  trackByValue: any;
  data: ObjectMap<any>;
  dataContextConfig: ObjectMap<DataSource>;
}


@Component({
  selector: 'bmi-foreach-widget',
  templateUrl: './foreach-widget.component.html',
  styleUrls: ['./foreach-widget.component.scss']
})
export class ForeachWidgetComponent {

  instances = this.context.widgetConfig.pipe(
    distinctUntilNotEqual(),
    switchMap(config => this.binder.getFetcher(config.fields.dataSourceId).pipe(
      switchMap(ds => ds && ds.bindThis ? this.getLoopInstances(ds as DataSourceWithListApi, config) : of(<LoopInstance[]>[]))
    ))
  );

  templateConfig = this.context.widgetConfig.pipe(
    map(config => config.fields.template)
  );

  dataSourceName = this.context.widgetConfig.pipe(
    map(config => config.fields.dataSourceId || 'No datasource selected')
  );

  templateDataContextConfig = this.context.widgetConfig.pipe(
    distinctUntilNotEqual(),
    map(config => ({
      'this': new ObjectDataSource(of(mapValues(config.fields.bindings || {}, () => undefined)))
    }))
  );

  gridColumns = this.context.widgetConfig.pipe(
    map(config => Math.max(1, Math.min(config.fields.columns || 3, 6)))
  );


  placeholderColumns = this.gridColumns.pipe(
    map(columns => range(Math.max(0, columns - 1))),
  );

  isEditMode = this.context.isEditMode;

  constructor(
    private context: WidgetContext<WidgetConfig<ForeachWidgetFields>>,
    private binder: PageDataContext
  ) {}

  updateTemplateConfig(config: WidgetConfig) {
    this.context.updateWidgetConfig(c => c.fields.template = config);
  }

  private getLoopInstances(ds: DataSourceWithListApi, config: WidgetConfig<ForeachWidgetFields>): Observable<LoopInstance[]> {
    return ds.bindThis(Object.values(config.fields.bindings)).pipe(
      switchMap(boundList => combineLatestDeferred(
        boundList.rows.map(r => bindDataMap(r, config.fields.bindings))
      )),
      map(dataValues => dataValues.map((data, index) => <LoopInstance>{
        trackByValue: index,
        data: data,
        dataContextConfig: {
          this: new ObjectDataSource(of(data))
        }
      }))
    )
  }

  trackByLoopInstance(index: number, instance: LoopInstance) {
    return instance && instance.trackByValue;
  }
}
