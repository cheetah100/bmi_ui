import { WidgetHarness } from '../testing/widget-harness';
import { WidgetTestEnvironment } from '../testing/widget-test-environment';
import { WidgetTestOptions } from "../testing/widget-test-options";
import { ForeachWidgetComponent } from './foreach-widget.component';
import { ForeachWidgetFields } from './foreach-widget-config';
import { WidgetConfig } from '../../widget-config';
import { MarkdownWidgetComponent } from '../markdown/markdown-widget.component';
import { By } from '@angular/platform-browser';

type ForeachWidgetConfig = WidgetConfig<ForeachWidgetFields>;

const dataConfig = {
  cards: {
    type: 'card-list',
    board: 'test',
    conditions: []
  }
}
const templateConfig = {
  widgetType: 'person',
  fields: {
    cecId: {
      bindingType: 'data-path',
      path: 'this.pname'
    },
    name: {
      bindingType: 'data-path',
      path: 'this.pname'
    }
  }
};
const foreachConfig: ForeachWidgetConfig = {
  widgetType: 'foreach',
  fields: {
    columns: 1,
    dataSourceId: 'cards',
    bindings: {
      pvalue: {
        bindingType: 'data-path',
        path: 'this.value'
      },
      pname: {
        bindingType: 'data-path',
        path: 'this.name'
      }
    },
    template: templateConfig
  },
};
const envOptions: WidgetTestOptions<ForeachWidgetConfig, ForeachWidgetComponent> = {
  config: foreachConfig,
  componentType: ForeachWidgetComponent,
  declarations: [MarkdownWidgetComponent],
  dataContextConfig: dataConfig
}

let harness: WidgetHarness;
let env: WidgetTestEnvironment<ForeachWidgetConfig, ForeachWidgetComponent>;

describe('ForeachWidget', () => {

  beforeEach(async () => {
    env = new WidgetTestEnvironment<ForeachWidgetConfig, ForeachWidgetComponent>(envOptions);
    harness = await env.getHarness(WidgetHarness);
  });

  afterEach(async () => {
    env.fixture.destroy();
  });

  describe('Edit mode', () => {
    beforeEach(() => {
      env.viewMode = 'edit';
    });
    it('should show the no datasource message for no config datasource', async() => {
      const config = Object.assign({}, foreachConfig);
      config.fields = Object.assign({}, foreachConfig.fields);
      config.fields.dataSourceId = undefined;
      env.config = config;
      const display = await harness.getDisplayText('.foreach__template-title');
      expect(display).toBe(`Loop Template (No datasource selected)`);
    });
    it('should show the add template message for no config template', async () => {
      const config = Object.assign({}, foreachConfig);
      config.fields = Object.assign({}, foreachConfig.fields);
      config.fields.template = undefined;
      env.config = config;
      const display = await harness.getDisplayText('.foreach__grid');
      expect(display).toBe(`Loop Template (cards) Add Widget`);
    })
    it('should show the default message otherwise', async () => {
      env.config = foreachConfig;
      const display = await harness.getDisplayText('.foreach__template-title');
      expect(display).toBe(`Loop Template (cards)`);
    });
  });

  describe('View mode', () => {
    it('should show looped widgets in view mode', async () => {
      env.viewMode = 'view';
      env.fixture.whenStable().then(() => {
        const items = env.fixture.debugElement.queryAll(By.css('.profile-image__info'));
        expect(items.length).toBe(3);
      });
      const display = await harness.getDisplayText('.foreach__grid');
      expect(display).toBe(`DogWayneBeelzebub`);
    });
  });

});
