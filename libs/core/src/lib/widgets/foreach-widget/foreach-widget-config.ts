import { ObjectMap } from '@bmi/utils';
import { WidgetConfig } from '../../widget-config';
import { ParameterBinding } from '../../page-data-context/parameter-binding';


export interface ForeachWidgetFields {
  template: WidgetConfig;
  dataSourceId: string;
  bindings: ObjectMap<ParameterBinding>;
  columns: number;
}

