import { Component, OnInit } from '@angular/core';

import { TypedFormGroup, TypedFormControl } from '@bmi/ui';

import { WidgetConfig } from '../../widget-config';
import { WidgetContext } from '../../widget-context';
import { ForeachWidgetFields } from './foreach-widget-config';

@Component({
  templateUrl: './foreach-widget-editor.component.html'
})
export class ForeachWidgetEditorComponent implements OnInit {
  form = new TypedFormGroup<ForeachWidgetFields>({
    dataSourceId: new TypedFormControl(null),
    bindings: new TypedFormControl({}),
    template: new TypedFormControl(null),
    columns: new TypedFormControl(null),
  });

  constructor (
    private widgetContext: WidgetContext<WidgetConfig<ForeachWidgetFields>>
  ) {}

  ngOnInit() {
    this.widgetContext.widgetConfig.subscribe(config => this.form.patchValue(config.fields, {emitEvent: false}));
    this.form.valueChanges.subscribe(fields => this.widgetContext.updateWidgetConfig(c => c.fields = fields));
  }
}
