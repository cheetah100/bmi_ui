import { WidgetHarness } from '../testing/widget-harness';
import { DATA_CONFIG_DEFAULT_CARDS, WidgetTestOptions } from '../testing/widget-test-options';
import { WidgetTestEnvironment } from '../testing/widget-test-environment';
import { BasicChartWidgetConfig } from './basic-chart-widget-config';
import { BasicChartWidgetComponent } from './basic-chart-widget.component';

const envOptions: WidgetTestOptions<BasicChartWidgetConfig, BasicChartWidgetComponent> = {
  config: {
    widgetType: 'basic-chart',
    fields: {
      chartSettings: {
        xAxisTitle: 'Test X',
        yAxisTitle: 'Test Y',
        xAxisType: null,
        yAxisType: null,
        polar: false,
        stacked: null,
        seriesGroups: [
          {
            dataSeries: {
              dataSourceId: 'cards',
              seriesId: {
                bindingType: 'data-path',
                path: 'this.name'
              },
              label: {
                bindingType: 'data-path',
                path: 'this.name'
              },
              fields: {
                x: {
                  bindingType: 'data-path',
                  path: 'this.value'
                },
                y: {
                  bindingType: 'data-path',
                  path: 'this.value'
                },
                name: {
                  bindingType: 'data-path',
                  path: 'this.name'
                }
              }
            },
            selectedOnly: false,
            seriesSettings: [],
            type: 'line'
          }
        ]
      }
    }
  },
  componentType: BasicChartWidgetComponent,
  dataContextConfig: DATA_CONFIG_DEFAULT_CARDS,
}
let harness: WidgetHarness;
let env: WidgetTestEnvironment<BasicChartWidgetConfig, BasicChartWidgetComponent>;

/**
 * @todo
 * Incomplete. 
 * Tests are working and render a graph, BUT
 * While this test provides some coverage, it's only currently testing for 
 * legend display.
 * We don't want to test highcharts, but we should be testing chart / series
 * generation properly.
 */

describe('BasicChartWidget', () => {

  beforeEach(async () => {
    env = new WidgetTestEnvironment<BasicChartWidgetConfig, BasicChartWidgetComponent>(envOptions);
    harness = await env.getHarness(WidgetHarness);
  });

  afterEach(async () => {
    env.fixture.destroy();
  });

  describe('legend creation', () => {
    it('should show the series names in the chart legend', async () => {
      const legends = await harness.getDisplayTexts('.highcharts-legend-item');
      expect(legends).toEqual(['Dog','Wayne','Beelzebub']);
    })
  });

});
