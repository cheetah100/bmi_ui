import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';

import { ChartDataWithSeries } from '@bmi/legacy-charts';
import { TypedFormGroup, TemplatedFormArray, Option, TypedFormControl } from '@bmi/ui';

import { WidgetContext } from '../../widget-context';
import { BasicChartSettings, SeriesGroupConfig } from '../../charts/basic-chart-config';
import { BasicChartWidgetConfig } from './basic-chart-widget-config';
import { CHART_SERIES_TYPE_OPTIONS, CHART_STACKING_OPTIONS, GROUP_OPTIONS, VALUE_OPTIONS } from '../../charts/basic-chart-contstants';


@Component({
  templateUrl: './basic-chart-widget-editor.component.html',
  styles: [`
    .chart-settings {
      display: grid;
      grid-template-columns: 1fr 1fr;
      gap: var(--gra-spacing-half);
      margin-bottom: var(--gra-spacing-half);
    }
    .series-group__header {
      display: flex;
      align-items: center;
      margin-bottom: var(--gra-spacing-qtr);
    }

    .series-group__title {
      flex: 1;
    }

    ui-collapsible-section {
      background: rgba(0,0,0,.06);
      display: block;
      margin: 0 -18px -4px -8px;
      padding: 4px 8px;
    }
    h2 {
      padding: var(--gra-spacing-half) 0;
      border-top: 1px solid var(--gra-color--gray-400);
      border-bottom: 1px solid var(--gra-color--gray-400);
    }
  `],
})
export class BasicChartWidgetEditorComponent implements OnInit, OnDestroy {

  errorMessage: string = null;
  seriesMessage: string = null;

  readonly defaultColor = null;

  readonly typeOptions = CHART_SERIES_TYPE_OPTIONS;
  stackingOptions: Option[] = CHART_STACKING_OPTIONS;

  chartData: ChartDataWithSeries[];

  private subscription = new Subscription();
  readonly GROUP_OPTIONS = GROUP_OPTIONS;
  readonly VALUE_OPTIONS = VALUE_OPTIONS;

  get data(): ChartDataWithSeries {
    return this.chartData && this.chartData[0];
  }

  readonly form = new TypedFormGroup<BasicChartSettings>({
    xAxisTitle: new FormControl(null),
    xAxisType: new FormControl('category'),
    yAxisTitle: new FormControl(null),
    yAxisType: new FormControl('count'),
    polar: new FormControl(false),
    stacked: new FormControl(null),
    seriesGroups: new TemplatedFormArray<SeriesGroupConfig>(() => new TypedFormControl<SeriesGroupConfig>(null))
  });

  get seriesGroups() {
    return this.form.controls.seriesGroups as TemplatedFormArray<SeriesGroupConfig>;
  }

  constructor(
    private widgetContext: WidgetContext<BasicChartWidgetConfig>,
  ) { }

  ngOnInit() {

    this.subscription.add(this.widgetContext.widgetConfig.subscribe(config => {
      const QUIET = { emitEvent: false };
      const settings = config.fields.chartSettings;
      this.form.patchValue(settings, QUIET);
    }));

    this.subscription.add(this.form.valueChanges.subscribe(updatedValue => 
      this.widgetContext.updateWidgetConfig(c => c.fields.chartSettings = updatedValue)
    ));

  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  addSeriesGroup() {
    this.seriesGroups.pushValue({
      dataSeries: null,
      selectedOnly: false,
      seriesSettings: [],
      type: null
    });
  }

  deleteSeriesGroup(index: number) {
    this.seriesGroups.removeAt(index);
  }

}
