import { WidgetConfig } from '../../widget-config';

import { BasicChartSettings } from '../../charts/basic-chart-config';
import { DataSeriesDef } from '../../data-series/data-series-def';

export type BasicChartWidgetConfig = WidgetConfig<BasicChartWidgetFields>;


export interface BasicChartWidgetFields {
  chartSettings: BasicChartSettings;
}

export const seriesTypes = [
  'area',
  'areaspline',
  'column',
  'line',
  'scatter',
  'spline',
];
