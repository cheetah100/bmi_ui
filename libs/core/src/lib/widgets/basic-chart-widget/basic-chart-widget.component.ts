import { Component, OnDestroy, OnInit } from '@angular/core';
import { CardCacheService, CardData } from '@bmi/gravity-services';
import { Options as HighchartsOptions } from 'highcharts';
import { combineLatest, iif, Observable, of, Subscription, timer } from 'rxjs';
import { catchError, filter, map, switchMap, take, throttle } from 'rxjs/operators';
import { ChartBuildService } from '../../charts/highcharts-viewer/chart-build-service';
import { DataSeries } from '../../data-series/data-series';
import { DataSeriesDef } from '../../data-series/data-series-def';
import { generateSeriesFromListSource } from '../../data-series/generate-data-series';
import { PageDataContext } from '../../page-data-context/page-data-context';
import { distinctUntilNotEqual } from '../../util/distinct-until-not-equal';
import { WidgetContext } from '../../widget-context';
import { BasicChartWidgetConfig } from './basic-chart-widget-config';


@Component({
  selector: 'bmi-basic-chart-widget',
  templateUrl: './basic-chart-widget.component.html',
  styleUrls: ['./basic-chart-widget.component.scss']
})
export class BasicChartWidgetComponent implements OnInit, OnDestroy {

  private subscription: Subscription;
  chartConfig: HighchartsOptions;

  isLoading = true;
  errorMessage: string = null;

  constructor(
    private widgetContext: WidgetContext<BasicChartWidgetConfig>,
    private dataContext: PageDataContext,
    private chartBuildService: ChartBuildService,
  ) { }

  ngOnInit() {
    this.subscription = this.widgetContext.widgetConfig.pipe(
      filter(config => !!config),
      distinctUntilNotEqual(),
      // ThrottleTime is broken in RxJS 6.4 or whatever. The generic throttle
      // has a different implementation so I'm hoping it's not broken too when
      // there is a single event with trailing mode enabled.
      //
      // This was previously a debounceTime(500) but that introduces a huge
      // delay when showing a chart widget which is a bit unacceptable.
      throttle(() => timer(100), {leading: true, trailing: true}),
      switchMap(config => {
        this.isLoading = true;
        return this.prepareChart(config).pipe(
          catchError(err => {
            console.error(err);
            this.errorMessage = 'Error loading chart';
            return of(<HighchartsOptions>null);
          })
        )
      }),
    ).subscribe(chartConfig => {
      this.isLoading = false;
      this.errorMessage = null;
      this.chartConfig = chartConfig;
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  prepareChart(config: BasicChartWidgetConfig): Observable<HighchartsOptions> {
    // TODO: we need another custom combineLatest operator that works correctly
    // on 0 length arrays, this kind of code is very awkward and error prone
    const seriesGroups = config.fields.chartSettings.seriesGroups ?? [];
    const seriesDataSources = seriesGroups.map(sg => this.getSeriesData(sg.dataSeries));
    return (seriesDataSources.length > 0 ? combineLatest(seriesDataSources) : of(<DataSeries[][]>[])).pipe(
      switchMap(chartData => this.chartBuildService.chartOptions({
        chartSettings: config.fields.chartSettings,
        dataSeries: chartData
      }))
    );
  }

  getSeriesData(dsd: DataSeriesDef): Observable<DataSeries[]> {
    if (dsd) {
      return this.dataContext.getFetcher(dsd.dataSourceId).pipe(
        switchMap(fetcher => fetcher ? generateSeriesFromListSource(fetcher, {
          fields: {
            ...dsd.fields,
            seriesId: dsd.seriesId,
            seriesLabel: dsd.label,
          }
        }) : of([]))
      );
    } else {
      return of([]);
    }
  }

}
