import { ColorSeries } from './color-series';

describe('Color Series', () => {

  const hexRegex = /^#[0-9A-F]{6}$/;
  const cs = new ColorSeries();
  const inRange = (n: number): boolean => (n >= 0 && n <= 255);
  const isHex = (hex: string): boolean => hexRegex.test(hex);
  const areAll = (
    array: (string | number)[],
    test: (item: string | number) => boolean
  ): boolean => {
    return array.reduce(
      (a: boolean, c: string | number) => (a ? test(c) : false),
      true
    );
  };

  describe('rgbToHex()', () => {
    it('should convert an rgb array value to hex string correctly', () => {
      expect(cs.rgbToHex([0, 1, 2])).toBe('#000102');
      expect(cs.rgbToHex([15, 16, 17])).toBe('#0F1011');
      expect(cs.rgbToHex([253, 254, 255])).toBe('#FDFEFF');
    });
  });

  describe('rgbValue()', () => {
    it('should return a value within the range 0 to 255', () => {
      const values = [-101, -1, 0, 1, 3, 50, 99999999999].map(n => cs.rgbValue(n));
      expect(values.length).toBe(7);
      expect(areAll(values, inRange)).toBe(true);
    });
  });

  describe('nextDefault()', () => {
    it('should return a correct hex value that varies depending upon input array length', () => {
      const values = [];
      for (let i = 0; i < 20; i++) {
        values.push(cs.nextDefault(values.length));
      }
      expect(values.length).toBe(20);
      expect(areAll(values, isHex)).toBe(true);
      // all values are unique
      expect([...new Set(values)].length).toBe(20);
    });
  });

  describe('getSteps()', () => {
    cs.colors = [
      [255, 0, 0],
      [10, 200, 240],
    ];
    it('should return an array of hex values of correct length', () => {
      const values = cs.getSteps(8);
      expect(values.length).toBe(8);
      expect(areAll(values, isHex)).toBe(true);
    });
    it('should return a range of colors that take linear steps from start to end', () => {
      expect(cs.getSteps(3)[1]).toBe('#856478');
    });
  });

});
