import { Component, OnInit } from '@angular/core';
import { TypedFormGroup, TypedFormControl } from '@bmi/ui';

import { WidgetContext } from '../../widget-context';
import { WidgetConfig } from '../../widget-config';

import { PageEditorContext } from '../../page-editor/page-editor-context';
import { PageConfig, isValidPageConfig } from '../../page-config';
import { clipboardPasteText } from '../../util/clipboard';
import { cloneAndUpdate } from '../../util/clone-and-update';

import { PageLayoutConfig, PageLayoutWidgetFields } from './page-layout-config';


// tslint:disable-next-line:max-line-length
type PageConfigTransformFunction = (current: PageConfig<PageLayoutConfig>, pasted: PageConfig<PageLayoutConfig>) => PageConfig<PageLayoutConfig>;


@Component({
  selector: 'bmi-core-page-layout-widget-editor',
  templateUrl: './page-layout-widget-editor.component.html'
})
export class PageLayoutWidgetEditorComponent implements OnInit {
  widgetConfig: PageLayoutConfig;

  form = new TypedFormGroup<Partial<PageLayoutWidgetFields>>({
    header: new TypedFormControl(null),
    breadcrumbs: new TypedFormControl(null),
    mainFilter: new TypedFormControl(null),
    body: new TypedFormControl(null),
    loadingValues: new TypedFormControl(null),
    requiredMessage: new TypedFormControl(null),
    requiredValues: new TypedFormControl(null),
    pageNavigation: new TypedFormControl(null),
    showBreadcrumbs: new TypedFormControl(null),
    showPageNavigation: new TypedFormControl(null),
  });

  constructor(
    private widgetContext: WidgetContext<PageLayoutConfig>,
    private pageEditorContext: PageEditorContext,
  ) { }

  ngOnInit() {
    this.widgetContext.widgetConfig.subscribe(widgetConfig => {
      this.widgetConfig = widgetConfig;
      this.form.patchValue(widgetConfig.fields, { emitEvent: false });
    });

    this.form.valueChanges.subscribe(updatedFields => {
      this.widgetContext.updateWidgetConfig(config => {
        config.fields = {
          ...config.fields,
          ...updatedFields
        };
      });
    });
  }

  pastePageShell() {
    this.pasteAndUpdate((current, pastedPage) => {
      return cloneAndUpdate(
        current,
        page => {
          const currentRootWidget = page.ui.rootWidget;
          page.ui.dataContext = {
            ...page.ui.dataContext,
            ...pastedPage.ui.dataContext
          };

          page.ui.rootWidget.fields = {
            ...pastedPage.ui.rootWidget.fields,
            body: currentRootWidget.fields.body
          }
        }
      )
    })
  }

  pastePageContents() {
    this.pasteAndUpdate((current, pasted) => cloneAndUpdate(current, page => {
      page.ui.rootWidget.fields.body = pasted.ui.rootWidget.fields.body
    }));
  }

  pasteDataSources() {
    this.pasteAndUpdate((current, pasted) => cloneAndUpdate(current, page => {
      page.ui.dataContext = {
        ...page.ui.dataContext,
        ...pasted.ui.dataContext
      };
    }));
  }

  private pasteAndUpdate(
    transformFn: PageConfigTransformFunction) {
    clipboardPasteText()
      .then(pastedText => {
        const pastedPage = JSON.parse(pastedText) as PageConfig<PageLayoutConfig>;
        if (!isValidPageConfig(pastedPage)) {
          throw new Error('Page config is not valid. Please ensure the type is correct and it has a root widget config');
        }

        const pastedRootWidget = pastedPage.ui.rootWidget;
        if (pastedRootWidget.widgetType !== 'page-layout') {
          throw new Error('Unsupported page type: can only paste pages using the normal "page-layout" root widget');
        }

        this.pageEditorContext.undoController.apply(
          transformFn(
            this.pageEditorContext.pageConfigSnapshot as PageConfig<PageLayoutConfig>,
            pastedPage
          )
        );
      })
      .catch(error => {
        alert('Paste Error: ' + error);
      });
  }
}


