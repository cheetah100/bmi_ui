import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription, combineLatest } from 'rxjs';
import { map, switchMap, publishBehavior, refCount } from 'rxjs/operators';
import { combineLatestDeferred, ObjectMap } from '@bmi/utils';

import { WidgetConfig } from '../../widget-config';
import { WidgetContext } from '../../widget-context';
import { PageLayoutConfig } from './page-layout-config';

import { PageDataContext } from '../../page-data-context/page-data-context';
import { ParameterBinding } from '../../page-data-context/parameter-binding';
import { distinctUntilNotEqual } from '../../util/distinct-until-not-equal';
import { TypedFormGroup, TypedFormControl } from '@bmi/ui';


@Component({
  selector: 'bmi-page-layout-widget',
  templateUrl: './page-layout-widget.component.html',
  styleUrls: ['./page-layout-widget.component.scss']
})
export class PageLayoutWidgetComponent implements OnInit, OnDestroy {
  public headerWidgetConfig: WidgetConfig;
  public bodyWidgetConfig: WidgetConfig;
  public mainFilterWidgetConfig: WidgetConfig;
  public secondaryFilterConfig: WidgetConfig;
  public breadcrumbsWidgetConfig: WidgetConfig;
  public pageNavigationConfig: WidgetConfig;
  public showBreadcrumbs: boolean;
  public showPageNavigation: boolean;

  public isEditMode = this.context.isEditMode;
  // This one is updated on change
  public isViewShowing = false;

  private subscription = new Subscription();

  hasAllLoadingValues = this.hasAllBindings(config => config.fields.loadingValues);
  hasAllRequiredValues = this.hasAllBindings(config => config.fields.requiredValues);

  requiredMessage = this.context.widgetConfig.pipe(
    map(config => config.fields.requiredMessage || 'Page cannot be displayed until all required values are provided'),
  );

  bodyMode = combineLatest([
    this.hasAllRequiredValues,
    this.hasAllLoadingValues,
    this.isEditMode
  ]).pipe(
    map(([hasRequiredValues, hasLoadingValues, isEditMode]) => {
      if (isEditMode) {
        return 'normal';
      } else if (!hasRequiredValues) {
        return 'missing-required-values';
      } else if (!hasLoadingValues) {
        return 'loading';
      } else {
        return 'normal';
      }
    }),
    publishBehavior('loading'),
    refCount()
  );


  constructor(
    private context: WidgetContext<PageLayoutConfig>,
    private binder: PageDataContext
  ) { }

  ngOnInit() {

    this.subscription.add(this.context.widgetConfig.subscribe(config => {
      this.headerWidgetConfig = config.fields.header;
      this.bodyWidgetConfig = config.fields.body;
      this.mainFilterWidgetConfig = config.fields.mainFilter;
      this.secondaryFilterConfig = config.fields.secondaryFilter;
      this.breadcrumbsWidgetConfig = config.fields.breadcrumbs;
      this.pageNavigationConfig = config.fields.pageNavigation;
      this.showBreadcrumbs = config.fields.showBreadcrumbs;
      this.showPageNavigation = config.fields.showPageNavigation;
    }));

    this.subscription.add(this.context.isViewMode.subscribe(
      isView => this.isViewShowing = isView
    ));

  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  widgetConfigUpdated(section: keyof PageLayoutConfig['fields'], widgetConfig: WidgetConfig) {
    this.context.updateWidgetConfig(c => (c.fields[section] as WidgetConfig) = widgetConfig);
  }

  addSecondaryFilterConfig() {
    this.widgetConfigUpdated('secondaryFilter', {
      widgetType: 'filter-controls',
      fields: {
        columns: 1,
        filters: [{
          id: 'filterId',
          name: 'filterName',
          filterType: 'optionlist',
          controlType: 'single-select'
        }]
      }
    });
  }

  private hasAllBindings(selector: (config: PageLayoutConfig) => ParameterBinding[]): Observable<boolean> {
    return this.context.widgetConfig.pipe(
      map(config => selector(config) || []),
      distinctUntilNotEqual(),
      switchMap(bindings => combineLatestDeferred((bindings).map(b => this.binder.bind(b))).pipe(
        map(results => results.every(x => !!x))
      ))
    );
  }
}
