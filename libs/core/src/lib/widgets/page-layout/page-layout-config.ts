import { WidgetConfig } from '../../widget-config';
import { ParameterBinding } from '../../page-data-context/parameter-binding';

export interface PageLayoutWidgetFields {
  header: WidgetConfig;
  breadcrumbs: WidgetConfig;
  mainFilter: WidgetConfig;
  secondaryFilter?: WidgetConfig;
  pageNavigation?: WidgetConfig;
  body: WidgetConfig;

  requiredValues: ParameterBinding[];
  requiredMessage: string;
  loadingValues: ParameterBinding[];

  showBreadcrumbs: boolean;
  showPageNavigation: boolean;
}

export type PageLayoutConfig = WidgetConfig<PageLayoutWidgetFields>;
