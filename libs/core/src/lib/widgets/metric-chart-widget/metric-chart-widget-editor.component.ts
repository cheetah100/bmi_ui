import { Component, OnInit, OnDestroy } from '@angular/core';
import { MetricChartWidgetFields, MetricChartWidgetConfig, KpiSeries } from './metric-chart-generator.service';
import { TypedFormGroup, TemplatedFormArray } from '@bmi/ui';
import { FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { WidgetContext } from '../../widget-context';
import { CHART_SERIES_TYPE_OPTIONS, CHART_STACKING_OPTIONS } from '../../charts/basic-chart-contstants';

@Component({
  templateUrl: './metric-chart-widget-editor.component.html',
  styleUrls: ['./metric-chart-widget-editor.scss'],
})
export class MetricChartWidgetEditorComponent implements OnInit, OnDestroy {

  readonly subscription = new Subscription();
  readonly typeOptions = CHART_SERIES_TYPE_OPTIONS;
  readonly stackingOptions = CHART_STACKING_OPTIONS;
  readonly form = new TypedFormGroup<MetricChartWidgetFields>({
    dataSource: new FormControl(null),
    chartType: new FormControl('column'),
    kpis: new TemplatedFormArray<KpiSeries>(
      () => new TypedFormGroup({
        kpi: new FormControl(null),
        type: new FormControl(null),
        label: new FormControl(null),
        zones: new FormControl([{ color: '#adb5bd' }]), // cui-gray-500
      })
    ),
    xAxisLabel: new FormControl('Quarter'),
    yAxisLabel: new FormControl(''),
    stacked: new FormControl(null),
  });

  constructor(
    private widgetContext: WidgetContext<MetricChartWidgetConfig>,
  ) { }

  ngOnInit() {
    this.subscription.add(this.widgetContext.widgetConfig.subscribe(config => {
      this.form.patchValue(config.fields, { emitEvent: false });
    }));

    this.subscription.add(this.form.valueChanges.subscribe(updatedValue =>
      this.widgetContext.updateWidgetConfig(c => c.fields = updatedValue)
    ));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  addKpi() {
    const cardsFormArray = this.form.controls.kpis as TemplatedFormArray<KpiSeries>;
    cardsFormArray.push(new TypedFormGroup({
      kpi: new FormControl(null),
      type: new FormControl(null),
      label: new FormControl(null),
      zones: new FormControl([{ color: '#adb5bd' }]),
    }));
  }

}
