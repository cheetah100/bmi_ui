import { Injectable } from '@angular/core';

import { WidgetConfig } from '../../widget-config';
import { DataSeriesDef } from '../../data-series/data-series-def';
import { BasicChartWidgetConfig } from '../basic-chart-widget/basic-chart-widget-config';
import { Observable, forkJoin } from 'rxjs';
import { CardCacheService, CardFields } from '@bmi/gravity-services';
import { map, take } from 'rxjs/operators';
import { ParameterBinding } from '../../page-data-context/parameter-binding';
import { createPivotSeriesDef } from '../../data-series/create-pivot-source-def';
import { combineLatestDeferred } from '@bmi/utils';

import { GeneratorService } from '../generator-widget/generator-service';
import { ColorZone } from '../../util/color-zone';

export type MetricChartWidgetConfig = WidgetConfig<MetricChartWidgetFields>;

export interface MetricChartWidgetFields {
  dataSource: string;
  kpis: KpiSeries[];
  chartType: string;
  xAxisLabel: string;
  yAxisLabel: string;
  stacked: string;
}

export interface KpiSeries {
  kpi: string;
  type: string;
  label: string;
  zones?: ColorZone[];
}


@Injectable({ providedIn: 'root' })
export class MetricChartGeneratorService implements GeneratorService<MetricChartWidgetConfig, BasicChartWidgetConfig> {
  constructor(private cardCache: CardCacheService) { }

  generate(config: MetricChartWidgetConfig): Observable<BasicChartWidgetConfig> {
    return combineLatestDeferred(
      config.fields.kpis.map(s => this.cardCache.get('kpi', s.kpi))
    ).pipe(
      map(kpiCards => (<BasicChartWidgetConfig>{
        widgetType: 'basic-chart',
        fields: {
          chartSettings: {
            xAxisTitle: config.fields.xAxisLabel,
            yAxisTitle: config.fields.yAxisLabel,
            polar: false,
            stacked: config.fields.stacked,

            seriesGroups: [
              {
                dataSeries: createPivotSeriesDef(config.fields.dataSource),
                type: config.fields.chartType,
                selectedOnly: true,
                seriesSettings: config.fields.kpis
                  .filter(Boolean)
                  .filter(value => kpiCards.find(card => card.id === value.kpi))
                  .map(value => ({
                    value: kpiCards.find(card => card.id === value.kpi).title,
                    color: null,
                    label: value.label,
                    type: value.type,
                    zones: value.zones,
                  })),
              }
            ]
          },
        }
      }))
    );

  }
}
