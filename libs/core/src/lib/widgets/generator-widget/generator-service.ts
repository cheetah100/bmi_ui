import { Type, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { WidgetConfig } from '../../widget-config';


/**
 * Interface for a widget generator service
 *
 * A widget generator is a function that transforms widget configs.
 *
 * It has one method: generate(config): WidgetConfig | Observable<WidgetConfig>.
 * For convenience, it can synchronously return, or return an observable. By
 * returning an observable, this permits asynchronous and updating configs.
 *
 * This interface also has generic types for the input and output configs. This just improves developer ergonomics
 */
export interface GeneratorService<T extends WidgetConfig = WidgetConfig, U extends WidgetConfig = WidgetConfig> {
  generate(config: T, widgetInjector?: Injector): U | Observable<U>;
}



export interface GeneratorWidgetOptions {
  generator: Type<GeneratorService>;
}
