import { Component, OnInit, Injector } from '@angular/core';
import { Observable, Subscription, of, isObservable } from 'rxjs';
import { switchMap, catchError, map } from 'rxjs/operators';

import { WidgetConfig, looksLikeWidgetConfig } from '../../widget-config';
import { WidgetContext } from '../../widget-context';

import { GeneratorService, GeneratorWidgetOptions } from './generator-service';


@Component({
  template: `
    <bmi-widget-host
      *ngIf="!!generatedConfig"
      mode="view"
      [config]="generatedConfig"
      [updateFlairIcons]="false">
    </bmi-widget-host>
  `,
})
export class GeneratorWidgetComponent implements OnInit {

  generatedConfig: WidgetConfig = null;
  private subscription = new Subscription();
  private widgetOptions = this.widgetContext.widgetDefinition.options as GeneratorWidgetOptions;

  constructor(
    private widgetContext: WidgetContext,
    private injector: Injector,
  ) { }

  ngOnInit() {
    this.subscription.add(this.widgetContext.widgetConfig.pipe(
      switchMap(config => this.generateWidget(config).pipe(
        map(config => {
          config.generatedWidget = true;
          return config;
        }),
        catchError(() => of(<WidgetConfig>null))
      ))
    ).subscribe(config => this.generatedConfig = config));
  }

  private generateWidget(config: WidgetConfig): Observable<WidgetConfig> {
    if (!looksLikeWidgetConfig(config)) {
      throw new Error('No Widget Configuration');
    }

    const generatorClass = this.widgetOptions.generator;
    if (!generatorClass) {
      throw new Error('No Widget Generator Provided');
    }

    const generator = this.injector.get(generatorClass);
    const configOrObservable = generator.generate(config, this.injector);

    return isObservable(configOrObservable) ? configOrObservable : of(configOrObservable);
  }
}
