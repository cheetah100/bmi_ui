import { Component, OnInit, OnDestroy } from '@angular/core';
import { TypedFormControl, TypedFormGroup } from '@bmi/ui';
import { WidgetContext } from '../../widget-context';
import { ObjectMap } from '@bmi/utils';
import { ParameterBinding } from '../../page-data-context/parameter-binding';
import { MarkdownWidgetConfig, MarkdownWidgetFields } from './markdown.config';
import { Subscription } from 'rxjs';
import { PathResolver } from '@bmi/gravity-services';


@Component({
  selector: 'bmi-markdown-widget-editor',
  template: `
<ng-container [formGroup]="form">

  <ui-form-field label="Markdown">
    <textarea
      class="markdown-textarea"
      formControlName="markdown"
      rows="10">
    </textarea>
  </ui-form-field>

  <ui-form-field label="Title Template Values">
    <bmi-data-map-editor
      formControlName="bindings"
      [resolver]="resolver">
    </bmi-data-map-editor>
  </ui-form-field>

</ng-container>
  `,
  styles: [`
.markdown-textarea {
  margin-bottom: 1rem;
}
  `],
})
export class MarkdownWidgetEditorComponent implements OnInit, OnDestroy {

  readonly form = new TypedFormGroup<MarkdownWidgetFields>({
    markdown: new TypedFormControl<string>(''),
    bindings: new TypedFormControl<ObjectMap<ParameterBinding>>({}),
  });
  private readonly subscription = new Subscription();
  resolver: PathResolver = null;

  constructor(private widgetContext: WidgetContext) { }

  ngOnInit() {
    this.subscription.add(this.widgetContext.widgetConfig.subscribe(config => {
      this.form.patchValue(config.fields, { emitEvent: false });
    }));

    this.subscription.add(this.form.valueChanges.subscribe(updatedValue =>
      this.widgetContext.updateWidgetConfig(c => c.fields = updatedValue)
    ));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
