import { WidgetConfig } from '../../widget-config';
import { ObjectMap } from '@bmi/utils';
import { ParameterBinding } from '../../page-data-context/parameter-binding';


export type MarkdownWidgetConfig = WidgetConfig<MarkdownWidgetFields>;

export interface MarkdownWidgetFields {
  value?: string; // deprecated. remove when migration is done.
  markdown: string;
  bindings: ObjectMap<ParameterBinding>;
}
