import { Component, Optional } from '@angular/core';
import { combineLatestObject, distinctUntilNotEqual } from '@bmi/utils';
import { mapValues } from 'lodash-es';
import { combineLatest, of } from 'rxjs';
import { map, shareReplay, switchMap } from 'rxjs/operators';
import { PageDataContext } from '../../page-data-context/page-data-context';
import { PageInfoService } from '../../page/page-info.service';
import { mustacheDown } from '../../util/mustachedown';
import { WidgetContext } from '../../widget-context';
import { MarkdownWidgetConfig } from './markdown.config';


@Component({
  selector: 'bmi-markdown-widget',
  template: `
      <div class="markdown-content" [innerHtml]="renderedHtml | async"></div>
      <div *ngIf="showEditModePlaceholder | async" class="markdown-widget__edit-mode-placeholder">
        Markdown Widget
      </div>
    `,

  styles: [`
    .markdown-widget__edit-mode-placeholder {
      margin: 5px;
      padding 5px;
      border: 1px dashed var(--gra-color--gray-300);
      color: var(--gra-color--gray-400);
      text-align: center;
    }
  `]
})
export class MarkdownWidgetComponent {
  constructor(
    private widgetContext: WidgetContext<MarkdownWidgetConfig>,
    @Optional() private binder: PageDataContext,
    @Optional() private pageInfoService: PageInfoService,
  ) { }

  isEditMode = this.widgetContext.isEditMode;

  renderedHtml = this.widgetContext.widgetConfig.pipe(
    switchMap(widgetConfig => combineLatest([
      this.pageInfoService ? this.pageInfoService.pageInfo : of(null),
      of(widgetConfig),
      combineLatestObject(mapValues(
        widgetConfig.fields.bindings || {}, b => this.binder.bind(b)
      )),
      this.isEditMode,
    ])),
    distinctUntilNotEqual(),
    map(([pageInfo, widgetConfig, values, isEditMode]) => {
      return mustacheDown(widgetConfig.fields.markdown, {
          context: {
            ...values,
            page: pageInfo
          },
          applyTemplate: !isEditMode
        });
    }),
    shareReplay(1),
  );

  showEditModePlaceholder = combineLatest([
    this.isEditMode,
    this.renderedHtml
  ]).pipe(
    map(([isEditMode, renderedHtml]) => isEditMode && !renderedHtml)
  );
}
