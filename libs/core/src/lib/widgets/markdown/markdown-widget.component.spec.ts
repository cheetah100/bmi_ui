import { WidgetHarness } from '../testing/widget-harness';
import { MarkdownWidgetComponent } from './markdown-widget.component';
import { MarkdownWidgetConfig } from './markdown.config';
import { WidgetTestOptions } from '../testing/widget-test-options';
import { WidgetTestEnvironment } from '../testing/widget-test-environment';

const envOptions: WidgetTestOptions<MarkdownWidgetConfig, MarkdownWidgetComponent> = {
  config: {
    widgetType: 'markdown',
    fields: {
      markdown: 'Some markdown content for testing, featuring {{ page.title }} in here.',
      bindings: null
    }
  },
  componentType: MarkdownWidgetComponent,
}
let harness: WidgetHarness;
let env: WidgetTestEnvironment<MarkdownWidgetConfig, MarkdownWidgetComponent>;
const selector = 'div.markdown-content';

describe('MarkdownWidget', () => {

  beforeEach(async () => {
    env = new WidgetTestEnvironment<MarkdownWidgetConfig, MarkdownWidgetComponent>(envOptions);
    harness = await env.getHarness(WidgetHarness);
  });

  describe('changed template', () => {
    beforeEach(() => {
      env.config = {widgetType: 'markdown', fields: {markdown: 'This is updated on page {{ page.title }}.',bindings: null}};
      env.viewMode = 'view';
    });
    it('should show updated text to reflect the new template', async () => {
      const displayText = await harness.getDisplayText(selector);
      expect(displayText).toBe('This is updated on page Test page for testing.');
    });
  });

  describe('view mode', () => {
    it('should show the template, with substituted variable values', async () => {
      env.viewMode = 'view';
      const displayText = await harness.getDisplayText(selector);
      expect(displayText).toBe('Some markdown content for testing, featuring Test page for testing in here.');
    })
  });

  describe('edit mode', () => {
    it('should show the template, without placing variable values', async () => {
      env.viewMode = 'edit';
      const displayText = await harness.getDisplayText(selector);
      expect(displayText).toBe('Some markdown content for testing, featuring {{ page.title }} in here.');
    })
  });

});
