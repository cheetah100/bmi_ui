import {Injectable, Injector} from '@angular/core';
import {GeneratorService} from '../generator-widget/generator-service';
import {WidgetConfig} from '../../widget-config';
import {Observable, of} from 'rxjs';
import {BoardConfig, BoardService} from '@bmi/gravity-services';
import {createCell, GridLayoutUtils, GridLayoutConfig, GridLayoutCell } from '../grid-layout/grid-layout-config';
import {makeFormControlWidget} from '../../forms/make-form-control-widget';
import {CardEditFormConfig} from '../card-edit-form/card-edit-form.component';
import {FormField} from '../../forms/form-context';
import { map } from "rxjs/operators";



export type CardEditFormGeneratorWidgetFields = CardEditFormConfig['fields'];


@Injectable({providedIn: 'root'})
export class CardEditFormGeneratorService implements GeneratorService<CardEditFormConfig> {
  constructor(
    private boardService: BoardService,
  ) {  }

  generate(config: CardEditFormConfig, injector: Injector): Observable<WidgetConfig> {
    const boardService = this.boardService;
    let boardFields: FormField[] = [];
    const boardId = config.fields.board;
    const emptyBoard: Observable<BoardConfig> = of(null);
    const boardObs = boardId ? boardService.getConfig(boardId) : emptyBoard;
    return boardObs.pipe(
      map(boardConfig => {
        if (boardConfig) {
          boardFields = Object.values(boardConfig.fields)
            .filter(field => !field.isMetadata)  // TODO: check if editable too?
            .map(f => {
              return <FormField>{
                id: f.id,
                label: f.label || f.name || f.id,
                optionlist: f.optionlist || undefined,
                type: f.type || 'STRING',
                control: null,
                required: f.required,
                immutable: f.immutable
              };
            });
          // Check if any of the fields are editable
          if (boardFields.length === 0 ) {
            return {
              widgetType: 'alert',
              fields: {
                level: 'warning-alt',
                template: `Card Form Generator: Board '${config.fields.board}' does not have any editable fields`
              }
            }
          }
          return this.createWidget(config, boardFields, boardConfig);
        } else {
          return {
            widgetType: 'alert',
            fields: {
              level: 'warning-alt',
              template: `Card Form Generator: Board '${config.fields.board}' does not exist`
            }
          }
        }
      })
    )
  }

  createWidget(config: CardEditFormConfig, boardFields: FormField[], boardConfig: BoardConfig) {
    return <CardEditFormConfig>{
      widgetType: 'card-edit-form',
      fields: {
        ...config.fields,
        body: GridLayoutUtils.createStackedGrid([
          {widgetType: 'markdown', fields: {markdown: `### ${boardConfig.name}`}},
          ...this.createFormControls(boardFields),
          {
            widgetType: 'form-control:button',
            fields: {
              buttonLabel: 'Save',
              onClick: [{type: 'trigger-form-action', action: 'save'}]
            }
          }
        ], false, {
          neverShowCards: true,
        }),
      }
    };
  }

  createFormControls(fields: FormField[]) {
    return fields.map(field => {
      const widgetConfig = makeFormControlWidget(field);
      widgetConfig.fields.fieldId = field.id;
      widgetConfig.fields.required = field.required;
      return widgetConfig;
    });
  }
}
