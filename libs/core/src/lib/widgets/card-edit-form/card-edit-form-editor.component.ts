import { Component, OnInit } from '@angular/core';
import { map, shareReplay } from 'rxjs/operators';

import { GravityConfigService, PathResolver } from '@bmi/gravity-services';
import { TypedFormGroup, TypedFormControl, Option, TypedFormArray } from '@bmi/ui';

import { WidgetConfig } from '../../widget-config';
import { WidgetContext } from '../../widget-context';
import { CardEditorConfig } from '../../card-editor/card-editor-config';
import { CardEditFormConfigFields } from './card-edit-form.component';

import { FormContext } from '../../forms/form-context';
import { OptionlistService } from '../../services/optionlist.service';
import { PageDataContext } from '../../page-data-context/page-data-context';


@Component({
  template: `
    <ng-container [formGroup]="form">
      <bmi-card-editor-config formControlName="config"></bmi-card-editor-config>
    </ng-container>
  `
})
export class CardEditFormEditorComponent implements OnInit {

  // This could be handled as a single form control, and it would probably be be
  // a little simpler if it was... but this leaves room for any custom setting
  // which might need to be added later.
  //
  // There are some fields like "body" which are specific to the card edit form,
  // but the updateWidgetConfig call below handles that by merging with the
  // existing value.
  form = new TypedFormGroup({
    config: new TypedFormControl<CardEditorConfig>(),
  });

  constructor(
    private widgetContext: WidgetContext<WidgetConfig<CardEditFormConfigFields>>,
    private gravityConfigService: GravityConfigService,
    private optionListService: OptionlistService,
    private binder: PageDataContext,
  ) {}

  ngOnInit() {
    this.widgetContext.widgetConfig.subscribe(config => {
      this.form.patchValue({config: config.fields}, { emitEvent: false });
    });

    this.form.valueChanges.subscribe(fields => {
      this.widgetContext.updateWidgetConfig(c => c.fields = {
        ...c.fields,
        ...fields.config
      });
    });
  }
}
