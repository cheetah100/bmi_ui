import {
  Component,
  OnChanges,
  OnDestroy,
  OnInit,
  Optional
} from '@angular/core';
import {
  Subscription,
  of,
  combineLatest,
  Observable,
  BehaviorSubject,
  EMPTY,
} from 'rxjs';
import {
  catchError,
  distinctUntilChanged,
  map,
  shareReplay,
  switchMap,
  tap,
  finalize,
  withLatestFrom
} from 'rxjs/operators';
import { WidgetConfig } from '../../widget-config';
import { WidgetContext } from '../../widget-context';
import { FormContext, FormFieldConfig } from '../../forms/form-context';

import { PageDataContext } from '../../page-data-context/page-data-context';
import { provideDataContextServices } from '../../page-data-context/helpers';
import { ObjectDataSource } from '../../data-sources/object-data-source';
import { distinctUntilNotEqual } from '../../util/distinct-until-not-equal';
import { bindDataMap } from '../../util/bind-data-map';

import {
  BoardService,
  BoardConfig,
  CardData,
  CardCacheService,

} from '@bmi/gravity-services';
import {
  ConfirmationModalService,
  ErrorModalService,
} from '@bmi/ui';

import { ObjectMap } from '@bmi/utils';

import { EventService } from '../../events/event.service';

import { CardEditorConfig } from '../../card-editor/card-editor-config';

import isNil from 'lodash-es/isNil';
import pickBy from 'lodash-es/pickBy';

export interface CardEditFormConfigFields extends CardEditorConfig {
  body: WidgetConfig;
}

export type CardEditFormConfig = WidgetConfig<CardEditFormConfigFields>;


@Component({
  selector: 'bmi-card-edit-form',
  templateUrl: './card-edit-form.component.html',
  styleUrls: ['./card-edit-form.component.scss'],
  providers: [
    FormContext,
    EventService,
    provideDataContextServices(),
  ]
})
export class CardEditFormComponent implements OnInit, OnDestroy {
  constructor(
    private widgetContext: WidgetContext<CardEditFormConfig>,
    private formContext: FormContext,
    private boardService: BoardService,
    private dataContext: PageDataContext,
    private binder: PageDataContext,
    private errorModalService: ErrorModalService,
    private eventService: EventService,
    @Optional() private cardCacheService: CardCacheService,
  ) {}

  body = this.widgetContext.widgetConfig.pipe(
    map(config => config.fields.body)
  );

  isEditMode = this.widgetContext.isEditMode;

  isNewCard = false;
  boardConfig = this.widgetContext.widgetConfig.pipe(
    tap( config => this.isNewCard = config.fields.cardId === null),
    map(config => config.fields.board),
    distinctUntilChanged(),
    switchMap(board =>
      board ? this.boardService.getConfig(board) : of(<BoardConfig>undefined)
    )
  );

  defaultValues = this.widgetContext.widgetConfig.pipe(
    map(config => config.fields.defaults || {}),
    distinctUntilNotEqual(),
    switchMap(defaultValueBindings => bindDataMap(this.binder, defaultValueBindings)),
    shareReplay(1)
  );

  initialValues = new BehaviorSubject<ObjectMap<unknown>>({});

  combinedValues = combineLatest([this.initialValues, this.defaultValues]).pipe(
    map(([initialValues, defaultValues]) => ({
      ...initialValues,

      // Only include the default value if the initial value is null. In future
      // we might want to support multiple modes here -- we might want to always
      // replace, depending on config setting.
      ...pickBy(defaultValues, (value, key) => isNil(initialValues[key]))
    }))
  );

  private subscription = new Subscription();

  private fetchedCard: CardData;
  loadingMessage: string = null;


  ngOnInit() {
    this.dataContext.setDataSources({
      form: new ObjectDataSource(this.formContext.value)
    });

    this.subscription.add(
      this.boardConfig.subscribe(boardConfig => {
        if (boardConfig) {
          const fields = Object.values(boardConfig.fields)
            .filter(field => !field.isMetadata) // TODO: check if editable too?
            .map(f => {
              return <FormFieldConfig>{
                id: f.id,
                label: f.label || f.name || f.id,
                optionlist: f.optionlist || undefined,
                required: f.required || false,
                type: f.type || 'STRING',
                immutable: f.immutable && !this.isNewCard
              };
            });
          this.formContext.clearFields();
          this.formContext.addFields(fields);
        }
      })
    );

    this.subscription.add(this.widgetContext.widgetConfig.subscribe(config => {
      this.eventService.setConfig({
        'form:after-save': config.fields.afterSaveEvent
      })
    }));

    this.subscription.add(
      this.widgetContext.widgetConfig
        .pipe(
          switchMap(config =>
            combineLatest([
              of(config.fields.board),
              this.binder.bind(config.fields.cardId)
            ])
          ),
          distinctUntilNotEqual(),
          tap(() => (this.loadingMessage = 'Loading')),
          switchMap(([boardId, cardId]) =>
            boardId && cardId
              ? this.boardService.fetchById(boardId, String(cardId))
              : of(<CardData>null)
          ),
          finalize(() => (this.loadingMessage = null))
        )
        .subscribe(card => {
          this.loadingMessage = null;
          this.fetchedCard = card;
          this.initialValues.next(card && card.fields ? card.fields : {});
        })
    );

    this.subscription.add(
      this.initialValues
        .pipe(withLatestFrom(this.combinedValues))
        .subscribe(([initialValues, combinedValues]) =>
          this.formContext.setValue(combinedValues)
        )
    );

    this.subscription.add(
      this.combinedValues.subscribe(combined =>
        this.formContext.updateUntouchedFields(combined)
      )
    );

    this.subscription.add(
      this.formContext
        .watchForAction('save')
        .pipe(
          withLatestFrom(
            this.formContext.value,
            this.widgetContext.widgetConfig
          )
        )
        .subscribe(([action, formData, widgetConfig]) => {
          this.saveForm(widgetConfig, formData);
        })
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  updateBodyConfig(body: WidgetConfig) {
    this.widgetContext.updateWidgetConfig(c => (c.fields.body = body));
  }

  private saveForm(
    config: WidgetConfig<CardEditFormConfigFields>,
    data: { [key: string]: any }
  ) {
    const boardId = config.fields.board;
    const cardToSave = createOrUpdateCard(boardId, this.fetchedCard, data);
    this.loadingMessage = 'Saving';
    this.subscription.add(
      this.boardService
        .saveCard(boardId, cardToSave)
        .pipe(
          this.errorModalService.catchError({retryable: true}),
          finalize(() => (this.loadingMessage = null))
        )
        .subscribe({
          next: savedCard => {
            this.cardCacheService.refreshBoard(boardId, true);
            this.eventService.emit('form:after-save', {dataContext: this.binder})
          }
        })
    );
  }

}

function createOrUpdateCard(
  boardId: string,
  existingCard: CardData,
  fields: { [key: string]: any }
): CardData {
  return {
    ...existingCard ?? {},
    id: existingCard?.id,
    phase: existingCard?.phase,
    board: boardId,
    fields: {
      ...existingCard?.fields ?? {},
      ...fields
    }
  };
}
