import { NgModule, Provider, Type } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { PortalModule } from '@angular/cdk/portal';

import { GravityUiModule } from '@bmi/gravity-services';
import { SharedUiModule } from '@bmi/ui';
import { BmiChartsEditorModule } from '@bmi/legacy-charts';

import { BmiCoreEditorComponentsModule } from '../editor-components/bmi-core-editor-components.module';

import { BmiCoreModule } from '../bmi-core.module';
import { BmiWidgetEditorCommonModule } from '../bmi-core-widget-editor.module';

import { defineEditor, defineConfigTemplate, defineWidgetConverter, defineGenericWidgetEditor } from '../util/editor-def-helpers';
import { EditorWidgets } from '../forms/generic-editor-schema';
import { WidgetConfig } from '../widget-config';

import { FormControlEditorRegistryModule } from './form-controls/form-control-editor-registry.module';
import { EditorFormControlRegistry } from './editor-form-controls/editor-form-control.registry';

import { GridLayoutSectionSidebarComponent } from './grid-layout/grid-layout-section-sidebar/grid-layout-section-sidebar.component';
import { GridLayoutCellSidebarComponent } from './grid-layout/grid-layout-cell-sidebar/grid-layout-cell-sidebar.component';
import { GridLayoutWidgetEditorComponent } from './grid-layout/grid-layout-widget-editor.component';
import { GridLayoutConfig } from './grid-layout/grid-layout-config';
import { WrapWithGridConverter } from './grid-layout/wrap-with-grid-converter';

import { MarkdownWidgetEditorComponent } from './markdown/markdown-widget-editor.component';
import { InfoboxWidgetEditorComponent } from './infobox/infobox-widget-editor.component';
import { PageLayoutWidgetEditorComponent } from './page-layout/page-layout-widget-editor.component';
import { ActionWidgetEditorComponent } from './action-widget/action-widget-editor.component';
import { FilterControlsWidgetEditorComponent } from './filter-controls/filter-controls-widget-editor.component';
import { TableWidgetEditorComponent } from './table/table-widget-editor.component';
import { ContentTabsWidgetEditorComponent } from './content-tabs-widget/content-tabs-widget-editor.component';
import { WrapWithTabsConverter } from './content-tabs-widget/wrap-with-tabs-converter';
import { TimelineWidgetEditorComponent } from './timeline-widget/timeline-widget-editor.component';
import { MultiStatusWidgetEditorComponent } from './multi-status-widget/multi-status-widget-editor.component';
import { BasicChartWidgetEditorComponent } from './basic-chart-widget/basic-chart-widget-editor.component';
import { ReferenceWidgetEditorComponent } from './reference-widget/reference-widget-editor.component';
import { NavListWidgetEditorComponent } from './nav-list-widget/nav-list-widget-editor.component';
import { BreadcrumbsWidgetEditorComponent } from './breadcrumbs/breadcrumbs-widget-editor.component';

import { GridPresetIconComponent } from './grid-layout/grid-preset-icon/grid-preset-icon.component';
import { GridPresetPickerComponent } from './grid-layout/grid-preset-picker/grid-preset-picker.component';
import { BmiCoreChartsEditorModule } from '../charts/bmi-core-charts-editor.module';
import { MetricChartWidgetEditorComponent } from './metric-chart-widget/metric-chart-widget-editor.component';
import { GoalActualKpiChartEditorComponent } from './goal-actual-kpi-chart/goal-actual-kpi-chart-editor.component';
import { BasicChartWidgetConfig } from './basic-chart-widget/basic-chart-widget-config';
import { PersonWidgetEditorComponent } from './person-widget/person-widget-editor.component';
import { ForeachWidgetEditorComponent } from './foreach-widget/foreach-widget-editor.component';
import { WrapWithSectionConverter } from './section-widget/wrap-with-section-converter';

import { GaugeChartToggleWidgetEditorComponent } from './gauge-chart-toggle/gauge-chart-toggle-widget-editor.component';
import { ContentTabsToGaugeChartConverter } from './gauge-chart-toggle/content-tabs-to-gauge-chart-converter';

import { CardEditFormEditorComponent } from './card-edit-form/card-edit-form-editor.component';

import { AlertWidgetEditorComponent } from './alert-widget/alert-widget-editor.component';

import { GenericWidgetEditorComponent } from './generic-editor/generic-widget-editor.component';

import { ButtonToolbarWidgetFields } from './button-toolbar/button-toolbar-widget.component';
import { TableWidgetConfig } from './table/table-config';
import { MetricGaugeWidgetEditorComponent } from './metric-gauge-widget/metric-gauge-widget-editor.component';

@NgModule({
  imports: [
    CommonModule,
    BmiCoreModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    PortalModule,
    SharedUiModule,
    GravityUiModule,
    BmiCoreEditorComponentsModule,
    BmiWidgetEditorCommonModule,
    BmiCoreChartsEditorModule,
    FormControlEditorRegistryModule,
    EditorFormControlRegistry,

    // Deprecated: is currently providing zone control and a couple of other
    // minor components. We should move these into BmiCore and remove this
    // module.
    BmiChartsEditorModule,
  ],

  declarations: [
    GridLayoutSectionSidebarComponent,
    GridLayoutCellSidebarComponent,
    MarkdownWidgetEditorComponent,
    InfoboxWidgetEditorComponent,
    PageLayoutWidgetEditorComponent,
    ActionWidgetEditorComponent,
    FilterControlsWidgetEditorComponent,
    TableWidgetEditorComponent,
    ContentTabsWidgetEditorComponent,
    TimelineWidgetEditorComponent,
    MultiStatusWidgetEditorComponent,
    BasicChartWidgetEditorComponent,
    GridPresetIconComponent,
    GridPresetPickerComponent,
    ReferenceWidgetEditorComponent,
    NavListWidgetEditorComponent,
    BreadcrumbsWidgetEditorComponent,
    MetricChartWidgetEditorComponent,
    PersonWidgetEditorComponent,
    GoalActualKpiChartEditorComponent,
    ForeachWidgetEditorComponent,
    GaugeChartToggleWidgetEditorComponent,
    CardEditFormEditorComponent,
    AlertWidgetEditorComponent,
    GridLayoutWidgetEditorComponent,
    GenericWidgetEditorComponent,
    MetricGaugeWidgetEditorComponent,
  ],

  providers: [
    defineEditor('page-layout', PageLayoutWidgetEditorComponent),

    defineEditor('markdown', MarkdownWidgetEditorComponent, {
      name: 'Markdown Text',
      description: 'Display formatted text and headings on a page using the Markdown syntax.',
      groups: ['Common'],
      icon: 'cui-keywords',
      widgetConfig: {
        widgetType: 'markdown',
        fields: {
          markdown: '### Markdown\n\nPut your *Markdown* text here. You can also use Mustache templating to insert data-bound text, e.g. {{page.title}}. This will only appear in view mode.'
        }
      }
    }),

    defineEditor('person', PersonWidgetEditorComponent, {
      name: 'Person',
      groups: ['Common'],
      icon: 'cui-contact',
      widgetConfig: {
        widgetType: 'person',
        fields: {
          userId: null,
          name: null,
        }
      }
    }),

    defineEditor('infobox', InfoboxWidgetEditorComponent, {
      name: 'Infobox',
      description: 'Display a title and value on a page',
      groups: ['Common'],
      icon: 'cui-square-o',
      widgetConfig: {
        widgetType: 'infobox',
        fields: {
          title: 'Put your text here',
          value: {
            bindingType: 'data-path',
            path: ''
          },
          valueFormat: 'integer'
        }
      }
    }),

    defineEditor('filter-controls', FilterControlsWidgetEditorComponent, {
      name: 'Filter Controls',
      widgetConfig: {
        widgetType: 'filter-controls',
        fields: {
          filters: [{
            id: null,
            name: null,
            filterType: 'optionlist',
            controlType: 'single-select'
          }]
        }
      }
    }),

    defineEditor('table-v2', TableWidgetEditorComponent, {
      name: 'Table Widget',
      groups: ['Common', 'Graphs'],
      icon: 'shared-ui-table',
      widgetConfig: <TableWidgetConfig>{
        widgetType: 'table-v2',
        fields: {
          dataSourceId: null,
          hasSearch: false,
          hasPagination: true,
          rowsPerPage: 10,
          isEditable: false,
          editOptions: {},
          columns: [],
        }
      }
    }),

    defineEditor('action-widget', ActionWidgetEditorComponent, {
      name: 'Actions Widget',
      icon: 'cui-checklist',
      widgetConfig: {
        widgetType: 'action-widget',
        fields: {
          actionSources: []
        }
      }
    }),

    defineEditor('content-tabs', ContentTabsWidgetEditorComponent, {
      name: 'Content Tabs',
      groups: ['Layouts'],
      widgetConfig: {
        widgetType: 'content-tabs',
        fields: {
          navLocation: 'top',
          filter: null,
          sections: [
            {
              title: 'Tab 1',
              id: 'default1',
              widgetConfig: { widgetType: null, fields: null },
            },
          ],
        }
      }
    }),

    defineWidgetConverter(WrapWithTabsConverter),

    defineEditor('gantt', TimelineWidgetEditorComponent, {
      name: 'Gantt',
      widgetConfig: {
        widgetType: 'gantt',
        fields: {}
      }
    }),

    defineEditor('multi-status', MultiStatusWidgetEditorComponent, {
      name: 'Multi Status',
      widgetConfig: {
        widgetType: 'multi-status',
        fields: {}
      }
    }),

    defineEditor('basic-chart', BasicChartWidgetEditorComponent, {
      name: 'Advanced XY Chart',
      groups: ['Graphs'],
      icon: 'shared-ui-bar-chart',
      widgetConfig: <BasicChartWidgetConfig>{
        widgetType: 'basic-chart',
        fields: {
          chartSettings: {
            polar: false,
            stacked: undefined,
            xAxisTitle: '',
            yAxisTitle: '',
            seriesGroups: [{
              dataSeries: null,
              type: 'column',
              selectedOnly: false,
              seriesSettings: [],
            }]
          }
        }
      }
    }),

    defineEditor('metric-chart', MetricChartWidgetEditorComponent, {
      name: 'Metric Chart',
      groups: ['Common', 'Graphs'],
      icon: 'shared-ui-line-chart',
      widgetConfig: {
        widgetType: 'metric-chart',
        fields: {
          kpis: [
            { kpi: null }
          ]
        },
      }
    }),

    defineEditor('goal-actual-kpi-chart', GoalActualKpiChartEditorComponent, {
      name: 'Goal/Actual KPI Chart',
      groups: ['Graphs'],
      icon: 'shared-ui-bar-chart',
      widgetConfig: {
        widgetType: 'goal-actual-kpi-chart',
        fields: {
          actualDataSource: null,
          goalDataSource: null,
          kpi: null
        }
      }
    }),

    defineEditor('reference', ReferenceWidgetEditorComponent, {
      name: 'Reference (graph widget)',
      groups: ['Graphs'],
      icon: 'shared-ui-line-chart',
      widgetConfig: {
        widgetType: 'reference',
        fields: {},
      }
    }),

    defineEditor('nav-list', NavListWidgetEditorComponent, {
      name: 'Links list',
      groups: ['Navigation'],
      icon: 'cui-link',
      widgetConfig: {
        widgetType: 'nav-list',
        fields: {
          type: 'horizontal',
          links: [],
          visible: true,
        },
      }
    }),

    defineEditor('breadcrumbs', BreadcrumbsWidgetEditorComponent),

    defineEditor('foreach', ForeachWidgetEditorComponent, {
      name: 'For-each Loop',
      groups: ['Layouts'],
      icon: 'cui-apps',
      widgetConfig: {
        widgetType: 'foreach',
        fields: {
          dataSourceId: null,
          columns: 3,
          bindings: {},
          template: null
        }
      }
    }),

    defineEditor('gauge-chart-toggle', GaugeChartToggleWidgetEditorComponent, {
      name: 'Gauge/chart toggle',
      groups: ['Graphs'],
      icon: 'cui-analysis',
      widgetConfig: {
        widgetType: 'gauge-chart-toggle',
        fields: {
          size: 'medium',
          useZones: false,
          zones: null,
          showChart: false,
          metricCardSource: null,
          actualDataSource: null,
          goalDataSource: null,
        },
      },
    }),

    defineEditor('alert', AlertWidgetEditorComponent, {
      name: 'Alert',
      icon: 'cui-warning',
      widgetConfig: {
        widgetType: 'alert',
        fields: {
          level: 'warning-alt',
          template: '',
        }
      }
    }),

    defineWidgetConverter(ContentTabsToGaugeChartConverter),

    defineEditor('grid-layout', GridLayoutWidgetEditorComponent, {
      id: 'empty-grid',
      name: 'Grid Layout',
      groups: ['Common', 'Layouts'],
      icon: 'cui-team-expanded-view',
      widgetConfig: {
        widgetType: 'grid-layout',
        fields: {
          sections: [{
            layoutId: '2-column',
            cells: [
              { span: 6, widgetConfig: null },
              { span: 6, widgetConfig: null }
            ]
          }]
        }
      }
    }),

    defineGenericWidgetEditor<WidgetConfig<ButtonToolbarWidgetFields>>({
      widgetType: 'button-toolbar',
      name: 'Button Toolbar',
      groups: ['Interactivity and Scripting'],
      widgetConfig: {
        widgetType: 'button-toolbar',
        fields: {
          buttons: [
            {type: 'action', text: 'Example', icon: 'cui-info', onInvoke: []}
          ]
        }
      },
      schema: {
        buttons: EditorWidgets.actionButtons({label: 'Buttons'}),
      }
    }),

    defineConfigTemplate({
      name: 'Section',
      groups: ['Layouts'],
      icon: 'cui-default-app',
      widgetConfig: {
        widgetType: 'section',
        fields: {
          body: null
        },
        flairs: [
          { flairType: 'widget-title', title: 'Section Widget' }
        ]
      }
    }),

    defineConfigTemplate({
      name: 'Drawer (Show More/Less)',
      groups: ['Layouts'],
      icon: 'fa4-angle-double-down',
      widgetConfig: {
        widgetType: 'drawer',
        fields: {
          body: null,
        }
      }
    }),


    defineWidgetConverter(WrapWithSectionConverter),
    defineWidgetConverter(WrapWithGridConverter),

    defineEditor('card-edit-form', CardEditFormEditorComponent, {
      name: 'Card Editor Form',
      icon: 'fa4-edit',
      groups: ['Forms'],
      widgetConfig: {
        widgetType: 'card-edit-form',
        fields: {
          body: <GridLayoutConfig>{
            widgetType: 'grid-layout',
            fields: {
              sections: [
                {
                  layoutId: '2-column',
                  cells: [
                    {widgetConfig: null},
                    {widgetConfig: null}
                  ]
                },
                {
                  layoutId: '1-column',
                  cells: [
                    {
                      widgetConfig: {
                        widgetType: 'form-control:button',
                        fields: {
                          buttonLabel: 'Save',
                          enableWhenFormValid: true,
                          onClick: [{type: 'trigger-form-action', action: 'save'}]
                        }
                      }
                    }
                  ]
                }
              ]
            }
          }
        }
      }
    }),

    defineEditor('card-edit-form-generator', CardEditFormEditorComponent, {
      name: 'Card Editor Form Generator',
      groups: ['Forms'],
      widgetConfig: {
        widgetType: 'card-edit-form-generator',
        fields: {}
      }
    }),

    defineConfigTemplate({
      id: 'bmi-page-layout-template',
      type: 'page',
      name: 'BMI Page Layout',
      description: 'A simple template page.',
      widgetConfig: {
        widgetType: 'page-layout',
        fields: {
          'header': {
            'widgetType': 'markdown',
            'fields': {
              'value': '# {{page.title}}'
            }
          },
          breadcrumbs: {
            widgetType: 'breadcrumbs',
            fields: {
              type: 'breadcrumbs',
              visible: false,
              links: [],
            },
          },
          'mainFilter': {
            'widgetType': 'filter-controls',
            'fields': {
              'filters': []
            }
          },
          'body': {
            'widgetType': 'grid-layout',
            'fields': {
              sections: [{
                layoutId: '2-column',
                cells: [
                  { span: 6, widgetConfig: null },
                  { span: 6, widgetConfig: null }
                ]
              }]
            }
          }
        }
      }
    }),
    defineEditor('metric-gauge', MetricGaugeWidgetEditorComponent, {
      name: 'Metric Gauge',
      groups: ['Common'],
      icon: 'cui-analysis',
      widgetConfig: {
        widgetType: 'metric-gauge',
        fields: {
          size: 'medium'
        },
      },
    }),
  ]
})
export class BmiWidgetsEditorModule { }
