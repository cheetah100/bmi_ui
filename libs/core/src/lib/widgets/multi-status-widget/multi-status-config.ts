import { WidgetConfig } from '../../widget-config';
import { ParameterBinding } from '../../page-data-context/parameter-binding';

export interface MultiStatusWidgetFields {
  optionlistBoard: string;
  fieldId: ParameterBinding;
}

export type MultiStatusWidgetConfig = WidgetConfig<MultiStatusWidgetFields>;
