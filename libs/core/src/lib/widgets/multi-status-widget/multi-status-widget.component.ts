import { Component, OnInit, Optional, OnDestroy } from '@angular/core';
import { MultiStatusWidgetConfig } from './multi-status-config';
import { WidgetContext } from '../../widget-context';
import { PageDataContext } from '../../page-data-context/page-data-context';
import { of, combineLatest, Subscription } from 'rxjs';
import { switchMap, publishReplay, refCount } from 'rxjs/operators';
import { OptionlistService } from '../../services/optionlist.service';

/**
 * Multi Status or a Multi Status widget is a derivative of the Info box widget.
 * Intent: To show a list of cards with the addition of
 * highlighting a subset based on a condition.
 */
@Component({
  selector: 'bmi-multi-status-widget',
  templateUrl: './multi-status-widget.component.html',
  styleUrls: ['./multi-status-widget.component.scss']
})
export class MultiStatusWidgetComponent implements OnInit, OnDestroy {

  multiStatusOptions = [];
  subscription: Subscription = new Subscription();

  readonly fieldId = this.context.widgetConfig.pipe(
    switchMap(config => this.pageDataContext ? this.pageDataContext.bindParameter(config.fields.fieldId) : of(null)),
    publishReplay(1),
    refCount()
  );

  readonly optionlistCards = this.context.widgetConfig.pipe(
    switchMap(config => {
      if (config && config.fields && config.fields.optionlistBoard) {
        return this.optionlistService.getOptions(config.fields.optionlistBoard);
      } else {
        return of(null);
      }
    }),
    publishReplay(1),
    refCount()
  );

  constructor(
    private context: WidgetContext<MultiStatusWidgetConfig>,
    private optionlistService: OptionlistService,
    @Optional() private pageDataContext: PageDataContext,
  ) { }

  ngOnInit() {
    this.subscription.add(combineLatest([
      this.fieldId,
      this.optionlistCards
    ]).subscribe(response => {
      this.multiStatusOptions = [];
      /**
       * This assumes the data binding field selected has
       * a response of a type array / list. We could be smarter here
       * in suggesting that if the admin choses a field of a type string
       * a suggestion to use an Info Box widget could be given.
       */
      if (response[1] && Array.isArray(response[1])) {
        response[1].forEach(optionlist => {
          this.multiStatusOptions.push({
            title: optionlist.text,
            highlight: (response[0] && response[0].includes(optionlist.value)) ? true : false
          });
        });
      }
    }));

  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
