import { Component, OnInit, Optional } from '@angular/core';
import { TypedFormControl, TypedFormGroup } from '@bmi/ui';
import { Validators } from '@angular/forms';
import { MultiStatusWidgetFields, MultiStatusWidgetConfig } from './multi-status-config';
import { GravityConfigService } from '@bmi/gravity-services';

import { WidgetContext } from '../../widget-context';
import { Observable, of } from 'rxjs';
import { map, publishBehavior, refCount } from 'rxjs/operators';
import { Option } from '@bmi/ui';
import { distinctUntilNotEqual } from '../../util/distinct-until-not-equal';

@Component({
  selector: 'bmi-multi-status-widget-editor',
  templateUrl: './multi-status-widget-editor.component.html',
  styleUrls: []
})
export class MultiStatusWidgetEditorComponent implements OnInit {

  optionlistBoards: Observable<Option[]> = this.getBoardOptions();

  readonly multiStatusEditorForm = new TypedFormGroup<MultiStatusWidgetFields>({
    optionlistBoard: new TypedFormControl(null, [Validators.required]),
    fieldId: new TypedFormControl(null, [Validators.required])
  });

  constructor(
    private widgetContext: WidgetContext<MultiStatusWidgetConfig>,
    @Optional() private gravityConfigService: GravityConfigService
  ) { }

  ngOnInit() {

    this.widgetContext.widgetConfig.pipe(
      distinctUntilNotEqual()
    )
      .subscribe(config => {
        this.multiStatusEditorForm.patchValue({
          fieldId: config.fields.fieldId,
          optionlistBoard: config.fields.optionlistBoard
        }, { emitEvent: false });
      });

    this.multiStatusEditorForm.valueChanges.pipe(
      distinctUntilNotEqual()
    )
      .subscribe(update => {
        this.widgetContext.updateWidgetConfig(config => config.fields = update);
      });
  }

  getBoardOptions(): Observable<Option[]> {
    if (!this.gravityConfigService) {
      return of([]);
    } else {
      return this.gravityConfigService.watchAllConfigs().pipe(
        map(boards => boards.map(b => <Option>{ value: b.id, text: b.name })),
        publishBehavior([]),
        refCount()
      );
    }
  }

}
