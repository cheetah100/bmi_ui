import { Component } from '@angular/core';
import { map } from 'rxjs/operators';
import { distinctUntilNotEqual } from '@bmi/utils';

import { WidgetConfig } from '../../widget-config';
import { WidgetContext } from '../../widget-context';

import { ConfigurableToolbarItem } from '../../components/configurable-action-buttons/config';


export interface ButtonToolbarWidgetFields {
  buttons: ConfigurableToolbarItem[];
}


@Component({
  template: `
    <bmi-configurable-action-buttons [buttons]="buttons | async"></bmi-configurable-action-buttons>
  `
})
export class ButtonToolbarWidgetComponent {
  constructor(
    private context: WidgetContext<WidgetConfig<ButtonToolbarWidgetFields>>,
  ) {}

  buttons = this.context.widgetConfig.pipe(
    map(config => config.fields.buttons || []),
  );
}
