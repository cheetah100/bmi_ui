import { Component, OnDestroy, OnInit, Optional } from "@angular/core";
import { BoardService, CardCacheService, CardData } from "@bmi/gravity-services";
import { ErrorModalService, TypedFormControl } from "@bmi/ui";
import { distinctUntilNotEqual, Option } from "@bmi/utils";
import { combineLatest, Observable, Subscription } from "rxjs";
import { finalize, shareReplay, switchMap, tap } from "rxjs/operators";
import { EventService } from "../../events/event.service";
import { PageDataContext } from "../../page-data-context/page-data-context";
import { ParameterBinding } from "../../page-data-context/parameter-binding";
import { PhaseEditorConfig } from "../../phase-editor/phase-editor-config";
import { OptionlistService } from "../../services/optionlist.service";
import { WidgetConfig } from "../../widget-config";
import { WidgetContext } from "../../widget-context";


export interface PhaseEditFormConfigFields extends PhaseEditorConfig {
  body: WidgetConfig;
  cardId: string | ParameterBinding;
  boardId: string | ParameterBinding;
}


export type PhaseEditFormConfig = WidgetConfig<PhaseEditFormConfigFields>;


@Component({
  selector: 'bmi-phase-edit-form',
  templateUrl: './phase-edit-form.component.html',
  styles: [`
h2 {
  border-bottom: 2px solid var(--gra-theme-primary);
  padding-bottom: var(--gra-spacing-half);
}
ui-loading-container {
  height: 132px;
}
  `],
  providers: [
    EventService,
  ]
})
export class PhaseEditFormComponent implements OnInit, OnDestroy {
  constructor(
    private widgetContext: WidgetContext<PhaseEditFormConfig>,
    private dataContext: PageDataContext,
    private eventService: EventService,
    private boardService: BoardService,
    private optionlistService: OptionlistService,
    private errorModalService: ErrorModalService,
    @Optional() private cardCacheService: CardCacheService,
  ) {}

  private subscription = new Subscription();
  cardBoard: string = null;
  cardId: string = null;
  cardData: CardData = null;
  loadingMessage: string = null;
  phaseOptions: Observable<Option[]>;
  phaseControl = new TypedFormControl<string>(null);

  ngOnInit() {

    this.subscription.add(this.widgetContext.widgetConfig.subscribe(config => {
      this.eventService.setConfig({
        'form:after-save': config.fields.afterSaveEvent
      })
    }));

    this.subscription.add(
      this.widgetContext.widgetConfig
        .pipe(
          tap(() => this.loadingMessage = 'Loading'),
          distinctUntilNotEqual(),
          switchMap(config => {
            const cardId = config.fields.cardId as string;
            const cardBoard = config.fields.boardId as string;
            this.cardBoard = cardBoard;
            this.cardId = cardId;
            this.phaseOptions = this.optionlistService
              .getSystemOptions('gravity-system-PHASE', cardBoard)
              .pipe(shareReplay(1));
            return this.cardCacheService.get(cardBoard, cardId);
          }),
        )
        .subscribe((card: CardData) => {
          this.cardData = card;
          this.phaseControl.setValue(card.phase);
          this.loadingMessage = null;
        })
      );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  saveForm() {
    this.loadingMessage = 'Saving';
    const phase = this.phaseControl.value;
    this.subscription.add(
      this.boardService
        .moveCard(this.cardBoard, this.cardId, phase)
        .pipe(
          this.errorModalService.catchError({retryable: true}),
          finalize(() => (this.loadingMessage = null))
        )
        .subscribe({
          next: () => {
            this.cardCacheService.refreshBoard(this.cardBoard, true);
            this.eventService.emit('form:after-save', {dataContext: this.dataContext})
          }
        })
    );
  }

}
