import { BmiRoute } from '../../navigation/bmi-route';


export interface ActionItem {
  severity: string;
  titleHtml: string;
  bodyHtml: string;
  html: string;
  route: BmiRoute;
}
