import { WidgetHarness } from '../testing/widget-harness';
import { ActionWidgetComponent } from './action-widget.component';
import { DATA_CONFIG_DEFAULT_CARDS, WidgetTestOptions } from '../testing/widget-test-options';
import { WidgetTestEnvironment } from '../testing/widget-test-environment';
import { ActionWidgetConfig } from './action-widget-config';
import { ActionItemComponent } from './action-item.component';


const defaultConfig = {
  widgetType: 'action-widget',
  fields: {
    noActionsMessage: 'Nothing to see here!',
    density: 'comfortable',
    columns: 1,
    actionSources: [
      {
        dataSourceId: 'cards',
        severity: 'default',
        title: '{{name}}',
        body: '{{name}}: has {{value}} points.',
        route: null,
        data: {
          name: {
            bindingType: 'data-path' as 'data-path',
            path: 'this.name'
          },
          value: {
            bindingType: 'data-path' as 'data-path',
            path: 'this.value'
          },
        }
      }
    ]
  }
};
const envOptions: WidgetTestOptions<ActionWidgetConfig, ActionWidgetComponent> = {
  config: defaultConfig,
  declarations: [ActionItemComponent],
  componentType: ActionWidgetComponent,
  dataContextConfig: DATA_CONFIG_DEFAULT_CARDS,
}

let harness: WidgetHarness;
let env: WidgetTestEnvironment<ActionWidgetConfig, ActionWidgetComponent>;
let widgetComponent;

describe('ActionWidget', () => {

  beforeEach(async () => {
    env = new WidgetTestEnvironment<ActionWidgetConfig, ActionWidgetComponent>(envOptions);
    widgetComponent = await env.component;
    harness = await env.getHarness(WidgetHarness);
  });

  describe('Display with no data', () => {
    it('should show the no actions message', async () => {
      const layoutBox = await harness.getDisplayText('bmi-core-action-widget');
      expect(layoutBox).toBe('Nothing to see here!');
    });
    it('should show the no data edit message in edit mode when no message is defined', async () => {
      env.config = Object.assign({}, defaultConfig, { 
        fields: { actionSources: [] }
      });
      env.viewMode = 'edit';
      env.fixture.detectChanges();
      const layoutBox = await harness.getDisplayText('bmi-core-action-widget');
      expect(layoutBox).toBe('Action Widget: this will be invisible outside edit mode');
    });
    it('should show as empty for no actions message in view mode', async () => {
      env.config = Object.assign({}, defaultConfig, {
        fields: { actionSources: [] }
      });
      env.fixture.detectChanges();
      const layoutBox = await harness.getDisplayText('bmi-core-action-widget');
      expect(layoutBox).toBe('');
    }); 
  });

  describe('Display actions', () => {
    /**
     * @todo
     * Incomplete. 
     * While this test provides some coverage, it's only currently testing for 
     * cases with no display data
     * We'll need to test looped item displays to complete these tests
     */
  });

});