import { Component, OnInit, Optional } from '@angular/core';

import { map } from 'rxjs/operators';

import { TypedFormControl, TypedFormGroup, TemplatedFormArray, Option } from '@bmi/ui';
import { PathResolver } from '@bmi/gravity-services';
import { ObjectMap } from '@bmi/utils';

import { WidgetContext } from '../../widget-context';
import { PageDataContext } from '../../page-data-context/page-data-context';
import { ParameterBinding } from '../../page-data-context/parameter-binding';
import { PageInfoService } from '../../page/page-info.service';
import { BmiRoute } from '../../navigation/bmi-route';

import { distinctUntilNotEqual } from '../../util/distinct-until-not-equal';

import { ActionSourceConfig, ActionWidgetConfig, ActionWidgetFields } from './action-widget-config';

@Component({
  selector: 'bmi-action-widget-editor',
  templateUrl: './action-widget-editor.component.html'
})
export class ActionWidgetEditorComponent implements OnInit {

  readonly form = new TypedFormGroup<ActionWidgetFields>({
    noActionsMessage: new TypedFormControl(''),
    columns: new TypedFormControl(1),
    density: new TypedFormControl('comfortable'),
    actionSources: new TemplatedFormArray<ActionSourceConfig>(() => new TypedFormGroup({
      dataSourceId: new TypedFormControl(''),
      severity: new TypedFormControl('default'),
      title: new TypedFormControl(''),
      body: new TypedFormControl(''),
      route: new TypedFormControl<BmiRoute>(null),
      data: new TypedFormControl<ObjectMap<ParameterBinding>>({})
    }))
  });

  private resolvers = new Map<string, PathResolver>();
  moduleId = this.pageInfo ? this.pageInfo.currentModuleId : null;

  densityOptions: Option[] = [
    { value: 'comfortable', text: 'Comfortable' },
    { value: 'compact', text: 'Compact' }
  ];

  get actionSources() {
    return this.form.controls.actionSources as TemplatedFormArray<ActionSourceConfig>;
  }

  constructor(
    private widgetContext: WidgetContext<ActionWidgetConfig>,
    @Optional() private dataContext: PageDataContext,
    @Optional() private pageInfo: PageInfoService
  ) { }

  ngOnInit() {
    this.widgetContext.widgetConfig.subscribe(config => {
      this.form.patchValue(config.fields, { emitEvent: false });
    });

    this.widgetContext.widgetConfig.pipe(
      map(config => config.fields.actionSources.map(s => s.dataSourceId)),
      distinctUntilNotEqual(),
    ).subscribe(sources => this.updatePathResolvers(sources));

    this.form.valueChanges.pipe().subscribe(value => {
      this.widgetContext.updateWidgetConfig(config => config.fields = value);
    });
  }

  getResolver(dataSourceId: string) {
    return this.resolvers.get(dataSourceId);
  }

  private updatePathResolvers(dataSources: string[]) {
    this.resolvers.clear();
    if (this.dataContext) {
      for (const source of dataSources) {
        this.resolvers.set(source, this.dataContext.getPathResolverNow({ thisDataSourceId: source }));
      }
    }
  }

  addActionSource() {
    this.actionSources.pushValue({
      dataSourceId: null,
      title: '{{name}}',
      body: '',
      severity: 'default',
      data: {
        'name': { bindingType: 'data-path', path: 'this.title' }
      },
      route: null
    });
  }
}
