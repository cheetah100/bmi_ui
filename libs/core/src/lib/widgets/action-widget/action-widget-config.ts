import { WidgetConfig } from '../../widget-config';
import { ParameterBinding } from '../../page-data-context/parameter-binding';
import { BmiRoute } from '../../navigation/bmi-route';


export interface ActionWidgetFields {
  noActionsMessage?: string;
  actionSources: ActionSourceConfig[];
  columns?: number;
  density?: string;
}


export interface ActionSourceConfig {
  dataSourceId: string;
  title: string;
  body: string;
  severity: string;
  data: { [field: string]: ParameterBinding };
  route: BmiRoute;
}


export type ActionWidgetConfig = WidgetConfig<ActionWidgetFields>;
