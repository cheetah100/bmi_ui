import { Component, Input, OnChanges } from '@angular/core';

import { ObjectMap } from '@bmi/utils';
import { ActionItem } from './action-item';

@Component({
  selector: 'bmi-core-action-item',
  templateUrl: './action-item.component.html',
  styleUrls: ['./action-item.component.scss']
})
export class ActionItemComponent implements OnChanges {
  @Input() action: ActionItem;
  @Input() showIcon: boolean = true;
  @Input() compact: boolean = false;

  actionStyles: ObjectMap<{type: string, icon: string}> = {
    'default': { type: 'info', icon: 'icon-info-outline' },
    'info': { type: 'info', icon: 'icon-info-outline' },
    'warning': { type: 'warning', icon: 'icon-warning-outline' },
    'critical': { type: 'critical', icon: 'icon-warning-outline' },
    'success': { type: 'success', icon: 'icon-check-outline' },
  };

  actionStyle = this.actionStyles['default'];

  ngOnChanges() {
    const severity = this.action ? this.action.severity : null;
    if (this.actionStyles[severity]) {
      this.actionStyle = this.actionStyles[severity];
    } else {
      this.actionStyle = this.actionStyles['default'];
    }
  }
}
