import { Component } from '@angular/core';
import { combineLatestDeferred, combineLatestObject } from '@bmi/utils';
import mapValues from 'lodash-es/mapValues';
import { combineLatest, Observable, of } from 'rxjs';
import {
  catchError,
  map,
  publishBehavior,
  refCount,
  switchMap
} from 'rxjs/operators';
import { FormatterService } from '../../formatter';
import {
  getRouteBindings
} from '../../navigation/bmi-route-utils';
import { Binder } from '../../page-data-context/binder';
import { PageDataContext } from '../../page-data-context/page-data-context';
import { FormatterBinding, ParameterBinding } from '../../page-data-context/parameter-binding';
import { RoutesService } from '../../services/routes.service';
import { mustacheDown } from '../../util/mustachedown';
import { WidgetContext } from '../../widget-context';
import { ActionItem } from './action-item';
import { ActionSourceConfig, ActionWidgetConfig } from './action-widget-config';


@Component({
  selector: 'bmi-core-action-widget',
  templateUrl: './action-widget.component.html',
  styleUrls: ['./action-widget.component.scss']
})
export class ActionWidgetComponent {
  readonly actions = this.widgetContext.widgetConfig.pipe(
    switchMap(config =>
      combineLatest(config.fields.actionSources.map(at => this.getActions(at)))
    ),
    map(results => results.reduce((a, v) => a.concat(v)), []),
    publishBehavior<ActionItem[]>([]),
    refCount()
  );

  hasActions = this.actions.pipe(map(actions => actions.length > 0));

  noActionsMessage = combineLatest([
    this.widgetContext.widgetConfig,
    this.widgetContext.isEditMode
  ]).pipe(
    map(([config, isEditMode]) => {
      const message = config.fields.noActionsMessage;
      if (isEditMode && !message) {
        return 'Action Widget: this will be invisible outside edit mode';
      } else {
        return message;
      }
    })
  );

  density = this.widgetContext.widgetConfig.pipe(
    map(config => config.fields.density || 'comfortable')
  );

  columns = this.widgetContext.widgetConfig.pipe(
    map(config => (config.fields.columns > 0 ? config.fields.columns : 1))
  );

  showAsCompact = this.density.pipe(map(density => density === 'compact'));

  constructor(
    private widgetContext: WidgetContext<ActionWidgetConfig>,
    private dataContext: PageDataContext,
    private routeService: RoutesService,
    private formatterService: FormatterService,
  ) {}

  private getActions(
    sourceConfig: ActionSourceConfig
  ): Observable<ActionItem[]> {
    return this.dataContext
      .bindThis(
        sourceConfig.dataSourceId,
        this.getRequiredBindings(sourceConfig)
      )
      .pipe(
        switchMap(result =>
          combineLatestDeferred(
            result.rows.map(r => this.makeAction(sourceConfig, r))
          )
        ),

        // TODO: better error handling is needed, but this needs some API
        // improvements to properly support this -- handling an error here will
        // complete the 'bindThis()' stream, but if the data sources change (e.g.
        // the user adds a new source) then this bindThis won't pick it up because
        // it has already errored out.
        catchError(() => of([]))
      );
  }

  private getRequiredBindings(
    sourceConfig: ActionSourceConfig
  ): ParameterBinding[] {
    return [
      ...Object.values(sourceConfig.data || {}),
      ...getRouteBindings(sourceConfig.route)
    ].filter(x => x);
  }

  private makeAction(
    actionConfig: ActionSourceConfig,
    binder: Binder
  ): Observable<ActionItem> {
    return combineLatestObject({
      ...mapValues(actionConfig.data, b => {
        if (b.hasOwnProperty('bindingType') && b['bindingType'] === 'formatter') {
          return this.formatterService.bindFormatter(b as FormatterBinding, binder);
        }
        return binder.bind(b);
      }),
      route: this.routeService.resolveRoute(actionConfig.route, {binder})
    }).pipe(
      map(actionData => {
        const template = (text: string, asMarkdown = true) =>
          mustacheDown(text, {
            context: actionData,
            formatMarkdown: asMarkdown,
            handleError: err => {
              console.error(
                'Action Widget: Mustache/Markdown Error',
                actionConfig,
                err
              );
              return text;
            }
          });
        return <ActionItem>{
          titleHtml: template(actionConfig.title),
          bodyHtml: template(actionConfig.body),
          severity: template(actionConfig.severity || 'default', false),
          route: actionConfig.route
        };
      })
    );
  }
}
