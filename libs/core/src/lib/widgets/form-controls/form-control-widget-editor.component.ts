import { Component, OnInit, Optional } from '@angular/core';
import { of, iif, Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { TypedFormGroup, TypedFormControl, Option } from '@bmi/ui';

import { WidgetConfig } from '../../widget-config';
import { WidgetContext } from '../../widget-context';
import { FormContext } from '../../forms/form-context';

export interface FormControlWidgetFields {
  fieldId: string;
  required?: boolean;
}

@Component({
  template: `
    <ng-container [formGroup]="form">
      <ui-form-field label="Field ID">
        <ng-container [ngSwitch]="useDropdownFieldList">
          <ui-select
            *ngSwitchCase="true"
            formControlName="fieldId"
            [options]="fieldOptions | async"
          ></ui-select>
          <input *ngSwitchDefault formControlName="fieldId" />
        </ng-container>
      </ui-form-field>
    </ng-container>
  `
})
export class FormControlWidgetEditorComponent implements OnInit {
  form = new TypedFormGroup<FormControlWidgetFields>({
    fieldId: new TypedFormControl(null)
  });

  useDropdownFieldList = !!this.formContext;

  fieldOptions = this.formContext
    ? this.formContext.fields.pipe(
        map(fields => fields.map(f => <Option>{ value: f.id, text: f.label })),
        shareReplay(1)
      )
    : of(<Option[]>[]);

  constructor(
    private widgetContext: WidgetContext<WidgetConfig<FormControlWidgetFields>>,
    @Optional() private formContext: FormContext
  ) {}

  ngOnInit() {
    this.widgetContext.widgetConfig.subscribe(config =>
      this.form.patchValue(config.fields, { emitEvent: false })
    );
    this.form.valueChanges.subscribe(fields =>
      this.widgetContext.updateWidgetConfig(c => (c.fields = fields))
    );
  }
}
