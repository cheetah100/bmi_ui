import { NgModule, Provider, Type } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { PortalModule } from '@angular/cdk/portal';

import { SharedUiModule } from '@bmi/ui';
import { BmiCoreModule } from '../../bmi-core.module';
import { BmiCoreEditorComponentsModule } from '../../editor-components/bmi-core-editor-components.module';
import { BmiWidgetEditorCommonModule } from '../../bmi-core-widget-editor.module';

import { GravityUiModule } from '@bmi/gravity-services';

import {
  defineEditor,
  defineConfigTemplate,
  defineGenericWidgetEditor
} from '../../util/editor-def-helpers';

import { WidgetConfig } from '../../widget-config';
import { EditorWidgets } from '../../forms/generic-editor-schema';

import { FormControlWidgetEditorComponent } from './form-control-widget-editor.component';
import { ButtonWidgetFields } from './button/button-widget.component';



@NgModule({
  imports: [
    CommonModule,
    BmiCoreModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    PortalModule,
    SharedUiModule,
    GravityUiModule,
    BmiCoreEditorComponentsModule,
    BmiWidgetEditorCommonModule
  ],

  declarations: [
    FormControlWidgetEditorComponent,
  ],

  entryComponents: [
    FormControlWidgetEditorComponent,
  ],

  providers: [
    defineEditor('form-control:checkbox', FormControlWidgetEditorComponent, {
      name: 'Checkbox',
      groups: ['Form Controls'],
      icon: 'fa4-check-square-o',
      widgetConfig: {
        widgetType: 'form-control:checkbox',
        fields: {
          fieldId: null
        }
      }
    }),

    defineEditor('form-control:text-input', FormControlWidgetEditorComponent, {
      name: 'Text Input',
      groups: ['Form Controls'],
      icon: 'shared-ui-i-cursor',
      widgetConfig: {
        widgetType: 'form-control:text-input',
        fields: {
          fieldId: null
        }
      }
    }),

    defineEditor('form-control:textarea', FormControlWidgetEditorComponent, {
      name: 'Long text input',
      groups: ['Form Controls'],
      icon: 'shared-ui-i-cursor',
      widgetConfig: {
        widgetType: 'form-control:textarea',
        fields: {
          fieldId: null
        }
      }
    }),

    defineEditor(
      'form-control:numeric-input',
      FormControlWidgetEditorComponent,
      {
        name: 'Numeric Input',
        groups: ['Form Controls'],
        widgetConfig: {
          widgetType: 'form-control:numeric-input',
          fields: {
            fieldId: null
          }
        }
      }
    ),

    defineEditor(
      'form-control:dropdown-select',
      FormControlWidgetEditorComponent,
      {
        name: 'Dropdown Select',
        groups: ['Form Controls'],
        icon: 'fa4-caret-square-o-down',
        widgetConfig: {
          widgetType: 'form-control:dropdown-select',
          fields: {
            fieldId: null
          }
        }
      }
    ),
    defineEditor(
      'form-control:select-input',
      FormControlWidgetEditorComponent,
      {
        name: 'Select Input',
        groups: ['Form Controls'],
        icon: 'fa4-caret-square-o-down',
        widgetConfig: {
          widgetType: 'form-control:select-input',
          fields: {
            fieldId: null,
          }
        }
      }
    ),
    defineEditor(
      'form-control:dropdown-multiselect',
      FormControlWidgetEditorComponent,
      {
        name: 'Dropdown Multi Select',
        groups: ['Form Controls'],
        icon: 'fa4-caret-square-o-down',
        widgetConfig: {
          widgetType: 'form-control:dropdown-multiselect',
          fields: {
            fieldId: null
          }
        }
      }
    ),
    defineEditor('form-control:datepicker', FormControlWidgetEditorComponent, {
      name: 'Datepicker',
      groups: ['Form Controls'],
      icon: 'fa4-calendar',
      widgetConfig: {
        widgetType: 'form-control:datepicker',
        fields: {
          fieldId: null
        }
      }
    }),
    defineEditor('form-control:list', FormControlWidgetEditorComponent, {
      name: 'List',
      groups: ['Form Controls'],
      icon: 'fa4-list',
      widgetConfig: {
        widgetType: 'form-control:list',
        fields: {
          fieldId: null
        }
      }
    }),
    defineEditor('form-control:map', FormControlWidgetEditorComponent, {
      name: 'Map',
      groups: ['Form Controls'],
      icon: 'fa4-th-list',
      widgetConfig: {
        widgetType: 'form-control:map',
        fields: {
          fieldId: null
        }
      }
    }),
    defineEditor('form-control:cron', FormControlWidgetEditorComponent, {
      name: 'Scheduler',
      groups: ['Form Controls'],
      icon: 'fa4-th-clock-o',
      widgetConfig: {
        widgetType: 'form-control:cron',
        fields: {
          fieldId: null
        }
      }
    }),


    defineGenericWidgetEditor<WidgetConfig<ButtonWidgetFields>>({
      widgetType: 'form-control:button',
      name: 'Action Button',
      groups: ['Form Controls', "Interactivity and Scripting"],
      icon: 'fa4-share-square-o',
      widgetConfig: {
        widgetType: 'form-control:button',
        fields: {
          buttonLabel: 'Button',
          onClick: [{type: 'markdown-confirmation', message: 'Button was clicked'}],
        }
      },
      schema: {
        buttonLabel: EditorWidgets.textInput({label: 'Button Label'}),
        enableWhenFormValid: EditorWidgets.checkbox({label: 'Only enable if form is valid'}),
        onClick: EditorWidgets.eventEditor({label: 'Event: On Click'}),
      }
    }),

    defineConfigTemplate({
      name: 'Form Save Button',
      groups: ['Form Controls'],
      icon: 'fa4-share-square-o',
      widgetConfig: {
        widgetType: 'form-control:button',
        fields: {
          buttonLabel: 'Save',
          onClick: [
            {type: 'trigger-form-action', action: 'save'}
          ]
        }
      }
    })
  ]
})
export class FormControlEditorRegistryModule {}
