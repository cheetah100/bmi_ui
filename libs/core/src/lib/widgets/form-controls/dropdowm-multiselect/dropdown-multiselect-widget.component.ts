import { Component, Optional } from '@angular/core';
import { FormControl } from '@angular/forms';
import { combineLatest, of } from 'rxjs';
import { distinctUntilChanged, map, switchMap, tap } from 'rxjs/operators';

import { FormContext, FormField } from '../../../forms/form-context';
import { WidgetConfig } from '../../../widget-config';
import { WidgetContext } from '../../../widget-context';

import { OptionGroup } from '@bmi/ui';
import { OptionlistService } from '../../../services/optionlist.service';

export interface DropdownSelectWidgetFields {
  fieldId: string;
}


@Component({
  template: `
    <ui-form-field [label]="label | async">
      <ui-multi-select [formControl]="control | async"
                       [optionGroups]="optionGroups | async"
                       data-cy="form-control:dropdown-multiselect"
      ></ui-multi-select>
    </ui-form-field>
  `
})
export class DropdownMultiselectWidgetComponent {

  field = this.widgetContext.widgetConfig.pipe(
    map(config => config.fields.fieldId),
    distinctUntilChanged(),
    switchMap(fieldId => this.formContext ? this.formContext.get(fieldId) : of(<FormField>undefined))
  );

  control = this.field.pipe(map(field => field ? field.control : new FormControl()));

  label = combineLatest([
    this.field,
    this.widgetContext.widgetConfig
  ]).pipe(
    map(([field, config]) => (field && field.label) || 'No label')
  );

  optionGroups = this.field.pipe(
    distinctUntilChanged(),
    switchMap(field => field && field.optionlist ? this.optionlistService.getOptionGroups(field.optionlist) : of(<OptionGroup[]>[]).pipe(
      tap(group => console.log('group:', group))
    )),
  );

  constructor (
    private widgetContext: WidgetContext<WidgetConfig<DropdownSelectWidgetFields>>,
    private optionlistService: OptionlistService,
    @Optional() private formContext: FormContext,
  ) {}
}
