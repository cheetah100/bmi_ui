import { ComponentFixture, fakeAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { EventService } from '../../../events';
import { ButtonWidgetConfig } from '../../../page-migrations/button-widget-add-default-click-handler';
import { TestEventService } from '../../testing/test-event-service';
import { WidgetHarness } from '../../testing/widget-harness';
import { WidgetHostHostComponent } from '../../testing/widget-host-host.component';
import { WidgetTestEnvironment } from '../../testing/widget-test-environment';
import { WidgetTestOptions } from '../../testing/widget-test-options';
import { ButtonWidgetComponent } from './button-widget.component';

const eventService = new TestEventService;
const envOptions: WidgetTestOptions<ButtonWidgetConfig, ButtonWidgetComponent> = {
  config: {
    widgetType: 'form-control:button',
    fields: {
      buttonLabel: 'Action',
      enableWhenFormValid: true,
      onClick: [
        {
          type: 'markdown-confirmation',
          message: 'You clicked.'
        }
      ]
    }
  },
  componentType: ButtonWidgetComponent,
  override: testBed => testBed.overrideComponent(
    ButtonWidgetComponent, {
      set: {
        providers: [{provide: EventService, useValue: eventService }]
      }
    }
  )
}
let harness: WidgetHarness;
let fixture: ComponentFixture<WidgetHostHostComponent>;
let env: WidgetTestEnvironment<ButtonWidgetConfig, ButtonWidgetComponent>;

describe('ButtonWidget', () => {

  beforeEach(async () => {
    env = new WidgetTestEnvironment<ButtonWidgetConfig, ButtonWidgetComponent>(envOptions);
    fixture = env.fixture;
    harness = await env.getHarness(WidgetHarness);
    eventService.resetEmitted();
  });

  describe('edit mode', () => {
    it('should show a button with the correct label', async () => {
      const buttonText = await harness.getDisplayText('.btn');
      expect(buttonText).toBe('Action');
    })
  });

  describe('click action', () => {

    let clickableButton;
    beforeEach(async () => {
      clickableButton = fixture.debugElement.query(By.css('.btn'));
      fixture.detectChanges();
    });

    it('should not trigger an event in edit mode', async() => {
      env.viewMode = 'edit';
      fixture.detectChanges();
      clickableButton.nativeElement.click();
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(eventService.hasEmitted).toBe(false);
      });
    });

    it('should trigger an event with the onClick config in view mode', fakeAsync(() => {
      clickableButton.nativeElement.click();
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(eventService.hasEmitted).toBe(true);
      });
    }));

  });

});
