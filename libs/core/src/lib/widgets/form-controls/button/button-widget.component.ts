import { Component, OnDestroy, OnInit, Optional } from '@angular/core';
import { combineLatest, of, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { EventActionConfig, EventService } from '../../../events';
import { FormContext } from '../../../forms/form-context';
import { WidgetConfig } from '../../../widget-config';
import { WidgetContext } from '../../../widget-context';


export interface ButtonWidgetFields {
  buttonLabel?: string;
  enableWhenFormValid?: boolean;
  onClick?: EventActionConfig[];
}


@Component({
  template: `
    <button
        type="button"
        class="btn btn--{{ buttonClass | async }}"
        data-cy="form-control:button"
        (click)="buttonClicked()"
        [disabled]="isDisabled | async">
      {{ label | async }}
    </button>
  `,
  providers: [
    EventService
  ]
})
export class ButtonWidgetComponent implements OnInit, OnDestroy {
  subscription = new Subscription();
  disabled = true;

  label = this.widgetContext.widgetConfig.pipe(
    map(config => config.fields && config.fields.buttonLabel || 'Save')
  );

  isValid = this.formContext ? this.formContext.isValid : of(true);
  disableWhenInvalid = this.widgetContext.widgetConfig.pipe(
    map(config => !config.fields.enableWhenFormValid)
  );

  isDisabled = combineLatest([
    this.disableWhenInvalid,
    this.isValid,
  ]).pipe(
    map(([disableIfInvalid, isValid]) => !isValid && disableIfInvalid)
  );

  buttonClass = of('primary');

  constructor (
    private widgetContext: WidgetContext<WidgetConfig<ButtonWidgetFields>>,
    private eventService: EventService,
    @Optional() private formContext: FormContext,

  ) {}

  ngOnInit() {
    this.subscription.add(this.widgetContext.widgetConfig.subscribe(config => {
      this.eventService.setConfig({
        click: config.fields.onClick
      });
    }));
  }


  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  buttonClicked() {
    if (this.widgetContext.isViewModeNow) {
      this.eventService.emit('click');
    }
  }
}
