import { NgModule, Provider, Type } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { SharedUiModule } from '@bmi/ui';
import { BmiCoreModule } from '../../bmi-core.module';
import { defineWidget } from '../../util/widget-def-helpers';

import { TextInputWidgetComponent } from './text-input/text-input-widget.component';
import { ButtonWidgetComponent } from './button/button-widget.component';
import { DropdownSelectWidgetComponent } from './dropdown-select/dropdown-select-widget.component';
import { NumericInputWidgetComponent } from './numeric-input/numeric-input-widget.component';
import { CheckboxWidgetComponent } from './checkbox/checkbox-widget.component';
import { DropdownMultiselectWidgetComponent } from './dropdowm-multiselect/dropdown-multiselect-widget.component';
import { DatepickerWidgetComponent } from './datepicker/datepicker-widget.component';

import { MatNativeDateModule } from '@angular/material/core';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';

import { ListEditorWidgetComponent } from './list-editor/list-editor-widget.component';
import { MapEditorWidgetComponent } from './map-editor/map-editor-widget-component';
import { SelectInputWidgetComponent } from './dropdown-select/select-input-widget.component';
import { CronEditorWidgetComponent } from './cron/cron-editor-widget.component';
import { TextareaWidgetComponent } from './text-input/textarea-widget.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedUiModule,
    RouterModule,
    BmiCoreModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule
  ],

  declarations: [
    CheckboxWidgetComponent,
    NumericInputWidgetComponent,
    TextInputWidgetComponent,
    ButtonWidgetComponent,
    SelectInputWidgetComponent,
    DropdownSelectWidgetComponent,
    DropdownMultiselectWidgetComponent,
    DatepickerWidgetComponent,
    ListEditorWidgetComponent,
    MapEditorWidgetComponent,
    CronEditorWidgetComponent,
    TextareaWidgetComponent
  ],

  entryComponents: [
    CheckboxWidgetComponent,
    NumericInputWidgetComponent,
    TextInputWidgetComponent,
    ButtonWidgetComponent,
    SelectInputWidgetComponent,
    DropdownSelectWidgetComponent,
    DropdownMultiselectWidgetComponent,
    DatepickerWidgetComponent,
    ListEditorWidgetComponent,
    MapEditorWidgetComponent,
    CronEditorWidgetComponent,
    TextareaWidgetComponent
  ],

  providers: [
    defineWidget({
      widgetType: 'form-control:numeric-input',
      component: NumericInputWidgetComponent
    }),
    defineWidget({
      widgetType: 'form-control:checkbox',
      component: CheckboxWidgetComponent
    }),
    defineWidget({
      widgetType: 'form-control:text-input',
      component: TextInputWidgetComponent
    }),
    defineWidget({
      widgetType: 'form-control:textarea',
      component: TextareaWidgetComponent
    }),
    defineWidget({
      widgetType: 'form-control:button',
      component: ButtonWidgetComponent
    }),
    defineWidget({
      widgetType: 'form-control:select-input',
      component: SelectInputWidgetComponent
    }),
    defineWidget({
      widgetType: 'form-control:dropdown-select',
      component: DropdownSelectWidgetComponent
    }),
    defineWidget({
      widgetType: 'form-control:dropdown-multiselect',
      component: DropdownMultiselectWidgetComponent
    }),
    defineWidget({
      widgetType: 'form-control:datepicker',
      component: DatepickerWidgetComponent
    }),
    defineWidget({
      widgetType: 'form-control:list',
      component: ListEditorWidgetComponent
    }),
    defineWidget({
      widgetType: 'form-control:map',
      component: MapEditorWidgetComponent
    }),
    defineWidget({
      widgetType: 'form-control:cron',
      component: CronEditorWidgetComponent
    })
  ]
})
export class FormControlWidgetRegistryModule {}
