import { Component, Optional } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { combineLatest, of } from 'rxjs';
import { distinctUntilChanged, map, switchMap } from 'rxjs/operators';

import { FormContext, FormField } from '../../../forms/form-context';
import { WidgetConfig } from '../../../widget-config';
import { WidgetContext } from '../../../widget-context';

export interface TextInputWidgetFields {
  fieldId: string;
  required?: boolean;
}

@Component({
  template: `
    <ui-form-field *ngIf="true" [label]="label | async" [required]="required | async">
      <input [formControl]="control | async" [attr.data-cy]="'form-control:text-input:' + (label | async)" >
    </ui-form-field>
  `
})
export class TextInputWidgetComponent {

  field = this.widgetContext.widgetConfig.pipe(
    map(config => config.fields.fieldId),
    distinctUntilChanged(),
    switchMap(fieldId => this.formContext ? this.formContext.get(fieldId) : of(<FormField>undefined))
  );

  required = this.widgetContext.widgetConfig.pipe(
    map( config => config.fields.required || false)
  );

  control = this.field.pipe(
    map(field => {
      const control:FormControl = field ? field.control : new FormControl();
      return control;
    }),
  );

  label = combineLatest([
    this.field,
    this.widgetContext.widgetConfig
  ]).pipe(
    map(([field, config]) => (field && field.label) || 'No label')
  );

  constructor (
    private widgetContext: WidgetContext<WidgetConfig<TextInputWidgetFields>>,
    @Optional() private formContext: FormContext,
  ) {}
}
