import { Component, Optional } from '@angular/core';
import { FormControl } from '@angular/forms';
import { of } from 'rxjs';
import { distinctUntilChanged, map, switchMap } from 'rxjs/operators';

import { FormContext, FormField } from '../../../forms/form-context';
import { WidgetConfig } from '../../../widget-config';
import { WidgetContext } from '../../../widget-context';
import { FormControlWidgetFields } from '../form-control-widget-editor.component';

@Component({
  template: `
    <ui-form-field
      *ngIf="true"
      [label]="label | async"
      [required]="required | async"
    >
      <textarea [formControl]="control | async" rows="7" data-cy="form-control:textarea"></textarea>
    </ui-form-field>
  `,
  styles: [
    `
      ui-form-field textarea {
        padding: 0.8rem;
        border: 1px solid var(--gra-color--gray-300);
        border-radius: 0.2rem;
        line-height: var(--gra-line-height);
        resize: vertical;
      }
    `
  ]
})
export class TextareaWidgetComponent {
  field = this.widgetContext.widgetConfig.pipe(
    map(config => config.fields.fieldId),
    distinctUntilChanged(),
    switchMap(fieldId =>
      this.formContext
        ? this.formContext.get(fieldId)
        : of(<FormField>undefined)
    )
  );

  required = this.widgetContext.widgetConfig.pipe(
    map(config => config.fields.required || false)
  );

  control = this.field.pipe(
    map(field => (field ? field.control : new FormControl()))
  );

  label = this.field.pipe(map(field => (field && field.label) || 'No label'));

  constructor(
    private widgetContext: WidgetContext<WidgetConfig<FormControlWidgetFields>>,
    @Optional() private formContext: FormContext
  ) {}
}
