import { Component, Optional } from '@angular/core';
import { WidgetContext } from '../../../widget-context';
import { FormControl } from '@angular/forms';
import { map, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { FormField, FormContext } from '../../../forms/form-context';
import { WidgetConfig } from '../../../widget-config';
import { FormControlWidgetFields } from '../form-control-widget-editor.component';

@Component({
  template: `
    <ui-form-field [label]="label | async">
      <bmi-ui-cron-editor [formControl]="control | async" data-cy="form-control:cron" ></bmi-ui-cron-editor>
    </ui-form-field>
  `
})
export class CronEditorWidgetComponent {
  field = this.widgetContext.widgetConfig.pipe(
    map(config => config.fields.fieldId),
    distinctUntilChanged(),
    switchMap(fieldId =>
      this.formContext
        ? this.formContext.get(fieldId)
        : of(<FormField>undefined)
    )
  );
  control = this.field.pipe(
    map(field => (field ? field.control : new FormControl()))
  );
  label = this.field.pipe(map(field => (field && field.label) || 'No label'));

  constructor(
    private widgetContext: WidgetContext<WidgetConfig<FormControlWidgetFields>>,
    @Optional() private formContext: FormContext
  ) {}
}
