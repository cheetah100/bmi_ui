import { FormFieldConfig, FormField } from '../../../forms/form-context';
import { filter, tap, switchMap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { Binder } from '../../../page-data-context/binder';
import { OptionGroup } from '@bmi/ui';
import { OptionlistService } from '../../../services/optionlist.service';

const getFormBoardContext = (
  field: FormFieldConfig,
  binder: Binder
): Observable<string> => {
  if (field.options) {
    if (field.options.useBoardFromField) {
      if (!binder) {
        return null;
      }
      return binder.bind({
        bindingType: 'data-path',
        path: 'form.' + field.options.useBoardFromField
      });
    } else if (field.options.board) {
      return of(field.options.board);
    }
  }
  return of(field.optionlist);
};

export function getOptionGroups(
  fieldData: Observable<FormField>,
  binder: Binder,
  optionlistService: OptionlistService
): Observable<OptionGroup[]> {
  let fieldConfig: FormFieldConfig;

  return fieldData.pipe(
    filter(field => !!field),
    tap((field: FormFieldConfig) => (fieldConfig = field)),
    switchMap(field => getFormBoardContext(field, binder)),
    filter(boardId => !!boardId),
    switchMap((boardId: string) =>
      optionlistService.getOptionGroups(fieldConfig.optionlist, boardId)
    )
  );
}
