import { Component, Optional } from '@angular/core';
import { FormControl } from '@angular/forms';
import { of, Observable } from 'rxjs';
import { distinctUntilChanged, map, switchMap } from 'rxjs/operators';

import { FormContext, FormField } from '../../../forms/form-context';
import { WidgetConfig } from '../../../widget-config';
import { WidgetContext } from '../../../widget-context';
import { OptionlistService } from '../../../services/optionlist.service';
import { OptionGroup } from '@bmi/ui';
import { PageDataContext } from '../../../page-data-context/page-data-context';
import { FormControlWidgetFields } from '../form-control-widget-editor.component';
import { getOptionGroups } from './select-options';

@Component({
  template: `
    <ui-form-field [label]="label | async">
      <bmi-ui-select-input
        [formControl]="control | async"
        [optionGroups]="optionGroups | async"
        data-cy="form-control:select-input"
      ></bmi-ui-select-input>
    </ui-form-field>
  `
})
export class SelectInputWidgetComponent {
  field = this.widgetContext.widgetConfig.pipe(
    map(config => config.fields.fieldId),
    distinctUntilChanged(),
    switchMap(fieldId =>
      this.formContext
        ? this.formContext.get(fieldId)
        : of(<FormField>undefined)
    )
  );

  control = this.field.pipe(
    map(field => (field ? field.control : new FormControl()))
  );

  label = this.field.pipe(map(field => (field && field.label) || 'No label'));

  optionGroups: Observable<OptionGroup[]>;

  constructor(
    private widgetContext: WidgetContext<WidgetConfig<FormControlWidgetFields>>,
    private optionlistService: OptionlistService,
    @Optional() private formContext: FormContext,
    @Optional() private binder: PageDataContext
  ) {
    if (formContext) {
      getOptionGroups(
        this.field,
        this.binder || null,
        this.optionlistService
      ).subscribe(groups => (this.optionGroups = of(groups)));
    }
  }
}
