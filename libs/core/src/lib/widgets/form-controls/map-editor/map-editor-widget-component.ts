import { Component, Optional } from '@angular/core';
import { map, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { FormField, FormContext } from '../../../forms/form-context';
import { FormControl } from '@angular/forms';
import { WidgetContext } from '../../../widget-context';
import { WidgetConfig } from '../../../widget-config';
import { OptionGroup } from '@bmi/ui';
import { FormControlWidgetFields } from '../form-control-widget-editor.component';
import { OptionlistService } from '../../../services/optionlist.service';
import { PageDataContext } from '../../../page-data-context/page-data-context';
import { getOptionGroups } from '../dropdown-select/select-options';

@Component({
  template: `
    <ui-form-field [label]="label | async">
      <bmi-map-editor
        [formControl]="control | async"
        [optionGroups]="optionGroups"
        data-cy="form-control:map"
      >
      </bmi-map-editor>
    </ui-form-field>
  `
})
export class MapEditorWidgetComponent {
  field = this.widgetContext.widgetConfig.pipe(
    map(config => config.fields.fieldId),
    distinctUntilChanged(),
    switchMap(fieldId =>
      this.formContext
        ? this.formContext.get(fieldId)
        : of(<FormField>undefined)
    )
  );

  control = this.field.pipe(
    map(field => (field ? field.control : new FormControl()))
  );
  optionGroups: OptionGroup[];
  label = this.field.pipe(map(field => (field && field.label) || 'No label'));

  constructor(
    private widgetContext: WidgetContext<WidgetConfig<FormControlWidgetFields>>,
    private optionlistService: OptionlistService,
    @Optional() private formContext: FormContext,
    @Optional() private binder: PageDataContext
  ) {
    if (formContext) {
      getOptionGroups(
        this.field,
        this.binder || null,
        this.optionlistService
      ).subscribe(groups => (this.optionGroups = groups));
    }
  }
}
