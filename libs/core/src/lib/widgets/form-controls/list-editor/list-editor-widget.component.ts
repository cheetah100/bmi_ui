import { Component, Optional } from '@angular/core';
import { map, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { of, Observable } from 'rxjs';
import { FormField, FormContext } from '../../../forms/form-context';
import { FormControl } from '@angular/forms';
import { WidgetContext } from '../../../widget-context';
import { WidgetConfig } from '../../../widget-config';
import { FormControlWidgetFields } from '../form-control-widget-editor.component';
import { OptionlistService } from '../../../services/optionlist.service';
import { PageDataContext } from '../../../page-data-context/page-data-context';
import { getOptionGroups } from '../dropdown-select/select-options';
import { OptionGroup } from '@bmi/ui';

@Component({
  template: `
    <ui-form-field [label]="label | async">
      <bmi-list-editor
        [formControl]="control | async"
        [optionGroups]="optionGroups"
        data-cy="form-control:list"
      ></bmi-list-editor>
    </ui-form-field>
  `
})
export class ListEditorWidgetComponent {
  field = this.widgetContext.widgetConfig.pipe(
    map(config => config.fields.fieldId),
    distinctUntilChanged(),
    switchMap(fieldId =>
      this.formContext
        ? this.formContext.get(fieldId)
        : of(<FormField>undefined)
    )
  );

  control = this.field.pipe(
    map(field => (field ? field.control : new FormControl()))
  );
  label = this.field.pipe(map(field => (field && field.label) || 'No label'));
  optionGroups: OptionGroup[];

  constructor(
    private widgetContext: WidgetContext<WidgetConfig<FormControlWidgetFields>>,
    private optionlistService: OptionlistService,
    @Optional() private formContext: FormContext,
    @Optional() private binder: PageDataContext
  ) {
    if (formContext) {
      getOptionGroups(
        this.field,
        this.binder || null,
        this.optionlistService
      ).subscribe(groups => (this.optionGroups = groups));
    }
  }
}
