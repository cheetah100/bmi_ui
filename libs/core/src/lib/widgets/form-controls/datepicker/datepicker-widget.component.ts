import { Component, forwardRef, OnChanges, OnInit, Optional } from '@angular/core';
import { FormControl } from '@angular/forms';
import { combineLatest, of } from 'rxjs';
import { distinctUntilChanged, map, switchMap } from 'rxjs/operators';

import { FormContext, FormField } from '../../../forms/form-context';
import { WidgetConfig } from '../../../widget-config';
import { WidgetContext } from '../../../widget-context';

export interface DatepickerWidgetFields {
  fieldId: string;
  required?: boolean;
}


@Component({
  template: `
    <ui-form-field *ngIf="true" [label]="label | async" [required]="required | async">
      <ui-datepicker [formControl]="control | async" data-cy="form-control:datepicker" ></ui-datepicker>
    </ui-form-field>
  `
})

export class DatepickerWidgetComponent implements OnChanges, OnInit {

  field = this.widgetContext.widgetConfig.pipe(
    map(config => config.fields.fieldId),
    distinctUntilChanged(),
    switchMap(fieldId => this.formContext ? this.formContext.get(fieldId) : of(<FormField>undefined))
  );

  required = this.widgetContext.widgetConfig.pipe(
    map( config => config.fields.required || false)
  );

  control = this.field.pipe(
    map(field => {
      const control:FormControl = field ? field.control : new FormControl('DatePicker');
      return control;
    }),
  );

  label = combineLatest([
    this.field,
    this.widgetContext.widgetConfig
  ]).pipe(
    map(([field, config]) => (field && field.label) || 'No label')
  );

  constructor (
    private widgetContext: WidgetContext<WidgetConfig<DatepickerWidgetFields>>,
    @Optional() private formContext: FormContext,
  ) {
  }

  ngOnInit() {
    // const locale = this.getUsersLocale();
  }
  ngOnChanges(changes) {
  }


  getUsersLocale(defaultValue: string='en-US'): string {
    if (typeof window === 'undefined' || typeof window.navigator === 'undefined') {
      return defaultValue;
    }
    const wn = window.navigator as any;
    let lang = wn.languages ? wn.languages[0] : defaultValue;
    lang = lang || wn.language || wn.browserLanguage || wn.userLanguage;
    return lang;
  }
}
