import { Component, Optional } from '@angular/core';
import { FormControl } from '@angular/forms';
import { combineLatest, of } from 'rxjs';
import { distinctUntilChanged, map, switchMap } from 'rxjs/operators';

import { FormContext, FormField } from '../../../forms/form-context';
import { WidgetConfig } from '../../../widget-config';
import { WidgetContext } from '../../../widget-context';

export interface NumericInputWidgetFields {
  fieldId: string;
}


@Component({
  template: `
    <ui-form-field [label]="label | async">
      <input type="number" [formControl]="control | async" data-cy="form-control:numeric-input" >
    </ui-form-field>
  `
})
export class NumericInputWidgetComponent {

  field = this.widgetContext.widgetConfig.pipe(
    map(config => config.fields.fieldId),
    distinctUntilChanged(),
    switchMap(fieldId => this.formContext ? this.formContext.get(fieldId) : of(<FormField>undefined))
  );

  control = this.field.pipe(map(field => field ? field.control : new FormControl()));

  label = combineLatest([
    this.field,
    this.widgetContext.widgetConfig
  ]).pipe(
    map(([field, config]) => (field && field.label) || 'No label')
  );

  constructor (
    private widgetContext: WidgetContext<WidgetConfig<NumericInputWidgetFields>>,
    @Optional() private formContext: FormContext,
  ) {}
}
