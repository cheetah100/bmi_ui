
export interface FilterDefinition {
  /** The ID of the filter */
  id: string;

  /** The user-visible name for the filter. */
  name: string;

  /**
   * The type of Filter object that parses the raw value
   */
  filterType: string;

  /**
   * The type of filter control to display.
   *
   * If this is falsy, then a
   */
  controlType: string;

  /** An optional default value for the filter. */
  defaultValue?: string;

  config?: {
    optionlist: string;
    includeNullOption?: boolean;
    onlyShowInEditor?: boolean;
  };
}
