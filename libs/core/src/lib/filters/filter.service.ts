import { Injectable, Optional, SkipSelf } from '@angular/core';

import { BehaviorSubject, combineLatest, merge, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, publishReplay, refCount, switchMap, tap } from 'rxjs/operators';

import { ObjectMap, mapObjectValues, combineLatestObject } from '@bmi/utils';

import { Filter, FilterState } from './filter';

// Make an ObjectMap of filter states, taking the first of each id. This is like
// _.keyBy, except that it checks if the state is already in the result. This
// ensures that we're always picking the first state, and it should be
// consistent with Array.find() if there are multiple IDs registered.
function keyByFirstFilterId<T extends {id: string}>(states: T[]): ObjectMap<T> {
  const results: ObjectMap<T> = {};
  for (const s of states) {
    if (s.id && !results[s.id]) {
      results[s.id] = s;
    }
  }
  return results;
}

/**
 * A service for managing filter instances.
 *
 * This is a place where Filter objects can be registered so that control and
 * widgets on the page can observe and update their state.
 *
 * There can be a hierarchy of filter services defined on the page: a component
 * can provide the FilterService to establish a new filtering scope for views
 * below it. Each filter service inherits any filters set on the parent, but can
 * 'shadow' these filters by creating a new filter with the same ID. (like
 * variable scoping in programming languages).
 */
@Injectable()
export class FilterService {
  private filtersSubject = new BehaviorSubject<Filter[]>([]);
  private filterStateSubject = new BehaviorSubject<ObjectMap<FilterState>>({});

  private rawFilterValuesSubject = new BehaviorSubject<ObjectMap<string>>({});

  constructor(
    @Optional() @SkipSelf() private parentFilterService: FilterService
  ) {
    this.filtersSubject.pipe(
      // This pipeline concurrently watches the state of all registered filters.
      // When filters are added/removed, this switchMap will watch again.
      switchMap(filters => combineLatest(filters.map(f => f.state))),
      map(states => keyByFirstFilterId(states)),
    ).subscribe(this.filterStateSubject);

    this.filtersSubject.pipe(
      switchMap(filters => merge(
        ...filters.map(f => f.rawValueChanged.pipe(map(value => ({id: f.id, value})))))
      )
    ).subscribe(({id, value}) => {
        this.setRawFilterValue(id, value);
    });
  }

  get state(): Observable<ObjectMap<FilterState>> {
    return combineLatest(
      this.filterStateSubject,
      this.parentFilterService ? this.parentFilterService.state : of({})
    ).pipe(
      map(([localStates, parentStates]) => ({ ...parentStates, ...localStates })),
      publishReplay(1),
      refCount()
    );
  }

  get currentFilters(): Filter[] {
    return this.filtersSubject.value;
  }

  get rawFilterValues(): Observable<ObjectMap<string>> {
    return this.rawFilterValuesSubject.asObservable();
  }

  get rawFilterValuesSnapshot() {
    return this.rawFilterValuesSubject.value;
  }

  /**
   * Observe a particular filter ID. This will emit if the filter object is
   * removed or replaced.
   *
   * If the filter is not found, this will emit `undefined`.
   */
  watchFilter<T = unknown>(id: string): Observable<Filter<T>> {
    return this.filtersSubject.pipe(
      map(filters => filters.find(x => x.id === id) as Filter<T>),
      distinctUntilChanged(),
      publishReplay(1),
      refCount()
    );
  }

  /**
   * Observes the state of a particular filter ID
   *
   * If the filter is not found, this will emit an `undefined` value.
   */
  watchFilterState<T = unknown>(id: string): Observable<FilterState<T>> {
    return this.state.pipe(
      map(states => states[id] as FilterState<T>),
      distinctUntilChanged(),
      publishReplay(1),
      refCount()
    );
  }

  /**
   * Add an array of filters to the service.
   */
  addFilter(filters: Filter | Filter[]) {
    const filtersToAdd = Array.isArray(filters) ? filters : [filters];

    filtersToAdd.forEach(f => {
      if (this.rawFilterValuesSnapshot[f.id] !== undefined) {
        f.set(this.rawFilterValuesSnapshot[f.id]);
      }
    });

    this.filtersSubject.next([
      ...this.filtersSubject.value,
      ...filtersToAdd
    ]);

  }

  /**
   * Removes one or more filters from the service.
   */
  removeFilter(filters: Filter | Filter[]) {
    const filtersToRemove = Array.isArray(filters) ? filters : [filters];
    const updatedFilters = this.filtersSubject.value.filter(x => !filtersToRemove.includes(x));
    this.filtersSubject.next(updatedFilters);
  }

  private setRawFilterValue(filterId: string, rawValue: string) {
    const newValues = { ...this.rawFilterValuesSubject.value };
    newValues[filterId] = rawValue;
    this.rawFilterValuesSubject.next(newValues);
  }

  /**
   * Sets the value of a filter.
   *
   * If a matching filter is found on this service, it will be updated;
   * otherwise if no filter is found, the set will propagate up to the parent
   * filter service. (This behaviour can be disabled with a parameter)
   *
   * If there are duplicate filter IDs within a service, all instances will be
   * updated with this new value.
   *
   * This method will return false if no matching filter was found to update
   * anywhere up the hierarchy.
   *
   * @param filterId the id of the filter to update
   * @param rawValue the raw filter value to set
   * @param propagateIfNotFound if true, pass this value up to the parent.
   * @returns true if a suitable filter was found, else false.
   */
  set(filterId: string, rawValue: string, propagateIfNotFound = true): boolean {
    // Store this value so that if the filter doesn't yet exist the value will
    // get updated once it's created. This was added to help with binding to a
    // URL -- the filter value from the URL will be available before the page
    // finishes loading, therefore the filters don't exist yet.
    this.setRawFilterValue(filterId, rawValue);

    const matchingFilters = this.filtersSubject.value.filter(x => x.id === filterId);
    if (matchingFilters.length > 0) {
      matchingFilters.forEach(filter => filter.set(rawValue));
      return true;
    } else if (this.parentFilterService && propagateIfNotFound) {
      return this.parentFilterService.set(filterId, rawValue, true);
    } else {
      console.warn(`Cannot set filter, no suitable ID found: ${filterId} = '${rawValue}'`);
      return false;
    }
  }
}
