import { Observable, of, isObservable, Subject, BehaviorSubject, ReplaySubject, EMPTY } from 'rxjs';
import { catchError, filter, map, switchMap, tap } from 'rxjs/operators';

import { Filter, FilterState } from './filter';


export interface FilterValue<T> {
  value: T;
  displayText: string;
}


function ensureObservable<T>(valueOrObservable: T | Observable<T>): Observable<T> {
  return isObservable(valueOrObservable) ? valueOrObservable : of(valueOrObservable);
}


/**
 * A base class for filter implementations
 */
export abstract class AbstractFilter<T> implements Filter<T> {
  private readonly _rawValueSubject = new ReplaySubject<string>(1);
  private readonly _state = new BehaviorSubject<FilterState<T>>(this.makeState(null, null, 'Unset'));

  constructor(
    public readonly id: string,
    public readonly name: string
  ) {
    // When the raw value changes we want to pass it through this pipeline to
    // prepare the resolved value. This could be async, so we switchmap here to
    // ensure we only get the latest value.
    //
    // To keep the pages relatively resilient, we'll catch and handle errors
    // which may occur during the resolution step without interrupting this
    // subscription.
    this._rawValueSubject.pipe(
      switchMap(rawValue => ensureObservable(this.resolveValue(rawValue)).pipe(
        catchError(error => {
          console.error('Error occurred when resolving filter state', this.id, this, error);
          return EMPTY;
        }),
        map(resolvedValue => this.makeState(rawValue, resolvedValue.value, resolvedValue.displayText)),
      ))
    ).subscribe(this._state);
  }

  private makeState(rawValue: string, value: T, displayText: string): FilterState<T> {
    return {
      id: this.id,
      name: this.name,
      rawValue,
      displayText,
      value
    };
  }

  /**
   * Called when the raw value changes, giving the filter a chance to resolve
   * the actual value in order for the state to be emitted.
   *
   * The return value of this method is an object with the value and
   * displayText, which will be turned into the FilterState and emitted.
   */
  protected abstract resolveValue(rawValue: string): Observable<FilterValue<T>> | FilterValue<T>;

  get state() {
    return this._state.asObservable();
  }

  get snapshot() {
    return this._state.value;
  }

  get rawValueChanged() {
    return this._rawValueSubject.asObservable();
  }

  set(rawValue: string): void {
    this._rawValueSubject.next(rawValue);
  }
}

