import { FilterService } from './filter.service';
import { Filter, FilterState } from './filter';
import { StringFilter } from './string-filter';

import { ObjectMap } from '@bmi/utils';


describe('Filter Service', () => {
  let filterService: FilterService;
  let updateCount: number;
  let latestUpdate: ObjectMap<FilterState>;

  beforeEach(() => {
    filterService = new FilterService(null);
    updateCount = 0;
    latestUpdate = undefined;
    filterService.state.subscribe(state => {
      updateCount += 1;
      latestUpdate = state;
    });
  });

  describe('with no filters registered', () => {
    it('should emit the initial state', () => {
      expect(updateCount).toBe(1);
      expect(latestUpdate).toEqual({});
    });
  });


  describe('adding new filter', () => {
    let filter: Filter<string>;
    beforeEach(() => {
      updateCount = 0;
      latestUpdate = undefined;

      // Create the filter and set an initial value before putting it into the
      // service. We don't have to do this, but it's a plausible use case
      filter = new StringFilter('test', 'Foo');
      filter.set('the-value');
      filterService.addFilter(filter);
    });

    it('should notify once', () => expect(updateCount).toBe(1));
    it('should have the new filter in the state map', () => expect(latestUpdate['test']).toBeDefined());
    it('should have the correct value in the state map', () => expect(latestUpdate['test'].rawValue).toBe('the-value'));
  });

  describe('watching an individual filter state', () => {
    let states: FilterState[];
    beforeEach(() => {
      states = [];
      filterService.watchFilterState('subject').subscribe(state => states.push(state));
    });

    it('should return an undefined state before the filter is created', () => {
      expect(states[0]).toBeUndefined();
    });

    it('should emit the filters initial state after it is added', () => {
      filterService.addFilter(new StringFilter('subject', 'The Test Subject'));
      expect(states.length).toBe(2);
      expect(states[1]).toBeDefined();
      expect(states[1].name).toBe('The Test Subject');
    });

    it('should emit when the filter value is changed', () => {
      const filter = new StringFilter('subject', 'The Test Subject');
      filterService.addFilter(filter);
      filter.set('tested');
      expect(states.length).toBe(3);
      expect(states[2].rawValue).toBe('tested');
    });
  });


  describe('set()', () => {
    let filter: StringFilter;
    beforeEach(() => {
      filter = new StringFilter('test', 'Test Filter');
      filterService.addFilter(filter);
    });

    it('should return false if the filter is not found', () => expect(filterService.set('does-not-exist', 'foo')).toBe(false));
    it('should return true if a filter is found', () => expect(filterService.set('test', 'foo')).toBe(true));
    it('should actually update the filter', () => {
      filterService.set('test', 'foo');
      expect(filter.snapshot.rawValue).toBe('foo');
    });
  });

  describe('Removing filters', () => {
    let filter1: Filter;
    let filter2: Filter;
    let filter1Duplicate: Filter;
    beforeEach(() => {
      filter1 = new StringFilter('test1', 'Test');
      filter2 = new StringFilter('test2', 'Test2');
      filter1Duplicate = new StringFilter('test1', 'Test 1 duplicate');
      [filter1, filter2, filter1Duplicate].forEach(f => filterService.addFilter(f));

      updateCount = 0;
      latestUpdate = undefined;
    });

    it('should remove a single filter object', () => {
      filterService.removeFilter(filter1);
      expect(filterService.currentFilters).not.toContain(filter1);
    });

    it('should support removing an array of filters in a single update', () => {
      filterService.removeFilter([filter1, filter2]);
      expect(updateCount).toBe(1);
      expect(filterService.currentFilters).not.toContain(filter1);
      expect(filterService.currentFilters).not.toContain(filter2);
    });

    it('should delete based on object identity', () => {
      filterService.removeFilter(filter1Duplicate);
      expect(filterService.currentFilters).not.toContain(filter1Duplicate);
      expect(filterService.currentFilters).toContain(filter1);
    });
  });

});
