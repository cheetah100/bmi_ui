import { Observable } from 'rxjs';

import { Option, OptionGroup } from '@bmi/ui';

/**
 * A Filter object defines and tracks the state of a filter on a page.
 *
 * At its simplest, a filter is just a string value (this is referred to as the
 * rawValue). This might be okay for the most rudimentary filters, but in the
 * real world, that filter value represents something -- perhaps it's the ID for
 * a card in an optionlist, or the ID of a quarter. It's the job of this filter
 * object to map that raw value onto the parsed `value`.
 *
 * There will be different filter implementations for different types of data,
 * and the UI will be served by corresponding controls.
 *
 * A Filter Component may be bound to this filter instance. It would observe the
 * `state` property to get updated whenever the state changes, and to set the
 * value of the filter, it would call `set()` with the new rawValue.
 *
 * When the state changes and the value is resolved, the filter should emit a
 * new FilterState object. The filter state must be immutable to simplify
 * change detection.
 *
 * The FilterService will aggregate the current state of all of the active
 * filters, making this easily available to widgets.
 */
export interface Filter<T = unknown> {
  id: string;
  name: string;
  state: Observable<FilterState<T>>;
  snapshot: FilterState<T>;

  /**
   * Emits the raw value when it is set.
   *
   * This allows the FilterService which owns this filter to keep track of the
   * state when set() is called on the filter object.
   *
   * Ideally, the filter service should be the authoritative source for the raw
   * filter state, as this can be set before the page is loaded and filters
   * initialized. Values from the URL need to be set on the filters, but should
   * not be interfered with by the normal initialization of filter objects.
   */
  rawValueChanged: Observable<string>;

  /**
   * [Optionlist Protocol] Provides a list of known option values
   *
   * This is used as part of the optionlist protocol. If this property is
   * defined, then this can emit a list of known options for the filter,
   * intended to be fed into a select control.
   */
  options?: Observable<Option[]>;

  optionGroups?: Observable<OptionGroup[]>;


  // TODO: multiselect?
  //
  // Multi-select is a pretty common feature for filters, so it would be good if
  // there was a standardized protocol for handling multiple filter values.
  //
  // @angular/router treats array values as comma separated, so
  // rawValue,rawValue... would be a good choice for compatibility there.
  //
  // We could provide an overload of set() which accepted an array of strings,
  // and the FilterState could have a value and a `values` parameter. The latter
  // would be a single element array for a regular filter.

  set(rawValue: string): void;

}

/**
 * A snapshot of the state of a filter.
 *
 * This object must be immutable. When the filter is updated, a new state should
 * be produced.
 */
export interface FilterState<T = unknown> {
  /**
   * The ID of the filter.
   */
  readonly id: string;

  /**
   * The name of the filter.
   */
  readonly name: string;

  /**
   * Nice display text representing the filter value, for use in the UI
   */
  readonly displayText: string;

  /**
   * The raw value of this filter state.
   */
  readonly rawValue: string;

  /**
   * The resolved value of this filter. This could be a Card object, or a
   * Quarter, or a string, a parsed number, etc.
   */
  readonly value: T;
}

