import { defer, Observable, of } from 'rxjs';
import { map, publishReplay, refCount } from 'rxjs/operators';

import { AbstractFilter, FilterValue } from './abstract-filter';

import { CardData } from '@bmi/gravity-services';
import { Option, OptionGroup } from '@bmi/ui';
import { OptionlistService } from '../services/optionlist.service';
import { FilterDefinition } from './filter-definition';

export class GravityOptionlistFilter extends AbstractFilter<string> {
  constructor(
    private def: FilterDefinition,
    private optionlistService: OptionlistService,
  ) {
    super(def.id, def.name);
  }

  get boardId() {
    return this.def.config?.optionlist;
  }

  get includeNullOption() {
    return this.def.config?.includeNullOption ?? false;
  }

  options = this.optionlistService.getOptions(this.boardId).pipe(
    map(options => {
      if (this.includeNullOption) {
        return [{value: null, text: 'None'}, ...options];
      } else {
        return options;
      }
    }),
    publishReplay(1),
    refCount(),
  );

  optionGroups = this.optionlistService.getOptionGroups(this.boardId).pipe(
    map(optionGroups => {
      if (this.includeNullOption) {
        return <OptionGroup[]>[
          {options: [{value: null, text: 'None'}]},
          ...optionGroups,
        ]
      } else {
        return optionGroups;
      }
    }),
    publishReplay(1),
    refCount()
  );

  resolveValue(rawValue: string): Observable<FilterValue<string>> {
    if (!rawValue) {
      return of(<FilterValue<string>>{
        value: null,
        displayText: 'Unset'
      });
    } else {
      return this.options.pipe(
        map(options => {
          const card = options.find(o => o.value === rawValue);
          return <FilterValue<string>>{
            value: card ? card.value : null,
            displayText: card ? card.text : `${rawValue} (unknown)`
          };
        })
      )
    }
  }
}
