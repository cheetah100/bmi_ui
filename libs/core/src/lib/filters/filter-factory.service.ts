import { Injectable } from '@angular/core';

import { OptionlistService } from '../services/optionlist.service';

import { FilterDefinition } from './filter-definition';
import { Filter } from './filter';

import { StringFilter } from './string-filter';
import { GravityOptionlistFilter } from './gravity-optionlist-filter';


interface FilterType {
  type: string;
  factory: (x: FilterDefinition) => Filter;
  validate?: (x: FilterDefinition) => boolean;
}


@Injectable({providedIn: 'root'})
export class FilterFactoryService {
  private readonly FILTER_TYPES: FilterType[] = [
    {
      type: 'text',
      factory: (def) => new StringFilter(def.id, def.name)
    },
    {
      type: 'optionlist',
      factory: (def) => new GravityOptionlistFilter(def, this.optionlistService),
      validate: (def) => def.config && !!def.config.optionlist
    }
  ];

  constructor(private optionlistService: OptionlistService) {}

  createFilter(def: FilterDefinition): Filter {
    this.validateFilter(def);
    const filterType = this.FILTER_TYPES.find(x => x.type === def.filterType);
    return filterType.factory(def);
  }

  validateFilter(def: FilterDefinition): void {
    if (!def) {
      throw new Error('Filter definition is null or undefined');
    } else if (!def.id) {
      throw new Error('No filter ID provided');
    }

    const filterType = this.FILTER_TYPES.find(x => x.type === def.filterType);
    if (!filterType) {
      throw new Error(`Unknown filter type: '${def.filterType}'`);
    }

    if (filterType.validate && !filterType.validate(def)) {
      throw new Error(`${def.filterType} filter config is not valid.`);
    }
  }
}
