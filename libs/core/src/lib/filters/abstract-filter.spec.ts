import { fakeAsync, tick, discardPeriodicTasks } from '@angular/core/testing';

import { of, Observable, timer } from 'rxjs';
import { map } from 'rxjs/operators';
import { Filter, FilterState } from './filter';
import { AbstractFilter, FilterValue } from './abstract-filter';



describe('AbstractFilter', () => {
  class TestSynchronousFilter extends AbstractFilter<string> {
    resolveValue(rawValue: string): FilterValue<string> {
      const value = String(rawValue).toUpperCase();
      return {
        value: value,
        displayText: value
      };
    }
  }

  let filter: Filter<string>;
  let notificationCount = 0;
  let emittedState: FilterState<string>;
  let initialState: FilterState<string>;
  beforeEach(() => {
    filter = new TestSynchronousFilter('test', 'Test Filter');
    notificationCount = 0;
    emittedState = undefined;
    initialState = filter.snapshot;
    filter.state.subscribe(state => {
      notificationCount += 1;
      emittedState = state;
    });
  });

  describe('initial state', () => {
    it('should have an initial state snapshot', () => expect(filter.snapshot).toBeTruthy());
    it('should have a null raw value', () => expect(filter.snapshot.rawValue).toBeNull());
    it('should have a null value', () => expect(filter.snapshot.value).toBeNull());
    it('should have a string display text', () => expect(typeof filter.snapshot.displayText).toBe('string'));
    it('should have the id set', () => expect(filter.snapshot.id).toBe('test'));
    it('should have the name set', () => expect(filter.snapshot.name).toBe('Test Filter'));

    it('should emit the initial state when subscribed', () => {
      expect(notificationCount).toBe(1);
      expect(emittedState).toBe(initialState);
    });
  });

  describe('after setting a valid value', () => {
    beforeEach(() => {
      filter.set('test value');
    });

    describe('the state snapshot', () => {
      it('should be a different object from the initial state', () => expect(filter.snapshot).not.toBe(initialState));
      it('should have the new value', () => expect(filter.snapshot.rawValue).toBe('test value'));
    })

    it('should emit the new state', () => {
      expect(notificationCount).toBe(2);
      expect(emittedState).toBe(filter.snapshot);
    });
  });
});


describe('AbstractFilter (with async resolveValue)', () => {
  class TestAsyncFilter extends AbstractFilter<string> {
    resolveValue(rawValue: string): Observable<FilterValue<string>> {
      return timer(1000).pipe(
        map(() => (<FilterValue<string>>{
          displayText: String(rawValue).toUpperCase(),
          value: String(rawValue).toUpperCase()
        }))
      );
    }
  }

  let filter: Filter<string>;
  let initialState: FilterState<string>;
  beforeEach(() => {
    filter = new TestAsyncFilter('test', 'Async Filter Test');
    initialState = filter.snapshot;
  });

  describe('initial state', () => {
    it('should immediately have an initial state', fakeAsync(() => {
      expect(initialState).toBeTruthy();
    }));

    it('should initially have a null value', fakeAsync(() => {
      expect(initialState.rawValue).toBeNull();
    }));
  });

  it('should not update the state until resolveValue() has finished', fakeAsync(() => {
    filter.set('test');
    expect(filter.snapshot).toBe(initialState);
    tick(500);
    expect(filter.snapshot).toBe(initialState);
    discardPeriodicTasks();
  }));

  it('should update the state after resolveValue() has finished', fakeAsync(() => {
    filter.set('test');
    tick(1500);
    expect(filter.snapshot.rawValue).toBe('test');
    expect(filter.snapshot.value).toBe('TEST');
  }));

  it('should emit the new state once finished', fakeAsync(() => {
    let notificationCount = 0;
    let emittedState: FilterState<string>
    filter.state.subscribe(state => {
      emittedState = state;
      notificationCount += 1;
    });

    // The initial state.
    expect(notificationCount).toBe(1);

    filter.set('test');
    tick(500);

    expect(notificationCount).toBe(1);

    tick(500);

    expect(notificationCount).toBe(2);
    expect(emittedState).toBe(filter.snapshot);
    expect(emittedState.rawValue).toBe('test');
  }));
});
