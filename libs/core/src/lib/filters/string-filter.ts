import { AbstractFilter, FilterValue } from './abstract-filter';

/**
 * Implements a string filter.
 *
 * This is the simplest possible filter implementation, it just passes the raw
 * value through unchanged.
 */
export class StringFilter extends AbstractFilter<string> {
  resolveValue(rawValue: string): FilterValue<string> {
    return {
      displayText: rawValue,
      value: rawValue
    };
  }
}
