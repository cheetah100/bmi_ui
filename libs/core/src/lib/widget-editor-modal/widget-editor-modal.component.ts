import { Component } from '@angular/core';
import { ModalContext, TypedFormControl } from '@bmi/ui';
import { WidgetConfig } from '../widget-config';


export interface WidgetEditorModalOptions {
  config: WidgetConfig;
}


@Component({
  template: `
    <bmi-widget-config-editor [formControl]="control" [flairEditing]="false"></bmi-widget-config-editor>
  `
})
export class WidgetEditorModalComponent {
  constructor(
    private modalContext: ModalContext<WidgetConfig>
  ) {}

  get options() {
    return this.modalContext.data as WidgetEditorModalOptions;
  }

  control = new TypedFormControl(this.options.config);

  ngOnInit() {
    this.modalContext.updateOptions({
      actions: [
        {
          action: 'save',
          buttonText: 'OK',
          buttonStyle: 'primary',
          invoke: () => ({action: 'save', data: this.control.value})
        },
        {
          action: 'cancel',
          buttonStyle: 'ghost',
          buttonText: 'Cancel',
        }
      ]
    })
  }
}
