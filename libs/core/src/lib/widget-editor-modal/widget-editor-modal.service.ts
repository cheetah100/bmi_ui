import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { ModalService, ModalResult } from '@bmi/ui';
import { WidgetConfig } from '../widget-config';
import { WidgetEditorModalComponent, WidgetEditorModalOptions } from './widget-editor-modal.component';

@Injectable({providedIn: 'root'})
export class WidgetEditorModalService {
  constructor(
    private modalService: ModalService,
  ) {}

  create(config: WidgetConfig, title = 'Edit Widget', injector?: Injector) {
    return this.modalService.open({
      title,
      content: WidgetEditorModalComponent,
      injector: injector,
      data: {
        config
      }
    }) as Observable<ModalResult<WidgetConfig>>;
  }
}
