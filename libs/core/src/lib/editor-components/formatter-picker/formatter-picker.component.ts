import { Component, Input, forwardRef, OnChanges, Injector, Inject, Optional, SimpleChanges, ViewChild, TemplateRef } from '@angular/core';
import { ComponentPortal, TemplatePortal, Portal } from '@angular/cdk/portal';

import { BehaviorSubject, combineLatest } from 'rxjs';
import { filter, map, shareReplay } from 'rxjs/operators';

import { BaseControlValueAccessor, provideAsControlValueAccessor } from '@bmi/ui';
import { ObjectMap, createMapFrom } from '@bmi/utils';
import { distinctUntilNotEqual } from '../../util/distinct-until-not-equal';

import {
  ParameterBinding,
  FormatterConfig,
  isFormatterConfig,
  FormatterEditorContext,
  FormatterMetadata,
  FORMATTER_METADATA,
} from '../../formatter';

@Component({
  selector: 'bmi-formatter-picker, bmi-binding-picker',
  templateUrl: './formatter-picker.component.html',
  styleUrls: ['./formatter-picker.component.scss'],
  providers: [
  provideAsControlValueAccessor(forwardRef(() => FormatterPickerComponent))]
})
export class FormatterPickerComponent extends BaseControlValueAccessor<ParameterBinding> implements OnChanges {
  @Input() preferredType = 'data-binding';
  @Input() hints: ObjectMap<any> = {};

  @Input() resolver: any;

  @ViewChild('formatterNamePortal', { static: true }) formatterNamePortal: TemplateRef<any>;

  readonly config = new BehaviorSubject<ParameterBinding>(undefined);
  readonly preferredFormatterType = new BehaviorSubject(this.preferredType);
  private readonly hintsSubject = new BehaviorSubject(this.hints);

  readonly formatterType = combineLatest([
    this.config,
    this.preferredFormatterType,
    this.hintsSubject,
  ]).pipe(
    distinctUntilNotEqual(),
    map(([config, preferredType]) => this.resolvePreferredFormatter(config, preferredType)),
    distinctUntilNotEqual(),
  );

  readonly formatterPortal = this.formatterType.pipe(
    map(type => this.createFormatterPortal(type)),
    shareReplay(1)
  );

  formatterMetadata: FormatterMetadata[];
  metadataByType: Map<string, FormatterMetadata>;

  visibleFormatters = this.hintsSubject.pipe(
    map(hints => this.formatterMetadata.filter(fm => !fm.shouldShowFormatter || fm.shouldShowFormatter(hints)))
  );

  constructor(
    private injector: Injector,
    @Optional() @Inject(FORMATTER_METADATA) formatterMetadata: FormatterMetadata[]
  ) {
    super();
    this.formatterMetadata = formatterMetadata || [];
    this.metadataByType = createMapFrom(this.formatterMetadata, fm => fm.formatterType);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.preferredType) {
      this.preferredFormatterType.next(this.preferredType);
    }

    if (changes.hints) {
      this.hintsSubject.next(this.hints);
    }
  }

  updateComponentValue(config: ParameterBinding) {
    this.config.next(config);
  }

  configUpdated(config: ParameterBinding) {
    this.onChanged(config);
    this.config.next(config);
  }

  updateFormatterConfig(config: ParameterBinding | FormatterConfig) {
    if (isFormatterConfig(config)) {
      this.configUpdated({...config, bindingType: 'formatter'});
    } else {
      this.configUpdated(config);
    }
  }

  getFormatterType(config: ParameterBinding) {
    if (isFormatterConfig(config)) {
      return config.formatterType;
    } else if (typeof config === 'string') {
      return 'text';
    } else if (config) {
      return 'data-binding';
    } else {
      return null;
    }
  }

  selectFormatterType(chosenType: string) {
    this.preferredFormatterType.next(chosenType);

    const currentConfig = this.config.value;
    const currentType = this.getFormatterType(currentConfig);
    if (!this.areFormattersCompatible(currentType, chosenType)) {
      this.configUpdated(this.makeDefaultValue(chosenType));
    }
  }

  private makeDefaultValue(configType: string): ParameterBinding {
    const currentMetadata = this.metadataByType.get(configType);
    const defaultConfig = currentMetadata && currentMetadata.defaultConfig;
    if (!configType || defaultConfig === undefined) {
      return null;
    } else if (defaultConfig && typeof defaultConfig === 'object' && configType !== 'data-binding') {
      return {
        ...defaultConfig,
        formatterType: configType,
        bindingType: 'formatter',
      };
    } else {
      return defaultConfig as ParameterBinding;
    }
  }

  private resolvePreferredFormatter(config: ParameterBinding, preferredType: string): string {
    const configType = this.getFormatterType(config);
    const canUsePreferredType = this.areFormattersCompatible(configType, preferredType) && this.isFormatterAvailable(preferredType);
    return canUsePreferredType ? preferredType : configType;
  }

  private areFormattersCompatible(configType: string, preferredType: string) {
    const preferredMetadata = this.metadataByType.get(preferredType);

    return (configType === preferredType)
        || (configType === 'text' && preferredMetadata && preferredMetadata.supportsText)
        || (!configType && preferredMetadata && preferredMetadata.supportsNull);
  }

  private isFormatterAvailable(type: string): boolean {
    const fm = this.metadataByType.get(type);
    return fm && (!fm.shouldShowFormatter || fm.shouldShowFormatter(this.hints));
  }

  private makeNamePortal(name: string): TemplatePortal {
    return new TemplatePortal(this.formatterNamePortal, undefined, {name});
  }

  private createFormatterPortal(formatterType: string): Portal<any> {
    if (!formatterType) {
      return this.makeNamePortal('No value');
    }

    const metadata = this.formatterMetadata.find(m => m.formatterType === formatterType);
    if (metadata && metadata.component) {
      const injector = Injector.create({
        name: 'Formatter Picker Injector',
        parent: this.injector,
        providers: [
          {
            provide: FormatterEditorContext,
            useValue: <FormatterEditorContext>{
              formatterConfig: this.config.pipe(filter(c => this.areFormattersCompatible(this.getFormatterType(c), formatterType))),
              setFormatterConfig: config => this.updateFormatterConfig(config),
              hints: this.hintsSubject.asObservable(),
            }
          }
        ]
      });
      return new ComponentPortal(metadata.component, undefined, injector);
    } else {
      return this.makeNamePortal(metadata && metadata.name || formatterType);
    }
  }
}
