import { Component, Input, forwardRef, OnInit } from '@angular/core';

import {
  BaseControlValueAccessor, provideAsControlValueAccessor, TypedFormControl
} from '@bmi/ui';

import { PathResolver } from '@bmi/gravity-services';
import { BmiRoute } from '../../navigation/bmi-route';

@Component({
  selector: 'bmi-route-editor',
  templateUrl: './bmi-route-editor.component.html',
  providers: [provideAsControlValueAccessor(forwardRef(() => BmiRouteEditorComponent))]
})
export class BmiRouteEditorComponent extends BaseControlValueAccessor<BmiRoute> implements OnInit {
  @Input() resolver: PathResolver;
  @Input() moduleId: string;

  readonly route = new TypedFormControl<BmiRoute>(null);

  get hasLink() {
    return !!this.route.value;
  }

  ngOnInit() {
    this.route.valueChanges.subscribe(value => this.onChanged(value));
  }

  toggleLink() {
    if (this.hasLink) {
      this.route.setValue(null);
    } else {
      this.route.setValue({
        type: 'page',
        moduleId: this.moduleId,
        pageId: null,
        filters: null
      })
    }
  }

  updateComponentValue(route: BmiRoute) {
    this.route.setValue(route, { emitEvent: false });
  }
}
