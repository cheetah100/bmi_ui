import { Component, OnInit, forwardRef, Input, Optional } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { RoutesService } from '../../services/routes.service';
import { BmiRoute, BmiPageRoute, isPageRoute } from '../../navigation/bmi-route';
import { PageFilterInfo } from '../../page-config';
import { PageInfoService } from '../../page/page-info.service';
import { ParameterBinding } from '../../page-data-context/parameter-binding';
import { FilterService } from '../../filters/filter.service';

import {
  BaseControlValueAccessor,
  TypedFormControl,
  TypedFormGroup,
  IdpValidators,
  provideAsControlValueAccessor,
  BaseDropdownComponent,
  OptionGroup,
  Option
} from '@bmi/ui';

import { PathResolver } from '@bmi/gravity-services';
import { ObjectMap, createMapFrom } from '@bmi/utils';
import { objectFromEntries } from '../../util/object-from-entries';

import flatten from 'lodash-es/flatten';
import omitBy from 'lodash-es/omitBy';


const UNSUPPORTED_ROUTE_TYPE_KEY = '__unsupported_route_type';


function makeRouteTypeKey(route: BmiRoute): string {
  if (route === null || route === undefined) {
    return null;
  } else if (typeof route === 'string') {
    return 'url';
  } else if (Array.isArray(route)) {
    return UNSUPPORTED_ROUTE_TYPE_KEY;
  } else if (route.type === 'page') {
    return `page-${route.moduleId}-${route.pageId}`;
  } else if (route.type === 'builtin') {
    return `builtin-${route.moduleId}-${route.routeId}`;
  } else {
    return UNSUPPORTED_ROUTE_TYPE_KEY;
  }
}


interface RouteOptionGroup extends OptionGroup {
  options: RouteOption[];
}


interface RouteOption extends Option {
  value: string;
  text: string;
  route: BmiRoute;

  showParameters?: boolean;
  showUrlInput?: boolean;
  availableFilters?: PageFilterInfo[];
}


@Component({
  selector: 'bmi-core-route-selector',
  styleUrls: ['./route-selector.component.scss'],
  templateUrl: './route-selector.component.html',
  providers: [
    provideAsControlValueAccessor(forwardRef(() => RouteSelectorComponent))
  ],
})
export class RouteSelectorComponent extends BaseControlValueAccessor<BmiRoute> implements OnInit {

  @Input() moduleId: string;
  @Input() resolver: PathResolver;

  optionGroups: RouteOptionGroup[] = [];
  allOptions: RouteOption[] = [];

  selectedOption: RouteOption = null;

  routeTypeControl = new TypedFormControl<string>(null);

  form = new TypedFormGroup({
    route: new TypedFormControl<BmiRoute>(null),
    filters: new TypedFormControl<ObjectMap<ParameterBinding>>(null)
  });

  get showUrlInput() {
    return this.selectedOption && this.selectedOption.showUrlInput;
  }

  get showParameters() {
    return this.selectedOption && this.selectedOption.showParameters;
  }

  get isSupportedRouteType() {
    return this.routeTypeControl.value !== UNSUPPORTED_ROUTE_TYPE_KEY;
  }

  get currentModuleId() {
    return this.moduleId || this.pageInfoService && this.pageInfoService.currentModuleId;
  }

  constructor(
    private routesService: RoutesService,
    @Optional() private pageInfoService: PageInfoService,
    @Optional() private filterService: FilterService,
  ) {
    super();
  }

  ngOnInit() {
    this.getRouteOptionsForModule(this.currentModuleId).subscribe(options => {
      this.optionGroups = [
        {
          title: null,
          collapsible: false,
          options: [
            {value: null, text: 'No route', route: null},
          ]
        },
        {
          title: `${this.moduleId} pages`,
          collapsible: true,
          collapsedByDefault: false,
          options: options
        },

        {
          title: 'Advanced',
          collapsible: true,
          collapsedByDefault: false,
          options: [
            {
              value: 'url',
              text: 'Custom URL',
              route: '',
              showUrlInput: true,
            },
          ]
        }
      ];
      this.allOptions = flatten(this.optionGroups.map(g => g.options));
      this.updateSelectedOption();
    });

    this.routeTypeControl.valueChanges.subscribe(selectedType => {
      this.updateSelectedOption(selectedType);
      const selectedOption = this.selectedOption;
      this.form.patchValue({
        route: selectedOption.route,
        filters: selectedOption.showParameters ?
            this.fillInAvailableFilters(selectedOption, this.form.value.filters) :
            null
      });
    });

    this.form.valueChanges.subscribe(value => {
      const route = value.route;
      if (isPageRoute(route)) {
        this.onChanged({
          ...route,
          filters: value.filters
        });
      } else {
        this.onChanged(route);
      }
    })
  }

  updateComponentValue(route: BmiRoute) {
    this.form.patchValue({
      route: route,
      filters: isPageRoute(route) ? route.filters || {} : {}
    }, {emitEvent: false});
    this.updateSelectedRouteDropdown(false);
  }

  private getRouteOptionsForModule(moduleId: string): Observable<RouteOption[]> {
    return this.routesService.getRouteOptions(moduleId).pipe(
      map(routes => routes.map(r => (<RouteOption>{
        value: makeRouteTypeKey(r.route),
        route: r.route,
        text: r.title,
        showParameters: isPageRoute(r.route),
        showUrlInput: false,
        availableFilters: r.availableFilters,
      })))
    );
  }

  private fillInAvailableFilters(route: RouteOption, existingFilters: ObjectMap<ParameterBinding>): ObjectMap<ParameterBinding> {
    if (route.availableFilters) {
      const filtersOnRoute = route.availableFilters.map(f => f.id);
      const filtersOnCurrentPage = new Set(this.filterService ? this.filterService.currentFilters.map(f => f.id) : []);
      return {
        ...objectFromEntries(filtersOnRoute.map<[string, ParameterBinding]>(filterId => {
          return [filterId, filtersOnCurrentPage.has(filterId) ? {bindingType: 'filter', filterId}  : null ]
        })),
        ...omitBy(existingFilters || {}, (binding, key) => !binding && !filtersOnRoute.includes(key))
      };
    } else {
      return existingFilters;
    }
  }

  private updateSelectedOption(selectedType = this.routeTypeControl.value) {
    this.selectedOption = this.allOptions.find(r => r.value === selectedType);
  }

  private updateSelectedRouteDropdown(emitEvent = false) {
    const route = this.form.value.route;
    const routeKey = makeRouteTypeKey(route);
    this.routeTypeControl.setValue(routeKey, {emitEvent});
    this.updateSelectedOption();
  }

  clearLink() {
    this.routeTypeControl.setValue(null);
  }
}
