import { Component, forwardRef, OnInit } from '@angular/core';
import { map, shareReplay } from 'rxjs/operators';
import { TypedFormGroup, BaseControlValueAccessor, TypedFormControl, provideAsControlValueAccessor, Option } from '@bmi/ui';
import { OptionlistService } from '../../services/optionlist.service';
import { CardEditorConfig } from '../../card-editor/card-editor-config';

@Component({
  selector: 'bmi-card-editor-config',
  templateUrl: './card-editor-config.component.html',
  styles: [`
    ui-form-field {
      margin-bottom: var(--gra-spacing-half);
    }
  `],
  providers: [
    provideAsControlValueAccessor(forwardRef(() => CardEditorConfigComponent))
  ]
})
export class CardEditorConfigComponent extends BaseControlValueAccessor<CardEditorConfig> implements OnInit {
  constructor(
    private optionListService: OptionlistService
  ) {
    super();
  }

  form = new TypedFormGroup<CardEditorConfig>({
    board: new TypedFormControl(null),
    cardId: new TypedFormControl(null),
    defaults: new TypedFormControl({}),
    afterSaveEvent: new TypedFormControl(undefined),
  });

  boardOptions = this.optionListService.getAllBoards().pipe(
    map(boards => boards.map(b => <Option>{value: b.id, text: b.name})),
    shareReplay(1),
  );


  ngOnInit() {
    this.form.valueChanges.subscribe(formValue => {
      const updatedConfig = {...formValue};

      // If these fields aren't configured, we will make them undefined. It
      // would be nicer to remove them altogether, but this ensures that if the
      // value is merged with an existing config it will replace any existing
      // configs for these.
      if (!updatedConfig.defaults || Object.keys(updatedConfig.defaults).length === 0) {
        updatedConfig.defaults = undefined;
      }

      if (!updatedConfig.afterSaveEvent || updatedConfig.afterSaveEvent.length === 0) {
        updatedConfig.afterSaveEvent = undefined;
      }

      this.onTouched();
      this.onChanged(updatedConfig);
    });
  }

  updateComponentValue(config: CardEditorConfig) {
    if (!config) {
      this.form.reset({}, {emitEvent: false});
    } else {
      this.form.patchValue(config, {emitEvent: false});
    }
  }
}
