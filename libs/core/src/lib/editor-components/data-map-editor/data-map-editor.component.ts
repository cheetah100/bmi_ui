import { Component, Input, OnInit, forwardRef } from '@angular/core';

import { map } from 'rxjs/operators';

import {
  TypedFormControl, TypedFormGroup, TemplatedFormArray,
  provideAsControlValueAccessor, BaseControlValueAccessor,
} from '@bmi/ui';
import { PathResolver } from '@bmi/gravity-services';

import { ObjectMap } from '@bmi/utils';

import { distinctUntilNotEqual } from '../../util/distinct-until-not-equal';
import { ParameterBinding } from '../../formatter';


interface DataEntry {
  key: string;
  binding: ParameterBinding;
}


@Component({
  selector: 'bmi-data-map-editor',
  templateUrl: './data-map-editor.component.html',
  styleUrls: ['./data-map-editor.component.scss'],
  providers: [
    provideAsControlValueAccessor(forwardRef(() => DataMapEditorComponent))
  ]
})
export class DataMapEditorComponent extends BaseControlValueAccessor<ObjectMap<ParameterBinding>> implements OnInit {
  @Input() resolver: PathResolver;
  @Input() useFormatters: boolean = false;

  readonly form = new TemplatedFormArray<DataEntry>(
    () => new TypedFormGroup({
      key: new TypedFormControl(''),
      binding: new TypedFormControl<ParameterBinding>(null)
    })
  );

  ngOnInit() {
    this.form.valueChanges.pipe(
      map(value => this.makeData(value)),
      distinctUntilNotEqual()
    ).subscribe(results => {
      this.onChanged(results);
    });
  }

  updateComponentValue(value: ObjectMap<ParameterBinding>) {
    if (!value) {
      this.form.reset([], { emitEvent: false });
    } else {
      const entries = Object.entries(value)
        .map(([key, binding]) => ({ key, binding }));
      this.form.patchValue(entries, { emitEvent: false });
    }
  }

  addParameter() {
    this.form.pushValue({
      key: '',
      binding: null
    });
  }

  private makeData(entries: DataEntry[]): ObjectMap<ParameterBinding> {
    const results: ObjectMap<ParameterBinding> = {};
    for (const { key, binding } of entries) {
      results[key] = binding;
    }
    return results;
  }

}
