import { Component, forwardRef } from '@angular/core';
import { BaseControlValueAccessor, TypedFormGroup, TypedFormControl, provideAsControlValueAccessor } from '@bmi/ui';

import { ConfigurableToolbarItem, ConfigurableToolbarAction, ConfigurableToolbarMenu } from '../../components/configurable-action-buttons/config';

// Items and menus are quite similar here, so the view model will contain the
// mixture of both -- a menu has items, and an action has 'onInvoke' so we can
// sort this out easily enough.
type ItemViewModel =
  {type: 'menu' | 'action' | 'separator'}
  & Omit<ConfigurableToolbarMenu, 'type'>
  & Omit<ConfigurableToolbarAction, 'type'>;


@Component({
  selector: 'bmi-button-toolbar-item-editor',
  templateUrl: './button-toolbar-item-editor.component.html',
  providers: [
    provideAsControlValueAccessor(forwardRef(() => ButtonToolbarItemEditorComponent))
  ]
})
export class ButtonToolbarItemEditorComponent extends BaseControlValueAccessor<ConfigurableToolbarItem> {

  form = new TypedFormGroup<ItemViewModel>({
    type: new TypedFormControl('action'),
    text: new TypedFormControl(null),
    icon: new TypedFormControl(null),
    items: new TypedFormControl([]),
    onInvoke: new TypedFormControl(undefined),
  });

  ngOnInit() {
    this.form.valueChanges.subscribe(value => {
      this.onTouched();
      const commonProps = {
        text: value.text,
        icon: value.icon,
      };

      if (value.type === 'action') {
        this.onChanged({...commonProps, type: 'action', onInvoke: value.onInvoke});
      } else if (value.type === 'menu') {
        this.onChanged({...commonProps, type: 'menu', items: value.items});
      } else {
        this.onChanged({type: value.type});
      }
    });
  }

  updateComponentValue(config: ConfigurableToolbarItem) {
    this.form.patchValue(config ?? {}, {emitEvent: false});
  }
}
