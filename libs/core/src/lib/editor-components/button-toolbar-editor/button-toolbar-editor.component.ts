import { Component, forwardRef, Input } from '@angular/core';
import { BaseControlValueAccessor, TemplatedFormArray, TypedFormControl, provideAsControlValueAccessor } from '@bmi/ui';

import { ConfigurableToolbarItem, ConfigurableToolbarAction, ConfigurableToolbarMenu } from '../../components/configurable-action-buttons/config';

import keyBy from 'lodash-es/keyBy';
import cloneDeep from 'lodash-es/cloneDeep';

interface ItemTypeInfo {
  type: string;
  name: string;
  icon: string;
  config: ConfigurableToolbarItem
}

@Component({
  selector: 'bmi-button-toolbar-editor',
  templateUrl: './button-toolbar-editor.component.html',
  providers: [
    provideAsControlValueAccessor(forwardRef(() => ButtonToolbarEditorComponent))
  ]
})
export class ButtonToolbarEditorComponent extends BaseControlValueAccessor<ConfigurableToolbarItem[]> {

  @Input() isEditingMenu = false;

  form = new TemplatedFormArray<ConfigurableToolbarItem>(() => new TypedFormControl());

  itemTypes: ItemTypeInfo[] = [
    {
      type: 'action',
      name: 'Action Button',
      icon: 'cui-touch-point',
      config: {
        type: 'action',
        text: 'Info',
        icon: 'cui-info',
        onInvoke: null
      }
    },
    {
      type: 'menu',
      name: 'Dropdown Menu',
      icon: 'cui-list-menu',
      config: {
        type: 'menu',
        text: null,
        icon: 'cui-chevron-down',
        items: [],
      }
    },
    {
      type: 'separator',
      name: 'Separator',
      icon: 'fa4-minus',
      config: {type: 'separator'}
    }
  ];

  itemTypesByType = keyBy(this.itemTypes, t => t.type);


  get addButtonText() {
    return this.isEditingMenu ? 'Add menu item': 'Add button';
  }

  ngOnInit() {
    this.form.valueChanges.subscribe(value => this.onChanged(value));
  }

  updateComponentValue(config: ConfigurableToolbarItem[]) {
    this.form.patchValue(config || [], {emitEvent: false});
  }

  addItem(itemInfo: ItemTypeInfo) {
    this.form.pushValue(cloneDeep(itemInfo.config));
  }
}
