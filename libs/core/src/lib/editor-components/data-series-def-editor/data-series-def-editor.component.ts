import { Component, Input, OnInit, forwardRef } from '@angular/core';
import { distinctUntilChanged, shareReplay, map } from 'rxjs/operators';
import { PathResolver } from '@bmi/gravity-services';
import { TypedFormGroup, TypedFormControl, BaseControlValueAccessor, provideAsControlValueAccessor } from '@bmi/ui';

import { ParameterBinding } from '../../page-data-context/parameter-binding';
import { PageDataContext } from '../../page-data-context/page-data-context';
import { DataSeriesDef } from '../../data-series/data-series-def';


const EMPTY_SERIES_DEF: DataSeriesDef = {
  dataSourceId: '',
  seriesId: null,
  label: null,
  fields: {}
};


@Component({
  selector: 'bmi-data-series-def-editor',
  templateUrl: './data-series-def-editor.component.html',
  providers: [
    provideAsControlValueAccessor(forwardRef(() => DataSeriesDefEditorComponent))
  ]
})
export class DataSeriesDefEditorComponent extends BaseControlValueAccessor<DataSeriesDef> implements OnInit {

  @Input() dataFields = ['x', 'y', 'name'];

  resolver: PathResolver;

  readonly form = new TypedFormGroup<DataSeriesDef>({
    dataSourceId: new TypedFormControl(''),
    seriesId: new TypedFormControl(null),
    label: new TypedFormControl(null),
    fields: new TypedFormControl({})
  });

  constructor(
    private binder: PageDataContext
  ) { super(); }

  ngOnInit() {
    this.form.valueChanges.subscribe(dsd => {
      this.onChanged(dsd);
    });
  }

  updateComponentValue(value: DataSeriesDef) {
    const dsd = value || EMPTY_SERIES_DEF;
    this.form.patchValue({
      ...dsd,
      fields: {
        ...this.makeDefaultFields(),
        ...dsd.fields,
      }
    }, {emitEvent: false});
    this.resolver = this.binder.getPathResolverNow({thisDataSourceId: dsd.dataSourceId});
  }

  private makeDefaultFields() {
    const obj: DataSeriesDef['fields'] = {};
    this.dataFields.forEach(f => obj[f] = null);
    return obj;
  }
}
