import { Component, OnInit, EventEmitter, Output, forwardRef } from '@angular/core';
import { map } from 'rxjs/operators';
import { provideAsControlValueAccessor, BaseControlValueAccessor, Option, TypedFormControl } from '@bmi/ui';
import { PageDataContext } from '../../page-data-context/page-data-context';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'bmi-data-source-picker',
  template: `
    <ui-select [options]="dataSourceOptions | async" [formControl]="dataSource"></ui-select>
  `,
  providers: [provideAsControlValueAccessor(forwardRef(() => DataSourcePickerComponent))]
})
export class DataSourcePickerComponent extends BaseControlValueAccessor<string> implements OnInit {
  constructor(private dataContext: PageDataContext) {
    super();
  }

  dataSource = new TypedFormControl<string>();

  dataSourceOptions = this.dataContext.dataSources.pipe(
    map(dataSources => Object.entries(dataSources)),
    map(dataSourceEntries => dataSourceEntries
      .map(([dataSourceId, dataSource]) => <Option>{text: dataSourceId, value: dataSourceId}))
  );

  ngOnInit() {
    this.dataSource.valueChanges.subscribe(updatedDataSourceId => this.onChanged(updatedDataSourceId));
  }

  updateComponentValue(value: string) {
    this.dataSource.patchValue(value, { emitEvent: false });
  }
}
