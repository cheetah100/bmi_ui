import { Component, OnInit, OnDestroy, forwardRef, Optional } from '@angular/core';

import { Observable, of, Subscription } from 'rxjs';
import { map, publishBehavior, refCount } from 'rxjs/operators';

import { GravityConfigService } from '@bmi/gravity-services';
import { BaseControlValueAccessor, Option, provideAsValidatingCVA } from '@bmi/ui';
import { TypedFormGroup, TypedFormControl, IdpValidators } from '@bmi/ui';

import { FilterDefinition } from '../../filters/filter-definition';
import { OptionlistService } from '../../services/optionlist.service';


interface FilterTypeOption extends Option {
  controlTypes: string[];
  preferredControl: string;
}


@Component({
  selector: 'bmi-core-filter-def-editor',
  templateUrl: './filter-def-editor.component.html',
  styleUrls: ['./filter-def-editor.component.scss'],
  providers: [
    provideAsValidatingCVA(forwardRef(() => FilterDefEditorComponent))
  ]
})
export class FilterDefEditorComponent extends BaseControlValueAccessor<FilterDefinition> implements OnInit, OnDestroy {

  readonly form = new TypedFormGroup<FilterDefinition>({
    id: new TypedFormControl('', [IdpValidators.required, IdpValidators.mustBeNonBlank]),
    name: new TypedFormControl('', [IdpValidators.required, IdpValidators.mustBeNonBlank]),
    filterType: new TypedFormControl<string>(null, [IdpValidators.required, IdpValidators.mustBeNonBlank]),
    controlType: new TypedFormControl<string>(null),
    defaultValue: new TypedFormControl<string>(null),
    config: new TypedFormGroup({
      optionlist: new TypedFormControl<string>(
        null,
        IdpValidators.if(() => this.showOptionlistPicker, IdpValidators.required, IdpValidators.mustBeNonBlank())
      ),
      includeNullOption: new TypedFormControl(false),
      onlyShowInEditor: new TypedFormControl(false),
    })
  });

  optionlistBoards: Observable<Option[]> = this.getBoardOptions();

  allControlTypes: Option[] = [
    {value: null, text: 'Hidden'},
    {value: 'text', text: 'Textbox'},
    {value: 'single-select', text: 'Single-select'}
  ];

  filterTypes: FilterTypeOption[] = [
    {value: 'optionlist', text: 'Optionlist', controlTypes: ['text', 'single-select'], preferredControl: 'single-select'},
    {value: 'text', text: 'Free Text', controlTypes: ['text'], preferredControl: 'text'},
  ];

  controlTypes: Option[] = this.allControlTypes;

  private subscriptions: Subscription[];

  constructor (
    @Optional() private gravityConfigService: GravityConfigService,
    private optionListService: OptionlistService
  ) {
    super();
  }

  get showOptionlistPicker(): boolean {
    return this.form && this.form.value && this.form.value.filterType === 'optionlist';
  }

  ngOnInit() {
    this.subscriptions = [
      this.form.valueChanges.subscribe(value => {
        this.onChanged(value);
      }),

      this.form.controls.filterType.valueChanges.subscribe(filterType => {
        this.updateVisibleControlTypes(filterType);
      })
    ];
  }

  ngOnDestroy() {
    super.cleanup();
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  getBoardOptions(): Observable<Option[]> {
    if (!this.gravityConfigService) {
      return of([]);
    } else {
      return this.optionListService.getAllBoards().pipe(
        map(boards => boards.map(b => <Option>{value: b.id, text: b.name})),
        publishBehavior([]),
        refCount()
      );
    }
  }

  updateVisibleControlTypes(filterType: string) {
    const filterOption = this.filterTypes.find(x => x.value === filterType);
    if (!filterOption) {
      this.controlTypes = this.allControlTypes;
    } else {
      // We'll always include the "null" option for a hidden filter type
      this.controlTypes = this.allControlTypes.filter(ct => !ct.value || filterOption.controlTypes.includes(ct.value));

      // If the user seems to be changing the filter type, we want to ensure the
      // control type is appropriate for the filter type. For now, we'll just
      // set the control to the preferred control for the filter. This behaviour
      // is fine for newly created filters, but could cause problems when
      // editing existing pages -- we should only do this for newly created
      // filters.
      const selectedControlType = this.form.controls.controlType.value;
      const isAppropriateControl = !selectedControlType || filterOption.controlTypes.includes(selectedControlType);
      if (this.form.controls.controlType.untouched || !isAppropriateControl) {
        this.form.patchValue({
          controlType: filterOption.preferredControl
        });
      }
    }
  }

  validateSelf() {
    if (this.form.valid) {
      return null
    } else {
      return {
        'filterDefinition': 'Filter definition is invalid'
      };
    }
  }


  updateComponentValue(filterDef: FilterDefinition) {
    if (!filterDef) {
      this.resetForm(false);
    } else {
      this.form.patchValue(filterDef, {emitEvent: false});
    }
  }

  resetForm(emitEvent: boolean) {
    this.form.reset({
      id: '',
      filterType: 'optionlist',
      controlType: 'single-select',
      name: 'Unnamed Filter',
      defaultValue: null,
      config: {
        optionlist: null
      }
    }, {emitEvent})
  }
}
