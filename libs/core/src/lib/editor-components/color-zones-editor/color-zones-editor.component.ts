import { Component, Input, forwardRef, OnDestroy, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, Validators, } from '@angular/forms';
import { ObjectMap } from '@bmi/utils';
import { Subscription } from 'rxjs';
import { BaseControlValueAccessor, TemplatedFormArray, TypedFormGroup, TypedFormControl } from '@bmi/ui';
import { skip } from 'rxjs/operators';

import { ColorZone } from '../../util/color-zone';

@Component({
  selector: 'bmi-color-zones-editor',
  templateUrl: './color-zones-editor.component.html',
  styleUrls: ['./color-zones-editor.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => ColorZonesEditorComponent),
    multi: true
  }],
})
export class ColorZonesEditorComponent extends BaseControlValueAccessor<ColorZone[]> implements ControlValueAccessor, OnDestroy {

  /**
   * zones: value means 'up to'
   * there is no lower bound.
   * zones have to be ordered correctly in the data.
   * lowest => highest value. With default last.
   * lack of value denotes the zone as default.
   *
   * valueRange: we don't know what the range will be for a series.
   * starting with an arbitrary 0-100. And first value split at 50.
   * We'll move the upper bound (r) if zones are set. So that zones make up most of the width.
   * And we'll extend the lower bound (l) below zero if required by negative or low values.
   */

  @Input() defaultColor = '#049FD9';
  private valueRange: ObjectMap<number>;
  private savedZones: ColorZone[];
  zones: ColorZone[];

  ngOnDestroy() {
    super.ngOnDestroy();
  }

  getFlex(value: number, index: number): number {
    if (value === undefined) {
      return 1;
    }
    const range = this.valueRange.max - this.valueRange.min;
    const previous: ColorZone = this.zones[index - 1];
    const width = value - (!!previous ? previous.value : this.valueRange.min);
    return 6 * width / range + (index ? 0 : 1);
  }

  canSplitZone(i: number): boolean {
    if (i === 0) {
      return true;
    }
    const value: number = this.zones[i].value;
    if (typeof (value) === undefined) {
      return false;
    }
    return (value - this.zones[i - 1].value > 1);
  }

  splitZone(i: number): void {
    const zoneValues = [...this.zones];
    const previous = (i > 0) ? zoneValues[i - 1].value : this.valueRange.min;
    const midpoint = Math.round(((zoneValues[i].value || this.valueRange.max || 100) + previous) / 2);
    const newZone: ColorZone = { value: midpoint, color: zoneValues[i].color };
    this.zones.push(newZone);
    this.sortZones();
    this.doChange();
  }

  deleteZone(i: number): void {
    this.zones.splice(i, 1);
    this.doChange();
  }

  checkChanges(open: boolean): void {
    if (!open) {
      this.sortZones();
      this.doChange();
    }
  }

  updateRange(): void {
    if (!this.zones.length) {
      this.valueRange = { min: 0, max: 100, };
    }
    this.valueRange = Object.values(this.zones)
      .filter((zone: ColorZone) => zone.value || zone.value === 0)
      .reduce(
        (a: ObjectMap<number>, c: ColorZone) => {
          return {
            min: Math.min(a.min, c.value),
            max: Math.max(a.max, c.value),
          };
        }, { min: 0, max: 0 }
      );
  }

  changesExist(base: ColorZone[] = this.savedZones): boolean {
    if (base.length !== this.zones.length) {
      return true;
    }
    return this.zones.some(
      (zone, i) => zone.value !== base[i].value || zone.color !== base[i].color
    );
  }

  sortZones(): void {
    this.zones.sort((a: ColorZone, b: ColorZone) => {
      return (a.value || Infinity) > (b.value || Infinity) ? 1 : -1;
    });
  }

  copyZones(zones: ColorZone[]): ColorZone[] {
    return zones.map(zone => this.copyZone(zone));
  }

  copyZone(zone: ColorZone): ColorZone {
    return { ...zone };
  }

  isDefaultZone(i: number): boolean {
    return i === this.zones.length - 1;
  }

  doChange(): void {
    if (this.changesExist) {
      this.updateRange();
      this.onChanged(this.zones);
    }
  }

  updateComponentValue(zonesValue: ColorZone[]) {
    if (Array.isArray(zonesValue)) {
      this.savedZones = this.copyZones(zonesValue);
      if (!this.zones || this.changesExist(zonesValue)) {
        this.zones = this.copyZones(zonesValue);
        this.updateRange();
      }
    }
  }

}
