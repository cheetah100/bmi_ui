import { Component, Input, forwardRef, OnInit, OnDestroy, Optional } from '@angular/core';

import { Unsubscribable } from 'rxjs';

import {
  TypedFormGroup,
  TypedFormControl,
  Option,
  BaseControlValueAccessor,
  provideAsControlValueAccessor,
} from '@bmi/ui';



import { ParameterBinding } from '../../page-data-context/parameter-binding';
import { distinctUntilNotEqual } from '../../util/distinct-until-not-equal';

@Component({
  selector: 'bmi-advanced-binding-picker',
  templateUrl: './advanced-binding-picker.component.html',
  styleUrls: ['./advanced-binding-picker.component.scss'],
  providers: [
    provideAsControlValueAccessor(forwardRef(() => AdvancedBindingPickerComponent))
  ]
})
export class AdvancedBindingPickerComponent extends BaseControlValueAccessor<ParameterBinding> implements OnInit, OnDestroy {
  readonly form = new TypedFormGroup({
    bindingType: new TypedFormControl<string>(''),
    value: new TypedFormControl<string>('')
  });

  readonly bindingTypes: Option[] = [
    { value: 'data-path', text: 'Data' },
    { value: 'text', text: 'Text' },
    { value: 'filter', text: 'Filter' }
  ];

  private subscriptions: Unsubscribable[] = [];

  ngOnInit() {
    this.resetForm();
    this.subscriptions = [
      this.form.valueChanges.pipe(
        distinctUntilNotEqual(),
      ).subscribe(value => {
        this.onTouched();
        this.onChanged(this.getBinding())
      })
    ];
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  get placeholderText() {
    switch (this.form.value.bindingType) {
      case 'text':
        return 'Enter text here...';
      case 'filter':
        return 'Filter ID';
      case 'data-path':
        return 'Page data path (e.g. service.owner)';
      default:
        return '';
    }
  }

  private updateForm(bindingType: string, value: string) {
    this.form.patchValue({ bindingType, value }, { emitEvent: false });
  }

  private resetForm() {
    this.updateForm('data-path', '');
  }

  updateComponentValue(binding: ParameterBinding) {
    if (!binding) {
      this.resetForm();
    } else if (typeof binding === 'string') {
      this.updateForm('text', binding);
    } else if (binding.bindingType === 'filter') {
      this.updateForm('filter', binding.filterId);
    } else if (binding.bindingType === 'data-path') {
      this.updateForm('data-path', binding.path);
    } else if (binding.bindingType === 'this-path') {
      // We want to deprecate this style of binding in favour of using a dynamic
      // "this" data source for consistency.
      this.updateForm('data-path', 'this.' + binding.path);
    }
  }

  private getBinding(): ParameterBinding {
    const { bindingType, value } = this.form.value;
    if (!value) {
      return null;
    }

    switch (bindingType) {
      case 'text':
        return value;
      case 'filter':
        return { bindingType: 'filter', filterId: value };
      case 'data-path':
        return { bindingType: 'data-path', path: value };
      case 'this-path':
        return { bindingType: 'data-path', path: `this.${value}`};
      default:
        return null;
    }
  }

}
