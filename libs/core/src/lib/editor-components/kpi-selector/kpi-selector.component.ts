import { Component, forwardRef, OnInit, OnDestroy } from '@angular/core';
import { Subscription, Observable, combineLatest } from 'rxjs';

import { BaseControlValueAccessor, TypedFormControl, OptionGroup, provideAsControlValueAccessor } from '@bmi/ui';

import { OptionlistService } from '../../services/optionlist.service';


@Component({
  selector: 'bmi-kpi-selector',
  template: `
      <ui-single-select
        [optionGroups]="kpiOptionGroups"
        preferenceId="kpiSelector"
        [formControl]="formControl">
      </ui-single-select>
  `,
  providers: [provideAsControlValueAccessor(forwardRef(() => KpiSelectorComponent))],
})
export class KpiSelectorComponent extends BaseControlValueAccessor<string> implements OnInit, OnDestroy {

  private subscription = new Subscription();
  kpiOptionGroups: OptionGroup[] = [];

  readonly formControl = new TypedFormControl<string>(null);

  constructor(
    private optionlistService: OptionlistService
  ) {
    super();
  }

  ngOnInit() {
    this.subscription.add(this.optionlistService.getOptionGroups('kpi').subscribe(groups => this.kpiOptionGroups = groups));
    this.formControl.valueChanges.subscribe(value => this.onChanged(value));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  updateComponentValue(value: string) {
    this.formControl.patchValue(value, { emitEvent: false });
  }

}
