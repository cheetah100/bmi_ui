import { Component, Input, OnInit, forwardRef } from '@angular/core';

import { map } from 'rxjs/operators';

import {
  TypedFormControl, TypedFormGroup, TemplatedFormArray,
  provideAsControlValueAccessor, BaseControlValueAccessor
} from '@bmi/ui';
import { PathResolver } from '@bmi/gravity-services';
import { ObjectMap } from '@bmi/utils';

import { distinctUntilNotEqual } from '../../util/distinct-until-not-equal';
import { ParameterBinding } from '../../page-data-context/parameter-binding';


@Component({
  selector: 'bmi-binding-list',
  templateUrl: './binding-list.component.html',
  styleUrls: ['./binding-list.component.scss'],
  providers: [
    provideAsControlValueAccessor(forwardRef(() => BindingListComponent))
  ]
})
export class BindingListComponent extends BaseControlValueAccessor<ParameterBinding[]> implements OnInit {
  @Input() resolver: PathResolver;
  @Input() addButtonText = 'Add Binding';

  readonly form = new TemplatedFormArray(
    () => new TypedFormControl<ParameterBinding>(null)
  );

  ngOnInit() {
    this.form.valueChanges.pipe(
      distinctUntilNotEqual()
    ).subscribe(results => {
      this.onChanged(results);
    });
  }

  updateComponentValue(value: ParameterBinding[]) {
    if (!value) {
      this.form.reset([], {emitEvent: false});
    } else {
      this.form.patchValue(value, {emitEvent: false});
    }
  }

  addParameter() {
    this.form.pushValue(null);
  }
}
