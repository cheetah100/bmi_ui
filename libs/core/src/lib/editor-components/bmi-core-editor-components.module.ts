import { NgModule, Provider } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { PortalModule } from '@angular/cdk/portal';

import { GravityUiModule } from '@bmi/gravity-services';
import { SharedUiModule } from '@bmi/ui';

import { AdvancedBindingPickerComponent } from './advanced-binding-picker/advanced-binding-picker.component';
import { BindingPickerComponent } from './binding-picker/binding-picker.component';
import { DataMapEditorComponent } from './data-map-editor/data-map-editor.component';
import { FilterDefEditorComponent } from './filter-def-editor/filter-def-editor.component';
import { BmiRouteEditorComponent } from './bmi-route-editor/bmi-route-editor.component';
import { RouteSelectorComponent } from './route-selector/route-selector.component';
import { DataSeriesDefEditorComponent } from './data-series-def-editor/data-series-def-editor.component';
import { DataSourcePickerComponent } from './data-source-picker/data-source-picker.component';
import { KpiSelectorComponent } from './kpi-selector/kpi-selector.component';
import { BindingListComponent } from './binding-list/binding-list-component';
import { CardEditorConfigComponent } from './card-editor-config/card-editor-config.component';
import { PhaseEditorConfigComponent } from '../phase-editor/phase-editor-config.component';

import { FormatterPickerComponent } from './formatter-picker/formatter-picker.component';
import { ColorZonesEditorComponent } from './color-zones-editor/color-zones-editor.component';

import { ButtonToolbarEditorComponent } from './button-toolbar-editor/button-toolbar-editor.component';
import { ButtonToolbarItemEditorComponent } from './button-toolbar-editor/button-toolbar-item-editor.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedUiModule,
    GravityUiModule,
    RouterModule,
    PortalModule,
  ],

  declarations: [
    AdvancedBindingPickerComponent,
    BindingPickerComponent,
    DataMapEditorComponent,
    FilterDefEditorComponent,
    BmiRouteEditorComponent,
    RouteSelectorComponent,
    DataSeriesDefEditorComponent,
    DataSourcePickerComponent,
    KpiSelectorComponent,
    FormatterPickerComponent,
    BindingListComponent,
    ColorZonesEditorComponent,
    CardEditorConfigComponent,
    PhaseEditorConfigComponent,
    ButtonToolbarEditorComponent,
    ButtonToolbarItemEditorComponent,
  ],

  exports: [
    BindingPickerComponent,
    DataMapEditorComponent,
    FilterDefEditorComponent,
    BmiRouteEditorComponent,
    RouteSelectorComponent,
    DataSeriesDefEditorComponent,
    DataSourcePickerComponent,
    KpiSelectorComponent,
    FormatterPickerComponent,
    BindingListComponent,
    ColorZonesEditorComponent,
    CardEditorConfigComponent,
    PhaseEditorConfigComponent,
    ButtonToolbarEditorComponent,
  ]
})
export class BmiCoreEditorComponentsModule { }
