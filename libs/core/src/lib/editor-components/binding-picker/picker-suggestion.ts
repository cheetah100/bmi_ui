import { PathResolver, FieldPath } from '@bmi/gravity-services';
import lodashIsEqual from 'lodash-es/isEqual';
import { ParameterBinding, isPathBinding, isFilterBinding } from '../../page-data-context/parameter-binding';
import { FilterService } from '../../filters/filter.service';


export interface PickerSuggestion {
  type: 'filter' | 'data' | 'field';
  text: string;
  binding: ParameterBinding;
  canExpand?: boolean;
}


export interface SuggestionGroup {
  title: string;
  suggestions: PickerSuggestion[];
}


const GROUP_TYPES = [
  {type: 'this', title: 'Current Row'},
  {type: 'data', title: 'Data Sources'},
  {type: 'filter', title: 'Filters'},
  {type: 'field', title: 'Fields'}
];


export function groupSuggestionsByType(suggestions: PickerSuggestion[]): SuggestionGroup[] {
  const knownGroupTypes = GROUP_TYPES.map(t => t.type);

  return [
    ...GROUP_TYPES.map(({type, title}) => <SuggestionGroup>{
      title: title,
      suggestions: suggestions.filter(s => s.type === type)
    }),

    {
      title: 'Unknown',
      suggestions: suggestions.filter(s => !knownGroupTypes.includes(s.type))
    }
  ].filter(g => g.suggestions.length > 0)
}


export function noGrouping(suggestions: PickerSuggestion[]): SuggestionGroup[] {
  return [
    {
      title: null,
      suggestions: suggestions || []
    }
  ];
}


export interface SuggestionSource {
  getRoots(): PickerSuggestion[];
  getSiblings(binding: ParameterBinding): PickerSuggestion[];
  getChildren(binding: ParameterBinding): PickerSuggestion[];
  getBreadcrumbs(binding: ParameterBinding): PickerSuggestion[];
  isPickableBinding(binding: ParameterBinding): boolean;
}


export class BindingSuggestionSource implements SuggestionSource {
  constructor (
    private resolver: PathResolver,
    private filterService: FilterService
  ) {}

  getRoots(): PickerSuggestion[] {
    return [
      ...this.getFilterSuggestions(),
      ...this.getPathSuggestions(null, 'child')
    ];
  }

  getSiblings(binding: ParameterBinding): PickerSuggestion[] {
    if (isPathBinding(binding)) {
      const fp = this.getFieldPath(binding.path);
      if (fp && fp.parent)  {
        return this.getPathSuggestions(binding.path, 'sibling');
      }
    }

    return this.getRoots();
  }

  getChildren(binding: ParameterBinding): PickerSuggestion[] {
    return isPathBinding(binding) ? this.getPathSuggestions(binding.path, 'child') : [];
  }

  getBreadcrumbs(binding: ParameterBinding): PickerSuggestion[] {
    if (isFilterBinding(binding)) {
      return this.getFilterSuggestions().filter(r => lodashIsEqual(r.binding, binding));
    } else if (isPathBinding(binding)) {
      const fp = this.resolver.resolve(binding.path);
      if (!fp) {
        return null;
      } else {
        return fp.segments.map(p => this.makePathSuggestion(p));
      }
    } else {
      return null;
    }
  }

  isPickableBinding(binding: ParameterBinding): boolean {
    if (isFilterBinding(binding) && this.filterService && this.filterService.currentFilters.find(f => f.id === binding.filterId)) {
      return true;
    } else if (isPathBinding(binding) && this.resolver) {
      const fp = this.getFieldPath(binding.path);
      return !fp || fp.isValid;
    } else {
      // Any other types of bindings are deemed unpickable, meaning we should
      // display the advanced picker.
      return false;
    }
  }

  private getFieldPath(path: string): FieldPath {
    return this.resolver ? this.resolver.resolve(path) : null;
  }

  private getPathSuggestions(path: string, mode: 'child' | 'sibling'): PickerSuggestion[] {
    if (this.resolver) {
      const fp = this.resolver.resolve(path);
      const children = this.resolver.getChildPaths(mode === 'child' ? fp : fp && fp.parent);
      return children.map(fieldPath => this.makePathSuggestion(fieldPath));
    } else {
      return [];
    }
  }

  private makePathSuggestion(fieldPath: FieldPath) {
    const isDataSource = !fieldPath.parent;
    const suggestionType = isDataSource ? 'data' : 'field';
    return <PickerSuggestion>{
      type: (isDataSource && fieldPath.fieldName === 'this') ? 'this' : suggestionType,
      text: fieldPath.fieldName,
      binding: {bindingType: 'data-path', path: fieldPath.path},
      canExpand: isDataSource || !!fieldPath.targetBoard
    };
  }

  private getFilterSuggestions() {
    if (this.filterService) {
      return this.filterService.currentFilters.map(f => <PickerSuggestion>{
        type: 'filter',
        text: f.name || f.id,
        binding: {bindingType: 'filter', filterId: f.id},
        canExpand: false
      });
    } else {
      return [];
    }
  }
}


