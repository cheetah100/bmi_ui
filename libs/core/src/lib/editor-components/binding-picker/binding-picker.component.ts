import { Component, Input, Optional, forwardRef, SimpleChanges, OnInit, OnChanges, OnDestroy } from '@angular/core';

import { defer, Subscription, BehaviorSubject, of, combineLatest } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { PathResolver, FieldPath } from '@bmi/gravity-services';
import { BaseControlValueAccessor, provideAsControlValueAccessor } from '@bmi/ui';

import { ParameterBinding, migrateBinding } from '../../page-data-context/parameter-binding';
import { PathResolverContext } from '../../page-data-context/path-resolver-context';
import { FilterService } from '../../filters/filter.service';

import { PickerSuggestion, SuggestionGroup, groupSuggestionsByType, SuggestionSource, BindingSuggestionSource } from './picker-suggestion';

import lodashIsEqual from 'lodash-es/isEqual';

@Component({
  selector: 'bmi-binding-picker-dropdown',
  templateUrl: './binding-picker.component.html',
  styleUrls: ['./binding-picker.component.scss'],
  providers: [provideAsControlValueAccessor(forwardRef(() => BindingPickerComponent))]
})
export class BindingPickerComponent extends BaseControlValueAccessor<ParameterBinding> implements OnInit, OnChanges {
  // tslint:disable-next-line:no-input-rename
  @Input('resolver') userDefinedResolver: PathResolver = null;

  currentBinding: ParameterBinding = null;

  suggestionGroups: SuggestionGroup[] = [];
  selectedSuggestion: PickerSuggestion = null;
  breadcrumbs: PickerSuggestion[] = null;

  suggestionSource: SuggestionSource = new BindingSuggestionSource(null, this.filterService);

  // tslint:disable-next-line:max-union-size
  pillType: 'none' | 'text' | 'filter' | 'data' = 'none';
  mode: 'picker' | 'advanced' = 'picker';
  canUsePickerMode = true;

  displayValue: string = null;
  isOpen = false;

  isLookingAtChildPaths = false;
  private subscription = new Subscription();
  private userDefinedResolverSubject = new BehaviorSubject<PathResolver>(undefined);

  pathResolver = combineLatest([
    this.userDefinedResolverSubject,
    this.pathResolverContext ? this.pathResolverContext.getPathResolver() : of(<PathResolver>undefined)
  ]).pipe(
    map(([userResolver, contextResolver]) => userResolver || contextResolver),
    distinctUntilChanged()
  );

  resolver: PathResolver = null;

  constructor(
    @Optional() private pathResolverContext: PathResolverContext,
    @Optional() private filterService: FilterService
  ) {
    super();
  }

  ngOnInit() {
    this.initializeSuggestionSource(null);
    this.setBinding(null);
    this.updateSuggestions();

    this.subscription.add(this.pathResolver.subscribe(resolver => {
      this.resolver = resolver;
      this.initializeSuggestionSource(resolver);
    }));
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.userDefinedResolver) {
      this.userDefinedResolverSubject.next(this.userDefinedResolver);
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private initializeSuggestionSource(resolver: PathResolver) {
    this.suggestionSource = new BindingSuggestionSource(
      resolver,
      this.filterService
    );
  }

  updateComponentValue(binding: ParameterBinding) {
    const migratedBinding = migrateBinding(binding);

    // If the path changes while the user is picking child paths, then we want
    // to drop out of this mode.
    if (!lodashIsEqual(migratedBinding, this.currentBinding)) {
      this.isLookingAtChildPaths = false;
    }

    this.setBinding(migratedBinding);
    this.updateSuggestions();
  }

  chooseBinding(binding: ParameterBinding, options = {keepOpen: false}) {
    this.setBinding(binding);
    this.onChanged(binding);

    // The user has chosen this option. If it has any children, we will change
    // mode to look at child options of this path.
    const childOptions = this.suggestionSource.getChildren(binding);
    if (childOptions.length > 0) {
      this.isLookingAtChildPaths = true;
      this.updateSuggestions();
    } else if (!options.keepOpen) {
      this.finishPicking();
    } else {
      this.updateSuggestions();
    }
  }

  crumbClicked(suggestion: PickerSuggestion) {
    this.chooseBinding(suggestion.binding, {keepOpen: true})
  }

  startPicking() {
    this.onTouched();
    this.setBestPickerMode();
    this.isLookingAtChildPaths = false;
    this.updateSuggestions();
  }

  finishPicking() {
    this.onTouched();
    this.suggestionGroups = [];
    this.isOpen = false;
    this.isLookingAtChildPaths = false;
  }

  setPickerMode(mode: 'picker' | 'advanced') {
    this.mode = mode;
    this.updateSuggestions();
  }

  clearSelection() {
    this.setBinding(null);
    this.onChanged(null);
    this.isLookingAtChildPaths = false;
    this.updateSuggestions();
  }

  onDropdownStateChanged(isOpen: boolean) {
    if (isOpen) {
      this.startPicking();
    } else {
      this.finishPicking();
    }
  }

  updateSuggestions() {
    const suggestions = this.getSuggestions(this.currentBinding);
    this.suggestionGroups = groupSuggestionsByType(suggestions);
    this.selectedSuggestion = suggestions.find(s => lodashIsEqual(s.binding, this.currentBinding));
    this.breadcrumbs = this.suggestionSource.getBreadcrumbs(this.currentBinding) || [];
    if (this.breadcrumbs.length === 0) {
      this.breadcrumbs = null;
    }
  }

  private setBestPickerMode() {
    this.mode = this.canUsePickerMode ? 'picker' : 'advanced';
  }

  private setBinding(binding: ParameterBinding) {
    this.currentBinding = binding;
    this.canUsePickerMode = !this.currentBinding || this.suggestionSource.isPickableBinding(this.currentBinding);

    if (this.mode === 'picker') {
      this.setBestPickerMode();
    }

    if (!binding) {
      this.pillType = 'none';
      this.displayValue = 'No binding';
    } else if (typeof binding === 'string') {
      this.pillType = 'text';
      this.displayValue = binding;
    } else if (binding.bindingType === 'filter') {
      this.pillType = 'filter';
      this.displayValue = binding.filterId;
    } else if (binding.bindingType === 'data-path') {
      this.displayValue = binding.path;
      this.pillType = 'data';
    }
  }

  getSuggestions(binding: ParameterBinding): PickerSuggestion[] {
    if (!binding) {
      return this.suggestionSource.getRoots();
    } else if (this.isLookingAtChildPaths) {
      return this.suggestionSource.getChildren(binding);
    } else {
      return this.suggestionSource.getSiblings(binding);
    }
  }
}
