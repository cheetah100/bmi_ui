import {
  TimelineDataPoint,
  TimelineLayout,
  TimelineMarker,
  TimelineType,
  currentTimeAsPercentage,
  datapointIsCurrent,
  makeGrid,
  makeTimeline,
} from './timeline-display';

describe('timeline-display', () => {

  const datapoint1: TimelineDataPoint = {
    name: 'dp',
    start: 300,
    end: 400,
  };
  const datapoint2: TimelineDataPoint = {
    name: 'dp',
    start: 1e20,
    end: 1e20,
  };
  const datapoint3: TimelineDataPoint = {
    name: 'dp',
    start: 1,
    end: 1e20,
  };

  describe('currentTimeAsPercentage()', () => {
    it('should constrain the result to percentage range', () => {
      expect(currentTimeAsPercentage(300, 50, 250)).toBe(100);
      expect(currentTimeAsPercentage(300, 1e20, 2e20)).toBe(0);
    });
    it('should give the percentage within range', () => {
      expect(currentTimeAsPercentage(200, 100, 500)).toBe(25);
    });
  });

  describe('datapointIsCurrent()', () => {
    it('should return false if not current', () => {
      expect(datapointIsCurrent(500, datapoint1)).toBe(false);
      expect(datapointIsCurrent(500, datapoint2)).toBe(false);
    });
    it('should return true if current', () => {
      expect(datapointIsCurrent(1e10, datapoint3)).toBe(true);
    });
  });

  describe('makeGrid()', () => {
    it('should place markers within the timeline range correctly', () => {
      expect(makeGrid([{ at: 100, style: 'marker', color: 'gray' }], 0, 200)).toEqual([50]);
    });
    it('should ignore markers outside the range', () => {
      expect(makeGrid([
        { at: 100, style: 'marker', color: 'gray' },
        { at: -50, style: 'marker', color: 'gray' },
        { at: 300, style: 'marker', color: 'gray' },
      ], 0, 200)).toEqual([50]);
    });
    it('should return an empty array for no markers', () => {
      expect(makeGrid([], 0, 200)).toEqual([]);
    });
  });

  describe('makeTimeline()', () => {
    it('should include only datapoints within range', () => {
      expect(makeTimeline([datapoint1, datapoint2], 0, 1000)).toEqual([
        { name: 'dp', left: 30, width: 10, data: datapoint1 }
      ]);
    });
    it('if a datapoint crosses the range border, it should be trimmed to fit', () => {
      expect(makeTimeline([datapoint1], 150, 350)).toEqual([
        { name: 'dp', left: 75, width: 25, data: datapoint1 }
      ]);
    });
    it('should return an empty array for no given datapoints', () => {
      expect(makeTimeline([], 100, 200)).toEqual([]);
    });
  });

});

