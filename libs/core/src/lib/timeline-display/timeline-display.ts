import { mapRange, sortBy } from '@bmi/utils';

interface TimelineDataPoint {
  name: string;
  start: number;
  end: number;
}


interface TimelineMarker {
  at: number;

  // TODO: actually implement these! For now this just considers the 'at' value
  // for the position.
  style: 'grid-line' | 'marker';
  aboveItems?: boolean;
  color: 'gray' | 'red';
}


interface TimelineLayout {
  name: string;
  left: number;
  width: number;
  data: TimelineDataPoint;
}

type TimelineType = 'roadmap-cell' | 'roadmap-header';

const mapToPercent = (ms: number, start: number, end: number): number => {
  return mapRange(ms, [start, end], [0, 100]);
};

const makeGrid = (markers: TimelineMarker[], start: number, end: number): number[] => {
  return markers
    .filter((item: TimelineMarker) => (!!item.at && item.at >= start && item.at <= end))
    .map((item: TimelineMarker) => mapToPercent(item.at, start, end));
};

const makeTimeline = (
  datapoints: TimelineDataPoint[],
  start: number,
  end: number,
): TimelineLayout[] => {
  if (datapoints.length) {
    const filteredPoints = datapoints.filter((dp: TimelineDataPoint) => {
      return (
        !!dp.start
        && !!dp.end
        && dp.end > dp.start
      );
    });
    return sortBy(filteredPoints, 'start')
      .map(dp => {
        const leftMS: number = Math.max(start, dp.start);
        const rightMS: number = Math.min(end, dp.end);
        const left = mapToPercent(leftMS, start, end);
        return {
          name: dp.name,
          left: left,
          width: mapToPercent(rightMS, start, end) - left,
          data: dp,
        };
      });
  } else {
    return [];
  }
};

const currentTimeAsPercentage = (currentMillis: number, start: number, end: number): number => {
  return mapToPercent(Math.min(end, Math.max(start, currentMillis)), start, end);
};

const datapointIsCurrent = (nowMS: number, datapoint: TimelineDataPoint): boolean => {
  return (datapoint.start <= nowMS && nowMS < datapoint.end);
};

export {
  TimelineDataPoint,
  TimelineLayout,
  TimelineMarker,
  TimelineType,
  currentTimeAsPercentage,
  datapointIsCurrent,
  makeGrid,
  makeTimeline,
};
