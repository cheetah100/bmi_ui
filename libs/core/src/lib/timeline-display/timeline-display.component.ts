import { Component, ChangeDetectionStrategy, Input, OnChanges, OnInit } from '@angular/core';
import {
  TimelineDataPoint,
  TimelineLayout,
  TimelineMarker,
  TimelineType,
  currentTimeAsPercentage,
  datapointIsCurrent,
  makeGrid,
  makeTimeline,
} from './timeline-display';

@Component({
  selector: 'bmi-core-timeline-display',
  templateUrl: './timeline-display.component.html',
  styleUrls: ['./timeline-display.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TimelineDisplayComponent implements OnChanges, OnInit {

  @Input() dataPoints: TimelineDataPoint[] = [];
  @Input() markers: TimelineMarker[] = [];
  @Input() timelineStart: number = 0;
  @Input() timelineEnd: number = 0;
  @Input() type: TimelineType = 'roadmap-cell';

  timelineLayout: TimelineLayout[];
  gridLayout: number[];

  ngOnInit() {
    this.update();
  }

  ngOnChanges() {
    this.update();
  }

  update() {
    this.gridLayout = makeGrid(this.markers, this.timelineStart, this.timelineEnd);
    this.timelineLayout = makeTimeline(this.dataPoints, this.timelineStart, this.timelineEnd);
  }

  currentPercent(): number {
    return currentTimeAsPercentage(new Date().getTime(), this.timelineStart, this.timelineEnd);
  }

  itemIsCurrent(item: TimelineLayout): boolean {
    return datapointIsCurrent(new Date().getTime(), item.data);
  }

}
