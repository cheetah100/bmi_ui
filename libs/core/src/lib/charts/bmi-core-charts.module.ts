import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { GravityUiModule } from '@bmi/gravity-services';
import { SharedUiModule } from '@bmi/ui';
import { BmiCoreComponentsModule } from '../components/bmi-core-components.module';

import { HighchartsViewerComponent } from './highcharts-viewer/highcharts-viewer.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SharedUiModule,
    BmiCoreComponentsModule,
  ],

  declarations: [
    HighchartsViewerComponent
  ],

  exports: [
    HighchartsViewerComponent
  ]
})
export class BmiCoreChartsModule {}
