import { Injectable } from '@angular/core';
import { CardCacheService } from '@bmi/gravity-services';
import { Options } from 'highcharts';
import { isNil } from 'lodash-es';
import { Observable, of } from 'rxjs';
import { addAllSeries, addBothAxes, builderStarter } from '../basic-chart-builder';
import { BasicChartBuilderOptions } from '../basic-chart-config';


@Injectable({providedIn: 'root'})
export class ChartBuildService {

  constructor(
    private cardCacheService: CardCacheService
  ) {}

  chartOptions(options: BasicChartBuilderOptions): Observable<Options> {
    const {chartSettings} = options;

    const builder = builderStarter(chartSettings);

    if (chartSettings.chartHeight) {
      builder.set({
        chart: {
          height: chartSettings.chartHeight || null
        }
      });
    }

    if (chartSettings.hideLegend) {
      builder.set({ legend: { enabled: false }});
    }

    addAllSeries(builder, options);

    if (chartSettings.yAxisRange) {
      builder.yAxis(
        axis => axis.set({
          softMin: chartSettings.yAxisRange.softMin,
          softMax: chartSettings.yAxisRange.softMax,
          min: chartSettings.yAxisRange.min,
          max: chartSettings.yAxisRange.max,
          endOnTick: !isNil(chartSettings.yAxisRange.endOnTick) ? chartSettings.yAxisRange.endOnTick : true,
        })
      );
    }

    if (chartSettings.yAxisLabels) {
      builder.yAxis(
        axis => axis.set({
          labels: {
            format: chartSettings.yAxisLabels.formatString || undefined,
            formatter: chartSettings.yAxisLabels.formatFunction ?
                function() { return chartSettings.yAxisLabels.formatFunction(this.value); } :
                undefined
          }
        })
      );
    }

    // TODO: refactor this. copy pasting is gross.
    if (chartSettings.xAxisLabels) {
      builder.xAxis(
        axis => axis.set({
        labels: {
          format: chartSettings.xAxisLabels.formatString || undefined,
          formatter: chartSettings.xAxisLabels.formatFunction ?
              function() { return chartSettings.xAxisLabels.formatFunction(this.value); } :
              undefined
        }
      }));
    }

    addBothAxes(builder, chartSettings);
    builder.xAxis(
      x => x.title(!chartSettings.hideAxisTitles ? chartSettings.xAxisTitle : null)
    )
    .yAxis(
      y => y.title(!chartSettings.hideAxisTitles ? chartSettings.yAxisTitle : null)
    );
    return of(builder.options);

  }

}
