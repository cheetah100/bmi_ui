import { Component, Input, OnInit, OnDestroy, OnChanges, SimpleChanges, ViewChild, ElementRef } from '@angular/core';

import * as Highcharts from 'highcharts';


@Component({
  selector: 'bmi-highcharts-viewer',
  template: `<div #chartContainer class="chart-container" data-cy="chart"></div>`
})
export class HighchartsViewerComponent implements OnDestroy, OnChanges {
  @Input() config: Highcharts.Options;

  @ViewChild('chartContainer', { static: true }) chartContainer: ElementRef;

  private chartInstance: Highcharts.Chart;

  destroyChart() {
    if (this.chartInstance) {
      try {
        this.chartInstance.destroy();
      } catch (e) {
        console.error({ highchartsDestroyError: e });
      }
      this.chartInstance = null;
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.config) {
      this.destroyChart();
      this.initHighcharts();
    }
  }

  ngOnDestroy() {
    this.destroyChart();
  }

  initHighcharts() {
    if (this.config) {
      this.chartInstance = Highcharts.chart(
        this.chartContainer.nativeElement as HTMLElement,
        this.config
      );
    }
  }

  onChartCreated(chart: Highcharts.Chart) {
    // Highcharts will automatically clean up an existing chart instance in the
    // container, if the chart was reinitialized; we just need to track the
    // chart reference so that we can destroy it when the component is
    // destroyed.
    this.chartInstance = chart;
  }
}
