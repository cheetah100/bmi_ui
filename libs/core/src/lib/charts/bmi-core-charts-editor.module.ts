import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { GravityUiModule } from '@bmi/gravity-services';
import { SharedUiModule } from '@bmi/ui';
import { BmiCoreComponentsModule } from '../components/bmi-core-components.module';
import { BmiCoreEditorComponentsModule } from '../editor-components/bmi-core-editor-components.module';

import { ChartSeriesSettingsEditorComponent } from './chart-series-settings-editor/chart-series-settings-editor.component';
import { ChartSeriesGroupEditorComponent } from './chart-series-group-editor/chart-series-group-editor.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SharedUiModule,
    GravityUiModule,
    BmiCoreComponentsModule,
    BmiCoreEditorComponentsModule,
    FormsModule,
    ReactiveFormsModule,
  ],

  declarations: [
    ChartSeriesSettingsEditorComponent,
    ChartSeriesGroupEditorComponent
  ],

  exports: [
    ChartSeriesSettingsEditorComponent,
    ChartSeriesGroupEditorComponent
  ]
})
export class BmiCoreChartsEditorModule {}
