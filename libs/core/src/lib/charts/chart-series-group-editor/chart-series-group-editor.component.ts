import { Component, OnInit, forwardRef } from '@angular/core';
import { TypedFormGroup, TypedFormControl, TemplatedFormArray, BaseControlValueAccessor, provideAsControlValueAccessor } from '@bmi/ui';
import { BasicChartSeriesConfig, SeriesGroupConfig } from '../basic-chart-config';
import { CHART_SERIES_TYPE_OPTIONS } from '../basic-chart-contstants';


@Component({
  selector: 'bmi-chart-series-group-editor',
  templateUrl: './chart-series-group-editor.component.html',
  styleUrls: ['./chart-series-group-editor.component.scss'],
  providers: [provideAsControlValueAccessor(forwardRef(() => ChartSeriesGroupEditorComponent))]
})
export class ChartSeriesGroupEditorComponent extends BaseControlValueAccessor<SeriesGroupConfig> implements OnInit {
  readonly form = new TypedFormGroup<SeriesGroupConfig>({
    dataSeries: new TypedFormControl(null),
    selectedOnly: new TypedFormControl(false),
    seriesSettings: new TemplatedFormArray<BasicChartSeriesConfig>(() => new TypedFormControl<BasicChartSeriesConfig>(null)),
    type: new TypedFormControl('column'),
  });

  readonly typeOptions = CHART_SERIES_TYPE_OPTIONS;

  ngOnInit() {
    this.form.valueChanges.subscribe(value => this.onChanged(value));
  }

  get seriesSettings() {
    return this.form.controls.seriesSettings as TemplatedFormArray<BasicChartSeriesConfig>;
  }

  updateComponentValue(config: SeriesGroupConfig) {
    this.form.patchValue(config, {emitEvent: false});
  }

  addSeries(seriesId: string = ''): void {
    this.seriesSettings.pushValue(this.createSeriesWithDefaults(seriesId));
  }

  createSeriesWithDefaults(id: string, seriesType: string = null): BasicChartSeriesConfig {
    return {
      value: id,
      label: id,
      type: seriesType,
      color: null,
      zones: null,
      zoneAxis: null,
      visible: true,
      connectNulls: false
    };
  }
}
