import { Component, OnInit, forwardRef } from '@angular/core';
import { TypedFormGroup, BaseControlValueAccessor, provideAsControlValueAccessor } from '@bmi/ui';
import { FormControl } from '@angular/forms';
import { distinctUntilNotEqual } from '../../util/distinct-until-not-equal';
import { BasicChartSeriesConfig } from '../basic-chart-config';
import { CHART_SERIES_TYPE_OPTIONS } from '../basic-chart-contstants';

@Component({
  selector: 'bmi-chart-series-settings-editor',
  templateUrl: './chart-series-settings-editor.component.html',
  styles: [`
    :host {
      display: block;
    }

    .series-settings__layout {
      display: grid;
      grid-template-columns: 1fr 1fr;
      gap: var(--gra-spacing-qtr) var(--gra-spacing-half);
    }
  `],
  providers: [provideAsControlValueAccessor(forwardRef(() => ChartSeriesSettingsEditorComponent))]
})
export class ChartSeriesSettingsEditorComponent extends BaseControlValueAccessor<BasicChartSeriesConfig> implements OnInit {
  readonly form = new TypedFormGroup<BasicChartSeriesConfig>({
    value: new FormControl(),
    label: new FormControl(),
    color: new FormControl(),
    type: new FormControl(null),
    connectNulls: new FormControl(false),
    visible: new FormControl(true),
    zoneAxis: new FormControl('y'),
    zones: new FormControl(null)
  });

  readonly typeOptions = [
    {value: null, text: 'Unset (use default)'},
    ...CHART_SERIES_TYPE_OPTIONS
  ];

  ngOnInit() {
    this.form.valueChanges.pipe(
      distinctUntilNotEqual()
    ).subscribe(settings => this.onChanged(settings));
  }

  updateComponentValue(seriesSettings: BasicChartSeriesConfig) {
    this.form.patchValue(seriesSettings, {emitEvent: false});
  }

  toggleZones(): void {
    // Just a heads up: this isn't used right now
    if (this.form.value.zones) {
      this.form.controls.zones.clearValidators();
      this.form.controls.zones.reset();
    } else {
      this.form.patchValue({
        zones: [],
        zoneAxis: this.form.value.zoneAxis || 'y'
      });
    }
  }
}
