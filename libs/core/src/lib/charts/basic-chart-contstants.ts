import { capitalize } from 'lodash-es';
import { BasicChartSeriesConfig } from './basic-chart-config';
import { Option } from '@bmi/utils';

export const GROUP_OPTIONS: Option[] = [
  { text: 'Categories', value: 'category' },
  { text: 'Standard dates', value: 'datetime' }
];

export const VALUE_OPTIONS: Option[] = [
  { text: 'Currency', value: 'currency'},
  { text: 'Count', value: 'count' },
  { text: 'Percent', value: 'percent' }
];

/**
 * Later, we might allow x and y axes to use any of these types.
 * Will need some changes to series data rendering
 */
export const AXIS_OPTIONS: Option[] = [
  ...GROUP_OPTIONS,
  ...VALUE_OPTIONS
];

export const CHART_SERIES_TYPE_OPTIONS: Option[] = [
  'area',
  'areaspline',
  'column',
  'line',
  'scatter',
  'spline',
].map(type => (<Option>{ value: type, text: capitalize(type) }));

export const CHART_STACKING_OPTIONS: Option[] = [
  { text: 'Do not stack', value: undefined, },
  { text: 'Normal stacking', value: 'normal', },
  { text: 'Percentage stacking', value: 'percent', },
];

export const DEFAULT_CHART_CONFIG = {
  credits: { enabled: false },
  chart: { animation: false },
  exporting: { enabled: false },
  plotOptions: {
    series: {
      animation: false,
      states: {
        select: {
          enabled: false
        },
        inactive: {
          opacity: 0.8
        }
      }
    }
  }
};

export const DEFAULT_SERIES_CONFIG: BasicChartSeriesConfig = {
  color: undefined,
  label: undefined,
  value: undefined,
  connectNulls: false,
  type: undefined,
  zoneAxis: 'y',
  zones: undefined,
  visible: true,
};
