import * as Highcharts from 'highcharts';
import lodashMerge from 'lodash-es/merge';
import { ColorZone } from '../util/color-zone';


type BuilderFunction<T> = (builder: T) => void;

interface HighchartsSeriesOptions extends Highcharts.SeriesOptions {
  color?: string;
  data?: any[];
  zones?: ColorZone[];
  connectNulls?: boolean;
  visible?: boolean;
  zoneAxis?: 'x' | 'y';
}


abstract class BaseBuilder<T extends {}> {
  public abstract get options(): T;

  public set(options: Partial<T>, mergeOptions = true) {
    if (mergeOptions) {
      lodashMerge(this.options, options);
    } else {
      Object.assign(this.options, options);
    }
    return this;
  }
}


class AxisBuilder extends BaseBuilder<Highcharts.AxisOptions> {
  constructor( public options: Highcharts.AxisOptions) {
    super();
  }

  title(title: string) {
    return this.set({
      title: {
        text: title
      }
    });
  }

}


class SeriesBuilder extends BaseBuilder<HighchartsSeriesOptions> {
  constructor(public options: HighchartsSeriesOptions) {
    super();
  }
}


export class HighchartsBuilder extends BaseBuilder<Highcharts.Options> {
  constructor(public options: Highcharts.Options = {}) {
    super();
  }

  title(title: string) {
    return this.set({
      title: {
        text: title
      }
    });
  }

  chart(chartType: string, polar: boolean = false) {
    return this.set({
      chart: {
        type: chartType,
        polar,
      }
    });
  }

  plotOptions(stacked: 'normal' | 'percent') {
    /**
     * We could be smarter, and insert these just for the series.types that are actually being used.
     * But for now, including the basic chart types by default
     */
    const stacking: 'normal' | 'percent' = stacked || undefined;
    return this.set({
      plotOptions: {
        area: { stacking },
        areaspline: { stacking },
        column: { stacking },
        line: { stacking },
        scatter: { stacking },
        spline: { stacking },
      }
    });
  }

  axis(axis: 'xAxis' | 'yAxis', builderFunction: BuilderFunction<AxisBuilder>) {
    if (Array.isArray(this.options[axis])) {
      throw new Error('Multiple axes are not supported');
    }
    this.set({ [axis]: {} });
    const axisBuilder = new AxisBuilder(this.options[axis] as Highcharts.AxisOptions);
    builderFunction(axisBuilder);
    return this;
  }

  xAxis(builderFunction: BuilderFunction<AxisBuilder>) {
    return this.axis('xAxis', builderFunction);
  }

  yAxis(builderFunction: BuilderFunction<AxisBuilder>) {
    return this.axis('yAxis', builderFunction);
  }

  addSeries(builderFunction: BuilderFunction<SeriesBuilder>) {
    if (!this.options.series) {
      this.options.series = [];
    }

    // There are specific types of series options, and Highcharts has a bunch of
    // interfaces that are 'switched' using the mandatory type parameter, but
    // this will just keep it simple with the basic options.
    const builder = new SeriesBuilder({ type: undefined });
    builderFunction(builder);

    // The Type property strikes again. Casting to any to shut this up.
    this.options.series.push(builder.options as any);
    return this;
  }
}

