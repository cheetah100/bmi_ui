import { COLOR_GROUPS } from '@bmi/ui';
import { createMapFrom } from '@bmi/utils';
import defaults from 'lodash-es/defaults';
import flatten from 'lodash-es/flatten';
import sortBy from 'lodash-es/sortBy';
import uniq from 'lodash-es/uniq';
import { DataSeries } from '../data-series/data-series';
import { findCategories, makeCategorical } from '../data-series/make-categorical';
import { AxisType, BasicChartBuilderOptions, BasicChartSettings, SeriesGroupConfig } from './basic-chart-config';
import { DEFAULT_CHART_CONFIG, DEFAULT_SERIES_CONFIG } from './basic-chart-contstants';
import { HighchartsBuilder } from './highcharts-builder';

export class ColorAllocator {
  private usedColors = new Set<string>();
  private colorPalette = COLOR_GROUPS.find(g => g.name === 'Chart Colors').colors
    .map(color => color.cssColor);

  /**
   * Mark a color as being used.
   *
   * This will add it to a set of used colours
   */
  markAsUsed(colorString: string) {
    this.usedColors.add(colorString);
  }

  /**
   * Get the next available color, and mark it as used.
   *
   * Once the color palette has been exhausted, this will give magenta as a
   * warning colour. This shouldn't happen under normal circumstances.
   */
  get(): string {
    const unusedColor = this.colorPalette.find(c => !this.usedColors.has(c));
    if (unusedColor) {
      this.markAsUsed(unusedColor);
      return unusedColor;
    } else {
      // We're out of colors. This probably means the chart has an extraordinary
      // number of visible series. We'll use magenta as the universal awful
      // color, but we could easily just generate 'random' colors instead.
      return '#FF00FF';
    }
  }
}


export function addAllSeries(builder: HighchartsBuilder, options: BasicChartBuilderOptions) {
  const chartSettings = options.chartSettings;
  const colorAllocator = new ColorAllocator();

  let dataForGroups = options.dataSeries;

  if (isCategoryAxis(chartSettings.xAxisType)) {
    const categories = findCategories(flatten(options.dataSeries));
    builder.xAxis(x => x.set({categories}));
    dataForGroups = options.dataSeries.map(serieses => makeCategorical(serieses, categories));
  }

  const seriesGroups = chartSettings.seriesGroups || [];

  if (seriesGroups.length !== options.dataSeries.length) {
    throw new Error('Chart Builder: number of data series arrays must correspond to series groups');
  }

  for (let i = 0; i < seriesGroups.length; i++) {
    addSeriesGroup(builder, seriesGroups[i], dataForGroups[i], colorAllocator);
  }
}


function addSeriesGroup(
  builder: HighchartsBuilder,
  group: SeriesGroupConfig,
  dataSeries: DataSeries[],
  colorAllocator: ColorAllocator
  ) {
    // The order of series in the chart settings should determine the order they
    // appear on the chart, if present in the data. In some modes, we are only
    // interested in the configured series, but in other cases we want to show all
    // series, even if they aren't configured.
    //
    // In the "all series, but some are configured" case, we want the configured
    // ones to come first, in order, and the rest to follow. We don't want to show
    // the same series twice.
    const seriesSettings = group.seriesSettings || [];
    const dataSeriesById = createMapFrom(dataSeries, ds => ds.seriesId);
    const seriesWithConfigs = group.seriesSettings
      .map(seriesConfig => dataSeriesById.get(seriesConfig.value))
      .filter(x => !!x);

    const dataSeriesForChart = uniq([
      ...seriesWithConfigs,
      ...(!group.selectedOnly ? dataSeries : [])
    ]);

    const seriesConfigsById = createMapFrom(seriesSettings || [], s => s.value);

    for (const ds of dataSeriesForChart) {
      const seriesConfig = defaults({},
        seriesConfigsById.get(ds.seriesId),
        DEFAULT_SERIES_CONFIG
      );

      builder.addSeries(chartSeries => {
        chartSeries
          .set({ id: ds.seriesId })
          .set({ name: seriesConfig.label ?? ds.label ?? ds.seriesId })
          .set({ data: sortBy(ds.datapoints, dp => dp.x) })
          .set({ connectNulls: seriesConfig.connectNulls })
          .set({ type: seriesConfig.type ?? group.type })
          .set({ visible: seriesConfig.visible })
          .set({ zoneAxis: seriesConfig.zoneAxis })
          .set({ zones: seriesConfig.zones });

        if (seriesConfig.color) {
          chartSeries.set({ color: seriesConfig.color });
          colorAllocator.markAsUsed(seriesConfig.color);
        } else {
          chartSeries.set({ color: colorAllocator.get() });
        }
      });
    }
}


const isCategoryAxis = (type: string): boolean => {
  /**
   * This will eventually just be for category (type === 'categories')
   * But for now, category handling is the default
   */
  return (type !== 'datetime');
}


export const hasAxisType = (chartSettings, type: AxisType): boolean => (
  (!!chartSettings.xAxisType && chartSettings.xAxisType === type)
  || (!!chartSettings.yAxisType && chartSettings.yAxisType === type)
);


export const builderStarter = (chartSettings: BasicChartSettings) => new HighchartsBuilder()
  .title(null)
  .xAxis(
    x => x.title(!chartSettings.hideAxisTitles ? chartSettings.xAxisTitle : null)
  )
  .yAxis(
    y => y.title(!chartSettings.hideAxisTitles ? chartSettings.yAxisTitle : null)
  )
  .chart('line', chartSettings.polar)
  .plotOptions(chartSettings.stacked)
  .set(DEFAULT_CHART_CONFIG);


export const addBothAxes = (builder, chartSettings) => {
  builder.xAxis(axis => axis.set(buildRegularAxis(chartSettings.xAxisType)));
  builder.yAxis(axis => axis.set(buildRegularAxis(chartSettings.yAxisType)));
}

const buildRegularAxis = (type: AxisType) => {
  const formatted = value => new Intl.NumberFormat().format(value);
  switch(type) {

    case 'count':
      return {
        labels: {
          formatter: function () {
            return formatted(this.value);
          }
        }
      };
  
    case 'percent':
      return {
        labels: {
          formatter: function () {
            return `${formatted(this.value)}%`;
          }
        }
      };

    case 'currency':
      return {
        labels: {
          formatter: function () {
            return `$${formatted(this.value)}`;
          }
        }
      };

    case 'datetime':
      // just let Highcharts format this. Varying time ranges are handled fine
      // later, we might allow custom formatting
      return {
        type: 'datetime',
      };

    default:
      return {};

  }
} 
