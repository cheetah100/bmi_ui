import { DataSeries } from '../data-series/data-series';
import { DataSeriesDef } from '../data-series/data-series-def';
import { ColorZone } from '../util/color-zone';


export type AxisType =
   'category'
 | 'datetime'
 | 'currency'
 | 'count'
 | 'percent';
  

export interface BasicChartSettings {
  xAxisTitle: string;
  xAxisType: AxisType;
  yAxisTitle: string;
  yAxisType: AxisType;
  polar: boolean;
  stacked: null | 'normal' | 'percent';

  hideAxisTitles?: boolean;
  hideLegend?: boolean;

  xAxisLabels?: AxisLabels;
  yAxisLabels?: AxisLabels;
  yAxisRange?: AxisRange;

  /**
   * If specified, sets the chart height in the Highcharts config.
   */
  chartHeight?: null | number | string;

  seriesGroups: SeriesGroupConfig[];
}


export interface AxisLabels {
  formatString?: string;

  /**
   * Allows custom formatting of the Y axis labels. Obviously this is not JSON
   * serializable, so only an option for dynamically generated widgets!
   */
  formatFunction?: (value: number) => string;
}

export interface AxisRange {
  min?: number;
  max?: number;
  softMin?: number;
  softMax?: number;
  endOnTick?: boolean;
}


export interface BasicChartSeriesConfig {
  value: string;
  label?: string;
  type?: string;
  color?: string;
  connectNulls?: boolean;
  visible?: boolean;
  zoneAxis?: 'x' | 'y';
  zones?: ColorZone[];
}


export interface SeriesGroupConfig {
  dataSeries: DataSeriesDef;
  type: string;
  selectedOnly: boolean;
  seriesSettings: BasicChartSeriesConfig[];
}

export interface BasicChartBuilderOptions {
  chartSettings: BasicChartSettings;
  dataSeries: DataSeries[][];
}

