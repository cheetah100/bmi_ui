import { Injectable, InjectionToken, Type, Provider, Inject } from '@angular/core';
import { Binder } from '../page-data-context/binder';
import { DataSource, DataSourceConfig } from './types';
import { createMapFrom } from '@bmi/utils';


export interface DataSourceFactory<
    TConfig extends DataSourceConfig = DataSourceConfig ,
    TSource extends DataSource = DataSource> {

  /**
   * Creates a DataSource from a config.
   *
   * This requires a binder to be provided that may be used to resolve any
   * bindings within the source config, or within the created data source
   * itself.
   */
  create(config: TConfig, binder: Binder): TSource;
}


export interface DataSourceTypeDefinition<TConfig extends DataSourceConfig = DataSourceConfig> {
  type: string;
  name: string;
  defaultConfig?: TConfig;
  service: Type<DataSourceFactory<TConfig, DataSource>>;
}


export const DATA_SOURCE_TYPE_TOKEN = new InjectionToken<DataSourceTypeDefinition>('Data Source Type Definitions');
export const DATA_SOURCE_FACTORY_TOKEN = new InjectionToken<DataSourceFactory>('Data Source Factories');


export function defineDataSourceType<TConfig extends DataSourceConfig = DataSourceConfig>(dst: DataSourceTypeDefinition<TConfig>): Provider[] {
  return [
    dst.service,
    {
      provide: DATA_SOURCE_TYPE_TOKEN,
      multi: true,
      useValue: dst
    },
    {
      provide: DATA_SOURCE_FACTORY_TOKEN,
      multi: true,
      useExisting: dst.service
    }
  ];
}



