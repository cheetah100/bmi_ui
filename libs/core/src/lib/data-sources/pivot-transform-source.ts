import { Injectable } from '@angular/core';
import { Observable, NEVER, of } from 'rxjs';
import { switchMap, map, shareReplay, tap } from 'rxjs/operators';
import { ParameterBinding, DataPathBinding } from '../page-data-context/parameter-binding';
import { BoundListResult, makeBoundList } from '../page-data-context/bound-list-result';
import { DataSourceWithListApi } from './types';
import { DataSourceFactory } from './data-source-factory';

import { FilterConditionWithBinding, bindConditions } from './filter-condition-with-binding';
import { TransformCacheService } from '../services/transform-cache.service';
import { Binder } from '../page-data-context/binder';
import { unrollPivot } from '../util/unroll-pivot';
import { PivotData } from '@bmi/gravity-services';
import { distinctUntilNotEqual } from '../util/distinct-until-not-equal';


export interface PivotTransformSourceConfig {
  type: string;
  transformId: string;
  conditions: FilterConditionWithBinding[];
}


@Injectable({providedIn: 'root'})
export class PivotTransformSourceFactory implements DataSourceFactory<PivotTransformSourceConfig, PivotTransformSource> {
  constructor(
    private transformCacheService: TransformCacheService,
  ) {}

  create(config: PivotTransformSourceConfig, binder: Binder) {
    return new PivotTransformSource(config, binder, this.transformCacheService);
  }
}


export class PivotTransformSource implements DataSourceWithListApi {
  private transform = bindConditions(this.config.conditions, this.binder).pipe(
    distinctUntilNotEqual(),
    tap(conditions => console.log('Pivot conditions', conditions)),
    switchMap(conditions => {
      if (conditions) {
        return this.transformCacheService.get({
          transformId: this.config.transformId,
          conditions: conditions
        });
      } else {
        return of(undefined);
      }
    }),
    shareReplay(1),
  );

  constructor(
    private config: PivotTransformSourceConfig,
    private binder: Binder,
    private transformCacheService: TransformCacheService,
  ) {}

  bindThis(bindings: ParameterBinding[]): Observable<BoundListResult> {
    const pathBinding = path => (<DataPathBinding>{bindingType: 'data-path', path: `this.${path}`});

    return this.transform.pipe(
      map(results => results ? unrollPivot(results as PivotData) : []),
      map(unrolledPivot => makeBoundList({
        listBindings: [pathBinding('x'), pathBinding('y'), pathBinding('value')],
        listData: unrolledPivot.map(r => ([r.x, r.y, r.value]))
      }))
    );
  }
}
