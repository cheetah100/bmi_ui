import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { DataSourceConfig, DataSourceWithPathLookup, DataSourceWithPathMetadata } from './types';

import lodashGet from 'lodash-es/get';

export class ObjectDataSource<T = any> implements DataSourceWithPathLookup {
  constructor(private data: Observable<T>) {}

  lookupPath(path: string) {
    return this.data.pipe(
      map(data => lodashGet(data || {}, path))
    );
  }
}
