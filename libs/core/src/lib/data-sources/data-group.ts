import { Observable } from 'rxjs';
import { DataSourceWithListApi } from './types';
import { ParameterBinding } from '../page-data-context/parameter-binding';


export interface DataSourceWithListGroupApi {
  bindGroups(groupId: ParameterBinding, groupName: ParameterBinding): Observable<DataGroup[]>;
}


export interface DataGroup {
  id: string;
  name: string;
  items: DataSourceWithListApi;
}
