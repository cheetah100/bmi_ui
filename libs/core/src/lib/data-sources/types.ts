import { InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';
import { ParameterBinding } from '../page-data-context/parameter-binding';
import { BoundListResult } from '../page-data-context/bound-list-result';
import { PathResolver } from '@bmi/gravity-services';
import { Binder } from '../page-data-context/binder';

export interface DataSourceConfig {
  type: string;
}


// tslint:disable-next-line:no-empty-interface
export interface DataSource {
  // Actually nothing right now. Add stuff here if we need a common interface
  // for all data sources to implement
}


export interface DataSourceWithBoard {
  board: string;
}


export interface DataSourceWithListApi {
  /**
   * If supported, binds to 'this' data source, resolving bindings
   *
   * Use case: you have a list (e.g. card list) and you want to resolve some
   * bindings against it, using 'this'.
   *
   * It will produce values for each row, and so you've got to iterate over the
   * results and extract the values you're interested in. BUT: to optimize this
   * you ideally need to know ahead of time what values will be needed; as an
   * implementation may be able to efficiently bulk-fetch all of these values in
   * a single operation.
   *
   * This API aims to solve the problem: you can and should provide a list of
   * bindings that you want (to allow prefetch), this will then enumarate the
   * results returning Binder objects -- these have a bind(x) -> Observable
   * method allowing you to look up `x` on the current row.
   *
   * If you don't prefetch, the exact behaviour is left up to the implementation
   * but it *should* make an effort to fetch these values. Perhaps it will emit
   * null values for now, but then trigger an update with all the newly
   * requested data.
   */
  bindThis(bindings: ParameterBinding[]): Observable<BoundListResult>;

  /**
   * A new, simpler list binding API.
   *
   * This returns a list of data sources for each item, allowing more
   * composability. The rationale here is that a card search has the full card
   * data available, so it can create a source to wrap around each result,
   * rather than projecting out all of the required bindings. This allows that
   * Card Data Source to be passed along into a nested data context or for each
   * list item much more simply.
   */
  bindList?(): Observable<DataSource[]>;

  getExampleItem?(): DataSource;
}

export interface DataSourceWithPathLookup {
  /**
   * Do a path lookup for this data source
   *
   * This will get passed a path like "foo.bar", and it's up to this method to
   * figure out what that means for this data source. It should emit a new value
   * when any bound parameters cause it to update.
   *
   * This method is optional. It will usually be called via the lookupPath
   * method on the page data context itself.
   *
   * @param path the path 'relative' to this data source
   * @returns the value, or null if it's not valid/available.
   */
  lookupPath(path: string): Observable<any>;
}


export interface DataSourceWithPathMetadata {
  /**
   * Provides a PathResolver instance for this fetcher to allow field completion
   * in the UI.
   */
  getPathResolver(): PathResolver;
}

