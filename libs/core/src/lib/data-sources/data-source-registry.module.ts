import { NgModule } from '@angular/core';
import { defineDataSourceType } from './data-source-factory';


import { PivotTransformSourceFactory } from './pivot-transform-source';
import { CardDataSourceFactory } from './card-data-source';
import { CardListDataSourceFactory } from './card-list-data-source';
import { CardDataSourceConfig } from '../page-data-context/card-fetcher-impl';
import { NotesDataSourceFactory } from '../notes/notes-data-source';

@NgModule({
  providers: [
    defineDataSourceType<CardDataSourceConfig>({
      type: 'card',
      name: 'Card',
      service: CardDataSourceFactory,
      defaultConfig: {
        type: 'card',
        board: null,
        conditions: null,
      }
    }),

    defineDataSourceType<CardDataSourceConfig>({
      type: 'card-list',
      name: 'Card List',
      service: CardListDataSourceFactory,
      defaultConfig: {
        type: 'card-list',
        board: null,
        conditions: null
      }
    }),

    defineDataSourceType({
      type: 'pivot-transform',
      name: 'Pivot Transform',
      service: PivotTransformSourceFactory,
      defaultConfig: {
        type: 'pivot-transform',
        transformId: null,
        conditions: []
      }
    }),

    defineDataSourceType({
      type: 'notes',
      name: 'Notes Data Source',
      service: NotesDataSourceFactory
    }),
  ]
})
export class DataSourceRegistryModule {}
