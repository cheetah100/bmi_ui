import { Injectable, Inject } from '@angular/core';

import { createMapFrom } from '@bmi/utils';
import { Binder } from '../page-data-context/binder';
import { DataSourceFactory, DATA_SOURCE_TYPE_TOKEN, DATA_SOURCE_FACTORY_TOKEN, DataSourceTypeDefinition } from './data-source-factory';
import { DataSource, DataSourceConfig } from './types';

import { AbstractConfigFactory } from '../util/config-factory';


@Injectable({providedIn: 'root'})
export class DataSourceService
  extends AbstractConfigFactory<DataSourceConfig, DataSource, Binder>
  implements DataSourceFactory {
  constructor(
    @Inject(DATA_SOURCE_TYPE_TOKEN) dataSourceTypes: DataSourceTypeDefinition[],
    @Inject(DATA_SOURCE_FACTORY_TOKEN) dataSourceFactories: DataSourceFactory[]
  ) {
    super('Data Source', dataSourceTypes, dataSourceFactories);
  }
}
