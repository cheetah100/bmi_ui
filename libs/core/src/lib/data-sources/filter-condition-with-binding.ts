import { Observable, of, combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

import { ParameterBinding, FormatterBinder } from '../formatter';
import { GravityApiConditionOperation, GravityApiCondition } from '@bmi/gravity-services';
import { Binder } from '../page-data-context/binder';

import { DataBindingEvent, makeValueEvent } from '../data-binding-event';
import { combineBindingEvents, bindingStatusPassthrough, extractValuesOnly } from '../util/data-binding-operators';


export interface FilterConditionWithBinding {
  fieldName: string;
  optional?: boolean;
  value: ParameterBinding;
  operation?: GravityApiConditionOperation;
}


export function bindConditions(conditions: FilterConditionWithBinding[], binder: FormatterBinder) {
  // The simple version of this function, and probably the one you should use
  // for now. This just picks out the events that have updated values, ignoring
  // the 'loading' events.
  return bindConditionsEvents(conditions, binder).pipe(
    extractValuesOnly()
  );
}


export function bindConditionsEvents(conditions: FilterConditionWithBinding[], binder: FormatterBinder): Observable<DataBindingEvent<GravityApiCondition[]>> {
  // The data binding event system is pretty confusing and was never fully
  // implemented due to it being a bit awkward. Nevertheless some of the
  // existing code uses it, and this causes the data sources to be invalidated
  // when their filters change, even if the immediately bound value is waiting
  // on a request.
  if (!conditions || conditions.length === 0) {
    return of(makeValueEvent([]));
  } else {
    return combineLatest(
      conditions.map(c => binder.bind(c.value))
    ).pipe(
      map(boundValues => boundValues.map((v, index) => ({ boundValue: v, condition: conditions[index] }))),
      map(conditionValuePairs => makeConditions(conditionValuePairs)),
      map(x => makeValueEvent(x))
    );
  }
}


function makeConditions(conditionValuePairs: {boundValue: any, condition: FilterConditionWithBinding}[]): GravityApiCondition[] {
  if (conditionValuePairs.some(c => !c.condition.optional && !c.boundValue)) {
    return null;
  } else {
    // TODO: we might actually want to filter by null fields here, but
    // supporting that seems non-trivial at this stage. It's not just a
    // problem here, but a UI issue, and what it means to have a null
    // filter.
    return conditionValuePairs
      .filter(c => !!c.boundValue)
      // TODO: Use the Condition class for generating these, and add
      // capabilities to Condition to transparently map metadata fields like
      // id -> _id.
      .map(c => (<GravityApiCondition>{
        fieldName: c.condition.fieldName,
        operation: c.condition.operation || 'EQUALTO',
        conditionType: 'PROPERTY',
        value: Array.isArray(c.boundValue) ? c.boundValue.join('|') : String(c.boundValue)
      }));
  }
}
