import { Injectable } from '@angular/core';
import { Observable, of, OperatorFunction, combineLatest } from 'rxjs';
import { distinctUntilChanged, map, switchMap } from 'rxjs/operators';
import { CardData, CardCacheService, PathLookupService, BoardPathResolver, GravityConfigService, } from '@bmi/gravity-services';
import { distinctUntilNotEqual } from '@bmi/utils';
import { Binder } from '../page-data-context/binder';
import { CardDataSourceConfig, makeCardSearchQuery } from '../page-data-context/card-fetcher-impl';

import { bindConditions } from './filter-condition-with-binding';
import { DataSource, DataSourceWithPathLookup } from './types';
import { DataSourceFactory } from './data-source-factory';


@Injectable({providedIn: 'root'})
export class CardDataSourceFactory
  implements DataSourceFactory<CardDataSourceConfig, CardDataSource> {

  constructor(
    private cardCache: CardCacheService,
    private pathLookup: PathLookupService,
    private configService: GravityConfigService,
  ) {}

  create(config: CardDataSourceConfig, binder: Binder): CardDataSource {
    return this.createFromCard(this.fetchCard(config, binder), binder, config.board);
  }

  createFromCard(card: Observable<CardData>, binder: Binder, boardId?: string) {
    return new CardDataSource(
      card,
      binder,
      this.pathLookup,
      boardId || undefined,
      boardId ? new BoardPathResolver(boardId, this.configService) : undefined
    );
  }

  private fetchCard(config: CardDataSourceConfig, binder: Binder): Observable<CardData> {
    return bindConditions(config.conditions, binder).pipe(
      distinctUntilNotEqual(),
      switchMap(conditions => {
        if (!conditions) {
          return of(null);
        } else {
          return this.cardCache.search(makeCardSearchQuery(config, conditions)).pipe(
            map(cards => cards && cards[0] || null)
          );
        }
      })
    );
  }
}


export class CardDataSource
    implements DataSource, DataSourceWithPathLookup {

  constructor(
    private card: Observable<CardData>,
    private binder: Binder,
    private pathLookup: PathLookupService,
    public readonly board?:string,
    private pathResolver?: BoardPathResolver,
  ) {}

  lookupPath(path: string) {
    return this.card.pipe(
      switchMap(card => card ? this.pathLookup.getField(card, path) : of(null)),
      distinctUntilChanged(),
    )
  }

  getPathResolver() {
    return this.pathResolver;
  }
}
