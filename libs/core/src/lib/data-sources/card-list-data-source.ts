import { Injectable } from '@angular/core';
import { Observable, of, combineLatest } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { CardData, CardCacheService, PathLookupService, BoardPathResolver, GravityConfigService, } from '@bmi/gravity-services';
import { combineLatestObject, combineLatestDeferred, distinctUntilNotEqual, switchMapOverItems } from '@bmi/utils';
import { Binder } from '../page-data-context/binder';
import { ParameterBinding, getThisPath, isThisPathBinding } from '../page-data-context/parameter-binding';
import { BoundListResult, makeBoundList, } from '../page-data-context/bound-list-result';
import { CardDataSourceConfig, makeCardSearchQuery } from '../page-data-context/card-fetcher-impl';

import { bindConditions } from './filter-condition-with-binding';
import { DataSource, DataSourceWithListApi, DataSourceWithPathLookup } from './types';
import { DataGroup } from './data-group';
import { DataSourceFactory } from './data-source-factory';
import { CardDataSource } from './card-data-source';

import groupBy from 'lodash-es/groupBy';
import sortBy from 'lodash-es/sortBy';


@Injectable({providedIn: 'root'})
export class CardListDataSourceFactory
  implements DataSourceFactory<CardDataSourceConfig, CardListDataSource> {

  constructor(
    private cardCache: CardCacheService,
    private pathLookup: PathLookupService,
    private configService: GravityConfigService,
  ) {}

  create(config: CardDataSourceConfig, binder: Binder): CardListDataSource {
    return this.createFromCards(this.fetchCards(config, binder), binder, config.board);
  }

  createFromCards(cards: Observable<CardData[]>, binder: Binder, boardId?: string) {
    return new CardListDataSource(
      cards,
      binder,
      this.pathLookup,
      boardId ? boardId : undefined,
      boardId ? new BoardPathResolver(boardId, this.configService) : undefined
    );
  }

  private fetchCards(config: CardDataSourceConfig, binder: Binder): Observable<CardData[]> {
    return bindConditions(config.conditions, binder).pipe(
      distinctUntilNotEqual(),
      switchMap(conditions => {
        if (!conditions) {
          return of(null);
        } else {
          return this.cardCache.search(makeCardSearchQuery(config, conditions));
        }
      })
    );
  }
}


export class CardListDataSource
  implements DataSource, DataSourceWithListApi, DataSourceWithPathLookup {

  constructor(
    private cards: Observable<CardData[]>,
    private binder: Binder,
    private pathLookup: PathLookupService,
    public readonly board?:string,
    private pathResolver?: BoardPathResolver,
  ) {}

  lookupPath(path: string) {
    return this.cards.pipe(
      distinctUntilNotEqual(),
      switchMapOverItems(card => this.pathLookup.getField(card, path))
    );
  }

  bindThis(bindings: ParameterBinding[]): Observable<BoundListResult> {
    // Split the bindings into the 'this' bindings that we need to resolve
    // against this list, vs. any other 'global' bindings that will be the same
    // value for every row.
    const thisBindings = bindings.filter(x => isThisPathBinding(x));
    const otherBindings = bindings.filter(x => !thisBindings.includes(x));

    return combineLatest([
      this.cards.pipe(
        switchMapOverItems(card => combineLatestDeferred(thisBindings.map(x => this.bindOnCard(card, x))))
      ),
      combineLatestDeferred(otherBindings.map(x => this.binder.bind(x)))
    ]).pipe(
      map(([listData, otherData]) => makeBoundList({
        listBindings: thisBindings,
        listData: listData,
        otherBindings: otherBindings,
        otherData: otherData
      }))
    );
  }

  bindList(): Observable<CardDataSource[]> {
    // This new API operation just needs to return data sources for each item in
    // the list. We can reuse the card data source to get all of its powers.
    return this.cards.pipe(
      map(cards => cards?.map(c => this.makeCardItemSource(of(c))))
    );
  }

  getExampleItem(): CardDataSource {
    return this.makeCardItemSource(of(null));
  }

  private bindOnCard(card: CardData, binding: ParameterBinding): Observable<any> {
    if (isThisPathBinding(binding)) {
      return this.pathLookup.getField(card, getThisPath(binding));
    } else {
      return this.binder.bind(binding);
    }
  }

  private makeCardItemSource(card: Observable<CardData>) {
    return new CardDataSource(card, this.binder, this.pathLookup, this.board, this.pathResolver);
  }

  private spawnDataSource(cards: Observable<CardData[]>) {
    return new CardListDataSource(cards, this.binder, this.pathLookup, this.board, this.pathResolver);
  }

  bindGroups(groupId: ParameterBinding, groupName: ParameterBinding): Observable<DataGroup[]> {
    return this.cards.pipe(
      switchMapOverItems(card => combineLatestObject({
        card: of(card),
        id: this.bindOnCard(card, groupId),
        name: this.bindOnCard(card, groupName),
      })),
      map(cardsWithGroupInfo => {
        const grouped = groupBy(cardsWithGroupInfo, c => String(c.id));
        const dataGroups = Object.entries(grouped).map(([id, items]) => <DataGroup>{
          id: id,
          name: String(items[0].name || id),
          items: this.spawnDataSource(of(items.map(x => x.card)))
        });

        return sortBy(dataGroups, dg => dg.name, dg => dg.id);
      })
    );
  }

  getPathResolver() {
    return this.pathResolver;
  }
}


