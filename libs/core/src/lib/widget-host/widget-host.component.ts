import {
  Component,
  Input,
  Output,
  EventEmitter,
  SimpleChanges,
  OnInit,
  OnDestroy,
  OnChanges,
  AfterContentInit,
  forwardRef,
  Type,
  InjectFlags,
  ComponentFactoryResolver,
  ComponentRef,
  Injector,
  Optional,
  Inject,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';

import { HostBinding, HostListener } from '@angular/core';

import { Observable, of, ReplaySubject, BehaviorSubject, combineLatest, Subscription, EMPTY } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  distinctUntilKeyChanged,
  filter,
  map,
  publishReplay,
  refCount,
  switchMap,
  tap
} from 'rxjs/operators';

import cloneDeep from 'lodash-es/cloneDeep';

import { WidgetConfig, WidgetSelection } from '../widget-config';
import { WidgetContext, WidgetMode } from '../widget-context';
import { EditorContext } from '../editor/editor-context';
import { WidgetDefinition, WidgetDefinitionService } from '../widget-definition';
import { EditWidgetAspect } from './edit-widget-aspect';

import { distinctUntilNotEqual } from '../util/distinct-until-not-equal';

import { PageInfoService } from '../page/page-info.service';
import { WidgetRepositoryService } from '../services/widget-repository.service';
import { WidgetWrap, provideWidgetWrapperContext } from '../components/widget-wrapper/widget-wrap';
import { WidgetWrapperComponent } from '../components/widget-wrapper/widget-wrapper.component';

import { FLAIR_FACTORY_TOKEN, FlairFactory } from '../flair/flair';
import { FlairContextService } from '../flair/flair-context.service';
import { FlairConfig } from '../flair/flair-config';



@Component({
  selector: 'bmi-widget-host',
  template: `<ng-template #target></ng-template>`,
  styles: [`
    :host {
      display: block;
      width: 100%;
    }

    :host.is-editable {
      border: 1px solid transparent;
    }

    :host.is-selected {
      border: 1px dashed blue;
    }
  `]
})
export class WidgetHostComponent implements OnInit, OnDestroy, OnChanges, AfterContentInit {
  @Input() config: WidgetSelection;


  /**
   * The display mode for this widget: view or edit? [optional]
   *
   * If set, this will put the widget explicitly into view or edit mode. The
   * default is undefined, which means inherit from parent. If no parent is set,
   * then it will default to 'view'
   */
  @Input('mode') modeInput: WidgetMode = undefined;

  /**
   * If true, provide some basic editing facilities if possible.
   *
   * If in the editor and this property is true the widget will have some basic
   * selection behavior when clicked. This will show a generic widget editor
   * in the sidebar (if available).
   *
   * This feature should simplify the job of layout widgets as it means they
   * don't have to handle child widget selection in simple cases.
   *
   * When selected, the widget host will get a .is-selected class applied to its
   * element, and some default styling will be added. This can be customized by
   * the parent component.
   */
  @Input() editable = false;

  /**
   * For a specific use case: chart widgets sitting inside reference widgets
   * Set to false to prevent this widget changing the flair icons on widget wrapper
   */
  @Input() updateFlairIcons = true;

  /**
   * Disable flairs on this widget host. The primary use case for this is on the
   * widget editors, which are themselves widgets. If flairs are processed then
   * the flairs also apply to the widget editors, which usually doesn't have a
   * visual effect, but has a very bizarre effect if
   */
  @Input() noFlairs = false;

  /**
   * Overrides the WidgetDefinitionService to use when looking up widgets
   *
   * By default, the injected Widget def service will be used. Widget editors
   * use the same widget host architecture, but their defs are provided by a
   * different factory. This input allows the service to be overriden for a
   * specific WidgetHost without it affecting all children.
   *
   * This case comes up if you wanted to try and create a widget host inside an
   * editor (e.g. using dynamic forms), as previously the widget-config-editor
   * would just provide a different service, but this affected the entire
   * subtree, not just the editor in question.
   */
  @Input('widgetDefinitionService') inputWidgetDefinitionService: WidgetDefinitionService = undefined;
  /**
   * Emitted when the widget wants to propagate a change to the configuration.
   *
   * This should be taken as a request, in response to a user action. A
   * container widget in a hierarchy should listen for changes from its
   * children, merge them with its own config and pass them up, as the
   * config will likely be embedded inside the parent widget's config.
   *
   * At the top level of a widget hierarchy, a page designer (for example) will
   * subscribe to this event, update the state and then pass the state back down
   * via the config input. (unidirectional data flow)
   */
  @Output() updateWidgetConfig = new EventEmitter<WidgetSelection>();

  @ViewChild('target', { read: ViewContainerRef, static: true }) target: ViewContainerRef;


  // tslint:disable-next-line:no-unsafe-any
  @HostBinding('class.is-editable')
  get isEditable(): boolean {
    return !!this.editorContext && this.editable;
  }

  // tslint:disable-next-line:no-unsafe-any
  @HostBinding('class.is-selected')
  get isSelected(): boolean {
    return this.isEditable && !!this.selectedAspect && this.isEditModeNow;
  }

  get widgetDefinitionService() {
    return this.inputWidgetDefinitionService || this.injectedWidgetDefinitionService;
  }

  private selectedAspect: any = null;

  private componentRef: ComponentRef<any>;

  private contentHasInitialized = new EventEmitter<void>();
  private widgetConfigSubject = new ReplaySubject<WidgetSelection>(1);

  private readonly subscription = new Subscription();
  private widgetContext: WidgetContext = null;
  private widgetContextInjector: Injector = null;

  public readonly widgetConfig = this.widgetConfigSubject.pipe(
    distinctUntilChanged(),
    switchMap(configOrId => {
      if (typeof (configOrId) === 'string') {
        return this.getWidgetConfigById(configOrId);
      } else {
        return of(configOrId);
      }
    }),
    publishReplay(1),
    refCount()
  );

  private widgetModeInputSubject = new BehaviorSubject<WidgetMode>(this.modeInput);
  private _currentModeSubject = new BehaviorSubject<WidgetMode>(undefined);
  public readonly mode = this._currentModeSubject.asObservable();

  get isEditModeNow() {
    return this._currentModeSubject.value === 'edit';
  }

  public readonly widgetSelection = this.widgetConfigSubject.pipe(distinctUntilChanged())

  constructor(
    private injectedWidgetDefinitionService: WidgetDefinitionService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private injector: Injector,
    private widgetRepositoryService: WidgetRepositoryService,
    @Optional() private parentWidgetContext: WidgetContext,
    @Optional() private editorContext: EditorContext,
    @Optional() private pageInfoService: PageInfoService,
  ) { }


  private getWidgetConfigById(widgetId: string): Observable<WidgetConfig> {
    if (!this.pageInfoService || !this.widgetRepositoryService) {
      console.error('WidgetHost: cannot load widget by ID outside the context of a page!', this);
      return EMPTY;
    } else {
      return this.pageInfoService.pageInfo.pipe(
        map(pageInfo => pageInfo.applicationId),
        distinctUntilChanged(),
        switchMap(appId => this.widgetRepositoryService.getWidget(appId, widgetId)),
      );
    }
  }

  ngOnInit() {
    this.subscription.add(combineLatest([
      this.widgetConfig,

      // After the widget config subject we can put other observable sources
      // that will trigger the widget to be created (or recreated)
      this.contentHasInitialized
    ]).pipe(
      map(([widgetConfig]) => widgetConfig || { widgetType: <string>null }),
      distinctUntilKeyChanged('widgetType'),
      switchMap(widgetConfig => this.widgetDefinitionService.getWidgetDefinition(widgetConfig ? widgetConfig.widgetType : null))
    ).subscribe(widgetDefinition => {
      this.createWidget(widgetDefinition);
    }));

    this.subscription.add( combineLatest([
      this.widgetModeInputSubject,
      this.parentWidgetContext ? this.parentWidgetContext.mode : of('view' as WidgetMode)
    ]).pipe(
      map(([mode, parentMode]) => mode || parentMode || 'view')
    ).subscribe(this._currentModeSubject));

    if (this.isEditable) {
      this.editorContext.watchForOwnSelection(this).subscribe(selected => this.selectedAspect = selected);
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.config) {
      this.widgetConfigSubject.next(this.config);
    }

    if (changes.modeInput) {
      this.widgetModeInputSubject.next(this.modeInput);
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.destroyWidget();
    if (this.selectedAspect) {
      this.editorContext.select(null);
    }
  }

  ngAfterContentInit() {
    this.contentHasInitialized.emit();
  }

  // tslint:disable-next-line:no-unsafe-any
  @HostListener('click', ['$event'])
  handleClick(event: MouseEvent) {
    if (this.isEditable && this.widgetContext && this.isEditModeNow) {
      this.editorContext.select(<EditWidgetAspect>{
        owner: this,
        tempEditorTypeId: 'generic-edit-widget',
        widgetSelection: this.widgetSelection,
        injector: this.widgetContextInjector,
        setWidgetSelection: selection => this.widgetContext.setWidgetConfig(() => cloneDeep(selection))
      });
    }
  }

  private createWidget(widgetDefinition: WidgetDefinition) {
    this.destroyWidget();

    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(widgetDefinition.component);
    const cleanup = new Subscription();

    // The widget context is a private service to communicate with the child
    // instance. We can use this instead of trying to assign properties on the
    // child widget -- this doesn't play nicely with change detection. Instead,
    // the context service can provide convenient observables which can be
    // watched, filtered and combined with other sources in the widget
    // implementation.
    const widgetContext = this.widgetContext = new WidgetContext(
      widgetDefinition,
      this.widgetConfig.pipe(
        // We want to support widget configs changing, but if the widget type
        // changes, it means we will tear-down and recreate the widget: this might
        // not happen immediately, so we don't want these new changes to go to the
        // wrong child widget.
        filter(x => !x || x.widgetType === widgetDefinition.widgetType)
      ),
      this.widgetSelection,
      this.mode,
      this.editorContext
    );

    cleanup.add(() => widgetContext.destroy());

    cleanup.add(widgetContext.onWidgetConfigUpdateRequested.subscribe(config => {
      this.updateWidgetConfig.emit(config);
    }));

    const widgetInjector = this.widgetContextInjector = Injector.create({
      name: 'Widget Child Injector',
      parent: this.injector,
      providers: [
        { provide: WidgetContext, useValue: widgetContext },
        { provide: FlairContextService, deps: [FLAIR_FACTORY_TOKEN, Injector] },

        // Some widget types can request to have their own wrapper context,
        // allowing them to capture and control their own title and links.
        // If this is set, titles, route and icons will need to be handled by
        // the widget, as they will not propagate outside the child view.
        widgetDefinition.provideOwnWrapperContext ? provideWidgetWrapperContext() : []
      ],
    });

    const flairService = widgetInjector.get(FlairContextService);

    // If flairs are disabled on this widget, we just ignore setting them up
    // automatically. For DI reasons, the flair context can still exist, and we
    // will clean it up. The main concern here is preventing widget editors from
    // loading the flairs they are editing, and then those effects spreading
    // into the page editor via the selection's injector
    if (!this.noFlairs) {
      cleanup.add(widgetContext.widgetConfig.pipe(
        map(config => config && config.flairs || null),
        distinctUntilNotEqual(),
      ).subscribe(flairConfig => flairService.initialize(flairConfig)));
    }

    cleanup.add(() => flairService.destroy());

    const widgetWrapper = widgetInjector.get(WidgetWrap as Type<WidgetWrap>, null, InjectFlags.Optional);
    if (widgetWrapper && this.updateFlairIcons) {
      widgetWrapper.setIcon(flairService.icons);
    }

    this.componentRef = this.target.createComponent(componentFactory, undefined, widgetInjector);
    this.componentRef.onDestroy(() => cleanup.unsubscribe());
  }

  private destroyWidget() {
    if (this.componentRef) {
      this.componentRef.destroy();
      this.componentRef = null;
      this.widgetContextInjector = null;
      this.widgetContext = null;
    }
  }
}
