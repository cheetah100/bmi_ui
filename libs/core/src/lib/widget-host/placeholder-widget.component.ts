import { Component, Optional } from '@angular/core';

import { WidgetConfig } from '../widget-config';
import { WidgetContext } from '../widget-context';
import { EditorContext } from '../editor/editor-context';


interface PlaceholderOptions {
  message?: string;
  onlyVisibleInEditor?: boolean;
  placeholderStyle?: 'plain' | 'add-widget';
}


@Component({
  selector: 'bmi-placeholder-widget',
  template: `
    <div *ngIf="shouldShowPlaceholder | async" class="widget-placeholder--{{placeholderStyle}}">
      <div *ngIf="placeholderStyle === 'add-widget'; else plainPlaceholder" class="widget-placeholder__menu-wrapper">
        <bmi-add-widget-dropdown (widgetCreated)="widgetCreated($event)" #menu>
          <button type=button class="widget-placeholder__add-widget-button {{ menu.isOpen ? 'widget-placeholder__add-widget-button--open' : '' }}">
            Add Widget <ui-icon icon="shared-ui-caret-down"></ui-icon>
          </button>
        </bmi-add-widget-dropdown>
      </div>
      <ng-template #plainPlaceholder>
        {{message}}
      </ng-template>
    </div>
  `,
  styleUrls: ['./placeholder-widget.component.scss']
})
export class PlaceholderWidgetComponent {

  private options: PlaceholderOptions = this.widgetContext.widgetDefinition.options || {};

  constructor (
    private widgetContext: WidgetContext,
    @Optional() private editorContext: EditorContext
  ) {}

  get message() {
    return this.options.message || 'Cannot display this widget';
  }

  get placeholderStyle() {
    return this.options.placeholderStyle || 'plain';
  }

  get shouldShowPlaceholder() {
    return this.widgetContext.isEditMode;
  }

  widgetCreated(config: WidgetConfig) {
    this.widgetContext.setWidgetConfig(() => config);
  }
}
