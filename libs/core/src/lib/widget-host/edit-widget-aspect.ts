import { Injector } from '@angular/core';

import { Observable } from 'rxjs';

import { EditableAspect } from '../editor/editable-aspect';
import { WidgetConfig, WidgetSelection } from '../widget-config';
import { WidgetContext } from '../widget-context';

import cloneDeep from 'lodash-es/cloneDeep';

export interface EditWidgetAspect extends EditableAspect {
  widgetSelection: Observable<WidgetSelection>;
  setWidgetSelection(widgetSelection: WidgetSelection);
}
