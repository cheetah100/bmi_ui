import { Inject, Injectable, Optional } from '@angular/core';

import { Observable, of } from 'rxjs';

import { WidgetDefinition, WidgetDefinitionService,  BMI_WIDGETS } from '../widget-definition';

import { PlaceholderWidgetComponent } from '../widget-host/placeholder-widget.component';




@Injectable({providedIn: 'root'})
export class BmiWidgetDefinitionService implements WidgetDefinitionService {
  constructor(
    @Inject(BMI_WIDGETS) private widgetDefinitions: WidgetDefinition[],
  ) {}

  makeDefaultWidgetDefinition(widgetType: string): WidgetDefinition {
    return {
      widgetType: widgetType,
      component: PlaceholderWidgetComponent,
      options: {
        message: 'Add Widget Here',
        onlyVisibleInEditor: true,
        placeholderStyle: 'add-widget'
      }
    }
  }

  getWidgetDefinition(widgetType: string): Observable<WidgetDefinition> {
    const existingWidgetDefinition = this.widgetDefinitions.find(x => x.widgetType === widgetType);
    return of(existingWidgetDefinition || this.makeDefaultWidgetDefinition(widgetType));
  }
}
