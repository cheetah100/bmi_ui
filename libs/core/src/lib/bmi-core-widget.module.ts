import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedUiModule } from '@bmi/ui';

import { WidgetHostComponent } from './widget-host/widget-host.component';

import { WidgetDefinitionService } from './widget-definition';
import { BmiWidgetDefinitionService } from './widget-host/bmi-widget-definition.service';

import { BmiCoreComponentsModule } from './components/bmi-core-components.module';
// import { WidgetHostHostComponent } from './widgets/testing/widget-host-host.component';


@NgModule({
  imports: [
    CommonModule,
    SharedUiModule,
  ],

  declarations: [
    WidgetHostComponent,
    // WidgetHostHostComponent,
  ],

  exports: [
    WidgetHostComponent,
    // WidgetHostHostComponent,
  ],

  providers: [
    // The WidgetHost component uses DI to locate the widget definition service.
    // Regular widgets are provided by this BmiWidgetDefinitionService, so we
    // set up this global mapping here.
    //
    // The same WidgetHost infrastructure is used for editor widgets, but this
    // gets wrapped by the WidgetConfigEditor component, which injects a
    // different provider.
    { provide: WidgetDefinitionService, useExisting: BmiWidgetDefinitionService }
  ],
})
export class BmiCoreWidgetModule { }
