import { Component, forwardRef, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';

import { from } from 'rxjs';
import { map, publishBehavior, refCount, switchMap, take, withLatestFrom } from 'rxjs/operators';

import { BaseControlValueAccessor, provideAsControlValueAccessor, TypedFormGroup, TypedFormControl } from '@bmi/ui';

import { WidgetConfig, WidgetSelection, validateWidgetSelection } from '../widget-config';
import { WidgetTemplateService } from '../widget-config-template.service';
import { WidgetRepositoryService } from '../services/widget-repository.service';
import { PageEditorContext } from '../page-editor/page-editor-context';

import { WidgetConverter } from '../widget-converter/widget-converter';
import { WidgetConverterService } from '../widget-converter/widget-converter.service';


import sortBy from 'lodash-es/sortBy';

import { Option } from '@bmi/ui';

import { clipboardCopyText, clipboardPasteText } from '../util/clipboard';
import { pasteWidgetConfig } from '../util/copy-paste-widget-config';


@Component({
  selector: 'bmi-widget-selector',
  templateUrl: './widget-selector.component.html',
  styleUrls: ['./widget-selector.component.scss'],
  providers: [
    provideAsControlValueAccessor(forwardRef(() => WidgetSelectorComponent))
  ]
})
export class WidgetSelectorComponent extends BaseControlValueAccessor<WidgetSelection> implements OnChanges, OnInit {

  @Input() widgetSelection: WidgetSelection;
  @Output() updateWidgetSelection = new EventEmitter<WidgetSelection>();

  // tslint:disable-next-line: max-union-size
  mode: 'choosing-widget-template' | 'configuring';

  moduleId = this.pageEditorContext.appId;

  availableConverters: WidgetConverter[] = [];

  readonly form = new TypedFormGroup({
    widgetConfig: new TypedFormControl<WidgetConfig>(null)
  });

  get title() {
    if (this.mode === 'configuring') {
      return 'Embedded Widget';
    } else {
      return 'Choose Widget Type';
    }
  }

  get widgetConfig() {
    return this.form.value.widgetConfig;
  }

  constructor(
    private widgetTemplateService: WidgetTemplateService,
    private pageEditorContext: PageEditorContext,
    private widgetConverterService: WidgetConverterService
  ) {
    super();
  }

  ngOnInit() {

    this.form.valueChanges.subscribe(value => {
      const widgetSelection = value.widgetConfig;
      this.updateWidgetSelection.emit(widgetSelection);
      this.onChanged(widgetSelection);
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.widgetSelection) {
      this.updateComponentValue(this.widgetSelection);
    }
  }

  getWidgetSelection(): WidgetSelection {
    const formValue = this.form.value;
    return formValue.widgetConfig;
  }


  private updateModeFromFormState() {
    // The form has been updated, so update the display mode of this control to
    // correspond to the new widget selection.
    const value = this.form.value;
    if (value.widgetConfig && value.widgetConfig.widgetType) {
      this.mode = 'configuring';
    } else {
      this.mode = 'choosing-widget-template';
    }
  }

  private updateForm(value: WidgetSelection, emitEvent = true) {
    this.form.setValue({
      widgetConfig: value && typeof (value) !== 'string' ? value : null
    }, { emitEvent });
    this.updateModeFromFormState();
    this.availableConverters = this.widgetConverterService.getConverters(this.widgetConfig);
  }

  updateComponentValue(value: WidgetSelection) {
    this.updateForm(value, false);
    this.updateModeFromFormState();
  }

  enterConfigMode() {
    this.mode = 'choosing-widget-template';
  }

  addWidgetConfig(config: WidgetConfig) {
    this.updateWidgetConfig(config);
  }

  updateWidgetConfig(widgetConfig: WidgetConfig) {
    this.updateForm(widgetConfig);
  }

  applyConversion(converter: WidgetConverter) {
    const config = this.widgetConfig;
    if (this.widgetConverterService.doesConverterSupport(converter, config)) {
      this.updateForm(converter.convert(config), true);
    }
  }

  clearSelection() {
    this.form.reset();
    this.mode = 'choosing-widget-template';
  }

  copyWidgetConfig() {
    clipboardCopyText(JSON.stringify(this.getWidgetSelection(), undefined, 2))
      .catch(error => alert('Copy Error: ' + error));
  }
  pasteWidgetConfig() {
    pasteWidgetConfig().subscribe({
      next: widgetSelection => this.updateForm(widgetSelection),
      error: err => alert('Paste Error: ' + err)
    });
  }
}


