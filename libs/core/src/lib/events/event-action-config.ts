export interface EventActionConfig {
  type: string;
  [key: string]: any;
}
