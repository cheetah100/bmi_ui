import { Injectable, Inject, Injector, InjectionToken, Provider } from '@angular/core';
import { Observable } from 'rxjs';

import { Binder } from '../page-data-context/binder';
import { AbstractConfigFactory, ServiceDef, ConfigFactory } from '../util/config-factory';
import { PageDataContext } from '../page-data-context/page-data-context';
import { EventActionConfig } from './event-action-config';
import { EventActionFunction, chainActions, ActionResult, EventContext, runAction } from './event-action';


export interface EventActionFactoryParams {
  injector: Injector;
  dataContext: PageDataContext;
}

export type EventActionFactory<TConfig = EventActionConfig> = ConfigFactory<TConfig, EventActionFunction, EventActionFactoryParams>;
export type EventActionFactoryDef<TConfig = EventActionConfig> = ServiceDef<EventActionFactory<TConfig>>;

export const EVENT_ACTION_DEF_TOKEN = new InjectionToken<EventActionFactoryDef>('Event Action Definitions');
export const EVENT_ACTION_FACTORIES = new InjectionToken<EventActionFactory>('Event Action Factories');


export function defineEventAction<TConfig>(def: EventActionFactoryDef<TConfig>): Provider[] {
  return [
    def.service ? def.service : [],
    {
      provide: EVENT_ACTION_DEF_TOKEN,
      multi: true,
      useValue: def
    },
    {
      provide: EVENT_ACTION_FACTORIES,
      multi: true,
      useExisting: def.service
    }
  ]
}


@Injectable({providedIn: 'root'})
export class EventActionFactoryService
  extends AbstractConfigFactory<EventActionConfig, EventActionFunction, EventActionFactoryParams>
  implements EventActionFactory {
  constructor(
    @Inject(EVENT_ACTION_DEF_TOKEN) defs: EventActionFactoryDef[],
    @Inject(EVENT_ACTION_FACTORIES) factories: EventActionFactory[]
  ) {
    super('Event Actions', defs, factories);
  }

  createActionChain(configs: EventActionConfig[], params: EventActionFactoryParams): EventActionFunction {
    return chainActions(configs.map(config => this.create(config, params)));
  }


  runActionChain(config: EventActionConfig[], params: EventActionFactoryParams): Observable<ActionResult> {
    return runAction(
      this.createActionChain(config, params),

      // Filling out the event context here. This whole aspect of the event
      // system needs some design and thought!
      {dataContext: params.dataContext},
    );
  }
}
