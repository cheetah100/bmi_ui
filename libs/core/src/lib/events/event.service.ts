import { Injectable, OnDestroy, Injector } from '@angular/core';
import { Observable, BehaviorSubject, Subscription, Subject, merge, EMPTY, isObservable, of } from 'rxjs';
import { map, mergeMap, switchMapTo, catchError, withLatestFrom, switchMap } from 'rxjs/operators';
import { ObjectMap, distinctUntilNotEqual, flattenLatest } from '@bmi/utils';
import { EventContext, EventActionFunction, runAction, NOOP_ACTION } from './event-action';
import { EventActionConfig } from './event-action-config';
import { EventActionFactoryService } from './event-action-factory.service';
import { PageDataContext } from '../page-data-context/page-data-context';

interface BmiEventListener {
  eventName: string;
  action: EventActionFunction;
}


interface BmiEventInvocation {
  eventName: string,
  ctx: EventContext
}


@Injectable()
export class EventService implements OnDestroy {
  constructor(
    private actionFactory: EventActionFactoryService,
    private injector: Injector,
    private dataContext: PageDataContext,
  ) {
    this.handlerSubscription.add(this.eventSubject.pipe(
      withLatestFrom(this.listeners),
      mergeMap(([{eventName, ctx}, listeners]) => {
        const matchingListeners = listeners.filter(x => x.eventName === eventName);
        if (matchingListeners.length > 0) {
          return merge(...matchingListeners.map(x => this.invokeListener(x, ctx)));
        } else {
          return EMPTY;
        }
      })
    ).subscribe());

    this.handlerSubscription.add(this.on(this.listenersFromConfig));
  }

  private handlerSubscription = new Subscription();
  private eventSubject = new Subject<BmiEventInvocation>();
  private configSubject = new BehaviorSubject<ObjectMap<EventActionConfig[]>>({})
  private listenerSources = new BehaviorSubject<Observable<BmiEventListener[]>[]>([]);

  private listeners = this.listenerSources.pipe(
    switchMap(listenerSources => flattenLatest(listenerSources))
  );

  private listenersFromConfig = this.configSubject.pipe(
    distinctUntilNotEqual(),
    map(config => Object.entries(config)
      .map(([eventName, actionConfig]) => this.createListenerFromConfig(eventName, actionConfig))
    )
  );

  ngOnDestroy() {
    this.handlerSubscription.unsubscribe();
  }

  emit(eventName: string, ctx: EventContext = this.makeActionContext()) {
    this.eventSubject.next({eventName, ctx});
  }

  on(listener_or_sources: BmiEventListener | Observable<BmiEventListener[]>): Subscription {
    const listenerSource = isObservable(listener_or_sources) ? listener_or_sources : of([listener_or_sources]);
    this.listenerSources.next([...this.listenerSources.value, listenerSource]);
    return new Subscription(() => this.removeSource(listenerSource));
  }

  setConfig(eventConfig: ObjectMap<EventActionConfig[]>) {
    this.configSubject.next(eventConfig || {});
  }

  private removeSource(source: Observable<BmiEventListener[]>) {
    this.listenerSources.next(this.listenerSources.value.filter(s => s === source));
  }

  private invokeListener(listener: BmiEventListener, ctx: EventContext) {
    return runAction(listener.action || NOOP_ACTION, ctx).pipe(
      catchError((err) => {
        console.error(`Event Error (${listener.eventName})`, err);
        return EMPTY;
      }),
      switchMapTo(EMPTY)
    )
  }

  private makeActionContext(): EventContext {
    return {
      dataContext: this.dataContext
    };
  }

  private createListenerFromConfig(eventName: string, actionConfig: EventActionConfig[]): BmiEventListener {
    let action: EventActionFunction = undefined;
    try {
      action = actionConfig && this.actionFactory.createActionChain(actionConfig, {
        dataContext: this.dataContext,
        injector: this.injector
      });
    } catch (error) {
      console.error('Cannot create action: ', error);
    }

    return {
      eventName: eventName,
      action: action
    };
  }
}
