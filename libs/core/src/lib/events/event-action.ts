import { Observable, of, EMPTY, defer } from 'rxjs';
import { defaultIfEmpty, last, switchMap } from 'rxjs/operators';
import { PageDataContext } from '../page-data-context/page-data-context';


export interface EventContext {
  dataContext: PageDataContext;
}


export class ActionResult {
  constructor(
    private continueProcessing: boolean
  ) {}

  get continue() {
    // Using a property here
    return this.continueProcessing;
  }

  static readonly CONTINUE = of(new ActionResult(true));
  static readonly STOP = of(new ActionResult(false));
}


export type EventActionFunction = (eventContext: EventContext) => Observable<ActionResult>;



/**
 * A simple action function that will immediately continue. This is essentially a noop.
 */
export const NOOP_ACTION: EventActionFunction = ctx => ActionResult.CONTINUE;

export const HALT_ACTION: EventActionFunction = ctx => ActionResult.STOP;



/**
 * Sequences an array of action functions
 *
 * Each action in the chain must complete before the subsequent will run. Once
 * an action returns continue:false, this will 'short circuit' the rest of the
 * chain.
 */
export function chainActions(actionFunctions: EventActionFunction[]): EventActionFunction {
  return actionFunctions.reduce(composeAction, NOOP_ACTION);
}


/**
 * Sequences two action functions together
 *
 * This performs a single combination. If the results of the first action allow
 * it to continue, then the second will follow on afterwards.
 *
 * If the first action returns a continue:false result, the second action will
 * be skipped and the first result is returned.
 */
export function composeAction(a: EventActionFunction, b: EventActionFunction): EventActionFunction {
  return ctx => runAction(a, ctx).pipe(
    switchMap(result => result.continue ? runAction(b, ctx) : of(result))
  );
}


/**
 * Makes a cold observable that will run the action when subscribed to.
 *
 * This will run the action function, and is only interested in the final
 * result. The action function will not be called until a subscription is made.
 */
export function runAction(action: EventActionFunction, ctx: EventContext): Observable<ActionResult> {
  return defer(() =>  action(ctx).pipe(
    defaultIfEmpty(new ActionResult(true)),
    last()
  ));
}
