import { InjectionToken, Provider } from '@angular/core';
import { DynamicEditorDefinition, registerDynamicEditor } from '@bmi/ui';
import { ObjectMap, Omit } from '@bmi/utils';

import { EventActionConfig } from './event-action-config';


export interface EventActionEditorDef<TConfig extends EventActionConfig = EventActionConfig> extends DynamicEditorDefinition {
  defaultConfig: Omit<TConfig, 'type'>;
  options?: ObjectMap<any>;
}


export const EVENT_ACTION_EDITORS = new InjectionToken<EventActionEditorDef>('BMI Event Action Editors');


export function defineActionEditor<TConfig extends EventActionConfig = EventActionConfig>(editorDef: EventActionEditorDef<TConfig>): Provider {
  return registerDynamicEditor('event-action', editorDef);
}

