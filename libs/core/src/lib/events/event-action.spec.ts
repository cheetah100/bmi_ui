import { Subject, Observable } from 'rxjs';
import { ObservableWatcher } from '@bmi/utils';
import { NOOP_ACTION, EventActionFunction, chainActions, composeAction, ActionResult, EventContext, HALT_ACTION, runAction } from './event-action';


class TestAction {
  public resultSubject = new Subject<ActionResult>();
  actionCallCount = 0;

  action: EventActionFunction = ctx => {
    this.actionCallCount += 1;
    return this.resultSubject;
  };

  emit(shouldContinue: boolean) {
    this.resultSubject.next(new ActionResult(shouldContinue));
  }

  finish(shouldContinue: boolean) {
    this.emit(shouldContinue);
    this.resultSubject.complete();
  }
}


describe('Event Action composition', () => {
  const ctx: EventContext = { dataContext: null };
  let watch: ObservableWatcher<ActionResult>;

  function expectResultContinue() {
    expect(watch.latestValue.continue).toBeTruthy();
    expect(watch.isComplete).toBeTruthy();
  }

  function expectResultHalt() {
    expect(watch.latestValue.continue).toBeFalsy();
    expect(watch.isComplete).toBeTruthy();
  }


  describe('NOOP_ACTION', () => {
    beforeEach(() => watch = new ObservableWatcher(NOOP_ACTION(ctx)));

    it('should emit a single value', () =>
      expect(watch.values.length).toBe(1));

    it('should be a continue result', () =>
      expect(watch.latestValue.continue).toBeTruthy())

    it('should be complete', () =>
      expect(watch.isComplete).toBeTruthy());
  });

  describe('HALT_ACTION', () => {
    beforeEach(() => watch = new ObservableWatcher(HALT_ACTION(ctx)));

    it('should emit a single value', () =>
      expect(watch.values.length).toBe(1));

    it('should be a continue:false result', () =>
      expect(watch.latestValue.continue).toBeFalsy());

    it('should complete the stream', () =>
      expect(watch.isComplete).toBeTruthy());
  });

  describe('runAction', () => {
    let a: TestAction;
    let observable: Observable<ActionResult>;
    beforeEach(() => {
      a = new TestAction();
      observable = runAction(a.action, ctx);
    });

    it('should not have called the action function before subscrption', () =>
      expect(a.actionCallCount).toBe(0));

    describe('After subscribing', () => {
      beforeEach((() => watch = new ObservableWatcher(observable)));

      it('should return a "continue" result if the action completes without a result', () =>{
        a.resultSubject.complete();
        expectResultContinue();
      });
    })
  });

  describe('composeAction: behaviour around continue', () => {
    const TEST_CASES = [
      {should: 'continue when chaining two noops', actions: [NOOP_ACTION, NOOP_ACTION], expectedContinue: true},
      {should: 'halt when chaining two halts', actions: [HALT_ACTION, HALT_ACTION], expectedContinue: false},
      {should: 'halt if the first action is halt', actions: [HALT_ACTION, NOOP_ACTION], expectedContinue: false},
      {should: 'halt if the second action is halt', actions: [NOOP_ACTION, HALT_ACTION], expectedContinue: false}
    ];

    for (const {should, actions, expectedContinue} of TEST_CASES) {
      it(`should ${should}`, () => {
        watch = new ObservableWatcher(composeAction(actions[0], actions[1])(ctx));
        expect(watch.values.length).toBe(1);
        expect(watch.latestValue.continue).toBe(expectedContinue);
        expect(watch.isComplete).toBeTruthy();
      });
    }
  });

  describe('composeAction: short circuit behaviour', () => {
    let action1: TestAction;
    let action2: TestAction;

    beforeEach(() => {
      action1 = new TestAction();
      action2 = new TestAction();
      watch = new ObservableWatcher(composeAction(action1.action, action2.action)(ctx));
    });

    describe('Before the first action resolves', () => {
      it('should have called the first action once', () =>
        expect(action1.actionCallCount).toBe(1));

      it('should not have called the second action', () =>
        expect(action2.actionCallCount).toBe(0));

      it('should not emit anything yet', () =>
        expect(watch.values).toEqual([]));

    });

    describe('After the first action resolves', () => {
      beforeEach(() => action1.finish(true));

      it('should call the second action', () =>
        expect(action2.actionCallCount).toBe(1));

      it('should not have called the first action any more', () =>
        expect(action1.actionCallCount).toBe(1));

      it('should not emit anything yet', () =>
        expect(watch.hasEmittedValue).toBeFalsy());
    });

    it('should not call the second action until the first completes', () => {
      action1.emit(true);
      expect(action2.actionCallCount).toBe(0);
    });

    it('should only return the last result of the second action', () => {
      action1.finish(true);
      action2.emit(true);
      action2.finish(false);

      expectResultHalt();
    })

    describe('If the first action returns continue: false', () => {
      beforeEach(() => action1.finish(false));

      it('should not call the second', () =>
        expect(action2.actionCallCount).toBe(0));

      it('should return a false action result', () =>
        expect(watch.latestValue.continue).toBeFalsy());

      it('should complete the result stream', () =>
        expect(watch.isComplete).toBeTruthy());
    })
  });


  describe('chainActions: composing many actions together', () => {
    // This function is just a reduction of composeAction, but we should focus
    // on the edge cases -- empty list, etc.


    function watchChain(actions: EventActionFunction[]) {
      watch = new ObservableWatcher(chainActions(actions)(ctx));
    }

    it('should treat an empty array as equivalent to noop', () => {
      watchChain([]);
      expectResultContinue();
    });

    it('should treat a chain of noops as noop', () => {
      watchChain([NOOP_ACTION, NOOP_ACTION, NOOP_ACTION]);
      expectResultContinue();
    });

    it('should treat a chain with a halt as a halt', () => {
      watchChain([NOOP_ACTION, HALT_ACTION, NOOP_ACTION, NOOP_ACTION]);
      expectResultHalt();
    });


    describe('When action functions should get called', () => {

      let a1: TestAction;
      let a2: TestAction;
      let chain: EventActionFunction;

      beforeEach(() => {
        a1 = new TestAction();
        a2 = new TestAction();
        // I've added in some extra noops so that we're really testing the chain
        // behaviour rather than just having the actions immediately adjacent,
        // as this would overlap with the composeAction test cases.
        chain = chainActions([
          NOOP_ACTION,
          a1.action,
          NOOP_ACTION,
          NOOP_ACTION,
          a2.action,
          NOOP_ACTION
        ]);
      });

      it('should not run the action functions as part of chaining', () => {
        expect(a1.actionCallCount).toBe(0);
        expect(a2.actionCallCount).toBe(0);
      });

      describe('When calling the chained action function', () => {
        let resultObservable: Observable<ActionResult>;
        beforeEach(() => resultObservable = chain(ctx));

        it('should not call the functions without a subscription', () =>
          expect(a1.actionCallCount).toBe(0));

        it('should call the action function after a subscription', () => {
          resultObservable.subscribe();
          expect(a1.actionCallCount).toBe(1);
        });

        it('should call the actions per subscription (i.e. it is a cold observable)', () => {
          resultObservable.subscribe();
          resultObservable.subscribe();
          expect(a1.actionCallCount).toBe(2);
        });
      });
    });

  });
});

