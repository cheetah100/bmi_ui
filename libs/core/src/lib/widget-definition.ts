import { Type, InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';

export type WidgetComponentClass = Type<{}>;


export interface WidgetDefinition {
  widgetType: string;
  component: WidgetComponentClass;
  noFlairEditing?: boolean;

  /**
   * If true, the widget host should provide an instance of the WidgetWrapper on
   * the child injector. This allows the widget to handle its own titles, flairs
   * etc.
   */
  provideOwnWrapperContext?: boolean;

  options?: {[key: string]: any};
}

export const BMI_WIDGETS = new InjectionToken<WidgetDefinition>('BMI Widget Definitions');
export const BMI_WIDGET_EDITORS = new InjectionToken<WidgetDefinition>('BMI Widget Editor Definitions');


/**
 * A class-interface for a widget definition service.
 *
 * This is used as a DI token by the WidgetHost component for finding widget
 * definitions. By providing a different service implementation you can change
 * the widgets that will be loadable.
 *
 * If the widget cannot be found, this service can return a falsy value, which
 * will cause the WidgetHost to error out, or it can return a suitable
 * placeholder.
 *
 * If there is an error loading a widget, (e.g. the component factory cannot be
 * resolved in the current module), then the getErrorWidget() method will be
 * called for an alternative placeholder widget to load in its place.
 *
 * The getWidgetDefinition() method returns an observable: this could emit
 * multiple values -- for example if it was necessary to load a widget
 * definition asynchronously, it could return a "Loading" widget definition,
 * then later emit the real one.
 */
export abstract class WidgetDefinitionService {
  /**
   * Gets a widget definition for a widget type.
   *
   * This can be asynchronous, and can emit serveral values; for example if a
   * widget definition needed to be loaded async, it could first return a
   * loading placeholder, then later emit the correct widget definition.
   *
   * The exact behaviour if a widget isn't found is up to the implementation of
   * this service: it could throw an error which might upset the widget host, or
   * it could return a placeholder widget definition to load in place of the
   * widget.
   */
  abstract getWidgetDefinition(widgetType: string): Observable<WidgetDefinition>;

  /**
   * If the widget host encounters an error, it may ask the service for a widget
   * definition to use.
   *
   * This method could return a falsy value or throw an error if it really
   * wanted to make its point.
   */
  abstract getErrorWidget?(originalWidgetDefinition?: WidgetDefinition): WidgetDefinition;
}
