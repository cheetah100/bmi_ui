import { Injectable } from '@angular/core';
import { switchMap, take } from 'rxjs/operators';
import { ParameterBinding } from '../../page-data-context/parameter-binding';
import { CardEditorModalService } from '../../card-editor/card-editor-modal.service';
import { CardEditorConfig } from '../../card-editor/card-editor-config';

import { EventActionFunction, ActionResult, EventActionFactory, EventActionFactoryService, EventActionFactoryParams } from '../../events';

export interface EditCardActionConfig extends CardEditorConfig {}


@Injectable()
export class EditCardEventAction implements EventActionFactory<EditCardActionConfig> {
  constructor(
    private cardEditorService: CardEditorModalService
  ){}

  create(config: EditCardActionConfig, params: EventActionFactoryParams): EventActionFunction {
    return ctx => {
      const dataContext = ctx.dataContext || params.dataContext;
      return dataContext.bind(config.cardId).pipe(
        take(1),
        switchMap(cardId => this.cardEditorService.show(
          {...config, cardId: cardId },
          { dataContext: dataContext }
        )),
        switchMap(result => result && result.action !== 'cancel' ? ActionResult.CONTINUE : ActionResult.STOP)
      );
    };
  }
}
