import { Component } from '@angular/core';
import { OptionlistService } from '../../services/optionlist.service';
import { TypedFormGroup, TypedFormControl, DynamicEditorContext } from '@bmi/ui';
import { debugObservableEvents } from '@bmi/utils';
import { EditCardActionConfig } from './edit-card-event-action';

@Component({
  template: `
    <bmi-card-editor-config [formControl]="form"></bmi-card-editor-config>
  `
})
export class EditCardActionEditorComponent {
  constructor(
    private context: DynamicEditorContext<EditCardActionConfig>,
    private optionlistService: OptionlistService
  ){}

  form = new TypedFormControl<EditCardActionConfig>();

  ngOnInit() {
    this.context.config.subscribe(config =>this.form.patchValue(config || {}, {emitEvent: false}));
    this.form.valueChanges.subscribe(fields => this.context.setConfig(fields));
  }
}
