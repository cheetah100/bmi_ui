import { Omit } from '@bmi/utils';
import { GenericEventActionEditorComponent } from './generic-event-action-editor.component';
import { defineActionEditor, EventActionEditorDef, EventActionConfig } from '../../events'
import { MappedFormSchemaForType } from '../../forms/generic-editor-schema';



interface DefineGenericActionEditorParams<T> extends Omit<EventActionEditorDef<T & EventActionConfig>, 'component'> {

  schema: MappedFormSchemaForType<T>;
}


export function defineGenericActionEditor<T>(params: DefineGenericActionEditorParams<T>) {
  return defineActionEditor<T & EventActionConfig>({
    // Ideally we'd just spread ...params here, but Angular's metadata compiler
    // doesn't support spread, and fails silently in prod mode.
    type: params.type,
    name: params.name,
    icon: params.icon,
    defaultConfig: params.defaultConfig,
    component: GenericEventActionEditorComponent,
    options: {
      schema: params.schema,
    }
  });
}
