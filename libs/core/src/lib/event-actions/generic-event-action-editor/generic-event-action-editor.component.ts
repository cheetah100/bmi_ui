import { Component } from '@angular/core';
import { TypedFormControl, DynamicEditorContext } from '@bmi/ui';
import { PluginMetadata, PluginMetadataField } from '@bmi/gravity-services'
import { ObjectMap, Omit } from '@bmi/utils';

import { EventActionConfig } from '../../events';

import { MappedFormSchemaForType, generatePluginMetadataFromSchema } from '../../forms/generic-editor-schema';


interface GenericEventActionEditorOptions<T extends EventActionConfig> {
  schema: MappedFormSchemaForType<Omit<T, 'type'>>;
}


@Component({
  template: `
    <bmi-plugin-form
      *ngIf="pluginMetadata"
      [pluginMetadata]="pluginMetadata"
      [formControl]="control"
      [defaultColumnWidth]="12">
    </bmi-plugin-form>
  `
})
export class GenericEventActionEditorComponent<TConfig extends EventActionConfig> {
  constructor(
    private context: DynamicEditorContext<TConfig>
  ) {}

  control = new TypedFormControl<TConfig>(null);
  pluginMetadata: PluginMetadata;

  readonly options = (this.context.definition.options || {}) as GenericEventActionEditorOptions<TConfig>
  readonly schema = this.options.schema;


  ngOnInit() {
    if (this.schema) {
      this.pluginMetadata = generatePluginMetadataFromSchema(this.schema);
    }

    this.context.config.subscribe(config => this.control.setValue(config, {emitEvent: false}));
    this.control.valueChanges.subscribe(value => this.context.setConfig(value));
  }
}

