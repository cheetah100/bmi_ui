import { Injectable, InjectFlags } from '@angular/core';
import { defer } from 'rxjs';
import { switchMap, take, defaultIfEmpty } from 'rxjs/operators';
import { BmiRoute } from '../navigation/bmi-route';
import { BmiRouteHandler } from '../navigation/bmi-route-handler';
import { RoutesService } from '../services/routes.service';
import { PageInfoService } from '../page/page-info.service';

import { EventActionFunction, ActionResult, EventActionFactory, EventActionFactoryService, EventActionFactoryParams } from '../events';

export interface NavigateToPageConfig {
  route: BmiRoute;
}


@Injectable()
export class NavigateToPageAction implements EventActionFactory<NavigateToPageConfig> {
  constructor(
    private routeService: RoutesService,
  ){}

  create(config: NavigateToPageConfig, params: EventActionFactoryParams): EventActionFunction {
    const pageInfoService  = params.injector.get(PageInfoService, null, InjectFlags.Optional);
    const routeHandler = params.injector.get(BmiRouteHandler) || this.routeService
    return ctx => {
      const currentModuleId = pageInfoService ? pageInfoService.currentModuleId : undefined;
      return routeHandler.navigate(config.route, {
        binder: ctx.dataContext || params.dataContext,
        moduleId: currentModuleId || undefined
      }).pipe(
        switchMap(success => success ? ActionResult.CONTINUE : ActionResult.STOP)
      );
    };
  }
}
