import { Injectable } from '@angular/core';
import { switchMap, take, defaultIfEmpty } from 'rxjs/operators';
import { ConfirmationModalService } from '@bmi/ui';
import { ParameterBinding } from '../page-data-context/parameter-binding';
import { CardEditorModalService } from '../card-editor/card-editor-modal.service';

import { EventActionFunction, ActionResult, EventActionFactory, EventActionFactoryService, EventActionFactoryParams } from '../events';

export interface MarkdownConfirmationConfig {
  message: string;
  buttonYesText: string;
  buttonCancelText: string;
}


@Injectable()
export class MarkdownConfirmationAction implements EventActionFactory<MarkdownConfirmationConfig> {
  constructor(
    private confirmationService: ConfirmationModalService
  ){}

  create(config: MarkdownConfirmationConfig, params: EventActionFactoryParams): EventActionFunction {
    return ctx => {
      return this.confirmationService.show({
        content: config.message,
        buttonYesText: config.buttonYesText || 'OK',
        buttonCancelText: config.buttonCancelText,
        emitCancelEvent: true,
      }).pipe(
        switchMap(result => result && result.action !== 'cancel' ? ActionResult.CONTINUE : ActionResult.STOP),
      );
    };
  }
}
