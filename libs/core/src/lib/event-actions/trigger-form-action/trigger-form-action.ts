import { Injectable } from '@angular/core';

import { FormContext } from '../../forms/form-context';

import { EventActionFunction, ActionResult, EventActionFactory, EventActionFactoryService, EventActionFactoryParams } from '../../events';


export interface TriggerFormActionConfig {
  action: string;
}


@Injectable()
export class TriggerFormAction implements EventActionFactory<TriggerFormActionConfig> {

  create(config: TriggerFormActionConfig, params: EventActionFactoryParams): EventActionFunction {
    return ctx => {
      const formContext = params.injector.get(FormContext);

      formContext.triggerAction({
        type: 'save',
      });

      return ActionResult.CONTINUE;
    };
  }
}

