import { NgModule, Provider } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { PortalModule } from '@angular/cdk/portal';

import { GravityUiModule } from '@bmi/gravity-services';
import { SharedUiModule } from '@bmi/ui';

import { BmiCoreEditorComponentsModule } from '../editor-components/bmi-core-editor-components.module';

import { BmiFormsModule } from '../forms/bmi-forms.module';
import { defineActionEditor } from '../events/event-action-editor-def';
import { EditorWidgets } from '../forms/generic-editor-schema';

import { GenericEventActionEditorComponent } from './generic-event-action-editor/generic-event-action-editor.component';
import { defineGenericActionEditor } from './generic-event-action-editor/define-generic-action-editor';

import { EditCardActionEditorComponent } from './edit-card-modal/edit-card-action-editor.component';

import { MarkdownConfirmationConfig } from './markdown-confirmation-action';
import { NavigateToPageConfig } from './navigate-to-page-action';
import { EditPhaseActionEditorComponent } from '../phase-editor/edit-phase-action-editor.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedUiModule,
    GravityUiModule,
    RouterModule,
    PortalModule,
    BmiCoreEditorComponentsModule,
    BmiFormsModule,
  ],

  declarations: [
    GenericEventActionEditorComponent,
    EditCardActionEditorComponent,
    EditPhaseActionEditorComponent,
  ],

  entryComponents: [
    GenericEventActionEditorComponent,
    EditCardActionEditorComponent,
    EditPhaseActionEditorComponent,
  ],

  providers: [
    defineGenericActionEditor<MarkdownConfirmationConfig>({
      type: 'markdown-confirmation',
      name: 'Confirmation Popup',
      defaultConfig: {
        message: '## Question?\n\nAre you sure you want to do that?',
        buttonYesText: 'Yes',
        buttonCancelText: 'No',
      },

      schema: {
        message: EditorWidgets.textarea({label: 'Message Text'}),
        buttonYesText: EditorWidgets.textInput({label: 'Yes button label (leave blank for "OK")'}),
        buttonCancelText: EditorWidgets.textInput({label: 'Cancel button label (leave blank to hide)'})
      }
    }),

    defineActionEditor({
      type: 'edit-card-modal',
      name: 'Show Card Editor',
      component: EditCardActionEditorComponent,
      defaultConfig: {}
    }),

    defineActionEditor({
      type: 'show-change-phase',
      name: 'Show Phase Editor',
      component: EditPhaseActionEditorComponent,
      defaultConfig: {}
    }),

    defineActionEditor({
      type: 'trigger-form-action',
      name: 'Trigger Form Save',
      component: null,
      defaultConfig: {action: 'save'}
    }),

    defineGenericActionEditor<NavigateToPageConfig>({
      type: 'navigate',
      name: 'Navigate to Page',
      defaultConfig: {route: null},
      schema: {
        route: EditorWidgets.routeSelector({label: 'Target Page', required: true})
      }
    })
  ]
})
export class EventActionsEditorRegistry { }
