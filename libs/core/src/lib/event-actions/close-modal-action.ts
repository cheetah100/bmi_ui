import { Injectable, InjectFlags } from '@angular/core';
import { defer } from 'rxjs';
import { switchMap, take, defaultIfEmpty } from 'rxjs/operators';
import { ModalContext } from '@bmi/ui';

import { EventActionFunction, ActionResult, EventActionFactory, EventActionFactoryService, EventActionFactoryParams } from '../events';


@Injectable()
export class CloseModalAction implements EventActionFactory<{}> {
  create(config: {}, params: EventActionFactoryParams): EventActionFunction {
    const modalContext = params.injector.get(ModalContext, undefined, InjectFlags.Optional);
    return ctx => {
      if (modalContext) {
        modalContext.complete({action: 'yes'});
      }
      return ActionResult.CONTINUE;
    };
  }
}
