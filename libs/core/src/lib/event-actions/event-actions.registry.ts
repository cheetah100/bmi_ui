import { NgModule, Provider } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { PortalModule } from '@angular/cdk/portal';

import { GravityUiModule } from '@bmi/gravity-services';
import { SharedUiModule } from '@bmi/ui';

import { defineEventAction } from '../events/event-action-factory.service';

import { EditCardEventAction } from './edit-card-modal/edit-card-event-action';
import { TriggerFormAction } from './trigger-form-action/trigger-form-action';
import { MarkdownConfirmationAction } from './markdown-confirmation-action';
import { NavigateToPageAction } from './navigate-to-page-action';
import { CloseModalAction } from './close-modal-action';
import { EditPhaseEventAction } from '../phase-editor/edit-phase-event-action';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedUiModule,
    GravityUiModule,
    RouterModule,
    PortalModule,
  ],

  providers: [
    defineEventAction({
      type: 'markdown-confirmation',
      service: MarkdownConfirmationAction,
    }),

    defineEventAction({
      type: 'edit-card-modal',
      service: EditCardEventAction
    }),

    defineEventAction({
      type: 'show-change-phase',
      service: EditPhaseEventAction
    }),

    defineEventAction({
      type: 'trigger-form-action',
      service: TriggerFormAction
    }),

    defineEventAction({
      type: 'navigate',
      service: NavigateToPageAction,
    }),

    defineEventAction({
      type: 'trigger-close-modal',
      service: CloseModalAction,
    })
  ]
})
export class EventActionsRegistry { }
