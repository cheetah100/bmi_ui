import { ADD_BINDING_TYPE_TO_FORMATTERS } from './add-binding-type-to-formatters';


describe('Page Migration: add binding type to formatters', () => {

  const TEST_CASES = [
    {
      it: 'should add a binding type to formatters',
      input: {formatterType: 'example', foo: 'bar'},
      expected: {bindingType: 'formatter', formatterType: 'example', foo: 'bar'}
    },
    {
      it: 'should leave other types of bindings alone',
      input: {bindingType: 'data-path', 'path': 'foo.bar'},
      expected: {bindingType: 'data-path', 'path': 'foo.bar'}
    },
    {it: 'should pass values', input: null, expected: null},
    {it: 'should pass through strings'},
    {
      it: 'should not touch objects that are not formatters',
      input: {foo: 'bar'},
      expected: {foo: 'bar'}
    }
  ];

  for (const testCase of TEST_CASES) {
    it(testCase.it, () => {
      expect(ADD_BINDING_TYPE_TO_FORMATTERS(testCase.input as any)).toEqual(testCase.expected as any);
    })
  }

})
