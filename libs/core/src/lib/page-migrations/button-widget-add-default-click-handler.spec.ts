import { BUTTON_WIDGET_ADD_DEFAULT_CLICK, ButtonWidgetConfig } from './button-widget-add-default-click-handler';



function makeButton(fields: Partial<ButtonWidgetConfig['fields']>): ButtonWidgetConfig {
  return {
    widgetType: 'form-control:button',
    fields: {...fields}
  }
}

function convert(config: ButtonWidgetConfig): ButtonWidgetConfig {
  return BUTTON_WIDGET_ADD_DEFAULT_CLICK(config);
}

const TRIGGER_SAVE_ACTION = [{type: 'trigger-form-action', action: 'save'}];

function expectHasTriggerSaveOnClick(config: ButtonWidgetConfig) {
  expect(config.fields.onClick).toEqual(TRIGGER_SAVE_ACTION);
}


describe('Migrate button widget to default to a Form Save action', () => {
  it('should ignore widgets that are not buttons', () => {
    const widget = {widgetType: 'foobar', fields: {}};
    expect(convert(widget as ButtonWidgetConfig)).toEqual(widget);
  });

  const CASES_TO_ADD_TRIGGER = [
    {when: 'onClick is undefined', widget: makeButton({onClick: undefined})},
    {when: 'onClick is null', widget: makeButton({onClick: null})},
    {when: 'onClick is absent', widget: makeButton({})}
  ];

  for (const {when, widget} of CASES_TO_ADD_TRIGGER) {
    it(`should add a trigger save when ${when}`, () => expectHasTriggerSaveOnClick(convert(widget)));
  }

  const CASES_TO_LEAVE_ALONE = [
    {when: 'onClick is an empty array', widget: makeButton({onClick: []})},
    {
      when: 'onClick has some other events already',
      widget: makeButton({onClick: [{type: 'confirmation-modal'}, ...TRIGGER_SAVE_ACTION]})
    }
  ];

  for (const {when, widget} of CASES_TO_LEAVE_ALONE) {
    it(`should leave onClick alone when ${when}`, () => {
      expect(convert(widget).fields.onClick).toEqual(widget.fields.onClick);
    });
  }
});
