import { defineTransform } from "@bmi/utils";
import { matches } from 'lodash-es';
import { ParameterBinding } from '../formatter';
import { BmiPageRoute } from '../navigation/bmi-route';
import { WidgetConfig } from '../widget-config';
import { DefaultColumnOptions } from '../widgets/table/default-table-column/default-column-config';
import { TableWidgetColumn, TableWidgetConfig } from '../widgets/table/table-config';
import { TimelineColumnOptions } from '../widgets/table/timeline-column/timeline-column-config';


interface LegacyTableWidgetColumn {
  /** @deprecated use the value ParameterBinding instead */
  field?: string;

  value: ParameterBinding;
  width?: string;
  type?: string;
  title?: string;
  route?: BmiPageRoute;

  mergeRows?: boolean;

  columnStyle?: string;
  options?: LegacyTableColumnOptions;
}

interface LegacyActionColumnOptions {
  actionsColumnTitle: string,
  add: boolean,
  edit: boolean,
  delete: boolean
}

interface LegacyTableColumnOptions {
  startQuarterId?: string;
  endQuarterId?: string;

  itemStartDate?: ParameterBinding;
  itemEndDate?: ParameterBinding;
  itemName?: ParameterBinding;


  edit?: boolean,
  delete?: boolean

}

export interface LegacyTableWidgetFields {
  pageRows: number;

  /** @deprecated use dataSourceId instead */
  context?: string;

  /**
   * The data source to use for this table.
   */
  dataSourceId: string;

  search: boolean;
  editable: boolean;
  columns: LegacyTableWidgetColumn[];
  actions: LegacyActionColumnOptions;
}

export type LegacyTableWidgetConfig = WidgetConfig<LegacyTableWidgetFields>;


/**
 * This does a config migration for tables to tidy up a couple of config fields
 * before these configs get deployed to the wider world.
 *
 * In a nutshell:
 *
 *  - context -> dataSourceId
 *  - column.field -> column.value, gets translated to a binding.
 */
function updateVeryOldConfig(config: LegacyTableWidgetConfig): LegacyTableWidgetConfig {
  const fields = config.fields;
  const updated: LegacyTableWidgetConfig = {
    ...config,
    fields: {
      ...fields,
      dataSourceId: fields.dataSourceId || fields.context,

      columns: fields.columns.map(c => ({
        ...c,
        value: c.field && !c.value ? <ParameterBinding>{ bindingType: 'data-path', path: `this.${c.field}` } : c.value
      }))
    }
  }

  // Remove any deprecated fields
  delete updated.fields.context;
  updated.fields.columns.forEach(c => delete c.field);

  return updated;
}



function performMigration(tableConfig: LegacyTableWidgetConfig) {
  const config = updateVeryOldConfig(tableConfig);

  const v2Table: TableWidgetConfig = {
    ...config,
    widgetType: 'table-v2',
    fields: {
      dataSourceId: config.fields.dataSourceId,
      hasPagination: true,
      rowsPerPage: config.fields.pageRows,
      hasSearch: config.fields.search,
      columns: config.fields.columns.map(c => migrateColumn(c)),
      isEditable: config.fields.editable ?? false,
      editOptions: {
        add: config.fields.actions?.add ?? false,
        edit: config.fields.actions?.edit ?? false,
        delete: config.fields.actions?.delete ?? false,
        cardId: {bindingType: 'data-path', path: 'this.id'},
      }
    }
  };

  return v2Table;
}

function migrateColumn(original: LegacyTableWidgetColumn): TableWidgetColumn {

  const commonProps: Partial<TableWidgetColumn> = {
    title: original.title,
    mergeRows: original.mergeRows,
    width: original.width ?? '1fr',
  };

  const columnStyle = original.columnStyle ?? 'default';
  if (columnStyle === 'timeline') {
    return {
      ...commonProps,
      content: <TimelineColumnOptions>{
        ...original.options,
        type: 'timeline',
      }
    };
  } else {
    // Some of the existing column formats can be implemented with formatter
    // bindings. The 'special' table formatters also handled sorting, but this
    // is a feature that should be added to the formatter system instead.
    let valueBinding = original.value;

    if (columnStyle === 'percentage') {
      valueBinding = {
        bindingType: 'formatter',
        formatterType: 'number-format',
        input: valueBinding,
        style: 'percentage-0-to-1'
      };
    } else if (columnStyle === 'date') {
      valueBinding = {
        bindingType: 'formatter',
        formatterType: 'datetime',
        input: valueBinding,
        style: 'date'
      };
    }

    // As display style is optional here, we'll just make a null/falsy value the default text style.
    let displayStyle: string = null;
    if (columnStyle === 'boolean' || columnStyle === 'status') {
      displayStyle = columnStyle;
    } else if (original.route) {
      displayStyle = 'link';
    }

    return {
      ...commonProps,
      content: <DefaultColumnOptions>{
        type: 'default',
        value: valueBinding,
        route: original.route,
        displayStyle: displayStyle
      }
    };
  }
}

export const MIGRATE_TABLE_WIDGET = defineTransform<LegacyTableWidgetConfig, TableWidgetConfig>({
  match: matches({widgetType: 'table'}),
  transform: tableConfig => performMigration(tableConfig)
});
