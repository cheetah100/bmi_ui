import { defineTransform } from '@bmi/utils';

import { isFormatterConfig, FormatterConfig } from '../formatter';
import overEvery from 'lodash-es/overEvery';


/**
 * Ensures that all Formatter-y bindings have a "bindingType" set. This was an
 * oversight when formatters were first created; they need this so that they can
 * be part of the discriminated union of binding types.
 */
export const ADD_BINDING_TYPE_TO_FORMATTERS = defineTransform<FormatterConfig>({
  match: overEvery<FormatterConfig>(
    isFormatterConfig,
    x => !x['bindingType']
  ),

  transform: result => ({
    ...result,
    bindingType: 'formatter',
  })
});
