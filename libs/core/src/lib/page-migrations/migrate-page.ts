import { transformObject, defineTransform } from '@bmi/utils';
import matches from 'lodash-es/matches';

import { PageConfig } from '../page-config';
import {
  ParameterBinding,
  migrateBinding
} from '../page-data-context/parameter-binding';
import { cloneAndUpdate } from '../util/clone-and-update';

import { TableWidgetConfig } from '../widgets/table/table-config';
import { PageLayoutConfig } from '../widgets/page-layout/page-layout-config';
import { MarkdownWidgetConfig } from '../widgets/markdown/markdown.config';

import { BUTTON_WIDGET_ADD_DEFAULT_CLICK } from './button-widget-add-default-click-handler';
import { ADD_BINDING_TYPE_TO_FORMATTERS } from './add-binding-type-to-formatters';
import { MIGRATE_TABLE_WIDGET } from './table-widget-migration';

// These transformations are applied to pages in the order they are defined here.
// A migration function defines:
//
//   `match`: a predicate that picks which parts of the page structure they are
//            interested in. As the transformObject function explores the page
//            tree, this match predicate will be called with each value. If
//            match returns true, it will call the transform function, otherwise
//            the value will be unchanged
//
//   `transform`: a function that transforms the matched value. This should not
//            mutate the original object (immutability), so it should clone as
//            necessary when updating/replacing the value
//
// Lodash `matches` is a convenient way of defining the predicate here -- this
// will pick out objects that match particular values, e.g. any object that has
// `widgetType: "table"`. This will ignore non-object things like array, null etc.
//
// It may be a good idea to define these transforms in their own files and
// import them here. The defined transform functions can be easily unit tested
// in isolation to ensure they have the desired outcome when fed a range of
// possible inputs.
const activeMigrations = [
  defineTransform<ParameterBinding>({
    match: matches({
      bindingType: 'this-path'
    }),
    transform: binding => migrateBinding(binding)
  }),

  defineTransform<TableWidgetConfig>({
    match: matches({
      widgetType: 'table'
    }),

    transform: config =>
      cloneAndUpdate(config, c => {
        c.fields.columns.forEach(col => (col.width = col.width || '1fr'));
      })
  }),

  // add breadrumbs starter config to pages
  defineTransform<PageLayoutConfig>({
    match: matches({
      widgetType: 'page-layout'
    }),

    transform: config =>
      cloneAndUpdate(config, c => {
        if (!c.fields.breadcrumbs) {
          c.fields.breadcrumbs = {
            widgetType: 'breadcrumbs',
            fields: {
              type: 'breadcrumbs',
              links: [],
              flairs: null
            }
          };
        }
        if (!c.fields.pageNavigation) {
          c.fields.pageNavigation = {
            widgetType: 'nav-list',
            fields: {
              type: 'tabs-h',
              links: [],
              flairs: null
            }
          };
        }
      })
  }),

  defineTransform<MarkdownWidgetConfig>({
    match: matches({
      widgetType: 'markdown'
    }),

    transform: config => cloneAndUpdate(config, c => {
      if (c.fields.value) {
        c.fields = {
          markdown: c.fields.value,
          bindings: {},
        };
      }
    })
  }),

  BUTTON_WIDGET_ADD_DEFAULT_CLICK,
  ADD_BINDING_TYPE_TO_FORMATTERS,
  MIGRATE_TABLE_WIDGET,
];

/**
 * Applies any applicable migrations to a page config
 *
 * This will transform and update a page config by applying migration transforms
 * to it, returning a new PageConfig.
 *
 * Due to the way `transformObject` is implemented, the current implementation
 * may return a different object instance even if no migrations were run, but
 * this behaviour should not be relied on. As always, do not modify the page
 * configs without first creating a clone.
 */
export function migratePage(pageConfig: PageConfig): PageConfig {
  return activeMigrations.reduce(
    (config, migration) => transformObject(config, migration) as PageConfig,
    pageConfig
  );
}
