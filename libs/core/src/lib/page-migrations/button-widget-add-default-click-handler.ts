import { WidgetConfig } from '../widget-config';
import { ButtonWidgetFields } from '../widgets/form-controls/button/button-widget.component';

import { transformObject, defineTransform } from '@bmi/utils';
import { cloneAndUpdate } from '../util/clone-and-update';
import matches from 'lodash-es/matches';
import overEvery from 'lodash-es/overEvery';

export type ButtonWidgetConfig = WidgetConfig<ButtonWidgetFields>;


export const BUTTON_WIDGET_ADD_DEFAULT_CLICK = defineTransform<ButtonWidgetConfig>({
  match: overEvery<ButtonWidgetConfig>(
    matches({widgetType: 'form-control:button'}),
    widget => !widget.fields.onClick
  ),

  transform: config => cloneAndUpdate(config, c => {
    c.fields.onClick = [{type: 'trigger-form-action', action: 'save'}];
  })
});

