import { TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { take } from 'rxjs/operators';

import { gravityTestbed2, makeBoard, BoardConfigBuilder, BoardService, CardData} from '@bmi/gravity-services';

import { Note } from './note';
import { NoteCollection } from './note-collection';
import { NotesBoardService, NoteCardFields } from './notes-board.service';

import isEqual from 'lodash-es/isEqual';


export const TEST_SERVICE_NOTES = makeBoard({
  config: new BoardConfigBuilder('service_notes')
    .addField('service', f => f.required())
    .addField('kpi', f => f.required())
    .addField('detail', f => f.required())
    .addField('extraData'),

  cards: [
    {id: 'N1', fields: {service: 'S1', kpi: 'K1', detail: 'This is a note'}},
    {id: 'N2', fields: {service: 'S1', kpi: 'K2', detail: 'This is a note'}},
    {id: 'N3', fields: {service: 'S1', kpi: 'K2', detail: 'This is another note'}},

    {id: 'N4', fields: {service: 'S2', kpi: 'K1', detail: 'This is a note'}},
    {id: 'N5', fields: {service: 'S2', kpi: 'K1', detail: 'This is a note'}},
    {id: 'N6', fields: {service: 'S2', kpi: 'K1', detail: 'This is a note'}},

    {id: 'N7', fields: {service: 'S3', kpi: 'K1', detail: 'This is a note'}},
    {id: 'N8', fields: {service: 'S3', kpi: 'K2', detail: 'This is a note'}},
  ]
});


function expectSetsEqual<T>(actual: Iterable<T>, expected: Iterable<T>, comparer = isEqual): void {
  const actualArray = Array.from(actual);
  const expectedArray = Array.from(expected);

  const everyActualValueIsExpected = actualArray.every(x => !!expectedArray.find(y => comparer(x, y)));
  const everyExpectedIsPresent = expectedArray.every(x => !!actualArray.find(y => comparer(x, y)));

  if (!everyExpectedIsPresent || !everyActualValueIsExpected) {
    fail(`Sets did not match: Actual: ${actualArray}, Expected: ${expectedArray}`);
  }
}


function subscribeFirst<T>(observable: Observable<T>): T {
  let hasCompleted = false;
  let value: T;
  observable.pipe(
    take(1)
  ).subscribe({
    next: x => value = x,
    complete: () => hasCompleted = true
  });
  expect(hasCompleted).toBeTruthy();
  return value;
}


describe('NotesBoardService', () => {
  let service: NotesBoardService;
  let boardService: BoardService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        gravityTestbed2({boards: [TEST_SERVICE_NOTES]}),
        NotesBoardService
      ]
    });

    service = TestBed.get(NotesBoardService) as NotesBoardService;
    boardService = TestBed.get(BoardService) as BoardService;
  });


  function getNoteCards(ids: string[]) {
    return ids.map(id => subscribeFirst(boardService.fetchById('service_notes', id)));
  }

  describe('Getting a notes collection for some keys', () => {
    let collection: NoteCollection;

    beforeEach(() => {
      collection = subscribeFirst(service.getNotesCollection('service_notes', {service: 'S1', kpi: 'K2'}));
    });

    it('should return a collection', () => expect(collection).toBeTruthy());

    it('should have the right keys in the collection', () => {
      expect(collection.keys).toEqual({service: 'S1', kpi: 'K2'});
    });

    describe('Getting notes', () => {
      let notes: Note[];
      let expectedCards: CardData[];

      beforeEach(() => {
        notes = subscribeFirst(collection.notes);
        expectedCards = getNoteCards(['N2', 'N3']);
      });

      it('should get the right number of notes', () => {
        expect(notes.length).toEqual(2);
      });

      it('should set the note IDs to the card IDs', () => {
        const noteIds = notes.map(n => n.id);
        const cardIds = expectedCards.map(c => c.id);
        expectSetsEqual(noteIds, cardIds);
      });

      it('should set the note text', () => {
        const noteTexts = notes.map(n => ({id: n.id, text: n.noteText}));
        const cardTexts = expectedCards.map(c => ({id: c.id, text: c.fields.detail}));
        expectSetsEqual(noteTexts, cardTexts);
      });
    });
  });

  describe('Getting notes collection for a partial set of keys', () => {
    let collection: NoteCollection;
    let notes: Note[];
    let expectedCards: CardData[];

    beforeEach(() => {
      // These are the notes for all cards with KPI: K1 regardless of service
      collection = subscribeFirst(service.getNotesCollection('service_notes', {kpi: 'K1'}));
      notes = subscribeFirst(collection.notes);
      expectedCards = subscribeFirst(
        boardService.search({
          boardId: 'service_notes',
          conditions: [{ fieldName: 'kpi', operation: 'EQUALTO', value: 'K1' }],
        })
      );
    });

    // For some reason, canAddNotes got removed and the test suite wasn't updated. I still think this is necessary functionality.
    //it('should not support adding notes', () => expect(collection.canAddNotes).toBeFalsy());

    it('should return notes for the expected cards', () => {
      const noteIds = notes.map(n => n.id);
      const cardIds = expectedCards.map(c => c.id);
      expectSetsEqual(noteIds, [...cardIds]);
    });
  });

  describe('Saving Notes', () => {
    let collection: NoteCollection;
    let emittedNotes: Note[][];

    beforeEach(() => {
      emittedNotes = [];
      collection = subscribeFirst(service.getNotesCollection('service_notes', {service: 'S1', kpi: 'K2'}, of(true)));
      collection.notes.subscribe(notes => emittedNotes.push(notes));
    });

    it('should have initially just emitted one Notes value', () => {
      expect(emittedNotes.length).toBe(1);
    });

    describe('Adding a new note', () => {
      let savedNote: Note;

      beforeEach(() => {
        savedNote = undefined;
        collection.saveNote({
          noteText: 'Test note'
        }).subscribe(n => savedNote = n);
      });

      it('should emit the saved note', () => {
        expect(savedNote).toBeTruthy();
      });

      it('should generate a new ID for the saved note', () => {
        expect(savedNote.id).toBeTruthy();
      });

      it('should cause the notes collection to refresh', () => {
        expect(emittedNotes.length).toBe(2);
      });

      it('should include the saved note in the latest notes emission', () => {
        const latestNotes = emittedNotes[emittedNotes.length - 1];
        const addedNote = latestNotes.find(n => n.id === savedNote.id);
        expect(addedNote).toEqual(savedNote);
      });

      describe('The created note card', () => {
        let noteCard: CardData<NoteCardFields>;
        beforeEach(() => {
          noteCard = subscribeFirst(boardService.fetchById('service_notes', savedNote.id)) as CardData<NoteCardFields>;
        });

        it('should exist', () => expect(noteCard).toBeTruthy());
        it('should have the note text set as the detail field', () => expect(noteCard.fields.detail).toEqual('Test note'));
      });
    });


    describe('Editing a note', () => {
      let originalNote: Note;
      let savedNote: Note;

      beforeEach(() => {
        originalNote = subscribeFirst(collection.notes).find(n => n.id === 'N3');
        savedNote = subscribeFirst(collection.saveNote({
          ...originalNote,
          noteText: 'Updated!'
        }));
      });

      it('should have the updated value in the saved note', () => {
        expect(savedNote.noteText).toEqual('Updated!');
      });

      it('should cause the notes collection to refresh', () => {
        expect(emittedNotes.length).toBe(2);
      });

      it('should keep the note ID the same', () => {
        expect(savedNote.id).toBe(originalNote.id);
      });

      it('should include the updated note in the latest notes value', () => {
        const latestNotes = emittedNotes[emittedNotes.length - 1];
        const updatedNote = latestNotes.find(n => n.id === savedNote.id);
        expect(updatedNote).toBeTruthy();
      });
    });


    it('should not save a note with mismatching keys', () => {
      const otherCollection = subscribeFirst(service.getNotesCollection('service_notes', {service: 'S2', kpi: 'K1'}));
      const otherNotes = subscribeFirst(otherCollection.notes);
      const note = otherNotes.find(n => n.id === 'N4');
      expect(note).toBeTruthy();

      const emittedValues: Note[] = [];
      let error: unknown;
      let isComplete = false;

      collection.saveNote({
        ...note,
        noteText: 'Should not update'
      }).subscribe({
        next: x => emittedValues.push(x),
        complete: () => isComplete = true,
        error: (err: unknown) => error = err
      });

      expect(emittedValues).toEqual([]);
      expect(error).toBeTruthy();
      expect(error instanceof Error).toBeTruthy();

    });

    describe('Saving a note that belongs to a different collection i.e. (mismatching keys)', () => {

    });
  });

  describe('getSuggestedKeyFields()', () => {
    let emittedKeyFields: string[];

    beforeEach(() => {
      emittedKeyFields = [];
      service.getSuggestedKeyFields('service_notes').subscribe(value => emittedKeyFields = value);
    });

    it('should produce some results', () => expect(emittedKeyFields).toBeTruthy());

    it('should not suggest the Notes board fields', () => expect(emittedKeyFields).not.toContain('detail'));
    it('should not suggest metadata fields', () => {
      const badFields = ['id', 'phase', 'creator', 'modified', 'title'];
      badFields.forEach(f => expect(emittedKeyFields).not.toContain(f));
    });

    it('should suggest required fields', () => {
      expect(emittedKeyFields).toContain('service');
      expect(emittedKeyFields).toContain('kpi');
    });

    it('should not include optional fields', () => {
      expect(emittedKeyFields).not.toContain('extraData');
    });
  });
});
