import { Injectable } from '@angular/core';

import { Observable, EMPTY, combineLatest, of } from 'rxjs';
import { shareReplay, switchMap, map, take } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

import { ObjectMap } from '@bmi/utils';
import flatten from 'lodash-es/flatten';

import { Binder } from '../page-data-context/binder';
import { ParameterBinding } from '../page-data-context/parameter-binding';
import { Fetcher } from '../page-data-context/fetcher';
import { bindDataMap } from '../util/bind-data-map';
import { distinctUntilNotEqual } from '../util/distinct-until-not-equal';

import { NoteCollection, NoteKeys } from './note-collection';
import { NotesBoardService } from './notes-board.service';

import { GravityConfigService } from '@bmi/gravity-services';


export interface NotesDataSourceConfig {
  type: string;
  board: string;
  keys: ObjectMap<ParameterBinding>;
  visibilityKeys: string[];
}


@Injectable({providedIn: 'root'})
export class NotesDataSourceFactory {
  constructor(
    private noteBoardService: NotesBoardService,
    private configService: GravityConfigService,
    private http: HttpClient
  ) {}

  create(config: NotesDataSourceConfig, binder: Binder): NotesDataSource {
    return new NotesDataSource(config, binder, this.noteBoardService, this.configService, this.http);
  }
}


export class NotesDataSource implements Fetcher {
  readonly state = EMPTY;

  constructor(
    private config: NotesDataSourceConfig,
    private binder: Binder,
    private notesService: NotesBoardService,
    private configService: GravityConfigService,
    private http: HttpClient
  ) {}

  getNotes(keys: ObjectMap<string>): Observable<NoteCollection> {

    return combineLatest([
      bindDataMap(this.binder, {
        ...this.config.keys,
        ...keys
      }),
      this.canManageNotes(),
    ]).pipe(
      take(1),
      distinctUntilNotEqual<ObjectMap<any>>(),
      switchMap(keyData => this.notesService.getNotesCollection(this.config.board, keyData[0], keyData[1])),
      shareReplay(1)
    );
  }

  getNoteKeys() {
    const boardConfig = this.configService.getBoardConfig(this.config.board);
    return Object.entries(this.config.keys)
    .filter(([key, value]) => value === null || value === undefined)
    .map(([key, value]) => {
      return {
        field: key,
        optionlist: (boardConfig && boardConfig.fields[key]) ? boardConfig.fields[key].optionlist : null
      };
    });
  }

  canManageNotes(): Observable<boolean> {
    return this.getUserId().pipe(
      switchMap(loggedInUserId => {
        return this.fetchAllAuthorizedUsers().pipe(
          map((authorizedGroups: any[]) => {
            let authorizedIds = [];
            authorizedIds = flatten(authorizedGroups);
            return authorizedIds.indexOf(loggedInUserId) > -1;
          })
        );
      })
    );
  }

  fetchAllAuthorizedUsers(): (Observable<any[]>) {
    const source: Observable<any[]>[] = [];
    if (this.config.visibilityKeys) {
      this.config.visibilityKeys.forEach((visibilityKey: string) => {
        source.push(this.binder.bind(visibilityKey));
        return combineLatest(source);
      });
    return of([]);
  }
}

  /**
   * TODO: Replace this with an api
   * from the user repository from @bmi/gravity-services
   */
  getUserId(): Observable<string> {
    return this.http.get('/gravity/spring/user/current').pipe(
      map(user => user['id'])
    );
    //return of('stevboyl');
    //return of('demurray');
  }

}
