import { Note } from './note';
import { Observable } from 'rxjs';


export interface NoteKeys {
  readonly [key: string]: any;
}



/**
 * A collection of notes, matching a particular set of keys
 */
export interface NoteCollection {
  keys: NoteKeys;
  notes: Observable<Note[]>;

  /**
   * Can notes be modified? This may be affected by permissions, as well
   */
  canModifyNotes: Observable<boolean>;

  /**
   * Adds or updates a note.
   *
   * If the note object has an `id`, and that id exists in the note board, then
   * the card should be updated, setting the
   */
  saveNote(note: Partial<Note>): Observable<Note>;
}


