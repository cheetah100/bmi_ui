import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, publishLast, refCount, shareReplay, switchMap, tap } from 'rxjs/operators';

import { BoardService, CardCacheService, GravityApiCondition, CardData } from '@bmi/gravity-services';
import { ObjectMap } from '@bmi/utils';

import cloneDeep from 'lodash-es/cloneDeep';

import { objectToConditions } from '../util/condition-utils';
import { distinctUntilNotEqual } from '../util/distinct-until-not-equal';

import { Note } from './note';
import { NoteCollection } from './note-collection';


export interface NoteCardFields {
  detail: string;
}


const NOTES_BOARD_FIELDS = [
  'detail',
  'quarter'
  // TODO: author, date etc.
];


@Injectable({providedIn: 'root'})
export class NotesBoardService {

  constructor (
    private boardService: BoardService,
    private cardCacheService: CardCacheService,
  ) {}

  /**
   * Gets a collection of notes from a notes board.
   *
   * The collection of notes is identified by 'key' fields on the notes cards,
   * which depend on the data the notes are being tracked against -- ASG metric
   * notes may use asgId + kpi as the keys to identify the notes.
   *
   * If these fields adequately cover the required fields for a note card, (and
   * permissions allow), the note collection can also be used to save new cards
   * to the collection. This will fill out the key fields with the values that
   * were bound to this collection.
   */
  public getNotesCollection(boardId: string, noteKeys: ObjectMap<any>, canModify?: Observable<boolean>): Observable<NoteCollection> {
    // This represents a collection of notes matching a set of keys fields --
    // these are the fields that are used as a filter to identify a set of
    // related notes (e.g. serviceId + kpi).
    //
    // This notes collection effectively 'binds' those keys -- everything in the
    // collection matches those keys, and calling saveNote() on the collection
    // should set the keys on the new note
    //
    // However, it's possible that the supplied noteKeys are not specific enough
    // e.g. they don't bind all of the required fields; e.g. they might select
    // all notes by serviceId, regardless of KPI. This might be a useful
    // behaviour, but in those situations, we shouldn't allow adding new notes,
    // because the required fields aren't met (editing should be okay though)
    //
    //    TODO: do we want this function to automatically inspect board template
    //    and decide if adding notes is possible?
    //
    // Edit permissions may also determine whether the user can add/edit notes
    // within the collection. The basic permission check can be added here, but
    // we should add more parameters to this function to allow editing to be
    // turned off.
    //
    //    TODO: do we want this function to check if the user can edit the
    //    board?
    //
    // Adding permission/template checks would require a call to
    // boardService.getConfig(boardId)
    return of(<NoteCollection>{
      canModifyNotes: canModify || of(false),
      keys: {...noteKeys},
      notes: this.getNotesMatchingKeyFields(boardId, noteKeys),
      saveNote: note => this.saveNote(boardId, noteKeys, note)
    });
  }

  /**
   * Gets the suggested 'key' fields for a notes board.
   *
   * These are required fields that aren't part of the notes themselves -- e.g.
   * the service_id or kpi that is used to identify a notes grouping.
   */
  public getSuggestedKeyFields(boardId: string): Observable<string[]> {
    return this.boardService.getConfig(boardId).pipe(
      map(config => {
        // Get the required fields on the board template, sans any Note board
        // fields -- we want to find the other data that might be used to
        // identify a notes group.
        return Object.values(config.fields)
          .filter(field => !field.isMetadata && field.required && !NOTES_BOARD_FIELDS.includes(field.id))
          .map(f => f.id);
      }),
      distinctUntilNotEqual(),
    );
  }


  private getNotesMatchingKeyFields(boardId: string, noteKeys: ObjectMap<any>): Observable<Note[]> {
    return this.cardCacheService.search(this.getNoteSearchParams(boardId, noteKeys)).pipe(
      map((cards: CardData<NoteCardFields>[]) => cards.map(c => this.makeNote(c))),
      shareReplay(1)
    );
  }

  private getNoteSearchParams(boardId: string, keys: ObjectMap<any>) {
    return {
      boardId: boardId,
      conditions: objectToConditions(keys)
    };
  }

  private makeNote(card: CardData<NoteCardFields>): Note {
    return {
      id: card.id,
      noteText: card.fields.detail,

      // TODO: this Gravity metadata shouldn't be relied on here. We should
      // explicitly gather the created time and author as this is _data_ that
      // belongs to the notes (as integrations, migrations, rules may mess up
      // the card metadata.)
      addedAt: card.created,
      author: card.creator,
    };
  }

  private saveNote(boardId: string, noteKeys: ObjectMap<any>, note: Partial<Note>): Observable<Note> {
    // TODO: this should create or update a note card.
    //
    // If the note has no id, it should create a new note. If an id is
    // specified, it should fetch the existing note and merge in the updated
    // fields from the (partial) note.
    //
    // (when editing an existing note, maybe we should verify the keys match the
    // card in Gravity?)
    //
    // Once the update has been completed, we should invalidate the notes board
    // in the cache so that new data gets fetched for any views that are
    // watching the notes collections
    const isNewNote = !note.id;

    const source = isNewNote ?
      this.createNewNoteCard(boardId, noteKeys, note) :
      this.updateExistingNoteCard(boardId, noteKeys, note);

    return source.pipe(
      switchMap(card => this.boardService.saveCard(boardId, card)),
      map(card => this.makeNote(card as CardData<NoteCardFields>)),
      tap(() => this.cardCacheService.refreshSearch(this.getNoteSearchParams(boardId, noteKeys))),
      publishLast(),
      refCount()
    );
  }

  private createNewNoteCard(boardId: string, noteKey: ObjectMap<any>, note: Partial<Note>): Observable<CardData<NoteCardFields>> {
    return of({
      id: null,
      board: boardId,
      phase: null,
      fields: {
        ...noteKey,
        detail: note.noteText
      }
    });
  }

  private updateExistingNoteCard(boardId: string, noteKeys: ObjectMap<any>, note: Partial<Note>): Observable<CardData<NoteCardFields>> {
    return this.boardService.fetchById(boardId, note.id).pipe(
      map((card: CardData<NoteCardFields>) => {
        const matchesAllKeys = Object.entries(noteKeys).every(([key, value]) => card.fields[key] === value);
        if (!matchesAllKeys) {
          throw new Error('Cannot update Note: not all key fields match!');
        }

        const clonedCard = cloneDeep(card);
        clonedCard.fields.detail = note.noteText || '';
        return clonedCard;
      })
    );
  }
}
