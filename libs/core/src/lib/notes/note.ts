
/**
 * Represents a note entry.
 */
export interface Note {
  /**
   * The note ID
   *
   * If defined, this is probably a card ID allowing the note to be updated.
   */
  readonly id?: string;

  /**
   * The USERID of the note author
   */
  readonly author: string;

  /**
   * Time the note was added
   */
  readonly addedAt: number;

  /**
   * The note body.
   */
  readonly noteText: string;
}
