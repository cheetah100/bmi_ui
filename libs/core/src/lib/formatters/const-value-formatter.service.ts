import { Injectable } from '@angular/core';
import { of } from 'rxjs';

import { Formatter, FormatterConfig } from '../formatter';


export interface ConstValueFormatterConfig<T = unknown> extends FormatterConfig {
  value: T;
}


@Injectable()
export class ConstValueFormatterService implements Formatter<ConstValueFormatterConfig> {
  format(config: ConstValueFormatterConfig) {
    return of(config.value);
  }
}
