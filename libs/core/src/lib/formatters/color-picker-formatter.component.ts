import { Component, OnInit } from '@angular/core';
import { TypedFormControl } from '@bmi/ui';

import { FormatterEditorContext } from '../formatter';
import { ParameterBinding } from '../page-data-context/parameter-binding';

import { ConstValueFormatterConfig } from './const-value-formatter.service';

@Component({
  template: `<ui-color-dropdown [formControl]="control"></ui-color-dropdown>`,
})
export class ColorPickerFormatterComponent implements OnInit {
  readonly control = new TypedFormControl<string>(null);

  constructor(private context: FormatterEditorContext<ConstValueFormatterConfig<string>>) {}

  ngOnInit() {
    this.context.formatterConfig.subscribe(config => this.control.patchValue(config ? config.value : null, {emitEvent: false}));
    this.control.valueChanges.subscribe(value => this.context.setFormatterConfig({
      value,
      formatterType: 'color-picker'
    }));
  }
}
