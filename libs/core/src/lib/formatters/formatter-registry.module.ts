import { NgModule, Type, Provider } from '@angular/core';

import { Formatter, FORMATTER_SERVICES, FORMATTER_TYPE_MAPPING } from '../formatter';

import { ConstValueFormatterService } from './const-value-formatter.service';
import { NumberFormatterService } from './number-formatter/number-formatter.service';
import { ColorZoneFormatterService } from './color-zone/color-zone-formatter.service';
import { TemplateStringFormatterService } from './template-string/template-string-formatter.service';
import { QuartersRangeFormatterService } from './quarters-range/quarters-range-formatter.service';
import { CurrentUserIdFormatterService } from './current-meta/current-user-formatter.service';
import { ColorNameFormatterService } from './color-name/color-name-formatter.service';
import { CurrentTimeFormatterService } from './current-meta/current-time-formatter.service';
import { FiscalMonthFormatterService } from './fiscal-month-formatter.service';
import { DateTimeFormatterService } from './date-time-formatter.service';

export function defineFormatter(formatterType: string, formatterClass: Type<Formatter>): Provider[] {
  return [
    formatterClass,
    {
      provide: FORMATTER_SERVICES,
      multi: true,
      useExisting: formatterClass,
    },
    {
      provide: FORMATTER_TYPE_MAPPING,
      multi: true,
      useValue: {
        formatterType: formatterType,
        formatterClass: formatterClass
      }
    }
  ];
}


@NgModule({
  providers: [
    defineFormatter('color-picker', ConstValueFormatterService),
    defineFormatter('optionlist', ConstValueFormatterService),
    defineFormatter('number-format', NumberFormatterService),
    defineFormatter('color-zone', ColorZoneFormatterService),
    defineFormatter('template-string', TemplateStringFormatterService),
    defineFormatter('quarters-range', QuartersRangeFormatterService),
    defineFormatter('current-user-id', CurrentUserIdFormatterService),
    defineFormatter('color-name', ColorNameFormatterService),
    defineFormatter('fiscal-month', FiscalMonthFormatterService),
    defineFormatter('current-time', CurrentTimeFormatterService),
    defineFormatter('datetime', DateTimeFormatterService),
  ]
})
export class FormatterRegistryModule { }
