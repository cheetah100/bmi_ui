import { Component, OnInit } from '@angular/core';
import { TypedFormControl, TypedFormGroup } from '@bmi/ui';
import { FormatterEditorContext } from '../../formatter';
import { ColorNameFormatterConfig } from './color-name-formatter.service';

@Component({
  template: `
<ng-container [formGroup]="form">
  <bmi-binding-picker formControlName="input"></bmi-binding-picker>
</ng-container>`,
})
export class ColorNameFormatterComponent implements OnInit {
  readonly form = new TypedFormGroup<ColorNameFormatterConfig>({
    formatterType: new TypedFormControl<string>('color-name'),
    input: new TypedFormControl(null),
  });

  constructor(private context: FormatterEditorContext<ColorNameFormatterConfig>) { }

  ngOnInit() {
    this.context.formatterConfig.subscribe(config => this.form.patchValue(config, { emitEvent: false }));
    this.form.valueChanges.subscribe(config => this.context.setFormatterConfig(config));
  }
}
