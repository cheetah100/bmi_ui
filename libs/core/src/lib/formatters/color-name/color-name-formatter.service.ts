import { Injectable } from '@angular/core';

import { Formatter, FormatterConfig } from '../../formatter';
import { ParameterBinding } from '../../page-data-context/parameter-binding';
import { Binder } from '../../page-data-context/binder';
import { map } from 'rxjs/operators';
import { ObjectMap } from '@bmi/utils';
import { COLORS, ColorDefinition } from '@bmi/ui';


export interface ColorNameFormatterConfig extends FormatterConfig {
  input: ParameterBinding;
}

const mappings: ObjectMap<ColorDefinition> = {
  green: COLORS.success,
  yellow: COLORS.warningAlt,
  red: COLORS.danger,
  tbd: COLORS.gray500,
};

@Injectable()
export class ColorNameFormatterService implements Formatter<ColorNameFormatterConfig> {

  format(config: ColorNameFormatterConfig, binder: Binder) {
    return binder.bind(config.input).pipe(
      map(input => {
        if (mappings[input]) {
          return mappings[input].cssColor;
        }
        return input;
      })
    );
  }
}
