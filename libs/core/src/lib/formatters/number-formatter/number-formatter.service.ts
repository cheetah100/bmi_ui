import { Injectable } from '@angular/core';

import { map } from 'rxjs/operators';

import { ParameterBinding } from '../../page-data-context/parameter-binding';
import { Binder } from '../../page-data-context/binder';
import { Formatter, FormatterConfig } from '../../formatter';
import { formatNumber, NUMBER_FORMAT_STYLES } from '../../util/format-number';

import { Option } from '@bmi/ui';


export interface NumberFormatterConfig extends FormatterConfig {
  input: ParameterBinding;
  style: string;
}


@Injectable()
export class NumberFormatterService implements Formatter<NumberFormatterConfig> {
  private _formatOptions = NUMBER_FORMAT_STYLES.map(fs => <Option>{value: fs.id, text: fs.name});

  format(config: NumberFormatterConfig, binder: Binder) {
    return binder.bind(config.input).pipe(
      map(value => parseFloat(value as string)),
      map(value => formatNumber(value, config.style, '-'))
    );
  }

  getFormatOptions(): Option[] {
    return this._formatOptions;
  }
}
