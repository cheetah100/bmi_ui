import { Component, OnInit } from '@angular/core';
import { TypedFormGroup, TypedFormControl } from '@bmi/ui';

import { FormatterEditorContext } from '../../formatter';
import { ParameterBinding } from '../../page-data-context/parameter-binding';

import { NumberFormatterService, NumberFormatterConfig } from './number-formatter.service';

@Component({
  template: `
    <ng-container [formGroup]="form">
      <bmi-binding-picker formControlName="input"></bmi-binding-picker>

      <ui-form-field label="Number Format">
        <ui-select formControlName="style" [options]="styleOptions"></ui-select>
      </ui-form-field>
    </ng-container>
  `,
})
export class NumberFormatterComponent implements OnInit {
  readonly form = new TypedFormGroup<NumberFormatterConfig>({
    formatterType: new TypedFormControl('number-format'),
    input: new TypedFormControl(null),
    style: new TypedFormControl('default')
  });

  styleOptions = this.numberFormatterService.getFormatOptions();

  constructor(
    private context: FormatterEditorContext<NumberFormatterConfig>,
    private numberFormatterService: NumberFormatterService
  ) {}

  ngOnInit() {
    this.context.formatterConfig.subscribe(config => {
      if (config) {
        this.form.patchValue(config, {emitEvent: false});
      } else {
        this.form.reset({style: 'default'}, {emitEvent: false});
      }
    });

    this.form.valueChanges.subscribe(config => this.context.setFormatterConfig({
      ...config,
      formatterType: 'number-format'
    }));
  }
}
