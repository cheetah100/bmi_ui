import { Component, OnInit } from '@angular/core';
import { TypedFormControl } from '@bmi/ui';

import { FormatterEditorContext } from '../formatter';

@Component({
  template: `<input type="text" [formControl]="control">`,
  styles: [`
    input { width: 100%; }
  `]
})
export class TextInputFormatterComponent implements OnInit {
  readonly control = new TypedFormControl<string>(null);

  constructor(private context: FormatterEditorContext<string>) {}

  ngOnInit() {
    this.context.formatterConfig.subscribe(text => this.control.patchValue(text, {emitEvent: false}));
    this.control.valueChanges.subscribe(text => this.context.setFormatterConfig(text));
  }
}
