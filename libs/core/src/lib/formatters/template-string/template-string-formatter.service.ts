import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { ObjectMap } from '@bmi/utils';
import { FormatterConfig, Formatter } from '../../formatter';
import { Binder } from '../../page-data-context/binder';
import { ParameterBinding, isParameterBinding } from '../../page-data-context/parameter-binding';
import { bindDataMap } from '../../util/bind-data-map';
import { mustacheDown } from '../../util/mustachedown';

export interface TemplateStringFormatterConfig extends FormatterConfig {
  value: string;
  data: ObjectMap<ParameterBinding>;
}

@Injectable()
export class TemplateStringFormatterService implements Formatter<TemplateStringFormatterConfig> {
  format(config: TemplateStringFormatterConfig, binder: Binder) {
    return bindDataMap(binder, config.data || {}).pipe(
      map(values => mustacheDown(config.value, {
        context: values,
        formatMarkdown: false,
        handleError: (error, text) => {
          console.warn('Template String Formatter: ', error);
          return text;
        }
      }))
    );
  }
}
