import { Component, OnInit } from '@angular/core';
import { TypedFormGroup, TypedFormControl } from '@bmi/ui';

import { FormatterEditorContext } from '../../formatter';
import { ParameterBinding } from '../../page-data-context/parameter-binding';

import { TemplateStringFormatterConfig } from './template-string-formatter.service';


function prepareFormatterConfig(config: string | TemplateStringFormatterConfig): TemplateStringFormatterConfig {
  if (typeof config === 'string') {
    return {
      formatterType: 'template-string',
      value: config || '',
      data: {}
    };
  } else {
    return config;
  }
}

@Component({
  template: `
    <ng-container [formGroup]="form">
      <input class="template-string__template-input" formControlName="value">

      <ui-collapsible-section label="Template Values">
        <bmi-data-map-editor formControlName="data"></bmi-data-map-editor>
      </ui-collapsible-section>

    </ng-container>
  `,

  styles: [`
    .template-string__template-input {
      width: 100%;
    }
  `]
})
export class TemplateStringFormatterComponent implements OnInit {
  readonly form = new TypedFormGroup<TemplateStringFormatterConfig>({
    formatterType: new TypedFormControl('template-string'),
    value: new TypedFormControl(''),
    data: new TypedFormControl({})
  });

  constructor(
    private context: FormatterEditorContext<string | TemplateStringFormatterConfig>,
  ) {}

  ngOnInit() {
    this.context.formatterConfig.subscribe(config => {
      if (config) {
        this.form.patchValue(prepareFormatterConfig(config), {emitEvent: false});
      } else {
        this.form.reset(undefined, {emitEvent: false});
      }
    });

    this.form.valueChanges.subscribe(config => this.context.setFormatterConfig({
      ...config,
      formatterType: 'template-string'
    }));
  }
}
