import { NgModule, Provider } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { GravityUiModule } from '@bmi/gravity-services';
import { SharedUiModule } from '@bmi/ui';
import { BmiChartsEditorModule } from '@bmi/legacy-charts';
import { BmiCoreEditorComponentsModule } from '../editor-components/bmi-core-editor-components.module';
import { FORMATTER_METADATA, FormatterMetadata } from '../formatter';

import { TextInputFormatterComponent } from './text-input-formatter.component';
import { BindingPickerFormatterComponent } from './binding-picker-formatter';
import { OptionlistFormatterComponent, OptionlistFormatterHints } from './optionlist-formatter.component';
import { NumberFormatterComponent } from './number-formatter/number-formatter.component';
import { ColorPickerFormatterComponent } from './color-picker-formatter.component';
import { ColorZoneFormatterComponent } from './color-zone/color-zone-formatter.component';
import { TemplateStringFormatterComponent } from './template-string/template-string-formatter.component';

import { QuartersRangeFormatterComponent } from './quarters-range/quarters-range-formatter.component';
// import { PivotValueFormatterComponent } from './pivot-value/pivot-value-formatter.component';
import { ColorNameFormatterComponent } from './color-name/color-name-formatter.component';
import { FiscalMonthFormatterComponent } from './fiscal-month-formatter.component';
import { DateTimeFormatterComponent } from './date-time-formatter.component';

export function defineFormatterEditor(metadata: FormatterMetadata): Provider {
  return {
    provide: FORMATTER_METADATA,
    multi: true,
    useValue: metadata
  };
}


export function hasOptionlistHints(hints: OptionlistFormatterHints) {
  return !!hints.optionBoard || !!hints.options || !!hints.optionGroups;
}


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedUiModule,
    GravityUiModule,
    RouterModule,
    BmiCoreEditorComponentsModule,

    // Used for color zone control
    BmiChartsEditorModule
  ],

  declarations: [
    BindingPickerFormatterComponent,
    TextInputFormatterComponent,
    OptionlistFormatterComponent,
    NumberFormatterComponent,
    ColorPickerFormatterComponent,
    ColorZoneFormatterComponent,
    TemplateStringFormatterComponent,
    QuartersRangeFormatterComponent,
    // PivotValueFormatterComponent,
    ColorNameFormatterComponent,
    FiscalMonthFormatterComponent,
    DateTimeFormatterComponent,
  ],

  providers: [
    defineFormatterEditor({
      formatterType: 'text',
      name: 'Text',
      component: TextInputFormatterComponent,
      defaultConfig: '',
      supportsText: true,
      supportsNull: true,
    }),

    defineFormatterEditor({
      formatterType: 'data-binding',
      name: 'Data Binding',
      component: BindingPickerFormatterComponent,
      defaultConfig: null,
      supportsText: true,
      supportsNull: true,
    }),

    defineFormatterEditor({
      formatterType: 'optionlist',
      name: 'Optionlist',
      component: OptionlistFormatterComponent,
      shouldShowFormatter: hasOptionlistHints,
      defaultConfig: null,
      supportsText: true,
      supportsNull: true,
    }),

    defineFormatterEditor({
      formatterType: 'number-format',
      name: 'Formatted Number',
      component: NumberFormatterComponent,
      defaultConfig: {
        input: null,
        style: 'default'
      }
    }),

    defineFormatterEditor({
      formatterType: 'datetime',
      name: 'Formatted Date',
      component: DateTimeFormatterComponent,
      defaultConfig: {
        input: null,
        style: 'datetime'
      }
    }),

    defineFormatterEditor({
      formatterType: 'color-picker',
      name: 'Color Picker',
      component: ColorPickerFormatterComponent,
      supportsNull: true,
    }),

    defineFormatterEditor({
      formatterType: 'color-zone',
      name: 'Color Zones',
      component: ColorZoneFormatterComponent,
      supportsNull: true,
    }),

    defineFormatterEditor({
      formatterType: 'template-string',
      name: 'Template String',
      component: TemplateStringFormatterComponent,
      supportsNull: true,
      supportsText: true,
      defaultConfig: {
        value: '',
        data: {}
      }
    }),

    defineFormatterEditor({
      formatterType: 'quarters-range',
      name: 'Quarters Range',
      component: QuartersRangeFormatterComponent,
      supportsNull: true,
    }),

    defineFormatterEditor({
      formatterType: 'fiscal-month',
      name: 'Fiscal Month',
      component: FiscalMonthFormatterComponent,
      supportsNull: false,
      defaultConfig: {},
    }),

    defineFormatterEditor({
      formatterType: 'current-time',
      name: 'Current Time',
      component: null,
      supportsNull: false,
      defaultConfig: {},
    }),

    defineFormatterEditor({
      formatterType: 'current-user-id',
      name: 'Current User ID',
      component: null,
      supportsNull: false,
      defaultConfig: {},
    }),
    // defineFormatterEditor({
    //   formatterType: 'pivot-value',
    //   name: 'Value from Pivot',
    //   component: PivotValueFormatterComponent,
    // }),

    defineFormatterEditor({
      formatterType: 'color-name',
      name: 'Color Name',
      component: ColorNameFormatterComponent,
      supportsNull: true,
      supportsText: true,
    }),
  ]
})
export class FormatterEditorRegistryModule { }
