import { Component, OnInit } from '@angular/core';
import { of } from 'rxjs';
import { switchMap, shareReplay } from 'rxjs/operators';
import { TypedFormControl, Option, OptionGroup } from '@bmi/ui';

import { FormatterEditorContext } from '../formatter';
import { ParameterBinding } from '../page-data-context/parameter-binding';
import { OptionlistService, optionsToOptionGroups } from '../services/optionlist.service';

import { ConstValueFormatterConfig } from './const-value-formatter.service';


/**
 * Supported hints for the optionlist formatter.
 *
 * The purpose here is to hint which optionlist should be used to pick from.
 * This can be provided based on context via the `hints` input on the formatter
 * picker.
 *
 * While multiple methods are supported, only one should be used at a time!
 */
export interface OptionlistFormatterHints {
  options?: Option[];
  optionGroups?: OptionGroup[];
  optionBoard?: string;
}

@Component({
  template: `<ui-select [formControl]="control" [optionGroups]="optionGroups | async"></ui-select>`,
})
export class OptionlistFormatterComponent implements OnInit {
  readonly control = new TypedFormControl<string>(null);

  readonly optionGroups = this.context.hints.pipe(
    switchMap(hints => {
      if (!!hints.optionBoard) {
        return this.optionlistService.getOptionGroups(hints.optionBoard);
      } else if (!!hints.optionGroups) {
        return of(hints.optionGroups);
      } else if (!!hints.options) {
        return of(optionsToOptionGroups(hints.options));
      } else {
        return of(<OptionGroup[]>[]);
      }
    }),
    shareReplay(1)
  );

  constructor(
    private context: FormatterEditorContext<string, OptionlistFormatterHints>,
    private optionlistService: OptionlistService
  ) {}

  ngOnInit() {
    this.context.formatterConfig.subscribe(value => this.control.patchValue(value ? value : null, {emitEvent: false}));
    this.control.valueChanges.subscribe(value => this.context.setFormatterConfig(value));
  }
}
