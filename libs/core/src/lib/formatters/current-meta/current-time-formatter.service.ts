import { Injectable } from '@angular/core';
import { Formatter } from '../../formatter';

import { of, Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class CurrentTimeFormatterService implements Formatter {

  format(): Observable<number> {
    const date = new Date();
    return of(Math.round(date.getTime() / 1000));
  }
}
