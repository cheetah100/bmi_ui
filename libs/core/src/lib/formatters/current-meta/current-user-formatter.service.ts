import { Injectable } from '@angular/core';
import { Formatter } from '../../formatter';

import { CurrentUserService } from '../../services/current-user.service';

@Injectable({providedIn: 'root'})
export class CurrentUserIdFormatterService implements Formatter {
  constructor(
    private currentUserService: CurrentUserService
  ) {}

  format() {
    return this.currentUserService.userid;
  }
}
