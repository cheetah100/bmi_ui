import { Component, OnInit } from '@angular/core';
import { TypedFormGroup, TypedFormControl } from '@bmi/ui';

import { debounceTime } from 'rxjs/operators';
import { distinctUntilNotEqual } from '../../util/distinct-until-not-equal';

import { FormatterEditorContext } from '../../formatter';
import { ParameterBinding } from '../../page-data-context/parameter-binding';

import { COLORS } from '@bmi/ui';
import { ColorZoneFormatterService, ColorZoneFormatterConfig } from './color-zone-formatter.service';

@Component({
  template: `
    <ng-container [formGroup]="form">
      <bmi-binding-picker formControlName="input"></bmi-binding-picker>

      <ui-form-field label="Color Zones">
        <bmi-color-zones-editor *ngIf="hasValidZonesSetUp" formControlName="zones"></bmi-color-zones-editor>
        <button *ngIf="!hasValidZonesSetUp" class="btn btn--link" (click)="initColorZones()">
          <ui-icon icon="cui-plus" fixedWidth="true"></ui-icon>
          Add color zones
        </button>
      </ui-form-field>
    </ng-container>
  `,
})
export class ColorZoneFormatterComponent implements OnInit {

  readonly form = new TypedFormGroup<ColorZoneFormatterConfig>({
    formatterType: new TypedFormControl('color-zone'),
    input: new TypedFormControl(null),
    zones: new TypedFormControl([])
  });

  get hasValidZonesSetUp() {
    const zones = this.form.value && this.form.value.zones;
    return !!zones && zones.length > 0;
  }

  constructor(
    private context: FormatterEditorContext<ColorZoneFormatterConfig>,
  ) { }

  ngOnInit() {
    this.context.formatterConfig.subscribe(config => {
      if (config) {
        this.form.patchValue(config, { emitEvent: false });
      } else {
        this.form.reset(undefined, { emitEvent: false });
      }
    });

    this.form.valueChanges.pipe(
      // This is here because the Zones control is a badly behaved component
      // that spams updates and will cause an infinite loop.
      debounceTime(50),
      distinctUntilNotEqual()
    ).subscribe(config => this.context.setFormatterConfig({
      ...config,
      formatterType: 'color-zone'
    }));
  }

  initColorZones() {
    this.form.patchValue({
      zones: [{
        value: 25,
        color: COLORS.statusRed.cssColor
      }, {
        value: 75,
        color: COLORS.statusYellow.cssColor
      }, {
        value: null,
        color: COLORS.statusGreen.cssColor
      }]
    })
  }
}
