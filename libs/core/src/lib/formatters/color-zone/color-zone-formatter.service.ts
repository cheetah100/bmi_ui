import { Injectable } from '@angular/core';

import { map } from 'rxjs/operators';

import { ParameterBinding } from '../../page-data-context/parameter-binding';
import { Binder } from '../../page-data-context/binder';
import { Formatter, FormatterConfig } from '../../formatter';

import { ColorZone, evaluateColorZone } from '../../util/color-zone';


export interface ColorZoneFormatterConfig extends FormatterConfig {
  input: ParameterBinding;
  zones: ColorZone[];
}


@Injectable()
export class ColorZoneFormatterService implements Formatter<ColorZoneFormatterConfig> {

  format(config: ColorZoneFormatterConfig, binder: Binder) {
    return binder.bind(config.input).pipe(
      map(value => parseFloat(value as string)),
      map(value => this.getColorForValue(config, value))
    );
  }

  private getColorForValue(config: ColorZoneFormatterConfig, value: number): string {
    return evaluateColorZone(config.zones, value, '#000000');
  }
}
