import { Component, OnInit } from '@angular/core';
import { defer, Observable } from 'rxjs';
import { TypedFormControl, OptionGroup, TypedFormGroup } from '@bmi/ui';

import { FormatterEditorContext, FormatterConfig } from '../formatter';
import { OptionlistService } from '../services/optionlist.service';

interface FiscalMonthFormatterConfig extends FormatterConfig {
  value: string;
}

@Component({
  template: `
<ui-select [formControl]="control" [optionGroups]="optionGroups | async"></ui-select>
<p>(Leave unselected to use current month)</p>
  `,
})
export class FiscalMonthFormatterComponent implements OnInit {

  readonly control = new TypedFormControl<string>(null);
  readonly form = new TypedFormGroup<FiscalMonthFormatterConfig>({
    formatterType: new TypedFormControl('fiscal-month'),
    value: this.control,
  });
  optionGroups = defer(() => this.optionlistService.getOptionGroups('fiscal_months'));

  constructor(
    private context: FormatterEditorContext<FiscalMonthFormatterConfig>,
    private optionlistService: OptionlistService
  ) { }

  ngOnInit() {
    this.context.formatterConfig.subscribe(value => this.form.patchValue(value, {emitEvent: false}));
    this.form.valueChanges.subscribe(value => this.context.setFormatterConfig(value));
  }
}
