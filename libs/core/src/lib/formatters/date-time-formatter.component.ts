import { Component, OnInit } from '@angular/core';
import { TypedFormControl, TypedFormGroup, Option } from '@bmi/ui';

import { FormatterEditorContext } from '../formatter';
import { DateTimeFormatterConfig } from './date-time-formatter.service';

@Component({
  template: `
    <ng-container [formGroup]="form">
      <bmi-binding-picker formControlName="input"></bmi-binding-picker>
      <ui-select [options]="dateStyles" formControlName="style"></ui-select>
    </ng-container>
  `,
})
export class DateTimeFormatterComponent implements OnInit {
  constructor(private context: FormatterEditorContext<DateTimeFormatterConfig>) {}

  dateStyles: Option[] = [
    {value: 'date', text: 'Date'},
    {value: 'datetime', text: 'Date and Time'},
    {value: 'relative', text: 'Relative to now'},
  ];

  form = new TypedFormGroup<DateTimeFormatterConfig>({
    formatterType: new TypedFormControl(),
    input: new TypedFormControl(),
    style: new TypedFormControl(),
  });


  ngOnInit() {
    this.context.formatterConfig.subscribe(config => this.form.patchValue(config, {emitEvent: false}));
    this.form.valueChanges.subscribe(value => this.context.setFormatterConfig(value));
  }
}
