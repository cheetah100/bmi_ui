import { Injectable } from "@angular/core";
import { formatDate } from '@bmi/utils';
import { map } from 'rxjs/operators';
import { Formatter, FormatterConfig, ParameterBinding } from '../formatter';
import { Binder } from '../page-data-context/binder';


export interface DateTimeFormatterConfig extends FormatterConfig {
  input: ParameterBinding;
  style: 'date' | 'longDate' | 'datetime' | 'relative';
}


@Injectable({providedIn: 'root'})
export class DateTimeFormatterService implements Formatter<DateTimeFormatterConfig> {
  format(config: FormatterConfig, binder: Binder) {
    return binder.bind(config.input).pipe(
      map(value => (typeof value === 'number' || value instanceof Date) ? formatDate(value, config.style ?? 'date') : value)
    )
  }
}
