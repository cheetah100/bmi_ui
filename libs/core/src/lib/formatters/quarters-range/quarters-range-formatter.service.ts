import { Injectable } from '@angular/core';

import { combineLatest, of, Observable } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';

import { ParameterBinding } from '../../page-data-context/parameter-binding';
import { Binder } from '../../page-data-context/binder';
import { Formatter, FormatterConfig } from '../../formatter';
import { bindDataMap } from '../../util/bind-data-map';

import { Option } from '@bmi/ui';
import { createMapFrom } from '@bmi/utils';
import { GravityTimelineService, Quarter } from '@bmi/gravity-services';


export interface QuartersRangeFormatterConfig extends FormatterConfig {
  mode: string;
  startQuarter?: ParameterBinding;
  endQuarter?: ParameterBinding;
  numberOfQuarters?: number;
}


@Injectable()
export class QuartersRangeFormatterService implements Formatter<QuartersRangeFormatterConfig> {
  constructor(
    private timelineService: GravityTimelineService
  ) { }

  format(config: QuartersRangeFormatterConfig, binder: Binder) {
    if (config.mode === 'range') {
      return this.formatQuarterRange(config, binder);
    } else if (config.mode === 'preceding') {
      return this.formatPrecedingQuarters(config, binder);
    } else if (config.mode === 'preceding-current') {
      return this.formatPrecedingCurrentQuarters(config, binder);
    } else if ((config.mode) === 'current-quarter') {
      return this.formatCurrentQuarter();
    } else if ((config.mode) === 'previous-quarter') {
      return this.formatPreviousQuarter();
    } else {
      return of([]);
    }
  }
  
  private formatQuarterRange(config: QuartersRangeFormatterConfig, binder: Binder) {
    return bindDataMap(binder, {
      startQuarter: config.startQuarter,
      endQuarter: config.endQuarter
    }).pipe(
      switchMap(({ startQuarter, endQuarter }) => {
        if (startQuarter && endQuarter) {
          return this.timelineService.getAllQuartersInThisRange(String(startQuarter), String(endQuarter));
        } else {
          return of(<Quarter[]>[]);
        }
      }),
      map(quarters => quarters.map(q => q.id).reverse())
    );
  }

  private getCurrentQuarterId(): Observable<string> {
    return this.timelineService.getCurrentQuarter().pipe(
      map(quarter => quarter.id)
    );
  }

  private formatCurrentQuarter(): Observable<string[]> {
    return this.getCurrentQuarterId().pipe(
      map((quarterId: string) => [quarterId])
    );
  }

  private formatPreviousQuarter(): Observable<string[]> {
    return this.timelineService.getPreviousQuarter().pipe(
      map(quarter => [quarter.id])
    );
  }

  private formatPrecedingQuarters(config: QuartersRangeFormatterConfig, binder: Binder) {
    return binder.bind(config.endQuarter).pipe(
      switchMap(endQuarter => this.getPrecedingRange(String(endQuarter), config.numberOfQuarters))
    );
  }

  private formatPrecedingCurrentQuarters(config: QuartersRangeFormatterConfig, binder: Binder) {
    return this.getCurrentQuarterId().pipe(
      switchMap(currentQuarter => this.getPrecedingRange(currentQuarter, config.numberOfQuarters))
    );
  }

  private getPrecedingRange(endQuarter: string, numberOfQuarters: number): Observable<string[]> {
    if (endQuarter) {
      return this.timelineService.getQuarterAndPastQuarters(endQuarter, numberOfQuarters - 1).pipe(
        map(quarters => quarters.map(q => q.id).reverse())
      )
    } else {
      return of([]);
    }
  }
}
