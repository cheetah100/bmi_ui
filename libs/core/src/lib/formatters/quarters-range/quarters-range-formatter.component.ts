import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { TypedFormGroup, TypedFormControl, Option } from '@bmi/ui';
import { createMapFrom } from '@bmi/utils';

import { FormatterEditorContext } from '../../formatter';
import { ParameterBinding } from '../../page-data-context/parameter-binding';
import { OptionlistService } from '../../services/optionlist.service';
import { QuartersRangeFormatterConfig } from './quarters-range-formatter.service';


interface ModeOption extends Option {
  showStartQuarter?: boolean;
  showEndQuarter?: boolean;
  showNumber?: boolean;
}

@Component({
  templateUrl: './quarters-range-formatter.component.html',
  styleUrls: ['./quarters-range-formatter.component.scss'],
})
export class QuartersRangeFormatterComponent implements OnInit {
  readonly form = new TypedFormGroup<QuartersRangeFormatterConfig>({
    formatterType: new TypedFormControl('quarters-range'),
    mode: new TypedFormControl('preceding'),
    startQuarter: new TypedFormControl(null),
    endQuarter: new TypedFormControl(null),
    numberOfQuarters: new TypedFormControl(4)
  });

  modeOptions: ModeOption[] = [
    { value: 'current-quarter', text: 'Current Quarter' },
    { value: 'previous-quarter', text: 'Previous Quarter' },
    { value: 'range', text: 'Range', showStartQuarter: true, showEndQuarter: true },
    { value: 'preceding', text: 'Preceding', showEndQuarter: true, showNumber: true },
    { value: 'preceding-current', text: 'Before Current', showNumber: true }
  ];

  modesByValue = createMapFrom(this.modeOptions, m => m.value);

  quartersOptions = this.optionlistService.getOptionGroups('quarter');

  get currentModeOption() {
    const mode = this.form.value.mode;
    return this.modesByValue.get(mode);
  }

  constructor(
    private context: FormatterEditorContext<QuartersRangeFormatterConfig>,
    private optionlistService: OptionlistService
  ) { }

  ngOnInit() {
    this.context.formatterConfig.subscribe(config => {
      if (config) {
        this.form.patchValue(config, { emitEvent: false });
      } else {
        this.form.reset({
          mode: 'preceding'
        }, { emitEvent: false });
      }
    });

    this.form.valueChanges.pipe(
      map(config => this.tidyUpConfig(config))
    ).subscribe(config => this.context.setFormatterConfig(config));
  }

  tidyUpConfig(originalConfig: QuartersRangeFormatterConfig): QuartersRangeFormatterConfig {
    const config = {
      ...originalConfig,
      formatterType: 'quarters-range',
    };

    if (config.mode === 'range') {
      delete config.numberOfQuarters;
    } else if (config.mode === 'preceding') {
      delete config.startQuarter;
    }

    return config;
  }
}
