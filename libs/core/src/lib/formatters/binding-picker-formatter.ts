import { Component, OnInit } from '@angular/core';
import { TypedFormControl } from '@bmi/ui';

import { FormatterEditorContext } from '../formatter';
import { ParameterBinding } from '../page-data-context/parameter-binding';

@Component({
  template: `<bmi-binding-picker-dropdown [formControl]="control"></bmi-binding-picker-dropdown>`,
})
export class BindingPickerFormatterComponent implements OnInit {
  readonly control = new TypedFormControl<ParameterBinding>(null);

  constructor(private context: FormatterEditorContext<ParameterBinding>) {}

  ngOnInit() {
    this.context.formatterConfig.subscribe(binding => this.control.patchValue(binding, {emitEvent: false}));
    this.control.valueChanges.subscribe(binding => this.context.setFormatterConfig(binding));
  }
}
