import { Injectable } from '@angular/core';
import { of } from 'rxjs';

import { Formatter } from '../formatter';
import { CardCacheService, Card, CardFields, CardData } from '@bmi/gravity-services';
import { map, tap } from 'rxjs/operators';
import { ConstValueFormatterConfig } from './const-value-formatter.service';
import { sortBy } from '@bmi/utils';

interface Month {
  id: string;
  startDate: number;
}


@Injectable()
export class FiscalMonthFormatterService implements Formatter<ConstValueFormatterConfig> {

  constructor(
    private cardCacheService: CardCacheService,
  ) { }


  /**
   * Apply current month if empty
   */
  format(config: ConstValueFormatterConfig) {
    if (config.value) {
      return of(config.value);
    }
    return this.cardCacheService.search({ boardId: 'fiscal_months' }).pipe(
      map(
        (cards: CardData<CardFields>[]) => sortBy(
          cards.map(
            (card: CardData<CardFields>) => ({
              id: card.id,
              startDate: card.fields.start_date,
            })
          ), ((month: Month) => month.startDate)
        )
      ),
      map((months: Month[]) => {
        if (!months.length) {
          return null;
        }
        const currentMillis = new Date().getTime();
        return months.reduce((a: string, c: Month) => {
          if (c.startDate < currentMillis) {
            return c.id;
          }
          return a;
        }, null);
      }),
    );
  }
}
