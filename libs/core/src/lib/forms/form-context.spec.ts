import { Observable, BehaviorSubject } from 'rxjs';
import { } from 'rxjs/operators';

import { FormContext, FormField } from './form-context';

import { ObservableWatcher } from '@bmi/utils';

import keyBy from 'lodash-es/keyBy';


describe('FormContext', () => {
  let ctx: FormContext;
  let fields: ObservableWatcher<FormField[]>;

  beforeEach(() => {
    ctx = new FormContext();
    fields = new ObservableWatcher(ctx.fields);
  });


  describe('Initial state', () => {
    it('should have no fields', () => expect(fields.values).toEqual([[]]));
    it('should initially have a value of {}', () => expect(new ObservableWatcher(ctx.value).latestValue).toEqual({}));
  });

  describe('Adding a field', () => {
    beforeEach(() => {
      ctx.addField({ id: 'test', label: 'Test Field' });
    });

    it('should cause fields to emit', () => expect(fields.values.length).toBe(2));
    it('should emit the same array as the current fieldsSnapshot', () => expect(fields.latestValue).toBe(ctx.fieldsSnapshot));
    it('should create a FormControl for the field', () => {
      expect(ctx.fieldsSnapshot[0].control).toBeTruthy();
    });
  });

  describe('Adding multiple fields', () => {
    beforeEach(() => {
      ctx.addFields([
        { id: 'field1' },
        { id: 'field2' },
      ]);
    });

    it('should only cause fields to emit once', () => expect(fields.values.length).toBe(2));
    it('should create the fields in the same order as the array', () => {
      expect(ctx.fieldsSnapshot.map(f => f.id)).toEqual(['field1', 'field2']);
    });
  });

  describe('get() while adding fields', () => {
    let fieldUpdates: ObservableWatcher<FormField>;

    beforeEach(() => {
      fieldUpdates = new ObservableWatcher(ctx.get('watched'));
    });

    it('should emit undefined if you subscribe to a field that does not exist', () => {
      expect(fieldUpdates.values).toEqual([undefined]);
    });

    it('should not emit anything if you add an unrelated field', () => {
      ctx.addField({ id: 'unrelated', label: 'unrelated' });
      expect(fieldUpdates.values).toEqual([undefined]);
    });

    it('should emit the field once it gets added', () => {
      ctx.addField({ id: 'watched' });
      expect(fieldUpdates.values.length).toBe(2);
      expect(fieldUpdates.latestValue.id).toBe('watched');
    })
  });

  describe('getFieldValue()', () => {
    let watch1: ObservableWatcher<any>;
    let watchMissing: ObservableWatcher<any>

    beforeEach(() => {
      ctx.addFields([
        {id: 'field1'},
      ]);

      watch1 = new ObservableWatcher(ctx.getFieldValue('field1'));
      watchMissing = new ObservableWatcher(ctx.getFieldValue('missing'));
    });

    it('should emit the current control value first', () => {
      // Default value for a FormControl is null, not undefined
      expect(watch1.values).toEqual([null]);
    });


    it('should emit undefined if the field does not exist', () =>
      expect(watchMissing.values).toEqual([undefined]));

    it('should emit once a missing control is added', () => {
      ctx.addField({id: 'missing'});
      expect(watchMissing.values).toEqual([undefined, null]);
    });


    describe('After some values set', () => {
      beforeEach(() => {
        ctx.setValue({
          field1: 'foo',
          field2: 'bar'
        });
      });

      it('should cause existing watches to emit', () =>
        expect(watch1.values).toEqual([null, 'foo']));


      it('should emit the current value for new subscriptions', () => {
        const watch = new ObservableWatcher(ctx.getFieldValue('field1'));
        expect(watch.values).toEqual(['foo']);
      });

    });


    it('should emit the current value when subscribing', () => {

    });
  });


  describe('setValue() with no fields defined', () => {
    let formValue: ObservableWatcher<any>;
    beforeEach(() => {
      formValue = new ObservableWatcher(ctx.value);
    });

    it('should emit an empty object when given an nil value', () => {
      ctx.setValue(undefined);
      expect(formValue.latestValue).toEqual({});
    });

    it('should emit an object equal to input value', () => {
      const testPayload = {
        title: 'Soul Music',
        author: 'Terry Pratchett',
      };

      ctx.setValue(testPayload);
      expect(formValue.latestValue).toEqual(testPayload);
    });
  });

  describe('setValue(), adding and removing fields', () => {
    let formValue: ObservableWatcher<any>;

    let titleWatcher: ObservableWatcher<FormField>;
    let authorWatcher: ObservableWatcher<FormField>;

    const EXAMPLE_BOOK = {
      title: 'Soul Music',
      author: 'Terry Pratchett',
      genre: 'Fantasy'
    };

    beforeEach(() => {
      ctx.addFields([
        { id: 'title' },
        { id: 'author' }
        // intentionally omitting genre
      ]);

      formValue = new ObservableWatcher(ctx.value);
      titleWatcher = new ObservableWatcher(ctx.get('title'));
      authorWatcher = new ObservableWatcher(ctx.get('author'));

      ctx.setValue(EXAMPLE_BOOK);
    });

    it('should emit the full value', () => {
      expect(formValue.latestValue).toEqual(EXAMPLE_BOOK);
    });


    it('should set the values in thne form controls', () => {
      expect(titleWatcher.latestValue.control.value).toEqual(EXAMPLE_BOOK.title);
      expect(authorWatcher.latestValue.control.value).toEqual(EXAMPLE_BOOK.author);
    });

    it('should emit an updated value if a one of the field controls is set', () => {
      titleWatcher.latestValue.control.setValue('Moving Pictures');

      expect(formValue.latestValue).toEqual({
        ...EXAMPLE_BOOK,
        title: 'Moving Pictures'
      });
    });

    it('should set the value for a newly added form control', () => {
      ctx.addField({ id: 'genre' });
      const genreWatcher = new ObservableWatcher(ctx.get('genre'));
      expect(genreWatcher.latestValue.control.value).toBe('Fantasy');
    });

    describe('clearFields()', () => {
      beforeEach(() => ctx.clearFields());
      it('should remove all fields from the fields array', () => expect(fields.latestValue).toEqual([]));
      it('should cause any observed fields to emit undefined', () => expect(titleWatcher.latestValue).toBeUndefined());
      it('should not affect the value of the form', () => expect(formValue.latestValue).toEqual(EXAMPLE_BOOK));
    });
  });

  describe('updateUntouchedFields', () => {

    let formValue: ObservableWatcher<any>;
    let titleWatcher: ObservableWatcher<FormField>;
    const EXAMPLE_BOOK = {
      title: 'Knitting with Dog Hair',
      author: 'Kendall Crolius',
    };
    const newTitle = 'Knitting with Dog Hair 2nd Ed.';

    beforeEach(() => {
      ctx.addFields([
        { id: 'title' },
        { id: 'author' },
      ]);
      formValue = new ObservableWatcher(ctx.value);
      titleWatcher = new ObservableWatcher(ctx.get('title'));
      ctx.setValue(EXAMPLE_BOOK);
    });

    it('should do nothing for no defined fields', () => {
      ctx.updateUntouchedFields({});
      expect(formValue.latestValue).toEqual(EXAMPLE_BOOK);
    });

    it('should change a value if the related field has untouched state', () => {
      ctx.updateUntouchedFields({ 'title': newTitle });
      expect(titleWatcher.latestValue.control.value).toBe(newTitle);
    });

    it('should not update a field that has been touched', () => {
      titleWatcher.latestValue.control.markAsTouched();
      ctx.updateUntouchedFields({ 'title': newTitle });
      expect(titleWatcher.latestValue.control.value).toBe(EXAMPLE_BOOK.title);
    });

    it('should not add fields', () => {
      ctx.updateUntouchedFields({ genre: 'Craft' });
      expect(formValue.latestValue).toEqual(EXAMPLE_BOOK);
    });

  });

});
