import { MappedFormSchemaForType, EditorWidgets, generatePluginMetadataFromSchema } from './generic-editor-schema';
import { PluginMetadata } from '@bmi/gravity-services';


interface ExampleConfig {
  name: string;
  enabled: boolean;
  someOptionalThing?: number;
  dontConfigureThis: string;
}


describe('Generate PluginMetadata from Schema', () => {
  let pluginMetadata: PluginMetadata;

  beforeEach(() => {
    pluginMetadata = generatePluginMetadataFromSchema<ExampleConfig>({
      name: EditorWidgets.textInput({label: 'Name', required: true}),
      enabled: EditorWidgets.checkbox({label: 'Is Enabled'}),
      // Ignoring someOptionalThing
      dontConfigureThis: null
    });
  });

  it('should generate fields for the things with defined entries in order', () =>
    expect(pluginMetadata.fields.map(f => f.field)).toEqual(['name', 'enabled']));

  it('should set the labels', () =>
    expect(pluginMetadata.fields.map(f => f.label)).toEqual(['Name', 'Is Enabled']));

  it('should set the "required" flag', () =>
    expect(pluginMetadata.fields.map(f => !!f.required)).toEqual([true, false]));
});
