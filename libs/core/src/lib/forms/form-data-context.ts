import { Binder } from '../page-data-context/binder';
import { ParameterBinding } from '../page-data-context/parameter-binding';
import { Observable, BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { FormContext } from './form-context';
import { ObjectMap } from '@bmi/utils';
import { map } from 'rxjs/operators';
import { DataBindingEvent, makeValueEvent } from '../data-binding-event';

@Injectable()
export class FormDataContext implements Binder {
  private _fetchers = new BehaviorSubject<ObjectMap<any>>({});

  constructor(private formContext: FormContext) {
    this.formContext.value.subscribe(this._fetchers);
  }

  bind<T = any>(binding: ParameterBinding): Observable<T> {
    return this._fetchers.pipe(
      map((fetchers: ObjectMap<any>) => {
        if (typeof binding === 'string' && fetchers[binding]) {
          return fetchers[binding];
        }
        return null;
      })
    );
  }

  bindEvents(binding: ParameterBinding): Observable<DataBindingEvent<any>> {
    return this.bind(binding).pipe(map(value => makeValueEvent(value)));
  }
}
