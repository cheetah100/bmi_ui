import { PluginMetadata, PluginMetadataField } from '@bmi/gravity-services';
import { ObjectMap } from '@bmi/utils';
import { WidgetConfig } from '../widget-config';

import { ParameterBinding } from '../page-data-context/parameter-binding';
import { EventActionConfig } from '../events/event-action-config';
import { BmiRoute } from '../navigation/bmi-route';
import { ConfigurableToolbarItem } from '../components/configurable-action-buttons/config';

export interface FieldOptions {
  label: string;
  required?: boolean;
}


export interface EditorFieldDescription<TTypeHint = any> extends FieldOptions {
  type: string;

  // This function is used for to provide type guidance when defining these
  // field definitions. It's never actually used/implemented, but allows the
  // `TTypeHint` to be included as part of this type allowing compatibility to
  // be checked.
  //
  // Using a function here (rather than a property: TTypeHint) makes the type
  // compatibility a bit more forgiving for union types. E.g. an
  // EditorFieldDescription<string | number> can be used with a string or a
  // number property -- this might be an edge case that isn't likely to come up
  __typeKey?(x: TTypeHint): void;
}


export function mkFieldDescription<T>(type: string, options: FieldOptions) {
  // Angular metadata can't spread, so doing this mapping manually. This is
  // pretty awful, can be fixed by
  return {
    type: type,
    label: options.label,
    required: options.required
  };
}


export type MappedFormSchemaForType<T> = {[P in keyof T]: EditorFieldDescription<T[P]>};


export class EditorWidgets {
  static textInput(options: FieldOptions): EditorFieldDescription<string> {
    return mkFieldDescription('form-control:text-input', options);
  }

  static numberInput(options: FieldOptions): EditorFieldDescription<number> {
    return mkFieldDescription('form-control:numeric-input', options);
  }

  static checkbox(options: FieldOptions): EditorFieldDescription<boolean> {
    return mkFieldDescription('form-control:checkbox', options);
  }

  static textarea(options: FieldOptions): EditorFieldDescription<string> {
    return mkFieldDescription('form-control:textarea', options);
  }

  static bindingPicker(options: FieldOptions): EditorFieldDescription<ParameterBinding> {
    return mkFieldDescription('editor-form-control:binding-picker', options);
  }

  static formatter(options: FieldOptions): EditorFieldDescription<ParameterBinding> {
    return mkFieldDescription('editor-form-control:formatter', options);
  }

  static eventEditor(options: FieldOptions): EditorFieldDescription<EventActionConfig[]> {
    return mkFieldDescription('editor-form-control:event-editor', options);
  }

  static boardPicker(options: FieldOptions): EditorFieldDescription<string> {
    return mkFieldDescription('BOARD', options);
  }

  static routeSelector(options: FieldOptions): EditorFieldDescription<BmiRoute> {
    return mkFieldDescription('editor-form-control:route-selector', options)
  }

  static actionButtons(options: FieldOptions): EditorFieldDescription<ConfigurableToolbarItem[]> {
    return mkFieldDescription('editor-form-control:action-buttons', options);
  }
}


export function generatePluginMetadataFromSchema<T extends ObjectMap<any>>(schema: MappedFormSchemaForType<T>) {
  return <PluginMetadata> {
    name: `Generated plugin from a schema object`,
    fields: Object.entries(schema)
      .filter(([fieldId, def]) => !!def)
      .map(([fieldId, def]) => (<PluginMetadataField>{
        field: fieldId,
        label: def.label,
        required: def.required,
        type: def.type,
      })
    )
  };
}
