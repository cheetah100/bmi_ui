import { WidgetConfig } from '../widget-config';

export interface FormControlWidgetFields {
  fieldId: string;
  required?: boolean;
}

export type FormControlWidgetConfig<T extends FormControlWidgetFields = FormControlWidgetFields> = WidgetConfig<T>;
