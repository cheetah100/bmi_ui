import { Component, Input, forwardRef, OnInit, OnChanges } from '@angular/core';
import { Subject, BehaviorSubject, NEVER, combineLatest } from 'rxjs';
import { filter, map, shareReplay, tap } from 'rxjs/operators';
import { PluginMetadata } from '@bmi/gravity-services';
import {  provideAsControlValueAccessor, BaseControlValueAccessor } from '@bmi/ui';
import { ObjectMap } from '@bmi/utils';

import { PluginFormService } from './plugin-form.service';
import { PageDataContext } from '../../page-data-context/page-data-context';
import { provideDataContextServices } from '../../page-data-context/helpers';
import { FormContext } from '../form-context';
import { ObjectDataSource } from '../../data-sources/object-data-source';

import { FilterService } from '@bmi/core';

@Component({
  selector: 'bmi-plugin-form',
  template: `
    <bmi-widget-host mode="view" [config]="formWidgetConfig | async"></bmi-widget-host>
  `,

  providers: [
    FormContext,
    provideAsControlValueAccessor(forwardRef(() => PluginFormComponent)),
    provideDataContextServices(),
  ]
})
export class PluginFormComponent
  extends BaseControlValueAccessor<ObjectMap<any>>
  implements OnInit, OnChanges {
  @Input() pluginMetadata: PluginMetadata = undefined;
  @Input() boardId: string = null;
  @Input() defaultColumnWidth = 4;

  private _pluginMetadataSubject = new BehaviorSubject(this.pluginMetadata);
  private _columnWidthSubject = new BehaviorSubject(this.defaultColumnWidth);

  formWidgetConfig = combineLatest([
    this._pluginMetadataSubject,
    this._columnWidthSubject]
  ).pipe(
    map(([plugin, defaultWidth]) => plugin && this.pluginFormService.getFormWidgetConfig(plugin, defaultWidth)),
    shareReplay(1)
  );

  constructor(
    private formContext: FormContext,
    private pluginFormService: PluginFormService,
    private dataContext: PageDataContext
  ) {
    super();
  }

  ngOnInit() {
    this._pluginMetadataSubject.subscribe(pluginMetadata => {
      this.formContext.clearFields();
      if (pluginMetadata) {
        this.pluginFormService.addFormContextFields(
          pluginMetadata,
          this.formContext,
          this.boardId
        );
      }
    });

    this.dataContext.setDataSources({
      form: new ObjectDataSource(this.formContext.value)
    });

    this.formContext.value.subscribe(value => {
      if (!this.isWriteValueInProgress) {
        this.onChanged(value);
      }
    });
  }

  ngOnChanges(changes) {
    if (changes.pluginMetadata) {
      this._pluginMetadataSubject.next(this.pluginMetadata);
    }

    if (changes.defaultColumnWidth) {
      this._columnWidthSubject.next(this.defaultColumnWidth);
    }
  }

  updateComponentValue(value: ObjectMap<any>) {
    this.formContext.setValue(value);
  }
}
