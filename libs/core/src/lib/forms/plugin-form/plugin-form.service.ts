import { Injectable } from '@angular/core';
import {
  PluginMetadata,
  PluginMetadataField,
  PluginFieldType
} from '@bmi/gravity-services';

import { WidgetConfig } from '../../widget-config';
import { GridLayoutConfig, GridLayoutSection, } from '../../widgets/grid-layout/grid-layout-config';
import { FormContext, FormFieldConfig, FormFieldOptions } from '../form-context';
import { FormControlWidgetConfig } from '../form-control-widget-config';

import { createMapFrom } from '@bmi/utils';

@Injectable({ providedIn: 'root' })
export class PluginFormService {
  addFormContextFields(
    pluginMetadata: PluginMetadata,
    formContext: FormContext,
    boardId: string
  ): void {
    // At least one of the defined plugins has 2 board selectors.
    // We'll only set the form's board context for the simpler case of 1.
    const boardFields = pluginMetadata.fields.filter(
      field => field.type === 'BOARD'
    );
    const fields = pluginMetadata.fields.map((field: PluginMetadataField) => {
      const formField: FormFieldConfig = {
        ...field,
        id: field.field
      };
      const options: FormFieldOptions = { board: boardId };
      if (field.board) {
        // get the field referred to
        const referencedField = pluginMetadata.fields.find(f => f.field === field.board);
        options.useBoardFromField = referencedField.field;
      } else {
        if (boardFields.length === 1) {
          options.useBoardFromField = boardFields[0].field;
        }
      }
      switch (field.type) {
        case 'CREDENTIAL':
        case 'BOARD':
          formField.optionlist = `gravity-system-${field.type}`;
          break;
        case 'RESOURCE':
        case 'PHASE':
        case 'FIELD':
        case 'MAP':
        case 'LIST':
          formField.optionlist = `gravity-system-${field.type}`;
          formField.options = options;
          break;
        default:
          break;
      }
      return formField;
    });
    formContext.clearFields();
    formContext.addFields(fields);
  }

  getControlWidgetType(formField: PluginMetadataField): string {
    switch (formField.type) {
      case 'BOOLEAN':
        return 'form-control:checkbox';
      case 'RESOURCE':
        return 'resource-selector';
      case 'CREDENTIAL':
      case 'BOARD':
      case 'PHASE':
        return 'form-control:dropdown-select';
      case 'FIELD':
        return 'form-control:select-input';
      case 'LIST':
        return 'form-control:list';
      case 'MAP':
        return 'form-control:map';
      case 'CRONEXPRESSION':
        return 'form-control:cron';
      case 'STRING':
        return 'form-control:text-input';
      default:
        return formField.type;
    }
  }

  getControlWidgetConfig(actionField: PluginMetadataField) {
    return {
      widgetType: this.getControlWidgetType(actionField),
      fields: {
        fieldId: actionField.field,
        label: actionField.label
      }
    };
  }

  getFormWidgetConfig(pluginMetadata: PluginMetadata, defaultWidth = 4): WidgetConfig {
    const actionFields = pluginMetadata.fields;
    const formControlWidgets = actionFields.map(field =>
      this.getControlWidgetConfig(field)
    );
    const layoutHints = makeLayoutHintsForPlugin(pluginMetadata, defaultWidth);

    return createGridFromLayoutHints(formControlWidgets, layoutHints);

    // return {
    //   widgetType: 'grid-layout',
    //   fields: {
    //     neverShowCards: true,
    //     sections: [
    //       {
    //         layoutId: '3-column',
    //         cells: actionFields.map(field => ({
    //           widgetConfig: this.getControlWidgetConfig(field)
    //         }))
    //       }
    //     ]
    //   }
    // };
  }
}

// IDEA: we should have 'layout hint' metadata that can be separate from the
//       fields so that we can do a nicer job of layouts.
//
//
// These types define the layout hint. It's very minimal, but introduces a
// grouping concept (we could eventually have named groups). Each field is
// identified by id, and can have some metadata like width (in 12 column grid)

interface FormLayoutGroup {
  name?: string;
  fields: FormLayoutField[];
}

interface FormLayoutField {
  field: string;
  width?: number;
}

// Example:
//
//   const layoutHint: FormLayoutGroup[] = [
//     {
//       name: 'Source',
//       fields: [
//         {field: 'board', width: 4},
//         {field: 'view', width: 4}
//       ]
//     },
//     {
//       name: 'Mapping',
//       fields: [
//         {field: 'properties', width: 12}
//       ]
//     }
//   ];
//
// This format is pretty minimal, and it's not coupled to plugins or anything.
// Gravity could choose to auto-generate the layout hints from the metadata, but
// given this doesn't exist in the backend yet anyway, we should have some way
// to generate layouts for a Plugin.

function makeLayoutHintsForPlugin(
  pluginMetadata: PluginMetadata,
  defaultWidth = 4
): FormLayoutGroup[] {
  // Some controls look a bit weird when they are floating in the middle of a
  // layout, so we'll just make these ones full width; the rest will be the
  // default.
  const fullWidthControls = new Set<PluginFieldType>([
    'MAP',
    'LIST',
    'CRONEXPRESSION'
  ]);

  // Just one section because there's no other info to go off.
  return [
    {
      fields: pluginMetadata.fields.map(f => ({
        field: f.field,
        width: fullWidthControls.has(f.type) ? 12 : defaultWidth
      }))
    }
  ];
}

// And now, we want a way to arrange some form controls into a grid layout,
// based on the layout hints.
//
// This will create grid sections for each group (eventually we should add
// support for titles here too; a simple Markdown widget may suffice).

function createGridFromLayoutHints(
  controls: FormControlWidgetConfig[],
  layoutSections: FormLayoutGroup[]
): GridLayoutConfig {
  const unusedControls = [...controls];
  function takeControl(id: string): FormControlWidgetConfig | undefined {
    const controlIndex = unusedControls.findIndex(c => c.fields.fieldId === id);
    if (controlIndex >= 0) {
      const control = unusedControls[controlIndex];
      unusedControls.splice(controlIndex, 1);
      return control;
    } else {
      return undefined;
    }
  }

  // Build each section. We should insert the group titles at the start too.
  const groupSections: GridLayoutSection[] = layoutSections.map(s => ({
    cells: s.fields.map(f => ({
      span: f.width || 12,
      widgetConfig: takeControl(f.field)
    }))
  }));

  // If we have any left over form controls, we will stick them in a section at
  // the end. This will accommodate anything that's not in the layout hint
  const leftoverSection: GridLayoutSection = {
    cells: unusedControls.map(c => ({ span: 12, widgetConfig: c }))
  };

  return {
    widgetType: 'grid-layout',
    fields: {
      neverShowCards: true,
      sections: [...groupSections, leftoverSection].filter(
        s => s.cells.length > 0
      )
    }
  };
}
