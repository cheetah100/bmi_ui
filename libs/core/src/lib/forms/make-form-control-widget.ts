import { FormField } from './form-context';
import { FormControlWidgetConfig } from './form-control-widget-config';


export function makeFormControlWidget(formField: FormField): FormControlWidgetConfig {
  const fieldType = formField.type || 'STRING';
  const label = formField.label || '';
  let widgetType;
  switch (fieldType) {
    case 'STRING':
      widgetType = !!formField.optionlist ? 'form-control:dropdown-select' : 'form-control:text-input';
      break;
    case 'NUMBER':
      widgetType = 'form-control:numeric-input';
      break;
    case 'BOOLEAN':
      widgetType = 'form-control:checkbox';
      break;
    case 'DATE':
      widgetType = 'form-control:datepicker';
      break;
    case 'LIST':
      if (!!formField.optionlist) {
        /**
         * Array field with optionlist configured: pick from a list of boards
         */
        widgetType = 'form-control:dropdown-multiselect';
      } else {
        /**
         * Array field with no configured optionlist: a list of user-entered values
         */
        widgetType = 'form-control:list';
      }
      break;
    case 'MAP':
      widgetType = 'form-control:map';
      break;
    default:
      widgetType = 'form-control:text-input';
  }
  const formControlWidgetConfig = {
     widgetType,
     fields: {
       fieldId: formField.id,
       label
     }
  };
  return formControlWidgetConfig;
}
