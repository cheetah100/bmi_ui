import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BmiCoreWidgetModule } from '../bmi-core-widget.module';


import { PluginFormComponent } from './plugin-form/plugin-form.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BmiCoreWidgetModule,
  ],

  declarations: [
    PluginFormComponent,
  ],

  exports: [
    PluginFormComponent,
  ]
})
export class BmiFormsModule {}
