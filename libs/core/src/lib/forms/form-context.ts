import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable, BehaviorSubject, combineLatest, Subject, of, concat, defer, merge } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  map,
  startWith,
  switchMap,
  take
} from 'rxjs/operators';
import { distinctUntilNotEqual } from '../util/distinct-until-not-equal';

export interface FormFieldOptions {
  board?: string;
  useBoardFromField?: string;
}

/**
 * The config metadata for a form field
 */
export interface FormFieldConfig {
  id: string;
  label?: string;
  required?: boolean;
  immutable?: boolean
  /**
   * Optional: the field type
   */
  type?: string;

  /**
   * Optional: the suggested board to use as the optionlist for this field.
   */
  optionlist?: string;
  options?: FormFieldOptions;
}

/**
 * Represents a field on a form, and provides access to a FormControl model.
 */
export interface FormField extends FormFieldConfig {
  readonly control: FormControl;
  readonly value: Observable<any>;
}

interface FormData {
  [field: string]: any;
}

export interface FormAction {
  type: 'save';
  [fields: string]: any;
}

@Injectable()
export class FormContext<T extends FormData = FormData> {
  private inputData = new BehaviorSubject<FormData>({});
  private formFields = new BehaviorSubject<FormField[]>([]);
  private formGroup = new FormGroup({});

  private combinedValue = new BehaviorSubject<FormData>({});

  private _triggeredAction = new Subject<FormAction>();

  /**
   * Gets the fields available on this form
   */
  public readonly fields = this.formFields.asObservable();

  /**
   * The current value of this form context. This is the combination of the
   * current form state and any left over fields that were set.
   */
  public readonly value = this.combinedValue.asObservable();

  public readonly actions = this._triggeredAction.asObservable();

  public readonly isValid = merge(
    this.value,
    this.formGroup.statusChanges
  ).pipe(
    map(() => this.formGroup.valid),
    distinctUntilChanged(),
  );

  /**
   * Observable to access to expose formGroup value changes
   */
  public changedFields = this.formGroup.valueChanges.pipe(
    map(fields => fields)
  );

  constructor() {
    combineLatest([
      this.inputData,
      this.formGroup.valueChanges.pipe(startWith({}))
    ])
      .pipe(
        map(([inputData, formValue]) => ({ ...inputData, ...formValue } as T)),
        distinctUntilNotEqual()
      )
      .subscribe(value => this.combinedValue.next(value));

    this.formFields.subscribe(fields => this.updateFormGroupItems(fields));
  }

  get fieldsSnapshot() {
    return this.formFields.value as ReadonlyArray<FormField>;
  }


  /**
   * Gets a form field from this form context
   *
   * If it doesn't exist this will emit an 'undefined' value. If the form field
   * is changed this will emit another value.
   */
  get(id: string): Observable<FormField> {
    return this.formFields.pipe(
      map(fields => fields.find(f => f.id === id)),
      distinctUntilChanged()
    );
  }

  /**
   * Gets the value of a form field, emitting whenever it changes.
   *
   * If the form field does not exist, this will emit undefined
   */
  getFieldValue(id: string): Observable<any> {
    return this.get(id).pipe(
      switchMap(field => field ? field.value : of(undefined)),
      distinctUntilChanged()
    );
  }

  setFieldValue(id: string, value: any) {
    const fieldData = {}
    fieldData[id] = value;
    this.formGroup.patchValue(fieldData);
  }

  addField(fieldConfig: FormFieldConfig): void {
    this.addFields([fieldConfig]);
  }

  /**
   * Add fields to this form group.
   *
   * If you're adding multiple fields, using this method will avoid spamming
   * updates to the field array etc. -- all of the fields will be added and then
   * the state updated once.
   */
  addFields(fieldConfigs: FormFieldConfig[]): void {
    const fields = [...this.fieldsSnapshot];
    for (const config of fieldConfigs) {
      if (!fields.find(f => f.id === config.id)) {
        fields.push(mkFormField(config));
      }
    }

    this.formFields.next(fields);
  }

  clearFields() {
    this.formFields.next([]);
  }

  setValue(value: T) {
    this.inputData.next(value || {});
    this.formGroup.reset(value || {});
  }

  updateUntouchedFields(values: T) {
    this.value.pipe(take(1)).subscribe((updateValues: T) => {
      Object.keys(values)
        .filter((key: string) => {
          const control = this.formGroup.get(key);
          return !!control && control.untouched;
        })
        .forEach((key: string) => {
          updateValues = {
            ...updateValues,
            [key]: values[key]
          };
        });
      this.inputData.next(updateValues || {});
      this.formGroup.patchValue(updateValues, { emitEvent: true });
    });
  }

  private updateFormGroupItems(currentFields: FormField[]) {
    const formGroupControls = this.formGroup.controls;
    const fieldsToAdd = currentFields.filter(f => !formGroupControls[f.id]);
    const fieldsToRemove = Object.keys(formGroupControls).filter(
      id => !currentFields.find(f => f.id === id)
    );

    const combinedValueBeforeAddingControls = this.combinedValue.value;

    for (const field of fieldsToAdd) {
      const validators = [];
      if (field.required) {
        validators.push(Validators.required);
      }
      field.control.setValidators(validators);
      this.formGroup.addControl(field.id, field.control);
    }

    for (const id of fieldsToRemove) {
      this.formGroup.removeControl(id);
    }

    // This feeds the current combined value back into the form group, allowing
    // it to update any form controls that have been added.
    this.formGroup.patchValue(combinedValueBeforeAddingControls, {
      emitEvent: true
    });
  }

  /**
   * Triggers an action on this form context
   *
   * This has no inherent meaning to the form context itself, but allows for
   * some simple componnent interaction, such as triggering the form to save.
   */
  triggerAction(action: FormAction) {
    this._triggeredAction.next(action);
  }

  watchForAction(type: string) {
    return this.actions.pipe(filter(a => a.type === type));
  }
}


function mkFormField(config: FormFieldConfig): FormField {
  const control = new FormControl();
  if (config.immutable) {
    control.disable()
  }
  return {
    ...config,
    control: control,
    value: watchControlValue(control)
  };
}


function watchControlValue(control: FormControl): Observable<any> {
  return concat(
    defer(() => of(control.value)),
    control.valueChanges
  ).pipe(
    distinctUntilChanged()
  );
}
