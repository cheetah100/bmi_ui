import { Inject, Injectable, Optional } from '@angular/core';
import { Observable, of } from 'rxjs';
import { WidgetDefinition, WidgetDefinitionService, BMI_WIDGET_EDITORS } from '../widget-definition'
import { PlaceholderWidgetComponent } from '../widget-host/placeholder-widget.component';


@Injectable({providedIn: 'root'})
export class EditorWidgetDefinitionService implements WidgetDefinitionService {
  constructor(
    @Optional() @Inject(BMI_WIDGET_EDITORS) private widgetEditors: WidgetDefinition[]
  ) {
    if (!widgetEditors) {
      this.widgetEditors = [];
    }
  }

  getWidgetDefinition(widgetType: string): Observable<WidgetDefinition> {
    const def = this.widgetEditors.find(x => x.widgetType === widgetType);
    return of(def || this.getPlaceholder(widgetType));
  }

  getPlaceholder(widgetType: string): WidgetDefinition {
    return {
      widgetType: widgetType,
      component: PlaceholderWidgetComponent,
      options: {
        message: `'${widgetType}' has no editable properties`
      }
    };
  }
}
