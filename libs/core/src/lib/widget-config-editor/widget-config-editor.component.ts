import { Component, Input, Output, EventEmitter, OnInit, OnChanges, OnDestroy, SimpleChanges, forwardRef } from '@angular/core';

import { BehaviorSubject, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

import { TypedFormControl, BaseControlValueAccessor, provideAsControlValueAccessor } from '@bmi/ui';

import { WidgetConfig, WidgetSelection } from '../widget-config';
import { FlairConfig } from '../flair/flair-config';
import { WidgetDefinitionService } from '../widget-definition';
import { EditorWidgetDefinitionService } from './editor-widget-definition.service';
import { distinctUntilNotEqual } from '../util/distinct-until-not-equal';


@Component({
  selector: 'bmi-widget-config-editor',
  templateUrl: './widget-config-editor.component.html',
  providers: [
    provideAsControlValueAccessor(forwardRef(() => WidgetConfigEditorComponent))
  ]
})
export class WidgetConfigEditorComponent extends BaseControlValueAccessor<WidgetConfig> implements OnInit, OnChanges, OnDestroy {
  constructor(
    public editorDefService: EditorWidgetDefinitionService
  ) {
    super();
  }

  @Input() widgetConfig: WidgetSelection;
  @Input() flairEditing: boolean = true;
  @Output() updateWidgetConfig = new EventEmitter<WidgetConfig>();

  private subscription = new Subscription();
  widgetConfigSubject = new BehaviorSubject<WidgetConfig>(null);

  private widgetTypeSupportsFlairs = false;

  flairs = new TypedFormControl<FlairConfig[]>([]);

  get currentConfig() {
    return this.widgetConfigSubject.value;
  }

  ngOnInit() {
    this.subscription.add(this.widgetConfigSubject.pipe(
      map(config => config && config.flairs ? config.flairs : []),
      distinctUntilNotEqual(),
    ).subscribe(flairs => this.flairs.setValue(flairs, {emitEvent: false})));

    this.subscription.add(this.flairs.valueChanges.subscribe(flairConfigs => {
      if (this.currentConfig && typeof this.currentConfig === 'object') {
        this.widgetConfigUpdated(this.currentConfig, flairConfigs);
      }
    }));
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.widgetConfig) {
      this.updateComponentValue((this.widgetConfig && typeof this.widgetConfig === 'object') ? this.widgetConfig : null);
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  updateComponentValue(config: WidgetConfig) {
    this.widgetConfigSubject.next(config);
  }

  widgetConfigUpdated(config: WidgetConfig, flairConfigs: FlairConfig[] = this.flairs.value) {
    const updatedConfig = {
      ...config,
      flairs: flairConfigs && flairConfigs.length > 0 ? flairConfigs : null
    };

    this.updateWidgetConfig.emit(updatedConfig);
    this.onChanged(updatedConfig);
  }

  get canEditConfig() {
    return !!this.currentConfig && typeof (this.currentConfig) !== 'string';
  }
}
