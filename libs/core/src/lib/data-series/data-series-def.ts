import { ParameterBinding } from '../page-data-context/parameter-binding';
import { ObjectMap } from '@bmi/utils';


export interface DataSeriesDef {
  dataSourceId: string;
  seriesId: ParameterBinding;
  label: ParameterBinding;
  fields: ObjectMap<ParameterBinding>;
}
