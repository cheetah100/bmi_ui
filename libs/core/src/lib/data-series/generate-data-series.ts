import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { Fetcher } from '../page-data-context/fetcher';
import { ParameterBinding } from '../page-data-context/parameter-binding';
import { DataSeries } from './data-series';

import mapValues from 'lodash-es/mapValues';
import groupBy from 'lodash-es/groupBy';

export interface DataSeriesBindingMap {
  seriesId: ParameterBinding;
  seriesLabel?: ParameterBinding;
  [key: string]: ParameterBinding;
}


export interface DataSeriesExtractorConfig {
  fields: DataSeriesBindingMap;
}


export function generateSeriesFromListSource(
  listFetcher: Fetcher,
  config: DataSeriesExtractorConfig,
): Observable<DataSeries[]> {
  return listFetcher.bindThis(Object.values(config.fields)).pipe(
    map(result => {
      const datapoints = result.rows.map(row => mapValues(config.fields, b => row.get(b)));
      return Object.entries(groupBy(datapoints, x => String(x.seriesId)))
        .map(([seriesId, ds]) => <DataSeries>{
          seriesId: seriesId,

          // This assumes the label will be consistent for the entire series
          // which might not be true.
          label: String((ds[0] && ds[0].seriesLabel) || seriesId),
          datapoints: ds
        });
    })
  );
}
