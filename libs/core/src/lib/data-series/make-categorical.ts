import { DataSeries } from './data-series';
import flatten from 'lodash-es/flatten';
import uniq from 'lodash-es/uniq';


/**
 * Process some data series, rewriting the X axis so that it's an index into an
 * array of categories.
 *
 * Highcharts works with numeric axes, so this rewriting step lets us plot
 * discrete string values on charts across multiple series.
 *
 * Finding the categories can be done with the `findCategories` utilitiy, or you
 * can pre-configure the categories you want to show. If a datapoint does not
 * match a category IT WILL BE CLIPPED. This means
 */
export function makeCategorical(serieses: DataSeries[], categories: string[], field = 'x'): DataSeries[] {
  const indexMap = new Map<string, number>();
  categories.forEach((c, index) => indexMap.set(c, index));
  return serieses.map(series => {
    const results: typeof series.datapoints = [];
    for (const dp of series.datapoints) {
      const categoryIndex = indexMap.get(String(dp[field]));
      if (categoryIndex !== undefined) {
        results.push({
          ...dp,
          [field]: categoryIndex
        });
      }
    }

    return {
      ...series,
      datapoints: results
    };
  });
}


/**
 * Scans some data series, finding the set of unique values (as strings)
 *
 * By default, this uses the 'x' field on datapoints, but this can be overriden.
 *
 * This can optionally take an array of category strings which will always be
 * included as categories, in the order they are specified. Any additional
 * categories found will be added to the end of this array.
 *
 * @param serieses The data series to scan
 * @param field The field to use
 * @param initialCategories [optional] an array of categories that must exist.
 * These will come first, in this order.
 */
export function findCategories(serieses: DataSeries[], initialCategories: string[] = [], field = 'x') {
  const xValues = flatten(serieses.map(s => s.datapoints.map(d => String(d[field]))));
  return uniq([...initialCategories, ...xValues]);
}