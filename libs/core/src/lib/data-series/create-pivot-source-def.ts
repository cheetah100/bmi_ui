import { ParameterBinding } from '../page-data-context/parameter-binding';
import { DataSeriesDef } from './data-series-def';


function pathBinding(path: string): ParameterBinding {
  return { bindingType: 'data-path', path: path };
}


export function createPivotSeriesDef(sourceId: string): DataSeriesDef {
  return {
    dataSourceId: sourceId,
    seriesId: pathBinding('this.y'),
    label: pathBinding('this.y'),
    fields: {
      x: pathBinding('this.x'),
      y: pathBinding('this.value'),
    }
  };
}
