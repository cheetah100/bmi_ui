import { makeCategorical, findCategories } from './make-categorical';
import { DataSeries } from './data-series';

import cloneDeep from 'lodash-es/cloneDeep';
import omit from 'lodash-es/omit';

describe('makeCategorical', () => {
  const TEST_DATA: DataSeries[] = [
    {
      seriesId: 'actual',
      label: 'Actual',
      datapoints: [ {x: 'Q1', y: 45}, {x: 'Q2', y: 50}, {x: 'Q3', y: 42}, {x : 'Q4', y: 39}]
    },
    {
      seriesId: 'goal',
      label: 'Goal',

      // putting these datapoints backwards...
      datapoints: [{ x: 'Q4', y: 70 }, { x: 'Q3', y: 60 }, { x: 'Q2', y: 65 }, { x: 'Q1', y: 67 }]
    },
  ];


  let data: DataSeries[];
  let processed: DataSeries[];
  let categories: string[];

  beforeEach(() => {
    data = cloneDeep(TEST_DATA);
    categories = findCategories(data);
    processed = makeCategorical(data, categories);
  });

  it('should not modify the original data', () => {
    expect(data).toEqual(TEST_DATA);
  });

  it('should output an array of categories', () => {
    expect(categories).toEqual(['Q1', 'Q2', 'Q3', 'Q4']);
  });

  it('should preserve series metadata', () => {
    const before = data.map(s => omit(s, 'datapoints'));
    const after = processed.map(s => omit(s, 'datapoints'));

    expect(before).toEqual(after);
  });

  it('should preserve the y values (including data point order)', () => {
    const before = data.map(s => s.datapoints.map(d => d.y));
    const after = processed.map(s =>   s.datapoints.map(d => d.y));
    expect(before).toEqual(after);
  });

  it('should rewrite the X axis as numbers', () => {
    const after = processed.map(s => s.datapoints.map(d => d.x));
    expect(after).toEqual([
      [0, 1, 2, 3],
      [3, 2, 1, 0]
    ]);
  });
});
