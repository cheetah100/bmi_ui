import { ObjectMap } from '@bmi/utils';


export interface DataSeries {
  seriesId: string;
  label?: string;
  datapoints: SeriesDataPoint[];
}


export type SeriesDataPoint = ObjectMap<any>;
