import { Observable, of, EMPTY } from 'rxjs';

import { DataSeries } from './data-series';
import { generateSeriesFromListSource, DataSeriesExtractorConfig } from './generate-data-series';
import { makeBoundList, BoundListResult } from '../page-data-context/bound-list-result';
import { Fetcher } from '../page-data-context/fetcher';
import { ParameterBinding, isThisPathBinding, getThisPath } from '../page-data-context/parameter-binding';


export class TestListFetcher<T extends {}> implements Fetcher {
  state = EMPTY;

  constructor(private items: T[]) {}

  private lookup(item: T, binding: ParameterBinding): unknown {
    if (isThisPathBinding(binding)) {
      return item[getThisPath(binding)] as unknown;
    } else {
      throw new Error('Only `this` bindings are supported in this mock data source!');
    }
  }

  bindThis(bindings: ParameterBinding[]): Observable<BoundListResult> {
    const data = this.items.map(item => bindings.map(binding => this.lookup(item, binding)));
    return of(makeBoundList({
      listBindings: bindings,
      listData: data
    }));
  }
}


function path(bindingPath: string): ParameterBinding {
  return { bindingType: 'data-path', path: bindingPath }
}


describe('generateSeriesFromListSource()', () => {
  let source: Fetcher;
  beforeEach(() => {
    source = new TestListFetcher([
      {kpi: 'K1', quarter: 'Q1FY20', value: 1},
      {kpi: 'K1', quarter: 'Q2FY20', value: 2},
      {kpi: 'K1', quarter: 'Q3FY20', value: 3},
      {kpi: 'K1', quarter: 'Q4FY20', value: 4},

      {kpi: 'K2', quarter: 'Q1FY20', value: 52},
      {kpi: 'K2', quarter: 'Q2FY20', value: 50},
      {kpi: 'K2', quarter: 'Q3FY20', value: 49}
    ]);
  });

  describe('Extracting series', () => {
    let series: DataSeries[];

    beforeEach(() => {
      generateSeriesFromListSource(source, {
        fields: {
          seriesId: path('this.kpi'),
          x: path('this.quarter'),
          y: path('this.value')
        }
      }).subscribe(emitted => series = emitted);
    });

    it('should generate an array of results', () => {
      expect(Array.isArray(series)).toBeTruthy();
    });

    it('should create the right number of series', () => {
      expect(series.length).toBe(2);
    });

    it('should have the correct number of data points in each series', () => {
      expect(series.find(s => s.seriesId === 'K1').datapoints.length).toBe(4);
      expect(series.find(s => s.seriesId === 'K2').datapoints.length).toBe(3);
    });

    describe('The datapoints', () => {
      let datapoints: DataSeries['datapoints'];

      beforeEach(() => {
        datapoints = series.find(s => s.seriesId === 'K1').datapoints;
      });

      it('should extract the X axis values', () => {
        expect(datapoints.map(d => d['x'])).toEqual(['Q1FY20', 'Q2FY20', 'Q3FY20', 'Q4FY20']);
      });

      it('should extract the Y axis values', () => {
        expect(datapoints.map(d => d['y'])).toEqual([1, 2, 3, 4]);
      })
    })
  });
});
