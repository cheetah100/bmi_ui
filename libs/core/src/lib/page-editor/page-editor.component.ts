import {
  Component, Input, Output, EventEmitter, OnInit, OnChanges, SimpleChanges,
  ViewChild, ComponentFactoryResolver, Injector
} from '@angular/core';
import { ComponentPortal } from '@angular/cdk/portal';
import { timer } from 'rxjs';
import { map, materialize } from 'rxjs/operators';

import { ModalService, ModalResult } from '@bmi/ui';

import { PageDataSourceEditorModalComponent } from '../page-data-source-manager/page-data-source-editor-modal.component';
import { DataContextEditorModalService } from '../page-data-source-manager/data-context-editor-modal.service';

import cloneDeep from 'lodash-es/cloneDeep';

import { PageConfig, isValidPageConfig } from '../page-config';
import { EditableAspect } from '../editor/editable-aspect';
import { WidgetMode } from '../widget-context';
import { EditorContext } from '../editor/editor-context';
import { PageEditorContext } from './page-editor-context';
import { PageComponent } from '../page/page.component';
import { WidgetTemplateService } from '../widget-config-template.service';
import { WidgetRepositoryService } from '../services/widget-repository.service';
import { PageDataConfig } from '../page-data-context/page-data-config';

import { clipboardCopyText, clipboardPasteText } from '../util/clipboard';

import { BmiRouteHandler } from '../navigation/bmi-route-handler';
import { ConfirmBeforeNavigateRouteHandler } from '../navigation/confirm-before-navigate-route-handler';


@Component({
  selector: 'bmi-page-editor',
  templateUrl: './page-editor.component.html',
  styleUrls: ['./page-editor.component.scss'],
  providers: [
    PageEditorContext,
    // EditorContext is lightweight class interface for PageEditorContext.
    //
    // Using this in client-widgets that just need simple editor interactions
    // creates isolation between the editor code and the runtime code. Merely
    // mentioning PageEditorContext as a DI key in a widget will cause Webpack
    // to pull in a chain of unwanted dependencies when we ship the app.
    { provide: EditorContext, useExisting: PageEditorContext },
    { provide: BmiRouteHandler, useClass: ConfirmBeforeNavigateRouteHandler }
  ]
})
export class PageEditorComponent implements OnInit, OnChanges {
  @Input() config: PageConfig;
  @Output() updateConfig = new EventEmitter<PageConfig>();

  @ViewChild(PageComponent, { static: true }) private pageInstance: PageComponent;

  selectedAspect: EditableAspect = null;
  mode: WidgetMode = 'edit';

  get isViewMode() {
    return this.mode === 'view';
  }

  get pageConfig(): PageConfig {
    return this.undoController.currentState;
  }

  get undoController() {
    return this.pageEditorContext.undoController;
  }

  constructor(
    private pageEditorContext: PageEditorContext,
    private widgetTemplateService: WidgetTemplateService,
    private widgetRepositoryService: WidgetRepositoryService,
    private modalService: ModalService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private injector: Injector,
    private dataContextEditor: DataContextEditorModalService,
  ) { }

  ngOnInit() {
    this.initializePage();
    this.pageEditorContext.selected.subscribe(aspect => this.selectedAspect = aspect);
    this.pageEditorContext.pageConfig.subscribe(config => this.updateConfig.emit(config));
  }

  private initializePage() {
    this.pageEditorContext.initialize({
      pageConfig: this.config || this.createBlankPage(),
      pageInjectorGetter: () => this.pageInstance.injector
    });

    this.showPageProperties();

    // Ensure the widgets are refreshed when we start editing a page.
    const appId = this.pageEditorContext.pageConfigSnapshot.applicationId;
    if (appId) {
      this.widgetRepositoryService.refreshWidgets(appId);
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.config) {
      // If the config input changes, then we'll replace the current page state.
      // This might mean we're now editing a different page; or that the page has
      // been saved and this is the new authoritative config.
      // Doing this will reset the undo state and clear the selection.
      this.initializePage();
    }
  }

  createBlankPage() {
    return {
      id: null,
      name: 'Untitled Page',
      type: 'page-v2',
      applicationId: null,
      permissions: null,
      ui: {
        rootWidget: this.widgetTemplateService.createTemplate('bmi-page-layout-template')
      }
    };
  }

  undo() {
    this.undoController.undo();
  }

  redo() {
    this.undoController.redo();
  }

  onPageClicked(event: MouseEvent) {
    this.showPageProperties();
  }

  showPageProperties() {
    this.pageEditorContext.select({
      owner: this,
      tempEditorTypeId: 'page-properties'
    })
  }

  copyPageJson() {
    clipboardCopyText(JSON.stringify(this.undoController.currentState, null, 2))
      .catch(error => alert('Copy Error: ' + error));
  }

  pastePageJson() {
    clipboardPasteText()
      .then(pastedText => this.updatePageFromJson(pastedText))
      .catch(error => {
        alert('Paste Error: ' + error);
      });
  }

  private updatePageFromJson(json: string) {
    const page = JSON.parse(json) as PageConfig;
    if (!isValidPageConfig(page)) {
      throw new Error('Page config is not valid. Please ensure the type is correct and it has a root widget config');
    }

    this.pageEditorContext.undoController.apply({
      ...this.pageConfig,

      // We pick the properties we want to copy over.
      name: page.name,
      urlId: page.urlId,

      ui: page.ui
    });
  }

  toggleViewMode() {
    if (this.mode === 'edit') {
      this.mode = 'view'
    } else {
      this.mode = 'edit'
    }

    this.pageInstance.requestWidgetsResize();
  }

  showPageDataEditor() {
    this.dataContextEditor.show({
      title: 'Page Data Sources',
      injector: this.pageInstance.injector,
      config: cloneDeep(this.pageEditorContext.pageConfigSnapshot.ui.dataContext)
    }).subscribe(result => {
      this.pageEditorContext.updatePageConfig(c => c.ui.dataContext = result)
    });
  }

}
