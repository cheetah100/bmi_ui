import { Injectable, Injector } from '@angular/core';

import { Observable } from 'rxjs';
import { distinctUntilChanged, map, switchMap } from 'rxjs/operators';

import { EditorContext } from '../editor/editor-context';
import { UndoController } from '../util/undo-controller';
import { cloneAndUpdate } from '../util/clone-and-update';

import { PageConfig } from '../page-config';
import { WidgetConfig } from '../widget-config';

import { WidgetRepositoryService } from '../services/widget-repository.service';

import lodashIsEqual from 'lodash-es/isEqual';

@Injectable()
export class PageEditorContext extends EditorContext {
  public readonly undoController = new UndoController<PageConfig>(null);

  private _pageInjectorGetter: () => Injector = null;

  constructor(
    private widgetRepository: WidgetRepositoryService
  ) {
    super();
  }

  get appId(): Observable<string> {
    return this.pageConfig.pipe(
      map(config => config.applicationId),
      distinctUntilChanged()
    );
  }

  get allWidgets(): Observable<WidgetConfig[]> {
    return this.appId.pipe(
      switchMap(appId => this.widgetRepository.getAllWidgets(appId)),
    );
  }

  getWidget(widgetId: string): Observable<WidgetConfig> {
    return this.appId.pipe(
      switchMap(appId => this.widgetRepository.getWidget(appId, widgetId))
    );
  }

  /**
   * (Re-)Initializes this service with the initial page state.
   */
  initialize(options: {
    pageConfig: PageConfig,
    pageInjectorGetter: () => Injector,
  }) {
    this.undoController.initialize(options.pageConfig);
    this._pageInjectorGetter = options.pageInjectorGetter;
  }

  get pageConfig(): Observable<PageConfig> {
    return this.undoController.state;
  }

  get pageConfigSnapshot(): PageConfig {
    return this.undoController.currentState;
  }

  /**
   * Gets the page's injector.
   *
   * This provides access to the page's injector instance so that the sidebar
   * can get access to the live filter/data services.
   *
   * This privilege should not be abused! If the injector isn't available, this
   * will return null.
   */
  get pageInjector(): Injector {
    return this._pageInjectorGetter ? this._pageInjectorGetter() || null : null;
  }

  updatePageConfig(callback: (pageConfig: PageConfig) => void) {
    const newPageConfig = cloneAndUpdate(this.pageConfigSnapshot, callback);

    if (!lodashIsEqual(newPageConfig, this.pageConfigSnapshot)) {
      this.undoController.apply(newPageConfig);
    }
  }
}
