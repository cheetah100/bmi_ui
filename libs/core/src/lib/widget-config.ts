import { InjectionToken } from '@angular/core';
import { FlairConfig } from './flair/flair-config';

export interface WidgetConfig<T extends { [key: string]: any } = { [key: string]: any }> {
  id?: string;
  title?: string;
  flairs?: FlairConfig[];
  widgetType: string;
  fields: T;
  generatedWidget?: boolean;
}


/** @deprecated Just delete this. Concept is no longer required */
export type WidgetSelection<T extends WidgetConfig = WidgetConfig> = T;


export interface WidgetConfigTemplateDef {
  id?: string;
  name: string;
  icon?: string;
  groups?: string[];
  sortWeight?: number;
  description?: string;
  type?: string;
  widgetConfig: WidgetConfig;
}

export const BMI_WIDGET_CONFIG_TEMPLATE = new InjectionToken<WidgetConfigTemplateDef>('BMI Widget Config Templates');


export function looksLikeWidgetConfig(widgetConfig: WidgetConfig) {
  return widgetConfig && typeof (widgetConfig) === 'object' &&
    widgetConfig.widgetType && typeof (widgetConfig.widgetType) === 'string' &&
    widgetConfig.fields && typeof (widgetConfig.fields) === 'object';
}


export function validateWidgetSelection(value: any, appWidgetIds: string[] = []): WidgetSelection {
  if (value === null || value === undefined) {
    return null;
  } else if (looksLikeWidgetConfig(value as WidgetConfig)) {
    return value as WidgetConfig;
  } else {
    throw new Error('Widget config is not valid');
  }
}
