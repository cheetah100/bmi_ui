/*
 * Public API Surface of bmi-core
 */

export * from './lib/widget-host/widget-host.component';
export * from './lib/widget-context';

export * from './lib/widget-config';
export * from './lib/widget-definition';

export * from './lib/page-config';
export * from './lib/page/page-info.service';
export * from './lib/page-migrations/migrate-page';

export { BmiCoreModule } from './lib/bmi-core.module';
export { BmiCoreEditorModule } from './lib/bmi-core-editor.module';
export { BmiWidgetsModule } from './lib/widgets/bmi-widgets.module';
export {
  BmiWidgetsEditorModule
} from './lib/widgets/bmi-widgets-editor.module';

export {
  defineWidget,
  defineGeneratorWidget
} from './lib/util/widget-def-helpers';
export {
  defineConfigTemplate,
  defineEditor,
  defineWidgetConverter
} from './lib/util/editor-def-helpers';
export {
  GeneratorService,
  GeneratorWidgetOptions
} from './lib/widgets/generator-widget/generator-service';

export {
  BmiFlairRegistryModule,
  registerFlairFactory
} from './lib/flair/bmi-flair-registry.module';
export {
  BmiFlairEditorRegistryModule,
  provideFlairEditor
} from './lib/flair/bmi-flair-editor.registry.module';

export { PageRepositoryService } from './lib/services/page-repository.service';
export { SsoLogoutService } from './lib/services/sso-logout.service';
export {
  WidgetRepositoryService
} from './lib/services/widget-repository.service';
export { Module, ModuleService, ModuleMenuConfig, ModuleExtraSettings } from './lib/services/module.service';
export { RoutesService, BMI_PAGE_ROUTE_GENERATOR } from './lib/services/routes.service';
export { WidgetTemplateService } from './lib/widget-config-template.service';
export { PageComponent } from './lib/page/page.component';
export { Binder } from './lib/page-data-context/binder';
export * from './lib/page-data-context/parameter-binding';
export * from './lib/data-binding/utils';
export { provideDataContextServices } from './lib/page-data-context/helpers';
export { CurrentUserService } from './lib/services/current-user.service';
export {
  CardEditorModalService
} from './lib/card-editor/card-editor-modal.service';

export * from './lib/page-editor/page-editor.component';
export * from './lib/page-editor/page-editor-context';

export * from './lib/widget-config-editor/widget-config-editor.component';

export * from './lib/router-resolvers/module.resolver';
export * from './lib/router-resolvers/module-page.resolver';

export * from './lib/navigation/bmi-link';
export * from './lib/navigation/bmi-route';
export * from './lib/navigation/menu-item';
export * from './lib/navigation/bmi-route-handler'

export {
  BmiCoreComponentsModule
} from './lib/components/bmi-core-components.module';
export {
  ScoreMatrixComponent
} from './lib/components/score-matrix/score-matrix.component';
export {
  ScoreMatrixCell,
  ScoreMatrixData
} from './lib/components/score-matrix/score-matrix-types';

export {
  AppHeaderComponent
} from './lib/app-layout/header/app-header.component';
export {
  AppSidebarComponent
} from './lib/app-layout/sidebar/app-sidebar.component';

export { ModalPageService } from './lib/page/modal-page.service';

export * from './lib/forms/form-context';
export * from './lib/forms/form-data-context';
export * from './lib/forms/form-control-widget-config';

export {
  GridLayoutCell,
  GridLayoutSection,
  GridLayoutConfig,
  GridLayoutUtils
} from './lib/widgets/grid-layout/grid-layout-config';

export * from './lib/page-data-context/page-data-context';
export * from './lib/filters/filter.service';
export * from './lib/services/optionlist.service';
export * from './lib/widgets/form-controls/dropdown-select/select-options';
