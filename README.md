# BMI Monorepo: frontend apps, libs, application server and other NodeJS-y utils

This is the main codebase for the BMI web application. It consists of:

 * BMI Runtime Angular app (`apps/bmi`)
 * BMI Admin (`apps/bmi-admin`)
 * Several Angular libraries: @bmi/ui, @bmi/core, @bmi/gravity-services, @bmi/utils, etc.
 * An Express server to host the Angular apps which gets built into a docker
   image and deployed to k8s


## Monorepo

The apps and libraries are contained in an NX workspace as a monorepo.

## Developing

The app currently uses Angular v11.x. You will need NodeJS 10 LTS. Using a
newer version will cause compatibility issues, so stick with v10.

You shoudn't need to install anything globally besides NPM as most of commands
you want to run can be bootstrapped from the npm command.


### Install Dependencies

From the root of the checked out code. This will install the main 

```
$ npm ci
```

**Note**: Using `npm ci` will clear any existing node_modules and ensure the versions
exactly match the package lock. Using the command `npm i` on Windows can
sometimes be faster -- this will not delete existing packages.

### Doing Angular development

If you want to do work on the Angular apps, you can run them with `npm start`. In local dev mode you can choose which app to work on; it will default to the admin app, but you can also build SPC, e.g.


To build the default (bmi-admin):

```
$ npm run start
```

### Doing a prod build, and running this locally

If you want to test the whole system, you can build the Angular apps, then
serve them up from the real Express server. This isn't convenient for
development but it's good if you just want to run the apps locally.


Do a prod build of the apps and server:

```
$ npm run build-all:prod
```


This will put the compiled apps (html/js/css etc.) into `dist/apps/{bmi-admin,legacy-spc}`.
It will also build the NodeJS server application into `dist/apps/bmi-frontend-server`

You can run this server by changing into that dir and running `node main.js`
or by using the command `npm start server -- <options>`  (note, the `--` stops
the `npm` command from eating any options you want to pass to the server app)


For example, to run the server and point it at a local Gravity you might do something like this:

```
npm run server -- --gravity-url="http://localhost:8080/gravity"
```


### Client Secrets

To connect to the dev instance of Gravity, you'll need client secrets. The
apps, and server can support a JSON file containing the secrets and Gravity
URL for more convenient config. The Angular dev server is configured to expect
a `../bmi-dev-secrets.json` file (i.e. it should sit alongside the Git repo so
that it doesn't ever get committed)


For example:

```
{
  "client_secret": "<base64 encoded user:password>",
  "gravityUrl": "https://gravity-instance.com/gravity"
}
```


You can use this with the server too

```
$ npm run server -- --proxy-secrets ../bmi-dev-secrets.json
```
