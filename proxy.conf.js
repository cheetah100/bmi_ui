const { PingClientAuthToken, loadSecretsConfig, makeGravityProxyConfig } = require('./libs/ping-auth-helpers');

const proxyDisabled = process.env['DISABLE_DEV_PROXY'] || false;

if ( proxyDisabled ) {
  module.exports = {}
} else {
  const DEFAULT_SECRETS_FILE = '../bmi-dev-secrets.json';
  const secretsConfigFilename = process.env['PROXY_SECRETS_CONFIG'] || DEFAULT_SECRETS_FILE;


  const config = loadSecretsConfig(secretsConfigFilename);
  if (!config.gravityUrl) {
    throw new Error('Secrets Config: gravityUrl not specified, cannot set up proxy.');
  }

  // TODO: Specify
  const clientAuthToken = new PingClientAuthToken('AUTH_TOKEN_URL', config);

  module.exports = {
    "/gravity": makeGravityProxyConfig(config.gravityUrl, '/gravity', clientAuthToken)
  };
}
